NyxSql ReadMe
------------------------------------------------------------------------
Projectpage https://nyx.sigterm.eu/
SourceForge.net http://sourceforge.net/projects/nyxsql
------------------------------------------------------------------------

About Storing Settings
------------------------------------------------------------------------
Nyx currently differs between the following settings which names are defined 
through the app.config:
- Bookmarks (nyx-bookmarks.xml)
	The Bookmarks.
- RuntimeSettings (nyx-userruntime.xml)
	Splitter Distances, Toolstrippositions/visiblity, last Save/Load Path and so on...
- History (nyx-history.xml)
	The successfull executed queries from the user input
- Profiles (nyx-profiles.xml)
	The connection details for the defined profiles (Password stored encrypted).
- UserSettings (nyx-usersettings.xml)
	Auto-Check/Retrieve-Settings, Fonts, Colors and so on...
- HotKeys (nyx-hotkeys.xml)
	The HotKeys/Shortcutkeys available in NyxSql.
	
Further Nyx supports saving all settings inside the custom user path 
\Documents and Settings\<user>\Application Data\Nyx\ or inside the
application path itself. The behavior for all can be defined inside the SettingsManager,
further you have to set the inital locations at the first start of Nyx.

Please note, that the files are not automatically copied from one location to another
if you change it through the SettingsManager.

Licensing of included Assemblys
------------------------------------------------------------------------
The assemblys included/distributed with this software are under their own license!
Please read the licenses carefully if you accept/decline it (located in doc\License-*.txt).
The copyright/trademark and so on belong to the original distributor.

Oracle ODP.Net		see License-Oracle.ODP.Net.txt
MySql.Data		see License-MySql.Data.txt
NpgSql / Mono.Security	see License-Npgsql.txt

Credits
------------------------------------------------------------------------
- Thorsten (ThojToy) J�tte and Ralf (Cyber) Purr for testing and the endurance with me... :)
- Carsten (Ozymandias) for creating the logo
- everyone who helped me getting Nyx to what it is at the moment :)
	

misc.
------------------------------------------------------------------------
IDE:
	Using Visual C# Express Edition to develope Nyx, thanks to MS for delivering it for free.
Icons:
	99% of the icons are from the silk icon set of Mark James / http://www.famfamfam.com/lab/icons/silk/ - really a great work! :)) - Creative Commons Attribution 2.5 license - see Readme-SilkIcons.txt
	2 icons are from the Tango icon library / http://tango.freedesktop.org/Tango_Icon_Gallery - Creative Commons Attribution Share-Alike license

Archiver for building the release zips:
	7zip / http://www.7-zip.org/
	

Supported Databases
------------------------------------------------------------------------
- Oracle (ODP.Net)
- MSSql (SqlClient)
- MySqL (MySqlClient)
- PgSql (Experimental)
- ODBC

