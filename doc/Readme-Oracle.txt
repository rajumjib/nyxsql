The OracleSupport is based on ODP.NET from Oracle.com (See License-Oracle.ODP.Net.txt).

I started using ODP.NET instead of System.Data.OracleClient cause of the advanced features
and better memory/resource usage which results in faster responses (specially with huge data).

The Problem which start appearing when using this Client was, that the installed OracleClient
must have the same Release of the ODP.NET Client. Lukily the .NET Assembly should be distributed 
with every Oracle Client. Badly, if I would add the Assemblyreference to the GAC (Global Assembly 
Cache), Nyx would be completely unusable if the OracleClient isn't installed.

So I've decided to distribute an Oracle.DataAccess.dll for the latest tested Oracle Edition
and if it does not fit your version, you have to do the following:
- take a look into %oracle_home%\bin for the Oracle.DataAccess.dll
- grab it and copy it into the program folder (which holds NyxSql.exe)
- done :)

When you haven't got the needed Oracle.DataAccess.dll you have to download the ODP.NET package
which fits your version from http://www.oracle.com and grab it outside there.
The other possible solution is to install the latest OracleClient on your machine and use this
insted your old client (OracleClients are normally compatible 2 versions older and newer).