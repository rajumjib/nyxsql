namespace NyxUpdate
{
    partial class Download
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Download));
            this._lblStatus = new System.Windows.Forms.Label();
            this._tbllayout = new System.Windows.Forms.TableLayoutPanel();
            this._btCancel = new System.Windows.Forms.Button();
            this._curSpeed = new System.Windows.Forms.Label();
            this._curStatus = new System.Windows.Forms.Label();
            this._progress = new System.Windows.Forms.ProgressBar();
            this._lvOverview = new System.Windows.Forms.ListView();
            this._lvcName = new System.Windows.Forms.ColumnHeader();
            this._lvc2download = new System.Windows.Forms.ColumnHeader();
            this._lvcFinished = new System.Windows.Forms.ColumnHeader();
            this._tbllayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lblStatus
            // 
            resources.ApplyResources(this._lblStatus, "_lblStatus");
            this._lblStatus.Name = "_lblStatus";
            // 
            // _tbllayout
            // 
            resources.ApplyResources(this._tbllayout, "_tbllayout");
            this._tbllayout.Controls.Add(this._lvOverview, 0, 3);
            this._tbllayout.Controls.Add(this._btCancel, 2, 2);
            this._tbllayout.Controls.Add(this._lblStatus, 0, 0);
            this._tbllayout.Controls.Add(this._curSpeed, 2, 0);
            this._tbllayout.Controls.Add(this._curStatus, 1, 0);
            this._tbllayout.Controls.Add(this._progress, 0, 1);
            this._tbllayout.Name = "_tbllayout";
            // 
            // _btCancel
            // 
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Image = global::NyxUpdate.Properties.Resources.cancel;
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btCancel_Click);
            // 
            // _curSpeed
            // 
            resources.ApplyResources(this._curSpeed, "_curSpeed");
            this._curSpeed.Name = "_curSpeed";
            // 
            // _curStatus
            // 
            resources.ApplyResources(this._curStatus, "_curStatus");
            this._curStatus.Name = "_curStatus";
            // 
            // _progress
            // 
            resources.ApplyResources(this._progress, "_progress");
            this._tbllayout.SetColumnSpan(this._progress, 3);
            this._progress.Name = "_progress";
            // 
            // _lvOverview
            // 
            resources.ApplyResources(this._lvOverview, "_lvOverview");
            this._lvOverview.BackColor = System.Drawing.Color.AliceBlue;
            this._lvOverview.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._lvcName,
            this._lvc2download,
            this._lvcFinished});
            this._tbllayout.SetColumnSpan(this._lvOverview, 3);
            this._lvOverview.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this._lvOverview.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lvOverview.Items"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lvOverview.Items1"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lvOverview.Items2")))});
            this._lvOverview.Name = "_lvOverview";
            this._lvOverview.UseCompatibleStateImageBehavior = false;
            this._lvOverview.View = System.Windows.Forms.View.Details;
            // 
            // _lvcName
            // 
            resources.ApplyResources(this._lvcName, "_lvcName");
            // 
            // _lvc2download
            // 
            resources.ApplyResources(this._lvc2download, "_lvc2download");
            // 
            // _lvcFinished
            // 
            resources.ApplyResources(this._lvcFinished, "_lvcFinished");
            // 
            // Download
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tbllayout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Download";
            this.Load += new System.EventHandler(this.Download_Load);
            this._tbllayout.ResumeLayout(false);
            this._tbllayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _lblStatus;
        private System.Windows.Forms.TableLayoutPanel _tbllayout;
        private System.Windows.Forms.Label _curSpeed;
        private System.Windows.Forms.Label _curStatus;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.ProgressBar _progress;
        private System.Windows.Forms.ListView _lvOverview;
        private System.Windows.Forms.ColumnHeader _lvcName;
        private System.Windows.Forms.ColumnHeader _lvc2download;
        private System.Windows.Forms.ColumnHeader _lvcFinished;
    }
}