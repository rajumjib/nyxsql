/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.IO;
using System.Windows.Forms;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace NyxUpdate
{
    public partial class UpdateMain : Form
    {   
        private static System.Globalization.CultureInfo nyxCI = System.Globalization.CultureInfo.InvariantCulture;
        /// <remarks> cultureinfo </remarks>
        public static System.Globalization.CultureInfo NyxCI { get { return nyxCI; } }
        private static string verChk = null;
        private static string target = "https://sigterm.eu/";
        private static string nyxPath = Application.StartupPath;
        private static string nyxExec = "\\NyxSql.exe";
        private static string nyxUpdFile;
        private struct strThrCom
        {
            private string version;
            private string changelog;
            private Exception errExc;

            public string Version
            {
                get { return version; }
                set { version = value; }
            }
            public string Changelog
            {
                get { return changelog; }
                set { changelog = value; }
            }
            public Exception ErrExc
            {
                get { return errExc; }
                set { errExc = value; }
            }
        }
        private string verAct;
        private static ConnectionSettings sett = new ConnectionSettings();
        /// <remarks> target </remarks>
        public static string Target { get { return target; } }
        /// <remarks> nyxpath </remarks>
        public static string Nyxpath { get { return nyxPath; } }
        /// <remarks> file </remarks>
        public static string NyxUpdFile { get { return nyxUpdFile; } }
        /// <remarks> connectionsettings </remarks>
        public static ConnectionSettings Sett
        {
            get { return sett; }
            set { sett = value; }
        }
        /// <summary>
        /// delegate
        /// </summary>
        /// <param name="param"></param>
        public delegate void ThreadCom(object param);

        /// <summary> Updater for nyxsql </summary>
        public UpdateMain()
        {   
            // init components
            InitializeComponent();
            // set certcallback
            ServicePointManager.ServerCertificateValidationCallback = 
                new RemoteCertificateValidationCallback(ValidateServerCertificate);
        }
        private void CheckUpdate_Load(object sender, EventArgs e)
        {   // check if NyxSql.exe is here
            if (!System.IO.File.Exists(nyxPath + nyxExec))
            {
                ErrorMsg("NyxSql not found!");
                Application.Exit();
            }
            // check if directory is here
            if (!Directory.Exists(nyxPath + "update"))
            {
                try
                {
                    Directory.CreateDirectory(nyxPath + "\\update");
                }
                catch (Exception exc)
                {
                    ErrorMsg("Error creating directory \"update\":\n" + exc.Message.ToString());
                    Application.Exit();
                }
            }
            // retrieve version from nyxsql
            try
            {
                System.Diagnostics.FileVersionInfo fvi = 
                    System.Diagnostics.FileVersionInfo.GetVersionInfo(nyxPath + nyxExec);
                verAct = fvi.FileVersion;
                this._ssVersion.Text = verAct + " / ";
            }
            catch (Exception exc)
            {
                ErrorMsg("Couldn't retrieve NyxSql-Version!\n" + exc.Message.ToString());
                Application.Exit();
            }
            // load settings
            Settings.DeSerialize(ref sett);
            CheckVersion();
        }
        
        // retrieve version
        private void CheckVersion()
        {   // set gui
            CtrlProgressBar(true);
            this._ms.Enabled = false;
            this._lblResult.Text = "Checking Version...";
            this._lblVersion.Text = "Retrieving Version...";
            // start thread
            System.Threading.Thread thrCheck = new System.Threading.Thread(new System.Threading.ThreadStart(ThrCheckVersion));
            thrCheck.IsBackground = true;
            thrCheck.Start();
        }
        private void ThrCheckVersion()
        {   // vars
            System.Net.WebResponse response = null;
            strThrCom thrCom = new strThrCom();
            try
            {   // creating request
                System.Net.WebRequest request = System.Net.WebRequest.Create(target + "dev/nyx/nyxver.php");
                PrepareRequest(ref request);
                // trying to get the response
                response = request.GetResponse();
                // get the new version
                using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
                {   // reading one line...
                    thrCom.Version = sr.ReadLine();
                }
            }
            catch (TimeoutException)
            {   // err
                Exception exc = new Exception("Timeout");
                thrCom.ErrExc = exc;
            }
            catch (Exception exc)
            {   // err
                thrCom.ErrExc = exc;
            }
            finally { this.BeginInvoke(new ThreadCom(CheckVersionDone), thrCom); }
        }
        private void CheckVersionDone(object oThrCom)
        {   // cast
            strThrCom thrCom = (strThrCom)oThrCom;
            // checkup
            if (thrCom.ErrExc == null && thrCom.Version != null)
            {
                verChk = thrCom.Version;
                // set gui
                this._lblVersion.Text += "done.";
                this._picbVersion.Image = global::NyxUpdate.Properties.Resources.accept;
                this._picbVersion.Visible = true;
                // now we check if the version is newer than ours
                this._lblResult.Visible = true;
                this._ssVersion.Text = verChk;
                nyxUpdFile = "NyxSql-" + verChk;
                if (verAct != verChk)
                {
                    this._lblResult.Text = "New Version available..." + verChk;
                    this._picbResult.Image = global::NyxUpdate.Properties.Resources.accept;
                    this._picbResult.Visible = true;
                }
                else
                {
                    this._lblResult.Text = "No new Version available.";
                    this._picbResult.Image = global::NyxUpdate.Properties.Resources.delete;
                    this._picbResult.Visible = true;
                }
                // enable gui
                this._msChangelog.Enabled = true;
                this._msDownload.Enabled = true;
            }
            else
            {
                this._lblVersion.Text += "failed.";
                this._picbVersion.Image = global::NyxUpdate.Properties.Resources.delete;
                this._picbVersion.Visible = true;
                // disable gui
                this._msChangelog.Enabled = false;
                this._msDownload.Enabled = false;
                // err
                ErrorMsg("Error connecting.");
            }
            // set gui
            this._ms.Enabled = true;
            CtrlProgressBar(false);
        }

        // retrieve changelog
        private void _msChangelog_Click(object sender, EventArgs e)
        {   // set gui
            this._ms.Enabled = false;
            CtrlProgressBar(true);
            // start thread
            System.Threading.Thread thrCheck = new System.Threading.Thread(new System.Threading.ThreadStart(ThrGetChangelog));
            thrCheck.IsBackground = true;
            thrCheck.Start();
        }
        private void ThrGetChangelog()
        {
            // vars
            System.Net.WebResponse response = null;
            strThrCom thrCom = new strThrCom();

            try
            {
                // creating request
                System.Net.WebRequest request = System.Net.WebRequest.Create(target + "dev/nyx/Changelog.txt");
                PrepareRequest(ref request);
                // trying to get the response
                response = request.GetResponse();
                // get the new version
                using (System.IO.StreamReader sr = new System.IO.StreamReader(response.GetResponseStream()))
                {   // reading one line...
                    thrCom.Changelog = sr.ReadToEnd();
                }
            }
            catch (TimeoutException)
            {   // err
                Exception exc = new Exception("Timeout");
                thrCom.ErrExc = exc;
            }
            catch (Exception exc)
            {   // err
                thrCom.ErrExc = exc;
            }
            finally
            {
                // final invoke
                this.BeginInvoke(new ThreadCom(GetChangelogDone), thrCom);
            }
        }
        private void GetChangelogDone(object oThrCom)
        {
            // cast
            strThrCom thrCom = (strThrCom)oThrCom;
            // checkup
            if (thrCom.ErrExc == null && thrCom.Changelog != null)
                this._rtbChangelog.Text = thrCom.Changelog;
            else
                this._rtbChangelog.Text = "Failed to retrieve Changelog:\n" + thrCom.ErrExc.Message.ToString();
            // set gui
            this._ms.Enabled = true;

            CtrlProgressBar(false);
        }
        
        // gui options
        private void _msCheckVer_Click(object sender, EventArgs e)
        {
            CheckVersion();
        }
        private void _msSettings_Click(object sender, EventArgs e)
        {
            SettingsGUI sg = new SettingsGUI();
            sg.ShowDialog();
        }
        private void _msDownload_Click(object sender, EventArgs e)
        {
            if (!sett.GetSetup && !sett.GetSource && !sett.GetZip)
            {
                MessageBox.Show("No Downloadfile selected, please configure first.", "Error",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }

            Download dwn = new Download();
            DialogResult dRes = dwn.ShowDialog();
            if (dRes == DialogResult.OK)
            {
                DialogResult drQst = MessageBox.Show("Download successfull to update\\" + nyxUpdFile + ", Update?", 
                    "Update", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (drQst == DialogResult.Yes)
                {
                    UpdateDialog upd = new UpdateDialog();
                    upd.ShowDialog();
                }
            }
        }
        private void _msExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// error message
        /// </summary>
        /// <param name="msg"></param>
        public static void ErrorMsg(string msg)
        {
            MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        /// <summary>
        /// prepare request
        /// </summary>
        /// <param name="request"></param>
        public static void PrepareRequest(ref System.Net.WebRequest request)
        {
            request.Timeout = 5000;
            // got proxy?
            if (sett.ProxyEnabled && sett.ProxyAddress != null)
            {
                // port
                if (sett.ProxyPort <= 0)
                    sett.ProxyPort = 3128;
                // build webproxy
                System.Net.WebProxy wProxy = new System.Net.WebProxy();
                wProxy.Address = (new Uri(sett.ProxyAddress + ":" + sett.ProxyPort.ToString()));
                // check credentials
                if (sett.ProxyUser != null)
                {
                    System.Net.NetworkCredential netCred = new System.Net.NetworkCredential();
                    netCred.UserName = sett.ProxyUser;
                    if (sett.ProxyPasw != null)
                        netCred.Password = sett.ProxyPasw;
                    // apply to wproxy
                    wProxy.Credentials = netCred;
                }
                // apply wproxy to request
                request.Proxy = wProxy;
            }
        }
        /// <summary>
        /// validate certificate
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="certificate"></param>
        /// <param name="chain"></param>
        /// <param name="sslPolicyErrors"></param>
        /// <returns></returns>
        public static bool ValidateServerCertificate(Object sender, X509Certificate certificate,
            X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {   // required cause i don't want to pay the fees for a real certificate...
            return true;
        }
        private void CtrlProgressBar(bool start)
        {
            if (start)
            {
                this.Cursor = Cursors.AppStarting;
                this._ssPg.Style = ProgressBarStyle.Marquee;

            }
            else
            {
                this.Cursor = Cursors.Default;
                this._ssPg.Style = ProgressBarStyle.Blocks;
                this._ssPg.Value = 0;
            }
        }
    }
}