namespace NyxUpdate
{
    /// <summary> updatedialog interface </summary>
    partial class UpdateDialog
    {
        /// <summary> Required designer variable </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateDialog));
            this._rtbStatus = new System.Windows.Forms.RichTextBox();
            this._btClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _rtbStatus
            // 
            resources.ApplyResources(this._rtbStatus, "_rtbStatus");
            this._rtbStatus.Name = "_rtbStatus";
            this._rtbStatus.ReadOnly = true;
            this._rtbStatus.TabStop = false;
            // 
            // _btClose
            // 
            this._btClose.Image = global::NyxUpdate.Properties.Resources.delete;
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // UpdateDialog
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._btClose);
            this.Controls.Add(this._rtbStatus);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "UpdateDialog";
            this.Load += new System.EventHandler(this.Update_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox _rtbStatus;
        private System.Windows.Forms.Button _btClose;
    }
}