/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace NyxUpdate
{
    /// <summary> download interface </summary>
    public partial class Download : Form
    {
        private enum eDwnType { zip, setup, source, notset }
        private DateTime dwnStart;
        private System.Net.WebClient wclient;
        private eDwnType DwnType = eDwnType.notset;

        /// <summary> initialize </summary>
        public Download()
        {
            InitializeComponent();
        }
        private void Download_Load(object sender, EventArgs e)
        {   // init display and select first download action

            this._lvOverview.Items[0].Text = UpdateMain.NyxUpdFile + ".zip";
            if (UpdateMain.Sett.GetZip)
            {
                this._lvOverview.Items[0].SubItems[1].Text = "Yes";
                DwnType = eDwnType.zip;
            }
            else
                this._lvOverview.Items[0].SubItems[1].Text = "No";

            this._lvOverview.Items[1].Text = UpdateMain.NyxUpdFile + "-src.zip";
            if (UpdateMain.Sett.GetSource)
            {
                this._lvOverview.Items[1].SubItems[1].Text = "Yes";
                if (DwnType == eDwnType.notset)
                    DwnType = eDwnType.source;
            }
            else
                this._lvOverview.Items[1].SubItems[1].Text = "No";

            this._lvOverview.Items[2].Text = UpdateMain.NyxUpdFile + ".exe";
            if (UpdateMain.Sett.GetSetup)
            {
                this._lvOverview.Items[2].SubItems[1].Text = "Yes";
                if (DwnType == eDwnType.notset)
                    DwnType = eDwnType.setup;
            }
            else
                this._lvOverview.Items[2].SubItems[1].Text = "No";
            // start download
            DownloadFile();
        }

        private void DownloadFile()
        {   // webclient
            wclient = new System.Net.WebClient();
            PrepareWebclient(ref wclient);
            // 
            string fileName = null;
            switch (DwnType)
            {
                case eDwnType.zip:
                    fileName = UpdateMain.NyxUpdFile + ".zip";
                    this._lvOverview.Items[0].SubItems[2].Text = "Pending...";
                    break;
                case eDwnType.source:
                    fileName = UpdateMain.NyxUpdFile + "-src.zip";
                    this._lvOverview.Items[1].SubItems[2].Text = "Pending...";
                    break;
                case eDwnType.setup:
                    fileName = UpdateMain.NyxUpdFile + ".exe";
                    this._lvOverview.Items[2].SubItems[2].Text = "Pending...";
                    break;
            }
            
            // start download
            this.Cursor = Cursors.AppStarting;
            dwnStart = DateTime.Now;
            Uri uriTarget = new Uri(UpdateMain.Target + "dev/nyx/" + fileName); ;
            wclient.DownloadFileAsync(uriTarget, UpdateMain.Nyxpath + "\\update\\" + fileName);
        }
        private void PrepareWebclient(ref System.Net.WebClient wclient)
        {
            // got proxy?
            if (UpdateMain.Sett.ProxyEnabled && UpdateMain.Sett.ProxyAddress != null)
            {   // port
                if (UpdateMain.Sett.ProxyPort <= 0)
                    UpdateMain.Sett.ProxyPort = 3128;
                // build webproxy
                System.Net.WebProxy wProxy = new System.Net.WebProxy();
                wProxy.Address = (new Uri(UpdateMain.Sett.ProxyAddress + ":" + UpdateMain.Sett.ProxyPort.ToString()));
                // check credentials
                if (UpdateMain.Sett.ProxyUser != null)
                {
                    System.Net.NetworkCredential netCred = new System.Net.NetworkCredential();
                    netCred.UserName = UpdateMain.Sett.ProxyUser;
                    if (UpdateMain.Sett.ProxyPasw != null)
                        netCred.Password = UpdateMain.Sett.ProxyPasw;
                    // apply to wproxy
                    wProxy.Credentials = netCred;
                }
                // apply wproxy to request 
                wclient.Proxy = wProxy;
            }
            // initialize eventhandlers
            wclient.DownloadFileCompleted +=
                    new System.ComponentModel.AsyncCompletedEventHandler(downloadFileCompleted);
            wclient.DownloadProgressChanged +=
                new System.Net.DownloadProgressChangedEventHandler(downloadProgressChanged);
        }
        private void _btCancel_Click(object sender, EventArgs e)
        {
            try
            {   // cleanup
                if (wclient.IsBusy)
                    wclient.CancelAsync();
            }
            catch (Exception exc)
            {
                UpdateMain.ErrorMsg("Error cleaning up:\n" + exc.Message.ToString());
            }
            finally
            {   // finally close
                this.DialogResult = DialogResult.Abort;
                this.Close();
            }
        }
        
        private void downloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                UpdateMain.ErrorMsg("Error Downloading:\n" + e.Error.ToString());
                this.DialogResult = DialogResult.Abort;
            }
            else
            {
                switch (DwnType)
                {
                    case eDwnType.zip:
                        this._lvOverview.Items[0].SubItems[2].Text = "Done";
                        if (UpdateMain.Sett.GetSource)
                        {
                            DwnType = eDwnType.source;
                            DownloadFile();
                            return;
                        }
                        else
                            this.DialogResult = DialogResult.Cancel;
                        break;
                    case eDwnType.source:
                        this._lvOverview.Items[1].SubItems[2].Text = "Done";
                        if (UpdateMain.Sett.GetSetup)
                        {
                            DwnType = eDwnType.setup;
                            DownloadFile();
                            return;
                        }
                        else
                            this.DialogResult = DialogResult.Cancel;
                        break;
                    case eDwnType.setup:
                        this._lvOverview.Items[2].SubItems[2].Text = "Done";
                        this.DialogResult = DialogResult.OK;
                        //...
                        break;
                }
                
            }
            this.Close();
        }
        private void downloadProgressChanged(object sender, System.Net.DownloadProgressChangedEventArgs e)
        {   //update status
            this._progress.Value = e.ProgressPercentage;
            this._curStatus.Text = e.BytesReceived / 1024 + 
                "kB / " + 
                e.TotalBytesToReceive / 1024 + "kB";
            // calc time
            TimeSpan ts = DateTime.Now - dwnStart;
            // avoiding dividition through zero
            if (ts.Seconds > 0) 
            {
                long speed = (e.TotalBytesToReceive / ts.Seconds) / 1024;
                this._curSpeed.Text = speed.ToString() + "kB/s";
            }
            else
                this._curSpeed.Text = "0kB/s";
        }
    }
}