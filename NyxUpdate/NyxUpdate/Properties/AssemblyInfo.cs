﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NyxUpdate")]
[assembly: AssemblyDescription("NyxUpdate")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("NyxUpdate")]
[assembly: AssemblyProduct("NyxUpdate")]
[assembly: AssemblyCopyright("Copyright ©  2006 Dominik 'del' Schischma")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("ea6e9c7f-4af9-4112-acd1-670cea740d53")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.0.2.3")]
[assembly: AssemblyFileVersion("0.0.2.3")]
