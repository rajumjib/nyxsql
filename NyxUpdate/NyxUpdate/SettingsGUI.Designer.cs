namespace NyxUpdate
{
    partial class SettingsGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsGUI));
            this._gbProxy = new System.Windows.Forms.GroupBox();
            this._tb_proxyAddress = new System.Windows.Forms.TextBox();
            this._tbProxy_Pasw = new System.Windows.Forms.TextBox();
            this._tbProxy_User = new System.Windows.Forms.TextBox();
            this._mtbProxy_Port = new System.Windows.Forms.MaskedTextBox();
            this._lblProxy_Port = new System.Windows.Forms.Label();
            this._lblProxy_Pasw = new System.Windows.Forms.Label();
            this._lblProxy_User = new System.Windows.Forms.Label();
            this._lblProxy = new System.Windows.Forms.Label();
            this._ckbProxy = new System.Windows.Forms.CheckBox();
            this._ms = new System.Windows.Forms.MenuStrip();
            this._msSave = new System.Windows.Forms.ToolStripMenuItem();
            this._msClose = new System.Windows.Forms.ToolStripMenuItem();
            this._ckbSetup = new System.Windows.Forms.CheckBox();
            this._ckbZip = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._ckbSource = new System.Windows.Forms.CheckBox();
            this._gbProxy.SuspendLayout();
            this._ms.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _gbProxy
            // 
            this._gbProxy.Controls.Add(this._tb_proxyAddress);
            this._gbProxy.Controls.Add(this._tbProxy_Pasw);
            this._gbProxy.Controls.Add(this._tbProxy_User);
            this._gbProxy.Controls.Add(this._mtbProxy_Port);
            this._gbProxy.Controls.Add(this._lblProxy_Port);
            this._gbProxy.Controls.Add(this._lblProxy_Pasw);
            this._gbProxy.Controls.Add(this._lblProxy_User);
            this._gbProxy.Controls.Add(this._lblProxy);
            this._gbProxy.Controls.Add(this._ckbProxy);
            resources.ApplyResources(this._gbProxy, "_gbProxy");
            this._gbProxy.Name = "_gbProxy";
            this._gbProxy.TabStop = false;
            // 
            // _tb_proxyAddress
            // 
            resources.ApplyResources(this._tb_proxyAddress, "_tb_proxyAddress");
            this._tb_proxyAddress.Name = "_tb_proxyAddress";
            // 
            // _tbProxy_Pasw
            // 
            resources.ApplyResources(this._tbProxy_Pasw, "_tbProxy_Pasw");
            this._tbProxy_Pasw.Name = "_tbProxy_Pasw";
            // 
            // _tbProxy_User
            // 
            resources.ApplyResources(this._tbProxy_User, "_tbProxy_User");
            this._tbProxy_User.Name = "_tbProxy_User";
            // 
            // _mtbProxy_Port
            // 
            resources.ApplyResources(this._mtbProxy_Port, "_mtbProxy_Port");
            this._mtbProxy_Port.Name = "_mtbProxy_Port";
            // 
            // _lblProxy_Port
            // 
            resources.ApplyResources(this._lblProxy_Port, "_lblProxy_Port");
            this._lblProxy_Port.Name = "_lblProxy_Port";
            // 
            // _lblProxy_Pasw
            // 
            resources.ApplyResources(this._lblProxy_Pasw, "_lblProxy_Pasw");
            this._lblProxy_Pasw.Name = "_lblProxy_Pasw";
            // 
            // _lblProxy_User
            // 
            resources.ApplyResources(this._lblProxy_User, "_lblProxy_User");
            this._lblProxy_User.Name = "_lblProxy_User";
            // 
            // _lblProxy
            // 
            resources.ApplyResources(this._lblProxy, "_lblProxy");
            this._lblProxy.Name = "_lblProxy";
            // 
            // _ckbProxy
            // 
            resources.ApplyResources(this._ckbProxy, "_ckbProxy");
            this._ckbProxy.Name = "_ckbProxy";
            this._ckbProxy.UseVisualStyleBackColor = true;
            this._ckbProxy.CheckedChanged += new System.EventHandler(this._ckbProxy_CheckedChanged);
            // 
            // _ms
            // 
            this._ms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msSave,
            this._msClose});
            resources.ApplyResources(this._ms, "_ms");
            this._ms.Name = "_ms";
            // 
            // _msSave
            // 
            this._msSave.Image = global::NyxUpdate.Properties.Resources.accept;
            resources.ApplyResources(this._msSave, "_msSave");
            this._msSave.Name = "_msSave";
            this._msSave.Click += new System.EventHandler(this._msSave_Click);
            // 
            // _msClose
            // 
            this._msClose.Image = global::NyxUpdate.Properties.Resources.delete;
            resources.ApplyResources(this._msClose, "_msClose");
            this._msClose.Name = "_msClose";
            this._msClose.Click += new System.EventHandler(this._msClose_Click);
            // 
            // _ckbSetup
            // 
            resources.ApplyResources(this._ckbSetup, "_ckbSetup");
            this._ckbSetup.Name = "_ckbSetup";
            this._ckbSetup.UseVisualStyleBackColor = true;
            // 
            // _ckbZip
            // 
            resources.ApplyResources(this._ckbZip, "_ckbZip");
            this._ckbZip.Name = "_ckbZip";
            this._ckbZip.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._ckbSource);
            this.groupBox1.Controls.Add(this._ckbSetup);
            this.groupBox1.Controls.Add(this._ckbZip);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // _ckbSource
            // 
            resources.ApplyResources(this._ckbSource, "_ckbSource");
            this._ckbSource.Name = "_ckbSource";
            this._ckbSource.UseVisualStyleBackColor = true;
            // 
            // SettingsGUI
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this._ms);
            this.Controls.Add(this._gbProxy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "SettingsGUI";
            this.Load += new System.EventHandler(this.SettingsGUI_Load);
            this._gbProxy.ResumeLayout(false);
            this._gbProxy.PerformLayout();
            this._ms.ResumeLayout(false);
            this._ms.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox _gbProxy;
        private System.Windows.Forms.TextBox _tb_proxyAddress;
        private System.Windows.Forms.TextBox _tbProxy_Pasw;
        private System.Windows.Forms.TextBox _tbProxy_User;
        private System.Windows.Forms.MaskedTextBox _mtbProxy_Port;
        private System.Windows.Forms.Label _lblProxy_Port;
        private System.Windows.Forms.Label _lblProxy_Pasw;
        private System.Windows.Forms.Label _lblProxy_User;
        private System.Windows.Forms.Label _lblProxy;
        private System.Windows.Forms.CheckBox _ckbProxy;
        private System.Windows.Forms.MenuStrip _ms;
        private System.Windows.Forms.ToolStripMenuItem _msSave;
        private System.Windows.Forms.ToolStripMenuItem _msClose;
        private System.Windows.Forms.CheckBox _ckbSetup;
        private System.Windows.Forms.CheckBox _ckbZip;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox _ckbSource;
    }
}