/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;

namespace NyxUpdate
{
    /// <summary> connectionsettings interface </summary>
    public class ConnectionSettings
    {
        private bool getZip = false;
        private bool getSetup = true;
        private bool getSource = false;
        private bool proxyEnabled = false;
        private string proxyAddress; 
        private int proxyPort;
        private string proxyUser;
        private string proxyPasw;

        /// <remarks> get zipfile? </remarks>
        public bool GetZip { get { return getZip; } set { getZip = value; } }
        /// <remarks> get setup? </remarks>
        public bool GetSetup { get { return getSetup; } set { getSetup = value; } }
        /// <remarks> get source? </remarks>
        public bool GetSource { get { return getSource; } set { getSource = value; } }
        /// <remarks> user proxy? </remarks>
        public bool ProxyEnabled { get { return proxyEnabled; } set { proxyEnabled = value; } }
        /// <remarks> proxy address </remarks>
        public string ProxyAddress { get { return proxyAddress; } set { proxyAddress = value; } }
        /// <remarks> proxy port </remarks>
        public int ProxyPort { get { return proxyPort; } set { proxyPort = value; } }
        /// <remarks> proxy user </remarks>
        public string ProxyUser { get { return proxyUser; } set { proxyUser = value; } }
        /// <remarks> proxy password </remarks>
        public string ProxyPasw { get { return proxyPasw; } set { proxyPasw = value; } }
    }

    /// <summary> settingsclass </summary>
    public class Settings
    {
        private static string errMsg = "";
        /// <remarks> errormessage </remarks>
        public static string ErrMsg
        {
            get { return errMsg; }
            set { errMsg = value; }
        }

        /// <summary> deserialize updatesettings </summary>
        /// <param name="conSettings"></param>
        /// <returns></returns>
        public static bool DeSerialize(ref ConnectionSettings conSettings)
        {   // init vars
            System.IO.StreamReader sr = null;
            // check file
            if (!System.IO.File.Exists("NyxUpdate.xml"))
            {
                ConnectionSettings newSett = new ConnectionSettings();
                Settings.Serialize(newSett);
            }
            // init serializer, streamreader
            System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(typeof(ConnectionSettings));
            try
            {
                sr = System.IO.File.OpenText("NyxUpdate.xml");
                // readin and close reader
                conSettings = (ConnectionSettings)xs.Deserialize(sr);
                return true;
            }
            catch (System.Exception exc)
            {
                errMsg = exc.Message.ToString();
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        /// <summary> serialize updatesettings </summary>
        /// <param name="conSettings"></param>
        /// <returns></returns>
        public static bool Serialize(ConnectionSettings conSettings)
        {   // vars
            System.IO.StreamWriter sw = null;
            string file = "NyxUpdate.xml";
            // init serializer, streamwriter
            System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(conSettings.GetType());
            // do...
            try
            {
                sw = System.IO.File.CreateText(file);
                // now we serialize
                xs.Serialize(sw, conSettings);
                // flush
                sw.Flush();
                return true;
            }
            catch (Exception exc)
            {
                errMsg = exc.Message.ToString();
                return false;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }
    }
}
