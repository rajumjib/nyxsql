/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.IO;
using System.Diagnostics;
using System.Windows.Forms;

namespace NyxUpdate
{
    /// <summary> updatedialog interface </summary>
    public partial class UpdateDialog : Form
    {
        private string errMsg;
        private string zipFile = UpdateMain.Nyxpath + "\\update\\" + UpdateMain.NyxUpdFile;

        /// <summary> initialize </summary>
        public UpdateDialog()
        {
            InitializeComponent();
        }
        private void Update_Load(object sender, EventArgs e)
        {
            bool nyxrunning = true;
            while(nyxrunning)
            {
                nyxrunning = ChkNyxRunning();
                if (nyxrunning)
                {
                    DialogResult dr = MessageBox.Show("An instance of Nyx is still running,\nshut them down?",
                        "Update", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                    switch (dr)
                    {
                        case DialogResult.Yes:
                            CloseNyx();
                            break;
                        case DialogResult.Cancel:
                            Application.Exit();
                            break;
                    }
                }
            }
            // create backup
            this._rtbStatus.Text = "Creating Backup...";
            if (!CreateBackup())
                this._rtbStatus.Text += "Failed!\n" + errMsg;
            else
            {
                this._rtbStatus.Text += "Done.\nLaunchin Installer...";
                try
                {
                    System.Diagnostics.Process.Start("update\\" + UpdateMain.NyxUpdFile + ".exe");
                    Application.Exit();
                }
                catch (Exception exc)
                {
                    this._rtbStatus.Text += "\nError starting Update:\n" + exc.Message.ToString();
                }
            }
        }
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // installupdate
        private bool ChkNyxRunning()
        {
            Process[] processes = Process.GetProcessesByName("nyxsql");
            if (processes.Length > 0)
                return true;
            else
                return false;
        }
        private void CloseNyx()
        {
            try
            {
                Process[] processes = Process.GetProcessesByName("nyxsql");
                if (processes.Length > 0)
                {
                    foreach (Process proc in processes)
                    {
                        proc.CloseMainWindow();
                    }
                }
                System.Threading.Thread.Sleep(250);
            }
            catch (Exception exc)
            {
                UpdateMain.ErrorMsg("Error closing Nyx-Instances.\n" + exc.Message.ToString());
            }
        }
        private bool CreateBackup()
        {
            string bckDir = UpdateMain.Nyxpath + "\\update\\backup";
            // check backupdirectory
            if (Directory.Exists(bckDir))
            {
                try
                {
                    Directory.Delete(bckDir, true);
                }
                catch (Exception exc)
                {
                    errMsg =  exc.Message.ToString();
                    return false;
                }
            }
            // create backupdirectory
            try
            {
                Directory.CreateDirectory(bckDir);
            }
            catch (Exception exc)
            {
                UpdateMain.ErrorMsg("Error creating directory \"update\\backup\":\n" + exc.Message.ToString());
                return false;
            }
            try
            {
                // getting directoryinfo
                DirectoryInfo di = new DirectoryInfo(UpdateMain.Nyxpath);
                // parsing subdirectorys
                foreach (DirectoryInfo diSubDir in di.GetDirectories())
                {
                    Directory.CreateDirectory(bckDir + "\\" + diSubDir.Name);
                    DirectoryInfo diSub1 = new DirectoryInfo(UpdateMain.Nyxpath + "\\" + diSubDir.Name);

                    foreach (FileInfo file in diSub1.GetFiles())
                        file.CopyTo(bckDir + "\\" + diSubDir.Name + "\\" + file.Name);
                }
                foreach (FileInfo file in di.GetFiles())
                    file.CopyTo(bckDir + "\\" + file.Name);
                // everything successfull
                return true;
            }
            catch (Exception exc)
            {
                UpdateMain.ErrorMsg("Error creating Backup:\n" + exc.Message.ToString());
                return false;
            }
        }
    }
}