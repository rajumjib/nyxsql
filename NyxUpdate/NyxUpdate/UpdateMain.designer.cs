namespace NyxUpdate
{
    /// <summary>
    /// 
    /// </summary>
    partial class UpdateMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UpdateMain));
            this._ms = new System.Windows.Forms.MenuStrip();
            this._msCheckVer = new System.Windows.Forms.ToolStripMenuItem();
            this._msChangelog = new System.Windows.Forms.ToolStripMenuItem();
            this._msDownload = new System.Windows.Forms.ToolStripMenuItem();
            this._msSettings = new System.Windows.Forms.ToolStripMenuItem();
            this._msExit = new System.Windows.Forms.ToolStripMenuItem();
            this._lblVersion = new System.Windows.Forms.Label();
            this._lblResult = new System.Windows.Forms.Label();
            this._ss = new System.Windows.Forms.StatusStrip();
            this._ssPg = new System.Windows.Forms.ToolStripProgressBar();
            this._sslblVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this._ssVersion = new System.Windows.Forms.ToolStripStatusLabel();
            this._rtbChangelog = new System.Windows.Forms.RichTextBox();
            this._tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this._picbResult = new System.Windows.Forms.PictureBox();
            this._picbVersion = new System.Windows.Forms.PictureBox();
            this._ms.SuspendLayout();
            this._ss.SuspendLayout();
            this._tableLayoutPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._picbResult)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._picbVersion)).BeginInit();
            this.SuspendLayout();
            // 
            // _ms
            // 
            this._ms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msCheckVer,
            this._msChangelog,
            this._msDownload,
            this._msSettings,
            this._msExit});
            this._ms.Location = new System.Drawing.Point(0, 0);
            this._ms.Name = "_ms";
            this._ms.Size = new System.Drawing.Size(432, 24);
            this._ms.TabIndex = 15;
            this._ms.Text = "menuStrip1";
            // 
            // _msCheckVer
            // 
            this._msCheckVer.Image = global::NyxUpdate.Properties.Resources.information;
            this._msCheckVer.ImageTransparentColor = System.Drawing.Color.Transparent;
            this._msCheckVer.Name = "_msCheckVer";
            this._msCheckVer.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F12)));
            this._msCheckVer.Size = new System.Drawing.Size(102, 20);
            this._msCheckVer.Text = "Check Version";
            this._msCheckVer.Click += new System.EventHandler(this._msCheckVer_Click);
            // 
            // _msChangelog
            // 
            this._msChangelog.Image = global::NyxUpdate.Properties.Resources.information;
            this._msChangelog.ImageTransparentColor = System.Drawing.Color.Transparent;
            this._msChangelog.Name = "_msChangelog";
            this._msChangelog.Size = new System.Drawing.Size(106, 20);
            this._msChangelog.Text = "Get Changelog";
            this._msChangelog.Click += new System.EventHandler(this._msChangelog_Click);
            // 
            // _msDownload
            // 
            this._msDownload.Image = global::NyxUpdate.Properties.Resources.disk;
            this._msDownload.ImageTransparentColor = System.Drawing.Color.Transparent;
            this._msDownload.Name = "_msDownload";
            this._msDownload.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.U)));
            this._msDownload.Size = new System.Drawing.Size(82, 20);
            this._msDownload.Text = "Download";
            this._msDownload.Click += new System.EventHandler(this._msDownload_Click);
            // 
            // _msSettings
            // 
            this._msSettings.Image = global::NyxUpdate.Properties.Resources.overlays;
            this._msSettings.Name = "_msSettings";
            this._msSettings.Size = new System.Drawing.Size(74, 20);
            this._msSettings.Text = "Settings";
            this._msSettings.Click += new System.EventHandler(this._msSettings_Click);
            // 
            // _msExit
            // 
            this._msExit.Image = global::NyxUpdate.Properties.Resources.delete;
            this._msExit.ImageTransparentColor = System.Drawing.Color.Transparent;
            this._msExit.Name = "_msExit";
            this._msExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Shift | System.Windows.Forms.Keys.F12)));
            this._msExit.Size = new System.Drawing.Size(53, 20);
            this._msExit.Text = "Exit";
            this._msExit.Click += new System.EventHandler(this._msExit_Click);
            // 
            // _lblVersion
            // 
            this._lblVersion.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lblVersion.AutoSize = true;
            this._lblVersion.Location = new System.Drawing.Point(154, 5);
            this._lblVersion.Name = "_lblVersion";
            this._lblVersion.Size = new System.Drawing.Size(102, 13);
            this._lblVersion.TabIndex = 17;
            this._lblVersion.Text = "Retrieving Version...";
            // 
            // _lblResult
            // 
            this._lblResult.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this._lblResult.AutoSize = true;
            this._lblResult.Location = new System.Drawing.Point(154, 29);
            this._lblResult.Name = "_lblResult";
            this._lblResult.Size = new System.Drawing.Size(67, 13);
            this._lblResult.TabIndex = 18;
            this._lblResult.Tag = "";
            this._lblResult.Text = "New Version";
            this._lblResult.Visible = false;
            // 
            // _ss
            // 
            this._ss.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ssPg,
            this._sslblVersion,
            this._ssVersion});
            this._ss.Location = new System.Drawing.Point(0, 253);
            this._ss.Name = "_ss";
            this._ss.Size = new System.Drawing.Size(432, 22);
            this._ss.TabIndex = 23;
            this._ss.Text = "_sstrip";
            // 
            // _ssPg
            // 
            this._ssPg.MarqueeAnimationSpeed = 0;
            this._ssPg.Name = "_ssPg";
            this._ssPg.Size = new System.Drawing.Size(100, 16);
            this._ssPg.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // _sslblVersion
            // 
            this._sslblVersion.Name = "_sslblVersion";
            this._sslblVersion.Size = new System.Drawing.Size(133, 17);
            this._sslblVersion.Text = "Current/Available Version:";
            // 
            // _ssVersion
            // 
            this._ssVersion.Name = "_ssVersion";
            this._ssVersion.Size = new System.Drawing.Size(42, 17);
            this._ssVersion.Text = "version";
            // 
            // _rtbChangelog
            // 
            this._rtbChangelog.BackColor = System.Drawing.SystemColors.Window;
            this._tableLayoutPanel.SetColumnSpan(this._rtbChangelog, 2);
            this._rtbChangelog.Cursor = System.Windows.Forms.Cursors.Default;
            this._rtbChangelog.Dock = System.Windows.Forms.DockStyle.Fill;
            this._rtbChangelog.Location = new System.Drawing.Point(3, 51);
            this._rtbChangelog.Name = "_rtbChangelog";
            this._rtbChangelog.ReadOnly = true;
            this._rtbChangelog.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.ForcedVertical;
            this._rtbChangelog.Size = new System.Drawing.Size(426, 175);
            this._rtbChangelog.TabIndex = 24;
            this._rtbChangelog.TabStop = false;
            this._rtbChangelog.Text = "";
            // 
            // _tableLayoutPanel
            // 
            this._tableLayoutPanel.ColumnCount = 2;
            this._tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this._tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this._tableLayoutPanel.Controls.Add(this._lblVersion, 1, 0);
            this._tableLayoutPanel.Controls.Add(this._rtbChangelog, 0, 2);
            this._tableLayoutPanel.Controls.Add(this._lblResult, 1, 1);
            this._tableLayoutPanel.Controls.Add(this._picbResult, 0, 1);
            this._tableLayoutPanel.Controls.Add(this._picbVersion, 0, 0);
            this._tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this._tableLayoutPanel.Location = new System.Drawing.Point(0, 24);
            this._tableLayoutPanel.Name = "_tableLayoutPanel";
            this._tableLayoutPanel.RowCount = 3;
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this._tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this._tableLayoutPanel.Size = new System.Drawing.Size(432, 229);
            this._tableLayoutPanel.TabIndex = 25;
            // 
            // _picbResult
            // 
            this._picbResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._picbResult.Image = global::NyxUpdate.Properties.Resources.accept;
            this._picbResult.Location = new System.Drawing.Point(129, 27);
            this._picbResult.Name = "_picbResult";
            this._picbResult.Size = new System.Drawing.Size(19, 16);
            this._picbResult.TabIndex = 21;
            this._picbResult.TabStop = false;
            this._picbResult.Visible = false;
            // 
            // _picbVersion
            // 
            this._picbVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this._picbVersion.Image = global::NyxUpdate.Properties.Resources.accept;
            this._picbVersion.Location = new System.Drawing.Point(129, 3);
            this._picbVersion.Name = "_picbVersion";
            this._picbVersion.Size = new System.Drawing.Size(19, 16);
            this._picbVersion.TabIndex = 19;
            this._picbVersion.TabStop = false;
            this._picbVersion.Visible = false;
            // 
            // UpdateMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 275);
            this.Controls.Add(this._tableLayoutPanel);
            this.Controls.Add(this._ss);
            this.Controls.Add(this._ms);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "UpdateMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NyxSql - Updater";
            this.Load += new System.EventHandler(this.CheckUpdate_Load);
            this._ms.ResumeLayout(false);
            this._ms.PerformLayout();
            this._ss.ResumeLayout(false);
            this._ss.PerformLayout();
            this._tableLayoutPanel.ResumeLayout(false);
            this._tableLayoutPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._picbResult)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._picbVersion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _ms;
        private System.Windows.Forms.ToolStripMenuItem _msDownload;
        private System.Windows.Forms.ToolStripMenuItem _msExit;
        private System.Windows.Forms.Label _lblVersion;
        private System.Windows.Forms.Label _lblResult;
        private System.Windows.Forms.PictureBox _picbVersion;
        private System.Windows.Forms.PictureBox _picbResult;
        private System.Windows.Forms.StatusStrip _ss;
        private System.Windows.Forms.ToolStripStatusLabel _sslblVersion;
        private System.Windows.Forms.ToolStripStatusLabel _ssVersion;
        private System.Windows.Forms.ToolStripMenuItem _msChangelog;
        private System.Windows.Forms.RichTextBox _rtbChangelog;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutPanel;
        private System.Windows.Forms.ToolStripMenuItem _msSettings;
        private System.Windows.Forms.ToolStripProgressBar _ssPg;
        private System.Windows.Forms.ToolStripMenuItem _msCheckVer;
    }
}