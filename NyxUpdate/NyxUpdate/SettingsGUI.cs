/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace NyxUpdate
{
    /// <summary> Settings userinterface </summary>
    public partial class SettingsGUI : Form
    {   
        /// <summary> initalize </summary>
        public SettingsGUI()
        {
            InitializeComponent();
        }
        private void SettingsGUI_Load(object sender, EventArgs e)
        {   // downloadoptions
            if (UpdateMain.Sett.GetSetup)
                this._ckbSetup.Checked = true;
            if (UpdateMain.Sett.GetZip)
                this._ckbZip.Checked = true;
            if (UpdateMain.Sett.GetSource)
                this._ckbSource.Checked = true;
            // proxy
            if (UpdateMain.Sett.ProxyEnabled) 
                this._ckbProxy.Checked = true;
            else this._ckbProxy.Checked = false;
            if (UpdateMain.Sett.ProxyAddress != null) 
                this._tb_proxyAddress.Text = UpdateMain.Sett.ProxyAddress;
            if (UpdateMain.Sett.ProxyPort != 0) 
                this._mtbProxy_Port.Text = UpdateMain.Sett.ProxyPort.ToString();
            if (UpdateMain.Sett.ProxyUser != null) 
                this._tbProxy_User.Text = UpdateMain.Sett.ProxyUser;
            if (UpdateMain.Sett.ProxyPasw != null)
            {
                
                NyxCrypt nc = new NyxCrypt();
                string tmpcrypt = null;
                if (nc.DecryptString(UpdateMain.Sett.ProxyPasw, ref tmpcrypt))
                    this._tbProxy_Pasw.Text = tmpcrypt;
                else
                {
                    this._tbProxy_Pasw.Text = "";
                    // err
                    UpdateMain.ErrorMsg("Error decrypting Password:\n" + nc.ErrExc.Message.ToString());
                }
            }
        }
        
        private void _ckbProxy_CheckedChanged(object sender, EventArgs e)
        {
            if (this._ckbProxy.Checked)
            {
                this._tb_proxyAddress.Enabled = true;
                this._mtbProxy_Port.Enabled = true;
                this._tbProxy_User.Enabled = true;
                this._tbProxy_Pasw.Enabled = true;
            }
            else
            {
                this._tb_proxyAddress.Enabled = false;
                this._mtbProxy_Port.Enabled = false;
                this._tbProxy_User.Enabled = false;
                this._tbProxy_Pasw.Enabled = false;
            }
        }
        private void _msSave_Click(object sender, EventArgs e)
        {
            int itmp;
            // downloadoptions
            if (this._ckbSetup.Checked)
                UpdateMain.Sett.GetSetup = true;
            else UpdateMain.Sett.GetSetup = false;
            if (this._ckbZip.Checked)
                UpdateMain.Sett.GetZip = true;
            else UpdateMain.Sett.GetZip = false;
            if (this._ckbSource.Checked)
                UpdateMain.Sett.GetSource = true;
            else UpdateMain.Sett.GetSource = false;
            // proxy
            if (this._ckbProxy.Checked)
                UpdateMain.Sett.ProxyEnabled = true;
            else
                UpdateMain.Sett.ProxyEnabled = false;
            if (this._tb_proxyAddress.Text != string.Empty)
                UpdateMain.Sett.ProxyAddress = this._tb_proxyAddress.Text;
            if (this._mtbProxy_Port.Text != string.Empty
                && Int32.TryParse(this._mtbProxy_Port.ToString(), out itmp))
                UpdateMain.Sett.ProxyPort = itmp;
            if (this._tbProxy_User.Text != string.Empty)
                UpdateMain.Sett.ProxyUser = this._tbProxy_User.Text;
            if (this._tbProxy_Pasw.Text != string.Empty)
            {
                NyxCrypt nc = new NyxCrypt();
                UpdateMain.Sett.ProxyPasw = nc.EncryptString(this._tbProxy_Pasw.Text);
            }
            // save settings
            if (!Settings.Serialize(UpdateMain.Sett))
                UpdateMain.ErrorMsg("Error saving Settings:\n" + Settings.ErrMsg);
            this.Close();
        }
        private void _msClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}