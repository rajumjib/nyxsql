/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Drawing;
using NyxSettings;
using Nyx.Classes;
using Nyx.AppDialogs;
using System.Windows.Forms;
using System.Collections.Generic;
using System.ComponentModel;

namespace Nyx
{
    public partial class UserSettingsManager : Form
    {   // vars
        private bool settingChanged, restartRequired;
        // init classes for propertygrids
        private FontColorProperty fcp = new FontColorProperty();
        private AdvancedProperty ap = new AdvancedProperty();
        private GeneralProperty gp = new GeneralProperty();
        private WindowProperty wp = new WindowProperty();
        private HotKeys hk;

        /// <summary>
        /// Event which is fired when a restart is required and the user agreed
        /// </summary>
        public event EventHandler<ControlAppEventArgs> ControlAppEvent;
        /// <summary>
        /// Event which is fired after saving changed settings, for re-setting them in NyxMain
        /// </summary>
        public event EventHandler<SettingsChangedEventArgs> SettingChangedEvent;

        /// <summary>
        /// Init 
        /// </summary>
        public UserSettingsManager()
        {
            InitializeComponent();
            // get hotkeys
            hk = new HotKeys();
            if (!Settings.DeserializeHotkeys(ref hk, Settings.GetFolder(SettingsType.Hotkeys)))
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrHotkeysLoad", new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
            // propertygrids
            this._pGAdvanced.SelectedObject = ap;
            this._pGDGrid.SelectedObject = fcp;
            this._pGGeneral.SelectedObject = gp;
            this._pGWindows.SelectedObject = wp;
            this._pGHotkeys.SelectedObject = hk;
        }
        /// <summary>
        /// OnClosing override for getting sure to save changed settings
        /// </summary>
        /// <param name="e">CancelEventArgs</param>
        protected override void OnClosing(CancelEventArgs e)
        {
            if (settingChanged && NyxMain.UASett.WindowSaveSettings)
            {   // show settings changed required
                StructRetDialog ret = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("SettingsChanged"), 
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, true);
                // set NyxMain.UASett?, then save it 
                if (ret.DRes == NyxDialogResult.Yes)
                {
                    if (!NyxMain.UASett.WindowSaveSettings.Equals(ret.ShowAgain))
                        NyxMain.UASett.WindowSaveSettings = ret.ShowAgain;
                    SaveOptions();
                }
                // user doesn't want to save, so we just save if we shall show the window again or not
                else if (!NyxMain.UASett.WindowSaveSettings.Equals(ret.ShowAgain))
                {
                    NyxMain.UASett.WindowSaveSettings = ret.ShowAgain;
                    if (!Settings.SerializeUserSettings(NyxMain.UASett, Settings.GetFolder(SettingsType.AppSettings)))
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("UserSettingsSaveErr",
                            new string[] { Settings.ErrExc.Message.ToString() }),
                            Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
            }
            // user doesn't want to see the dialog, so we save
            else if (settingChanged && !NyxMain.UASett.WindowSaveSettings)
            {
                SaveOptions();
            }

            if (restartRequired)
            {   // show restart required, if the user doesn't want to see the window we won't restart
                if (NyxMain.UASett.WindowRestartNyx)
                {   // restart?
                    StructRetDialog ret = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("RestartRequired"),
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, true);
                    if (!NyxMain.UASett.WindowExit.Equals(ret.ShowAgain))
                    {
                        NyxMain.UASett.WindowExit = ret.ShowAgain;
                        if (!Settings.SerializeUserSettings(NyxMain.UASett, Settings.GetFolder(SettingsType.AppSettings)))
                            MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("UserSettingsSaveErr", new string[] { Settings.ErrExc.Message.ToString() }),
                                Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
                    }
                    if (ret.DRes == NyxDialogResult.Yes)
                    {
                        ControlAppEventArgs cae = new ControlAppEventArgs();
                        cae.RestartApp = true;
                        OnControlApp(cae);
                    }
                    // no restart, so we try to set all possible through firering an event
                    else
                    {
                        SettingsChangedEventArgs sae = new SettingsChangedEventArgs();
                        sae.SettingsType = SettingsType.AppSettings;
                        OnSettingChanged(sae);
                    }
                }
                // no restart, so we try to set all possible through firering an event
                else
                {
                    SettingsChangedEventArgs sae = new SettingsChangedEventArgs();
                    sae.SettingsType = SettingsType.AppSettings;
                    OnSettingChanged(sae);
                }
            }
        }
        private void SettingsDialog_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // creating temp uasett for loading
            UserAppSettings tmpUASett = NyxMain.UASett;
            // general propertygrid
            gp.DefaultProfile = tmpUASett.DefaultProfile;
            gp.Language = tmpUASett.Language;
            gp.HistorySaveQueries = tmpUASett.HistorySaveQueries;
            gp.HistorySaveQueryLimit = tmpUASett.HistorySaveQueryLimit;
            gp.SqlDefaultQryLimit = tmpUASett.SqlDefaultQryLimit;
            gp.ShowSystrayIcon = tmpUASett.ShowSystrayIcon;
            gp.FadeWindows = tmpUASett.FadeWindows;
            gp.FadeWindowDelay = tmpUASett.FadeWindowDelay;
            // advanced propertygrid
            ap.AutoPrefetchDbs = tmpUASett.AutoPrefetchDbs;
            ap.EnableAutoCompletition = tmpUASett.AutoCompletition;
            ap.AutoCompletitionUseWords = tmpUASett.AutoCompletitionUseWords;
            ap.AutoCompletitionUseProcedures = tmpUASett.AutoCompletitionUseProcedures;
            ap.AutoCompletitionUseTables = tmpUASett.AutoCompletitionUseTables;
            ap.AutoCompletitionUseViews = tmpUASett.AutoCompletitionUseViews;
            ap.SqlBuilderPrefetchTables = tmpUASett.SqlBuilderPrefetchTables;
            ap.SqlBuilderPrefetchProcedures = tmpUASett.SqlBuilderPrefetchProcedures;
            ap.SqlBuilderPrefetchViews = tmpUASett.SqlBuilderPrefetchViews;
            // fonts
            Font ft;
            if (GuiHelper.GetFont(tmpUASett.SqlEditorFont, out ft))
                fcp.SqlEditField = ft;
            if (GuiHelper.GetFont(tmpUASett.HistoryFont, out ft))
                fcp.SqlHistory = ft;
            if (GuiHelper.GetFont(tmpUASett.LogFont, out ft))
                fcp.SqlLogging = ft;
            if (GuiHelper.GetFont(tmpUASett.DataGridFont, out ft))
                fcp.DataGrid = ft;
            // colors for the sql result datagridview
            if (tmpUASett.DataGridForeColor != 0)
            {   // check color
                Color color = Color.FromArgb(tmpUASett.DataGridForeColor);
                if (!color.IsEmpty)
                    fcp.ForeColor = color;
            }
            if (tmpUASett.DataGridBackColor != 0)
            {   // check color
                Color color = Color.FromArgb(tmpUASett.DataGridBackColor);
                if (!color.IsEmpty)
                    fcp.BackColor = color;
            }
            if (tmpUASett.DataGridAltForeColor != 0)
            {   // check color
                Color color = Color.FromArgb(tmpUASett.DataGridAltForeColor);
                if (!color.IsEmpty)
                    fcp.AlternateForeColor = color;
            }
            if (tmpUASett.DataGridAltBackColor != 0)
            {   // check color
                Color color = Color.FromArgb(tmpUASett.DataGridAltBackColor);
                if (!color.IsEmpty)
                    fcp.AlternateBackColor = color;
            }
            if (tmpUASett.DataGridSelBackColor != 0)
            {   // check color
                Color color = Color.FromArgb(tmpUASett.DataGridSelBackColor);
                if (!color.IsEmpty)
                    fcp.SelectedBackColor = color;
            }
            if (tmpUASett.DataGridSelForeColor != 0)
            {   // check color
                Color color = Color.FromArgb(tmpUASett.DataGridSelForeColor);
                if (!color.IsEmpty)
                    fcp.SelectedForeColor = color;
            }
            /* auto-update settings
             */
            // check for update at startup? and in what delay
            gp.AutoCheckUpdate = tmpUASett.AutoCheckUpdate;
            gp.AutoCheckDelay = tmpUASett.AutoCheckDelay;

            // the settings scope save settings 
            ap.SaveScopeHistory = tmpUASett.SaveScopeHistory;
            ap.SaveScopeProfiles = tmpUASett.SaveScopeProfiles;
            ap.SaveScopeRuntime = tmpUASett.SaveScopeRuntime;
            ap.SaveScopeBookmarks = tmpUASett.SaveScopeBookmarks;
            ap.SaveScopeHotKeys = tmpUASett.SaveScopeHotKeys;
            // database advanced
            ap.InitLongFetchSize = tmpUASett.InitLongFetchSize;
            ap.ConnectionTimeout = tmpUASett.ConnectionTimeout;
            // show again tabpage
            wp.SaveSettings = tmpUASett.WindowSaveSettings;
            wp.RestartNyx = tmpUASett.WindowRestartNyx;
            wp.ExecuteQuery = tmpUASett.WindowExecuteQuery;
            wp.Exit = tmpUASett.WindowExit;
            wp.AlternateDataUpdate = tmpUASett.WindowAlternateDataUpdate;
            wp.DBContentChanged = tmpUASett.WindowDBContentChanged;
            settingChanged = false;
            restartRequired = false;
        }
        
        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        /// <summary>
        /// Save the options set from the appsettings
        /// </summary>
        public void SaveOptions()
        {
            /* general options
             */
            UserAppSettings tmpUASett = NyxMain.UASett;
            // language
            tmpUASett.Language = gp.Language;
            // default profile
            if (gp.DefaultProfile.ToString() != "none")
                tmpUASett.DefaultProfile = gp.DefaultProfile;
            else
                tmpUASett.DefaultProfile = String.Empty;
            // history 
            tmpUASett.HistorySaveQueries = gp.HistorySaveQueries;
            tmpUASett.HistorySaveQueryLimit = gp.HistorySaveQueryLimit;
            // sql query limit
            tmpUASett.SqlDefaultQryLimit = gp.SqlDefaultQryLimit;
            // systray icon
            tmpUASett.ShowSystrayIcon = gp.ShowSystrayIcon;
            // fade windows?
            tmpUASett.FadeWindows = gp.FadeWindows;
            tmpUASett.FadeWindowDelay = gp.FadeWindowDelay;
            // auto-retrieve dbs (mysql)
            tmpUASett.AutoPrefetchDbs = ap.AutoPrefetchDbs;
            // autocompletition
            tmpUASett.AutoCompletition = ap.EnableAutoCompletition;
            tmpUASett.AutoCompletitionUseProcedures = ap.AutoCompletitionUseProcedures;
            tmpUASett.AutoCompletitionUseTables = ap.AutoCompletitionUseTables;
            tmpUASett.AutoCompletitionUseViews = ap.AutoCompletitionUseViews;
            tmpUASett.AutoCompletitionUseWords = ap.AutoCompletitionUseWords;
            // sqlbuilder
            tmpUASett.SqlBuilderPrefetchProcedures = ap.SqlBuilderPrefetchProcedures;
            tmpUASett.SqlBuilderPrefetchTables = ap.SqlBuilderPrefetchTables;
            tmpUASett.SqlBuilderPrefetchViews = ap.SqlBuilderPrefetchViews;
            // fonts
            tmpUASett.SqlEditorFont = String.Format(NyxMain.NyxCI, "{0};{1};{2}", fcp.SqlEditField.Name.ToString(),
                fcp.SqlEditField.Size.ToString(NyxMain.NyxCI), fcp.SqlEditField.Style.ToString());
            tmpUASett.HistoryFont = String.Format(NyxMain.NyxCI, "{0};{1};{2}", fcp.SqlHistory.Name.ToString(),
                fcp.SqlHistory.Size.ToString(NyxMain.NyxCI), fcp.SqlHistory.Style.ToString());
            tmpUASett.DataGridFont = String.Format(NyxMain.NyxCI, "{0};{1};{2}", fcp.DataGrid.Name.ToString(),
                fcp.DataGrid.Size.ToString(NyxMain.NyxCI), fcp.DataGrid.Style.ToString());
            tmpUASett.LogFont = String.Format(NyxMain.NyxCI, "{0};{1};{2}", fcp.SqlLogging.Name.ToString(),
                fcp.SqlLogging.Size.ToString(NyxMain.NyxCI), fcp.SqlLogging.Style.ToString());
            // colors
            tmpUASett.DataGridForeColor = fcp.ForeColor.ToArgb();
            tmpUASett.DataGridBackColor = fcp.BackColor.ToArgb();
            tmpUASett.DataGridAltForeColor = fcp.AlternateForeColor.ToArgb();
            tmpUASett.DataGridAltBackColor = fcp.AlternateBackColor.ToArgb();
            tmpUASett.DataGridSelForeColor = fcp.SelectedForeColor.ToArgb();
            tmpUASett.DataGridSelBackColor = fcp.SelectedBackColor.ToArgb();
            // auto check for update 
            tmpUASett.AutoCheckUpdate = gp.AutoCheckUpdate;
            tmpUASett.AutoCheckDelay = gp.AutoCheckDelay;
            // settingsscope
            tmpUASett.SaveScopeHistory = ap.SaveScopeHistory;
            tmpUASett.SaveScopeProfiles = ap.SaveScopeProfiles;
            tmpUASett.SaveScopeRuntime = ap.SaveScopeRuntime;
            tmpUASett.SaveScopeBookmarks = ap.SaveScopeBookmarks;
            tmpUASett.SaveScopeHotKeys = ap.SaveScopeHotKeys;
            // database advanced
            tmpUASett.InitLongFetchSize = ap.InitLongFetchSize;
            tmpUASett.ConnectionTimeout = ap.ConnectionTimeout;
            // show window options
            tmpUASett.WindowRestartNyx = wp.RestartNyx;
            tmpUASett.WindowSaveSettings = wp.SaveSettings;
            tmpUASett.WindowAlternateDataUpdate = wp.AlternateDataUpdate;
            tmpUASett.WindowDBContentChanged = wp.DBContentChanged;
            tmpUASett.WindowExecuteQuery = wp.ExecuteQuery;
            tmpUASett.WindowExit = wp.Exit;
            // save
            if (Settings.SerializeUserSettings(tmpUASett, Settings.GetFolder(SettingsType.AppSettings)))
            {
                NyxMain.UASett = tmpUASett;
                // invoke refresh
                SettingsChangedEventArgs scea = new SettingsChangedEventArgs();
                scea.SettingsType = SettingsType.AppSettings;
                OnSettingChanged(scea);
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("UserSettingsSaveErr", new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
                return;
            }

            if (Settings.SerializeHotkeys(hk, Settings.GetFolder(SettingsType.Hotkeys)))
            {
                // invoke refresh
                SettingsChangedEventArgs scea = new SettingsChangedEventArgs();
                scea.SettingsType = SettingsType.Hotkeys;
                OnSettingChanged(scea);
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrHotkeysSave", new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
                return;
            }
            // save done, therefor no save warning required
            settingChanged = false;
            // close
            this.Close();
        }

        /// <summary>
        /// Fire the controlapp event
        /// </summary>
        /// <param name="e"></param>
        protected void OnControlApp(ControlAppEventArgs e)
        {   // fire event
            if (ControlAppEvent != null)
                ControlAppEvent(this, e);
        }
        /// <summary>
        /// fire the settingschanged event
        /// </summary>
        /// <param name="e"></param>
        protected void OnSettingChanged(SettingsChangedEventArgs e)
        {   // fire event
            if (SettingChangedEvent != null)
                SettingChangedEvent(this, e);
        }

        // buttons and gui events
        private void _btExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _btOk_Click(object sender, EventArgs e)
        {
            SaveOptions();
        }
        private void _SettingChanged(object s, PropertyValueChangedEventArgs e)
        {
            settingChanged = true;
        }
        private void _SettingChangedRestart(object s, PropertyValueChangedEventArgs e)
        {
            restartRequired = true;
            settingChanged = true;
        }

        
    }

    /// <summary> window class for usage with propertygrid </summary>
    public class WindowProperty
    {
        // show windows again?
        private bool showExitDialog = true;
        private bool showExecQryDialog = true;
        private bool showDBContentChangedDialog = true;
        private bool showSettingsRestartDialog = true;
        private bool showSaveSettingsDialog = true;
        private bool showAltDGridUpdateDialog = true;

        /// <remarks> show exit window </remarks>
        [CategoryAttribute("Windows"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Show 'exit Nyx' question dialog? If not, Nyx will close without user interaction.")]
        public bool Exit { get { return showExitDialog; } set { showExitDialog = value; } }
        /// <remarks> show 'execute non select query' window </remarks>
        [CategoryAttribute("Windows"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Show 'execute non-selectect query' question dialog? If not, query will be submitted without user interaction.")]
        public bool ExecuteQuery { get { return showExecQryDialog; } set { showExecQryDialog = value; } }
        /// <remarks> show dbcontent change, refresh - question dialog? </remarks>
        [CategoryAttribute("Windows"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Show 'database content changed, reload' question dialog? If not, database content will get reloaded without user interaction.")]
        public bool DBContentChanged { get { return showDBContentChangedDialog; } set { showDBContentChangedDialog = value; } }
        /// <remarks> show 'use alternate datagrid update' - question dialog? </remarks>
        [CategoryAttribute("Windows"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Show 'use alternate datagrid update' question dialog? If not, the alternate datagrid update will start without user interaction.")]
        public bool AlternateDataUpdate { get { return showAltDGridUpdateDialog; } set { showAltDGridUpdateDialog = value; } }
        /// <remarks> show restart window </remarks>
        [CategoryAttribute("Windows"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Show 'restart Nyx' question dialog, when restart needed? If not, Nyx will restart without user interaction.")]
        public bool RestartNyx { get { return showSettingsRestartDialog; } set { showSettingsRestartDialog = value; } }
        /// <remarks> show settings save window </remarks>
        [CategoryAttribute("Windows"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Show 'save settings' question dialog? If not, settings will get saved without user interaction.")]
        public bool SaveSettings { get { return showSaveSettingsDialog; } set { showSaveSettingsDialog = value; } }
    }
    /// <summary> general class for usage with propertygrid </summary>
    [DefaultPropertyAttribute("Language")]
    public class GeneralProperty
    {
        private Language lang = Language.enUS;
        private string defaultProfile = "none";
        private int historyDepth = 50;
        private bool historySaveLimit;
        private int sqlQryLimit = 100;
        private bool systrayIcon;
        private bool fadeWindows;
        private int fadeDelay = 75;
        private bool autoCheckUpdate = true;
        private DefaultTimeSpan autoCheckDelay = DefaultTimeSpan.EveryWeek;

        /// <remarks> selected language </remarks>
        [CategoryAttribute("Language"), DefaultValueAttribute(typeof(Language), "enUS"),
        Description("Program language."), DisplayName("Language")]
        public Language Language { get { return lang; } set { lang = value; } }
        /// <remarks> default profile on load </remarks>
        [CategoryAttribute("General"), DefaultValueAttribute(typeof(string), "none"),
        Description("Profile to connect to at Startup."), TypeConverter(typeof(DefaultProfileConverter)), 
        DisplayName("Default profile")]
        public string DefaultProfile { get { return defaultProfile; } set { defaultProfile = value; } }
        /// <remarks> how much history entrys to save </remarks>
        [CategoryAttribute("History"), DefaultValueAttribute(typeof(int), "50"),
        Description("How many queries to save inside the history."), DisplayName("No. of queries to save")]
        public int HistorySaveQueries { get { return historyDepth; } set { historyDepth = value; } }
        /// <remarks> save the limit of select querys inside history </remarks>
        [CategoryAttribute("History"), DefaultValueAttribute(typeof(bool), "false"),
        Description("Save limit from query inside the history."), DisplayName("Save sql limit")]
        public bool HistorySaveQueryLimit { get { return historySaveLimit; } set { historySaveLimit = value; } }
        /// <remarks> default query limit </remarks>
        [CategoryAttribute("General"), DefaultValueAttribute(typeof(int), "100"),
        Description("Default limit to append to sql queries (only affects the query editor and changeable through combobox at runtime)."), 
        DisplayName("Default sql limit")]
        public int SqlDefaultQryLimit { get { return sqlQryLimit; } set { sqlQryLimit = value; } }
        /// <remarks> display/use system tray icone </remarks>
        [CategoryAttribute("UI"), DefaultValueAttribute(typeof(bool), "false"),
        Description("A Systemtray-Icon is used and Nyx will minimize instead of closing." +
                   " The minimize to Tray option is also available if this Option is not checked."), DisplayName("Icon in system tray")]
        public bool ShowSystrayIcon { get { return systrayIcon; } set { systrayIcon = value; } }
        /// <remarks> fade windows </remarks>
        [CategoryAttribute("UI"), DefaultValueAttribute(typeof(bool), "false"),
        Description("Fade windows in on load."), DisplayName("Fade windows")]
        public bool FadeWindows { get { return fadeWindows; } set { fadeWindows = value; } }
        /// <remarks> delay for fading windows </remarks>
        [CategoryAttribute("UI"), DefaultValueAttribute(typeof(int), "75"),
        Description("Delay to use for fading (in ms)."), DisplayName("Fade delay")]
        public int FadeWindowDelay { get { return fadeDelay; } set { fadeDelay = value; } }
        /// <remarks> automatically check for update </remarks>
        [CategoryAttribute("Update"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Automatically search for newer versions."), DisplayName("Auto check update")]
        public bool AutoCheckUpdate { get { return autoCheckUpdate; } set { autoCheckUpdate = value; } }
        /// <remarks> automatic check delay </remarks>
        [CategoryAttribute("Update"), DefaultValueAttribute(typeof(DefaultTimeSpan), "EveryWeek"),
        Description("Delay between update checks."), DisplayName("Check delay")]
        public DefaultTimeSpan AutoCheckDelay { get { return autoCheckDelay; } set { autoCheckDelay = value; } }
    }
    /// <summary> advancedproperty class for usage with propertygrid </summary>
    public class AdvancedProperty
    {   //
        private bool autoRetrieveDbs = true;
        private bool autoCompletition = true;
        private bool autoCompletUseWords = true;
        private bool autoCompletUseTables = true;
        private bool autoCompletUseViews = true;
        private bool autoCompletUseProcedures = true;
        private bool sqlBldrRetTables = true;
        private bool sqlBldrRetViews;
        private bool sqlBldrRetProcedures;
        // save scope, use user history?
        private SettingsLocation sSUserHistory = SettingsLocation.UsersApplicationFolder;
        private SettingsLocation sSProfiles = SettingsLocation.UsersApplicationFolder;
        private SettingsLocation sSRuntime = SettingsLocation.UsersApplicationFolder;
        private SettingsLocation sSBookmarks = SettingsLocation.UsersApplicationFolder;
        private SettingsLocation sSHotKeys = SettingsLocation.ApplicationFolder;
        // oracle, advanced
        private int initLongFetchSize = 200;
        // database general, advanced
        private int connectionTimeout = 15;

        /// <remarks> SaveScope history </remarks>
        [CategoryAttribute("SaveScope"), DefaultValueAttribute(typeof(SettingsLocation), "UsersApplicationFolder"),
        Description("Save the sql history to the selected scope either \\%homepath%\\ApplicationData\\Nyx or inside the program folder."),
        DisplayName("Save history to")]
        public SettingsLocation SaveScopeHistory { get { return sSUserHistory; } set { sSUserHistory = value; } }

        /// <remarks> SaveScope profiles </remarks>
        [CategoryAttribute("SaveScope"), DefaultValueAttribute(typeof(SettingsLocation), "UsersApplicationFolder"),
        Description("Save the profiles the selected scope either \\%homepath%\\ApplicationData\\Nyx or inside the program folder."),
        DisplayName("Save profiles to")]
        public SettingsLocation SaveScopeProfiles { get { return sSProfiles; } set { sSProfiles = value; } }

        /// <remarks> SaveScope runtime </remarks>
        [CategoryAttribute("SaveScope"), DefaultValueAttribute(typeof(SettingsLocation), "UsersApplicationFolder"),
        Description("Save the runtimesettings to the selected scope either \\%homepath%\\ApplicationData\\Nyx or inside the program folder."),
        DisplayName("Save runtime settings to")]
        public SettingsLocation SaveScopeRuntime { get { return sSRuntime; } set { sSRuntime = value; } }

        /// <remarks> SaveScope bookmarks </remarks>
        [CategoryAttribute("SaveScope"), DefaultValueAttribute(typeof(SettingsLocation), "UsersApplicationFolder"),
        Description("Save the bookmarks to the selected scope either \\%homepath%\\ApplicationData\\Nyx or inside the program folder."),
        DisplayName("Save bookmarks to")]
        public SettingsLocation SaveScopeBookmarks { get { return sSBookmarks; } set { sSBookmarks = value; } }

        /// <remarks> SaveScope hotkeys </remarks>
        [CategoryAttribute("SaveScope"), DefaultValueAttribute(typeof(SettingsLocation), "ApplicationFolder"),
        Description("Save the hotkeys to the selected scope either \\%homepath%\\ApplicationData\\Nyx or inside the program folder."),
        DisplayName("Save hotkeys to")]
        public SettingsLocation SaveScopeHotKeys { get { return sSHotKeys; } set { sSHotKeys = value; } }

        /// <remarks> oracle only setting for the initlongfetchsize (retrieving data from long fields) </remarks>
        [CategoryAttribute("Oracle only"), DefaultValueAttribute(typeof(int), "200"),
        Description("Defines the bytes to receive from long fields inside the database." +
                   " Oracle uses 200 for it's ERP products, so here we go."), DisplayName("Initial long fetch size")]
        public int InitLongFetchSize { get { return initLongFetchSize; } set { initLongFetchSize = value; } }

        /// <remarks> connection timeout for database connections </remarks>
        [CategoryAttribute("Database"), DefaultValueAttribute(typeof(int), "15"),
        Description("Connection timeout in seconds to use for database connections."), DisplayName("Connection timeout")]
        public int ConnectionTimeout { get { return connectionTimeout; } set { connectionTimeout = value; } }

        /// <remarks> automatically retrieve databases at connect, if dbms supported </remarks>
        [CategoryAttribute("Auto prefetch"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Auto prefetch databases at connect time (mysql/mssql only)."), DisplayName("Prefetch databases")]
        public bool AutoPrefetchDbs { get { return autoRetrieveDbs; } set { autoRetrieveDbs = value; } }
        /// <remarks> use autocompletition </remarks>
        [CategoryAttribute("Auto-completition"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Enable auto-completition."), DisplayName("Enable AutoCompletition")]
        public bool EnableAutoCompletition { get { return autoCompletition; } set { autoCompletition = value; } }
        /// <remarks> use reserved words for autocompletition </remarks>
        [CategoryAttribute("Auto-completition"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Include reserved words in auto-completition."), DisplayName("Use sqlwords in AC")]
        public bool AutoCompletitionUseWords { get { return autoCompletUseWords; } set { autoCompletUseWords = value; } }
        /// <remarks> use tables for autocompletition </remarks>
        [CategoryAttribute("Auto-completition"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Include tables in auto-completition."), DisplayName("Use tables in AC")]
        public bool AutoCompletitionUseTables { get { return autoCompletUseTables; } set { autoCompletUseTables = value; } }
        /// <remarks> use views for autocompletition </remarks>
        [CategoryAttribute("Auto-completition"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Include views in auto-completition."), DisplayName("Use views in AC")]
        public bool AutoCompletitionUseViews { get { return autoCompletUseViews; } set { autoCompletUseViews = value; } }
        /// <remarks> use procedures for autocompletition </remarks>
        [CategoryAttribute("Auto-completition"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Include procedures words in auto-completition."), DisplayName("Use procedures in AC")]
        public bool AutoCompletitionUseProcedures { get { return autoCompletUseProcedures; } set { autoCompletUseProcedures = value; } }
        /// <remarks> retrieve tables at connect time </remarks>
        [CategoryAttribute("Auto prefetch"), DefaultValueAttribute(typeof(bool), "true"),
        Description("Auto prefetch tables at connect time."), DisplayName("SqlBuilder prefetch tables")]
        public bool SqlBuilderPrefetchTables { get { return sqlBldrRetTables; } set { sqlBldrRetTables = value; } }
        /// <remarks> retrieve views at connect time </remarks>
        [CategoryAttribute("Auto prefetch"), DefaultValueAttribute(typeof(bool), "false"),
        Description("Auto prefetch views at connect time (mysql/mssql only)."), DisplayName("SqlBuilder prefetch views")]
        public bool SqlBuilderPrefetchViews { get { return sqlBldrRetViews; } set { sqlBldrRetViews = value; } }
        /// <remarks> retrieve procedures at connect time </remarks>
        [CategoryAttribute("Auto prefetch"), DefaultValueAttribute(typeof(bool), "false"),
        Description("Auto prefetch procedures at connect time (mysql/mssql only)."), DisplayName("SqlBuilder prefetch procedures")]
        public bool SqlBuilderPrefetchProcedures { get { return sqlBldrRetProcedures; } set { sqlBldrRetProcedures = value; } }
    }
    /// <summary> class for reflecting the settings, propertygrid </summary>
    [DefaultPropertyAttribute("Font")]
    public class FontColorProperty: IDisposable
    {   // private members, default init
        private Font sqlEditfield = new Font("Verdana", 8.25F);
        private Font sqlHistory = new Font("Verdana", 8.25F);
        private Font sqlLogging = new Font("Verdana", 8.25F);
        private Font dataGridFont = new Font("Verdana", 8.25F);
        private Color dataGridForeColor = Color.Black;
        private Color dataGridBackColor = Color.White;
        private Color dataGridAltForeColor = Color.Black;
        private Color dataGridAltBackColor = Color.GhostWhite;
        private Color dataGridSelForeColor = Color.AliceBlue;
        private Color dataGridSelBackColor = Color.Gray;

        /// <remarks> datagrid font </remarks>
        [CategoryAttribute("Font"), DefaultValueAttribute(typeof(Font), "Verdana, 8.25F"),
        Description("Datagrid font"), DisplayName("DataGrid font")]
        public Font DataGrid { get { return dataGridFont; } set { dataGridFont = value; } }

        /// <remarks> sql editfield font </remarks>
        [CategoryAttribute("Font"), DefaultValueAttribute(typeof(Font), "Verdana, 8.25F"),
        Description("Sql editfield font (Sql-Query)"), DisplayName("Sql editfield")]
        public Font SqlEditField { get { return sqlEditfield; } set { sqlEditfield = value; } }

        /// <remarks> datagrid font </remarks>
        [CategoryAttribute("Font"), DefaultValueAttribute(typeof(Font), "Verdana, 8.25F"),
        Description("Sql history combobox font"), DisplayName("Sql history box")]
        public Font SqlHistory { get { return sqlHistory; } set { sqlHistory = value; } }

        /// <remarks> sql logging </remarks>
        [CategoryAttribute("Font"), DefaultValueAttribute(typeof(Font), "Verdana, 8.25F"),
        Description("Sql logwindow font"), DisplayName("Sql logwindow")]
        public Font SqlLogging { get { return sqlLogging; } set { sqlLogging = value; } }

        /// <remarks> datagrid fore color </remarks>
        [CategoryAttribute("Colors"), DefaultValueAttribute(typeof(Color), "Black"),
        Description("Datagrid line fore color"), DisplayName("DataGrid forecolor")]
        public Color ForeColor { get { return dataGridForeColor; } set { dataGridForeColor = value; } }

        /// <remarks> datagrid back color </remarks>
        [CategoryAttribute("Colors"), DefaultValueAttribute(typeof(Color), "White"),
        Description("Datagrid line back color"), DisplayName("DataGrid backcolor")]
        public Color BackColor { get { return dataGridBackColor; } set { dataGridBackColor = value; } }

        /// <remarks> datagrid alternating fore color </remarks>
        [CategoryAttribute("Colors - alternate"), DefaultValueAttribute(typeof(Color), "Black"),
        Description("Datagrid alternating line fore color"), DisplayName("DataGrid alternating forecolor")]
        public Color AlternateForeColor { get { return dataGridAltForeColor; } set { dataGridAltForeColor = value; } }

        /// <remarks> datagrid alternating back color </remarks>
        [CategoryAttribute("Colors - alternate"), DefaultValueAttribute(typeof(Color), "GhostWhite"),
        Description("Datagrid alternating line back color"), DisplayName("DataGrid alternating backcolor")]
        public Color AlternateBackColor { get { return dataGridAltBackColor; } set { dataGridAltBackColor = value; } }

        /// <remarks> datagrid selected fore color </remarks>
        [CategoryAttribute("Colors - selected"), DefaultValueAttribute(typeof(Color), "AliceBlue"),
        Description("Datagrid selected line fore color"), DisplayName("DataGrid selected forecolor")]
        public Color SelectedForeColor { get { return dataGridSelForeColor; } set { dataGridSelForeColor = value; } }

        /// <remarks> datagrid selected back color </remarks>
        [CategoryAttribute("Colors - selected"), DefaultValueAttribute(typeof(Color), "Gray"),
        Description("Datagrid selected line back color"), DisplayName("DataGrid selected backcolor")]
        public Color SelectedBackColor { get { return dataGridSelBackColor; } set { dataGridSelBackColor = value; } }

        /// <summary> virtual dispose </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {   // dispose managed resources
                sqlEditfield.Dispose();
                sqlHistory.Dispose();
                sqlLogging.Dispose();
                dataGridFont.Dispose();
            }
            // free native resources
        }
        /// <summary> public dispose </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }

    /// <summary>
    /// DefaultProfileConverter for use with datagrid custom value override
    /// </summary>
    public class DefaultProfileConverter : StringConverter
    {
        /// <summary>
        /// return true, to declare that we support standard attributes which can get selected with a list
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override bool GetStandardValuesSupported(ITypeDescriptorContext context)
        {
            return true;
        }
        /// <summary>
        /// return standard values
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public override StandardValuesCollection GetStandardValues(ITypeDescriptorContext context)
        {
            string[] profiles = null;
            // check profiles
            if (NyxMain.HTProfiles != null)
            {   // init
                int i = 1;
                // init profiles array
                profiles = new string[NyxMain.HTProfiles.Count + 1];
                profiles[0] = "none";
                // parse profiles hashtable
                foreach (System.Collections.DictionaryEntry de in NyxMain.HTProfiles)
                {
                    profiles[i++] = de.Key.ToString();
                }
            }
            return new StandardValuesCollection(profiles);
        }
    }
}