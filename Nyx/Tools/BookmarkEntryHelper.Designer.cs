namespace Nyx
{
    /// <summary>
    /// GUI for adding a bookmark entry
    /// </summary>
    partial class BookmarkEntryHelper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookmarkEntryHelper));
            this._lblName = new System.Windows.Forms.Label();
            this._lblDesc = new System.Windows.Forms.Label();
            this._tbName = new System.Windows.Forms.TextBox();
            this._tbDesc = new System.Windows.Forms.TextBox();
            this._tbQry = new System.Windows.Forms.TextBox();
            this._lblQry = new System.Windows.Forms.Label();
            this._lblCats = new System.Windows.Forms.Label();
            this._cbCats = new System.Windows.Forms.ComboBox();
            this._tblLayout = new System.Windows.Forms.TableLayoutPanel();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._tblLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lblName
            // 
            resources.ApplyResources(this._lblName, "_lblName");
            this._lblName.Name = "_lblName";
            // 
            // _lblDesc
            // 
            resources.ApplyResources(this._lblDesc, "_lblDesc");
            this._lblDesc.Name = "_lblDesc";
            // 
            // _tbName
            // 
            resources.ApplyResources(this._tbName, "_tbName");
            this._tbName.BackColor = System.Drawing.Color.GhostWhite;
            this._tblLayout.SetColumnSpan(this._tbName, 2);
            this._tbName.Name = "_tbName";
            this._tbName.TextChanged += new System.EventHandler(this._CheckInput);
            // 
            // _tbDesc
            // 
            this._tbDesc.BackColor = System.Drawing.Color.GhostWhite;
            this._tblLayout.SetColumnSpan(this._tbDesc, 2);
            resources.ApplyResources(this._tbDesc, "_tbDesc");
            this._tbDesc.Name = "_tbDesc";
            // 
            // _tbQry
            // 
            this._tbQry.BackColor = System.Drawing.Color.GhostWhite;
            this._tblLayout.SetColumnSpan(this._tbQry, 2);
            resources.ApplyResources(this._tbQry, "_tbQry");
            this._tbQry.Name = "_tbQry";
            this._tbQry.TextChanged += new System.EventHandler(this._CheckInput);
            // 
            // _lblQry
            // 
            resources.ApplyResources(this._lblQry, "_lblQry");
            this._lblQry.Name = "_lblQry";
            // 
            // _lblCats
            // 
            resources.ApplyResources(this._lblCats, "_lblCats");
            this._lblCats.Name = "_lblCats";
            // 
            // _cbCats
            // 
            resources.ApplyResources(this._cbCats, "_cbCats");
            this._cbCats.BackColor = System.Drawing.Color.GhostWhite;
            this._tblLayout.SetColumnSpan(this._cbCats, 2);
            this._cbCats.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbCats.FormattingEnabled = true;
            this._cbCats.Name = "_cbCats";
            // 
            // _tblLayout
            // 
            resources.ApplyResources(this._tblLayout, "_tblLayout");
            this._tblLayout.Controls.Add(this._tbName, 1, 0);
            this._tblLayout.Controls.Add(this._lblQry, 0, 3);
            this._tblLayout.Controls.Add(this._lblCats, 0, 1);
            this._tblLayout.Controls.Add(this._cbCats, 1, 1);
            this._tblLayout.Controls.Add(this._tbQry, 1, 3);
            this._tblLayout.Controls.Add(this._lblDesc, 0, 2);
            this._tblLayout.Controls.Add(this._tbDesc, 1, 2);
            this._tblLayout.Controls.Add(this._lblName, 0, 0);
            this._tblLayout.Controls.Add(this._btCancel, 2, 4);
            this._tblLayout.Controls.Add(this._btOk, 1, 4);
            this._tblLayout.Name = "_tblLayout";
            // 
            // _btCancel
            // 
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            // 
            // _btOk
            // 
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            // 
            // BookmarkEntryHelper
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._tblLayout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "BookmarkEntryHelper";
            this.Load += new System.EventHandler(this.BookmarkEntryHelper_Load);
            this._tblLayout.ResumeLayout(false);
            this._tblLayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _lblName;
        private System.Windows.Forms.Label _lblDesc;
        private System.Windows.Forms.TextBox _tbName;
        private System.Windows.Forms.TextBox _tbDesc;
        private System.Windows.Forms.TextBox _tbQry;
        private System.Windows.Forms.Label _lblQry;
        private System.Windows.Forms.Label _lblCats;
        private System.Windows.Forms.ComboBox _cbCats;
        private System.Windows.Forms.TableLayoutPanel _tblLayout;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
    }
}