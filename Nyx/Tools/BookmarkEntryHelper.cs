/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx
{
    public partial class BookmarkEntryHelper : Form
    {
        private static StructBookmark strBM;
        /// <summary> result bookmark struct </summary>
        public static StructBookmark StrBM { get { return strBM; } set { strBM = value; } }
        
        /// <summary>
        /// Init
        /// </summary>
        public BookmarkEntryHelper()
        {
            InitializeComponent();
        }
        private void BookmarkEntryHelper_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }

        /// <summary> Public function for retrieving the new bookmark entry </summary>
        /// <returns>DialogResult</returns>
        public static DialogResult GetBookmarkEntry
        {
            get
            {
                // preset dialog
                BookmarkEntryHelper bmeh = new BookmarkEntryHelper();
                // prepare cats
                bmeh._cbCats.Items.Add("Select category...");
                bmeh._cbCats.SelectedIndex = 0;
                // check if we got cats or not
                foreach (System.Data.DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + true + "'"))
                    bmeh._cbCats.Items.Add(dr["name"].ToString());
                if (bmeh._cbCats.Items.Count == 0)
                    bmeh._cbCats.Enabled = false;
                // show dialog
                DialogResult dRes = bmeh.ShowDialog();
                if (dRes == DialogResult.OK)
                {   // set values
                    StructBookmark strBookmark = new StructBookmark();
                    strBookmark.Name = bmeh._tbName.Text;
                    strBookmark.Description = bmeh._tbDesc.Text;
                    strBookmark.Query = bmeh._tbQry.Text;
                    if (bmeh._cbCats.SelectedIndex != 0)
                        strBookmark.Category = bmeh._cbCats.Text;
                    else
                        strBookmark.Category = String.Empty;
                }
                // final return
                return dRes;
            }
        }
        /// <summary> alter bookmark entry </summary>
        /// <param name="qry"></param>
        /// <param name="strBookmark"></param>
        /// <returns></returns>
        public static DialogResult AlterBookmarkEntry(string qry, StructBookmark strBookmark)
        {   // preset dialog
            BookmarkEntryHelper bmeh = new BookmarkEntryHelper();
            // prepare cats
            bmeh._cbCats.Items.Add("Select category...");
            bmeh._cbCats.SelectedIndex = 0;
            foreach (System.Data.DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + true + "'"))
                bmeh._cbCats.Items.Add(dr["name"].ToString());
            // check if we got cats or not
            if (bmeh._cbCats.Items.Count > 1)
            {
                // allready below a category?
                if (strBookmark.Category != null)
                    bmeh._cbCats.SelectedItem = strBookmark.Category;
            }
            // no cats defined
            else
                bmeh._cbCats.Enabled = false;

            if (strBookmark.Name != null)
            {
                if (strBookmark.Name.Length > 0)
                    bmeh._tbName.Text = strBookmark.Name;

                if (strBookmark.Description.Length > 0)
                    bmeh._tbDesc.Text = strBookmark.Description;

                if (strBookmark.Query.Length > 0)
                    bmeh._tbQry.Text = strBookmark.Query;
                else
                    bmeh._tbQry.Text = qry;
            }
            // show dialog
            DialogResult dRes = bmeh.ShowDialog();
            if (dRes == DialogResult.OK)
            {   // set values
                strBookmark.Name = bmeh._tbName.Text;
                strBookmark.Description = bmeh._tbDesc.Text;
                strBookmark.Query = bmeh._tbQry.Text;
                if (bmeh._cbCats.SelectedIndex != 0)
                    strBookmark.Category = bmeh._cbCats.Text;
                else
                    strBookmark.Category = String.Empty;
            }
            // final return
            return dRes;
        }
        private void _CheckInput(object sender, EventArgs e)
        {
            if (this._tbName.Text.Length > 0 && this._tbQry.Text.Length > 0)
                this._btOk.Enabled = true;
            else
                this._btOk.Enabled = false;
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}