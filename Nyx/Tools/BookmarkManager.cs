/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Nyx.Classes;
using Nyx.AppDialogs;
using System.Data;
using System.Windows.Forms;
using NyxSettings;

namespace Nyx
{
    /// <summary> Bookmarkstruct </summary>
    public struct StructBookmark
    {   // private methods
        private string name;
        private string query;
        private string category;
        private string description;
        private string isCat;

        /// <remarks> bookmark name </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> bookmark query </remarks>
        public string Query { get { return query; } set { query = value; } }
        /// <remarks> bookmark category? </remarks>
        public string Category { get { return category; } set { category = value; } }
        /// <remarks> bookmark description </remarks>
        public string Description { get { return description; } set { description = value; } }
        /// <remarks> is a bookmark category? </remarks>
        public string IsCat { get { return isCat; } set { isCat = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(name.GetHashCode() + query.GetHashCode() + category.GetHashCode() 
                + description.GetHashCode() + isCat.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(StructBookmark struct1, StructBookmark struct2)
        {
            if (struct1.name == struct2.name
                && struct1.query == struct2.query
                && struct1.category == struct2.category
                && struct1.description == struct2.description
                && struct1.isCat == struct2.isCat)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(StructBookmark struct1, StructBookmark struct2) { return !(struct1 == struct2); }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is StructBookmark))
                return false;
            else
                return this == (StructBookmark)obj;
        }
    }
    /// <summary> Main Bookmarkmanagement GUI </summary>
    public partial class BookmarkManager : Form
    {   
        /// <summary> Notify changed bookmarks to NyxMain </summary>
        public event EventHandler<SettingsChangedEventArgs> SettingChangedEvent;
        private bool bookmarksChanged;

        /// <summary> Init </summary>
        public BookmarkManager()
        {
            InitializeComponent();
        }
        private void Bookmarks_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // parse datatable and add at first cetegories, then subcategories further at least bookmarks
            this._trview.BeginUpdate();
            if (NyxMain.DTBookmarks != null)
            {   // add cats (cats)
                foreach (DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + true + "' AND cat = ''"))
                {
                    TreeNode tn = GetCatTreeNode(dr["name"].ToString());
                    this._trview.Nodes.Add(tn);
                }
                // the subnodes
                foreach (DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + true + "' AND cat <> ''"))
                {   // search where to add
                    TreeNode[] tnSrch = this._trview.Nodes.Find(dr["cat"].ToString(), true);
                    if (tnSrch.Length == 1)
                    {
                        TreeNode tn = GetCatTreeNode(dr["name"].ToString());
                        tnSrch[0].Nodes.Add(tn);
                    }
                    else
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("BM_CategoryNotFound"),
                            MessageHandler.EnumMsgType.Warning);
                }
                // finally the entrys
                foreach (DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + false + "'"))
                {   // build treenode
                    TreeNode tn = GetEntryTreeNode(dr["name"].ToString(), dr["desc"].ToString(), dr["query"].ToString());
                    // below a category?
                    if (dr["cat"].ToString().Length > 0)
                    {
                        TreeNode[] tnSrch = this._trview.Nodes.Find(dr["cat"].ToString(), true);
                        if (tnSrch.Length == 1)
                            tnSrch[0].Nodes.Add(tn);
                        else
                            this._trview.Nodes.Add(tn);
                    }
                    else
                        this._trview.Nodes.Add(tn);
                }
            }
            this._trview.EndUpdate();
        }
        /// <summary>
        /// OnClosing override for getting sure to save changed bookmarks
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if (bookmarksChanged)
            {   // create eventargs
                SettingsChangedEventArgs sae = new SettingsChangedEventArgs();
                sae.SettingsType = SettingsType.Bookmarks;
                // start firering event
                OnSettingsChanged(sae);
            }
        }
        /// <summary>
        /// Fire event that bookmarks changed
        /// </summary>
        /// <param name="e">SettingsChangedEventArgs</param>
        protected void OnSettingsChanged(SettingsChangedEventArgs e)
        {   // firering event
            if (SettingChangedEvent != null)
                SettingChangedEvent(this, e);
        }

        // helpfunctions
        private static TreeNode GetCatTreeNode(string name)
        {
            TreeNode tn = new TreeNode();
            tn.ImageIndex = 1;
            tn.Name = name;
            tn.Text = name;
            tn.Tag = "cat";
            tn.ToolTipText = "Category: " + name;
            return tn;
        }
        private static DataRow GetCatDataRow(StructBookmark strBm)
        {
            DataRow dr = NyxMain.DTBookmarks.NewRow();
            dr["cat"] = strBm.Category;
            dr["name"] = strBm.Name;
            dr["iscat"] = "true";
            return dr;
        }
        private static TreeNode GetEntryTreeNode(string name, string desc, string query)
        {
            TreeNode tn = new TreeNode();
            tn.Name = name;
            tn.Text = name;
            tn.Tag = query;
            tn.ImageIndex = 0;
            tn.ToolTipText = desc;
            return tn;
        }
        private static DataRow GetEntryDataRow(StructBookmark strBm)
        {
            DataRow dr = NyxMain.DTBookmarks.NewRow();
            dr["name"] = strBm.Name;
            dr["cat"] = strBm.Category;
            dr["query"] = strBm.Query;
            dr["desc"] = strBm.Description;
            dr["iscat"] = "false";
            return dr;
        }

        // gui events
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _msNew_Cat_Click(object sender, EventArgs e)
        {
            AddCategory(true);
        }
        private void _msNew_Bm_Click(object sender, EventArgs e)
        {
            AddEntry(true);
        }
        private void _msEdit_Click(object sender, EventArgs e)
        {
            TreeNode selTn = this._trview.SelectedNode;
            // got selection?
            if (selTn == null)
                return;
            // subentrys/nodes?
            if (selTn.Nodes.Count > 0)
            {
                AppDialogs.MessageHandler.ShowDialog(
                    "Category has subcategorys.", AppDialogs.MessageHandler.EnumMsgType.Warning);
                return;
            }
            else
                EditEntry(selTn);
        }
        private void _msDelete_Click(object sender, EventArgs e)
        {   // check selected treenode
            TreeNode selTn = this._trview.SelectedNode;
            if (selTn == null)
                return;
            // try to find inside dataset
            DataRow[] dr = NyxMain.DTBookmarks.Select("name = '" + selTn.Name + "'");
            if (dr.Length != 1)
            {   // err...
                AppDialogs.MessageHandler.ShowDialog("Seems like a Bookmarksyncherror...\nClosing Manager.",
                    AppDialogs.MessageHandler.EnumMsgType.Error);
                this.Close();
            }
            DataRow drDel = dr[0];
            // build msg
            string msg = null;
            if (drDel["iscat"].ToString() == "true")
                msg = "Category " + drDel["name"].ToString() + "?";
            else
                msg = "Entry " + drDel["name"].ToString() + "?";
            // check if subcats exist
            DataRow[] drSC = NyxMain.DTBookmarks.Select("cat = '" + selTn.Name + "'");
            if (drSC.Length > 1)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("BM_BookmarkGotCats"), MessageHandler.EnumMsgType.Warning);
                return;
            }
            // ask
            StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("BM_DeleteBookmark", new string[] { msg }),
                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
            if (strRD.DRes == NyxDialogResult.Yes)
            {   // remove from treeview/datarow
                TreeNode[] srchTn = this._trview.Nodes.Find(selTn.Name, true);
                if(srchTn.Length == 1)
                    this._trview.Nodes.Remove(srchTn[0]);
                NyxMain.DTBookmarks.Rows.Remove(drDel);
                NyxMain.DTBookmarks.AcceptChanges();
            }

            if (Settings.SerializeBookmarks(NyxMain.DTBookmarks, Settings.GetFolder(SettingsType.Bookmarks)))
            {   // gui
                this._msEdit.Enabled = true;
                this._msDelete.Enabled = true;
                // bookmarks changed...
                bookmarksChanged = true;
            }
            else
            {
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("BM_SaveErr",
                    new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
        }
        private void _trview_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            TreeNode selTn = this._trview.SelectedNode;
            // got selection?
            if (selTn == null)
                return;
            // subentrys/nodes?
            if (selTn.Nodes.Count < 1)
                EditEntry(selTn);
        }
        private void _trview_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (this._trview.SelectedNode != null)
            {
                this._msDelete.Enabled = true;
                this._msEdit.Enabled = true;
            }
            else
            {
                this._msDelete.Enabled = true;
                this._msEdit.Enabled = true;
            }
        }

        /// <summary>
        /// Builds the Toolstripmenue of the bookmarks
        /// </summary>
        /// <param name="tsiBookmarks"></param>
        public static void SetBookmarks(System.Windows.Forms.ToolStripDropDownItem tsiBookmarks)
        {   // validate
            if (tsiBookmarks == null)
                throw new ArgumentNullException("tsiBookmarks", GuiHelper.GetResourceMsg("ArgumentNull"));

            if (NyxMain.DTBookmarks.Rows.Count > 0)
            {   // check if we have items to delete
                if (tsiBookmarks.DropDownItems.Count - 3 > 0)
                {   // helpvars
                    int i = 0;
                    ToolStripItem[] tiDel = new ToolStripItem[tsiBookmarks.DropDownItems.Count - 3];
                    // parse the toolstrip
                    foreach (ToolStripItem ti in tsiBookmarks.DropDownItems)
                    {
                        if (ti.Name.ToString() == "_msBookmarks_Mng"
                            || ti.Name.ToString() == "_msBookmarks_New"
                            || ti.Name.ToString() == "_msBookmarks_NewEntry"
                            || ti.Name.ToString() == "_msBookmarks_NewCat"
                            || ti.Name.ToString() == "_msBookmarks_Sep0")
                        {
                            continue;
                        }
                        else
                        {
                            tiDel[i] = ti;
                            i++;
                        }
                    }
                    // delete them
                    for (i = 0; i < tiDel.Length && tiDel[i] != null; i++)
                        tsiBookmarks.DropDownItems.Remove(tiDel[i]);
                }

                // add cats (cats)
                foreach (DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + true + "' AND cat = ''"))
                {
                    ToolStripMenuItem tsmiNew = new ToolStripMenuItem();
                    tsmiNew.Name = dr["name"].ToString();
                    tsmiNew.Text = dr["name"].ToString();
                    tsmiNew.Image = global::Nyx.Properties.Resources.script_code_red;
                    tsmiNew.Tag = "cat";
                    tsiBookmarks.DropDownItems.Add(tsmiNew);
                }
                // the subnodes
                foreach (DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + true + "' AND cat <> ''"))
                {   // search where to add
                    ToolStripItem[] tsmi = tsiBookmarks.DropDownItems.Find(dr["cat"].ToString(), true);
                    if (tsmi.Length == 1)
                    {
                        ToolStripMenuItem ti = (ToolStripMenuItem)tsmi[0];
                        ToolStripMenuItem tsmiNew = new ToolStripMenuItem();
                        tsmiNew.Name = dr["name"].ToString();
                        tsmiNew.Text = dr["name"].ToString();
                        tsmiNew.Image = global::Nyx.Properties.Resources.script_code_red;
                        tsmiNew.Tag = "cat";
                        ti.DropDownItems.Add(tsmiNew);
                    }
                }
                // finally the entrys
                foreach (DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + false + "'"))
                {   // if the entry is below a category
                    if (dr["cat"].ToString().Length > 0)
                    {
                        ToolStripItem[] tsmi = tsiBookmarks.DropDownItems.Find(dr["cat"].ToString(), true);
                        if (tsmi.Length == 1)
                        {   // create the new one
                            ToolStripMenuItem tsmiNew = new ToolStripMenuItem();
                            tsmiNew.Name = dr["name"].ToString();
                            tsmiNew.Text = dr["name"].ToString();
                            tsmiNew.Image = global::Nyx.Properties.Resources.script_code;
                            tsmiNew.Tag = "entry";
                            // finally ad to the found item
                            ToolStripMenuItem ti = (ToolStripMenuItem)tsmi[0];
                            ti.DropDownItems.Add(tsmiNew);
                        }
                    }
                    else
                    {
                        // create the new one
                        ToolStripMenuItem tsmiNew = new ToolStripMenuItem();
                        tsmiNew.Name = dr["name"].ToString();
                        tsmiNew.Text = dr["name"].ToString();
                        tsmiNew.Image = global::Nyx.Properties.Resources.script_code;
                        tsmiNew.Tag = "entry";
                        // finally ad to the found item
                        tsiBookmarks.DropDownItems.Add(tsmiNew);
                    }
                }
            }
        }
        /// <summary>
        /// Add a new entry to the bookmarks
        /// </summary>
        /// <param name="updateTree">true == add it also to the treeview</param>
        public void AddEntry(bool updateTree)
        {   // init
            StructBookmark strBm = new StructBookmark();
            bool showDialog = true;
            // start dialog
            while (showDialog)
            {
                DialogResult dRes = BookmarkEntryHelper.GetBookmarkEntry;
                if (dRes == DialogResult.OK)
                {   // check if we allready got a row with such name
                    if (NyxMain.DTBookmarks.Select("name = '" + strBm.Name + "'").Length > 0)
                    {
                        AppDialogs.MessageHandler.ShowDialog("Category/Bookmark allready exists.",
                            AppDialogs.MessageHandler.EnumMsgType.Warning);
                        continue;
                    }
                    // below a category?
                    if (strBm.Category.Length > 0)
                    {   // gui?
                        TreeNode[] tnSrch;
                        if (updateTree)
                        {   // init new
                            TreeNode tn = new TreeNode();
                            tn = GetEntryTreeNode(strBm.Name, strBm.Description, strBm.Query);
                            // search the catnode
                            tnSrch = this._trview.Nodes.Find(strBm.Category, true);
                            tnSrch[0].Nodes.Add(tn);
                        }
                        // update dataset
                        DataRow dr = GetEntryDataRow(strBm);
                        NyxMain.DTBookmarks.Rows.Add(dr);
                        // cancel while 
                        showDialog = false;
                    }
                    else
                    {   // gui?
                        if (updateTree)
                        {   // init new
                            TreeNode tn = new TreeNode();
                            tn = GetEntryTreeNode(strBm.Name, strBm.Description, strBm.Query);
                            // add to treeview
                            this._trview.Nodes.Add(tn);
                        }
                        DataRow dr = GetEntryDataRow(strBm);
                        NyxMain.DTBookmarks.Rows.Add(dr);
                        showDialog = false;
                    }
                    // save
                    if (!Settings.SerializeBookmarks(NyxMain.DTBookmarks, Settings.GetFolder(SettingsType.Bookmarks)))
                    {
                        AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("BM_SaveErr",
                            new string[] { Settings.ErrExc.Message.ToString() }),
                            Settings.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                    }
                    else if (updateTree)
                    {
                        this._msEdit.Enabled = true;
                        this._msDelete.Enabled = true;
                        // bookmarks changed...
                        bookmarksChanged = true;
                    }
                }
                else
                    showDialog = false;
            }
        }
        /// <summary>
        /// Add a new category to the bookmarks
        /// </summary>
        /// <param name="updateTree">true == add it also to the treeview</param>
        public void AddCategory(bool updateTree)
        {   // init
            bool showDialog = true;
            // start dialog
            while (showDialog)
            {
                DialogResult dRes = BookmarkCatHelper.GetCatEntry;
                if (dRes == DialogResult.OK)
                {   // set bookmark
                    StructBookmark strBM = BookmarkCatHelper.StrBM;
                    // check if we allready got a row with such name
                    if (NyxMain.DTBookmarks.Select("name = '" + strBM.Name + "'").Length > 0)
                    {
                        AppDialogs.MessageHandler.ShowDialog("Category/Bookmark allready exists.",
                            AppDialogs.MessageHandler.EnumMsgType.Warning);
                        continue;
                    }
                    // below a category?
                    if (strBM.Category.Length > 0)
                    {   // gui?
                        if (updateTree)
                        {   // prep treenode
                            TreeNode tn = new TreeNode();
                            tn = GetCatTreeNode(strBM.Name);
                            // search rootCat
                            TreeNode[] tnSrch = this._trview.Nodes.Find(strBM.Category, true);
                            tnSrch[0].Nodes.Add(tn);
                        }
                        // datatable
                        DataRow dr = GetCatDataRow(strBM);
                        NyxMain.DTBookmarks.Rows.Add(dr);
                        showDialog = false;
                    }
                    else
                    {   // gui?
                        if (updateTree)
                        {   // prep treenode
                            TreeNode tn = new TreeNode();
                            tn = GetCatTreeNode(strBM.Name);
                            // add
                            this._trview.Nodes.Add(tn);
                        }
                        // datatable
                        DataRow dr = GetCatDataRow(strBM);
                        NyxMain.DTBookmarks.Rows.Add(dr);
                        showDialog = false;
                    }
                    // save
                    if (!Settings.SerializeBookmarks(NyxMain.DTBookmarks, Settings.GetFolder(SettingsType.Bookmarks)))
                    {
                        AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("BM_SaveErr",
                            new string[] { Settings.ErrExc.Message.ToString() }),
                            Settings.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                    }
                    else if (updateTree)
                    {
                        this._msEdit.Enabled = true;
                        this._msDelete.Enabled = true;
                        // bookmarks changed...
                        bookmarksChanged = true;
                    }
                }
                else
                    showDialog = false;
            }
        }
        private void EditEntry(TreeNode selTn)
        {   // init
            bool showDialog = true;
            // try to find inside dataset
            DataRow[] dr = NyxMain.DTBookmarks.Select("name = '" + selTn.Name + "'");
            if (dr.Length != 1)
            {   // err...
                AppDialogs.MessageHandler.ShowDialog("Seems like a Bookmarksyncherror...\nClosing Manager.",
                    AppDialogs.MessageHandler.EnumMsgType.Error);
                this.Close();
            }
            // found
            DataRow drEdit = dr[0];
            // building struct
            StructBookmark strBm = new StructBookmark();
            strBm.Name = drEdit["name"].ToString();
            strBm.Description = drEdit["desc"].ToString();
            strBm.Category = drEdit["cat"].ToString();
            strBm.Query = drEdit["query"].ToString();
            strBm.IsCat = drEdit["iscat"].ToString();
            // entry
            if (drEdit["iscat"].ToString() == "false")
            {
                while (showDialog)
                {
                    DialogResult dRes = BookmarkEntryHelper.AlterBookmarkEntry("", strBm);
                    if (dRes == DialogResult.OK)
                    {   // update treeview
                        TreeNode[] tnSrch = this._trview.Nodes.Find(drEdit["name"].ToString(), true);
                        if (tnSrch.Length == 1)
                        {
                            this._trview.BeginUpdate();
                            // edit
                            tnSrch[0].Name = strBm.Name;
                            tnSrch[0].Text = strBm.Name;
                            tnSrch[0].Tag = strBm.Query;
                            tnSrch[0].ImageIndex = 0;
                            tnSrch[0].ToolTipText = strBm.Description;
                            this._trview.EndUpdate();
                        }
                        // update datarow
                        drEdit.BeginEdit();
                        drEdit["name"] = strBm.Name;
                        drEdit["desc"] = strBm.Description;
                        drEdit["cat"] = strBm.Category;
                        drEdit["query"] = strBm.Query;
                        drEdit["iscat"] = strBm.IsCat;
                        drEdit.EndEdit();
                        NyxMain.DTBookmarks.AcceptChanges();
                        // check if we can save
                        if (!Settings.SerializeBookmarks(NyxMain.DTBookmarks, Settings.GetFolder(SettingsType.Bookmarks)))
                        {
                            AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("BM_SaveErr",
                                new string[] { Settings.ErrExc.Message.ToString() }),
                                Settings.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                        }
                        // bookmarks changed...
                        else
                            bookmarksChanged = true;
                        // 
                        showDialog = false;
                    }
                    // cancel pressed
                    else
                        showDialog = false;
                }
            }
            // cat
            else
            {   // as long till the user pressed cancel or the checkups are successfull
                while (showDialog) 
                {   // show edit dialog
                    DialogResult dRes = BookmarkCatHelper.AlterCatEntry(strBm);
                    if (dRes == DialogResult.OK)
                    {   // set bookmark
                        strBm = BookmarkCatHelper.StrBM;
                        // update treeview
                        TreeNode[] tnSrch = this._trview.Nodes.Find(drEdit["name"].ToString(), true);
                        if (tnSrch.Length == 1)
                        {
                            this._trview.BeginUpdate();
                            // edit
                            tnSrch[0].Name = strBm.Name;
                            tnSrch[0].Text = strBm.Name;
                            tnSrch[0].Tag = "cat";
                            tnSrch[0].ImageIndex = 1;
                            tnSrch[0].ToolTipText = "Category: " + strBm.Name;
                            this._trview.EndUpdate();
                        }
                        // update datarow
                        drEdit.BeginEdit();
                        drEdit["name"] = strBm.Name;
                        drEdit["desc"] = strBm.Description;
                        drEdit["cat"] = strBm.Category;
                        drEdit["query"] = strBm.Query;
                        drEdit["iscat"] = strBm.IsCat;
                        drEdit.EndEdit();
                        NyxMain.DTBookmarks.AcceptChanges();
                        // check if we can save
                        if (!Settings.SerializeBookmarks(NyxMain.DTBookmarks, Settings.GetFolder(SettingsType.Bookmarks)))
                        {
                            AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("BM_SaveErr",
                                new string[] { Settings.ErrExc.Message.ToString() }),
                                Settings.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                        }
                        // 
                        showDialog = false;
                    }
                    // cancel pressed
                    else
                        showDialog = false;
                }
            }
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}


