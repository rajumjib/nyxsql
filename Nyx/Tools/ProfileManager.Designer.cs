namespace Nyx
{
    /// <summary>
    /// Profile manager GUI
    /// </summary>
    partial class ProfileManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProfileManager));
            this._lblAddStr = new System.Windows.Forms.Label();
            this._tbAddStr = new System.Windows.Forms.TextBox();
            this._tbDesc = new System.Windows.Forms.TextBox();
            this._lbl_description = new System.Windows.Forms.Label();
            this._tbName = new System.Windows.Forms.TextBox();
            this._lbl_profilename = new System.Windows.Forms.Label();
            this._cbProfiles = new System.Windows.Forms.ComboBox();
            this._ms = new System.Windows.Forms.MenuStrip();
            this._msNew = new System.Windows.Forms.ToolStripMenuItem();
            this._msCopyAs = new System.Windows.Forms.ToolStripMenuItem();
            this._msDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._msTest = new System.Windows.Forms.ToolStripMenuItem();
            this._cbMySql_Db = new System.Windows.Forms.ComboBox();
            this._lblMySql_Usr = new System.Windows.Forms.Label();
            this._lblMySql_Db = new System.Windows.Forms.Label();
            this._tbMySql_Usr = new System.Windows.Forms.TextBox();
            this._lblMySql_Host = new System.Windows.Forms.Label();
            this._lblMySql_Pasw = new System.Windows.Forms.Label();
            this._tbMySql_Host = new System.Windows.Forms.TextBox();
            this._tbMySql_Pasw = new System.Windows.Forms.TextBox();
            this._tbMSSql_Host = new System.Windows.Forms.TextBox();
            this._lblMSSql_Auth = new System.Windows.Forms.Label();
            this._cbMSSql_Auth = new System.Windows.Forms.ComboBox();
            this._lblMSSql_Usr = new System.Windows.Forms.Label();
            this._lblMSSql_Host = new System.Windows.Forms.Label();
            this._tbMSSql_Usr = new System.Windows.Forms.TextBox();
            this._lblMSSql_Pasw = new System.Windows.Forms.Label();
            this._tbMSSql_Pasw = new System.Windows.Forms.TextBox();
            this._cbOrcl_Priv = new System.Windows.Forms.ComboBox();
            this._lblOrcl_Usr = new System.Windows.Forms.Label();
            this._lblOrcl_Priv = new System.Windows.Forms.Label();
            this._tbOrcl_Usr = new System.Windows.Forms.TextBox();
            this._lblOrcl_SID = new System.Windows.Forms.Label();
            this._lblOrcl_Pasw = new System.Windows.Forms.Label();
            this._tbOrcl_SID = new System.Windows.Forms.TextBox();
            this._tbOrcl_Pasw = new System.Windows.Forms.TextBox();
            this._cbODBC_Driver = new System.Windows.Forms.ComboBox();
            this._lblODBC_Usr = new System.Windows.Forms.Label();
            this._lblODBC_Driver = new System.Windows.Forms.Label();
            this._tbODBC_Usr = new System.Windows.Forms.TextBox();
            this._lblODBC_Host = new System.Windows.Forms.Label();
            this._lblODBC_Pasw = new System.Windows.Forms.Label();
            this._tbODBC_Host = new System.Windows.Forms.TextBox();
            this._tbODBC_Pasw = new System.Windows.Forms.TextBox();
            this._sS = new System.Windows.Forms.StatusStrip();
            this._sS_lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this._pnlProfileDef = new System.Windows.Forms.Panel();
            this._tcDbType = new System.Windows.Forms.TabControl();
            this._tpMySql = new System.Windows.Forms.TabPage();
            this._tcMySql = new System.Windows.Forms.TabControl();
            this.tpMySql_Default = new System.Windows.Forms.TabPage();
            this.tpMySql_Advanced = new System.Windows.Forms.TabPage();
            this._cbMySql_Encoding = new System.Windows.Forms.ComboBox();
            this._lblMySql_Encoding = new System.Windows.Forms.Label();
            this._tpOrcl = new System.Windows.Forms.TabPage();
            this.tcOracle = new System.Windows.Forms.TabControl();
            this.tpOracle_Default = new System.Windows.Forms.TabPage();
            this.tpOracle_Advanced = new System.Windows.Forms.TabPage();
            this._lblOracle_InitLongNote = new System.Windows.Forms.Label();
            this._lblOracle_InitLong = new System.Windows.Forms.Label();
            this._mtbOracle_InitLongFetchSize = new System.Windows.Forms.MaskedTextBox();
            this._cbOrcl_Encoding = new System.Windows.Forms.ComboBox();
            this._lblOrcl_Encoding = new System.Windows.Forms.Label();
            this._tpMSSql = new System.Windows.Forms.TabPage();
            this._cbMSSql_Db = new System.Windows.Forms.ComboBox();
            this._lblMSSql_Db = new System.Windows.Forms.Label();
            this._tpPGSql = new System.Windows.Forms.TabPage();
            this._tbPGSql_Db = new System.Windows.Forms.TextBox();
            this._tbPGSql_Usr = new System.Windows.Forms.TextBox();
            this._lblPGSql_Pasw = new System.Windows.Forms.Label();
            this._lblPGSql_Host = new System.Windows.Forms.Label();
            this._lblPGSql_Usr = new System.Windows.Forms.Label();
            this._tbPGSql_Host = new System.Windows.Forms.TextBox();
            this._lblPGSql_Db = new System.Windows.Forms.Label();
            this._tbPGSql_Pasw = new System.Windows.Forms.TextBox();
            this._tpODBC = new System.Windows.Forms.TabPage();
            this._lblODBC_AddStr = new System.Windows.Forms.Label();
            this._tbODBC_AddStr = new System.Windows.Forms.TextBox();
            this._lblProfile = new System.Windows.Forms.Label();
            this._btOk = new System.Windows.Forms.Button();
            this._btCancel = new System.Windows.Forms.Button();
            this._ms.SuspendLayout();
            this._sS.SuspendLayout();
            this._pnlProfileDef.SuspendLayout();
            this._tcDbType.SuspendLayout();
            this._tpMySql.SuspendLayout();
            this._tcMySql.SuspendLayout();
            this.tpMySql_Default.SuspendLayout();
            this.tpMySql_Advanced.SuspendLayout();
            this._tpOrcl.SuspendLayout();
            this.tcOracle.SuspendLayout();
            this.tpOracle_Default.SuspendLayout();
            this.tpOracle_Advanced.SuspendLayout();
            this._tpMSSql.SuspendLayout();
            this._tpPGSql.SuspendLayout();
            this._tpODBC.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lblAddStr
            // 
            resources.ApplyResources(this._lblAddStr, "_lblAddStr");
            this._lblAddStr.Name = "_lblAddStr";
            // 
            // _tbAddStr
            // 
            this._tbAddStr.AllowDrop = true;
            this._tbAddStr.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbAddStr, "_tbAddStr");
            this._tbAddStr.Name = "_tbAddStr";
            // 
            // _tbDesc
            // 
            this._tbDesc.AllowDrop = true;
            this._tbDesc.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbDesc, "_tbDesc");
            this._tbDesc.Name = "_tbDesc";
            // 
            // _lbl_description
            // 
            resources.ApplyResources(this._lbl_description, "_lbl_description");
            this._lbl_description.Name = "_lbl_description";
            // 
            // _tbName
            // 
            this._tbName.AllowDrop = true;
            this._tbName.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbName, "_tbName");
            this._tbName.Name = "_tbName";
            this._tbName.ReadOnly = true;
            this._tbName.TextChanged += new System.EventHandler(this._tbName_TextChanged);
            // 
            // _lbl_profilename
            // 
            resources.ApplyResources(this._lbl_profilename, "_lbl_profilename");
            this._lbl_profilename.Name = "_lbl_profilename";
            // 
            // _cbProfiles
            // 
            this._cbProfiles.BackColor = System.Drawing.Color.GhostWhite;
            this._cbProfiles.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbProfiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbProfiles.FormattingEnabled = true;
            resources.ApplyResources(this._cbProfiles, "_cbProfiles");
            this._cbProfiles.Name = "_cbProfiles";
            this._cbProfiles.SelectedIndexChanged += new System.EventHandler(this._cbProfiles_SelectedIndexChanged);
            // 
            // _ms
            // 
            this._ms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msNew,
            this._msCopyAs,
            this._msDelete,
            this._msTest});
            resources.ApplyResources(this._ms, "_ms");
            this._ms.Name = "_ms";
            // 
            // _msNew
            // 
            this._msNew.Image = global::Nyx.Properties.Resources.lightning_add;
            resources.ApplyResources(this._msNew, "_msNew");
            this._msNew.Name = "_msNew";
            this._msNew.Click += new System.EventHandler(this._msNew_Click);
            // 
            // _msCopyAs
            // 
            resources.ApplyResources(this._msCopyAs, "_msCopyAs");
            this._msCopyAs.Image = global::Nyx.Properties.Resources.lightning_add;
            this._msCopyAs.Name = "_msCopyAs";
            this._msCopyAs.Click += new System.EventHandler(this._msCopy_Click);
            // 
            // _msDelete
            // 
            resources.ApplyResources(this._msDelete, "_msDelete");
            this._msDelete.Image = global::Nyx.Properties.Resources.lightning_delete;
            this._msDelete.Name = "_msDelete";
            this._msDelete.Click += new System.EventHandler(this._msDelete_Click);
            // 
            // _msTest
            // 
            resources.ApplyResources(this._msTest, "_msTest");
            this._msTest.Image = global::Nyx.Properties.Resources.lightning_go;
            this._msTest.Name = "_msTest";
            this._msTest.Click += new System.EventHandler(this._msTest_Click);
            // 
            // _cbMySql_Db
            // 
            this._cbMySql_Db.BackColor = System.Drawing.Color.GhostWhite;
            this._cbMySql_Db.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._cbMySql_Db.FormattingEnabled = true;
            this._cbMySql_Db.Items.AddRange(new object[] {
            resources.GetString("_cbMySql_Db.Items")});
            resources.ApplyResources(this._cbMySql_Db, "_cbMySql_Db");
            this._cbMySql_Db.Name = "_cbMySql_Db";
            this._cbMySql_Db.SelectedIndexChanged += new System.EventHandler(this._cbDb_SelectedIndexChanged);
            // 
            // _lblMySql_Usr
            // 
            resources.ApplyResources(this._lblMySql_Usr, "_lblMySql_Usr");
            this._lblMySql_Usr.Name = "_lblMySql_Usr";
            // 
            // _lblMySql_Db
            // 
            resources.ApplyResources(this._lblMySql_Db, "_lblMySql_Db");
            this._lblMySql_Db.Name = "_lblMySql_Db";
            // 
            // _tbMySql_Usr
            // 
            this._tbMySql_Usr.AllowDrop = true;
            this._tbMySql_Usr.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbMySql_Usr, "_tbMySql_Usr");
            this._tbMySql_Usr.Name = "_tbMySql_Usr";
            // 
            // _lblMySql_Host
            // 
            resources.ApplyResources(this._lblMySql_Host, "_lblMySql_Host");
            this._lblMySql_Host.Name = "_lblMySql_Host";
            // 
            // _lblMySql_Pasw
            // 
            resources.ApplyResources(this._lblMySql_Pasw, "_lblMySql_Pasw");
            this._lblMySql_Pasw.Name = "_lblMySql_Pasw";
            // 
            // _tbMySql_Host
            // 
            this._tbMySql_Host.AllowDrop = true;
            this._tbMySql_Host.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbMySql_Host, "_tbMySql_Host");
            this._tbMySql_Host.Name = "_tbMySql_Host";
            // 
            // _tbMySql_Pasw
            // 
            this._tbMySql_Pasw.AllowDrop = true;
            this._tbMySql_Pasw.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbMySql_Pasw, "_tbMySql_Pasw");
            this._tbMySql_Pasw.Name = "_tbMySql_Pasw";
            // 
            // _tbMSSql_Host
            // 
            this._tbMSSql_Host.AllowDrop = true;
            this._tbMSSql_Host.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbMSSql_Host, "_tbMSSql_Host");
            this._tbMSSql_Host.Name = "_tbMSSql_Host";
            // 
            // _lblMSSql_Auth
            // 
            resources.ApplyResources(this._lblMSSql_Auth, "_lblMSSql_Auth");
            this._lblMSSql_Auth.Name = "_lblMSSql_Auth";
            // 
            // _cbMSSql_Auth
            // 
            this._cbMSSql_Auth.BackColor = System.Drawing.Color.GhostWhite;
            this._cbMSSql_Auth.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbMSSql_Auth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbMSSql_Auth.FormattingEnabled = true;
            this._cbMSSql_Auth.Items.AddRange(new object[] {
            resources.GetString("_cbMSSql_Auth.Items"),
            resources.GetString("_cbMSSql_Auth.Items1")});
            resources.ApplyResources(this._cbMSSql_Auth, "_cbMSSql_Auth");
            this._cbMSSql_Auth.Name = "_cbMSSql_Auth";
            this._cbMSSql_Auth.SelectedIndexChanged += new System.EventHandler(this._cbMSSql_Auth_SelectedIndexChanged);
            // 
            // _lblMSSql_Usr
            // 
            resources.ApplyResources(this._lblMSSql_Usr, "_lblMSSql_Usr");
            this._lblMSSql_Usr.Name = "_lblMSSql_Usr";
            // 
            // _lblMSSql_Host
            // 
            resources.ApplyResources(this._lblMSSql_Host, "_lblMSSql_Host");
            this._lblMSSql_Host.Name = "_lblMSSql_Host";
            // 
            // _tbMSSql_Usr
            // 
            this._tbMSSql_Usr.AllowDrop = true;
            this._tbMSSql_Usr.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbMSSql_Usr, "_tbMSSql_Usr");
            this._tbMSSql_Usr.Name = "_tbMSSql_Usr";
            // 
            // _lblMSSql_Pasw
            // 
            resources.ApplyResources(this._lblMSSql_Pasw, "_lblMSSql_Pasw");
            this._lblMSSql_Pasw.Name = "_lblMSSql_Pasw";
            // 
            // _tbMSSql_Pasw
            // 
            this._tbMSSql_Pasw.AllowDrop = true;
            this._tbMSSql_Pasw.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbMSSql_Pasw, "_tbMSSql_Pasw");
            this._tbMSSql_Pasw.Name = "_tbMSSql_Pasw";
            // 
            // _cbOrcl_Priv
            // 
            this._cbOrcl_Priv.BackColor = System.Drawing.Color.GhostWhite;
            this._cbOrcl_Priv.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbOrcl_Priv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbOrcl_Priv.FormattingEnabled = true;
            this._cbOrcl_Priv.Items.AddRange(new object[] {
            resources.GetString("_cbOrcl_Priv.Items"),
            resources.GetString("_cbOrcl_Priv.Items1")});
            resources.ApplyResources(this._cbOrcl_Priv, "_cbOrcl_Priv");
            this._cbOrcl_Priv.Name = "_cbOrcl_Priv";
            // 
            // _lblOrcl_Usr
            // 
            resources.ApplyResources(this._lblOrcl_Usr, "_lblOrcl_Usr");
            this._lblOrcl_Usr.Name = "_lblOrcl_Usr";
            // 
            // _lblOrcl_Priv
            // 
            resources.ApplyResources(this._lblOrcl_Priv, "_lblOrcl_Priv");
            this._lblOrcl_Priv.Name = "_lblOrcl_Priv";
            // 
            // _tbOrcl_Usr
            // 
            this._tbOrcl_Usr.AllowDrop = true;
            this._tbOrcl_Usr.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbOrcl_Usr, "_tbOrcl_Usr");
            this._tbOrcl_Usr.Name = "_tbOrcl_Usr";
            // 
            // _lblOrcl_SID
            // 
            resources.ApplyResources(this._lblOrcl_SID, "_lblOrcl_SID");
            this._lblOrcl_SID.Name = "_lblOrcl_SID";
            // 
            // _lblOrcl_Pasw
            // 
            resources.ApplyResources(this._lblOrcl_Pasw, "_lblOrcl_Pasw");
            this._lblOrcl_Pasw.Name = "_lblOrcl_Pasw";
            // 
            // _tbOrcl_SID
            // 
            this._tbOrcl_SID.AllowDrop = true;
            this._tbOrcl_SID.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbOrcl_SID, "_tbOrcl_SID");
            this._tbOrcl_SID.Name = "_tbOrcl_SID";
            // 
            // _tbOrcl_Pasw
            // 
            this._tbOrcl_Pasw.AllowDrop = true;
            this._tbOrcl_Pasw.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbOrcl_Pasw, "_tbOrcl_Pasw");
            this._tbOrcl_Pasw.Name = "_tbOrcl_Pasw";
            // 
            // _cbODBC_Driver
            // 
            this._cbODBC_Driver.BackColor = System.Drawing.Color.GhostWhite;
            this._cbODBC_Driver.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbODBC_Driver.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbODBC_Driver.FormattingEnabled = true;
            this._cbODBC_Driver.Items.AddRange(new object[] {
            resources.GetString("_cbODBC_Driver.Items"),
            resources.GetString("_cbODBC_Driver.Items1"),
            resources.GetString("_cbODBC_Driver.Items2"),
            resources.GetString("_cbODBC_Driver.Items3")});
            resources.ApplyResources(this._cbODBC_Driver, "_cbODBC_Driver");
            this._cbODBC_Driver.Name = "_cbODBC_Driver";
            this._cbODBC_Driver.SelectedIndexChanged += new System.EventHandler(this._cbODBC_Driver_SelectedIndexChanged);
            // 
            // _lblODBC_Usr
            // 
            resources.ApplyResources(this._lblODBC_Usr, "_lblODBC_Usr");
            this._lblODBC_Usr.Name = "_lblODBC_Usr";
            // 
            // _lblODBC_Driver
            // 
            resources.ApplyResources(this._lblODBC_Driver, "_lblODBC_Driver");
            this._lblODBC_Driver.Name = "_lblODBC_Driver";
            // 
            // _tbODBC_Usr
            // 
            this._tbODBC_Usr.AllowDrop = true;
            this._tbODBC_Usr.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbODBC_Usr, "_tbODBC_Usr");
            this._tbODBC_Usr.Name = "_tbODBC_Usr";
            // 
            // _lblODBC_Host
            // 
            resources.ApplyResources(this._lblODBC_Host, "_lblODBC_Host");
            this._lblODBC_Host.Name = "_lblODBC_Host";
            // 
            // _lblODBC_Pasw
            // 
            resources.ApplyResources(this._lblODBC_Pasw, "_lblODBC_Pasw");
            this._lblODBC_Pasw.Name = "_lblODBC_Pasw";
            // 
            // _tbODBC_Host
            // 
            this._tbODBC_Host.AllowDrop = true;
            this._tbODBC_Host.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbODBC_Host, "_tbODBC_Host");
            this._tbODBC_Host.Name = "_tbODBC_Host";
            // 
            // _tbODBC_Pasw
            // 
            this._tbODBC_Pasw.AllowDrop = true;
            this._tbODBC_Pasw.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbODBC_Pasw, "_tbODBC_Pasw");
            this._tbODBC_Pasw.Name = "_tbODBC_Pasw";
            // 
            // _sS
            // 
            this._sS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._sS_lblStatus});
            resources.ApplyResources(this._sS, "_sS");
            this._sS.Name = "_sS";
            this._sS.SizingGrip = false;
            // 
            // _sS_lblStatus
            // 
            this._sS_lblStatus.Name = "_sS_lblStatus";
            resources.ApplyResources(this._sS_lblStatus, "_sS_lblStatus");
            // 
            // _pnlProfileDef
            // 
            this._pnlProfileDef.Controls.Add(this._tcDbType);
            this._pnlProfileDef.Controls.Add(this._tbName);
            this._pnlProfileDef.Controls.Add(this._lbl_profilename);
            this._pnlProfileDef.Controls.Add(this._lbl_description);
            this._pnlProfileDef.Controls.Add(this._lblAddStr);
            this._pnlProfileDef.Controls.Add(this._tbDesc);
            this._pnlProfileDef.Controls.Add(this._tbAddStr);
            resources.ApplyResources(this._pnlProfileDef, "_pnlProfileDef");
            this._pnlProfileDef.Name = "_pnlProfileDef";
            // 
            // _tcDbType
            // 
            resources.ApplyResources(this._tcDbType, "_tcDbType");
            this._tcDbType.Controls.Add(this._tpMySql);
            this._tcDbType.Controls.Add(this._tpOrcl);
            this._tcDbType.Controls.Add(this._tpMSSql);
            this._tcDbType.Controls.Add(this._tpPGSql);
            this._tcDbType.Controls.Add(this._tpODBC);
            this._tcDbType.HotTrack = true;
            this._tcDbType.Name = "_tcDbType";
            this._tcDbType.SelectedIndex = 0;
            // 
            // _tpMySql
            // 
            this._tpMySql.Controls.Add(this._tcMySql);
            resources.ApplyResources(this._tpMySql, "_tpMySql");
            this._tpMySql.Name = "_tpMySql";
            this._tpMySql.UseVisualStyleBackColor = true;
            // 
            // _tcMySql
            // 
            resources.ApplyResources(this._tcMySql, "_tcMySql");
            this._tcMySql.Controls.Add(this.tpMySql_Default);
            this._tcMySql.Controls.Add(this.tpMySql_Advanced);
            this._tcMySql.Name = "_tcMySql";
            this._tcMySql.SelectedIndex = 0;
            // 
            // tpMySql_Default
            // 
            this.tpMySql_Default.Controls.Add(this._tbMySql_Usr);
            this.tpMySql_Default.Controls.Add(this._cbMySql_Db);
            this.tpMySql_Default.Controls.Add(this._tbMySql_Pasw);
            this.tpMySql_Default.Controls.Add(this._lblMySql_Db);
            this.tpMySql_Default.Controls.Add(this._lblMySql_Pasw);
            this.tpMySql_Default.Controls.Add(this._tbMySql_Host);
            this.tpMySql_Default.Controls.Add(this._lblMySql_Host);
            this.tpMySql_Default.Controls.Add(this._lblMySql_Usr);
            resources.ApplyResources(this.tpMySql_Default, "tpMySql_Default");
            this.tpMySql_Default.Name = "tpMySql_Default";
            this.tpMySql_Default.UseVisualStyleBackColor = true;
            // 
            // tpMySql_Advanced
            // 
            this.tpMySql_Advanced.Controls.Add(this._cbMySql_Encoding);
            this.tpMySql_Advanced.Controls.Add(this._lblMySql_Encoding);
            resources.ApplyResources(this.tpMySql_Advanced, "tpMySql_Advanced");
            this.tpMySql_Advanced.Name = "tpMySql_Advanced";
            this.tpMySql_Advanced.UseVisualStyleBackColor = true;
            // 
            // _cbMySql_Encoding
            // 
            this._cbMySql_Encoding.BackColor = System.Drawing.Color.GhostWhite;
            this._cbMySql_Encoding.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._cbMySql_Encoding.FormattingEnabled = true;
            this._cbMySql_Encoding.Items.AddRange(new object[] {
            resources.GetString("_cbMySql_Encoding.Items"),
            resources.GetString("_cbMySql_Encoding.Items1"),
            resources.GetString("_cbMySql_Encoding.Items2"),
            resources.GetString("_cbMySql_Encoding.Items3"),
            resources.GetString("_cbMySql_Encoding.Items4"),
            resources.GetString("_cbMySql_Encoding.Items5"),
            resources.GetString("_cbMySql_Encoding.Items6"),
            resources.GetString("_cbMySql_Encoding.Items7"),
            resources.GetString("_cbMySql_Encoding.Items8"),
            resources.GetString("_cbMySql_Encoding.Items9"),
            resources.GetString("_cbMySql_Encoding.Items10"),
            resources.GetString("_cbMySql_Encoding.Items11"),
            resources.GetString("_cbMySql_Encoding.Items12"),
            resources.GetString("_cbMySql_Encoding.Items13"),
            resources.GetString("_cbMySql_Encoding.Items14"),
            resources.GetString("_cbMySql_Encoding.Items15"),
            resources.GetString("_cbMySql_Encoding.Items16"),
            resources.GetString("_cbMySql_Encoding.Items17"),
            resources.GetString("_cbMySql_Encoding.Items18"),
            resources.GetString("_cbMySql_Encoding.Items19"),
            resources.GetString("_cbMySql_Encoding.Items20"),
            resources.GetString("_cbMySql_Encoding.Items21"),
            resources.GetString("_cbMySql_Encoding.Items22"),
            resources.GetString("_cbMySql_Encoding.Items23"),
            resources.GetString("_cbMySql_Encoding.Items24"),
            resources.GetString("_cbMySql_Encoding.Items25"),
            resources.GetString("_cbMySql_Encoding.Items26"),
            resources.GetString("_cbMySql_Encoding.Items27"),
            resources.GetString("_cbMySql_Encoding.Items28"),
            resources.GetString("_cbMySql_Encoding.Items29"),
            resources.GetString("_cbMySql_Encoding.Items30"),
            resources.GetString("_cbMySql_Encoding.Items31"),
            resources.GetString("_cbMySql_Encoding.Items32"),
            resources.GetString("_cbMySql_Encoding.Items33"),
            resources.GetString("_cbMySql_Encoding.Items34"),
            resources.GetString("_cbMySql_Encoding.Items35")});
            resources.ApplyResources(this._cbMySql_Encoding, "_cbMySql_Encoding");
            this._cbMySql_Encoding.Name = "_cbMySql_Encoding";
            // 
            // _lblMySql_Encoding
            // 
            resources.ApplyResources(this._lblMySql_Encoding, "_lblMySql_Encoding");
            this._lblMySql_Encoding.Name = "_lblMySql_Encoding";
            // 
            // _tpOrcl
            // 
            this._tpOrcl.Controls.Add(this.tcOracle);
            resources.ApplyResources(this._tpOrcl, "_tpOrcl");
            this._tpOrcl.Name = "_tpOrcl";
            this._tpOrcl.UseVisualStyleBackColor = true;
            // 
            // tcOracle
            // 
            resources.ApplyResources(this.tcOracle, "tcOracle");
            this.tcOracle.Controls.Add(this.tpOracle_Default);
            this.tcOracle.Controls.Add(this.tpOracle_Advanced);
            this.tcOracle.Name = "tcOracle";
            this.tcOracle.SelectedIndex = 0;
            // 
            // tpOracle_Default
            // 
            this.tpOracle_Default.Controls.Add(this._tbOrcl_Usr);
            this.tpOracle_Default.Controls.Add(this._lblOrcl_SID);
            this.tpOracle_Default.Controls.Add(this._lblOrcl_Pasw);
            this.tpOracle_Default.Controls.Add(this._cbOrcl_Priv);
            this.tpOracle_Default.Controls.Add(this._tbOrcl_SID);
            this.tpOracle_Default.Controls.Add(this._lblOrcl_Priv);
            this.tpOracle_Default.Controls.Add(this._lblOrcl_Usr);
            this.tpOracle_Default.Controls.Add(this._tbOrcl_Pasw);
            resources.ApplyResources(this.tpOracle_Default, "tpOracle_Default");
            this.tpOracle_Default.Name = "tpOracle_Default";
            this.tpOracle_Default.UseVisualStyleBackColor = true;
            // 
            // tpOracle_Advanced
            // 
            this.tpOracle_Advanced.Controls.Add(this._lblOracle_InitLongNote);
            this.tpOracle_Advanced.Controls.Add(this._lblOracle_InitLong);
            this.tpOracle_Advanced.Controls.Add(this._mtbOracle_InitLongFetchSize);
            this.tpOracle_Advanced.Controls.Add(this._cbOrcl_Encoding);
            this.tpOracle_Advanced.Controls.Add(this._lblOrcl_Encoding);
            resources.ApplyResources(this.tpOracle_Advanced, "tpOracle_Advanced");
            this.tpOracle_Advanced.Name = "tpOracle_Advanced";
            this.tpOracle_Advanced.UseVisualStyleBackColor = true;
            // 
            // _lblOracle_InitLongNote
            // 
            resources.ApplyResources(this._lblOracle_InitLongNote, "_lblOracle_InitLongNote");
            this._lblOracle_InitLongNote.Name = "_lblOracle_InitLongNote";
            // 
            // _lblOracle_InitLong
            // 
            resources.ApplyResources(this._lblOracle_InitLong, "_lblOracle_InitLong");
            this._lblOracle_InitLong.Name = "_lblOracle_InitLong";
            // 
            // _mtbOracle_InitLongFetchSize
            // 
            this._mtbOracle_InitLongFetchSize.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._mtbOracle_InitLongFetchSize, "_mtbOracle_InitLongFetchSize");
            this._mtbOracle_InitLongFetchSize.Name = "_mtbOracle_InitLongFetchSize";
            // 
            // _cbOrcl_Encoding
            // 
            this._cbOrcl_Encoding.BackColor = System.Drawing.Color.GhostWhite;
            this._cbOrcl_Encoding.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._cbOrcl_Encoding.FormattingEnabled = true;
            this._cbOrcl_Encoding.Items.AddRange(new object[] {
            resources.GetString("_cbOrcl_Encoding.Items"),
            resources.GetString("_cbOrcl_Encoding.Items1"),
            resources.GetString("_cbOrcl_Encoding.Items2"),
            resources.GetString("_cbOrcl_Encoding.Items3"),
            resources.GetString("_cbOrcl_Encoding.Items4"),
            resources.GetString("_cbOrcl_Encoding.Items5"),
            resources.GetString("_cbOrcl_Encoding.Items6"),
            resources.GetString("_cbOrcl_Encoding.Items7"),
            resources.GetString("_cbOrcl_Encoding.Items8"),
            resources.GetString("_cbOrcl_Encoding.Items9"),
            resources.GetString("_cbOrcl_Encoding.Items10"),
            resources.GetString("_cbOrcl_Encoding.Items11"),
            resources.GetString("_cbOrcl_Encoding.Items12"),
            resources.GetString("_cbOrcl_Encoding.Items13"),
            resources.GetString("_cbOrcl_Encoding.Items14"),
            resources.GetString("_cbOrcl_Encoding.Items15"),
            resources.GetString("_cbOrcl_Encoding.Items16"),
            resources.GetString("_cbOrcl_Encoding.Items17"),
            resources.GetString("_cbOrcl_Encoding.Items18"),
            resources.GetString("_cbOrcl_Encoding.Items19"),
            resources.GetString("_cbOrcl_Encoding.Items20"),
            resources.GetString("_cbOrcl_Encoding.Items21"),
            resources.GetString("_cbOrcl_Encoding.Items22"),
            resources.GetString("_cbOrcl_Encoding.Items23"),
            resources.GetString("_cbOrcl_Encoding.Items24"),
            resources.GetString("_cbOrcl_Encoding.Items25"),
            resources.GetString("_cbOrcl_Encoding.Items26"),
            resources.GetString("_cbOrcl_Encoding.Items27"),
            resources.GetString("_cbOrcl_Encoding.Items28"),
            resources.GetString("_cbOrcl_Encoding.Items29"),
            resources.GetString("_cbOrcl_Encoding.Items30"),
            resources.GetString("_cbOrcl_Encoding.Items31"),
            resources.GetString("_cbOrcl_Encoding.Items32"),
            resources.GetString("_cbOrcl_Encoding.Items33"),
            resources.GetString("_cbOrcl_Encoding.Items34"),
            resources.GetString("_cbOrcl_Encoding.Items35"),
            resources.GetString("_cbOrcl_Encoding.Items36"),
            resources.GetString("_cbOrcl_Encoding.Items37"),
            resources.GetString("_cbOrcl_Encoding.Items38"),
            resources.GetString("_cbOrcl_Encoding.Items39"),
            resources.GetString("_cbOrcl_Encoding.Items40")});
            resources.ApplyResources(this._cbOrcl_Encoding, "_cbOrcl_Encoding");
            this._cbOrcl_Encoding.Name = "_cbOrcl_Encoding";
            // 
            // _lblOrcl_Encoding
            // 
            resources.ApplyResources(this._lblOrcl_Encoding, "_lblOrcl_Encoding");
            this._lblOrcl_Encoding.Name = "_lblOrcl_Encoding";
            // 
            // _tpMSSql
            // 
            this._tpMSSql.Controls.Add(this._cbMSSql_Db);
            this._tpMSSql.Controls.Add(this._lblMSSql_Db);
            this._tpMSSql.Controls.Add(this._tbMSSql_Host);
            this._tpMSSql.Controls.Add(this._cbMSSql_Auth);
            this._tpMSSql.Controls.Add(this._lblMSSql_Auth);
            this._tpMSSql.Controls.Add(this._tbMSSql_Pasw);
            this._tpMSSql.Controls.Add(this._lblMSSql_Pasw);
            this._tpMSSql.Controls.Add(this._lblMSSql_Usr);
            this._tpMSSql.Controls.Add(this._tbMSSql_Usr);
            this._tpMSSql.Controls.Add(this._lblMSSql_Host);
            resources.ApplyResources(this._tpMSSql, "_tpMSSql");
            this._tpMSSql.Name = "_tpMSSql";
            this._tpMSSql.UseVisualStyleBackColor = true;
            // 
            // _cbMSSql_Db
            // 
            this._cbMSSql_Db.BackColor = System.Drawing.Color.GhostWhite;
            this._cbMSSql_Db.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._cbMSSql_Db.FormattingEnabled = true;
            this._cbMSSql_Db.Items.AddRange(new object[] {
            resources.GetString("_cbMSSql_Db.Items")});
            resources.ApplyResources(this._cbMSSql_Db, "_cbMSSql_Db");
            this._cbMSSql_Db.Name = "_cbMSSql_Db";
            this._cbMSSql_Db.SelectedIndexChanged += new System.EventHandler(this._cbDb_SelectedIndexChanged);
            // 
            // _lblMSSql_Db
            // 
            resources.ApplyResources(this._lblMSSql_Db, "_lblMSSql_Db");
            this._lblMSSql_Db.Name = "_lblMSSql_Db";
            // 
            // _tpPGSql
            // 
            this._tpPGSql.Controls.Add(this._tbPGSql_Db);
            this._tpPGSql.Controls.Add(this._tbPGSql_Usr);
            this._tpPGSql.Controls.Add(this._lblPGSql_Pasw);
            this._tpPGSql.Controls.Add(this._lblPGSql_Host);
            this._tpPGSql.Controls.Add(this._lblPGSql_Usr);
            this._tpPGSql.Controls.Add(this._tbPGSql_Host);
            this._tpPGSql.Controls.Add(this._lblPGSql_Db);
            this._tpPGSql.Controls.Add(this._tbPGSql_Pasw);
            resources.ApplyResources(this._tpPGSql, "_tpPGSql");
            this._tpPGSql.Name = "_tpPGSql";
            this._tpPGSql.UseVisualStyleBackColor = true;
            // 
            // _tbPGSql_Db
            // 
            this._tbPGSql_Db.AllowDrop = true;
            this._tbPGSql_Db.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbPGSql_Db, "_tbPGSql_Db");
            this._tbPGSql_Db.Name = "_tbPGSql_Db";
            // 
            // _tbPGSql_Usr
            // 
            this._tbPGSql_Usr.AllowDrop = true;
            this._tbPGSql_Usr.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbPGSql_Usr, "_tbPGSql_Usr");
            this._tbPGSql_Usr.Name = "_tbPGSql_Usr";
            // 
            // _lblPGSql_Pasw
            // 
            resources.ApplyResources(this._lblPGSql_Pasw, "_lblPGSql_Pasw");
            this._lblPGSql_Pasw.Name = "_lblPGSql_Pasw";
            // 
            // _lblPGSql_Host
            // 
            resources.ApplyResources(this._lblPGSql_Host, "_lblPGSql_Host");
            this._lblPGSql_Host.Name = "_lblPGSql_Host";
            // 
            // _lblPGSql_Usr
            // 
            resources.ApplyResources(this._lblPGSql_Usr, "_lblPGSql_Usr");
            this._lblPGSql_Usr.Name = "_lblPGSql_Usr";
            // 
            // _tbPGSql_Host
            // 
            this._tbPGSql_Host.AllowDrop = true;
            this._tbPGSql_Host.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbPGSql_Host, "_tbPGSql_Host");
            this._tbPGSql_Host.Name = "_tbPGSql_Host";
            // 
            // _lblPGSql_Db
            // 
            resources.ApplyResources(this._lblPGSql_Db, "_lblPGSql_Db");
            this._lblPGSql_Db.Name = "_lblPGSql_Db";
            // 
            // _tbPGSql_Pasw
            // 
            this._tbPGSql_Pasw.AllowDrop = true;
            this._tbPGSql_Pasw.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbPGSql_Pasw, "_tbPGSql_Pasw");
            this._tbPGSql_Pasw.Name = "_tbPGSql_Pasw";
            // 
            // _tpODBC
            // 
            this._tpODBC.Controls.Add(this._lblODBC_AddStr);
            this._tpODBC.Controls.Add(this._tbODBC_AddStr);
            this._tpODBC.Controls.Add(this._cbODBC_Driver);
            this._tpODBC.Controls.Add(this._tbODBC_Usr);
            this._tpODBC.Controls.Add(this._tbODBC_Pasw);
            this._tpODBC.Controls.Add(this._lblODBC_Host);
            this._tpODBC.Controls.Add(this._lblODBC_Pasw);
            this._tpODBC.Controls.Add(this._lblODBC_Usr);
            this._tpODBC.Controls.Add(this._lblODBC_Driver);
            this._tpODBC.Controls.Add(this._tbODBC_Host);
            resources.ApplyResources(this._tpODBC, "_tpODBC");
            this._tpODBC.Name = "_tpODBC";
            this._tpODBC.UseVisualStyleBackColor = true;
            // 
            // _lblODBC_AddStr
            // 
            resources.ApplyResources(this._lblODBC_AddStr, "_lblODBC_AddStr");
            this._lblODBC_AddStr.Name = "_lblODBC_AddStr";
            // 
            // _tbODBC_AddStr
            // 
            this._tbODBC_AddStr.AllowDrop = true;
            this._tbODBC_AddStr.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbODBC_AddStr, "_tbODBC_AddStr");
            this._tbODBC_AddStr.Name = "_tbODBC_AddStr";
            // 
            // _lblProfile
            // 
            resources.ApplyResources(this._lblProfile, "_lblProfile");
            this._lblProfile.Name = "_lblProfile";
            // 
            // _btOk
            // 
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btOk_Click);
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btClose_Click);
            // 
            // ProfileManager
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Controls.Add(this._lblProfile);
            this.Controls.Add(this._pnlProfileDef);
            this.Controls.Add(this._sS);
            this.Controls.Add(this._ms);
            this.Controls.Add(this._cbProfiles);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MainMenuStrip = this._ms;
            this.MaximizeBox = false;
            this.Name = "ProfileManager";
            this.Load += new System.EventHandler(this.FrmProfiles_Load);
            this._ms.ResumeLayout(false);
            this._ms.PerformLayout();
            this._sS.ResumeLayout(false);
            this._sS.PerformLayout();
            this._pnlProfileDef.ResumeLayout(false);
            this._pnlProfileDef.PerformLayout();
            this._tcDbType.ResumeLayout(false);
            this._tpMySql.ResumeLayout(false);
            this._tcMySql.ResumeLayout(false);
            this.tpMySql_Default.ResumeLayout(false);
            this.tpMySql_Default.PerformLayout();
            this.tpMySql_Advanced.ResumeLayout(false);
            this.tpMySql_Advanced.PerformLayout();
            this._tpOrcl.ResumeLayout(false);
            this.tcOracle.ResumeLayout(false);
            this.tpOracle_Default.ResumeLayout(false);
            this.tpOracle_Default.PerformLayout();
            this.tpOracle_Advanced.ResumeLayout(false);
            this.tpOracle_Advanced.PerformLayout();
            this._tpMSSql.ResumeLayout(false);
            this._tpMSSql.PerformLayout();
            this._tpPGSql.ResumeLayout(false);
            this._tpPGSql.PerformLayout();
            this._tpODBC.ResumeLayout(false);
            this._tpODBC.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox _cbProfiles;
        private System.Windows.Forms.TextBox _tbName;
        private System.Windows.Forms.Label _lbl_profilename;
        private System.Windows.Forms.MenuStrip _ms;
        private System.Windows.Forms.ToolStripMenuItem _msNew;
        private System.Windows.Forms.ToolStripMenuItem _msDelete;
        private System.Windows.Forms.ToolStripMenuItem _msTest;
        private System.Windows.Forms.TextBox _tbDesc;
        private System.Windows.Forms.Label _lbl_description;
        private System.Windows.Forms.Label _lblAddStr;
        private System.Windows.Forms.TextBox _tbAddStr;
        private System.Windows.Forms.ToolStripMenuItem _msCopyAs;
        private System.Windows.Forms.ComboBox _cbMySql_Db;
        private System.Windows.Forms.Label _lblMySql_Usr;
        private System.Windows.Forms.Label _lblMySql_Db;
        private System.Windows.Forms.TextBox _tbMySql_Usr;
        private System.Windows.Forms.Label _lblMySql_Host;
        private System.Windows.Forms.Label _lblMySql_Pasw;
        private System.Windows.Forms.TextBox _tbMySql_Host;
        private System.Windows.Forms.TextBox _tbMySql_Pasw;
        private System.Windows.Forms.Label _lblMSSql_Usr;
        private System.Windows.Forms.Label _lblMSSql_Host;
        private System.Windows.Forms.TextBox _tbMSSql_Usr;
        private System.Windows.Forms.Label _lblMSSql_Pasw;
        private System.Windows.Forms.TextBox _tbMSSql_Pasw;
        private System.Windows.Forms.ComboBox _cbOrcl_Priv;
        private System.Windows.Forms.Label _lblOrcl_Usr;
        private System.Windows.Forms.Label _lblOrcl_Priv;
        private System.Windows.Forms.TextBox _tbOrcl_Usr;
        private System.Windows.Forms.Label _lblOrcl_SID;
        private System.Windows.Forms.Label _lblOrcl_Pasw;
        private System.Windows.Forms.TextBox _tbOrcl_SID;
        private System.Windows.Forms.TextBox _tbOrcl_Pasw;
        private System.Windows.Forms.ComboBox _cbODBC_Driver;
        private System.Windows.Forms.Label _lblODBC_Usr;
        private System.Windows.Forms.Label _lblODBC_Driver;
        private System.Windows.Forms.TextBox _tbODBC_Usr;
        private System.Windows.Forms.Label _lblODBC_Host;
        private System.Windows.Forms.Label _lblODBC_Pasw;
        private System.Windows.Forms.TextBox _tbODBC_Host;
        private System.Windows.Forms.TextBox _tbODBC_Pasw;
        private System.Windows.Forms.TextBox _tbMSSql_Host;
        private System.Windows.Forms.Label _lblMSSql_Auth;
        private System.Windows.Forms.ComboBox _cbMSSql_Auth;
        private System.Windows.Forms.StatusStrip _sS;
        private System.Windows.Forms.ToolStripStatusLabel _sS_lblStatus;
        private System.Windows.Forms.Panel _pnlProfileDef;
        private System.Windows.Forms.TabControl _tcDbType;
        private System.Windows.Forms.TabPage _tpMySql;
        private System.Windows.Forms.TabPage _tpOrcl;
        private System.Windows.Forms.TabPage _tpMSSql;
        private System.Windows.Forms.TabPage _tpODBC;
        private System.Windows.Forms.Label _lblMSSql_Db;
        private System.Windows.Forms.Label _lblODBC_AddStr;
        private System.Windows.Forms.TextBox _tbODBC_AddStr;
        private System.Windows.Forms.ComboBox _cbMSSql_Db;
        private System.Windows.Forms.TabPage _tpPGSql;
        private System.Windows.Forms.TextBox _tbPGSql_Db;
        private System.Windows.Forms.TextBox _tbPGSql_Usr;
        private System.Windows.Forms.Label _lblPGSql_Pasw;
        private System.Windows.Forms.Label _lblPGSql_Host;
        private System.Windows.Forms.Label _lblPGSql_Usr;
        private System.Windows.Forms.TextBox _tbPGSql_Host;
        private System.Windows.Forms.Label _lblPGSql_Db;
        private System.Windows.Forms.TextBox _tbPGSql_Pasw;
        private System.Windows.Forms.ComboBox _cbMySql_Encoding;
        private System.Windows.Forms.Label _lblMySql_Encoding;
        private System.Windows.Forms.ComboBox _cbOrcl_Encoding;
        private System.Windows.Forms.Label _lblOrcl_Encoding;
        private System.Windows.Forms.TabControl _tcMySql;
        private System.Windows.Forms.TabPage tpMySql_Default;
        private System.Windows.Forms.TabPage tpMySql_Advanced;
        private System.Windows.Forms.TabControl tcOracle;
        private System.Windows.Forms.TabPage tpOracle_Default;
        private System.Windows.Forms.TabPage tpOracle_Advanced;
        private System.Windows.Forms.Label _lblProfile;
        private System.Windows.Forms.Label _lblOracle_InitLong;
        private System.Windows.Forms.MaskedTextBox _mtbOracle_InitLongFetchSize;
        private System.Windows.Forms.Label _lblOracle_InitLongNote;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
    }
}