namespace Nyx
{
    partial class BookmarkManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookmarkManager));
            this._ms = new System.Windows.Forms.MenuStrip();
            this._msNew = new System.Windows.Forms.ToolStripMenuItem();
            this._msNew_Bm = new System.Windows.Forms.ToolStripMenuItem();
            this._msNew_Cat = new System.Windows.Forms.ToolStripMenuItem();
            this._msEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._msDelete = new System.Windows.Forms.ToolStripMenuItem();
            this._imglist = new System.Windows.Forms.ImageList(this.components);
            this._trview = new System.Windows.Forms.TreeView();
            this._tblLayoutPnl = new System.Windows.Forms.TableLayoutPanel();
            this._btClose = new System.Windows.Forms.Button();
            this._ms.SuspendLayout();
            this._tblLayoutPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _ms
            // 
            this._ms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msNew,
            this._msEdit,
            this._msDelete});
            resources.ApplyResources(this._ms, "_ms");
            this._ms.Name = "_ms";
            // 
            // _msNew
            // 
            this._msNew.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msNew_Bm,
            this._msNew_Cat});
            this._msNew.Image = global::Nyx.Properties.Resources.script_add;
            resources.ApplyResources(this._msNew, "_msNew");
            this._msNew.Name = "_msNew";
            // 
            // _msNew_Bm
            // 
            this._msNew_Bm.Image = global::Nyx.Properties.Resources.script_code;
            resources.ApplyResources(this._msNew_Bm, "_msNew_Bm");
            this._msNew_Bm.Name = "_msNew_Bm";
            this._msNew_Bm.Click += new System.EventHandler(this._msNew_Bm_Click);
            // 
            // _msNew_Cat
            // 
            this._msNew_Cat.Image = global::Nyx.Properties.Resources.script_code_red;
            resources.ApplyResources(this._msNew_Cat, "_msNew_Cat");
            this._msNew_Cat.Name = "_msNew_Cat";
            this._msNew_Cat.Click += new System.EventHandler(this._msNew_Cat_Click);
            // 
            // _msEdit
            // 
            resources.ApplyResources(this._msEdit, "_msEdit");
            this._msEdit.Image = global::Nyx.Properties.Resources.script_edit;
            this._msEdit.Name = "_msEdit";
            this._msEdit.Click += new System.EventHandler(this._msEdit_Click);
            // 
            // _msDelete
            // 
            resources.ApplyResources(this._msDelete, "_msDelete");
            this._msDelete.Image = global::Nyx.Properties.Resources.script_delete;
            this._msDelete.Name = "_msDelete";
            this._msDelete.Click += new System.EventHandler(this._msDelete_Click);
            // 
            // _imglist
            // 
            this._imglist.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_imglist.ImageStream")));
            this._imglist.TransparentColor = System.Drawing.Color.Transparent;
            this._imglist.Images.SetKeyName(0, "bookmark.png");
            this._imglist.Images.SetKeyName(1, "bookmark_folder.png");
            this._imglist.Images.SetKeyName(2, "arrow_right.png");
            // 
            // _trview
            // 
            this._trview.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._trview, "_trview");
            this._trview.FullRowSelect = true;
            this._trview.HideSelection = false;
            this._trview.HotTracking = true;
            this._trview.ImageList = this._imglist;
            this._trview.Name = "_trview";
            this._trview.ShowNodeToolTips = true;
            this._trview.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this._trview_AfterSelect);
            // 
            // _tblLayoutPnl
            // 
            resources.ApplyResources(this._tblLayoutPnl, "_tblLayoutPnl");
            this._tblLayoutPnl.Controls.Add(this._btClose, 0, 1);
            this._tblLayoutPnl.Controls.Add(this._trview, 0, 0);
            this._tblLayoutPnl.Name = "_tblLayoutPnl";
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // BookmarkManager
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btClose;
            this.Controls.Add(this._tblLayoutPnl);
            this.Controls.Add(this._ms);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "BookmarkManager";
            this.Load += new System.EventHandler(this.Bookmarks_Load);
            this._ms.ResumeLayout(false);
            this._ms.PerformLayout();
            this._tblLayoutPnl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _ms;
        private System.Windows.Forms.ToolStripMenuItem _msNew;
        private System.Windows.Forms.ToolStripMenuItem _msEdit;
        private System.Windows.Forms.ToolStripMenuItem _msDelete;
        private System.Windows.Forms.ToolStripMenuItem _msNew_Cat;
        private System.Windows.Forms.ToolStripMenuItem _msNew_Bm;
        private System.Windows.Forms.ImageList _imglist;
        private System.Windows.Forms.TreeView _trview;
        private System.Windows.Forms.TableLayoutPanel _tblLayoutPnl;
        private System.Windows.Forms.Button _btClose;
    }
}