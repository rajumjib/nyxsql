namespace Nyx
{
    /// <summary>
    /// UserSettingsManager, here the user can change the settings of nyx-usersettings.xml
    /// </summary>
    partial class UserSettingsManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserSettingsManager));
            this._tc = new System.Windows.Forms.TabControl();
            this._tabpgGeneral = new System.Windows.Forms.TabPage();
            this._pGGeneral = new System.Windows.Forms.PropertyGrid();
            this._tabpgLayout = new System.Windows.Forms.TabPage();
            this._pGDGrid = new System.Windows.Forms.PropertyGrid();
            this._tabpgAdvanced = new System.Windows.Forms.TabPage();
            this._pGAdvanced = new System.Windows.Forms.PropertyGrid();
            this._tabpgHotkeys = new System.Windows.Forms.TabPage();
            this._pGHotkeys = new System.Windows.Forms.PropertyGrid();
            this._tpWindows = new System.Windows.Forms.TabPage();
            this._pGWindows = new System.Windows.Forms.PropertyGrid();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._tc.SuspendLayout();
            this._tabpgGeneral.SuspendLayout();
            this._tabpgLayout.SuspendLayout();
            this._tabpgAdvanced.SuspendLayout();
            this._tabpgHotkeys.SuspendLayout();
            this._tpWindows.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tc
            // 
            this._tc.Controls.Add(this._tabpgGeneral);
            this._tc.Controls.Add(this._tabpgLayout);
            this._tc.Controls.Add(this._tabpgAdvanced);
            this._tc.Controls.Add(this._tabpgHotkeys);
            this._tc.Controls.Add(this._tpWindows);
            resources.ApplyResources(this._tc, "_tc");
            this._tc.Name = "_tc";
            this._tc.SelectedIndex = 0;
            // 
            // _tabpgGeneral
            // 
            this._tabpgGeneral.Controls.Add(this._pGGeneral);
            resources.ApplyResources(this._tabpgGeneral, "_tabpgGeneral");
            this._tabpgGeneral.Name = "_tabpgGeneral";
            this._tabpgGeneral.UseVisualStyleBackColor = true;
            // 
            // _pGGeneral
            // 
            resources.ApplyResources(this._pGGeneral, "_pGGeneral");
            this._pGGeneral.Name = "_pGGeneral";
            this._pGGeneral.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this._SettingChanged);
            // 
            // _tabpgLayout
            // 
            this._tabpgLayout.Controls.Add(this._pGDGrid);
            resources.ApplyResources(this._tabpgLayout, "_tabpgLayout");
            this._tabpgLayout.Name = "_tabpgLayout";
            this._tabpgLayout.UseVisualStyleBackColor = true;
            // 
            // _pGDGrid
            // 
            resources.ApplyResources(this._pGDGrid, "_pGDGrid");
            this._pGDGrid.Name = "_pGDGrid";
            this._pGDGrid.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this._SettingChanged);
            // 
            // _tabpgAdvanced
            // 
            this._tabpgAdvanced.Controls.Add(this._pGAdvanced);
            resources.ApplyResources(this._tabpgAdvanced, "_tabpgAdvanced");
            this._tabpgAdvanced.Name = "_tabpgAdvanced";
            this._tabpgAdvanced.UseVisualStyleBackColor = true;
            // 
            // _pGAdvanced
            // 
            resources.ApplyResources(this._pGAdvanced, "_pGAdvanced");
            this._pGAdvanced.Name = "_pGAdvanced";
            this._pGAdvanced.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this._SettingChangedRestart);
            // 
            // _tabpgHotkeys
            // 
            this._tabpgHotkeys.Controls.Add(this._pGHotkeys);
            resources.ApplyResources(this._tabpgHotkeys, "_tabpgHotkeys");
            this._tabpgHotkeys.Name = "_tabpgHotkeys";
            this._tabpgHotkeys.UseVisualStyleBackColor = true;
            // 
            // _pGHotkeys
            // 
            resources.ApplyResources(this._pGHotkeys, "_pGHotkeys");
            this._pGHotkeys.Name = "_pGHotkeys";
            this._pGHotkeys.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            // 
            // _tpWindows
            // 
            this._tpWindows.Controls.Add(this._pGWindows);
            resources.ApplyResources(this._tpWindows, "_tpWindows");
            this._tpWindows.Name = "_tpWindows";
            this._tpWindows.UseVisualStyleBackColor = true;
            // 
            // _pGWindows
            // 
            resources.ApplyResources(this._pGWindows, "_pGWindows");
            this._pGWindows.Name = "_pGWindows";
            this._pGWindows.PropertyValueChanged += new System.Windows.Forms.PropertyValueChangedEventHandler(this._SettingChanged);
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btExit_Click);
            // 
            // _btOk
            // 
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btOk_Click);
            // 
            // UserSettingsManager
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Controls.Add(this._tc);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "UserSettingsManager";
            this.Load += new System.EventHandler(this.SettingsDialog_Load);
            this._tc.ResumeLayout(false);
            this._tabpgGeneral.ResumeLayout(false);
            this._tabpgLayout.ResumeLayout(false);
            this._tabpgAdvanced.ResumeLayout(false);
            this._tabpgHotkeys.ResumeLayout(false);
            this._tpWindows.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _tc;
        private System.Windows.Forms.TabPage _tabpgLayout;
        private System.Windows.Forms.TabPage _tabpgAdvanced;
        private System.Windows.Forms.TabPage _tabpgHotkeys;
        private System.Windows.Forms.TabPage _tpWindows;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.PropertyGrid _pGDGrid;
        private System.Windows.Forms.PropertyGrid _pGAdvanced;
        private System.Windows.Forms.PropertyGrid _pGGeneral;
        private System.Windows.Forms.TabPage _tabpgGeneral;
        private System.Windows.Forms.PropertyGrid _pGWindows;
        private System.Windows.Forms.PropertyGrid _pGHotkeys;
    }
}