/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;
using NyxSettings;

namespace Nyx.Tools
{
    /// <summary>
    /// FirstStartSetup
    /// - only called when no nyx-main.xml exists
    /// - initialize the maincfg (nyx-main.xml) and userappsettings (nyx-usersettings.xml)
    /// </summary>
    public partial class FirstStartSetup : Form
    {
        /// <remarks>
        /// Initalize GUI, set SelectedIndex and Tooltips
        /// </remarks>
        public FirstStartSetup()
        {
            InitializeComponent();
            /*
             * set settings
             * 
             * 0 UserApplication-Folder
             * 1 NyxProgram-Folder
             */
            this._cbSettings.SelectedIndex = 0;
            this._cbRuntime.SelectedIndex = 0;
            this._cbProfiles.SelectedIndex = 0;
            this._cbHotKeys.SelectedIndex = 1;
            this._cbHistory.SelectedIndex = 0;
            this._cbBookmarks.SelectedIndex = 0;
        }

        private void _btSave_Click(object sender, EventArgs e)
        {   
            string folder;
            /* maincfg
             */ 
            MainSettings mSett = new MainSettings();
            if (this._cbSettings.SelectedIndex == 0)
            {
                mSett.UserSettingsLoc = SettingsLocation.UsersApplicationFolder;
                folder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "\\Nyx\\";
            }
            else
            {
                mSett.UserSettingsLoc = SettingsLocation.ApplicationFolder;
                folder = Application.StartupPath + "\\";
            }
            // save to program folder
            if (!Settings.SerializeMain(mSett, Application.StartupPath + "\\"))
            {
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("MainSettingsSaveErr", new string[] { Settings.ErrExc.Message.ToString() }),
                                    Settings.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                return;
            }

            /* userappsettings
             */
            NyxSettings.UserAppSettings uaSett = new NyxSettings.UserAppSettings();
            if (this._cbRuntime.SelectedIndex == 0)
                uaSett.SaveScopeRuntime = NyxSettings.SettingsLocation.UsersApplicationFolder;
            else
                uaSett.SaveScopeRuntime = NyxSettings.SettingsLocation.ApplicationFolder;

            if (this._cbProfiles.SelectedIndex == 0)
                uaSett.SaveScopeProfiles = NyxSettings.SettingsLocation.UsersApplicationFolder;
            else
                uaSett.SaveScopeProfiles = NyxSettings.SettingsLocation.ApplicationFolder;

            if (this._cbHotKeys.SelectedIndex == 0)
                uaSett.SaveScopeHotKeys = NyxSettings.SettingsLocation.UsersApplicationFolder;
            else
                uaSett.SaveScopeHotKeys = NyxSettings.SettingsLocation.ApplicationFolder;

            if (this._cbHistory.SelectedIndex == 0)
                uaSett.SaveScopeHistory = NyxSettings.SettingsLocation.UsersApplicationFolder;
            else
                uaSett.SaveScopeHistory = NyxSettings.SettingsLocation.ApplicationFolder;

            if(this._cbBookmarks.SelectedIndex == 0)
                uaSett.SaveScopeBookmarks = NyxSettings.SettingsLocation.UsersApplicationFolder;
            else
                uaSett.SaveScopeBookmarks = NyxSettings.SettingsLocation.ApplicationFolder;
            // save
            if (!Settings.SerializeUserSettings(uaSett, folder))
            {
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("UserSettingsSaveErr", new string[] { Settings.ErrExc.Message.ToString() }),
                                    Settings.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                return;
            }
            // set nyxmain uasett
            NyxMain.UASett = uaSett;
            // close
            this.Close();
        }
        private void _btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}