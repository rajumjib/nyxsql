namespace Nyx
{
    partial class BookmarkCatHelper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BookmarkCatHelper));
            this._lblName = new System.Windows.Forms.Label();
            this._tbName = new System.Windows.Forms.TextBox();
            this._lblCats = new System.Windows.Forms.Label();
            this._cbCats = new System.Windows.Forms.ComboBox();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _lblName
            // 
            resources.ApplyResources(this._lblName, "_lblName");
            this._lblName.Name = "_lblName";
            // 
            // _tbName
            // 
            this._tbName.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbName, "_tbName");
            this._tbName.Name = "_tbName";
            this._tbName.TextChanged += new System.EventHandler(this._CheckInput);
            // 
            // _lblCats
            // 
            resources.ApplyResources(this._lblCats, "_lblCats");
            this._lblCats.Name = "_lblCats";
            // 
            // _cbCats
            // 
            this._cbCats.BackColor = System.Drawing.Color.GhostWhite;
            this._cbCats.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbCats.FormattingEnabled = true;
            resources.ApplyResources(this._cbCats, "_cbCats");
            this._cbCats.Name = "_cbCats";
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            // 
            // _btOk
            // 
            this._btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            // 
            // BookmarkCatHelper
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Controls.Add(this._lblCats);
            this.Controls.Add(this._cbCats);
            this.Controls.Add(this._tbName);
            this.Controls.Add(this._lblName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "BookmarkCatHelper";
            this.Load += new System.EventHandler(this.BookmarkCatHelper_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _lblName;
        private System.Windows.Forms.TextBox _tbName;
        private System.Windows.Forms.Label _lblCats;
        private System.Windows.Forms.ComboBox _cbCats;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
    }
}