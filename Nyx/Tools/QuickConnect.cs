/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections.Generic;
using NyxSettings;
using System.Windows.Forms;

namespace Nyx
{
    /// <summary> Quickconnect Dialog </summary>
    public partial class QuickConnect : Form
    {
        /// <summary> init function </summary>
        public QuickConnect()
        {
            InitializeComponent();
        }
        private void QuickConnect_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // init
            this._cbMSSql_Auth.SelectedIndex = 0;
            this._cbOrcl_Priv.SelectedIndex = 0;
            this._cbODBC_Driver.SelectedIndex = 0;
            this._tbMySql_Usr.Focus();
        }

        /// <summary> Display QuickConnect, check userinput and return as a profilesetting </summary>
        /// <param name="prf">out passed profilesetting</param>
        /// <returns>dialogresult</returns>
        public DialogResult GetQuickConnect(ref ProfileSettings prf)
        {   // validate
            if (prf == null)
                return DialogResult.Cancel;

            DialogResult dRes = this.ShowDialog();
            if (dRes == DialogResult.OK)
            {
                prf.ProfileAddString = this._tbAddStr.Text;
                prf.ProfileName = GuiHelper.GetResourceMsg("QuickConnect");
                // set db-related parameter
                if (this._tcDbType.SelectedTab == this._tpMySql)
                {
                    prf.ProfileDBType = DBType.MySql;
                    prf.ProfileUser = this._tbMySql_Usr.Text;
                    prf.ProfilePasw = Classes.NyxCrypt.EncryptPasw(this._tbMySql_Pasw.Text);
                    prf.ProfileTarget = this._tbMySql_Host.Text;
                    prf.ProfileDB = this._cbMySql_Db.Text;
                    prf.Encoding = this._cbMySql_Encoding.Text;
                }
                else if (this._tcDbType.SelectedTab == this._tpOrcl)
                {
                    prf.ProfileDBType = DBType.Oracle;
                    prf.ProfileUser = this._tbOrcl_Usr.Text;
                    prf.ProfilePasw = Classes.NyxCrypt.EncryptPasw(this._tbOrcl_Pasw.Text);
                    prf.ProfileTarget = this._tbOrcl_SID.Text;
                    prf.ProfileDB = this._cbOrcl_Priv.Text;
                    prf.Encoding = this._cbOrcl_Encoding.Text;
                    // check if we got an input
                    if (this._mtbOracle_InitLongFetchSize.Text.Length > 0)
                    {   // conv int
                        int itmp;
                        if (Int32.TryParse(this._mtbOracle_InitLongFetchSize.Text, out itmp))
                            prf.InitLongFetchSize = itmp;
                    }
                    else
                        prf.InitLongFetchSize = -1;
                }
                else if (this._tcDbType.SelectedTab == this._tpMSSql)
                {
                    prf.ProfileDBType = DBType.MSSql;
                    prf.ProfileUser = this._tbMSSql_Usr.Text;
                    prf.ProfilePasw = Classes.NyxCrypt.EncryptPasw(this._tbMSSql_Pasw.Text);
                    prf.ProfileTarget = this._tbMSSql_Host.Text;
                    prf.ProfileDB = this._cbMSSql_Db.Text;
                    prf.ProfileParam1 = this._cbMSSql_Auth.SelectedItem.ToString();
                }
                else if (this._tcDbType.SelectedTab == this._tpPGSql)
                {
                    prf.ProfileDBType = DBType.PgSql;
                    prf.ProfileUser = this._tbPGSql_Usr.Text;
                    prf.ProfilePasw = Classes.NyxCrypt.EncryptPasw(this._tbPGSql_Pasw.Text);
                    prf.ProfileTarget = this._tbPGSql_Host.Text;
                    prf.ProfileDB = this._tbPGSql_Db.Text;
                }
                else if (this._tcDbType.SelectedTab == this._tpODBC)
                {
                    prf.ProfileDBType = DBType.Odbc;
                    prf.ProfileUser = this._tbODBC_Usr.Text;
                    prf.ProfilePasw = Classes.NyxCrypt.EncryptPasw(this._tbODBC_Pasw.Text);
                    prf.ProfileTarget = this._tbODBC_Host.Text;
                    prf.ProfileDB = this._tbODBC_AddStr.Text;
                    prf.ProfileParam1 = this._cbODBC_Driver.SelectedItem.ToString();
                }
            }
            return dRes;
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        private void _cbDb_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cbox;
            ProfileSettings prf = new ProfileSettings();
            // which dbtype?
            if (this._tcDbType.SelectedTab == this._tpMySql)
            {   // set combobox
                cbox = this._cbMySql_Db;
                // set profile
                prf.ProfileDBType = DBType.MySql;
                prf.ProfileUser = this._tbMySql_Usr.Text;
                prf.ProfilePasw = Classes.NyxCrypt.EncryptPasw(this._tbMySql_Pasw.Text);
                prf.ProfileTarget = this._tbMySql_Host.Text;
                prf.ProfileDB = String.Empty;
                prf.Encoding = this._cbMySql_Encoding.Text;
            }
            else
            {   // set combobox
                cbox = this._cbMSSql_Db;
                // set profile
                prf.ProfileDBType = DBType.MSSql;
                prf.ProfileUser = this._tbMSSql_Usr.Text;
                prf.ProfilePasw = Classes.NyxCrypt.EncryptPasw(this._tbMSSql_Pasw.Text);
                prf.ProfileTarget = this._tbMSSql_Host.Text;
                prf.ProfileDB = String.Empty;
                prf.ProfileParam1 = this._cbMSSql_Auth.SelectedItem.ToString();
            }

            if (cbox.Text == "<Retrieve Databases...>")
            {
                Queue<string> qDbs = new Queue<string>();
                // retrieve databases
                GeneralDB.RetrieveDbs rtvDbs = new GeneralDB.RetrieveDbs();
                if (rtvDbs.ShowDialog(prf, ref qDbs))
                {
                    cbox.BeginUpdate();
                    cbox.Items.Clear();
                    foreach (string db in qDbs)
                        cbox.Items.Add(db);
                    cbox.Items.Add("<Retrieve Databases...>");
                    cbox.EndUpdate();
                }
                else
                    cbox.Text = String.Empty;
            }
        }
        private void _cbODBC_Driver_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._cbODBC_Driver.SelectedItem.ToString() == "IBM DB2 ODBC DRIVER")
            {
                this._lblODBC_AddStr.Visible = true;
                this._lblODBC_AddStr.Text = GuiHelper.GetResourceMsg("Profile_ODBCAddStr");
                this._tbODBC_AddStr.Visible = true;
            }
            else
            {
                this._lblODBC_AddStr.Visible = false;
                this._tbODBC_AddStr.Visible = false;
            }
        }
        private void _cbMSSql_Auth_SelectedIndexChanged(object sender, EventArgs e)
        {   // windows auth
            if (this._cbMSSql_Auth.SelectedIndex == 0)
            {
                this._tbMSSql_Usr.Enabled = false;
                this._tbMSSql_Pasw.Enabled = false;
                this._tbMSSql_Usr.BackColor = System.Drawing.SystemColors.Control;
                this._tbMSSql_Pasw.BackColor = System.Drawing.SystemColors.Control;
            }
            // user/pasw
            else
            {
                this._tbMSSql_Usr.Enabled = true;
                this._tbMSSql_Pasw.Enabled = true;
                this._tbMSSql_Usr.BackColor = System.Drawing.Color.GhostWhite;
                this._tbMSSql_Pasw.BackColor = System.Drawing.Color.GhostWhite;
            }
        }
        private void _btConnect_Click(object sender, EventArgs e)
        {
            // checkups
            if (this._tcDbType.SelectedTab == this._tpMySql)
            {
                if (this._tbMySql_Usr.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbMySql_Usr.Focus();
                    return;
                }
                if (this._tbMySql_Host.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidHost"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbMySql_Host.Focus();
                    return;
                }
                if (this._cbMySql_Db.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidDb"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._cbMySql_Db.Focus();
                    return;
                }
            }
            else if (this._tcDbType.SelectedTab == this._tpMSSql)
            {
                if (this._cbMSSql_Auth.SelectedIndex == 1
                    && this._tbMSSql_Usr.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbMSSql_Usr.Focus();
                    return;
                }
                if (this._tbMSSql_Host.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidHost"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbMSSql_Host.Focus();
                    return;
                }
                if (this._cbMSSql_Db.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidDb"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._cbMSSql_Db.Focus();
                    return;
                }
            }
            else if (this._tcDbType.SelectedTab == this._tpOrcl)
            {
                if (this._tbOrcl_Usr.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbOrcl_Usr.Focus();
                    return;
                }
                if (this._tbOrcl_SID.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidSID"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbOrcl_SID.Focus();
                    return;
                }
            }
            else if (this._tcDbType.SelectedTab == this._tpPGSql)
            {
                if (this._tbPGSql_Usr.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbPGSql_Usr.Focus();
                    return;
                }
                if (this._tbPGSql_Host.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidHost"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbPGSql_Host.Focus();
                    return;
                }
                if (this._tbPGSql_Db.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidDb"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbPGSql_Db.Focus();
                    return;
                }
            }
            else if (this._tcDbType.SelectedTab == this._tpODBC)
            {
                if (this._tbODBC_Usr.Text.Length < 1)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._tbODBC_Usr.Focus();
                    return;
                }
                if (this._cbODBC_Driver.SelectedIndex == 0)
                {
                    AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("Prf_InvalidDriver"), AppDialogs.MessageHandler.EnumMsgType.Warning);
                    this._cbODBC_Driver.Focus();
                    return;
                }
            }
            // finally return dialogresult and close dialog
            this.DialogResult = DialogResult.OK;
            this.Close();
        }
        private void _tcDbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._tcDbType.SelectedTab == this._tpMySql) this._tbMySql_Usr.Focus();
            else if (this._tcDbType.SelectedTab == this._tpOrcl) this._tbOrcl_Usr.Focus();
            else if (this._tcDbType.SelectedTab == this._tpMSSql) this._tbMSSql_Usr.Focus();
            else if (this._tcDbType.SelectedTab == this._tpPGSql) this._tbPGSql_Usr.Focus();
            else if (this._tcDbType.SelectedTab == this._tpODBC) this._tbODBC_Usr.Focus();
        }
    }
}