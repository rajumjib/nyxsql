namespace Nyx.Tools
{
    partial class FirstStartSetup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FirstStartSetup));
            this._lblWelcome = new System.Windows.Forms.Label();
            this._cbHotKeys = new System.Windows.Forms.ComboBox();
            this._lblHotKeys = new System.Windows.Forms.Label();
            this._cbBookmarks = new System.Windows.Forms.ComboBox();
            this._lblBookmarks = new System.Windows.Forms.Label();
            this._cbProfiles = new System.Windows.Forms.ComboBox();
            this._cbRuntime = new System.Windows.Forms.ComboBox();
            this._lblProfiles = new System.Windows.Forms.Label();
            this._lblRuntime = new System.Windows.Forms.Label();
            this._lblHistory = new System.Windows.Forms.Label();
            this._cbHistory = new System.Windows.Forms.ComboBox();
            this._tooltip = new System.Windows.Forms.ToolTip(this.components);
            this._cbSettings = new System.Windows.Forms.ComboBox();
            this._lblSettings = new System.Windows.Forms.Label();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._lblNote = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _lblWelcome
            // 
            resources.ApplyResources(this._lblWelcome, "_lblWelcome");
            this._lblWelcome.Name = "_lblWelcome";
            // 
            // _cbHotKeys
            // 
            this._cbHotKeys.BackColor = System.Drawing.Color.GhostWhite;
            this._cbHotKeys.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbHotKeys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbHotKeys.FormattingEnabled = true;
            this._cbHotKeys.Items.AddRange(new object[] {
            resources.GetString("_cbHotKeys.Items"),
            resources.GetString("_cbHotKeys.Items1")});
            resources.ApplyResources(this._cbHotKeys, "_cbHotKeys");
            this._cbHotKeys.Name = "_cbHotKeys";
            this._tooltip.SetToolTip(this._cbHotKeys, resources.GetString("_cbHotKeys.ToolTip"));
            // 
            // _lblHotKeys
            // 
            resources.ApplyResources(this._lblHotKeys, "_lblHotKeys");
            this._lblHotKeys.Name = "_lblHotKeys";
            // 
            // _cbBookmarks
            // 
            this._cbBookmarks.BackColor = System.Drawing.Color.GhostWhite;
            this._cbBookmarks.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbBookmarks.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbBookmarks.FormattingEnabled = true;
            this._cbBookmarks.Items.AddRange(new object[] {
            resources.GetString("_cbBookmarks.Items"),
            resources.GetString("_cbBookmarks.Items1")});
            resources.ApplyResources(this._cbBookmarks, "_cbBookmarks");
            this._cbBookmarks.Name = "_cbBookmarks";
            this._tooltip.SetToolTip(this._cbBookmarks, resources.GetString("_cbBookmarks.ToolTip"));
            // 
            // _lblBookmarks
            // 
            resources.ApplyResources(this._lblBookmarks, "_lblBookmarks");
            this._lblBookmarks.Name = "_lblBookmarks";
            // 
            // _cbProfiles
            // 
            this._cbProfiles.BackColor = System.Drawing.Color.GhostWhite;
            this._cbProfiles.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbProfiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbProfiles.FormattingEnabled = true;
            this._cbProfiles.Items.AddRange(new object[] {
            resources.GetString("_cbProfiles.Items"),
            resources.GetString("_cbProfiles.Items1")});
            resources.ApplyResources(this._cbProfiles, "_cbProfiles");
            this._cbProfiles.Name = "_cbProfiles";
            this._tooltip.SetToolTip(this._cbProfiles, resources.GetString("_cbProfiles.ToolTip"));
            // 
            // _cbRuntime
            // 
            this._cbRuntime.BackColor = System.Drawing.Color.GhostWhite;
            this._cbRuntime.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbRuntime.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbRuntime.FormattingEnabled = true;
            this._cbRuntime.Items.AddRange(new object[] {
            resources.GetString("_cbRuntime.Items"),
            resources.GetString("_cbRuntime.Items1")});
            resources.ApplyResources(this._cbRuntime, "_cbRuntime");
            this._cbRuntime.Name = "_cbRuntime";
            this._tooltip.SetToolTip(this._cbRuntime, resources.GetString("_cbRuntime.ToolTip"));
            // 
            // _lblProfiles
            // 
            resources.ApplyResources(this._lblProfiles, "_lblProfiles");
            this._lblProfiles.Name = "_lblProfiles";
            // 
            // _lblRuntime
            // 
            resources.ApplyResources(this._lblRuntime, "_lblRuntime");
            this._lblRuntime.Name = "_lblRuntime";
            // 
            // _lblHistory
            // 
            resources.ApplyResources(this._lblHistory, "_lblHistory");
            this._lblHistory.Name = "_lblHistory";
            // 
            // _cbHistory
            // 
            this._cbHistory.BackColor = System.Drawing.Color.GhostWhite;
            this._cbHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbHistory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbHistory.FormattingEnabled = true;
            this._cbHistory.Items.AddRange(new object[] {
            resources.GetString("_cbHistory.Items"),
            resources.GetString("_cbHistory.Items1")});
            resources.ApplyResources(this._cbHistory, "_cbHistory");
            this._cbHistory.Name = "_cbHistory";
            this._tooltip.SetToolTip(this._cbHistory, resources.GetString("_cbHistory.ToolTip"));
            // 
            // _cbSettings
            // 
            this._cbSettings.BackColor = System.Drawing.Color.GhostWhite;
            this._cbSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbSettings.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbSettings.FormattingEnabled = true;
            this._cbSettings.Items.AddRange(new object[] {
            resources.GetString("_cbSettings.Items"),
            resources.GetString("_cbSettings.Items1")});
            resources.ApplyResources(this._cbSettings, "_cbSettings");
            this._cbSettings.Name = "_cbSettings";
            this._tooltip.SetToolTip(this._cbSettings, resources.GetString("_cbSettings.ToolTip"));
            // 
            // _lblSettings
            // 
            resources.ApplyResources(this._lblSettings, "_lblSettings");
            this._lblSettings.Name = "_lblSettings";
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btCancel_Click);
            // 
            // _btOk
            // 
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btSave_Click);
            // 
            // _lblNote
            // 
            resources.ApplyResources(this._lblNote, "_lblNote");
            this._lblNote.Name = "_lblNote";
            // 
            // FirstStartSetup
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._lblNote);
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Controls.Add(this._cbSettings);
            this.Controls.Add(this._lblSettings);
            this.Controls.Add(this._cbHotKeys);
            this.Controls.Add(this._lblHotKeys);
            this.Controls.Add(this._cbBookmarks);
            this.Controls.Add(this._lblBookmarks);
            this.Controls.Add(this._cbProfiles);
            this.Controls.Add(this._cbRuntime);
            this.Controls.Add(this._lblProfiles);
            this.Controls.Add(this._lblRuntime);
            this.Controls.Add(this._lblHistory);
            this.Controls.Add(this._cbHistory);
            this.Controls.Add(this._lblWelcome);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FirstStartSetup";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label _lblWelcome;
        private System.Windows.Forms.ComboBox _cbHotKeys;
        private System.Windows.Forms.Label _lblHotKeys;
        private System.Windows.Forms.ComboBox _cbBookmarks;
        private System.Windows.Forms.Label _lblBookmarks;
        private System.Windows.Forms.ComboBox _cbProfiles;
        private System.Windows.Forms.ComboBox _cbRuntime;
        private System.Windows.Forms.Label _lblProfiles;
        private System.Windows.Forms.Label _lblRuntime;
        private System.Windows.Forms.Label _lblHistory;
        private System.Windows.Forms.ComboBox _cbHistory;
        private System.Windows.Forms.ToolTip _tooltip;
        private System.Windows.Forms.ComboBox _cbSettings;
        private System.Windows.Forms.Label _lblSettings;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.Label _lblNote;
    }
}