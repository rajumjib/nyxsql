/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;
using System.Collections.Generic;
using NyxSettings;
using Nyx.Classes;
using Nyx.AppDialogs;

namespace Nyx
{
    public partial class ProfileManager : Form
    {
        // vars
        private static string cfgErr = String.Empty;
        /// <summary> Configuration error which occured, if occured </summary>
        public static string CfgErr
        {
            get { return cfgErr; }
        }
        /// <summary> Notify of changed settings for re-setting them in NyxMain </summary>
        public event EventHandler<SettingsChangedEventArgs> SettingsChangedEvent;
        /// <summary> Init </summary>
        public ProfileManager()
        {
            InitializeComponent();
        }
        private void FrmProfiles_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // setting gui
            RefreshProfile();
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        /// <summary>
        /// Fire Event that settings changed
        /// </summary>
        /// <param name="e">SettingsChangedEventArgs</param>
        protected void OnSettingsChanged(SettingsChangedEventArgs e)
        {
            if (SettingsChangedEvent != null)
                SettingsChangedEvent(this, e);
        }

        private void _msNew_Click(object sender, EventArgs e)
        {   // reset gui and get profilename
            ResetGui();
            GetPrfName();
        }
        private void _btOk_Click(object sender, EventArgs e)
        {
            SavePrf();
        }
        private void _msDelete_Click(object sender, EventArgs e)
        {
            DeletePrf();
        }
        private void _btClose_Click(object sender, EventArgs e)
        {
            ExitPrf();
        }
        private void _msTest_Click(object sender, EventArgs e)
        {
            TestPrf();
        }
        private void _msCopy_Click(object sender, EventArgs e)
        {
            GetPrfName();
        }

        private void _cbDb_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!CheckInput())
                return;

            ComboBox cbox = null;
            // which dbtype?
            if (this._tcDbType.SelectedTab == this._tpMySql)
            {   // set combobox
                cbox = this._cbMySql_Db;
            }
            else
            {   // set combobox
                cbox = this._cbMSSql_Db;
            }

            if (cbox != null && cbox.Text == "<Retrieve Databases...>")
            {
                Queue<string> qDbs = new Queue<string>();
                // retrieve databases
                GeneralDB.RetrieveDbs rtvDbs = new GeneralDB.RetrieveDbs();
                if (rtvDbs.ShowDialog(GetProfileSettings(), ref qDbs))
                {
                    cbox.BeginUpdate();
                    cbox.Items.Clear();
                    foreach (string db in qDbs)
                        cbox.Items.Add(db);
                    cbox.Items.Add("<Retrieve Databases...>");
                    cbox.EndUpdate();
                }
                else
                    cbox.Text = String.Empty;
            }
        }
        private void _cbProfiles_SelectedIndexChanged(object sender, EventArgs e)
        {
            // cleanup
            ResetGui();

            if (this._cbProfiles.SelectedItem.ToString() != "Select Profile...")
            {   // pre-gui settings
                this._tbName.ReadOnly = true;
                this._pnlProfileDef.Enabled = true;

                this._btOk.Enabled = true;
                this._msDelete.Enabled = true;
                this._msTest.Enabled = true;
                this._msCopyAs.Enabled = true;
                this._sS_lblStatus.Text = GuiHelper.GetResourceMsg("Profile_EditProfile");

                // filling the profile details
                if (NyxMain.HTProfiles.ContainsKey(this._cbProfiles.SelectedItem))
                {   // init temporary vars
                    string prfname = this._cbProfiles.SelectedItem.ToString();
                    ProfileSettings profile = (ProfileSettings)NyxMain.HTProfiles[prfname];

                    /* gui settings
                     */
                    this._tbName.Text = profile.ProfileName;
                    this._tbDesc.Text = profile.ProfileDescription;
                    this._tbAddStr.Text = profile.ProfileAddString;

                    // database dependant settings
                    switch (profile.ProfileDBType)
                    {
                        case DBType.Oracle:
                            this._tbOrcl_Usr.Text = profile.ProfileUser;
                            this._tbOrcl_SID.Text = profile.ProfileTarget;
                            this._cbOrcl_Encoding.Text = profile.Encoding;
                            if(profile.InitLongFetchSize > -1)
                                this._mtbOracle_InitLongFetchSize.Text = profile.InitLongFetchSize.ToString(NyxMain.NyxCI);
                            this._tbOrcl_Pasw.Text = NyxCrypt.DecryptPasw(profile.ProfilePasw);
                            // connect role?
                            switch (profile.ProfileParam1)
                            {
                                case "integrated security":
                                    this._cbOrcl_Priv.SelectedIndex = 1;
                                    break;
                                default:
                                case "normal":
                                    this._cbOrcl_Priv.SelectedIndex = 0;
                                    break;
                            }
                            this._tcDbType.SelectedTab = this._tpOrcl;
                            break;
                        case DBType.MySql:
                            this._tcDbType.SelectedTab = this._tpMySql;
                            this._tbMySql_Usr.Text = profile.ProfileUser;
                            this._tbMySql_Host.Text = profile.ProfileTarget;
                            this._cbMySql_Encoding.Text = profile.Encoding;
                            this._tbMySql_Pasw.Text = NyxCrypt.DecryptPasw(profile.ProfilePasw);
                            this._cbMySql_Db.Items.Add(profile.ProfileDB);
                            this._cbMySql_Db.SelectedItem = profile.ProfileDB;
                            break;
                        case DBType.MSSql:
                            this._tcDbType.SelectedTab = this._tpMSSql;
                            this._tbMSSql_Host.Text = profile.ProfileTarget;
                            this._cbMSSql_Db.Items.Add(profile.ProfileDB);
                            this._cbMSSql_Db.SelectedItem = profile.ProfileDB;
                            // sql server auth (user/pasw)
                            if (profile.ProfileParam1 == "SQL Server (User/Password)")
                            {
                                this._tbMSSql_Usr.Text = profile.ProfileUser;
                                this._tbMSSql_Pasw.Text = NyxCrypt.DecryptPasw(profile.ProfilePasw);
                                this._cbMSSql_Auth.SelectedItem = profile.ProfileDB;
                            }
                            else
                            {
                                this._tbMSSql_Usr.Enabled = false;
                                this._tbMSSql_Pasw.Enabled = false;
                                this._cbMSSql_Auth.SelectedItem = profile.ProfileDB;
                            }
                            break;
                        case DBType.PgSql:
                            this._tcDbType.SelectedTab = this._tpPGSql;
                            this._tbPGSql_Usr.Text = profile.ProfileUser;
                            this._tbPGSql_Host.Text = profile.ProfileTarget;
                            this._tbPGSql_Pasw.Text = NyxCrypt.DecryptPasw(profile.ProfilePasw);
                            this._tbPGSql_Db.Text = profile.ProfileDB;
                            break;
                        case DBType.Odbc:
                            this._tcDbType.SelectedTab = this._tpODBC;
                            this._tbODBC_Usr.Text = profile.ProfileUser;
                            this._tbODBC_Pasw.Text = NyxCrypt.DecryptPasw(profile.ProfilePasw);
                            this._tbODBC_Host.Text = profile.ProfileTarget;
                            this._cbODBC_Driver.SelectedItem = profile.ProfileDB;
                            break;
                    }
                    this._sS_lblStatus.Text = GuiHelper.GetResourceMsg("Profile_Loaded", new string[] { profile.ProfileName });
                }
            }
            // disable profile details
            else
            {
                ResetGui();

                this._pnlProfileDef.Enabled = false;
                this._btOk.Enabled = false;
                this._msCopyAs.Enabled = false;
                this._msDelete.Enabled = false;
                this._msTest.Enabled = false;
            }
        }
        private void _cbMSSql_Auth_SelectedIndexChanged(object sender, EventArgs e)
        {   // windows auth
            if (this._cbMSSql_Auth.SelectedIndex == 0)
            {
                this._tbMSSql_Usr.Enabled = false;
                this._tbMSSql_Pasw.Enabled = false;
                this._tbMSSql_Usr.BackColor = System.Drawing.SystemColors.Control;
                this._tbMSSql_Pasw.BackColor = System.Drawing.SystemColors.Control;
            }
            // user/pasw 
            else
            {
                this._tbMSSql_Usr.Enabled = true;
                this._tbMSSql_Pasw.Enabled = true;
                this._tbMSSql_Usr.BackColor = System.Drawing.Color.GhostWhite;
                this._tbMSSql_Pasw.BackColor = System.Drawing.Color.GhostWhite;
            }
        }
        private void _cbODBC_Driver_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._cbODBC_Driver.SelectedItem.ToString() == "IBM DB2 ODBC DRIVER")
            {
                this._lblODBC_AddStr.Visible = true;
                this._lblODBC_AddStr.Text = GuiHelper.GetResourceMsg("Profile_ODBCAddStr");
                this._tbODBC_AddStr.Visible = true;
            }
            else
            {
                this._lblODBC_AddStr.Visible = false;
                this._tbODBC_AddStr.Visible = false;
            }
        }
        private void _tbName_TextChanged(object sender, EventArgs e)
        {
            if (this._tbName.Text.Length > 0)
                this._msTest.Enabled = true;
            else
                this._msTest.Enabled = false;
        }

        private void ResetGui()
        {   // cleanup
            this._tbName.Text = String.Empty;
            this._tbDesc.Text = String.Empty;
            this._tbMSSql_Pasw.Text = String.Empty;
            this._tbMSSql_Host.Text = String.Empty;
            this._tbMSSql_Usr.Text = String.Empty;
            this._tbMySql_Host.Text = String.Empty;
            this._tbMySql_Pasw.Text = String.Empty;
            this._tbMySql_Usr.Text = String.Empty;
            this._tbODBC_Host.Text = String.Empty;
            this._tbODBC_Pasw.Text = String.Empty;
            this._tbODBC_Usr.Text = String.Empty;
            this._tbODBC_AddStr.Text = String.Empty;
            this._tbOrcl_Pasw.Text = String.Empty;
            this._tbOrcl_SID.Text = String.Empty;
            this._tbOrcl_Usr.Text = String.Empty;
            this._mtbOracle_InitLongFetchSize.Text = String.Empty;
            this._tbPGSql_Usr.Text = String.Empty;
            this._tbPGSql_Pasw.Text = String.Empty;
            this._tbPGSql_Host.Text = String.Empty;
            this._tbPGSql_Db.Text = String.Empty;
            this._cbMSSql_Auth.SelectedIndex = 0;
            this._cbMySql_Db.Items.Clear();
            this._cbMySql_Db.Text = String.Empty;
            this._cbMySql_Db.Items.Add(GuiHelper.GetResourceMsg("DatabasesRetrieveString"));
            this._cbMSSql_Db.Items.Clear();
            this._cbMSSql_Db.Items.Add(GuiHelper.GetResourceMsg("DatabasesRetrieveString"));
            this._cbMSSql_Db.Text = String.Empty;
            this._cbODBC_Driver.SelectedIndex = 0;
            this._cbOrcl_Priv.SelectedIndex = 0;
            this._tbAddStr.Text = String.Empty;
        }
        private void GetPrfName()
        {
            bool showInptBox = true;
            while (showInptBox)
            {   // get profilename
                string prfName = String.Empty;
                DialogResult dresInput = InputBox.ShowInputBox(GuiHelper.GetResourceMsg("Profile_Name"),
                                    GuiHelper.GetResourceMsg("Profile_NameInput"), ref prfName);
                if (dresInput == DialogResult.OK)
                {   // invalid name
                    if (prfName == GuiHelper.GetResourceMsg("Profile_NameInput"))
                    {
                        StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("Profile_NameInvalid"),
                            MessageBoxButtons.RetryCancel, MessageBoxIcon.Question, false);
                        if (strRD.DRes == NyxDialogResult.Cancel)
                        {
                            this._cbProfiles.SelectedIndex = 0;
                            showInptBox = false;
                        }
                    }
                    // allready here
                    else if (NyxMain.HTProfiles.ContainsKey(prfName))
                    {
                        StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("Profile_ProfileExist"),
                            MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning, false);
                        if (strRD.DRes == NyxDialogResult.Cancel)
                        {
                            this._cbProfiles.SelectedIndex = 0;
                            showInptBox = false;
                        }
                    }
                    else
                    {   // gui set
                        this._tbName.Text = prfName;
                        this._tbName.ReadOnly = false;
                        this._pnlProfileDef.Enabled = true;
                        this._sS_lblStatus.Text = GuiHelper.GetResourceMsg("Profile_NewProfile");
                        // menu
                        this._btOk.Enabled = true;
                        // everything done
                        showInptBox = false;
                    }
                }
                else
                    showInptBox = false;
            } 
        }
        private bool CheckInput()
        {
            // checkups
            if (this._tcDbType.SelectedTab == this._tpMySql)
            {
                if (this._tbMySql_Usr.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), MessageHandler.EnumMsgType.Warning);
                    this._tbMySql_Usr.Focus();
                    return false;
                }
                if (this._tbMySql_Host.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidHost"), MessageHandler.EnumMsgType.Warning);
                    this._tbMySql_Host.Focus();
                    return false;
                }
                if (this._cbMySql_Db.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidDb"), MessageHandler.EnumMsgType.Warning);
                    this._cbMySql_Db.Focus();
                    return false;
                }
            }
            else if (this._tcDbType.SelectedTab == this._tpMSSql)
            {
                if (this._cbMSSql_Auth.SelectedIndex == 1
                    && this._tbMSSql_Usr.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), MessageHandler.EnumMsgType.Warning);
                    this._tbMSSql_Usr.Focus();
                    return false;
                }
                if (this._tbMSSql_Host.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidHost"), MessageHandler.EnumMsgType.Warning);
                    this._tbMSSql_Host.Focus();
                    return false;
                }
                if (this._cbMSSql_Db.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidDb"), MessageHandler.EnumMsgType.Warning);
                    this._cbMSSql_Db.Focus();
                    return false;
                }
            }
            else if (this._tcDbType.SelectedTab == this._tpOrcl)
            {
                if (this._tbOrcl_Usr.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), MessageHandler.EnumMsgType.Warning);
                    this._tbOrcl_Usr.Focus();
                    return false;
                }
                if (this._tbOrcl_SID.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidSID"), MessageHandler.EnumMsgType.Warning);
                    this._tbOrcl_SID.Focus();
                    return false;
                }
            }
            else if (this._tcDbType.SelectedTab == this._tpPGSql)
            {
                if (this._tbPGSql_Usr.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), MessageHandler.EnumMsgType.Warning);
                    this._tbPGSql_Usr.Focus();
                    return false;
                }
                if (this._tbPGSql_Host.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidHost"), MessageHandler.EnumMsgType.Warning);
                    this._tbPGSql_Host.Focus();
                    return false;
                }
                if (this._tbPGSql_Db.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidDb"), MessageHandler.EnumMsgType.Warning);
                    this._tbPGSql_Db.Focus();
                    return false;
                }
            }
            else if (this._tcDbType.SelectedTab == this._tpODBC)
            {
                if (this._tbODBC_Usr.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), MessageHandler.EnumMsgType.Warning);
                    this._tbODBC_Usr.Focus();
                    return false;
                }
                if (this._cbODBC_Driver.SelectedIndex == 0)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("Prf_InvalidDriver"), MessageHandler.EnumMsgType.Warning);
                    this._cbODBC_Driver.Focus();
                    return false;
                }
            }
            return true;
        }
        private void SavePrf()
        {   // check input
            if (!CheckInput())
                return;

            // have we already got the name (count rows and check if select returns more than 1 row
            if (this._sS_lblStatus.Text == "New Profile")
            {
                if (NyxMain.HTProfiles.ContainsKey(this._tbName.Text))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnProfile_ProfileExist", new string[] { this._tbName.Text }), 
                        MessageHandler.EnumMsgType.Warning);
                    this._tbName.Focus();
                    return;
                }
            }

            // edit / insert hashtable config
            string prfname = this._tbName.Text;
            // create the profile setting class
            ProfileSettings setPrf = GetProfileSettings();
            setPrf.ProfileName = this._tbName.Text;
            setPrf.ProfileDescription = this._tbDesc.Text;
            setPrf.ProfileAddString = this._tbAddStr.Text;

            // add / edit hashtable and afterwards serialize
            NyxMain.HTProfiles[prfname] = setPrf;
            Settings.SerializeProfiles(NyxMain.HTProfiles, Settings.GetFolder(SettingsType.Profile));
            // messagebox
            RefreshProfile();
            this._pnlProfileDef.Enabled = false;
            this._sS_lblStatus.Text = GuiHelper.GetResourceMsg("Profile_Saved", new string[] { prfname });
            // fire event
            SettingsChangedEventArgs sae = new SettingsChangedEventArgs();
            sae.SettingsType = SettingsType.Profile;
            OnSettingsChanged(sae);
        }
        private void DeletePrf()
        {   // searching profile to delete
            string prfName = this._cbProfiles.SelectedItem.ToString();
            if (NyxMain.HTProfiles.ContainsKey(prfName))
            {
                StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("Profile_DeleteProfile", new string[] { prfName }),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                if (strRD.DRes == NyxDialogResult.Yes)
                {   // delete and update profiles.xml
                    NyxMain.HTProfiles.Remove(prfName);
                    if (Settings.SerializeProfiles(NyxMain.HTProfiles, Settings.GetFolder(SettingsType.Profile)))
                    {   // messagebox
                        RefreshProfile();
                        this._sS_lblStatus.Text = GuiHelper.GetResourceMsg("Profile_DeleteProfiled", new string[] { prfName });
                        // fire event
                        SettingsChangedEventArgs sae = new SettingsChangedEventArgs();
                        sae.SettingsType = SettingsType.Profile;
                        OnSettingsChanged(sae);
                    }
                    else
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrProfiles", new string[] { Settings.ErrExc.Message.ToString() }),
                            Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
                        this.Close();
                    }
                }
            }
        }
        private void ExitPrf()
        {
            this.Close();
        }
        private void RefreshProfile()
        {   // resetting to default
            this._cbProfiles.BeginUpdate();
            this._cbProfiles.Items.Clear();
            this._cbProfiles.Items.Add("Select Profile...");
            this._cbProfiles.SelectedIndex = 0;
            // adding profiles
            if (NyxMain.HTProfiles != null)
            {
                foreach (System.Collections.DictionaryEntry de in NyxMain.HTProfiles)
                    this._cbProfiles.Items.Add(de.Key);
            }
            this._cbProfiles.EndUpdate();
        }
        private void TestPrf()
        {   // check input
            if (!CheckInput())
                return;
            // some vars
            DBHelper dbh;
            // test connection
            if (this._tcDbType.SelectedTab == this._tpOrcl)
                dbh = new DBHelper(DBType.Oracle);
            else if (this._tcDbType.SelectedTab == this._tpMySql)
                dbh = new DBHelper(DBType.MySql);
             else if (this._tcDbType.SelectedTab == this._tpMSSql)
                dbh = new DBHelper(DBType.MSSql);
            else if (this._tcDbType.SelectedTab == this._tpPGSql)
                dbh = new DBHelper(DBType.PgSql);
            else if (this._tcDbType.SelectedTab == this._tpODBC)
                dbh = new DBHelper(DBType.Odbc);
            else
                return;
            // build profile
            ProfileSettings prfTmp = GetProfileSettings();
            if (dbh.ConnectDB(prfTmp, false))
            {
                dbh.DisconnectDB();
                this._sS_lblStatus.Text = GuiHelper.GetResourceMsg("Profile_TestProfile");
            }
            else
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrProfilesTest", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
        }

        private ProfileSettings GetProfileSettings()
        {
            ProfileSettings prf = new ProfileSettings();
            // set db-related parameter
            if (this._tcDbType.SelectedTab == this._tpMySql)
            {
                prf.ProfileDBType = DBType.MySql;
                prf.ProfileUser = this._tbMySql_Usr.Text;
                prf.ProfilePasw = NyxCrypt.EncryptPasw(this._tbMySql_Pasw.Text);
                prf.ProfileTarget = this._tbMySql_Host.Text;
                prf.ProfileDB = this._cbMySql_Db.Text;
                prf.Encoding = this._cbMySql_Encoding.Text;
            }
            else if (this._tcDbType.SelectedTab == this._tpOrcl)
            {
                prf.ProfileDBType = DBType.Oracle;
                prf.ProfileUser = this._tbOrcl_Usr.Text;
                prf.ProfilePasw = NyxCrypt.EncryptPasw(this._tbOrcl_Pasw.Text);
                prf.ProfileTarget = this._tbOrcl_SID.Text;
                prf.ProfileParam1 = this._cbOrcl_Priv.SelectedItem.ToString();
                prf.Encoding = this._cbOrcl_Encoding.Text;
                // check if we got an input
                if (this._mtbOracle_InitLongFetchSize.Text.Length > 0)
                {   // conv int
                    int itmp;
                    if (Int32.TryParse(this._mtbOracle_InitLongFetchSize.Text, out itmp))
                        prf.InitLongFetchSize = itmp;
                }
                else
                    prf.InitLongFetchSize = -1;

            }
            else if (this._tcDbType.SelectedTab == this._tpMSSql)
            {
                prf.ProfileDBType = DBType.MSSql;
                prf.ProfileUser = this._tbMSSql_Usr.Text;
                prf.ProfilePasw = NyxCrypt.EncryptPasw(this._tbMSSql_Pasw.Text);
                prf.ProfileTarget = this._tbMSSql_Host.Text;
                prf.ProfileDB = this._cbMSSql_Db.Text;
                prf.ProfileParam1 = this._cbMSSql_Auth.SelectedItem.ToString();
            }
            else if (this._tcDbType.SelectedTab == this._tpPGSql)
            {
                prf.ProfileDBType = DBType.PgSql;
                prf.ProfileUser = this._tbPGSql_Usr.Text;
                prf.ProfilePasw = NyxCrypt.EncryptPasw(this._tbPGSql_Pasw.Text);
                prf.ProfileTarget = this._tbPGSql_Host.Text;
                prf.ProfileDB = this._tbPGSql_Db.Text;
            }
            else if (this._tcDbType.SelectedTab == this._tpODBC)
            {
                prf.ProfileDBType = DBType.Odbc;
                prf.ProfileUser = this._tbODBC_Usr.Text;
                prf.ProfilePasw = NyxCrypt.EncryptPasw(this._tbODBC_Pasw.Text);
                prf.ProfileTarget = this._tbODBC_Host.Text;
                prf.ProfileDB = this._cbODBC_Driver.SelectedItem.ToString();
            }
            return prf;
        }
        
    }
}