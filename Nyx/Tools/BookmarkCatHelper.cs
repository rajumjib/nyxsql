/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx
{
    /// <summary>
    /// Represents the GUI form to create a <b>new</b> bookmark
    /// <i>under</i> a specific category.
    /// </summary>
    public partial class BookmarkCatHelper : Form
    {
        private static StructBookmark strBM;
        /// <summary> result bookmark struct </summary>
        public static StructBookmark StrBM { get { return strBM; } set { strBM = value; } }
        /// <summary> Initializes a new instance of the <see cref="BookmarkCatHelper"/> class. </summary>
        public BookmarkCatHelper()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Fades the window, if the user elected to do so in 
        /// the user settings.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void BookmarkCatHelper_Load(object sender, EventArgs e)
        {
            // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }
        /// <summary>
        /// Enables / Disables the OK button depending on whether or
        /// not the user has entered some value into the bookmark name field.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void _CheckInput(object sender, EventArgs e)
        {
            if (this._tbName.Text.Length > 0)
                this._btOk.Enabled = true;
            else
                this._btOk.Enabled = false;
        }

        /// <summary> Prompts and gets the <i>new</i> bookmark entry from the user </summary>
        /// <returns></returns>
        public static DialogResult GetCatEntry
        {
            get
            {
                // preset dialog
                BookmarkCatHelper bmch = new BookmarkCatHelper();
                // prepare cats
                bmch._cbCats.Items.Add("Select category...");
                bmch._cbCats.SelectedIndex = 0;
                // check if we got cats or not
                foreach (System.Data.DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + true + "'"))
                    bmch._cbCats.Items.Add(dr["name"].ToString());
                if (bmch._cbCats.Items.Count == 1)
                    bmch._cbCats.Enabled = false;
                // show dialog
                StructBookmark bookmark = new StructBookmark();
                DialogResult dRes = bmch.ShowDialog();
                if (dRes == DialogResult.OK)
                {   // set values
                    bookmark.Name = bmch._tbName.Text;
                    bookmark.IsCat = "true";
                    if (bmch._cbCats.SelectedIndex != 0)
                        bookmark.Category = bmch._cbCats.Text;
                    else
                        bookmark.Category = String.Empty;
                }
                // set bookmark
                strBM = bookmark;
                // final return
                return dRes;
            }
        }
        /// <summary> alter bookmark </summary>
        /// <param name="bookmark">bookmark alter</param>
        /// <returns></returns>
        public static DialogResult AlterCatEntry(StructBookmark bookmark)
        {   // preset dialog
            BookmarkCatHelper bmch = new BookmarkCatHelper();
            // prepare cats
            bmch._cbCats.Items.Add("Select category...");
            bmch._cbCats.SelectedIndex = 0;

            foreach (System.Data.DataRow dr in NyxMain.DTBookmarks.Select("iscat = '" + true + "'"))
                bmch._cbCats.Items.Add(dr["name"].ToString());
            // check if we got cats or not
            if (bmch._cbCats.Items.Count > 1)
            {   // allready below a category?
                if (bookmark.Category != null)
                    bmch._cbCats.SelectedItem = bookmark.Category;
            }
            // no cats defined
            else
                bmch._cbCats.Enabled = false;

            if (bookmark.Name != null && bookmark.Name.Length > 0)
                bmch._tbName.Text = bookmark.Name;
            // show dialog
            DialogResult dRes = bmch.ShowDialog();
            if (dRes == DialogResult.OK)
            {   // set values
                bookmark.Name = bmch._tbName.Text;
                bookmark.IsCat = "true";
                if (bmch._cbCats.SelectedIndex != 0)
                    bookmark.Category = bmch._cbCats.Text;
                else
                    bookmark.Category = String.Empty;
            }
            // set bookmark
            strBM = bookmark;
            // final return
            return dRes;
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}