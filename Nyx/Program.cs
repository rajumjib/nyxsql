/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;
using System.Security.Permissions;
using System.Runtime.CompilerServices;

// CLS compilance
[assembly: CLSCompliant(true)]
// security permissions
[assembly: IsolatedStorageFilePermission(SecurityAction.RequestMinimum, UserQuota = 1048576)]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, UnmanagedCode = true)]
[assembly: FileIOPermission(SecurityAction.RequestMinimum, Unrestricted = true)]
// 'hard binding' settings assembly
[assembly: DependencyAttribute("NyxSettings", LoadHint.Always)]
namespace Nyx
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {   // subscribe the threading unhandled exception handler, but only when not inside debug
            #if !DEBUG
            Classes.ThreadExceptionHandler thrEvntHdl = new Classes.ThreadExceptionHandler();
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(thrEvntHdl.Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(thrEvntHdl.Application_UnhandledException);
            #endif
            
            // start app
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new NyxMain());
        }
    }
}