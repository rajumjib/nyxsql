namespace Nyx
{
    /// <summary>
    /// SqlMultiTab UserClass
    /// </summary>
    partial class SqlTab
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {   // release dataset
                if (dsResult != null)
                {
                    dsResult.Clear();
                    dsResult = null;
                }
                // dispose gui
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SqlTab));
            this._tsSql = new System.Windows.Forms.ToolStrip();
            this._tsSql_sep5 = new System.Windows.Forms.ToolStripSeparator();
            this._tsSql_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tsSql_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsSql_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this._tsSql_Sep4 = new System.Windows.Forms.ToolStripSeparator();
            this._tsSql_lblLimit = new System.Windows.Forms.ToolStripLabel();
            this._tsSql_cbLimit = new System.Windows.Forms.ToolStripComboBox();
            this._tsSql_lblQryIdent = new System.Windows.Forms.ToolStripLabel();
            this._tsSql_cbQryIdent = new System.Windows.Forms.ToolStripComboBox();
            this._cmSqlDta = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmSqlDta_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmSqlDta_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._cmSqlQry = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmSqlQry_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._splitSql = new System.Windows.Forms.SplitContainer();
            this._rtbSqlQry = new System.Windows.Forms.RichTextBox();
            this._dgSql = new System.Windows.Forms.DataGridView();
            this._rtbErrorDsp = new System.Windows.Forms.RichTextBox();
            this._ilAutoComplete = new System.Windows.Forms.ImageList(this.components);
            this.SqlQry_CBCopy = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlQry_CBPaste = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlQry_CBCut = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlQry_LoadSql = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlQry_SaveSql = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDta_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDta_Update = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDtaCp = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDtaCp_Field = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDtaCp_Cells = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDtaCp_Table = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDtaSv = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDtaSv_Cells = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlDtaSv_Table = new System.Windows.Forms.ToolStripMenuItem();
            this._tsSql_NewTab = new System.Windows.Forms.ToolStripButton();
            this._tsSql_CloseTab = new System.Windows.Forms.ToolStripButton();
            this._tsSql_SqlBldr = new System.Windows.Forms.ToolStripButton();
            this._tsSql_ClearSql = new System.Windows.Forms.ToolStripButton();
            this._tsSql_Exec = new System.Windows.Forms.ToolStripButton();
            this._tsSql_ExecSel = new System.Windows.Forms.ToolStripButton();
            this._tsSql_EditData = new System.Windows.Forms.ToolStripButton();
            this._tsSql_UpdateData = new System.Windows.Forms.ToolStripButton();
            this._tsSql.SuspendLayout();
            this._cmSqlDta.SuspendLayout();
            this._cmSqlQry.SuspendLayout();
            this._splitSql.Panel1.SuspendLayout();
            this._splitSql.Panel2.SuspendLayout();
            this._splitSql.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgSql)).BeginInit();
            this.SuspendLayout();
            // 
            // _tsSql
            // 
            this._tsSql.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsSql.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsSql_NewTab,
            this._tsSql_CloseTab,
            this._tsSql_sep5,
            this._tsSql_SqlBldr,
            this._tsSql_Sep0,
            this._tsSql_ClearSql,
            this._tsSql_Sep1,
            this._tsSql_Exec,
            this._tsSql_ExecSel,
            this._tsSql_Sep2,
            this._tsSql_EditData,
            this._tsSql_UpdateData,
            this._tsSql_Sep4,
            this._tsSql_lblLimit,
            this._tsSql_cbLimit,
            this._tsSql_lblQryIdent,
            this._tsSql_cbQryIdent});
            resources.ApplyResources(this._tsSql, "_tsSql");
            this._tsSql.Name = "_tsSql";
            // 
            // _tsSql_sep5
            // 
            this._tsSql_sep5.Name = "_tsSql_sep5";
            resources.ApplyResources(this._tsSql_sep5, "_tsSql_sep5");
            // 
            // _tsSql_Sep0
            // 
            this._tsSql_Sep0.Name = "_tsSql_Sep0";
            resources.ApplyResources(this._tsSql_Sep0, "_tsSql_Sep0");
            // 
            // _tsSql_Sep1
            // 
            this._tsSql_Sep1.Name = "_tsSql_Sep1";
            resources.ApplyResources(this._tsSql_Sep1, "_tsSql_Sep1");
            // 
            // _tsSql_Sep2
            // 
            this._tsSql_Sep2.Name = "_tsSql_Sep2";
            resources.ApplyResources(this._tsSql_Sep2, "_tsSql_Sep2");
            // 
            // _tsSql_Sep4
            // 
            this._tsSql_Sep4.Name = "_tsSql_Sep4";
            resources.ApplyResources(this._tsSql_Sep4, "_tsSql_Sep4");
            // 
            // _tsSql_lblLimit
            // 
            this._tsSql_lblLimit.Name = "_tsSql_lblLimit";
            resources.ApplyResources(this._tsSql_lblLimit, "_tsSql_lblLimit");
            // 
            // _tsSql_cbLimit
            // 
            this._tsSql_cbLimit.BackColor = System.Drawing.Color.GhostWhite;
            this._tsSql_cbLimit.Items.AddRange(new object[] {
            resources.GetString("_tsSql_cbLimit.Items"),
            resources.GetString("_tsSql_cbLimit.Items1"),
            resources.GetString("_tsSql_cbLimit.Items2"),
            resources.GetString("_tsSql_cbLimit.Items3"),
            resources.GetString("_tsSql_cbLimit.Items4"),
            resources.GetString("_tsSql_cbLimit.Items5"),
            resources.GetString("_tsSql_cbLimit.Items6"),
            resources.GetString("_tsSql_cbLimit.Items7")});
            this._tsSql_cbLimit.Name = "_tsSql_cbLimit";
            resources.ApplyResources(this._tsSql_cbLimit, "_tsSql_cbLimit");
            // 
            // _tsSql_lblQryIdent
            // 
            this._tsSql_lblQryIdent.Name = "_tsSql_lblQryIdent";
            resources.ApplyResources(this._tsSql_lblQryIdent, "_tsSql_lblQryIdent");
            // 
            // _tsSql_cbQryIdent
            // 
            this._tsSql_cbQryIdent.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tsSql_cbQryIdent.Items.AddRange(new object[] {
            resources.GetString("_tsSql_cbQryIdent.Items"),
            resources.GetString("_tsSql_cbQryIdent.Items1"),
            resources.GetString("_tsSql_cbQryIdent.Items2")});
            this._tsSql_cbQryIdent.Name = "_tsSql_cbQryIdent";
            resources.ApplyResources(this._tsSql_cbQryIdent, "_tsSql_cbQryIdent");
            // 
            // _cmSqlDta
            // 
            resources.ApplyResources(this._cmSqlDta, "_cmSqlDta");
            this._cmSqlDta.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmSqlDta_Sep0,
            this._cmSqlDta_Edit,
            this._cmSqlDta_Update,
            this._cmSqlDta_Sep1,
            this._cmSqlDtaCp,
            this._cmSqlDtaSv});
            this._cmSqlDta.Name = "_cm2";
            // 
            // _cmSqlDta_Sep0
            // 
            this._cmSqlDta_Sep0.Name = "_cmSqlDta_Sep0";
            resources.ApplyResources(this._cmSqlDta_Sep0, "_cmSqlDta_Sep0");
            // 
            // _cmSqlDta_Sep1
            // 
            this._cmSqlDta_Sep1.Name = "_cmSqlDta_Sep1";
            resources.ApplyResources(this._cmSqlDta_Sep1, "_cmSqlDta_Sep1");
            // 
            // _cmSqlQry
            // 
            this._cmSqlQry.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.SqlQry_CBCopy,
            this._cmSqlQry_CBPaste,
            this._cmSqlQry_CBCut,
            this._cmSqlQry_Sep0,
            this._cmSqlQry_LoadSql,
            this._cmSqlQry_SaveSql});
            this._cmSqlQry.Name = "_cm";
            resources.ApplyResources(this._cmSqlQry, "_cmSqlQry");
            // 
            // _cmSqlQry_Sep0
            // 
            this._cmSqlQry_Sep0.Name = "_cmSqlQry_Sep0";
            resources.ApplyResources(this._cmSqlQry_Sep0, "_cmSqlQry_Sep0");
            // 
            // _splitSql
            // 
            resources.ApplyResources(this._splitSql, "_splitSql");
            this._splitSql.Name = "_splitSql";
            // 
            // _splitSql.Panel1
            // 
            this._splitSql.Panel1.Controls.Add(this._rtbSqlQry);
            // 
            // _splitSql.Panel2
            // 
            this._splitSql.Panel2.Controls.Add(this._dgSql);
            this._splitSql.Panel2.Controls.Add(this._rtbErrorDsp);
            // 
            // _rtbSqlQry
            // 
            this._rtbSqlQry.AcceptsTab = true;
            this._rtbSqlQry.BackColor = System.Drawing.Color.GhostWhite;
            this._rtbSqlQry.ContextMenuStrip = this._cmSqlQry;
            this._rtbSqlQry.DetectUrls = false;
            resources.ApplyResources(this._rtbSqlQry, "_rtbSqlQry");
            this._rtbSqlQry.EnableAutoDragDrop = true;
            this._rtbSqlQry.HideSelection = false;
            this._rtbSqlQry.Name = "_rtbSqlQry";
            this._rtbSqlQry.SelectionChanged += new System.EventHandler(this._rtbSqlQry_SelectionChanged);
            this._rtbSqlQry.KeyDown += new System.Windows.Forms.KeyEventHandler(this._rtbSqlQry_KeyDown);
            // 
            // _dgSql
            // 
            this._dgSql.AllowUserToAddRows = false;
            this._dgSql.AllowUserToDeleteRows = false;
            this._dgSql.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle1.NullValue = "null";
            this._dgSql.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dgSql.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._dgSql.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._dgSql.BackgroundColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgSql.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._dgSql.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._dgSql.ContextMenuStrip = this._cmSqlDta;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgSql.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this._dgSql, "_dgSql");
            this._dgSql.Name = "_dgSql";
            this._dgSql.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgSql.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.NullValue = "null";
            this._dgSql.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this._dgSql.RowTemplate.Height = 23;
            this._dgSql.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._dg_DataError);
            // 
            // _rtbErrorDsp
            // 
            this._rtbErrorDsp.AutoWordSelection = true;
            this._rtbErrorDsp.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._rtbErrorDsp, "_rtbErrorDsp");
            this._rtbErrorDsp.Name = "_rtbErrorDsp";
            this._rtbErrorDsp.ReadOnly = true;
            // 
            // _ilAutoComplete
            // 
            this._ilAutoComplete.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_ilAutoComplete.ImageStream")));
            this._ilAutoComplete.TransparentColor = System.Drawing.Color.Transparent;
            this._ilAutoComplete.Images.SetKeyName(0, "template_source.png");
            this._ilAutoComplete.Images.SetKeyName(1, "loupe.png");
            this._ilAutoComplete.Images.SetKeyName(2, "gear.png");
            this._ilAutoComplete.Images.SetKeyName(3, "fonts.png");
            // 
            // SqlQry_CBCopy
            // 
            this.SqlQry_CBCopy.Image = global::Nyx.Properties.Resources.page_copy;
            resources.ApplyResources(this.SqlQry_CBCopy, "SqlQry_CBCopy");
            this.SqlQry_CBCopy.Name = "SqlQry_CBCopy";
            this.SqlQry_CBCopy.Click += new System.EventHandler(this._cmSqlQry_CBCopy_Click);
            // 
            // _cmSqlQry_CBPaste
            // 
            this._cmSqlQry_CBPaste.Image = global::Nyx.Properties.Resources.page_paste;
            resources.ApplyResources(this._cmSqlQry_CBPaste, "_cmSqlQry_CBPaste");
            this._cmSqlQry_CBPaste.Name = "_cmSqlQry_CBPaste";
            this._cmSqlQry_CBPaste.Click += new System.EventHandler(this._cmSqlQry_CBPaste_Click);
            // 
            // _cmSqlQry_CBCut
            // 
            this._cmSqlQry_CBCut.Image = global::Nyx.Properties.Resources.cut;
            resources.ApplyResources(this._cmSqlQry_CBCut, "_cmSqlQry_CBCut");
            this._cmSqlQry_CBCut.Name = "_cmSqlQry_CBCut";
            this._cmSqlQry_CBCut.Click += new System.EventHandler(this._cmSqlQry_CBCut_Click);
            // 
            // _cmSqlQry_LoadSql
            // 
            this._cmSqlQry_LoadSql.Image = global::Nyx.Properties.Resources.folder;
            resources.ApplyResources(this._cmSqlQry_LoadSql, "_cmSqlQry_LoadSql");
            this._cmSqlQry_LoadSql.Name = "_cmSqlQry_LoadSql";
            this._cmSqlQry_LoadSql.Click += new System.EventHandler(this._cmSqlQry_LoadSql_Click);
            // 
            // _cmSqlQry_SaveSql
            // 
            this._cmSqlQry_SaveSql.Image = global::Nyx.Properties.Resources.disk;
            resources.ApplyResources(this._cmSqlQry_SaveSql, "_cmSqlQry_SaveSql");
            this._cmSqlQry_SaveSql.Name = "_cmSqlQry_SaveSql";
            this._cmSqlQry_SaveSql.Click += new System.EventHandler(this._cmSqlQry_SaveSql_Click);
            // 
            // _cmSqlDta_Edit
            // 
            resources.ApplyResources(this._cmSqlDta_Edit, "_cmSqlDta_Edit");
            this._cmSqlDta_Edit.Image = global::Nyx.Properties.Resources.pencil;
            this._cmSqlDta_Edit.Name = "_cmSqlDta_Edit";
            // 
            // _cmSqlDta_Update
            // 
            resources.ApplyResources(this._cmSqlDta_Update, "_cmSqlDta_Update");
            this._cmSqlDta_Update.Image = global::Nyx.Properties.Resources.database_save;
            this._cmSqlDta_Update.Name = "_cmSqlDta_Update";
            // 
            // _cmSqlDtaCp
            // 
            this._cmSqlDtaCp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmSqlDtaCp_Field,
            this._cmSqlDtaCp_Cells,
            this._cmSqlDtaCp_Table});
            resources.ApplyResources(this._cmSqlDtaCp, "_cmSqlDtaCp");
            this._cmSqlDtaCp.Image = global::Nyx.Properties.Resources.page_copy;
            this._cmSqlDtaCp.Name = "_cmSqlDtaCp";
            // 
            // _cmSqlDtaCp_Field
            // 
            this._cmSqlDtaCp_Field.Name = "_cmSqlDtaCp_Field";
            resources.ApplyResources(this._cmSqlDtaCp_Field, "_cmSqlDtaCp_Field");
            this._cmSqlDtaCp_Field.Click += new System.EventHandler(this._cmSqlDtaCp_Field_Click);
            // 
            // _cmSqlDtaCp_Cells
            // 
            this._cmSqlDtaCp_Cells.Name = "_cmSqlDtaCp_Cells";
            resources.ApplyResources(this._cmSqlDtaCp_Cells, "_cmSqlDtaCp_Cells");
            this._cmSqlDtaCp_Cells.Click += new System.EventHandler(this._cmSqlDtaCp_Cells_Click);
            // 
            // _cmSqlDtaCp_Table
            // 
            this._cmSqlDtaCp_Table.Name = "_cmSqlDtaCp_Table";
            resources.ApplyResources(this._cmSqlDtaCp_Table, "_cmSqlDtaCp_Table");
            this._cmSqlDtaCp_Table.Click += new System.EventHandler(this._cmSqlDtaCp_Table_Click);
            // 
            // _cmSqlDtaSv
            // 
            this._cmSqlDtaSv.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmSqlDtaSv_Cells,
            this._cmSqlDtaSv_Table});
            resources.ApplyResources(this._cmSqlDtaSv, "_cmSqlDtaSv");
            this._cmSqlDtaSv.Image = global::Nyx.Properties.Resources.disk;
            this._cmSqlDtaSv.Name = "_cmSqlDtaSv";
            // 
            // _cmSqlDtaSv_Cells
            // 
            this._cmSqlDtaSv_Cells.Name = "_cmSqlDtaSv_Cells";
            resources.ApplyResources(this._cmSqlDtaSv_Cells, "_cmSqlDtaSv_Cells");
            this._cmSqlDtaSv_Cells.Click += new System.EventHandler(this._cmSqlDtaSv_Cells_Click);
            // 
            // _cmSqlDtaSv_Table
            // 
            this._cmSqlDtaSv_Table.Name = "_cmSqlDtaSv_Table";
            resources.ApplyResources(this._cmSqlDtaSv_Table, "_cmSqlDtaSv_Table");
            this._cmSqlDtaSv_Table.Click += new System.EventHandler(this._cmSqlDtaSv_Table_Click);
            // 
            // _tsSql_NewTab
            // 
            this._tsSql_NewTab.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsSql_NewTab.Image = global::Nyx.Properties.Resources.tab_add;
            resources.ApplyResources(this._tsSql_NewTab, "_tsSql_NewTab");
            this._tsSql_NewTab.Name = "_tsSql_NewTab";
            this._tsSql_NewTab.Click += new System.EventHandler(this._tsSql_NewTab_Click);
            // 
            // _tsSql_CloseTab
            // 
            this._tsSql_CloseTab.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsSql_CloseTab.Image = global::Nyx.Properties.Resources.tab_delete;
            resources.ApplyResources(this._tsSql_CloseTab, "_tsSql_CloseTab");
            this._tsSql_CloseTab.Name = "_tsSql_CloseTab";
            this._tsSql_CloseTab.Click += new System.EventHandler(this._tsSql_CloseTab_Click);
            // 
            // _tsSql_SqlBldr
            // 
            this._tsSql_SqlBldr.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsSql_SqlBldr, "_tsSql_SqlBldr");
            this._tsSql_SqlBldr.Image = global::Nyx.Properties.Resources.application_tile_horizontal;
            this._tsSql_SqlBldr.Name = "_tsSql_SqlBldr";
            this._tsSql_SqlBldr.Click += new System.EventHandler(this._tsSql_SqlBldr_Click);
            // 
            // _tsSql_ClearSql
            // 
            this._tsSql_ClearSql.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsSql_ClearSql.Image = global::Nyx.Properties.Resources.page_white;
            resources.ApplyResources(this._tsSql_ClearSql, "_tsSql_ClearSql");
            this._tsSql_ClearSql.Name = "_tsSql_ClearSql";
            this._tsSql_ClearSql.Click += new System.EventHandler(this._tsSql_Clear_Click);
            // 
            // _tsSql_Exec
            // 
            this._tsSql_Exec.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsSql_Exec.Image = global::Nyx.Properties.Resources.control_play_blue;
            resources.ApplyResources(this._tsSql_Exec, "_tsSql_Exec");
            this._tsSql_Exec.Name = "_tsSql_Exec";
            this._tsSql_Exec.Click += new System.EventHandler(this._tsSql_Exec_Click);
            // 
            // _tsSql_ExecSel
            // 
            this._tsSql_ExecSel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsSql_ExecSel, "_tsSql_ExecSel");
            this._tsSql_ExecSel.Image = global::Nyx.Properties.Resources.control_play;
            this._tsSql_ExecSel.Name = "_tsSql_ExecSel";
            this._tsSql_ExecSel.Click += new System.EventHandler(this._tsSql_ExecSel_Click);
            // 
            // _tsSql_EditData
            // 
            this._tsSql_EditData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsSql_EditData, "_tsSql_EditData");
            this._tsSql_EditData.Image = global::Nyx.Properties.Resources.database_edit;
            this._tsSql_EditData.Name = "_tsSql_EditData";
            this._tsSql_EditData.Click += new System.EventHandler(this._tsSql_EditData_Click);
            // 
            // _tsSql_UpdateData
            // 
            this._tsSql_UpdateData.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsSql_UpdateData, "_tsSql_UpdateData");
            this._tsSql_UpdateData.Image = global::Nyx.Properties.Resources.database_save;
            this._tsSql_UpdateData.Name = "_tsSql_UpdateData";
            this._tsSql_UpdateData.Click += new System.EventHandler(this._tsSql_UpdateData_Click);
            // 
            // SqlTab
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._splitSql);
            this.Controls.Add(this._tsSql);
            this.Name = "SqlTab";
            this.Enter += new System.EventHandler(this.SqlMultiTab_Enter);
            this._tsSql.ResumeLayout(false);
            this._tsSql.PerformLayout();
            this._cmSqlDta.ResumeLayout(false);
            this._cmSqlQry.ResumeLayout(false);
            this._splitSql.Panel1.ResumeLayout(false);
            this._splitSql.Panel2.ResumeLayout(false);
            this._splitSql.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dgSql)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip _tsSql;
        private System.Windows.Forms.ToolStripButton _tsSql_SqlBldr;
        private System.Windows.Forms.ToolStripSeparator _tsSql_Sep0;
        private System.Windows.Forms.ToolStripButton _tsSql_ClearSql;
        private System.Windows.Forms.ToolStripSeparator _tsSql_Sep1;
        private System.Windows.Forms.ToolStripButton _tsSql_Exec;
        private System.Windows.Forms.ToolStripButton _tsSql_ExecSel;
        private System.Windows.Forms.ToolStripSeparator _tsSql_Sep2;
        private System.Windows.Forms.ToolStripLabel _tsSql_lblLimit;
        private System.Windows.Forms.ToolStripComboBox _tsSql_cbLimit;
        private System.Windows.Forms.ToolStripLabel _tsSql_lblQryIdent;
        private System.Windows.Forms.ToolStripComboBox _tsSql_cbQryIdent;
        private System.Windows.Forms.ContextMenuStrip _cmSqlDta;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDtaSv;
        private System.Windows.Forms.ToolStripSeparator _cmSqlDta_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDta_Update;
        private System.Windows.Forms.ContextMenuStrip _cmSqlQry;
        private System.Windows.Forms.ToolStripMenuItem SqlQry_CBCopy;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlQry_CBPaste;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlQry_CBCut;
        private System.Windows.Forms.ToolStripSeparator _cmSqlQry_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlQry_LoadSql;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlQry_SaveSql;
        private System.Windows.Forms.ToolStripButton _tsSql_NewTab;
        private System.Windows.Forms.ToolStripButton _tsSql_CloseTab;
        private System.Windows.Forms.ToolStripSeparator _tsSql_sep5;
        private System.Windows.Forms.SplitContainer _splitSql;
        private System.Windows.Forms.RichTextBox _rtbSqlQry;
        private System.Windows.Forms.RichTextBox _rtbErrorDsp;
        private System.Windows.Forms.ImageList _ilAutoComplete;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDta_Edit;
        private System.Windows.Forms.ToolStripButton _tsSql_EditData;
        private System.Windows.Forms.ToolStripButton _tsSql_UpdateData;
        private System.Windows.Forms.ToolStripSeparator _tsSql_Sep4;
        private System.Windows.Forms.ToolStripSeparator _cmSqlDta_Sep1;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDtaCp;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDtaCp_Field;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDtaCp_Cells;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDtaCp_Table;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDtaSv_Cells;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlDtaSv_Table;
        private System.Windows.Forms.DataGridView _dgSql;
    }
}
