/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx.AppDialogs
{
    /// <summary> Return Dialog Struct </summary>
    public struct StructRetDialog
    {
        private NyxDialogResult dRes;
        private bool showAgain;
        /// <remarks> dialogresult </remarks>
        public NyxDialogResult DRes { get { return dRes; } set { dRes = value; } }
        /// <remarks> bool show window again </remarks>
        public bool ShowAgain { get { return showAgain; } set { showAgain = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode() 
        { 
            return (int)System.Math.Sqrt(dRes.GetHashCode() + showAgain.GetHashCode()); 
        }
        /// <summary> override default == </summary>
        public static bool operator ==(StructRetDialog struct1, StructRetDialog struct2)
        {
            if (struct1.showAgain == struct2.showAgain && struct1.dRes == struct2.dRes)
                return true; 
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(StructRetDialog struct1, StructRetDialog struct2) { return !(struct1 == struct2); }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is StructRetDialog)) 
                return false; 
            else
                return this == (StructRetDialog)obj;
        }
    }
    
    /// <summary> MessageDialog Class </summary>
    public partial class MessageDialog : Form
    {
        private void MessageDialog_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }
        /// <summary>
        /// Init
        /// </summary>
        /// <param name="msg">message</param>
        /// <param name="msgbuttons">messageboxbuttons</param>
        /// <param name="msgicon">messageboxicon</param>
        public MessageDialog(string msg, MessageBoxButtons msgbuttons, MessageBoxIcon msgicon)
        {
            InitializeComponent();
            // set gui
            switch (msgbuttons)
            {
                case MessageBoxButtons.OKCancel:
                    this._btYes.Text = GuiHelper.GetResourceMsg("MD_Ok");
                    this._btYes.DialogResult = DialogResult.OK;
                    this._btNo.Enabled = false;
                    this._btCancel.DialogResult = DialogResult.Cancel;
                    break;
                case MessageBoxButtons.YesNo:
                    this._btYes.DialogResult = DialogResult.Yes;
                    this._btNo.DialogResult = DialogResult.No;
                    this._btCancel.Enabled = false;
                    break;
                case MessageBoxButtons.YesNoCancel:
                    this._btYes.DialogResult = DialogResult.Yes;
                    this._btNo.DialogResult = DialogResult.No;
                    this._btCancel.DialogResult = DialogResult.Cancel;
                    break;
                case MessageBoxButtons.RetryCancel:
                    this._btYes.Visible = false;
                    this._btNo.Text = GuiHelper.GetResourceMsg("MD_Retry");
                    this._btNo.DialogResult = DialogResult.Retry;
                    this._btCancel.DialogResult = DialogResult.Cancel;
                    break;
            }
            switch (msgicon)
            {
                case MessageBoxIcon.Error:
                    this._pic1.Image = global::Nyx.Properties.Resources.dialog_error;
                    break;
                case MessageBoxIcon.Information:
                    this._pic1.Image = global::Nyx.Properties.Resources.messagebox_info;
                    break;
                case MessageBoxIcon.Question:
                    this._pic1.Image = global::Nyx.Properties.Resources.messagebox_info;
                    break;
                case MessageBoxIcon.Warning:
                    this._pic1.Image = global::Nyx.Properties.Resources.dialog_warning;
                    break;
                case MessageBoxIcon.None:
                    this._pic1.Visible = false;
                    break;
            }
            this._rtbMsg.Text = msg;
        }
        /// <summary>
        /// static Show Message Dialog
        /// </summary>
        /// <param name="msg">message</param>
        /// <param name="msgbuttons">messageboxbuttons</param>
        /// <param name="msgicon">messageboxicon</param>
        /// <param name="showagain">display show again checkbox</param>
        /// <returns></returns>
        public static StructRetDialog ShowMsgDialog(string msg, MessageBoxButtons msgbuttons, MessageBoxIcon msgicon, bool showagain)
        {   // init and show
            StructRetDialog retVal = new StructRetDialog();
            MessageDialog md = new MessageDialog(msg, msgbuttons, msgicon);
            if (!showagain)
                md._ckbShowAgain.Hide();
            // set return values
            DialogResult dRes = md.ShowDialog();
            switch (dRes)
            {
                case DialogResult.Cancel:
                    retVal.DRes = NyxDialogResult.Cancel;
                    break;
                case DialogResult.No:
                    retVal.DRes = NyxDialogResult.No;
                    break;
                case DialogResult.OK:
                case DialogResult.Yes:
                    retVal.DRes = NyxDialogResult.Yes;
                    break;
            }
            // return
            return retVal;
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}