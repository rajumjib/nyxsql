namespace Nyx.AppDialogs
{
    partial class ExportOptionsDia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ExportOptionsDia));
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._lblFormat = new System.Windows.Forms.Label();
            this._cbFormat = new System.Windows.Forms.ComboBox();
            this._lblEnc = new System.Windows.Forms.Label();
            this._tbEncapsulate = new System.Windows.Forms.TextBox();
            this._gbExport = new System.Windows.Forms.GroupBox();
            this._mtbWidth = new System.Windows.Forms.MaskedTextBox();
            this._lblColWidth = new System.Windows.Forms.Label();
            this._cbLinebreak = new System.Windows.Forms.ComboBox();
            this._lblLineBreak = new System.Windows.Forms.Label();
            this._gbExport.SuspendLayout();
            this.SuspendLayout();
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            // 
            // _btOk
            // 
            this._btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            // 
            // _lblFormat
            // 
            resources.ApplyResources(this._lblFormat, "_lblFormat");
            this._lblFormat.Name = "_lblFormat";
            // 
            // _cbFormat
            // 
            this._cbFormat.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFormat.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbFormat.FormattingEnabled = true;
            this._cbFormat.Items.AddRange(new object[] {
            resources.GetString("_cbFormat.Items"),
            resources.GetString("_cbFormat.Items1"),
            resources.GetString("_cbFormat.Items2"),
            resources.GetString("_cbFormat.Items3"),
            resources.GetString("_cbFormat.Items4"),
            resources.GetString("_cbFormat.Items5"),
            resources.GetString("_cbFormat.Items6")});
            resources.ApplyResources(this._cbFormat, "_cbFormat");
            this._cbFormat.Name = "_cbFormat";
            this._cbFormat.SelectedIndexChanged += new System.EventHandler(this._cbFormat_SelectedIndexChanged);
            // 
            // _lblEnc
            // 
            resources.ApplyResources(this._lblEnc, "_lblEnc");
            this._lblEnc.Name = "_lblEnc";
            // 
            // _tbEncapsulate
            // 
            resources.ApplyResources(this._tbEncapsulate, "_tbEncapsulate");
            this._tbEncapsulate.Name = "_tbEncapsulate";
            // 
            // _gbExport
            // 
            this._gbExport.Controls.Add(this._mtbWidth);
            this._gbExport.Controls.Add(this._lblColWidth);
            this._gbExport.Controls.Add(this._cbLinebreak);
            this._gbExport.Controls.Add(this._tbEncapsulate);
            this._gbExport.Controls.Add(this._lblEnc);
            this._gbExport.Controls.Add(this._lblLineBreak);
            this._gbExport.Controls.Add(this._lblFormat);
            this._gbExport.Controls.Add(this._cbFormat);
            resources.ApplyResources(this._gbExport, "_gbExport");
            this._gbExport.Name = "_gbExport";
            this._gbExport.TabStop = false;
            // 
            // _mtbWidth
            // 
            resources.ApplyResources(this._mtbWidth, "_mtbWidth");
            this._mtbWidth.Name = "_mtbWidth";
            // 
            // _lblColWidth
            // 
            resources.ApplyResources(this._lblColWidth, "_lblColWidth");
            this._lblColWidth.Name = "_lblColWidth";
            // 
            // _cbLinebreak
            // 
            this._cbLinebreak.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbLinebreak.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbLinebreak.FormattingEnabled = true;
            this._cbLinebreak.Items.AddRange(new object[] {
            resources.GetString("_cbLinebreak.Items"),
            resources.GetString("_cbLinebreak.Items1")});
            resources.ApplyResources(this._cbLinebreak, "_cbLinebreak");
            this._cbLinebreak.Name = "_cbLinebreak";
            // 
            // _lblLineBreak
            // 
            resources.ApplyResources(this._lblLineBreak, "_lblLineBreak");
            this._lblLineBreak.Name = "_lblLineBreak";
            // 
            // ExportOptionsDia
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._gbExport);
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "ExportOptionsDia";
            this.Load += new System.EventHandler(this.ExportOptionsDia_Load);
            this._gbExport.ResumeLayout(false);
            this._gbExport.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.Label _lblFormat;
        private System.Windows.Forms.ComboBox _cbFormat;
        private System.Windows.Forms.Label _lblEnc;
        private System.Windows.Forms.TextBox _tbEncapsulate;
        private System.Windows.Forms.GroupBox _gbExport;
        private System.Windows.Forms.Label _lblColWidth;
        private System.Windows.Forms.ComboBox _cbLinebreak;
        private System.Windows.Forms.Label _lblLineBreak;
        private System.Windows.Forms.MaskedTextBox _mtbWidth;
    }
}