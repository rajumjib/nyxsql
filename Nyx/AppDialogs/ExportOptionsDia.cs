/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx.AppDialogs
{
    /// <summary> export type enum </summary>
    public enum EnumExpType {
        /// <remarks> xml </remarks>
        Xml,
        /// <remarks> coma seperated values </remarks>
        Csv,
        /// <remarks> tab seperated values </remarks>
        Tsv,
        /// <remarks> semicolon seperated </remarks>
        Semicolon,
        /// <remarks> html </remarks>
        Html,
        /// <remarks> insert sql statement </remarks>
        InsertStm,
        /// <remarks> delete sql statement </remarks>
        DelStm
    }
    /// <summary> linebreak enum </summary>
    public enum EnumLineBreak
    {
        /// <remarks> column </remarks>
        Column,
        /// <remarks> row </remarks>
        Row
    }
    /// <summary> export options struct </summary>
    public struct StructExpOpt
    {   // private members
        private EnumExpType expType;
        private string encapsStr;
        private int columnWidth;
        private EnumLineBreak lineBreak;

        /// <remarks> export type </remarks>
        public EnumExpType ExpType { get { return expType; } set { expType = value; } }
        /// <remarks> encapuslate string </remarks>
        public string EncapsStr { get { return encapsStr; } set { encapsStr = value; } }
        /// <remarks> single column width </remarks>
        public int ColumnWidth { get { return columnWidth; } set { columnWidth = value; } }
        /// <remarks> line break </remarks>
        public EnumLineBreak LineBreak { get { return lineBreak; } set { lineBreak = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode() 
        { 
            return (int)System.Math.Sqrt(expType.GetHashCode() + encapsStr.GetHashCode() + columnWidth.GetHashCode() + lineBreak.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(StructExpOpt struct1, StructExpOpt struct2)
        {
            if (struct1.expType == struct2.expType 
                && struct1.encapsStr == struct2.encapsStr
                && struct1.columnWidth == struct2.columnWidth 
                && struct1.lineBreak == struct2.lineBreak)
                return true; 
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(StructExpOpt struct1, StructExpOpt struct2) { return !(struct1 == struct2); }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            if (!(obj is StructExpOpt)) 
                return false;
            else
                return this == (StructExpOpt)obj;
        }
    }
    /// <summary>
    /// ExportOptionsDialog
    /// </summary>
    public partial class ExportOptionsDia : Form
    {
        /// <summary> Init </summary>
        public ExportOptionsDia()
        {
            InitializeComponent();
            // select default
            this._cbFormat.SelectedItem = GuiHelper.GetResourceMsg("FileExtensionXML");
            this._cbLinebreak.SelectedItem = GuiHelper.GetResourceMsg("Column");
        }
        private void ExportOptionsDia_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }
        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
        /// <summary> get export format selection as etype </summary>
        public EnumExpType ExpType
        {
            get
            {
                switch (this._cbFormat.Text)
                {
                    default:
                    case "xml":
                        return EnumExpType.Xml;
                    case "csv":
                        return EnumExpType.Csv;
                    case "tsv":
                        return EnumExpType.Tsv;
                    case "html":
                        return EnumExpType.Html;
                    case "semicolon":
                        return EnumExpType.Semicolon;
                    case "sql insertstm":
                        return EnumExpType.InsertStm;
                    case "sql deletestm":
                        return EnumExpType.DelStm;
                }
            }
        }
        /// <summary> get linebreak selection as etype </summary>
        public EnumLineBreak Linebreak
        {
            get
            {
                switch (this._cbLinebreak.Text)
                {
                    case "column":
                        return EnumLineBreak.Column;
                    default:
                    case "row":
                        return EnumLineBreak.Row;
                }
            }
        }
        /// <summary> get column width as int </summary>
        public int ColWidth
        {
            get
            {
                int value = 0;
                Int32.TryParse(this._mtbWidth.Text, out value);
                return value;
            }
        }

        /// <summary>
        /// Show exportdialog and return the user input
        /// </summary>
        /// <param name="title">title to set</param>
        /// <param name="expOpt">export options which are returned</param>
        /// <returns>DialogResult.Ok/Cancel</returns>
        public static DialogResult ShowDialog(string title, ref StructExpOpt expOpt)
        {   // init
            expOpt = new StructExpOpt();
            ExportOptionsDia expdia = new ExportOptionsDia();
            // set title
            expdia.Text = title;
            // show dialog and get result
            DialogResult dres = expdia.ShowDialog();
            if (dres == DialogResult.OK)
            {
                expOpt.EncapsStr = expdia._tbEncapsulate.Text.ToString();
                expOpt.ExpType = expdia.ExpType;
                expOpt.LineBreak = expdia.Linebreak;
                expOpt.ColumnWidth = expdia.ColWidth;
            }
            // finally return
            return dres;
        }

        private void _cbFormat_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this._cbFormat.Text)
            {
                case "xml":
                    this._cbLinebreak.Enabled = true;
                    this._mtbWidth.Enabled = true;
                    this._tbEncapsulate.Enabled = true;
                    break;
                case "sql deletestm":
                case "sql insertstm":
                    this._cbLinebreak.Enabled = false;
                    this._mtbWidth.Enabled = false;
                    this._tbEncapsulate.Enabled = true;
                    break;
                case "html":
                    this._cbLinebreak.Enabled = false;
                    this._mtbWidth.Enabled = false;
                    this._tbEncapsulate.Enabled = false;
                    break;
                default:
                    this._cbLinebreak.Enabled = false;
                    this._mtbWidth.Enabled = true;
                    this._tbEncapsulate.Enabled = true;
                    break;
            }
        }
    }

}