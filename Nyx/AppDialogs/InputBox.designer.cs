namespace Nyx.AppDialogs
{
    /// <summary>
    /// Small input box dialog, which is customizable through parameters
    /// </summary>
    partial class InputBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(InputBox));
            this._gbInputbox = new System.Windows.Forms.GroupBox();
            this._tbValue = new System.Windows.Forms.TextBox();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._gbInputbox.SuspendLayout();
            this.SuspendLayout();
            // 
            // _gbInputbox
            // 
            this._gbInputbox.Controls.Add(this._tbValue);
            resources.ApplyResources(this._gbInputbox, "_gbInputbox");
            this._gbInputbox.Name = "_gbInputbox";
            this._gbInputbox.TabStop = false;
            // 
            // _tbValue
            // 
            this._tbValue.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbValue, "_tbValue");
            this._tbValue.Name = "_tbValue";
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            // 
            // _btOk
            // 
            this._btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            // 
            // InputBox
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Controls.Add(this._gbInputbox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "InputBox";
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.InputBox_Load);
            this._gbInputbox.ResumeLayout(false);
            this._gbInputbox.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _gbInputbox;
        private System.Windows.Forms.TextBox _tbValue;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;

    }
}