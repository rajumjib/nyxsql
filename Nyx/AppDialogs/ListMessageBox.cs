/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx.AppDialogs
{
    public partial class ListMessageBox : Form
    {
        /// <summary>
        /// Init
        /// </summary>
        public ListMessageBox()
        {
            InitializeComponent();
            this._tblLayout.RowStyles[2].Height = 0;
        }
        private void ListMessageBox_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }

        /// <summary>
        /// public ShowListMessageBox
        /// </summary>
        /// <param name="title">title</param>
        /// <param name="msg">message</param>
        /// <param name="items">string array items</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ShowListMessageBox(string title, string msg, string[] items)
        {
            ListMessageBox lmb = new ListMessageBox();
            lmb.Text += title;
            lmb._lbHeadline.Text = msg;
            if (items != null)
            {
                foreach (string item in items)
                    lmb._lvList.Items.Add(item);
            }

            // return result
            DialogResult dRes = lmb.ShowDialog();
            return dRes;
        }

        /// <summary>
        /// public ShowListMessageBox
        /// </summary>
        /// <param name="title">title</param>
        /// <param name="msg">message</param>
        /// <param name="items">string array items</param>
        /// <param name="tblAction">tableaction</param>
        /// <param name="ckboxState">checkboxstate</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ShowListMessageBox(string title, string msg, string[] items,
                                                        TableAction tblAction, ref bool ckboxState)
        {
            ListMessageBox lmb = new ListMessageBox();
            lmb.Text += title;
            lmb._lbHeadline.Text = msg;
            if (items != null)
            {
                foreach (string item in items)
                    lmb._lvList.Items.Add(item);
            }
            // shall we display additional checkbox
            switch (tblAction)
            {
                case TableAction.TableDrop:
                    lmb._ckbAddOption.Visible = true;
                    lmb._ckbAddOption.Text = GuiHelper.GetResourceMsg("LMB_Purge");
                    lmb._tblLayout.RowStyles[2].Height = 25;
                    break;
                case TableAction.TableAnalyze:
                    lmb._ckbAddOption.Visible = true;
                    lmb._ckbAddOption.Text = GuiHelper.GetResourceMsg("LMB_ComputeStat");
                    lmb._tblLayout.RowStyles[2].Height = 25;
                    break;
            }
            // return result
            DialogResult dRes = lmb.ShowDialog();
            ckboxState = lmb._ckbAddOption.Checked;
            return dRes;
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion       
    }
}