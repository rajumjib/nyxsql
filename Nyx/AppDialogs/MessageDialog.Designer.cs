namespace Nyx.AppDialogs
{
    partial class MessageDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageDialog));
            this._ckbShowAgain = new System.Windows.Forms.CheckBox();
            this._btYes = new System.Windows.Forms.Button();
            this._btNo = new System.Windows.Forms.Button();
            this._btCancel = new System.Windows.Forms.Button();
            this._rtbMsg = new System.Windows.Forms.RichTextBox();
            this._pic1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this._pic1)).BeginInit();
            this.SuspendLayout();
            // 
            // _ckbShowAgain
            // 
            resources.ApplyResources(this._ckbShowAgain, "_ckbShowAgain");
            this._ckbShowAgain.Checked = true;
            this._ckbShowAgain.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ckbShowAgain.Name = "_ckbShowAgain";
            this._ckbShowAgain.UseVisualStyleBackColor = true;
            // 
            // _btYes
            // 
            this._btYes.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this._btYes.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btYes, "_btYes");
            this._btYes.Name = "_btYes";
            this._btYes.UseVisualStyleBackColor = true;
            // 
            // _btNo
            // 
            this._btNo.DialogResult = System.Windows.Forms.DialogResult.No;
            this._btNo.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._btNo, "_btNo");
            this._btNo.Name = "_btNo";
            this._btNo.UseVisualStyleBackColor = true;
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            // 
            // _rtbMsg
            // 
            this._rtbMsg.BackColor = System.Drawing.SystemColors.Control;
            this._rtbMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this._rtbMsg, "_rtbMsg");
            this._rtbMsg.Name = "_rtbMsg";
            this._rtbMsg.ReadOnly = true;
            this._rtbMsg.TabStop = false;
            // 
            // _pic1
            // 
            this._pic1.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this._pic1, "_pic1");
            this._pic1.Name = "_pic1";
            this._pic1.TabStop = false;
            // 
            // MessageDialog
            // 
            this.AcceptButton = this._btYes;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._pic1);
            this.Controls.Add(this._rtbMsg);
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btNo);
            this.Controls.Add(this._btYes);
            this.Controls.Add(this._ckbShowAgain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MessageDialog";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Load += new System.EventHandler(this.MessageDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this._pic1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox _ckbShowAgain;
        private System.Windows.Forms.Button _btYes;
        private System.Windows.Forms.Button _btNo;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.RichTextBox _rtbMsg;
        private System.Windows.Forms.PictureBox _pic1;
    }
}