/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx.AppDialogs
{
    public partial class MessageHandler : Form
    {
        /// <summary> enum Messagetype </summary>
        public enum EnumMsgType {
            /// <remarks> informational </remarks>
            Information,
            /// <remarks> warning </remarks>
            Warning,
            /// <remarks> error </remarks>
            Error,
            /// <remarks> fatal program exception </remarks>
            Exception
        }
        /// <summary> internal vars </summary>
        private string message;
        private Exception exc;

        /// <summary>
        /// ShowDialog, static function which calls MessageHandler
        /// </summary>
        /// <param name="msg">message to display</param>
        /// <param name="msgType">message type</param>
        public static DialogResult ShowDialog(string msg, EnumMsgType msgType)
        {
            MessageHandler msgHdl = new MessageHandler(
                msg,
                msgType);
            DialogResult dRes = msgHdl.ShowDialog();
            return dRes;
        }
        /// <summary>
        /// ShowDialog, static function which calls MessageHandler
        /// </summary>
        /// <param name="exc">exception</param>
        /// <param name="msgType">message type</param>
        public static DialogResult ShowDialog(Exception exc, EnumMsgType msgType)
        {
            if (exc != null)
            {
                MessageHandler msgHdl = new MessageHandler(
                    exc.Message.ToString(),
                    exc,
                    msgType);
                DialogResult dRes = msgHdl.ShowDialog();
                return dRes;
            }
            else 
                return DialogResult.Cancel;
        }
        /// <summary>
        /// ShowDialog, static function which calls MessageHandler
        /// </summary>
        /// <param name="msg">message to display</param>
        /// <param name="exc">exception</param>
        /// <param name="msgType">message type</param>
        public static DialogResult ShowDialog(string msg, Exception exc, EnumMsgType msgType)
        {
            MessageHandler msgHdl = new MessageHandler(
                msg,
                exc,
                msgType);
            DialogResult dRes = msgHdl.ShowDialog();
            return dRes;
        }

        /// <summary>
        /// MessageHandler init Class
        /// </summary>
        /// <param name="messagetext">short message</param>
        /// <param name="messageType">message type</param>
        public MessageHandler(string messagetext, EnumMsgType messageType)
        {
            InitializeComponent();
            // init internal vars
            message = messagetext;
            // no details
            this._btExit.Enabled = false;
            // set further gui
            SetGUI(messageType);
        }
        /// <summary>
        /// MessageHandler init function
        /// </summary>
        /// <param name="messagetext">short message</param>
        /// <param name="exception">message source</param>
        /// <param name="messageType">message type</param>
        public MessageHandler(string messagetext, Exception exception, EnumMsgType messageType)
        {
            InitializeComponent();
            // set internal vars
            message = messagetext;
            exc = exception;
            // set gui
            SetGUI(messageType);
        }
        private void MessageHandler_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // set default message
            this._rtbMsg.Text = message;
            // if we got no exception, disable details
            if (exc != null)
                this._btDetails.Enabled = true;
            // bring to front and select
            this.BringToFront();
            this.Select();
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        private void SetGUI(EnumMsgType messageType)
        {
            switch (messageType)
            {
                case EnumMsgType.Information:
                    this._lbl_hl.Text = GuiHelper.GetResourceMsg("MH_Information");
                    this.Text += GuiHelper.GetResourceMsg("MH_Information");

                    this._btReport.Enabled = false;
                    this._btExit.Enabled = false;

                    this._pic1.Image = global::Nyx.Properties.Resources.messagebox_info;
                    break;
                case EnumMsgType.Warning:
                    this._lbl_hl.Text = GuiHelper.GetResourceMsg("MH_Warning");
                    this.Text += GuiHelper.GetResourceMsg("MH_Warning");

                    this._btReport.Enabled = false;
                    this._btExit.Enabled = false;

                    this._pic1.Image = global::Nyx.Properties.Resources.dialog_warning;
                    break;
                case EnumMsgType.Error:
                    this.Size = new System.Drawing.Size(482, 218);
                    this._lbl_hl.Text = GuiHelper.GetResourceMsg("MH_CritError");
                    this.Text += GuiHelper.GetResourceMsg("MH_CritError");

                    this._btContinue.Text = GuiHelper.GetResourceMsg("MH_Continue");

                    this._pic1.Image = global::Nyx.Properties.Resources.dialog_error;
                    break;
                case EnumMsgType.Exception:
                    this.Size = new System.Drawing.Size(482, 218);
                    this._lbl_hl.Text = GuiHelper.GetResourceMsg("MH_Exception");
                    this.Text += GuiHelper.GetResourceMsg("MH_Exception");

                    this._btContinue.Text = GuiHelper.GetResourceMsg("MH_Continue");
                    this._btExit.Enabled = true;
                    this._pic1.Image = global::Nyx.Properties.Resources.dialog_error;
                    break;
            }
        }
        private string GetDetMsg()
        {
            return String.Format(NyxMain.NyxCI, "{0}\n\nSource:\n{1}\n\nVersion: {2}",
                exc.ToString(), exc.Source, System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
        }

        private void _btReport_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start("https://nyx.sigterm.eu/");
        }
        private void _btCopyCB_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(GetDetMsg());
        }
        private void _btDetails_Click(object sender, EventArgs e)
        {
            if (exc != null)
            {

                if (this._rtbMsg.Text == message)
                    this._rtbMsg.Text = GetDetMsg();
                else
                    this._rtbMsg.Text = message;
            }
        }
    }
}