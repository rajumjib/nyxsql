namespace Nyx.AppDialogs
{
    /// <summary>
    /// ListMessageBox, for listing many objects inside a listbox
    /// </summary>
    partial class ListMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ListMessageBox));
            this._tblLayout = new System.Windows.Forms.TableLayoutPanel();
            this._btOk = new System.Windows.Forms.Button();
            this._lbHeadline = new System.Windows.Forms.Label();
            this._lvList = new System.Windows.Forms.ListView();
            this._ckbAddOption = new System.Windows.Forms.CheckBox();
            this._btCancel = new System.Windows.Forms.Button();
            this._tblLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tblLayout
            // 
            resources.ApplyResources(this._tblLayout, "_tblLayout");
            this._tblLayout.Controls.Add(this._btOk, 0, 3);
            this._tblLayout.Controls.Add(this._lbHeadline, 0, 0);
            this._tblLayout.Controls.Add(this._lvList, 0, 1);
            this._tblLayout.Controls.Add(this._ckbAddOption, 0, 2);
            this._tblLayout.Controls.Add(this._btCancel, 1, 3);
            this._tblLayout.Name = "_tblLayout";
            // 
            // _btOk
            // 
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            // 
            // _lbHeadline
            // 
            resources.ApplyResources(this._lbHeadline, "_lbHeadline");
            this._tblLayout.SetColumnSpan(this._lbHeadline, 2);
            this._lbHeadline.Name = "_lbHeadline";
            // 
            // _lvList
            // 
            this._tblLayout.SetColumnSpan(this._lvList, 2);
            resources.ApplyResources(this._lvList, "_lvList");
            this._lvList.Name = "_lvList";
            this._lvList.TabStop = false;
            this._lvList.UseCompatibleStateImageBehavior = false;
            this._lvList.View = System.Windows.Forms.View.List;
            // 
            // _ckbAddOption
            // 
            resources.ApplyResources(this._ckbAddOption, "_ckbAddOption");
            this._tblLayout.SetColumnSpan(this._ckbAddOption, 2);
            this._ckbAddOption.Name = "_ckbAddOption";
            this._ckbAddOption.UseVisualStyleBackColor = true;
            // 
            // _btCancel
            // 
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            // 
            // ListMessageBox
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._tblLayout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ListMessageBox";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Load += new System.EventHandler(this.ListMessageBox_Load);
            this._tblLayout.ResumeLayout(false);
            this._tblLayout.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel _tblLayout;
        private System.Windows.Forms.Label _lbHeadline;
        private System.Windows.Forms.ListView _lvList;
        private System.Windows.Forms.CheckBox _ckbAddOption;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
    }
}