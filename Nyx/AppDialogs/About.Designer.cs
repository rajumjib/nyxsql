namespace Nyx.AppDialogs
{
    partial class About
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(About));
            this._picbox1 = new System.Windows.Forms.PictureBox();
            this._lblApp = new System.Windows.Forms.Label();
            this._tblLayoutPnl = new System.Windows.Forms.TableLayoutPanel();
            this._lbllnkHP = new System.Windows.Forms.LinkLabel();
            this._tbAbout = new System.Windows.Forms.TextBox();
            this._btClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this._picbox1)).BeginInit();
            this._tblLayoutPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _picbox1
            // 
            this._tblLayoutPnl.SetColumnSpan(this._picbox1, 2);
            resources.ApplyResources(this._picbox1, "_picbox1");
            this._picbox1.ErrorImage = global::Nyx.Properties.Resources.delete;
            this._picbox1.Name = "_picbox1";
            this._picbox1.TabStop = false;
            // 
            // _lblApp
            // 
            resources.ApplyResources(this._lblApp, "_lblApp");
            this._lblApp.Name = "_lblApp";
            // 
            // _tblLayoutPnl
            // 
            resources.ApplyResources(this._tblLayoutPnl, "_tblLayoutPnl");
            this._tblLayoutPnl.Controls.Add(this._picbox1, 0, 0);
            this._tblLayoutPnl.Controls.Add(this._lblApp, 0, 1);
            this._tblLayoutPnl.Controls.Add(this._lbllnkHP, 1, 1);
            this._tblLayoutPnl.Controls.Add(this._tbAbout, 0, 2);
            this._tblLayoutPnl.Controls.Add(this._btClose, 1, 3);
            this._tblLayoutPnl.Name = "_tblLayoutPnl";
            // 
            // _lbllnkHP
            // 
            resources.ApplyResources(this._lbllnkHP, "_lbllnkHP");
            this._lbllnkHP.Name = "_lbllnkHP";
            this._lbllnkHP.TabStop = true;
            this._lbllnkHP.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this._lbllnkHP_LinkClicked);
            // 
            // _tbAbout
            // 
            this._tbAbout.BackColor = System.Drawing.SystemColors.Window;
            this._tblLayoutPnl.SetColumnSpan(this._tbAbout, 2);
            this._tbAbout.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this._tbAbout, "_tbAbout");
            this._tbAbout.Name = "_tbAbout";
            this._tbAbout.ReadOnly = true;
            this._tbAbout.TabStop = false;
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // About
            // 
            this.AcceptButton = this._btClose;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.CancelButton = this._btClose;
            this.Controls.Add(this._tblLayoutPnl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "About";
            this.Load += new System.EventHandler(this.About_Load);
            ((System.ComponentModel.ISupportInitialize)(this._picbox1)).EndInit();
            this._tblLayoutPnl.ResumeLayout(false);
            this._tblLayoutPnl.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox _picbox1;
        private System.Windows.Forms.Label _lblApp;
        private System.Windows.Forms.TableLayoutPanel _tblLayoutPnl;
        private System.Windows.Forms.LinkLabel _lbllnkHP;
        private System.Windows.Forms.TextBox _tbAbout;
        private System.Windows.Forms.Button _btClose;
    }
}