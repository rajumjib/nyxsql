namespace Nyx.AppDialogs
{
    /// <summary>
    /// MessageHandler
    ///   Used inside Nyx mostly instead of MessageBox.
    ///   Special functions, like report and copy to clipboard included
    /// </summary>
    partial class MessageHandler
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageHandler));
            this._pic1 = new System.Windows.Forms.PictureBox();
            this._lbl_hl = new System.Windows.Forms.Label();
            this._pnlTop = new System.Windows.Forms.Panel();
            this._btReport = new System.Windows.Forms.Button();
            this._btCopyCB = new System.Windows.Forms.Button();
            this._tblLayout = new System.Windows.Forms.TableLayoutPanel();
            this._rtbMsg = new System.Windows.Forms.RichTextBox();
            this._btContinue = new System.Windows.Forms.Button();
            this._btExit = new System.Windows.Forms.Button();
            this._btDetails = new System.Windows.Forms.Button();
            this._tt = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this._pic1)).BeginInit();
            this._pnlTop.SuspendLayout();
            this._tblLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // _pic1
            // 
            this._pic1.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this._pic1, "_pic1");
            this._pic1.Name = "_pic1";
            this._pic1.TabStop = false;
            // 
            // _lbl_hl
            // 
            resources.ApplyResources(this._lbl_hl, "_lbl_hl");
            this._lbl_hl.Name = "_lbl_hl";
            // 
            // _pnlTop
            // 
            this._pnlTop.BackColor = System.Drawing.Color.Transparent;
            this._pnlTop.Controls.Add(this._pic1);
            this._pnlTop.Controls.Add(this._lbl_hl);
            resources.ApplyResources(this._pnlTop, "_pnlTop");
            this._pnlTop.Name = "_pnlTop";
            // 
            // _btReport
            // 
            resources.ApplyResources(this._btReport, "_btReport");
            this._btReport.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btReport.Image = global::Nyx.Properties.Resources.world_link;
            this._btReport.Name = "_btReport";
            this._tt.SetToolTip(this._btReport, resources.GetString("_btReport.ToolTip"));
            this._btReport.UseVisualStyleBackColor = true;
            this._btReport.Click += new System.EventHandler(this._btReport_Click);
            // 
            // _btCopyCB
            // 
            resources.ApplyResources(this._btCopyCB, "_btCopyCB");
            this._btCopyCB.Image = global::Nyx.Properties.Resources.page_copy;
            this._btCopyCB.Name = "_btCopyCB";
            this._tt.SetToolTip(this._btCopyCB, resources.GetString("_btCopyCB.ToolTip"));
            this._btCopyCB.UseVisualStyleBackColor = true;
            this._btCopyCB.Click += new System.EventHandler(this._btCopyCB_Click);
            // 
            // _tblLayout
            // 
            resources.ApplyResources(this._tblLayout, "_tblLayout");
            this._tblLayout.Controls.Add(this._rtbMsg, 0, 0);
            this._tblLayout.Controls.Add(this._btCopyCB, 1, 1);
            this._tblLayout.Controls.Add(this._btReport, 0, 1);
            this._tblLayout.Controls.Add(this._btContinue, 4, 1);
            this._tblLayout.Controls.Add(this._btExit, 5, 1);
            this._tblLayout.Controls.Add(this._btDetails, 2, 1);
            this._tblLayout.Name = "_tblLayout";
            // 
            // _rtbMsg
            // 
            this._rtbMsg.BackColor = System.Drawing.SystemColors.Control;
            this._rtbMsg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._tblLayout.SetColumnSpan(this._rtbMsg, 6);
            resources.ApplyResources(this._rtbMsg, "_rtbMsg");
            this._rtbMsg.Name = "_rtbMsg";
            this._rtbMsg.ReadOnly = true;
            this._rtbMsg.TabStop = false;
            // 
            // _btContinue
            // 
            resources.ApplyResources(this._btContinue, "_btContinue");
            this._btContinue.DialogResult = System.Windows.Forms.DialogResult.Ignore;
            this._btContinue.Image = global::Nyx.Properties.Resources.control_play_blue;
            this._btContinue.Name = "_btContinue";
            this._btContinue.UseVisualStyleBackColor = true;
            // 
            // _btExit
            // 
            resources.ApplyResources(this._btExit, "_btExit");
            this._btExit.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btExit.Image = global::Nyx.Properties.Resources.delete;
            this._btExit.Name = "_btExit";
            this._btExit.UseVisualStyleBackColor = true;
            // 
            // _btDetails
            // 
            resources.ApplyResources(this._btDetails, "_btDetails");
            this._btDetails.Image = global::Nyx.Properties.Resources.magnifier;
            this._btDetails.Name = "_btDetails";
            this._tt.SetToolTip(this._btDetails, resources.GetString("_btDetails.ToolTip"));
            this._btDetails.UseVisualStyleBackColor = true;
            this._btDetails.Click += new System.EventHandler(this._btDetails_Click);
            // 
            // MessageHandler
            // 
            this.AcceptButton = this._btContinue;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btExit;
            this.Controls.Add(this._tblLayout);
            this.Controls.Add(this._pnlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "MessageHandler";
            this.Load += new System.EventHandler(this.MessageHandler_Load);
            ((System.ComponentModel.ISupportInitialize)(this._pic1)).EndInit();
            this._pnlTop.ResumeLayout(false);
            this._pnlTop.PerformLayout();
            this._tblLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox _pic1;
        private System.Windows.Forms.Label _lbl_hl;
        private System.Windows.Forms.Panel _pnlTop;
        private System.Windows.Forms.Button _btReport;
        private System.Windows.Forms.Button _btCopyCB;
        private System.Windows.Forms.TableLayoutPanel _tblLayout;
        private System.Windows.Forms.RichTextBox _rtbMsg;
        private System.Windows.Forms.ToolTip _tt;
        private System.Windows.Forms.Button _btContinue;
        private System.Windows.Forms.Button _btExit;
        private System.Windows.Forms.Button _btDetails;
    }
}