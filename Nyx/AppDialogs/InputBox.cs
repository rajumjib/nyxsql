/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx.AppDialogs
{
    public partial class InputBox : Form
    {
        /// <summary>
        /// Displays a small windows form (appeareance of the form is customizable through parameters)
        /// </summary>
        /// <param name="title">title to set</param>
        /// <param name="text">text to display into the textbox</param>
        public InputBox(string title, string text)
        {
            InitializeComponent();
            // set title
            this.Text = title;
            this._gbInputbox.Text = title;
            // set textbox
            this._tbValue.Text = text;
            this._tbValue.Select();
        }
        private void InputBox_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }

        /// <summary> Displays a small windows form (appeareance of the form is customizable through parameters) </summary>
        /// <param name="title">window title</param>
        /// <param name="text">text to display inside the textbox (will be automatically selected)</param>
        /// <param name="result">entered string (if DialogResult.Ok)</param>
        /// <returns>DialogResult</returns>
        public static DialogResult ShowInputBox(string title, string text, ref string result)
        {
            InputBox box = new InputBox(title, text);
            DialogResult dres = box.ShowDialog();
            // set referenced string
            if(dres == DialogResult.OK)
                result = box._tbValue.Text;
            else
                result = String.Empty;
            // return
            return dres;
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}