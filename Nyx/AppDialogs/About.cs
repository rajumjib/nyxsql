/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx.AppDialogs
{
    /// <summary>
    /// About form
    /// </summary>
    public partial class About : Form
    {
        /// <summary>
        /// Init
        /// </summary>
        public About()
        {
            InitializeComponent();
        }
        private void About_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();

            this._lblApp.Text += System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
        }
        private void _lbllnkHP_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("https://nyx.sigterm.eu/");
        }
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion


    }
}