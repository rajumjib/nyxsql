﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("NyxSql - devrls")]
[assembly: AssemblyDescription("NyxSql - yet another SqlCmd")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("NyxSql")]
[assembly: AssemblyProduct("NyxSql")]
[assembly: AssemblyCopyright("Copyright ©  2006 Dominik Schischma")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("f2683f88-275a-444d-8252-4fee55425f0b")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.5.2.293")]
[assembly: AssemblyFileVersion("0.5.2.293")]
[assembly: NeutralResourcesLanguageAttribute("en")]
