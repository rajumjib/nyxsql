namespace Nyx.NyxMySql
{
    /// <summary>
    /// MySql Create View GUI
    /// </summary>
    partial class MySqlCreateView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlCreateView));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this._lblName = new System.Windows.Forms.Label();
            this._tblLayout = new System.Windows.Forms.TableLayoutPanel();
            this._btOk = new System.Windows.Forms.Button();
            this._gbGeneral = new System.Windows.Forms.GroupBox();
            this._tbName = new System.Windows.Forms.TextBox();
            this._btCancel = new System.Windows.Forms.Button();
            this._tcMode = new System.Windows.Forms.TabControl();
            this._tpGUI = new System.Windows.Forms.TabPage();
            this._tblLayoutGUI = new System.Windows.Forms.TableLayoutPanel();
            this._btMoveDown = new System.Windows.Forms.Button();
            this._btMoveRight = new System.Windows.Forms.Button();
            this._trvTables = new System.Windows.Forms.TreeView();
            this._dgView = new System.Windows.Forms.DataGridView();
            this.vTable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vShowInView = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.vLOperator = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.vFilter = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.vValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._btMoveLeft = new System.Windows.Forms.Button();
            this._btMoveUp = new System.Windows.Forms.Button();
            this._tpQuery = new System.Windows.Forms.TabPage();
            this._rtbViewStm = new System.Windows.Forms.RichTextBox();
            this._tblLayout.SuspendLayout();
            this._gbGeneral.SuspendLayout();
            this._tcMode.SuspendLayout();
            this._tpGUI.SuspendLayout();
            this._tblLayoutGUI.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgView)).BeginInit();
            this._tpQuery.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lblName
            // 
            resources.ApplyResources(this._lblName, "_lblName");
            this._lblName.Name = "_lblName";
            // 
            // _tblLayout
            // 
            resources.ApplyResources(this._tblLayout, "_tblLayout");
            this._tblLayout.Controls.Add(this._btOk, 0, 2);
            this._tblLayout.Controls.Add(this._gbGeneral, 0, 0);
            this._tblLayout.Controls.Add(this._btCancel, 1, 2);
            this._tblLayout.Controls.Add(this._tcMode, 0, 1);
            this._tblLayout.Name = "_tblLayout";
            // 
            // _btOk
            // 
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btOk_Click);
            // 
            // _gbGeneral
            // 
            this._tblLayout.SetColumnSpan(this._gbGeneral, 2);
            this._gbGeneral.Controls.Add(this._tbName);
            this._gbGeneral.Controls.Add(this._lblName);
            resources.ApplyResources(this._gbGeneral, "_gbGeneral");
            this._gbGeneral.Name = "_gbGeneral";
            this._gbGeneral.TabStop = false;
            // 
            // _tbName
            // 
            resources.ApplyResources(this._tbName, "_tbName");
            this._tbName.Name = "_tbName";
            // 
            // _btCancel
            // 
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btCancel_Click);
            // 
            // _tcMode
            // 
            this._tblLayout.SetColumnSpan(this._tcMode, 2);
            this._tcMode.Controls.Add(this._tpGUI);
            this._tcMode.Controls.Add(this._tpQuery);
            resources.ApplyResources(this._tcMode, "_tcMode");
            this._tcMode.Name = "_tcMode";
            this._tcMode.SelectedIndex = 0;
            // 
            // _tpGUI
            // 
            this._tpGUI.Controls.Add(this._tblLayoutGUI);
            resources.ApplyResources(this._tpGUI, "_tpGUI");
            this._tpGUI.Name = "_tpGUI";
            this._tpGUI.UseVisualStyleBackColor = true;
            // 
            // _tblLayoutGUI
            // 
            resources.ApplyResources(this._tblLayoutGUI, "_tblLayoutGUI");
            this._tblLayoutGUI.Controls.Add(this._btMoveDown, 0, 3);
            this._tblLayoutGUI.Controls.Add(this._btMoveRight, 1, 0);
            this._tblLayoutGUI.Controls.Add(this._trvTables, 0, 0);
            this._tblLayoutGUI.Controls.Add(this._dgView, 2, 0);
            this._tblLayoutGUI.Controls.Add(this._btMoveLeft, 1, 1);
            this._tblLayoutGUI.Controls.Add(this._btMoveUp, 1, 2);
            this._tblLayoutGUI.Name = "_tblLayoutGUI";
            // 
            // _btMoveDown
            // 
            resources.ApplyResources(this._btMoveDown, "_btMoveDown");
            this._btMoveDown.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btMoveDown.Image = global::Nyx.Properties.Resources.arrow_down;
            this._btMoveDown.Name = "_btMoveDown";
            this._btMoveDown.UseVisualStyleBackColor = true;
            this._btMoveDown.Click += new System.EventHandler(this._btMoveDown_Click);
            // 
            // _btMoveRight
            // 
            resources.ApplyResources(this._btMoveRight, "_btMoveRight");
            this._btMoveRight.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btMoveRight.Image = global::Nyx.Properties.Resources.arrow_right;
            this._btMoveRight.Name = "_btMoveRight";
            this._btMoveRight.UseVisualStyleBackColor = true;
            this._btMoveRight.Click += new System.EventHandler(this._btMoveRight_Click);
            // 
            // _trvTables
            // 
            resources.ApplyResources(this._trvTables, "_trvTables");
            this._trvTables.HideSelection = false;
            this._trvTables.Name = "_trvTables";
            this._tblLayoutGUI.SetRowSpan(this._trvTables, 4);
            this._trvTables.DoubleClick += new System.EventHandler(this._btMoveRight_Click);
            this._trvTables.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this._trvTables_AfterExpand);
            // 
            // _dgView
            // 
            this._dgView.AllowUserToAddRows = false;
            this._dgView.AllowUserToDeleteRows = false;
            this._dgView.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this._dgView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dgView.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._dgView.BackgroundColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._dgView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.vTable,
            this.vField,
            this.vShowInView,
            this.vLOperator,
            this.vFilter,
            this.vValue,
            this.vGroup});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgView.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this._dgView, "_dgView");
            this._dgView.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this._dgView.Name = "_dgView";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgView.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this._tblLayoutGUI.SetRowSpan(this._dgView, 4);
            this._dgView.RowTemplate.Height = 20;
            this._dgView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dgView.CellValidating += new System.Windows.Forms.DataGridViewCellValidatingEventHandler(this._dgView_CellValidating);
            this._dgView.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this._dgView_CellEndEdit);
            // 
            // vTable
            // 
            resources.ApplyResources(this.vTable, "vTable");
            this.vTable.Name = "vTable";
            this.vTable.ReadOnly = true;
            // 
            // vField
            // 
            resources.ApplyResources(this.vField, "vField");
            this.vField.Name = "vField";
            this.vField.ReadOnly = true;
            // 
            // vShowInView
            // 
            resources.ApplyResources(this.vShowInView, "vShowInView");
            this.vShowInView.Name = "vShowInView";
            // 
            // vLOperator
            // 
            resources.ApplyResources(this.vLOperator, "vLOperator");
            this.vLOperator.Items.AddRange(new object[] {
            "",
            "AND",
            "OR",
            "XOR"});
            this.vLOperator.Name = "vLOperator";
            // 
            // vFilter
            // 
            resources.ApplyResources(this.vFilter, "vFilter");
            this.vFilter.Items.AddRange(new object[] {
            "",
            "=",
            "<>",
            "<",
            ">",
            "<=",
            ">=",
            "LIKE",
            "NOT LIKE",
            "NOT",
            "BETWEEN"});
            this.vFilter.Name = "vFilter";
            // 
            // vValue
            // 
            resources.ApplyResources(this.vValue, "vValue");
            this.vValue.Name = "vValue";
            // 
            // vGroup
            // 
            resources.ApplyResources(this.vGroup, "vGroup");
            this.vGroup.Name = "vGroup";
            // 
            // _btMoveLeft
            // 
            resources.ApplyResources(this._btMoveLeft, "_btMoveLeft");
            this._btMoveLeft.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btMoveLeft.Image = global::Nyx.Properties.Resources.arrow_left;
            this._btMoveLeft.Name = "_btMoveLeft";
            this._btMoveLeft.UseVisualStyleBackColor = true;
            this._btMoveLeft.Click += new System.EventHandler(this._btMoveLeft_Click);
            // 
            // _btMoveUp
            // 
            resources.ApplyResources(this._btMoveUp, "_btMoveUp");
            this._btMoveUp.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btMoveUp.Image = global::Nyx.Properties.Resources.arrow_up;
            this._btMoveUp.Name = "_btMoveUp";
            this._btMoveUp.UseVisualStyleBackColor = true;
            this._btMoveUp.Click += new System.EventHandler(this._btMoveUp_Click);
            // 
            // _tpQuery
            // 
            this._tpQuery.Controls.Add(this._rtbViewStm);
            resources.ApplyResources(this._tpQuery, "_tpQuery");
            this._tpQuery.Name = "_tpQuery";
            this._tpQuery.UseVisualStyleBackColor = true;
            // 
            // _rtbViewStm
            // 
            resources.ApplyResources(this._rtbViewStm, "_rtbViewStm");
            this._rtbViewStm.Name = "_rtbViewStm";
            // 
            // MySqlCreateView
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tblLayout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MySqlCreateView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MySqlCreateView_FormClosing);
            this.Load += new System.EventHandler(this.MySqlCreateView_Load);
            this._tblLayout.ResumeLayout(false);
            this._gbGeneral.ResumeLayout(false);
            this._gbGeneral.PerformLayout();
            this._tcMode.ResumeLayout(false);
            this._tpGUI.ResumeLayout(false);
            this._tblLayoutGUI.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dgView)).EndInit();
            this._tpQuery.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label _lblName;
        private System.Windows.Forms.TableLayoutPanel _tblLayout;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.TabControl _tcMode;
        private System.Windows.Forms.TabPage _tpGUI;
        private System.Windows.Forms.TabPage _tpQuery;
        private System.Windows.Forms.GroupBox _gbGeneral;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.TreeView _trvTables;
        private System.Windows.Forms.RichTextBox _rtbViewStm;
        private System.Windows.Forms.DataGridView _dgView;
        private System.Windows.Forms.TableLayoutPanel _tblLayoutGUI;
        private System.Windows.Forms.Button _btMoveRight;
        private System.Windows.Forms.Button _btMoveLeft;
        private System.Windows.Forms.TextBox _tbName;
        private System.Windows.Forms.Button _btMoveDown;
        private System.Windows.Forms.Button _btMoveUp;
        private System.Windows.Forms.DataGridViewTextBoxColumn vTable;
        private System.Windows.Forms.DataGridViewTextBoxColumn vField;
        private System.Windows.Forms.DataGridViewCheckBoxColumn vShowInView;
        private System.Windows.Forms.DataGridViewComboBoxColumn vLOperator;
        private System.Windows.Forms.DataGridViewComboBoxColumn vFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn vValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn vGroup;
    }
}