namespace Nyx.NyxMySql
{
    partial class MySqlCreateTableField
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlCreateTableField));
            this._gbField = new System.Windows.Forms.GroupBox();
            this._lblFldValues = new System.Windows.Forms.Label();
            this._tbFldValues = new System.Windows.Forms.TextBox();
            this._cbFldCharset = new System.Windows.Forms.ComboBox();
            this._lblFldCharset = new System.Windows.Forms.Label();
            this._tbFldComment = new System.Windows.Forms.TextBox();
            this._lblFldComment = new System.Windows.Forms.Label();
            this._lblFldDefault = new System.Windows.Forms.Label();
            this._tbFldDefault = new System.Windows.Forms.TextBox();
            this._lblFldNull = new System.Windows.Forms.Label();
            this._cbFldNull = new System.Windows.Forms.ComboBox();
            this._lblFldLength = new System.Windows.Forms.Label();
            this._mtbFldLength = new System.Windows.Forms.MaskedTextBox();
            this._lblFldName = new System.Windows.Forms.Label();
            this._lblFldCollate = new System.Windows.Forms.Label();
            this._lblFldType = new System.Windows.Forms.Label();
            this._tbFldName = new System.Windows.Forms.TextBox();
            this._cbFldType = new System.Windows.Forms.ComboBox();
            this._cbFldCollate = new System.Windows.Forms.ComboBox();
            this._ckbFldBinary = new System.Windows.Forms.CheckBox();
            this._ckbFldAutoInc = new System.Windows.Forms.CheckBox();
            this._ckbFldOnUpdate = new System.Windows.Forms.CheckBox();
            this._ckbFldZerofill = new System.Windows.Forms.CheckBox();
            this._ckbFldUnsigned = new System.Windows.Forms.CheckBox();
            this._lbl_fldpos = new System.Windows.Forms.Label();
            this._cbPosition = new System.Windows.Forms.ComboBox();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._gbField.SuspendLayout();
            this.SuspendLayout();
            // 
            // _gbField
            // 
            this._gbField.Controls.Add(this._lblFldValues);
            this._gbField.Controls.Add(this._tbFldValues);
            this._gbField.Controls.Add(this._cbFldCharset);
            this._gbField.Controls.Add(this._lblFldCharset);
            this._gbField.Controls.Add(this._tbFldComment);
            this._gbField.Controls.Add(this._lblFldComment);
            this._gbField.Controls.Add(this._lblFldDefault);
            this._gbField.Controls.Add(this._tbFldDefault);
            this._gbField.Controls.Add(this._lblFldNull);
            this._gbField.Controls.Add(this._cbFldNull);
            this._gbField.Controls.Add(this._lblFldLength);
            this._gbField.Controls.Add(this._mtbFldLength);
            this._gbField.Controls.Add(this._lblFldName);
            this._gbField.Controls.Add(this._lblFldCollate);
            this._gbField.Controls.Add(this._lblFldType);
            this._gbField.Controls.Add(this._tbFldName);
            this._gbField.Controls.Add(this._cbFldType);
            this._gbField.Controls.Add(this._cbFldCollate);
            this._gbField.Controls.Add(this._ckbFldBinary);
            this._gbField.Controls.Add(this._ckbFldAutoInc);
            this._gbField.Controls.Add(this._ckbFldOnUpdate);
            this._gbField.Controls.Add(this._ckbFldZerofill);
            this._gbField.Controls.Add(this._ckbFldUnsigned);
            this._gbField.Controls.Add(this._lbl_fldpos);
            this._gbField.Controls.Add(this._cbPosition);
            resources.ApplyResources(this._gbField, "_gbField");
            this._gbField.Name = "_gbField";
            this._gbField.TabStop = false;
            // 
            // _lblFldValues
            // 
            resources.ApplyResources(this._lblFldValues, "_lblFldValues");
            this._lblFldValues.Name = "_lblFldValues";
            // 
            // _tbFldValues
            // 
            resources.ApplyResources(this._tbFldValues, "_tbFldValues");
            this._tbFldValues.Name = "_tbFldValues";
            // 
            // _cbFldCharset
            // 
            this._cbFldCharset.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldCharset.FormattingEnabled = true;
            this._cbFldCharset.Items.AddRange(new object[] {
            resources.GetString("_cbFldCharset.Items")});
            resources.ApplyResources(this._cbFldCharset, "_cbFldCharset");
            this._cbFldCharset.Name = "_cbFldCharset";
            // 
            // _lblFldCharset
            // 
            resources.ApplyResources(this._lblFldCharset, "_lblFldCharset");
            this._lblFldCharset.Name = "_lblFldCharset";
            // 
            // _tbFldComment
            // 
            resources.ApplyResources(this._tbFldComment, "_tbFldComment");
            this._tbFldComment.Name = "_tbFldComment";
            // 
            // _lblFldComment
            // 
            resources.ApplyResources(this._lblFldComment, "_lblFldComment");
            this._lblFldComment.Name = "_lblFldComment";
            // 
            // _lblFldDefault
            // 
            resources.ApplyResources(this._lblFldDefault, "_lblFldDefault");
            this._lblFldDefault.Name = "_lblFldDefault";
            // 
            // _tbFldDefault
            // 
            resources.ApplyResources(this._tbFldDefault, "_tbFldDefault");
            this._tbFldDefault.Name = "_tbFldDefault";
            // 
            // _lblFldNull
            // 
            resources.ApplyResources(this._lblFldNull, "_lblFldNull");
            this._lblFldNull.Name = "_lblFldNull";
            // 
            // _cbFldNull
            // 
            this._cbFldNull.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldNull.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbFldNull.FormattingEnabled = true;
            this._cbFldNull.Items.AddRange(new object[] {
            resources.GetString("_cbFldNull.Items"),
            resources.GetString("_cbFldNull.Items1"),
            resources.GetString("_cbFldNull.Items2")});
            resources.ApplyResources(this._cbFldNull, "_cbFldNull");
            this._cbFldNull.Name = "_cbFldNull";
            // 
            // _lblFldLength
            // 
            resources.ApplyResources(this._lblFldLength, "_lblFldLength");
            this._lblFldLength.Name = "_lblFldLength";
            // 
            // _mtbFldLength
            // 
            this._mtbFldLength.HidePromptOnLeave = true;
            resources.ApplyResources(this._mtbFldLength, "_mtbFldLength");
            this._mtbFldLength.Name = "_mtbFldLength";
            // 
            // _lblFldName
            // 
            resources.ApplyResources(this._lblFldName, "_lblFldName");
            this._lblFldName.Name = "_lblFldName";
            // 
            // _lblFldCollate
            // 
            resources.ApplyResources(this._lblFldCollate, "_lblFldCollate");
            this._lblFldCollate.Name = "_lblFldCollate";
            // 
            // _lblFldType
            // 
            resources.ApplyResources(this._lblFldType, "_lblFldType");
            this._lblFldType.Name = "_lblFldType";
            // 
            // _tbFldName
            // 
            resources.ApplyResources(this._tbFldName, "_tbFldName");
            this._tbFldName.Name = "_tbFldName";
            this._tbFldName.TextChanged += new System.EventHandler(this._tbFldName_TextChanged);
            // 
            // _cbFldType
            // 
            this._cbFldType.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbFldType.FormattingEnabled = true;
            this._cbFldType.Items.AddRange(new object[] {
            resources.GetString("_cbFldType.Items"),
            resources.GetString("_cbFldType.Items1"),
            resources.GetString("_cbFldType.Items2"),
            resources.GetString("_cbFldType.Items3"),
            resources.GetString("_cbFldType.Items4"),
            resources.GetString("_cbFldType.Items5"),
            resources.GetString("_cbFldType.Items6"),
            resources.GetString("_cbFldType.Items7"),
            resources.GetString("_cbFldType.Items8"),
            resources.GetString("_cbFldType.Items9"),
            resources.GetString("_cbFldType.Items10"),
            resources.GetString("_cbFldType.Items11"),
            resources.GetString("_cbFldType.Items12"),
            resources.GetString("_cbFldType.Items13"),
            resources.GetString("_cbFldType.Items14"),
            resources.GetString("_cbFldType.Items15"),
            resources.GetString("_cbFldType.Items16"),
            resources.GetString("_cbFldType.Items17"),
            resources.GetString("_cbFldType.Items18"),
            resources.GetString("_cbFldType.Items19"),
            resources.GetString("_cbFldType.Items20"),
            resources.GetString("_cbFldType.Items21"),
            resources.GetString("_cbFldType.Items22"),
            resources.GetString("_cbFldType.Items23"),
            resources.GetString("_cbFldType.Items24"),
            resources.GetString("_cbFldType.Items25"),
            resources.GetString("_cbFldType.Items26"),
            resources.GetString("_cbFldType.Items27"),
            resources.GetString("_cbFldType.Items28"),
            resources.GetString("_cbFldType.Items29"),
            resources.GetString("_cbFldType.Items30"),
            resources.GetString("_cbFldType.Items31"),
            resources.GetString("_cbFldType.Items32")});
            resources.ApplyResources(this._cbFldType, "_cbFldType");
            this._cbFldType.Name = "_cbFldType";
            this._cbFldType.SelectedIndexChanged += new System.EventHandler(this._cbFieldType_SelectedIndexChanged);
            // 
            // _cbFldCollate
            // 
            this._cbFldCollate.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldCollate.FormattingEnabled = true;
            this._cbFldCollate.Items.AddRange(new object[] {
            resources.GetString("_cbFldCollate.Items")});
            resources.ApplyResources(this._cbFldCollate, "_cbFldCollate");
            this._cbFldCollate.Name = "_cbFldCollate";
            // 
            // _ckbFldBinary
            // 
            resources.ApplyResources(this._ckbFldBinary, "_ckbFldBinary");
            this._ckbFldBinary.Name = "_ckbFldBinary";
            this._ckbFldBinary.UseVisualStyleBackColor = true;
            // 
            // _ckbFldAutoInc
            // 
            resources.ApplyResources(this._ckbFldAutoInc, "_ckbFldAutoInc");
            this._ckbFldAutoInc.Name = "_ckbFldAutoInc";
            this._ckbFldAutoInc.UseVisualStyleBackColor = true;
            // 
            // _ckbFldOnUpdate
            // 
            resources.ApplyResources(this._ckbFldOnUpdate, "_ckbFldOnUpdate");
            this._ckbFldOnUpdate.Name = "_ckbFldOnUpdate";
            this._ckbFldOnUpdate.UseVisualStyleBackColor = true;
            // 
            // _ckbFldZerofill
            // 
            resources.ApplyResources(this._ckbFldZerofill, "_ckbFldZerofill");
            this._ckbFldZerofill.Name = "_ckbFldZerofill";
            this._ckbFldZerofill.UseVisualStyleBackColor = true;
            // 
            // _ckbFldUnsigned
            // 
            resources.ApplyResources(this._ckbFldUnsigned, "_ckbFldUnsigned");
            this._ckbFldUnsigned.Name = "_ckbFldUnsigned";
            this._ckbFldUnsigned.UseVisualStyleBackColor = true;
            // 
            // _lbl_fldpos
            // 
            resources.ApplyResources(this._lbl_fldpos, "_lbl_fldpos");
            this._lbl_fldpos.Name = "_lbl_fldpos";
            // 
            // _cbPosition
            // 
            this._cbPosition.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbPosition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbPosition.FormattingEnabled = true;
            resources.ApplyResources(this._cbPosition, "_cbPosition");
            this._cbPosition.Name = "_cbPosition";
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btCancel_Click);
            // 
            // _btOk
            // 
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btCreate_Click);
            // 
            // MySqlCreateTableField
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Controls.Add(this._gbField);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MySqlCreateTableField";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MySqlCreateTableField_FormClosing);
            this.Load += new System.EventHandler(this.MySqlCreateTableField_Load);
            this._gbField.ResumeLayout(false);
            this._gbField.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _gbField;
        private System.Windows.Forms.Label _lbl_fldpos;
        private System.Windows.Forms.ComboBox _cbPosition;
        private System.Windows.Forms.CheckBox _ckbFldBinary;
        private System.Windows.Forms.CheckBox _ckbFldAutoInc;
        private System.Windows.Forms.CheckBox _ckbFldOnUpdate;
        private System.Windows.Forms.CheckBox _ckbFldZerofill;
        private System.Windows.Forms.CheckBox _ckbFldUnsigned;
        private System.Windows.Forms.Label _lblFldValues;
        private System.Windows.Forms.TextBox _tbFldValues;
        private System.Windows.Forms.ComboBox _cbFldCharset;
        private System.Windows.Forms.Label _lblFldCharset;
        private System.Windows.Forms.TextBox _tbFldComment;
        private System.Windows.Forms.Label _lblFldComment;
        private System.Windows.Forms.Label _lblFldDefault;
        private System.Windows.Forms.TextBox _tbFldDefault;
        private System.Windows.Forms.Label _lblFldNull;
        private System.Windows.Forms.ComboBox _cbFldNull;
        private System.Windows.Forms.Label _lblFldLength;
        private System.Windows.Forms.MaskedTextBox _mtbFldLength;
        private System.Windows.Forms.Label _lblFldName;
        private System.Windows.Forms.Label _lblFldCollate;
        private System.Windows.Forms.Label _lblFldType;
        private System.Windows.Forms.TextBox _tbFldName;
        private System.Windows.Forms.ComboBox _cbFldType;
        private System.Windows.Forms.ComboBox _cbFldCollate;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
    }
}