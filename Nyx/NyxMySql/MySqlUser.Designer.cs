namespace Nyx.NyxMySql
{
    /// <summary>
    /// MySql UserEditor Menue
    /// </summary>
    partial class MySqlUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlUser));
            this._lv_user = new System.Windows.Forms.ListView();
            this.user = new System.Windows.Forms.ColumnHeader();
            this.host = new System.Windows.Forms.ColumnHeader();
            this.passw = new System.Windows.Forms.ColumnHeader();
            this.global = new System.Windows.Forms.ColumnHeader();
            this._ms = new System.Windows.Forms.MenuStrip();
            this._ms_new = new System.Windows.Forms.ToolStripMenuItem();
            this._ms_apply = new System.Windows.Forms.ToolStripMenuItem();
            this._ms_delete = new System.Windows.Forms.ToolStripMenuItem();
            this._ms_exit = new System.Windows.Forms.ToolStripMenuItem();
            this._gb_globalprivs = new System.Windows.Forms.GroupBox();
            this._lv_gprivs = new System.Windows.Forms.ListView();
            this.grants = new System.Windows.Forms.ColumnHeader();
            this._cmlv = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmlv_selall = new System.Windows.Forms.ToolStripMenuItem();
            this._cmlv_selnone = new System.Windows.Forms.ToolStripMenuItem();
            this._cmlv_invertsel = new System.Windows.Forms.ToolStripMenuItem();
            this._lv_dbprivs = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this._lv_dblist = new System.Windows.Forms.ListView();
            this.database = new System.Windows.Forms.ColumnHeader();
            this._lbl_user = new System.Windows.Forms.Label();
            this._lbl_host = new System.Windows.Forms.Label();
            this._lbl_passw = new System.Windows.Forms.Label();
            this._tb_user = new System.Windows.Forms.TextBox();
            this._tb_passw2 = new System.Windows.Forms.TextBox();
            this._tb_passw = new System.Windows.Forms.TextBox();
            this._tb_host = new System.Windows.Forms.TextBox();
            this._lbl_globalgrant = new System.Windows.Forms.Label();
            this._cb_privpreselection = new System.Windows.Forms.ComboBox();
            this._tabc = new System.Windows.Forms.TabControl();
            this.users = new System.Windows.Forms.TabPage();
            this._pnl_userleft = new System.Windows.Forms.Panel();
            this._ckb_grantopt = new System.Windows.Forms.CheckBox();
            this.dbprivs = new System.Windows.Forms.TabPage();
            this._lbl_dbsel = new System.Windows.Forms.Label();
            this._pnl_dbprivright = new System.Windows.Forms.Panel();
            this.tblprivs = new System.Windows.Forms.TabPage();
            this._lv_log = new System.Windows.Forms.ListView();
            this.logentry = new System.Windows.Forms.ColumnHeader();
            this._ms.SuspendLayout();
            this._gb_globalprivs.SuspendLayout();
            this._cmlv.SuspendLayout();
            this._tabc.SuspendLayout();
            this.users.SuspendLayout();
            this._pnl_userleft.SuspendLayout();
            this.dbprivs.SuspendLayout();
            this._pnl_dbprivright.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lv_user
            // 
            this._lv_user.BackColor = System.Drawing.Color.GhostWhite;
            this._lv_user.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.user,
            this.host,
            this.passw,
            this.global});
            resources.ApplyResources(this._lv_user, "_lv_user");
            this._lv_user.FullRowSelect = true;
            this._lv_user.GridLines = true;
            this._lv_user.HideSelection = false;
            this._lv_user.MultiSelect = false;
            this._lv_user.Name = "_lv_user";
            this._lv_user.UseCompatibleStateImageBehavior = false;
            this._lv_user.View = System.Windows.Forms.View.Details;
            this._lv_user.SelectedIndexChanged += new System.EventHandler(this._lv_user_SelectedIndexChanged);
            // 
            // user
            // 
            resources.ApplyResources(this.user, "user");
            // 
            // host
            // 
            resources.ApplyResources(this.host, "host");
            // 
            // passw
            // 
            resources.ApplyResources(this.passw, "passw");
            // 
            // _ms
            // 
            this._ms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ms_new,
            this._ms_apply,
            this._ms_delete,
            this._ms_exit});
            resources.ApplyResources(this._ms, "_ms");
            this._ms.Name = "_ms";
            // 
            // _ms_new
            // 
            this._ms_new.Image = global::Nyx.Properties.Resources.add;
            resources.ApplyResources(this._ms_new, "_ms_new");
            this._ms_new.Name = "_ms_new";
            this._ms_new.Click += new System.EventHandler(this._ms_new_Click);
            // 
            // _ms_apply
            // 
            this._ms_apply.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._ms_apply, "_ms_apply");
            this._ms_apply.Name = "_ms_apply";
            this._ms_apply.Click += new System.EventHandler(this._ms_apply_Click);
            // 
            // _ms_delete
            // 
            this._ms_delete.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._ms_delete, "_ms_delete");
            this._ms_delete.Name = "_ms_delete";
            this._ms_delete.Click += new System.EventHandler(this._ms_delete_Click);
            // 
            // _ms_exit
            // 
            this._ms_exit.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._ms_exit, "_ms_exit");
            this._ms_exit.Name = "_ms_exit";
            this._ms_exit.Click += new System.EventHandler(this._ms_exit_Click);
            // 
            // _gb_globalprivs
            // 
            this._gb_globalprivs.Controls.Add(this._lv_gprivs);
            resources.ApplyResources(this._gb_globalprivs, "_gb_globalprivs");
            this._gb_globalprivs.Name = "_gb_globalprivs";
            this._gb_globalprivs.TabStop = false;
            // 
            // _lv_gprivs
            // 
            this._lv_gprivs.BackColor = System.Drawing.Color.GhostWhite;
            this._lv_gprivs.CheckBoxes = true;
            this._lv_gprivs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.grants});
            this._lv_gprivs.ContextMenuStrip = this._cmlv;
            this._lv_gprivs.FullRowSelect = true;
            this._lv_gprivs.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this._lv_gprivs.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items1"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items2"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items3"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items4"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items5"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items6"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items7"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items8"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items9"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items10"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items11"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items12"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items13"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items14"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items15"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items16"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items17"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items18"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items19"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items20"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items21"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_gprivs.Items22")))});
            resources.ApplyResources(this._lv_gprivs, "_lv_gprivs");
            this._lv_gprivs.Name = "_lv_gprivs";
            this._lv_gprivs.UseCompatibleStateImageBehavior = false;
            this._lv_gprivs.View = System.Windows.Forms.View.List;
            this._lv_gprivs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this._lv_gg_ItemChecked);
            // 
            // grants
            // 
            resources.ApplyResources(this.grants, "grants");
            // 
            // _cmlv
            // 
            this._cmlv.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmlv_selall,
            this._cmlv_selnone,
            this._cmlv_invertsel});
            this._cmlv.Name = "_cmlv";
            resources.ApplyResources(this._cmlv, "_cmlv");
            // 
            // _cmlv_selall
            // 
            this._cmlv_selall.Image = global::Nyx.Properties.Resources.arrow_redo;
            resources.ApplyResources(this._cmlv_selall, "_cmlv_selall");
            this._cmlv_selall.Name = "_cmlv_selall";
            this._cmlv_selall.Click += new System.EventHandler(this._cmlv_selall_Click);
            // 
            // _cmlv_selnone
            // 
            this._cmlv_selnone.Image = global::Nyx.Properties.Resources.arrow_undo;
            resources.ApplyResources(this._cmlv_selnone, "_cmlv_selnone");
            this._cmlv_selnone.Name = "_cmlv_selnone";
            this._cmlv_selnone.Click += new System.EventHandler(this._cmlv_selnone_Click);
            // 
            // _cmlv_invertsel
            // 
            this._cmlv_invertsel.Image = global::Nyx.Properties.Resources.arrow_rotate_clockwise;
            resources.ApplyResources(this._cmlv_invertsel, "_cmlv_invertsel");
            this._cmlv_invertsel.Name = "_cmlv_invertsel";
            this._cmlv_invertsel.Click += new System.EventHandler(this._cmlv_invertsel_Click);
            // 
            // _lv_dbprivs
            // 
            this._lv_dbprivs.BackColor = System.Drawing.Color.GhostWhite;
            this._lv_dbprivs.CheckBoxes = true;
            this._lv_dbprivs.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this._lv_dbprivs.ContextMenuStrip = this._cmlv;
            resources.ApplyResources(this._lv_dbprivs, "_lv_dbprivs");
            this._lv_dbprivs.FullRowSelect = true;
            this._lv_dbprivs.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this._lv_dbprivs.Items.AddRange(new System.Windows.Forms.ListViewItem[] {
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items1"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items2"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items3"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items4"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items5"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items6"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items7"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items8"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items9"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items10"))),
            ((System.Windows.Forms.ListViewItem)(resources.GetObject("_lv_dbprivs.Items11")))});
            this._lv_dbprivs.Name = "_lv_dbprivs";
            this._lv_dbprivs.UseCompatibleStateImageBehavior = false;
            this._lv_dbprivs.View = System.Windows.Forms.View.Details;
            this._lv_dbprivs.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this._lv_dbprivs_ItemChecked);
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // _lv_dblist
            // 
            this._lv_dblist.BackColor = System.Drawing.Color.GhostWhite;
            this._lv_dblist.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.database});
            resources.ApplyResources(this._lv_dblist, "_lv_dblist");
            this._lv_dblist.FullRowSelect = true;
            this._lv_dblist.GridLines = true;
            this._lv_dblist.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this._lv_dblist.HideSelection = false;
            this._lv_dblist.MultiSelect = false;
            this._lv_dblist.Name = "_lv_dblist";
            this._lv_dblist.UseCompatibleStateImageBehavior = false;
            this._lv_dblist.View = System.Windows.Forms.View.List;
            this._lv_dblist.SelectedIndexChanged += new System.EventHandler(this._lv_dgd_SelectedIndexChanged);
            // 
            // database
            // 
            resources.ApplyResources(this.database, "database");
            // 
            // _lbl_user
            // 
            resources.ApplyResources(this._lbl_user, "_lbl_user");
            this._lbl_user.Name = "_lbl_user";
            // 
            // _lbl_host
            // 
            resources.ApplyResources(this._lbl_host, "_lbl_host");
            this._lbl_host.Name = "_lbl_host";
            // 
            // _lbl_passw
            // 
            resources.ApplyResources(this._lbl_passw, "_lbl_passw");
            this._lbl_passw.Name = "_lbl_passw";
            // 
            // _tb_user
            // 
            this._tb_user.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tb_user, "_tb_user");
            this._tb_user.Name = "_tb_user";
            // 
            // _tb_passw2
            // 
            this._tb_passw2.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tb_passw2, "_tb_passw2");
            this._tb_passw2.Name = "_tb_passw2";
            this._tb_passw2.TextChanged += new System.EventHandler(this._tb_passw2_TextChanged);
            // 
            // _tb_passw
            // 
            this._tb_passw.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tb_passw, "_tb_passw");
            this._tb_passw.Name = "_tb_passw";
            this._tb_passw.TextChanged += new System.EventHandler(this._tb_passw_TextChanged);
            // 
            // _tb_host
            // 
            this._tb_host.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tb_host, "_tb_host");
            this._tb_host.Name = "_tb_host";
            // 
            // _lbl_globalgrant
            // 
            resources.ApplyResources(this._lbl_globalgrant, "_lbl_globalgrant");
            this._lbl_globalgrant.Name = "_lbl_globalgrant";
            // 
            // _cb_privpreselection
            // 
            this._cb_privpreselection.BackColor = System.Drawing.Color.GhostWhite;
            this._cb_privpreselection.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            resources.ApplyResources(this._cb_privpreselection, "_cb_privpreselection");
            this._cb_privpreselection.FormattingEnabled = true;
            this._cb_privpreselection.Items.AddRange(new object[] {
            resources.GetString("_cb_privpreselection.Items"),
            resources.GetString("_cb_privpreselection.Items1"),
            resources.GetString("_cb_privpreselection.Items2")});
            this._cb_privpreselection.Name = "_cb_privpreselection";
            this._cb_privpreselection.SelectedIndexChanged += new System.EventHandler(this._cb_globalgrant_SelectedIndexChanged);
            // 
            // _tabc
            // 
            this._tabc.Controls.Add(this.users);
            this._tabc.Controls.Add(this.dbprivs);
            this._tabc.Controls.Add(this.tblprivs);
            resources.ApplyResources(this._tabc, "_tabc");
            this._tabc.Name = "_tabc";
            this._tabc.SelectedIndex = 0;
            this._tabc.SelectedIndexChanged += new System.EventHandler(this._tabc_SelectedIndexChanged);
            // 
            // users
            // 
            this.users.BackColor = System.Drawing.SystemColors.Control;
            this.users.Controls.Add(this._lv_user);
            this.users.Controls.Add(this._pnl_userleft);
            resources.ApplyResources(this.users, "users");
            this.users.Name = "users";
            // 
            // _pnl_userleft
            // 
            this._pnl_userleft.Controls.Add(this._ckb_grantopt);
            this._pnl_userleft.Controls.Add(this._lbl_host);
            this._pnl_userleft.Controls.Add(this._tb_user);
            this._pnl_userleft.Controls.Add(this._cb_privpreselection);
            this._pnl_userleft.Controls.Add(this._tb_passw2);
            this._pnl_userleft.Controls.Add(this._gb_globalprivs);
            this._pnl_userleft.Controls.Add(this._lbl_passw);
            this._pnl_userleft.Controls.Add(this._lbl_globalgrant);
            this._pnl_userleft.Controls.Add(this._tb_passw);
            this._pnl_userleft.Controls.Add(this._lbl_user);
            this._pnl_userleft.Controls.Add(this._tb_host);
            resources.ApplyResources(this._pnl_userleft, "_pnl_userleft");
            this._pnl_userleft.Name = "_pnl_userleft";
            // 
            // _ckb_grantopt
            // 
            resources.ApplyResources(this._ckb_grantopt, "_ckb_grantopt");
            this._ckb_grantopt.Name = "_ckb_grantopt";
            this._ckb_grantopt.UseVisualStyleBackColor = true;
            this._ckb_grantopt.CheckedChanged += new System.EventHandler(this._ckb_grantopt_CheckedChanged);
            // 
            // dbprivs
            // 
            this.dbprivs.BackColor = System.Drawing.SystemColors.Control;
            this.dbprivs.Controls.Add(this._lbl_dbsel);
            this.dbprivs.Controls.Add(this._lv_dblist);
            this.dbprivs.Controls.Add(this._pnl_dbprivright);
            resources.ApplyResources(this.dbprivs, "dbprivs");
            this.dbprivs.Name = "dbprivs";
            // 
            // _lbl_dbsel
            // 
            resources.ApplyResources(this._lbl_dbsel, "_lbl_dbsel");
            this._lbl_dbsel.Name = "_lbl_dbsel";
            // 
            // _pnl_dbprivright
            // 
            this._pnl_dbprivright.Controls.Add(this._lv_dbprivs);
            resources.ApplyResources(this._pnl_dbprivright, "_pnl_dbprivright");
            this._pnl_dbprivright.Name = "_pnl_dbprivright";
            // 
            // tblprivs
            // 
            this.tblprivs.BackColor = System.Drawing.SystemColors.Control;
            resources.ApplyResources(this.tblprivs, "tblprivs");
            this.tblprivs.Name = "tblprivs";
            // 
            // _lv_log
            // 
            this._lv_log.BackColor = System.Drawing.Color.GhostWhite;
            this._lv_log.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.logentry});
            resources.ApplyResources(this._lv_log, "_lv_log");
            this._lv_log.FullRowSelect = true;
            this._lv_log.GridLines = true;
            this._lv_log.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this._lv_log.Name = "_lv_log";
            this._lv_log.UseCompatibleStateImageBehavior = false;
            this._lv_log.View = System.Windows.Forms.View.Details;
            this._lv_log.DoubleClick += new System.EventHandler(this._lv_log_DoubleClick);
            // 
            // logentry
            // 
            resources.ApplyResources(this.logentry, "logentry");
            // 
            // MySqlUser
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._tabc);
            this.Controls.Add(this._lv_log);
            this.Controls.Add(this._ms);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MySqlUser";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmMySQLUser_FormClosing);
            this.Load += new System.EventHandler(this.FrmMySQLUser_Load);
            this._ms.ResumeLayout(false);
            this._ms.PerformLayout();
            this._gb_globalprivs.ResumeLayout(false);
            this._cmlv.ResumeLayout(false);
            this._tabc.ResumeLayout(false);
            this.users.ResumeLayout(false);
            this._pnl_userleft.ResumeLayout(false);
            this._pnl_userleft.PerformLayout();
            this.dbprivs.ResumeLayout(false);
            this.dbprivs.PerformLayout();
            this._pnl_dbprivright.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView _lv_user;
        private System.Windows.Forms.ColumnHeader user;
        private System.Windows.Forms.ColumnHeader host;
        private System.Windows.Forms.ColumnHeader passw;
        private System.Windows.Forms.MenuStrip _ms;
        private System.Windows.Forms.ToolStripMenuItem _ms_new;
        private System.Windows.Forms.ToolStripMenuItem _ms_delete;
        private System.Windows.Forms.ToolStripMenuItem _ms_exit;
        private System.Windows.Forms.GroupBox _gb_globalprivs;
        private System.Windows.Forms.ListView _lv_gprivs;
        private System.Windows.Forms.ColumnHeader grants;
        private System.Windows.Forms.ListView _lv_dbprivs;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView _lv_dblist;
        private System.Windows.Forms.ColumnHeader database;
        private System.Windows.Forms.Label _lbl_user;
        private System.Windows.Forms.Label _lbl_host;
        private System.Windows.Forms.Label _lbl_passw;
        private System.Windows.Forms.TextBox _tb_user;
        private System.Windows.Forms.TextBox _tb_passw2;
        private System.Windows.Forms.TextBox _tb_passw;
        private System.Windows.Forms.TextBox _tb_host;
        private System.Windows.Forms.ColumnHeader global;
        private System.Windows.Forms.Label _lbl_globalgrant;
        private System.Windows.Forms.ComboBox _cb_privpreselection;
        private System.Windows.Forms.TabControl _tabc;
        private System.Windows.Forms.TabPage users;
        private System.Windows.Forms.TabPage dbprivs;
        private System.Windows.Forms.TabPage tblprivs;
        private System.Windows.Forms.ToolStripMenuItem _ms_apply;
        private System.Windows.Forms.ListView _lv_log;
        private System.Windows.Forms.ColumnHeader logentry;
        private System.Windows.Forms.CheckBox _ckb_grantopt;
        private System.Windows.Forms.Label _lbl_dbsel;
        private System.Windows.Forms.Panel _pnl_userleft;
        private System.Windows.Forms.Panel _pnl_dbprivright;
        private System.Windows.Forms.ContextMenuStrip _cmlv;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_selall;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_selnone;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_invertsel;
    }
}