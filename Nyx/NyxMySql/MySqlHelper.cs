/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace Nyx.NyxMySql
{
    class MySqlHelper
    {
        /// <summary> private to prevent the compiler from creating a default constructor </summary>
        private MySqlHelper()
        {
        }

        /// <summary>
        /// build column definitions
        /// </summary>
        /// <param name="sFldName"></param>
        /// <param name="sFldType"></param>
        /// <param name="sFldLength"></param>
        /// <param name="sFldUnsigned"></param>
        /// <param name="sFldAutoInc"></param>
        /// <param name="sFldZerofill"></param>
        /// <param name="sFldCharset"></param>
        /// <param name="sFldCollate"></param>
        /// <param name="sFldBinary"></param>
        /// <param name="sFldOnUpdate"></param>
        /// <param name="sFldValues"></param>
        /// <returns></returns>
        public static string BuildColumenDefinition(string sFldName, string sFldType, string sFldLength,
                string sFldUnsigned, string sFldAutoInc, string sFldZerofill, string sFldCharset,
                string sFldCollate, string sFldBinary, string sFldOnUpdate, string sFldValues)
        {
            string coldef = null;
            switch (sFldType)
            {
                case "BIT":         // BIT          [(length)]
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    break;
                case "TINYINT":     // TINYINT	    [(length)] [UNSIGNED] [ZEROFILL]
                case "SMALLINT":    // SMALLINT     [(length)] [UNSIGNED] [ZEROFILL]
                case "MEDIUMINT":   // MEDIUMINT    [(length)] [UNSIGNED] [ZEROFILL]
                case "INT":         // INT          [(length)] [UNSIGNED] [ZEROFILL]
                case "INTEGER":     // INTEGER      [(length)] [UNSIGNED] [ZEROFILL]
                case "BIGINT":      // BIGINT       [(length)] [UNSIGNED] [ZEROFILL]
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    // unsigned
                    if (sFldUnsigned == "Y")
                        coldef = String.Format(NyxMain.NyxCI, "{0} UNSIGNED", coldef);
                    // auto_increment
                    if (sFldAutoInc == "Y")
                        coldef = String.Format(NyxMain.NyxCI, "{0} AUTO_INCREMENT", coldef);
                    // zerofill
                    if (sFldZerofill == "Y")
                        coldef = String.Format(NyxMain.NyxCI, "{0} ZEROFILL", coldef, "");
                    break;
                case "REAL":        // REAL         [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "DOUBLE":      // DOUBLE       [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "FLOAT":       // FLOAT        [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "DECIMAL":     // DECIMAL		(length,decimals) [UNSIGNED] [ZEROFILL]
                case "NUMERIC":     // NUMERIC 	    (length,decimals) [UNSIGNED] [ZEROFILL]
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    // unsigned
                    if (sFldUnsigned == "Y")
                        coldef = String.Format(NyxMain.NyxCI, "{0} UNSIGNED", coldef);
                    // zerofill
                    if (sFldZerofill == "Y")
                        coldef = String.Format(NyxMain.NyxCI, "{0} ZEROFILL", coldef);
                    break;
                case "TIMESTAMP":   // TIMESTAMP
                    // name, type
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1}", sFldName, sFldType);
                    // on update set current timestamp
                    if (sFldOnUpdate == "Y")
                        coldef = String.Format(NyxMain.NyxCI, "{0} ON UPDATE CURRENT_TIMESTAMP", coldef);
                    break;
                case "DATE":        // DATE
                case "TIME":        // TIME
                case "DATETIME":    // DATETIME
                case "YEAR":        // YEAR
                case "TINYBLOB":    // TINYBLOB
                case "BLOB":        // BLOB
                case "MEDIUMBLOB":  // MEDIUMBLOB
                case "LONGBLOB":    // LONGBLOB
                    // name, type
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1}", sFldName, sFldType);
                    break;
                case "CHAR":        // CHAR		    (length) [CHARACTER SET charset_name] [COLLATE collation_name] -fulltext
                case "VARCHAR":     // VARCHAR		(length) [CHARACTER SET charset_name] [COLLATE collation_name] -fulltext
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    // character set
                    if (sFldCharset.Length > 0)
                        coldef = String.Format(NyxMain.NyxCI, "{0} CHARACTER SET {1}", coldef, sFldCharset);
                    // collate
                    if (sFldCollate.Length > 0)
                        coldef = String.Format(NyxMain.NyxCI, "{0} COLLATE {1}", coldef, sFldCollate);
                    break;
                case "BINARY":      // BINARY		(length)
                case "VARBINARY":   // VARBINARY	(length)
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    break;
                case "TINYTEXT":    // TINYTEXT 	[BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext 
                case "TEXT":        // TEXT 		[BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                case "MEDIUMTEXT":  // MEDIUMTEXT   [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                case "LONGTEXT":    // LONGTEXT     [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                    // name, type
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1}", sFldName, sFldType);
                    // binary
                    if (sFldBinary == "Y")
                        coldef = String.Format(NyxMain.NyxCI, "{0} {1}", coldef, "BINARY");
                    // character set
                    if (sFldCharset.Length > 0)
                        coldef = String.Format(NyxMain.NyxCI, "{0} CHARACTER SET {1}", coldef, sFldCharset);
                    // collate
                    if (sFldCollate.Length > 0)
                        coldef = String.Format(NyxMain.NyxCI, "{0} COLLATE {1}", coldef, sFldCollate);
                    break;
                case "ENUM":        // ENUM        (value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
                case "SET":         // SET         (value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
                    // name, type
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1}", sFldName, sFldType);
                    // values
                    if (sFldValues.Length > 0)
                        coldef = String.Format(NyxMain.NyxCI, "{0} {1}", coldef, sFldValues);
                    // character set
                    if (sFldCharset.Length > 0)
                        coldef = String.Format(NyxMain.NyxCI, "{0} CHARACTER SET {1}", coldef, sFldCharset);
                    // collate
                    if (sFldCollate.Length > 0)
                        coldef = String.Format(NyxMain.NyxCI, "{0} COLLATE {1}", coldef, sFldCollate);
                    break;
            }
            return coldef;
        }
    }
}
