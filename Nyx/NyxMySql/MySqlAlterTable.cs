/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Nyx.Classes;
using Nyx.AppDialogs;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;

using System.IO;

namespace Nyx.NyxMySql
{
    public partial class MySqlAlterTable : Form
    {   /// <remarks> event which gets fired when a table was created and the dialog is closed </remarks>
        public event EventHandler<DBContentChangedEventArgs> DBContentChangedEvent;
        // internal
        private string table;
        private Classes.DBHelper dbh = new DBHelper(NyxSettings.DBType.MySql);

        /// <summary>
        /// MySql Alter general table attributes
        /// </summary>
        /// <param name="table">table to alter</param>
        public MySqlAlterTable(string table)
        {   // set var
            this.table = table;
            // init gui
            InitializeComponent();
            this._tbName.Text = table;
            foreach (string charset in dbh.Encodings)
            {
                this._cbConvert_Charset.Items.Add(charset);
                this._cbDefault_Charset.Items.Add(charset);
            }
            foreach (string collate in dbh.Collations)
            {
                this._cbConvert_Collate.Items.Add(collate);
                this._cbDefault_Collate.Items.Add(collate);
            }
            if (dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                // retrieving table fields
                Queue<TableDetails> tblDesc;
                Classes.DBManagement dbm = new DBManagement(NyxSettings.DBType.MySql);
                if (dbm.GetTableDetails(table, out tblDesc, dbh))
                {
                    foreach (TableDetails strTblDesc in tblDesc)
                        this._cbOrderBy.Items.Add(strTblDesc.Name);
                }
                dbh.DisconnectDB();
            }
            else
            {   // closing connection
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
        }
        private void MySqlAlterTable_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }
        private void MySqlAlterTable_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }

        private bool RenameTable()
        {
            if (table != this._tbName.Text)
            {
                string qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} RENAME TO {1}", table, this._tbName.Text);
                if (dbh.ConnectDB(NyxMain.ConProfile, false))
                {
                    if (dbh.ExecuteNonQuery(qry))
                        this.table = this._tbName.Text;
                    else
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrTableAction", new string[] { dbh.ErrExc.Message.ToString() }),
                                dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                        return false;
                    }

                    dbh.DisconnectDB();
                }
                else
                {   // closing connection
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                        dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    this.Close();
                }
            }
            return true;
        }
        private bool Keys()
        {
            if (this._cbKeys.Text.ToString().Length > 0)
            {
                string qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} {1} KEYS", table, this._cbKeys.SelectedItem.ToString());
                bool qryExec = dbh.ExecuteNonQuery(qry);
                dbh.DisconnectDB();
                if (!qryExec)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrTableAction", new string[] { dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
            }
            return true;
        }
        private bool DefaultCharset()
        {
            if (this._cbDefault_Charset.Text.ToString().Length > 0)
            {   // check charset
                string qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} CONVERT TO CHARACTER SET {1}", table, this._cbDefault_Charset.Text.ToString());
                // check collate
                if (this._cbDefault_Collate.Text.Length > 0)
                    qry = String.Format(NyxMain.NyxCI, "{0} COLLATE {1}", qry, this._cbDefault_Collate.Text.ToString());
                // execute query
                if (!dbh.ExecuteNonQuery(qry))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrTableAction", new string[] { dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
            }
            return true;
        }
        private bool ConvertCharset()
        {
            if (this._cbConvert_Charset.Text.ToString().Length > 0)
            {   // check charset
                string qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} CONVERT TO CHARACTER SET {1}", table, this._cbConvert_Charset.Text.ToString());
                // check collate
                if (this._cbConvert_Collate.Text.Length > 0)
                    qry = String.Format(NyxMain.NyxCI, "{0} COLLATE {1}", qry, this._cbConvert_Collate.Text.ToString());
                // execute query
                if (!dbh.ExecuteNonQuery(qry))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrTableAction", new string[] { dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
            }
            return true;
        }
        private bool Tablespace()
        {
            if (this._cbTablespace.Text.ToString().Length > 0)
            {
                string qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} {1} TABLESPACE", table, this._cbTablespace.SelectedItem.ToString());
                if (!dbh.ExecuteNonQuery(qry))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrTableAction", new string[] { dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
            }
            return true;
        }
        private bool OrderBy()
        {
            if (this._cbOrderBy.Text.ToString().Length > 0)
            {
                string qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} ORDER BY {1}", table, this._cbOrderBy.SelectedItem.ToString());
                if (!dbh.ExecuteNonQuery(qry))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrTableAction", new string[] { dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
            }
            return true;
        }

        private void _btOk_Click(object sender, EventArgs e)
        {   // local var
            bool queryok = true;
            // disable gui
            this._gbTable.Enabled = false;
            this._btOk.Enabled = false;
            this._btCancel.Enabled = false;
            // execute actions
            queryok = RenameTable();
            if (queryok)
                queryok = DefaultCharset();
            if (queryok)
                queryok = ConvertCharset();
            if (queryok)
                queryok = Keys();
            if (queryok)
                queryok = Tablespace();
            if (queryok)
                queryok = OrderBy();
            // close or activate gui again
            if (!queryok)
            {
                this._gbTable.Enabled = true;
                this._btOk.Enabled = true;
                this._btCancel.Enabled = true;
            }
            else
            {
                MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("TableAlterDone"), MessageBoxButtons.OK, MessageBoxIcon.Information, false);
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
        }
        private void _btCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            this.Close();
        }

        /// <summary>
        /// Fire OnDBContentChanged event
        /// </summary>
        protected void OnDBContentChanged()
        {   // fire event
            DBContentChangedEventArgs e = new DBContentChangedEventArgs();
            e.DBContent = DatabaseContent.Tables;

            if (DBContentChangedEvent != null)
                DBContentChangedEvent(this, e);
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}