namespace Nyx.NyxMySql
{
    /// <summary>
    /// MySql Create Table GUI
    /// </summary>
    partial class MySqlCreateTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlCreateTable));
            this._gbFields = new System.Windows.Forms.GroupBox();
            this._btFldEdit = new System.Windows.Forms.Button();
            this._ckbFldBinary = new System.Windows.Forms.CheckBox();
            this._lblFldValues = new System.Windows.Forms.Label();
            this._tbFldValues = new System.Windows.Forms.TextBox();
            this._cbFldCharset = new System.Windows.Forms.ComboBox();
            this._lblFldCharset = new System.Windows.Forms.Label();
            this._btFldRemove = new System.Windows.Forms.Button();
            this._btFldAdd = new System.Windows.Forms.Button();
            this._ckbFldAutoInc = new System.Windows.Forms.CheckBox();
            this._ckbFldOnUpdate = new System.Windows.Forms.CheckBox();
            this._ckbFldZerofill = new System.Windows.Forms.CheckBox();
            this._ckbFldUnsigned = new System.Windows.Forms.CheckBox();
            this._ckbFldFulltext = new System.Windows.Forms.CheckBox();
            this._lblFldIndex = new System.Windows.Forms.Label();
            this._cbFldIndex = new System.Windows.Forms.ComboBox();
            this._tbFldComment = new System.Windows.Forms.TextBox();
            this._lblFldComment = new System.Windows.Forms.Label();
            this._lblFldDefault = new System.Windows.Forms.Label();
            this._tbFldDefault = new System.Windows.Forms.TextBox();
            this._lblFldNull = new System.Windows.Forms.Label();
            this._cbFldNull = new System.Windows.Forms.ComboBox();
            this._lblFldLength = new System.Windows.Forms.Label();
            this._mtbFldLength = new System.Windows.Forms.MaskedTextBox();
            this._lblFldName = new System.Windows.Forms.Label();
            this._lblFldCollate = new System.Windows.Forms.Label();
            this._lblFldType = new System.Windows.Forms.Label();
            this._tbFldName = new System.Windows.Forms.TextBox();
            this._cbFldType = new System.Windows.Forms.ComboBox();
            this._cbFldCollate = new System.Windows.Forms.ComboBox();
            this._gbGeneral = new System.Windows.Forms.GroupBox();
            this._lblTblName = new System.Windows.Forms.Label();
            this._tbTblComment = new System.Windows.Forms.TextBox();
            this._lblTblCollate = new System.Windows.Forms.Label();
            this._lblTblComment = new System.Windows.Forms.Label();
            this._btFldMoveDown = new System.Windows.Forms.Button();
            this._lvlTblType = new System.Windows.Forms.Label();
            this._btFldMoveUp = new System.Windows.Forms.Button();
            this._tbTblName = new System.Windows.Forms.TextBox();
            this._cbTblType = new System.Windows.Forms.ComboBox();
            this._cbTblCollate = new System.Windows.Forms.ComboBox();
            this._tooltip = new System.Windows.Forms.ToolTip(this.components);
            this._dgFields = new System.Windows.Forms.DataGridView();
            this._dgvcFldIndex = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldLength = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldUnsigned = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldFulltext = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldAutoInc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldZerofill = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldOnUpdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldBinary = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldDefault = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldCharset = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldCollate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldValues = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._dgvcFldComment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._ms = new System.Windows.Forms.MenuStrip();
            this._msTbl = new System.Windows.Forms.ToolStripMenuItem();
            this._msTblCreate = new System.Windows.Forms.ToolStripMenuItem();
            this._msTbl_sep = new System.Windows.Forms.ToolStripSeparator();
            this._msTblClose = new System.Windows.Forms.ToolStripMenuItem();
            this._msFld = new System.Windows.Forms.ToolStripMenuItem();
            this._msFldAdd = new System.Windows.Forms.ToolStripMenuItem();
            this._msFldEdit = new System.Windows.Forms.ToolStripMenuItem();
            this._msFldRemove = new System.Windows.Forms.ToolStripMenuItem();
            this._msFld_Sep = new System.Windows.Forms.ToolStripSeparator();
            this._msFldFieldUp = new System.Windows.Forms.ToolStripMenuItem();
            this._msFldFieldDown = new System.Windows.Forms.ToolStripMenuItem();
            this._tblLayoutPnl = new System.Windows.Forms.TableLayoutPanel();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._gbFields.SuspendLayout();
            this._gbGeneral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgFields)).BeginInit();
            this._ms.SuspendLayout();
            this._tblLayoutPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _gbFields
            // 
            this._tblLayoutPnl.SetColumnSpan(this._gbFields, 2);
            this._gbFields.Controls.Add(this._btFldEdit);
            this._gbFields.Controls.Add(this._ckbFldBinary);
            this._gbFields.Controls.Add(this._lblFldValues);
            this._gbFields.Controls.Add(this._tbFldValues);
            this._gbFields.Controls.Add(this._cbFldCharset);
            this._gbFields.Controls.Add(this._lblFldCharset);
            this._gbFields.Controls.Add(this._btFldRemove);
            this._gbFields.Controls.Add(this._btFldAdd);
            this._gbFields.Controls.Add(this._ckbFldAutoInc);
            this._gbFields.Controls.Add(this._ckbFldOnUpdate);
            this._gbFields.Controls.Add(this._ckbFldZerofill);
            this._gbFields.Controls.Add(this._ckbFldUnsigned);
            this._gbFields.Controls.Add(this._ckbFldFulltext);
            this._gbFields.Controls.Add(this._lblFldIndex);
            this._gbFields.Controls.Add(this._cbFldIndex);
            this._gbFields.Controls.Add(this._tbFldComment);
            this._gbFields.Controls.Add(this._lblFldComment);
            this._gbFields.Controls.Add(this._lblFldDefault);
            this._gbFields.Controls.Add(this._tbFldDefault);
            this._gbFields.Controls.Add(this._lblFldNull);
            this._gbFields.Controls.Add(this._cbFldNull);
            this._gbFields.Controls.Add(this._lblFldLength);
            this._gbFields.Controls.Add(this._mtbFldLength);
            this._gbFields.Controls.Add(this._lblFldName);
            this._gbFields.Controls.Add(this._lblFldCollate);
            this._gbFields.Controls.Add(this._lblFldType);
            this._gbFields.Controls.Add(this._tbFldName);
            this._gbFields.Controls.Add(this._cbFldType);
            this._gbFields.Controls.Add(this._cbFldCollate);
            resources.ApplyResources(this._gbFields, "_gbFields");
            this._gbFields.Name = "_gbFields";
            this._gbFields.TabStop = false;
            // 
            // _btFldEdit
            // 
            resources.ApplyResources(this._btFldEdit, "_btFldEdit");
            this._btFldEdit.Image = global::Nyx.Properties.Resources.pencil;
            this._btFldEdit.Name = "_btFldEdit";
            this._tooltip.SetToolTip(this._btFldEdit, resources.GetString("_btFldEdit.ToolTip"));
            this._btFldEdit.UseVisualStyleBackColor = true;
            this._btFldEdit.Click += new System.EventHandler(this._FldEdit_Click);
            // 
            // _ckbFldBinary
            // 
            resources.ApplyResources(this._ckbFldBinary, "_ckbFldBinary");
            this._ckbFldBinary.Name = "_ckbFldBinary";
            this._ckbFldBinary.UseVisualStyleBackColor = true;
            // 
            // _lblFldValues
            // 
            resources.ApplyResources(this._lblFldValues, "_lblFldValues");
            this._lblFldValues.Name = "_lblFldValues";
            // 
            // _tbFldValues
            // 
            resources.ApplyResources(this._tbFldValues, "_tbFldValues");
            this._tbFldValues.Name = "_tbFldValues";
            this._tooltip.SetToolTip(this._tbFldValues, resources.GetString("_tbFldValues.ToolTip"));
            // 
            // _cbFldCharset
            // 
            this._cbFldCharset.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._cbFldCharset.FormattingEnabled = true;
            this._cbFldCharset.Items.AddRange(new object[] {
            resources.GetString("_cbFldCharset.Items")});
            resources.ApplyResources(this._cbFldCharset, "_cbFldCharset");
            this._cbFldCharset.Name = "_cbFldCharset";
            // 
            // _lblFldCharset
            // 
            resources.ApplyResources(this._lblFldCharset, "_lblFldCharset");
            this._lblFldCharset.Name = "_lblFldCharset";
            // 
            // _btFldRemove
            // 
            resources.ApplyResources(this._btFldRemove, "_btFldRemove");
            this._btFldRemove.Image = global::Nyx.Properties.Resources.delete;
            this._btFldRemove.Name = "_btFldRemove";
            this._tooltip.SetToolTip(this._btFldRemove, resources.GetString("_btFldRemove.ToolTip"));
            this._btFldRemove.UseVisualStyleBackColor = true;
            this._btFldRemove.Click += new System.EventHandler(this._FldRemove_Click);
            // 
            // _btFldAdd
            // 
            resources.ApplyResources(this._btFldAdd, "_btFldAdd");
            this._btFldAdd.Image = global::Nyx.Properties.Resources.add;
            this._btFldAdd.Name = "_btFldAdd";
            this._tooltip.SetToolTip(this._btFldAdd, resources.GetString("_btFldAdd.ToolTip"));
            this._btFldAdd.UseVisualStyleBackColor = true;
            this._btFldAdd.Click += new System.EventHandler(this._FldAdd_Click);
            // 
            // _ckbFldAutoInc
            // 
            resources.ApplyResources(this._ckbFldAutoInc, "_ckbFldAutoInc");
            this._ckbFldAutoInc.Name = "_ckbFldAutoInc";
            this._ckbFldAutoInc.UseVisualStyleBackColor = true;
            // 
            // _ckbFldOnUpdate
            // 
            resources.ApplyResources(this._ckbFldOnUpdate, "_ckbFldOnUpdate");
            this._ckbFldOnUpdate.Name = "_ckbFldOnUpdate";
            this._ckbFldOnUpdate.UseVisualStyleBackColor = true;
            // 
            // _ckbFldZerofill
            // 
            resources.ApplyResources(this._ckbFldZerofill, "_ckbFldZerofill");
            this._ckbFldZerofill.Name = "_ckbFldZerofill";
            this._ckbFldZerofill.UseVisualStyleBackColor = true;
            // 
            // _ckbFldUnsigned
            // 
            resources.ApplyResources(this._ckbFldUnsigned, "_ckbFldUnsigned");
            this._ckbFldUnsigned.Name = "_ckbFldUnsigned";
            this._ckbFldUnsigned.UseVisualStyleBackColor = true;
            // 
            // _ckbFldFulltext
            // 
            resources.ApplyResources(this._ckbFldFulltext, "_ckbFldFulltext");
            this._ckbFldFulltext.Name = "_ckbFldFulltext";
            this._ckbFldFulltext.UseVisualStyleBackColor = true;
            // 
            // _lblFldIndex
            // 
            resources.ApplyResources(this._lblFldIndex, "_lblFldIndex");
            this._lblFldIndex.Name = "_lblFldIndex";
            // 
            // _cbFldIndex
            // 
            this._cbFldIndex.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldIndex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbFldIndex.FormattingEnabled = true;
            this._cbFldIndex.Items.AddRange(new object[] {
            resources.GetString("_cbFldIndex.Items"),
            resources.GetString("_cbFldIndex.Items1"),
            resources.GetString("_cbFldIndex.Items2"),
            resources.GetString("_cbFldIndex.Items3")});
            resources.ApplyResources(this._cbFldIndex, "_cbFldIndex");
            this._cbFldIndex.Name = "_cbFldIndex";
            // 
            // _tbFldComment
            // 
            resources.ApplyResources(this._tbFldComment, "_tbFldComment");
            this._tbFldComment.Name = "_tbFldComment";
            // 
            // _lblFldComment
            // 
            resources.ApplyResources(this._lblFldComment, "_lblFldComment");
            this._lblFldComment.Name = "_lblFldComment";
            // 
            // _lblFldDefault
            // 
            resources.ApplyResources(this._lblFldDefault, "_lblFldDefault");
            this._lblFldDefault.Name = "_lblFldDefault";
            // 
            // _tbFldDefault
            // 
            resources.ApplyResources(this._tbFldDefault, "_tbFldDefault");
            this._tbFldDefault.Name = "_tbFldDefault";
            // 
            // _lblFldNull
            // 
            resources.ApplyResources(this._lblFldNull, "_lblFldNull");
            this._lblFldNull.Name = "_lblFldNull";
            // 
            // _cbFldNull
            // 
            this._cbFldNull.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldNull.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbFldNull.FormattingEnabled = true;
            this._cbFldNull.Items.AddRange(new object[] {
            resources.GetString("_cbFldNull.Items"),
            resources.GetString("_cbFldNull.Items1"),
            resources.GetString("_cbFldNull.Items2")});
            resources.ApplyResources(this._cbFldNull, "_cbFldNull");
            this._cbFldNull.Name = "_cbFldNull";
            // 
            // _lblFldLength
            // 
            resources.ApplyResources(this._lblFldLength, "_lblFldLength");
            this._lblFldLength.Name = "_lblFldLength";
            // 
            // _mtbFldLength
            // 
            this._mtbFldLength.HidePromptOnLeave = true;
            resources.ApplyResources(this._mtbFldLength, "_mtbFldLength");
            this._mtbFldLength.Name = "_mtbFldLength";
            // 
            // _lblFldName
            // 
            resources.ApplyResources(this._lblFldName, "_lblFldName");
            this._lblFldName.Name = "_lblFldName";
            // 
            // _lblFldCollate
            // 
            resources.ApplyResources(this._lblFldCollate, "_lblFldCollate");
            this._lblFldCollate.Name = "_lblFldCollate";
            // 
            // _lblFldType
            // 
            resources.ApplyResources(this._lblFldType, "_lblFldType");
            this._lblFldType.Name = "_lblFldType";
            // 
            // _tbFldName
            // 
            resources.ApplyResources(this._tbFldName, "_tbFldName");
            this._tbFldName.Name = "_tbFldName";
            this._tbFldName.TextChanged += new System.EventHandler(this._tbFldName_TextChanged);
            // 
            // _cbFldType
            // 
            this._cbFldType.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbFldType.FormattingEnabled = true;
            this._cbFldType.Items.AddRange(new object[] {
            resources.GetString("_cbFldType.Items"),
            resources.GetString("_cbFldType.Items1"),
            resources.GetString("_cbFldType.Items2"),
            resources.GetString("_cbFldType.Items3"),
            resources.GetString("_cbFldType.Items4"),
            resources.GetString("_cbFldType.Items5"),
            resources.GetString("_cbFldType.Items6"),
            resources.GetString("_cbFldType.Items7"),
            resources.GetString("_cbFldType.Items8"),
            resources.GetString("_cbFldType.Items9"),
            resources.GetString("_cbFldType.Items10"),
            resources.GetString("_cbFldType.Items11"),
            resources.GetString("_cbFldType.Items12"),
            resources.GetString("_cbFldType.Items13"),
            resources.GetString("_cbFldType.Items14"),
            resources.GetString("_cbFldType.Items15"),
            resources.GetString("_cbFldType.Items16"),
            resources.GetString("_cbFldType.Items17"),
            resources.GetString("_cbFldType.Items18"),
            resources.GetString("_cbFldType.Items19"),
            resources.GetString("_cbFldType.Items20"),
            resources.GetString("_cbFldType.Items21"),
            resources.GetString("_cbFldType.Items22"),
            resources.GetString("_cbFldType.Items23"),
            resources.GetString("_cbFldType.Items24"),
            resources.GetString("_cbFldType.Items25"),
            resources.GetString("_cbFldType.Items26"),
            resources.GetString("_cbFldType.Items27"),
            resources.GetString("_cbFldType.Items28"),
            resources.GetString("_cbFldType.Items29"),
            resources.GetString("_cbFldType.Items30"),
            resources.GetString("_cbFldType.Items31"),
            resources.GetString("_cbFldType.Items32")});
            resources.ApplyResources(this._cbFldType, "_cbFldType");
            this._cbFldType.Name = "_cbFldType";
            this._cbFldType.SelectedIndexChanged += new System.EventHandler(this._cbFieldType_SelectedIndexChanged);
            // 
            // _cbFldCollate
            // 
            this._cbFldCollate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._cbFldCollate.FormattingEnabled = true;
            this._cbFldCollate.Items.AddRange(new object[] {
            resources.GetString("_cbFldCollate.Items")});
            resources.ApplyResources(this._cbFldCollate, "_cbFldCollate");
            this._cbFldCollate.Name = "_cbFldCollate";
            // 
            // _gbGeneral
            // 
            this._gbGeneral.Controls.Add(this._lblTblName);
            this._gbGeneral.Controls.Add(this._tbTblComment);
            this._gbGeneral.Controls.Add(this._lblTblCollate);
            this._gbGeneral.Controls.Add(this._lblTblComment);
            this._gbGeneral.Controls.Add(this._btFldMoveDown);
            this._gbGeneral.Controls.Add(this._lvlTblType);
            this._gbGeneral.Controls.Add(this._btFldMoveUp);
            this._gbGeneral.Controls.Add(this._tbTblName);
            this._gbGeneral.Controls.Add(this._cbTblType);
            this._gbGeneral.Controls.Add(this._cbTblCollate);
            resources.ApplyResources(this._gbGeneral, "_gbGeneral");
            this._gbGeneral.Name = "_gbGeneral";
            this._gbGeneral.TabStop = false;
            // 
            // _lblTblName
            // 
            resources.ApplyResources(this._lblTblName, "_lblTblName");
            this._lblTblName.Name = "_lblTblName";
            // 
            // _tbTblComment
            // 
            resources.ApplyResources(this._tbTblComment, "_tbTblComment");
            this._tbTblComment.Name = "_tbTblComment";
            // 
            // _lblTblCollate
            // 
            resources.ApplyResources(this._lblTblCollate, "_lblTblCollate");
            this._lblTblCollate.Name = "_lblTblCollate";
            // 
            // _lblTblComment
            // 
            resources.ApplyResources(this._lblTblComment, "_lblTblComment");
            this._lblTblComment.Name = "_lblTblComment";
            // 
            // _btFldMoveDown
            // 
            resources.ApplyResources(this._btFldMoveDown, "_btFldMoveDown");
            this._btFldMoveDown.Image = global::Nyx.Properties.Resources.arrow_down;
            this._btFldMoveDown.Name = "_btFldMoveDown";
            this._btFldMoveDown.TabStop = false;
            this._tooltip.SetToolTip(this._btFldMoveDown, resources.GetString("_btFldMoveDown.ToolTip"));
            this._btFldMoveDown.UseVisualStyleBackColor = true;
            this._btFldMoveDown.Click += new System.EventHandler(this._FldMoveDown_Click);
            // 
            // _lvlTblType
            // 
            resources.ApplyResources(this._lvlTblType, "_lvlTblType");
            this._lvlTblType.Name = "_lvlTblType";
            // 
            // _btFldMoveUp
            // 
            resources.ApplyResources(this._btFldMoveUp, "_btFldMoveUp");
            this._btFldMoveUp.Image = global::Nyx.Properties.Resources.arrow_up;
            this._btFldMoveUp.Name = "_btFldMoveUp";
            this._btFldMoveUp.TabStop = false;
            this._tooltip.SetToolTip(this._btFldMoveUp, resources.GetString("_btFldMoveUp.ToolTip"));
            this._btFldMoveUp.UseVisualStyleBackColor = true;
            this._btFldMoveUp.Click += new System.EventHandler(this._FldMoveUp_Click);
            // 
            // _tbTblName
            // 
            resources.ApplyResources(this._tbTblName, "_tbTblName");
            this._tbTblName.Name = "_tbTblName";
            this._tbTblName.TextChanged += new System.EventHandler(this._tbTblName_TextChanged);
            // 
            // _cbTblType
            // 
            this._cbTblType.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbTblType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbTblType.FormattingEnabled = true;
            this._cbTblType.Items.AddRange(new object[] {
            resources.GetString("_cbTblType.Items"),
            resources.GetString("_cbTblType.Items1"),
            resources.GetString("_cbTblType.Items2"),
            resources.GetString("_cbTblType.Items3")});
            resources.ApplyResources(this._cbTblType, "_cbTblType");
            this._cbTblType.Name = "_cbTblType";
            // 
            // _cbTblCollate
            // 
            this._cbTblCollate.Cursor = System.Windows.Forms.Cursors.IBeam;
            this._cbTblCollate.FormattingEnabled = true;
            this._cbTblCollate.Items.AddRange(new object[] {
            resources.GetString("_cbTblCollate.Items")});
            resources.ApplyResources(this._cbTblCollate, "_cbTblCollate");
            this._cbTblCollate.Name = "_cbTblCollate";
            // 
            // _dgFields
            // 
            this._dgFields.AllowUserToAddRows = false;
            this._dgFields.AllowUserToDeleteRows = false;
            this._dgFields.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this._dgFields.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dgFields.BackgroundColor = System.Drawing.Color.GhostWhite;
            this._dgFields.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgFields.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._dgFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgFields.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this._dgvcFldIndex,
            this._dgvcFldName,
            this._dgvcFldType,
            this._dgvcFldLength,
            this._dgvcFldUnsigned,
            this._dgvcFldFulltext,
            this._dgvcFldAutoInc,
            this._dgvcFldZerofill,
            this._dgvcFldOnUpdate,
            this._dgvcFldBinary,
            this._dgvcFldNull,
            this._dgvcFldDefault,
            this._dgvcFldCharset,
            this._dgvcFldCollate,
            this._dgvcFldValues,
            this._dgvcFldComment});
            this._tblLayoutPnl.SetColumnSpan(this._dgFields, 3);
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgFields.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this._dgFields, "_dgFields");
            this._dgFields.MultiSelect = false;
            this._dgFields.Name = "_dgFields";
            this._dgFields.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgFields.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this._dgFields.RowHeadersVisible = false;
            this._dgFields.RowTemplate.Height = 18;
            this._dgFields.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dgFields.RowsAdded += new System.Windows.Forms.DataGridViewRowsAddedEventHandler(this._dgFields_RowsAdded);
            this._dgFields.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this._dgFields_RowsRemoved);
            this._dgFields.SelectionChanged += new System.EventHandler(this._dgFields_SelectionChanged);
            // 
            // _dgvcFldIndex
            // 
            resources.ApplyResources(this._dgvcFldIndex, "_dgvcFldIndex");
            this._dgvcFldIndex.Name = "_dgvcFldIndex";
            this._dgvcFldIndex.ReadOnly = true;
            this._dgvcFldIndex.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldIndex.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldName
            // 
            resources.ApplyResources(this._dgvcFldName, "_dgvcFldName");
            this._dgvcFldName.Name = "_dgvcFldName";
            this._dgvcFldName.ReadOnly = true;
            // 
            // _dgvcFldType
            // 
            resources.ApplyResources(this._dgvcFldType, "_dgvcFldType");
            this._dgvcFldType.Name = "_dgvcFldType";
            this._dgvcFldType.ReadOnly = true;
            this._dgvcFldType.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldLength
            // 
            resources.ApplyResources(this._dgvcFldLength, "_dgvcFldLength");
            this._dgvcFldLength.Name = "_dgvcFldLength";
            this._dgvcFldLength.ReadOnly = true;
            // 
            // _dgvcFldUnsigned
            // 
            resources.ApplyResources(this._dgvcFldUnsigned, "_dgvcFldUnsigned");
            this._dgvcFldUnsigned.Name = "_dgvcFldUnsigned";
            this._dgvcFldUnsigned.ReadOnly = true;
            this._dgvcFldUnsigned.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldUnsigned.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldFulltext
            // 
            resources.ApplyResources(this._dgvcFldFulltext, "_dgvcFldFulltext");
            this._dgvcFldFulltext.Name = "_dgvcFldFulltext";
            this._dgvcFldFulltext.ReadOnly = true;
            this._dgvcFldFulltext.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldFulltext.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldAutoInc
            // 
            resources.ApplyResources(this._dgvcFldAutoInc, "_dgvcFldAutoInc");
            this._dgvcFldAutoInc.Name = "_dgvcFldAutoInc";
            this._dgvcFldAutoInc.ReadOnly = true;
            this._dgvcFldAutoInc.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldAutoInc.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldZerofill
            // 
            resources.ApplyResources(this._dgvcFldZerofill, "_dgvcFldZerofill");
            this._dgvcFldZerofill.Name = "_dgvcFldZerofill";
            this._dgvcFldZerofill.ReadOnly = true;
            this._dgvcFldZerofill.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldZerofill.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldOnUpdate
            // 
            resources.ApplyResources(this._dgvcFldOnUpdate, "_dgvcFldOnUpdate");
            this._dgvcFldOnUpdate.Name = "_dgvcFldOnUpdate";
            this._dgvcFldOnUpdate.ReadOnly = true;
            this._dgvcFldOnUpdate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldOnUpdate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldBinary
            // 
            resources.ApplyResources(this._dgvcFldBinary, "_dgvcFldBinary");
            this._dgvcFldBinary.Name = "_dgvcFldBinary";
            this._dgvcFldBinary.ReadOnly = true;
            this._dgvcFldBinary.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldBinary.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldNull
            // 
            resources.ApplyResources(this._dgvcFldNull, "_dgvcFldNull");
            this._dgvcFldNull.Name = "_dgvcFldNull";
            this._dgvcFldNull.ReadOnly = true;
            this._dgvcFldNull.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldNull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldDefault
            // 
            resources.ApplyResources(this._dgvcFldDefault, "_dgvcFldDefault");
            this._dgvcFldDefault.Name = "_dgvcFldDefault";
            this._dgvcFldDefault.ReadOnly = true;
            // 
            // _dgvcFldCharset
            // 
            resources.ApplyResources(this._dgvcFldCharset, "_dgvcFldCharset");
            this._dgvcFldCharset.Name = "_dgvcFldCharset";
            this._dgvcFldCharset.ReadOnly = true;
            this._dgvcFldCharset.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldCharset.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldCollate
            // 
            resources.ApplyResources(this._dgvcFldCollate, "_dgvcFldCollate");
            this._dgvcFldCollate.Name = "_dgvcFldCollate";
            this._dgvcFldCollate.ReadOnly = true;
            this._dgvcFldCollate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this._dgvcFldCollate.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // _dgvcFldValues
            // 
            resources.ApplyResources(this._dgvcFldValues, "_dgvcFldValues");
            this._dgvcFldValues.Name = "_dgvcFldValues";
            this._dgvcFldValues.ReadOnly = true;
            // 
            // _dgvcFldComment
            // 
            resources.ApplyResources(this._dgvcFldComment, "_dgvcFldComment");
            this._dgvcFldComment.Name = "_dgvcFldComment";
            this._dgvcFldComment.ReadOnly = true;
            // 
            // _ms
            // 
            this._ms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msTbl,
            this._msFld});
            resources.ApplyResources(this._ms, "_ms");
            this._ms.Name = "_ms";
            // 
            // _msTbl
            // 
            this._msTbl.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msTblCreate,
            this._msTbl_sep,
            this._msTblClose});
            this._msTbl.Name = "_msTbl";
            resources.ApplyResources(this._msTbl, "_msTbl");
            // 
            // _msTblCreate
            // 
            resources.ApplyResources(this._msTblCreate, "_msTblCreate");
            this._msTblCreate.Image = global::Nyx.Properties.Resources.accept;
            this._msTblCreate.Name = "_msTblCreate";
            this._msTblCreate.Click += new System.EventHandler(this._btOk_Click);
            // 
            // _msTbl_sep
            // 
            this._msTbl_sep.Name = "_msTbl_sep";
            resources.ApplyResources(this._msTbl_sep, "_msTbl_sep");
            // 
            // _msTblClose
            // 
            this._msTblClose.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._msTblClose, "_msTblClose");
            this._msTblClose.Name = "_msTblClose";
            this._msTblClose.Click += new System.EventHandler(this._msCloseDialog_Click);
            // 
            // _msFld
            // 
            this._msFld.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msFldAdd,
            this._msFldEdit,
            this._msFldRemove,
            this._msFld_Sep,
            this._msFldFieldUp,
            this._msFldFieldDown});
            this._msFld.Name = "_msFld";
            resources.ApplyResources(this._msFld, "_msFld");
            // 
            // _msFldAdd
            // 
            resources.ApplyResources(this._msFldAdd, "_msFldAdd");
            this._msFldAdd.Image = global::Nyx.Properties.Resources.add;
            this._msFldAdd.Name = "_msFldAdd";
            this._msFldAdd.Click += new System.EventHandler(this._FldAdd_Click);
            // 
            // _msFldEdit
            // 
            resources.ApplyResources(this._msFldEdit, "_msFldEdit");
            this._msFldEdit.Image = global::Nyx.Properties.Resources.pencil;
            this._msFldEdit.Name = "_msFldEdit";
            this._msFldEdit.Click += new System.EventHandler(this._FldEdit_Click);
            // 
            // _msFldRemove
            // 
            resources.ApplyResources(this._msFldRemove, "_msFldRemove");
            this._msFldRemove.Image = global::Nyx.Properties.Resources.delete;
            this._msFldRemove.Name = "_msFldRemove";
            this._msFldRemove.Click += new System.EventHandler(this._FldRemove_Click);
            // 
            // _msFld_Sep
            // 
            this._msFld_Sep.Name = "_msFld_Sep";
            resources.ApplyResources(this._msFld_Sep, "_msFld_Sep");
            // 
            // _msFldFieldUp
            // 
            resources.ApplyResources(this._msFldFieldUp, "_msFldFieldUp");
            this._msFldFieldUp.Image = global::Nyx.Properties.Resources.arrow_up;
            this._msFldFieldUp.Name = "_msFldFieldUp";
            this._msFldFieldUp.Click += new System.EventHandler(this._FldMoveUp_Click);
            // 
            // _msFldFieldDown
            // 
            resources.ApplyResources(this._msFldFieldDown, "_msFldFieldDown");
            this._msFldFieldDown.Image = global::Nyx.Properties.Resources.arrow_down;
            this._msFldFieldDown.Name = "_msFldFieldDown";
            this._msFldFieldDown.Click += new System.EventHandler(this._FldMoveDown_Click);
            // 
            // _tblLayoutPnl
            // 
            resources.ApplyResources(this._tblLayoutPnl, "_tblLayoutPnl");
            this._tblLayoutPnl.Controls.Add(this._gbFields, 1, 0);
            this._tblLayoutPnl.Controls.Add(this._dgFields, 0, 1);
            this._tblLayoutPnl.Controls.Add(this._gbGeneral, 0, 0);
            this._tblLayoutPnl.Controls.Add(this._btCancel, 2, 2);
            this._tblLayoutPnl.Controls.Add(this._btOk, 1, 2);
            this._tblLayoutPnl.Name = "_tblLayoutPnl";
            // 
            // _btCancel
            // 
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._msCloseDialog_Click);
            // 
            // _btOk
            // 
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btOk_Click);
            // 
            // MySqlCreateTable
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._tblLayoutPnl);
            this.Controls.Add(this._ms);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MainMenuStrip = this._ms;
            this.Name = "MySqlCreateTable";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MySqlCreateTable_FormClosed);
            this.Load += new System.EventHandler(this.MySQLCreateTable_Load);
            this._gbFields.ResumeLayout(false);
            this._gbFields.PerformLayout();
            this._gbGeneral.ResumeLayout(false);
            this._gbGeneral.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgFields)).EndInit();
            this._ms.ResumeLayout(false);
            this._ms.PerformLayout();
            this._tblLayoutPnl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolTip _tooltip;
        private System.Windows.Forms.DataGridView _dgFields;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldType;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldLength;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldUnsigned;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldFulltext;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldAutoInc;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldZerofill;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldOnUpdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldBinary;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldDefault;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldCharset;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldCollate;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldValues;
        private System.Windows.Forms.DataGridViewTextBoxColumn _dgvcFldComment;
        private System.Windows.Forms.MenuStrip _ms;
        private System.Windows.Forms.GroupBox _gbFields;
        private System.Windows.Forms.CheckBox _ckbFldBinary;
        private System.Windows.Forms.Label _lblFldValues;
        private System.Windows.Forms.TextBox _tbFldValues;
        private System.Windows.Forms.ComboBox _cbFldCharset;
        private System.Windows.Forms.Label _lblFldCharset;
        private System.Windows.Forms.Button _btFldMoveDown;
        private System.Windows.Forms.Button _btFldMoveUp;
        private System.Windows.Forms.Button _btFldRemove;
        private System.Windows.Forms.Button _btFldAdd;
        private System.Windows.Forms.CheckBox _ckbFldAutoInc;
        private System.Windows.Forms.CheckBox _ckbFldOnUpdate;
        private System.Windows.Forms.CheckBox _ckbFldZerofill;
        private System.Windows.Forms.CheckBox _ckbFldUnsigned;
        private System.Windows.Forms.CheckBox _ckbFldFulltext;
        private System.Windows.Forms.Label _lblFldIndex;
        private System.Windows.Forms.ComboBox _cbFldIndex;
        private System.Windows.Forms.TextBox _tbFldComment;
        private System.Windows.Forms.Label _lblFldComment;
        private System.Windows.Forms.Label _lblFldDefault;
        private System.Windows.Forms.TextBox _tbFldDefault;
        private System.Windows.Forms.Label _lblFldNull;
        private System.Windows.Forms.ComboBox _cbFldNull;
        private System.Windows.Forms.Label _lblFldLength;
        private System.Windows.Forms.MaskedTextBox _mtbFldLength;
        private System.Windows.Forms.Label _lblFldName;
        private System.Windows.Forms.Label _lblFldCollate;
        private System.Windows.Forms.Label _lblFldType;
        private System.Windows.Forms.TextBox _tbFldName;
        private System.Windows.Forms.ComboBox _cbFldType;
        private System.Windows.Forms.ComboBox _cbFldCollate;
        private System.Windows.Forms.GroupBox _gbGeneral;
        private System.Windows.Forms.Label _lblTblName;
        private System.Windows.Forms.TextBox _tbTblComment;
        private System.Windows.Forms.Label _lblTblCollate;
        private System.Windows.Forms.Label _lblTblComment;
        private System.Windows.Forms.Label _lvlTblType;
        private System.Windows.Forms.TextBox _tbTblName;
        private System.Windows.Forms.ComboBox _cbTblType;
        private System.Windows.Forms.ComboBox _cbTblCollate;
        private System.Windows.Forms.ToolStripMenuItem _msFld;
        private System.Windows.Forms.ToolStripMenuItem _msFldAdd;
        private System.Windows.Forms.ToolStripMenuItem _msFldRemove;
        private System.Windows.Forms.ToolStripSeparator _msFld_Sep;
        private System.Windows.Forms.ToolStripMenuItem _msFldFieldUp;
        private System.Windows.Forms.ToolStripMenuItem _msFldFieldDown;
        private System.Windows.Forms.ToolStripMenuItem _msTbl;
        private System.Windows.Forms.ToolStripMenuItem _msTblCreate;
        private System.Windows.Forms.ToolStripSeparator _msTbl_sep;
        private System.Windows.Forms.ToolStripMenuItem _msTblClose;
        private System.Windows.Forms.Button _btFldEdit;
        private System.Windows.Forms.ToolStripMenuItem _msFldEdit;
        private System.Windows.Forms.TableLayoutPanel _tblLayoutPnl;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
    }
}