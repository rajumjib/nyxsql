/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Nyx.Classes;
using Nyx.AppDialogs;

namespace Nyx.NyxMySql
{
    public partial class MySqlUser : Form
    {   // dbhelper
        DBHelper dbh = new DBHelper(NyxSettings.DBType.MySql);
        // bool var, that updatefunction for database grants won't run
        private bool stopUpdate = false;
        private bool userChanged = false;
        private bool dbprivsChanged = false;
        
        // datatable
        private System.Data.DataSet dsUserPrivs = new System.Data.DataSet("dsUserPrivs");
        private System.Data.DataTable dtDbPrivs = null;

        /// <summary>
        /// Init
        /// </summary>
        public MySqlUser()
        {
            InitializeComponent();
        }
        private void FrmMySQLUser_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // pre-gui settings
            stopUpdate = true;
            this._cb_privpreselection.SelectedIndex = 0;
            this._tabc.TabPages["dbprivs"].Enabled = false;
            this._tabc.TabPages["tblprivs"].Enabled = false;
            stopUpdate = false;
            // disable globalprivs
            this._gb_globalprivs.Enabled = false;

            if (!dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                MessageHandler.ShowDialog(new DBException(GuiHelper.GetResourceMsg("DatabasesRetrieveErr"), dbh.ErrExc),
                    MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
            if(GetInitData())
                RefreshGui();
        }
        private void FrmMySQLUser_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }

        #region gui funcs
        private void _lv_dgd_SelectedIndexChanged(object sender, EventArgs e)
        {   // setting label
            if (this._lv_dblist.SelectedItems.Count == 0)
            {
                this._lv_dbprivs.Enabled = false;
                stopUpdate = true;
                DeSelectChkLView(this._lv_dbprivs);
                stopUpdate = false;
            }
            else
            {
                this._lbl_dbsel.Text = this._lv_dblist.SelectedItems[0].Text;
                // setting dbprivs
                SetGuiDbPrivs();
                this._lv_dbprivs.Enabled = true;
            }
        }
        private void _lv_user_SelectedIndexChanged(object sender, EventArgs e)
        {
            ChkUserChanged();
            stopUpdate = true;
            SetUserData();
            stopUpdate = false;
            if (this._lv_user.SelectedItems.Count == 0)
            {
                this._tabc.TabPages["dbprivs"].Enabled = false;
                this._tabc.TabPages["tblprivs"].Enabled = false;
            }
        }
        private void _bt_usercancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _cb_globalgrant_SelectedIndexChanged(object sender, EventArgs e)
        {   // user changed something and it had been no loading function
            if(!stopUpdate)
                userChanged = true;
            // parsing what to do
            switch (this._cb_privpreselection.SelectedIndex)
            {
                case 2:
                    this._gb_globalprivs.Enabled = false;
                    this._tabc.TabPages["dbprivs"].Enabled = false;
                    this._tabc.TabPages["tblprivs"].Enabled = false;
                    break;
                case 1:
                    this._gb_globalprivs.Enabled = false;
                    this._tabc.TabPages["dbprivs"].Enabled = false;
                    this._tabc.TabPages["tblprivs"].Enabled = false;
                    break;
                case 0:
                    this._gb_globalprivs.Enabled = true;
                    this._tabc.TabPages["dbprivs"].Enabled = true;
                    this._tabc.TabPages["tblprivs"].Enabled = true;
                    break;
            }
        }
        private void _lv_gg_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            // user changed something and it had been no loading function
            if (!stopUpdate)
                userChanged = true;
            if (this._lv_gprivs.CheckedItems.Count == this._lv_gprivs.Items.Count)
            {
                this._cb_privpreselection.SelectedIndex = 2;
            }
        }
        private void _lv_log_DoubleClick(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection lviCol = this._lv_log.SelectedItems;
            if (lviCol.Count == 1)
            {
                MessageHandler.ShowDialog(lviCol[0].Text, MessageHandler.EnumMsgType.Information);
            }
        }
        private void _tabc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._tabc.SelectedIndex != 0)
                ChkUserChanged();
            else if (this._tabc.SelectedIndex != 1)
                ChkDbPrivsChanged();
        }
        private void _lv_dbprivs_ItemChecked(object sender, ItemCheckedEventArgs e)
        {   // check update
            if (stopUpdate)
                return;
            // user changed
            dbprivsChanged = true;
            // search the user row
            System.Data.DataRow[] dr = dsUserPrivs.Tables["dbprivs"].Select("User = '" + this._tb_user.Text + "' AND Host = '" + this._tb_host.Text + "' AND Db = '" + this._lbl_dbsel.Text + "'");
            if (dr.Length == 1)
            {
                switch (e.Item.Text)
                {
                    case "Select":
                        if (!e.Item.Checked && dr[0]["Select_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Select_priv", "N");
                        else if (e.Item.Checked && dr[0]["Select_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Select_priv", "Y");
                        break;
                    case "Insert":
                        if (!e.Item.Checked && dr[0]["Insert_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Insert_priv", "N");
                        else if (e.Item.Checked && dr[0]["Insert_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Insert_priv", "Y");
                        break;
                    case "Update":
                        if (!e.Item.Checked && dr[0]["Update_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Update_priv", "N");
                        else if (e.Item.Checked && dr[0]["Update_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Update_priv", "Y");
                        break;
                    case "Delete":
                        if (!e.Item.Checked && dr[0]["Delete_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Delete_priv", "N");
                        else if (e.Item.Checked && dr[0]["Delete_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Delete_priv", "Y");
                        break;
                    case "Create":
                        if (!e.Item.Checked && dr[0]["Create_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Create_priv", "N");
                        else if (e.Item.Checked && dr[0]["Create_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Create_priv", "Y");
                        break;
                    case "Index":
                        if (!e.Item.Checked && dr[0]["Index_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Index_priv", "N");
                        else if (e.Item.Checked && dr[0]["Index_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Index_priv", "Y");
                        break;
                    case "Alter":
                        if (!e.Item.Checked && dr[0]["Alter_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Alter_priv", "N");
                        else if (e.Item.Checked && dr[0]["Alter_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Alter_priv", "Y");
                        break;
                    case "Drop":
                        if (!e.Item.Checked && dr[0]["Drop_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Drop_priv", "N");
                        else if (e.Item.Checked && dr[0]["Drop_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Drop_priv", "Y");
                        break;
                    case "Create temporary tables":
                        if (!e.Item.Checked && dr[0]["Create_tmp_table_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Create_tmp_table_priv", "N");
                        else if (e.Item.Checked && dr[0]["Create_tmp_table_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Create_tmp_table_priv", "Y");
                        break;
                    case "Grant":
                        if (!e.Item.Checked && dr[0]["Grant_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Grant_priv", "N");
                        else if (e.Item.Checked && dr[0]["Grant_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Grant_priv", "Y");
                        break;
                    case "Lock tables":
                        if (!e.Item.Checked && dr[0]["Lock_tables_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "Lock_tables_priv", "N");
                        else if (e.Item.Checked && dr[0]["Lock_tables_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "Lock_tables_priv", "Y");
                        break;
                    case "References":
                        if (!e.Item.Checked && dr[0]["References_priv"].ToString() == "Y")
                            SetDSDBPrivs(dr[0], "References_priv", "N");
                        else if (e.Item.Checked && dr[0]["References_priv"].ToString() == "N")
                            SetDSDBPrivs(dr[0], "References_priv", "Y");
                        break;
                }
                dsUserPrivs.AcceptChanges();
            }
            // new checked item, so we have to set the new priv in a new row cause we found none
            else if (e.Item.Checked)
            {
                System.Data.DataRow drAdd = dsUserPrivs.Tables["dbprivs"].NewRow();
                drAdd.BeginEdit();
                drAdd["User"] = this._tb_user.Text;
                drAdd["Host"] = this._tb_host.Text;
                drAdd["Db"] = this._lbl_dbsel.Text;
                // adding all columns
                if (e.Item.Text == "Select")
                    drAdd["Select_priv"] = "Y";
                else
                    drAdd["Select_priv"] = "N";
                if (e.Item.Text == "Insert")
                    drAdd["Insert_priv"] = "Y";
                else
                    drAdd["Insert_priv"] = "N";
                if (e.Item.Text == "Update")
                    drAdd["Update_priv"] = "Y";
                else
                    drAdd["Update_priv"] = "N";
                if (e.Item.Text == "Delete")
                    drAdd["Delete_priv"] = "Y";
                else
                    drAdd["Delete_priv"] = "N";
                if (e.Item.Text == "Create")
                    drAdd["Create_priv"] = "Y";
                else
                    drAdd["Create_priv"] = "N";
                if (e.Item.Text == "Index")
                    drAdd["Index_priv"] = "Y";
                else
                    drAdd["Index_priv"] = "N";
                if (e.Item.Text == "Alter")
                    drAdd["Alter_priv"] = "Y";
                else
                    drAdd["Alter_priv"] = "N";
                if (e.Item.Text == "Drop")
                    drAdd["Drop_priv"] = "Y";
                else
                    drAdd["Drop_priv"] = "N";
                if (e.Item.Text == "Create temporary tables")
                    drAdd["Create_tmp_table_priv"] = "Y";
                else
                    drAdd["Create_tmp_table_priv"] = "N";
                if (e.Item.Text == "Grant")
                    drAdd["Grant_priv"] = "Y";
                else
                    drAdd["Grant_priv"] = "N";
                if (e.Item.Text == "Lock tables")
                    drAdd["Lock_tables_priv"] = "Y";
                else
                    drAdd["Lock_tables_priv"] = "N";
                if (e.Item.Text == "References")
                    drAdd["References_priv"] = "Y";
                else
                    drAdd["References_priv"] = "N";
                drAdd.EndEdit();
                dsUserPrivs.Tables["dbprivs"].Rows.Add(drAdd);
                dsUserPrivs.AcceptChanges();
            }
        }
        private void _tb_passw_TextChanged(object sender, EventArgs e)
        {
            // user changed something and it had been no loading function
            if (!stopUpdate)
                userChanged = true;
        }
        private void _tb_passw2_TextChanged(object sender, EventArgs e)
        {
            // user changed something and it had been no loading function
            if (!stopUpdate)
                userChanged = true;
        }
        private void _ckb_grantopt_CheckedChanged(object sender, EventArgs e)
        {
            // user changed something and it had been no loading function
            if (!stopUpdate)
                userChanged = true;
        }
        #endregion

        #region div. functions
        private bool GetInitData()
        {   // resetting dataset
            dsUserPrivs.Reset();
            System.Data.DataTable dt = null;
            // retrieving users
            dt = dsUserPrivs.Tables.Add("user");
            if (!dbh.FillDT("select * from mysql.user", ref dt))
            {

                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrGetUsers", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
                return false;
            }
            // database privs
            dt = dsUserPrivs.Tables.Add("dbprivs");
            dt.Locale = NyxMain.NyxCI;
            if (!dbh.FillDT("select * from mysql.db", ref dt))
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrGetDbPrivileges", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
                return false;
            }
            dtDbPrivs = dsUserPrivs.Tables["dbprivs"].Copy();
            // tableprivs
            dt = dsUserPrivs.Tables.Add("tblprivs");
            if (!dbh.FillDT("select * from mysql.tables_priv", ref dt))
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrGetTablePrivileges", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
                return false;
            }
            // getting databases
            if (dbh.ExecuteReader("show databases"))
            {   // clearup
                this._lv_dblist.Items.Clear();
                // init
                ListViewItem lvi = null;
                // read
                while (dbh.Read())
                {
                    lvi = new ListViewItem(dbh.GetValue(0).ToString());
                    this._lv_dblist.Items.Add(lvi);
                }
                dbh.Close();
            }
            else
            {
                MessageHandler.ShowDialog(new DBException(GuiHelper.GetResourceMsg("DatabasesRetrieveErr"), dbh.ErrExc),
                    MessageHandler.EnumMsgType.Warning);
                this.Close();
                return false;
            }
            return true;
        }
        private void RefreshGui()
        {   // clearing listview
            this._lv_user.Items.Clear();
            // filling users listview
            ListViewItem lvi = null;
            foreach (System.Data.DataRow dr in dsUserPrivs.Tables[0].Rows)
            {   // user
                if (dr["User"].ToString().Length == 0)
                    lvi = new ListViewItem("Any");
                else
                    lvi = new ListViewItem(dr["User"].ToString());
                // host
                lvi.SubItems.Add(dr["Host"].ToString());
                // password
                if (dr["Password"].ToString() != string.Empty)
                    lvi.SubItems.Add("Y");
                else
                    lvi.SubItems.Add("N");
                this._lv_user.Items.Add(lvi);
            }
        }
        private void SetUserData()
        {   // reset dialog
            ResetDialog();
            // set stopupdate
            stopUpdate = true;
            // disable user+hosts fields
            this._tb_user.ReadOnly = true;
            this._tb_host.ReadOnly = true;
            // get new data
            ListView.SelectedListViewItemCollection lviColl = this._lv_user.SelectedItems;
            if (lviColl.Count == 1)
            {   // parsing user details
                System.Data.DataRow[] dr = null;
                if(lviColl[0].Text == "Any")
                     dr = dsUserPrivs.Tables[0].Select("User = '' AND Host = '" + lviColl[0].SubItems[1].Text + "'");
                else
                     dr = dsUserPrivs.Tables[0].Select("User = '" + lviColl[0].Text + "' AND Host = '" + lviColl[0].SubItems[1].Text + "'");

                if (dr.Length == 1)
                {
                    if (dr[0]["User"].ToString().Length == 0)
                        this._tb_user.Text = "Any";
                    else
                        this._tb_user.Text = dr[0]["User"].ToString();
                    this._tb_host.Text = dr[0]["Host"].ToString();
                    if (dr[0]["Password"].ToString().Length > 0)
                    {
                        this._tb_passw.Text = "oldone";
                        this._tb_passw2.Text = "oldone";
                    }
                    // parsing privileges
                    // at first checkbox
                    if (dr[0]["Grant_priv"].ToString() == "Y")
                        this._ckb_grantopt.Checked = true;
                    // then listview
                    if (dr[0]["Select_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Select", true);
                    if (dr[0]["Insert_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Insert", true);
                    if (dr[0]["Update_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Update", true);
                    if (dr[0]["Delete_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Delete", true);
                    if (dr[0]["Create_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Create", true);
                    if (dr[0]["Drop_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Drop", true);
                    if (dr[0]["Reload_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Reload", true);
                    if (dr[0]["Shutdown_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Shutdown", true);
                    if (dr[0]["Process_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Process", true);
                    if (dr[0]["File_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "File", true);
                    if (dr[0]["References_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "References", true);
                    if (dr[0]["Index_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Index", true);
                    if (dr[0]["Alter_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Alter", true);
                    if (dr[0]["Show_db_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Show DB", true);
                    if (dr[0]["Super_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Super", true);
                    if (dr[0]["Create_tmp_table_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Create temporary tables", true);
                    if (dr[0]["Lock_tables_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Lock tables", true);
                    if (dr[0]["Execute_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Execute", true);
                    if (dr[0]["Repl_slave_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Replication Slave", true);
                    if (dr[0]["Repl_client_priv"].ToString() == "Y")
                        CheckPriv(this._lv_gprivs.Items, "Replication Client", true);
                    // mysql v4->5 fix
                    if (dsUserPrivs.Tables[0].Columns["Create_routine_priv"] != null)
                    {
                        if (dr[0]["Create_routine_priv"].ToString() == "Y")
                            CheckPriv(this._lv_gprivs.Items, "Create routine", true);
                    }
                    else
                        RemoveGlobalPriv("Create routine");
                    if (dsUserPrivs.Tables[0].Columns["Alter_routine_priv"] != null)
                    {
                        if (dr[0]["Alter_routine_priv"].ToString() == "Y")
                            CheckPriv(this._lv_gprivs.Items, "Alter routine", true);
                    }
                    else
                        RemoveGlobalPriv("Alter routine");
                    if (dsUserPrivs.Tables[0].Columns["Create_user_priv"] != null)
                    {
                        if (dr[0]["Create_user_priv"].ToString() == "Y")
                            CheckPriv(this._lv_gprivs.Items, "Create user", true);
                    }
                    else
                        RemoveGlobalPriv("Create user");
                }
                // check if user got any grants or just usage and set the combobox
                if (this._lv_gprivs.CheckedItems.Count == 0)
                    this._cb_privpreselection.SelectedIndex = 1;
                else if (this._lv_gprivs.CheckedItems.Count == this._lv_gprivs.Items.Count)
                    this._cb_privpreselection.SelectedIndex = 2;
                else
                    this._cb_privpreselection.SelectedIndex = 0;
                // enabling dialog
                EnableDialog();
            }
            // reset stopupdate
            stopUpdate = false;
        }     
        private void AddUser()
        {
            // at first we create the user
            string qry = "GRANT USAGE ON *.* TO '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
            if (this._tb_passw.Text != null)
                qry += " identified by '" + this._tb_passw.Text + "'";
            if (dbh.ExecuteNonQuery(qry))
            {
                AddLogEntry("User '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "' created.", false);
                if (GetInitData())
                    RefreshGui();
            }
            else
            {
                AddLogEntry("Unable to create user '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "':\n " + qry + "\n" + dbh.ErrExc.Message.ToString(), true);
                return;
            }
            // now adding the grants
            qry = null;
            if (this._cb_privpreselection.SelectedIndex == 2)
            {   // we got global grants
                qry = "GRANT ALL PRIVILEGES ON *.* TO '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
            }
            else if (this._cb_privpreselection.SelectedIndex == 1)
            {   // just usage
                qry = "GRANT USAGE ON *.* TO '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
            }
            else
            {
                foreach (ListViewItem lvi in this._lv_gprivs.CheckedItems)
                {
                    if (qry == null)
                        qry = "GRANT " + lvi.Text;
                    else
                        qry += ", " + lvi.Text;
                }
                // finalizing query
                qry += " ON *.* TO '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
            }
            // grant option
            if (this._ckb_grantopt.Checked)
                qry += " WITH GRANT OPTION";
            // submitting query
            if (dbh.ExecuteNonQuery(qry))
            {
                AddLogEntry("Grants '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "' done.", false);
                if (GetInitData())
                    RefreshGui();
                userChanged = false;
            }
            else
            {
                AddLogEntry("Unable to set grants for user '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "':\n " + qry + "\n" + dbh.ErrExc.Message.ToString(), true);
                return;
            }
        }
        private void EditUser()
        {   // get the user
            System.Data.DataRow[] dr = dsUserPrivs.Tables[0].Select("User = '" + this._tb_user.Text + "' AND Host = '" + this._tb_host.Text + "'");
            if (dr.Length == 1)
            {
                string qry = null;
                // checking password
                if (dr[0]["Password"].ToString() != string.Empty && this._tb_passw.Text.Length == 0)
                    qry = "SET PASSWORD FOR '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "' = PASSWORD('')";
                else if (this._tb_passw.Text != "oldone")
                    qry = "SET PASSWORD FOR '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "' = PASSWORD('" + this._tb_passw.Text + "')";
                if (qry != null)
                {
                    if (dbh.ExecuteNonQuery(qry))
                    {
                        AddLogEntry("Password changed for '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'.", false);
                        if (GetInitData())
                            RefreshGui();
                    }
                    else
                    {
                        AddLogEntry("Unable to change password for '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "':\n " + qry + "\n" + dbh.ErrExc.Message.ToString(), true);
                        return;
                    }
                }
                // at first revoking every privilege
                qry = "REVOKE ALL PRIVILEGES, GRANT OPTION FROM '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
                if (!dbh.ExecuteNonQuery(qry))
                {
                    AddLogEntry("Unable to set grants for user '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "':\n " + qry + "\n" + dbh.ErrExc.Message.ToString(), true);
                    return;
                }
                // now adding the grants
                qry = null;
                if (this._cb_privpreselection.SelectedIndex == 2)
                {   // we got global grants
                    qry = "GRANT ALL PRIVILEGES ON *.* TO '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
                }
                else if (this._cb_privpreselection.SelectedIndex == 1)
                {   // just usage
                    qry = "GRANT USAGE ON *.* TO '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
                }
                else
                {   // we got custom grants
                    foreach (ListViewItem lvi in this._lv_gprivs.CheckedItems)
                    {
                        if (qry == null)
                            qry = "GRANT " + lvi.Text;
                        else
                            qry += ", " + lvi.Text;
                    }
                    // finalizing query
                    qry += " ON *.* TO '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
                }
                // grant option
                if (this._ckb_grantopt.Checked)
                    qry += " WITH GRANT OPTION";
                // submitting query
                if (dbh.ExecuteNonQuery(qry))
                {
                    AddLogEntry("Grants '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "' done.", false);
                    if (GetInitData())
                        RefreshGui();
                    userChanged = false;
                }
                else
                {
                    AddLogEntry("Unable to set grants for user '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "':\n " + qry + "\n" + dbh.ErrExc.Message.ToString(), true);
                    return;
                }
            }
        }
        private void RemoveUser()
        {
            // check if a user has been selected
            ListView.SelectedListViewItemCollection lviCol = this._lv_user.SelectedItems;
            if (lviCol.Count == 1)
            {
                DialogResult dRes = MessageBox.Show("Really delete user '" + lviCol[0].Text + "'@'" + lviCol[0].SubItems[1].Text + "'?", "Delete User", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dRes == DialogResult.Yes)
                {
                    string qry = null;
                    dRes = MessageBox.Show("Revoke grants?", "Delete User", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dRes == DialogResult.Yes)
                    {
                        // query
                        qry = "REVOKE ALL PRIVILEGES";
                        // take a look if the user has the grant option
                        if (this._ckb_grantopt.Checked)
                             qry += ", GRANT OPTION";
                        // finalize query 
                        qry += " ON *.* FROM '" + lviCol[0].Text + "'@'" + lviCol[0].SubItems[1].Text + "'";
                        if (!dbh.ExecuteNonQuery(qry))
                        {
                            AddLogEntry("Unable to remove user grants from '" + lviCol[0].Text + "'@'" + lviCol[0].SubItems[1].Text + "': " + qry + " / " + dbh.ErrExc.Message.ToString(), true);
                            return;
                        }
                        qry = "DROP USER '" + lviCol[0].Text + "'@'" + lviCol[0].SubItems[1].Text + "'";
                        if (dbh.ExecuteNonQuery(qry))
                        {
                            AddLogEntry("User dropped '" + lviCol[0].Text + "'@'" + lviCol[0].SubItems[1].Text + "'.", false);
                        }
                        else
                        {
                            AddLogEntry("Unable to drop user '" + lviCol[0].Text + "'@'" + lviCol[0].SubItems[1].Text + "': " + qry + " / " + dbh.ErrExc.Message.ToString(), true);
                            return;
                         }
                    }
                    else
                    {
                        // query
                        qry = "DROP USER '" + lviCol[0].Text + "'@'" + lviCol[0].SubItems[1].Text + "'";
                        if (dbh.ExecuteNonQuery(qry))
                        {
                            AddLogEntry("User dropped " + this._tb_user.Text + "@" + lviCol[0].SubItems[1].Text + ".", false);
                        }
                        else
                        {
                            AddLogEntry("Unable to drop user " + this._tb_user.Text + "@" + lviCol[0].SubItems[1].Text + ": " + qry + " / " + dbh.ErrExc.Message.ToString(), true);
                            return;
                        }
                    }
                    if (GetInitData())
                        RefreshGui();
                }
            }
        }
        private void SetDbPrivs()
        {
            // parsing the dataset
            string qry = null;
            System.Data.DataRow[] dra = dsUserPrivs.Tables["dbprivs"].Select("User = '" + this._tb_user.Text + "' AND Host = '" + this._tb_host.Text + "'");
            if (dra.Length > 0)
            {
                // at first removing the grants from the database if there had been any
                System.Data.DataRow[] dbPrivs = dtDbPrivs.Select("User = '" + this._tb_user.Text + "' AND Host = '" + this._tb_host.Text + "'");
                if (dbPrivs.Length > 1)
                {
                    foreach (System.Data.DataRow drPriv in dbPrivs)
                    {
                        qry = "REVOKE ALL PRIVILEGES ON " + drPriv["Db"].ToString() + ".* FROM '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
                        if (!dbh.ExecuteNonQuery(qry))
                        {
                            AddLogEntry("Unable to remove grants on " + drPriv["Db"].ToString() + " for user '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "':\n " + qry + "\n" + dbh.ErrExc.Message.ToString(), true);
                            return;
                        }
                    }
                }
                // now setting the grants
                foreach (System.Data.DataRow dr in dra)
                {
                    qry = null;
                    if (dr["Select_priv"].ToString() == "Y")
                    {
                        qry = "GRANT SELECT";
                    }
                    if (dr["Insert_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT INSERT";
                        else qry += ", INSERT";
                    }
                    if (dr["Update_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT UPDATE";
                        else qry += ", UPDATE";
                    }
                    if (dr["Delete_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT DELETE";
                        else qry += ", DELETE";
                    }
                    if (dr["Create_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT CREATE";
                        else qry += ", CREATE";
                    }
                    if (dr["Index_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT INDEX";
                        else qry += ", INDEX";
                    }
                    if (dr["Alter_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT ALTER";
                        else qry += ", ALTER";
                    }
                    if (dr["Drop_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT DROP";
                        else qry += ", DROP";
                    }
                    if (dr["Create_tmp_table_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT CREATE TEMPORARY TABLE";
                        else qry += ", CREATE TEMPORARY TABLE";
                    }
                    if (dr["Grant_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT GRANT";
                        else qry += ", GRANT";
                    }
                    if (dr["Lock_tables_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT LOCK TABLES";
                        else qry += ", LOCK TABLES";
                    }
                    if (dr["References_priv"].ToString() == "Y")
                    {
                        if (qry == null) qry = "GRANT REFERENCES";
                        else qry += ", REFERENCES";
                    }
                    // finalizing query
                    if (qry != null)
                    {
                        qry += " ON " + dr["Db"].ToString() + ".* TO '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "'";
                        if (dbh.ExecuteNonQuery(qry))
                        {
                            AddLogEntry("Privileges updated for '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "' on " + dr["Db"].ToString() + ".", false);
                        }
                        else
                        {
                            AddLogEntry("Unable to update privileges for '" + this._tb_user.Text + "'@'" + this._tb_host.Text + "' on " + dr["Db"].ToString() + ":\n " + qry + "\n" + dbh.ErrExc.Message.ToString(), true);
                            return;
                        }
                    }
                }
                // reset userchanged
                dbprivsChanged = false;
                GetInitData();
            }
        }
        #endregion

        #region gui functions
        private void ResetDialog()
        {   // reset the dialog to the default
            this._tb_user.Text = String.Empty;
            this._tb_host.Text = String.Empty;
            this._tb_passw.Text = String.Empty;
            this._tb_passw2.Text = String.Empty;
            this._cb_privpreselection.SelectedIndex = 0;
            this._ckb_grantopt.Checked = false;
            // clearing listviews
            DeSelectChkLView(this._lv_gprivs);
            DeSelectChkLView(this._lv_dbprivs);
        }
        private void EnableDialog()
        {   // enabling the input items
            this._tb_user.Enabled = true;
            this._tb_user.ReadOnly = false;
            this._tb_host.Enabled = true;
            this._tb_host.ReadOnly = false;
            this._tb_passw.Enabled = true;
            this._tb_passw2.Enabled = true;
            this._cb_privpreselection.Enabled = true;
            if (this._cb_privpreselection.SelectedIndex == 0)
            {
                this._gb_globalprivs.Enabled = true;
                this._tabc.TabPages["dbprivs"].Enabled = true;
                this._tabc.TabPages["tblprivs"].Enabled = true;
            }
        }
        private void DisableDialog()
        {   // disabling the input items
            this._tb_user.Enabled = false;
            this._tb_host.Enabled = false;
            this._tb_passw.Enabled = false;
            this._tb_passw2.Enabled = false;
            this._gb_globalprivs.Enabled = false;
            this._tabc.TabPages["dbgrants"].Enabled = false;
            this._tabc.TabPages["tblgrants"].Enabled = false;
        }
        private void RemoveGlobalPriv(string grant)
        {
            foreach (ListViewItem lvi in this._lv_gprivs.Items)
                if (lvi.Text == grant)
                    this._lv_gprivs.Items.Remove(lvi);
        }
        private void CheckPriv(ListView.ListViewItemCollection lviCol, string priv, bool check)
        {
            foreach (ListViewItem lvi in lviCol)
                if (lvi.Text == priv)
                    lvi.Checked = check;
        }
        private void AddLogEntry(string logentry, bool error)
        {
            if (logentry != null && logentry != string.Empty)
            {   // clear log if we got more than 50 lines
                if (this._lv_log.Items.Count == 50)
                    this._lv_log.Items.RemoveAt(0);
                // adding log msg
                this._lv_log.Items.Add(logentry);
                // selecting last entry
                int lastentry = this._lv_log.Items.Count - 1;
                this._lv_log.Items[lastentry].EnsureVisible();
                // check if error?
                if (error)
                    this._lv_log.Items[lastentry].ForeColor = System.Drawing.Color.Red;
            }
        }
        private bool ChkUserInpt()
        {   // checkup if the userinput is correct
            if (this._tb_user.Text == null)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidUser"), MessageHandler.EnumMsgType.Warning);
                this._tb_user.Focus();
                return false;
            }
            else if (this._tb_host.Text == null)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnInvalidHost"), MessageHandler.EnumMsgType.Warning);
                this._tb_host.Focus();
                return false;
            }
            else if (this._tb_passw.Text != this._tb_passw2.Text)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnPasswordMissmatch"), MessageHandler.EnumMsgType.Warning);
                this._tb_passw.Text = String.Empty;
                this._tb_passw2.Text = String.Empty;
                return false;
            }
            else if (this._tb_passw.Text.Length == 0)
            {
                DialogResult dRes = MessageBox.Show(GuiHelper.GetResourceMsg("QstNoPassword"), "Empty Password", 
                                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes == DialogResult.No)
                {
                    this._tb_passw.Focus();
                    return false;
                }
            }
            return true;
        }
        private void SetGuiDbPrivs()
        {   // delsectall
            stopUpdate = true;
            DeSelectChkLView(this._lv_dbprivs);
            // parsing user details
            System.Data.DataRow[] dr = dsUserPrivs.Tables[1].Select("User = '" + this._tb_user.Text + "' AND Host = '" + this._tb_host.Text + "' AND Db = '" + this._lbl_dbsel.Text + "'");
            if (dr.Length == 1)
            {   // parsing privileges
                if (dr[0]["Select_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Select", true);
                if (dr[0]["Insert_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Insert", true);
                if (dr[0]["Update_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Update", true);
                if (dr[0]["Delete_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Delete", true);
                if (dr[0]["Create_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Create", true);
                if (dr[0]["Index_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Index", true);
                if (dr[0]["Alter_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Alter", true);
                if (dr[0]["Drop_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Drop", true);
                if (dr[0]["Create_tmp_table_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Create temporary tables", true);
                if (dr[0]["Grant_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Grant", true);
                if (dr[0]["Lock_tables_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "Lock tables", true);
                if (dr[0]["References_priv"].ToString() == "Y")
                    CheckPriv(this._lv_dbprivs.Items, "References", true);
            }
            // enabling dialog
            this._lv_dbprivs.Enabled = true;
            // reset stopupdate bool
            stopUpdate = false;
        }
        private void ApplyClick()
        {
            // user / database or tables priv?
            if (this._tabc.SelectedIndex == 0)
            {
                // check 
                if (ChkUserInpt())
                {
                    // at first, edit or new user?
                    System.Data.DataRow[] dr = dsUserPrivs.Tables[0].Select("User = '" + this._tb_user.Text + "' AND Host = '" + this._tb_host.Text + "'");
                    if (dr.Length > 0)
                        EditUser();
                    else
                        AddUser();
                }
            }
            else if (this._tabc.SelectedIndex == 1)
            {   // databases
                stopUpdate = true;
                SetDbPrivs();
                stopUpdate = false;
            }
            else if (this._tabc.SelectedIndex == 2)
            {   // tables
            }
        }
        #endregion

        #region helpfunctions
        private void SelectChkLView(ListView lvi)
        {
            ListView.ListViewItemCollection lviColl = lvi.Items;
            if (lviColl.Count > 0)
            {
                foreach (ListViewItem lvi_ in lviColl)
                    lvi_.Checked = true;
            }
        }
        private void DeSelectChkLView(ListView lvi)
        {
            ListView.ListViewItemCollection lviColl = lvi.Items;
            if (lviColl.Count > 0)
            {
                foreach (ListViewItem lvi_ in lviColl)
                    lvi_.Checked = false;
            }
        }
        private void SetDSDBPrivs(System.Data.DataRow dr, string priv, string setto)
        {
            dr.BeginEdit();
            dr[priv] = setto;
            dr.EndEdit();
        }
        private void ChkUserChanged()
        {
            if (userChanged)
            {
                DialogResult dRes = MessageBox.Show("Submit user privilege changes?", "Submit changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes == DialogResult.Yes)
                    ApplyClick();
                else
                {   // reset the gui to default
                    stopUpdate = true;
                    GetInitData();
                    SetUserData();
                    this._tabc.SelectedIndex = 0;
                    stopUpdate = false;
                }
                // reset userchanged
                userChanged = false;
            }
        }
        private void ChkDbPrivsChanged()
        {
            if (dbprivsChanged)
            {
                DialogResult dRes = MessageBox.Show("Submit db privilege changes?", "Submit changes", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dRes == DialogResult.Yes)
                    ApplyClick();
                else
                {   // reset the gui to default
                    GetInitData();
                    stopUpdate = true;
                    SetUserData();
                    stopUpdate = false;
                    this._tabc.SelectedIndex = 0;
                }
                // reset userchanged
                dbprivsChanged = false;
            }
        }
        #endregion

        #region menuestrip
        private void _ms_exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _ms_delete_Click(object sender, EventArgs e)
        {
            RemoveUser();
        }
        private void _ms_new_Click(object sender, EventArgs e)
        {
            userChanged = true;
            EnableDialog();
            ResetDialog();
            this._tb_user.Focus();
            this._cb_privpreselection.SelectedIndex = 0;
        }
        private void _ms_apply_Click(object sender, EventArgs e)
        {
            ApplyClick();
        }
        #endregion

        #region cm listview
        private void _cmlv_selall_Click(object sender, EventArgs e)
        {   // which listview?
            if (this._tabc.SelectedIndex == 0)
                GuiHelper.ListViewCheckedState(this._lv_gprivs, GuiHelper.EnumLVSelection.Select);
            else
                GuiHelper.ListViewCheckedState(this._lv_dbprivs, GuiHelper.EnumLVSelection.Select);
        }
        private void _cmlv_selnone_Click(object sender, EventArgs e)
        {   // which listview?
            if (this._tabc.SelectedIndex == 0)
                GuiHelper.ListViewCheckedState(this._lv_gprivs, GuiHelper.EnumLVSelection.Unselect);
            else
                GuiHelper.ListViewCheckedState(this._lv_dbprivs, GuiHelper.EnumLVSelection.Unselect);
        }
        private void _cmlv_invertsel_Click(object sender, EventArgs e)
        {   // which listview?
            if (this._tabc.SelectedIndex == 0)
                GuiHelper.ListViewCheckedState(this._lv_gprivs, GuiHelper.EnumLVSelection.Invert);
            else
                GuiHelper.ListViewCheckedState(this._lv_dbprivs, GuiHelper.EnumLVSelection.Invert);
        }
        #endregion

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}