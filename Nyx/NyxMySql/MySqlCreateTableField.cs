/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Nyx.AppDialogs;

namespace Nyx.NyxMySql
{
    /// <summary> MySql create tablefield dialog </summary>
    public partial class MySqlCreateTableField : Form
    {
        /// <remarks> event which gets fired when a table was created and the dialog is closed </remarks>
        public event EventHandler<DBContentChangedEventArgs> DBContentChangedEvent;
        // private vars
        private TableAction tblAction;
        private string table = null, field = null;
        Classes.DBHelper dbh = new Classes.DBHelper(NyxMain.ConProfile.ProfileDBType);

        /// <summary>
        /// init function
        /// </summary>
        /// <param name="fieldAction"></param>
        /// <param name="tableName"></param>
        /// <param name="fieldName"></param>
        public MySqlCreateTableField(TableAction fieldAction, string tableName, string fieldName)
        {   // transfer vars into local
            tblAction = fieldAction;
            table = tableName;
            field = fieldName;
            // check
            InitializeComponent();
            // gui
            if (tblAction == TableAction.FieldAdd)
            {
                this._cbPosition.Items.Add("Last");
                this._cbPosition.Items.Add("First");
                this._btOk.Text = "Create";
                // title
                this.Text = GuiHelper.GetResourceMsg("CTF_FieldCreateHl", new string[] { table });
            }
            else
            {
                this._cbPosition.Items.Add("not changed");
                this._btOk.Text = "Alter";
                // title
                this.Text = GuiHelper.GetResourceMsg("CTF_FieldAlterHl", new string[] { field, table });
            }
            // init charsets
            foreach (string charset in dbh.Encodings)
                this._cbFldCharset.Items.Add(charset);
            // init collate
            foreach (string collate in dbh.Collations)
                this._cbFldCollate.Items.Add(collate);
        }
        private void MySqlCreateTableField_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // retrieve fields of the table
            GetFields();
            // alter or create?
            if (tblAction == TableAction.FieldAlter)
            {   // get old field definition
                string qry = "SHOW FULL COLUMNS FROM " + table;
                if (dbh.ConnectDB(NyxMain.ConProfile, false))
                {
                    if (dbh.ExecuteReader(qry))
                    {   // datareader
                        Classes.TableDetails strTblDesc;
                        // read
                        while (dbh.Read())
                        {
                            if (dbh.GetValue(0).ToString() == field)
                            {   // fill tabledescribe
                                strTblDesc = new Classes.TableDetails();
                                strTblDesc.Name = field;
                                strTblDesc.Collation = dbh.GetValue(2).ToString();
                                strTblDesc.Null = dbh.GetValue(3).ToString().ToUpper(NyxMain.NyxCI);
                                strTblDesc.HasIndex = dbh.GetValue(4).ToString().ToUpper(NyxMain.NyxCI);
                                strTblDesc.DefaultValue = dbh.GetValue(5).ToString();
                                // fix for mysql 4.0 (comment didn't exist this long time ago...)
                                if (dbh.FieldCount == 8)
                                    strTblDesc.Comment = dbh.GetValue(8).ToString();
                                strTblDesc.Extra = dbh.GetValue(6).ToString().ToUpper(NyxMain.NyxCI);
                                // formating type
                                string tmp = dbh.GetValue(1).ToString();
                                int strPos = tmp.IndexOf("(");
                                int endPos = tmp.LastIndexOf(")");
                                if (strPos != -1)
                                    strTblDesc.Type = tmp.Substring(0, tmp.Length - (tmp.Length - strPos)).ToUpper(NyxMain.NyxCI);
                                else
                                    strTblDesc.Type = tmp.ToUpper(NyxMain.NyxCI);
                                // setting length
                                if (strPos != -1 && endPos != -1)
                                    strTblDesc.Lenght = tmp.Substring(strPos + 1, endPos - strPos - 1);
                                // cutting tmp for the attributes
                                if (tmp.Length > endPos + 2)
                                    strTblDesc.Attribute = tmp.Substring(endPos + 2).ToUpper(NyxMain.NyxCI);

                                // set gui
                                this._tbFldName.Text = strTblDesc.Name;
                                this._cbFldType.SelectedItem = strTblDesc.Type;
                                this._mtbFldLength.Text = strTblDesc.Lenght;
                                if (strTblDesc.Collation != "NULL")
                                    this._cbFldCollate.Text = strTblDesc.Collation;
                                this._tbFldDefault.Text = strTblDesc.DefaultValue;
                                this._tbFldComment.Text = strTblDesc.Comment;
                                switch (strTblDesc.Attribute)
                                {
                                    case "ZEROFILL":
                                        this._ckbFldZerofill.Checked = true;
                                        break;
                                    case "UNSIGNED":
                                        this._ckbFldUnsigned.Checked = true;
                                        break;
                                    case "UNSIGNED ZEROFILL":
                                        this._ckbFldUnsigned.Checked = true;
                                        this._ckbFldZerofill.Checked = true;
                                        break;
                                    case "ON_UPDATE SET CURRENT TIMESTAMP":
                                        this._ckbFldOnUpdate.Checked = true;
                                        break;
                                }
                                // null?
                                if (strTblDesc.Null == "YES")
                                    this._cbFldNull.SelectedIndex = 1;
                                else
                                    this._cbFldNull.SelectedIndex = 2;
                                // extra (auto_increment) ?
                                if (strTblDesc.Extra == "AUTO_INCREMENT")
                                    this._ckbFldAutoInc.Checked = true;
                                break;
                            }
                        }
                        // enabling button
                        dbh.Close();
                    }
                    else
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CTF_Get", new string[] { dbh.ErrExc.Message.ToString() }),
                                            MessageHandler.EnumMsgType.Warning);
                        // disconnect
                        dbh.DisconnectDB();
                        // close
                        this.Close();
                    }
                }
                else
                {   // error creating db connection
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                        dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    // close
                    this.Close();
                }
                // disconnect db again
                dbh.DisconnectDB();
            }
        }
        private void MySqlCreateTableField_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }

        private void GetFields()
        {
            if (dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                // getting all tablefields
                if (dbh.ExecuteReader("describe " + table))
                {   // filling the combobox
                    while (dbh.Read())
                    {
                        if (tblAction == TableAction.FieldAlter
                            && dbh.GetValue(0).ToString() == field)
                            continue;
                        else
                            this._cbPosition.Items.Add("AFTER " + dbh.GetValue(0).ToString());
                    }
                    this._cbPosition.SelectedIndex = 0;
                    // cleanup
                    dbh.Close();
                }
                else
                {   // error
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrGetTableFields", new string[] { table, dbh.ErrExc.Message.ToString() }),
                        MessageHandler.EnumMsgType.Warning);
                }
                // disconnect database
                dbh.DisconnectDB();
            }
            else
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
        }
        private void _btCreate_Click(object sender, EventArgs e)
        {   // init vars
            string qry = String.Empty, fldAutoInc = String.Empty, fldBinary = String.Empty, fldOnUpdate = String.Empty,
                    fldUnsigned = String.Empty, fldZerofill = String.Empty;
            if (this._ckbFldAutoInc.Checked)
                fldAutoInc = "Y";
            if (this._ckbFldBinary.Checked)
                fldBinary = "Y";
            if (this._ckbFldOnUpdate.Checked)
                fldOnUpdate = "Y";
            if (this._ckbFldUnsigned.Checked)
                fldUnsigned = "Y";
            if (this._ckbFldZerofill.Checked)
                fldZerofill = "Y";
            // build column definition
            string createDef = MySqlHelper.BuildColumenDefinition(this._tbFldName.Text.ToString().Trim(),
                    this._cbFldType.Text.ToString(),
                    this._mtbFldLength.Text.ToString().Trim(),
                    fldUnsigned, fldAutoInc, fldZerofill,
                    this._cbFldCharset.Text.ToString().Trim(),
                    this._cbFldCollate.Text.ToString().Trim(),
                    fldBinary, fldOnUpdate,
                    this._tbFldValues.Text.ToString());
            // building db connection
            if (dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                // check what todo
                if (tblAction == TableAction.FieldAdd)
                {
                    qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} ADD COLUMN {1}", table, createDef);
                    // exec query
                    if (dbh.ExecuteNonQuery(qry))
                    {   // fire refresh event
                        OnDBContentChanged();
                        // add another field?
                        StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("CTF_FieldCreated"),
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                        if (strRD.DRes == NyxDialogResult.Yes)
                            GetFields();
                        else
                            this.Close();
                    }
                    else
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CTF_CreateErr", new string[] { dbh.ErrExc.Message.ToString(), qry }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
                else if (tblAction == TableAction.FieldAlter)
                {   // execute change column
                    qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} CHANGE COLUMN {1} {2}", table, field, createDef);
                    if (dbh.ExecuteNonQuery(qry))
                    {   // close
                        OnDBContentChanged();
                        this.Close();
                    }
                    else
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CTF_AlterErr", new string[] { dbh.ErrExc.Message.ToString(), qry }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
            }
            else
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString(), qry }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
            // close dbconnection again
            dbh.DisconnectDB();
        }

        private void _tbFldName_TextChanged(object sender, EventArgs e)
        {
            if (this._tbFldName.Text.ToString().Length > 0)
                this._btOk.Enabled = true;
            else
                this._btOk.Enabled = false;
        }
        private void _cbFieldType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selitem = this._cbFldType.SelectedItem.ToString().ToUpper(NyxMain.NyxCI);
            switch (selitem)
            {
                case "BIT":         // BIT          [(length)]
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    break;
                case "TINYINT":     // TINYINT	    [(length)] [UNSIGNED] [ZEROFILL]
                case "SMALLINT":    // SMALLINT     [(length)] [UNSIGNED] [ZEROFILL]
                case "MEDIUMINT":   // MEDIUMINT    [(length)] [UNSIGNED] [ZEROFILL]
                case "INT":         // INT          [(length)] [UNSIGNED] [ZEROFILL]
                case "INTEGER":     // INTEGER      [(length)] [UNSIGNED] [ZEROFILL]
                case "BIGINT":      // BIGINT       [(length)] [UNSIGNED] [ZEROFILL]
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = true;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = true;
                    this._ckbFldZerofill.Enabled = true;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "REAL":        // REAL         [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "DOUBLE":      // DOUBLE       [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "FLOAT":       // FLOAT        [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "DECIMAL":     // DECIMAL		(length,decimals) [UNSIGNED] [ZEROFILL]
                case "NUMERIC":     // NUMERIC 	    (length,decimals) [UNSIGNED] [ZEROFILL]
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = true;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = true;
                    this._ckbFldZerofill.Enabled = true;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "TIMESTAMP":   // TIMESTAMP
                    this._mtbFldLength.Enabled = false;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = true;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "DATE":        // DATE
                case "TIME":        // TIME
                case "DATETIME":    // DATETIME
                case "YEAR":        // YEAR
                case "TINYBLOB":    // TINYBLOB
                case "BLOB":        // BLOB
                case "MEDIUMBLOB":  // MEDIUMBLOB
                case "LONGBLOB":    // LONGBLOB
                    this._mtbFldLength.Enabled = false;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "CHAR":        // CHAR		    (length) [CHARACTER SET charset_name] [COLLATE collation_name] -fulltext
                case "VARCHAR":     // VARCHAR		(length) [CHARACTER SET charset_name] [COLLATE collation_name] -fulltext
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = true;
                    this._cbFldCharset.Enabled = true;
                    this._tbFldValues.Enabled = false;
                    break;
                case "BINARY":      // BINARY		(length)
                case "VARBINARY":   // VARBINARY	(length)
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "TINYTEXT":    // TINYTEXT 	[BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext 
                case "TEXT":        // TEXT 		[BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                case "MEDIUMTEXT":  // MEDIUMTEXT   [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                case "LONGTEXT":    // LONGTEXT     [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                    this._mtbFldLength.Enabled = false;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = true;
                    this._cbFldCollate.Enabled = true;
                    this._cbFldCharset.Enabled = true;
                    this._tbFldValues.Enabled = false;
                    break;
                case "ENUM":        // ENUM        (value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
                case "SET":         // SET         (value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
                    this._mtbFldLength.Enabled = false;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = true;
                    this._cbFldCharset.Enabled = true;
                    this._tbFldValues.Enabled = true;
                    break;
            }
        }
        private void _btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Fire OnDBContentChanged event
        /// </summary>
        protected void OnDBContentChanged()
        {   // fire event
            DBContentChangedEventArgs e = new DBContentChangedEventArgs();
            e.DBContent = DatabaseContent.TableFields;
            e.Param1 = table;

            if (DBContentChangedEvent != null)
                DBContentChangedEvent(this, e);
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}