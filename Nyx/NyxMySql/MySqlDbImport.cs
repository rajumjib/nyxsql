/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using MySql.Data.MySqlClient;
using Nyx.Classes;
using Nyx.AppDialogs;
using NyxSettings;
using System.Windows.Forms;
using System.IO;

namespace Nyx.NyxMySql
{
    /// <summary>
    /// MySql DatabaseImport GUI
    /// </summary>
    public partial class MySqlDBImport : Form
    {   // helpvars
        private System.Threading.Thread thread;
        private string impFile;
        private string impError;
        private int rowcount;
        private bool impSuccessfull = true;
        // options
        private string[] impOptions = new string[2] { "", "" };
        private Char[] impSep;
        private string[] impColumns;
        // init dbhelper
        DBHelper dbh = new DBHelper(DBType.MySql);
        
        /// <summary>
        /// Init
        /// </summary>
        public MySqlDBImport()
        {
            InitializeComponent();
        }
        private void FrmDbImport_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // pre-set gui
            this._cbCustFileType.SelectedIndex = 0;
            // getting tables
            if (!dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
            if (dbh.ExecuteReader("show tables"))
            {   // read
                while (dbh.Read())
                    this._cbCustTable.Items.Add(dbh.GetValue(0).ToString());
                dbh.Close();
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrRetTable", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this._tcImport.TabPages["custimp"].Enabled = false;
            }
        }

        private void _btSelFile_Click(object sender, EventArgs e)
        {   // get file
            if (GuiHelper.OpenFileDialog("SQL-Files (*.sql)|*.sql|All Files (*.*)|*.*"))
            {
                string fullpath = GuiHelper.Fullpath;
                if (ChkFile(fullpath))
                    this._tbSqlFile.Text = fullpath;
            }
        }
        private void _btCustSelFile_Click(object sender, EventArgs e)
        {   // get file
            if (GuiHelper.OpenFileDialog("Coma seperated (*.csv)|*.csv|Text files (*.txt)|*.txt|Tab seperated (*.tsv)|*tsv|All files (*.*)|*.*"))
            {
                string fullpath = GuiHelper.Fullpath;
                if (ChkFile(fullpath))
                    this._tbCustFile.Text = fullpath;
            }
        }
        private void _msImport_Click(object sender, EventArgs e)
        {
            // gui
            this._pnlControl.Enabled = false;
            this._tcImport.Enabled = false;
            // progressbar
            this._ssPgBar.Style = ProgressBarStyle.Marquee;
            // calling 
            if (this._tcImport.SelectedIndex == 0)
                ThrSQLImport();
            else if (this._tcImport.SelectedIndex == 1)
                ThrCustImport();
        }
        private void _btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _btLoadFilePrompt_Click(object sender, EventArgs e)
        {
            if (this._tcImport.SelectedIndex == 0)
            {
                // check file
                if (this._tbSqlFile.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnNoFileSelected"), MessageHandler.EnumMsgType.Warning);
                    return;
                }
                if (!File.Exists(this._tbSqlFile.Text))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnFileNotfound"), MessageHandler.EnumMsgType.Warning);
                    return;
                }
                // create process to start the default viewapp
                System.Diagnostics.Process.Start(this._tbSqlFile.Text);
            }
            else if (this._tcImport.SelectedIndex == 1)
            {
                // check file
                if (this._tbCustFile.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnNoFileSelected"), MessageHandler.EnumMsgType.Warning);
                    return;
                }
                if (!File.Exists(this._tbCustFile.Text))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnfileNotFound"), MessageHandler.EnumMsgType.Warning);
                    return;
                }
                // create process to start the default viewapp
                System.Diagnostics.Process.Start(this._tbCustFile.Text);
            }
        }
        private void _tbSqlFile_TextChanged(object sender, EventArgs e)
        {
            CheckSqlFileInput();
        }
        private void CheckSqlFileInput()
        {
            if (this._tbSqlFile.Text.Length > 0)
            {
                this._btOk.Enabled = true;
                this._btOpenImp.Enabled = true;
            }
            else
            {
                this._btOk.Enabled = false;
                this._btOpenImp.Enabled = false;
            }
        }
        private void _tbCustFile_TextChanged(object sender, EventArgs e)
        {
            CheckCustFileInput();
        }
        private void CheckCustFileInput()
        {
            if (this._tbCustFile.Text.Length > 0)
            {
                if (this._lvCustColumns.CheckedItems.Count > 0)
                    this._btOk.Enabled = true;
                this._btOpenImp.Enabled = true;
            }
            else
            {
                this._btOk.Enabled = false;
                this._btOpenImp.Enabled = false;
            }
        }
        private void _tabc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._tcImport.SelectedTab == this._tpSqlImp)
                CheckSqlFileInput();
            else if (this._tcImport.SelectedTab == this._tpCustImp)
                CheckCustFileInput();
        }
        private void _lvCustColumns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (this._tbCustFile.Text.Length > 0 && this._lvCustColumns.CheckedItems.Count > 0)
                this._btOk.Enabled = true;
            else
                this._btOk.Enabled = false;
        }
        private void _cbCustTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            // clearing up listview
            this._lvCustColumns.Items.Clear();
            // refreshing listview
            if (dbh.ExecuteReader("describe " + this._cbCustTable.Text))
            {   // read
                while (dbh.Read())
                {
                    ListViewItem lvi = GuiHelper.BuildListViewItem(dbh.GetValue(0).ToString(),dbh.GetValue(1).ToString());
                    this._lvCustColumns.Items.Add(lvi);
                }
                dbh.Close();
                this._lvCustColumns.Enabled = true;
            }
            else
                this._lvCustColumns.Enabled = false;
        }
        private void _btUp_Click(object sender, EventArgs e)
        {
            if (this._lvCustColumns.SelectedItems.Count == 1)
            {
                int Idx = this._lvCustColumns.SelectedIndices[0];
                if (Idx - 1 >= 0)
                {   // tmp helpvar
                    string[] tmp = new string[2] { "", "" };
                    tmp[0] = this._lvCustColumns.Items[Idx - 1].Text;
                    tmp[1] = this._lvCustColumns.Items[Idx - 1].SubItems[1].Text;
                    // switching items
                    this._lvCustColumns.Items[Idx - 1].Text = this._lvCustColumns.Items[Idx].Text;
                    this._lvCustColumns.Items[Idx - 1].SubItems[1].Text = this._lvCustColumns.Items[Idx].SubItems[1].Text;
                    this._lvCustColumns.Items[Idx].Text = tmp[0];
                    this._lvCustColumns.Items[Idx].SubItems[1].Text = tmp[1];
                    // moving selection up
                    this._lvCustColumns.Items[Idx - 1].Selected = true;
                }
            }
        }
        private void _btDown_Click(object sender, EventArgs e)
        {
            if (this._lvCustColumns.SelectedItems.Count == 1)
            {
                int Idx = this._lvCustColumns.SelectedIndices[0];
                if (this._lvCustColumns.Items.Count > Idx + 1)
                {   // tmp helpvar
                    string[] tmp = new string[2] { "", "" };
                    tmp[0] = this._lvCustColumns.Items[Idx + 1].Text;
                    tmp[1] = this._lvCustColumns.Items[Idx + 1].SubItems[1].Text;
                    // switching items
                    this._lvCustColumns.Items[Idx + 1].Text = this._lvCustColumns.Items[Idx].Text;
                    this._lvCustColumns.Items[Idx + 1].SubItems[1].Text = this._lvCustColumns.Items[Idx].SubItems[1].Text;
                    this._lvCustColumns.Items[Idx].Text = tmp[0];
                    this._lvCustColumns.Items[Idx].SubItems[1].Text = tmp[1];
                    // moving selection down
                    this._lvCustColumns.Items[Idx + 1].Selected = true;
                }
            }
        }

        private void _cmlvSelAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lv in this._lvCustColumns.Items)
                lv.Checked = true;
        }
        private void _cmlvSelNone_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lv in this._lvCustColumns.Items)
                lv.Checked = false;
        }
        private void _cmlvInvertSel_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem lv in this._lvCustColumns.Items)
                if (lv.Checked)
                    lv.Checked = false;
                else
                    lv.Checked = true;
        }

        private void ThrSQLImport()
        {   
            // vars
            impFile = this._tbSqlFile.Text;
            // threadstart
            thread = new System.Threading.Thread(new System.Threading.ThreadStart(ImportSQLFile));
            thread.IsBackground = true;
            thread.Start();
        }
        private void ThrCustImport()
        {
            // vars
            impFile = this._tbCustFile.Text;
            // setting options and file
            if (this._cbCustFileType.Text.Length > 0)
            {
                if (this._cbCustFileType.Text == "tab")
                    impSep = new Char[1] { '\t' };
                else if(this._cbCustFileType.Text.Length == 1)
                    impSep = new Char[1] { this._cbCustFileType.Text.Substring(0,1).ToCharArray()[0] };
                else
                    impSep = new Char[1] { ',' }; 
            }
            impOptions[1] = this._cbCustTable.Text;
            // putting columns into an array
            impColumns = new string[this._lvCustColumns.CheckedItems.Count];
            for (int i = 0; i < this._lvCustColumns.CheckedItems.Count; i++)
            {
                impColumns[i] = this._lvCustColumns.CheckedItems[i].Text;
            }
            // threadstart
            thread = new System.Threading.Thread(new System.Threading.ThreadStart(ImportCustFile));
            thread.IsBackground = true;
            thread.Start();
        }
        private void ThrStopped()
        {   // gui
            this._tslblCurRowCount.Text = rowcount.ToString(NyxMain.NyxCI);
            rowcount = 0;
            this._tcImport.Enabled = true;
            this._pnlControl.Enabled = true;
            // progressbar
            this._ssPgBar.Value = 0;
            this._ssPgBar.Style = ProgressBarStyle.Blocks;
            // messages
            if (impSuccessfull)
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBImp_Finished"), MessageHandler.EnumMsgType.Information);
            else
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBImp_ImportErr", new string[] { impError }), 
                    MessageHandler.EnumMsgType.Warning);
        }
        private void ImportSQLFile()
        {
            // vars
            StreamReader sr = null;
            char readin;
            System.Text.StringBuilder sbQry = new System.Text.StringBuilder();
            // tryin
            try
            {
                sr = File.OpenText(impFile);
                // readin of file
                using (sr = new StreamReader(impFile))
                {
                    char lastchar = ' ';
                    while (sr.Peek() >= 0)
                    {
                        readin = (char)sr.Read();
                        if (readin == ';' && (lastchar == ')' && lastchar != '\\'))
                        {
                            if (!dbh.ExecuteNonQuery(sbQry.ToString()))
                            {
                                impError = GuiHelper.GetResourceMsg("DBImp_ImportErr", new string[] { dbh.ErrExc.Message.ToString(), sbQry.ToString() });
                                impSuccessfull = false;
                                // cleanup
                                sr.Close();
                                break;
                            }
                            rowcount += dbh.RecordsAffected;
                            // act display?
                            MethodInvoker actdsp = new MethodInvoker(ActImpRows);
                            this.BeginInvoke(actdsp);
                            sbQry = new System.Text.StringBuilder();
                        }
                        else
                            sbQry.Append(readin);
                        // setting lastchar
                        lastchar = readin;
                        /*
                        exp_rep = exp_rep.Replace(";", "\\;");
                        exp_rep = exp_rep.Replace("'", "''");
                        exp_rep = exp_rep.Replace("\"", "\"\"");
                        exp_rep = exp_rep.Replace("%", "\\%");
                        exp_rep = exp_rep.Replace("_", "\\_");
                        exp_rep = exp_rep.Replace(@"\", @"\\");
                        exp_rep = exp_rep.Replace("\n", "\\n");
                        */
                    }
                }
            }
            catch (UnauthorizedAccessException uaexc)
            {
                impError = "Error importing file! (" + uaexc.Message + ")";
                impSuccessfull = false;
            }
            catch (IOException ioexc)
            {
                impError = "Error importing file! (" + ioexc.Message + ")";
                impSuccessfull = false;
            }
            finally { dbh.DisconnectDB(); }
            // invoke stop thread method
            MethodInvoker thrstopped = new MethodInvoker(ThrStopped);
            this.BeginInvoke(thrstopped);
        }
        private void ImportCustFile()
        {
            // dbconnect
            if (!dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                impError = String.Format(NyxMain.NyxCI, GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.DBName, dbh.ErrExc.Message.ToString() } ));
                impSuccessfull = false;
            }
            else
            {   // vars
                StreamReader sr = null;
                System.Text.StringBuilder sbPreQry = new System.Text.StringBuilder();
                System.Text.StringBuilder sbQry = new System.Text.StringBuilder();
                // building pre-query
                foreach (string colname in impColumns)
                {
                    if (sbPreQry.Length == 0)
                        sbPreQry.Append(String.Format(NyxMain.NyxCI, "INSERT INTO {0} ({1}", impOptions[1], colname));
                    else
                        sbPreQry.Append(String.Format(NyxMain.NyxCI, ",{0}", colname));
                }
                // finalize preqry
                sbPreQry.Append(") VALUES (");
                // tryin
                try
                {
                    sr = File.OpenText(impFile);
                    // readin of file
                    using (sr = new StreamReader(impFile))
                    {
                        string line = null;
                        string[] imp = null;
                        while ((line = sr.ReadLine()) != null)
                        {
                            imp = line.Split(impSep);
                            foreach(string qryAdd in imp)
                            {
                                if (sbQry.Length == 0)
                                    sbQry.Append(String.Format(NyxMain.NyxCI, "{0} '{1}'", sbPreQry.ToString(), qryAdd));
                                else
                                    sbQry.Append(String.Format(NyxMain.NyxCI, ",'{0}'", qryAdd));
                            }
                            // finalizing qry
                            if (sbQry.Length > 0)
                            {
                                sbQry.Append(")");
                                if (imp.Length == impColumns.Length)
                                {
                                    if (!dbh.ExecuteNonQuery(sbQry.ToString()))
                                    {
                                        impError = GuiHelper.GetResourceMsg("DBImp_ImportErr", new string[] { dbh.ErrExc.Message.ToString(), sbQry.ToString() });
                                        impSuccessfull = false;
                                        break;
                                    }
                                    rowcount += dbh.RecordsAffected;
                                    // act display?
                                    MethodInvoker actdsp = new MethodInvoker(ActImpRows);
                                    this.BeginInvoke(actdsp);
                                    sbQry = null;
                                }
                                else
                                {
                                    impError = "Count's don't match!";
                                    impSuccessfull = false;
                                }
                            }
                        }
                    }
                }
                catch (UnauthorizedAccessException uaexc)
                {
                    impError = GuiHelper.GetResourceMsg("DBImp_ImportErr", new string[] { uaexc.Message.ToString(), sbQry.ToString() });
                    impSuccessfull = false;
                }
                catch (IOException ioexc)
                {
                    impError = GuiHelper.GetResourceMsg("DBImp_ImportErr", new string[] { ioexc.Message.ToString(), sbQry.ToString() });
                    impSuccessfull = false;
                }
                finally 
                { 
                    dbh.DisconnectDB();
                    if(sr != null)
                        sr.Dispose();
                }
                // invoke stop thread method
                MethodInvoker thrstopped = new MethodInvoker(ThrStopped);
                this.BeginInvoke(thrstopped);
            }
        }
        private void ActImpRows()
        {
            this._tslblCurRowCount.Text = rowcount.ToString(NyxMain.NyxCI);
        }
        private static bool ChkFile(string filenmae)
        {
            // check file and
            FileInfo fi = new FileInfo(filenmae);
            if (fi.Exists)
            {
                StreamReader sr = null;
                // try to open the file
                try { sr = fi.OpenText(); }
                catch (UnauthorizedAccessException nexc)
                {
                    MessageHandler.ShowDialog("File is not in a valid format!", nexc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
                catch (System.Security.SecurityException sexc)
                {   // show error
                    MessageHandler.ShowDialog("Error opening input file!", sexc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
                catch (FileNotFoundException fnfexc)
                {   // show error
                    MessageHandler.ShowDialog("Error opening input file!", fnfexc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
                catch (DirectoryNotFoundException dnfexc)
                {   // show error
                    MessageHandler.ShowDialog("Error opening input file!", dnfexc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
                finally { if(sr != null) sr.Dispose(); }
            }
            else return false;
            // fannly returning true
            return true;
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}