/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using Nyx.AppDialogs;
using System.Collections;
using Nyx.Classes;

namespace Nyx.NyxMySql
{
    public partial class MySqlDBInfo : Form
    {   // dbhelper
        private DBHelper dbh = new DBHelper(NyxSettings.DBType.MySql);
        private Hashtable htStatus = new Hashtable();
        private Hashtable htVars = new Hashtable();
        private ListViewColumnSorter lvcsStatus = new ListViewColumnSorter();
        private ListViewColumnSorter lvcsVars = new ListViewColumnSorter();

        /// <summary>
        /// Init
        /// </summary>
        public MySqlDBInfo()
        {
            InitializeComponent();
            // assign listviewcolumnsorter
            this._lvStatus.ListViewItemSorter = lvcsStatus;
            this._lvVariables.ListViewItemSorter = lvcsVars;
        }
        private void MysqlDBInfo_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // init
            string qryVars = "SHOW variables";
            string qryStatus = "SHOW status";
            // opening connection
            if (dbh.ConnectDB(NyxMain.ConProfile, false))
            {   // executing status query
                if (dbh.ExecuteReader(qryStatus))
                {   // init vars
                    ListViewItem lvi = null;
                    string varName, varValue;
                    try
                    {   // read
                        while (dbh.Read())
                        {
                            varName = dbh.GetValue(0).ToString();
                            varValue = dbh.GetValue(1).ToString();

                            lvi = GuiHelper.BuildListViewItem(varName, varValue);
                            this._lvStatus.Items.Add(lvi);
                            htStatus.Add(varName, varValue);
                        }
                    }
                    catch (IndexOutOfRangeException ioexc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { ioexc.Message.ToString() }), ioexc,
                                                    MessageHandler.EnumMsgType.Error);
                        this._lvStatus.Items.Clear();
                    }
                    finally { dbh.Close(); }
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { dbh.ErrExc.Message.ToString() }), dbh.ErrExc,
                                                MessageHandler.EnumMsgType.Error);
                    this._lvStatus.Items.Clear();
                }
                // executing variables query
                if (dbh.ExecuteReader(qryVars))
                {   // init vars
                    ListViewItem lvi = null;
                    string varName, varValue;
                    try
                    {   // read
                        while (dbh.Read())
                        {
                            varName = dbh.GetValue(0).ToString();
                            varValue = dbh.GetValue(1).ToString();

                            lvi = GuiHelper.BuildListViewItem(varName, varValue);
                            this._lvVariables.Items.Add(lvi);
                            htVars.Add(varName, varValue);
                        }
                    }
                    catch (IndexOutOfRangeException ioexc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { ioexc.Message.ToString() }), 
                                                    ioexc, MessageHandler.EnumMsgType.Error);
                        this._lvVariables.Items.Clear();
                    }
                    finally { dbh.Close(); }
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                                                dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    this._lvVariables.Items.Clear();
                }
                // disconnect
                dbh.DisconnectDB();
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                dbh.DisconnectDB();
                this.Close();
            }
        }

        private void _tbFilter_TextChanged(object sender, EventArgs e)
        {   // define vars
            string filter = this._tbFilter.Text.ToUpper(NyxMain.NyxCI);
            Hashtable ht;
            ListView lv;
            // set listview/hashtable
            if (this._tc.SelectedTab == this._tpStatus)
            {
                lv = this._lvStatus;
                ht = htStatus;
            }
            else
            {
                lv = this._lvVariables;
                ht = htVars;
            }
            // filter out
            lv.BeginUpdate();
            lv.Items.Clear();
            foreach (DictionaryEntry de in ht)
            {
                string htKey = de.Key.ToString().ToUpper(NyxMain.NyxCI);
                if (htKey.Contains(filter))
                    lv.Items.Add(GuiHelper.BuildListViewItem(de.Key.ToString(), de.Value.ToString()));
            }
            lv.Sort();
            lv.EndUpdate();
        }
        private void _tc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._tbFilter.Text.Length > 0)
            {
                this._tbFilter.Text = String.Empty;
                ListView lv;
                Hashtable ht;

                if (this._tc.SelectedTab == this._tpStatus)
                {   // reset variables
                    lv = this._lvVariables;
                    ht = htVars;
                }
                else
                {   // reset status
                    lv = this._lvStatus;
                    ht = htStatus;
                }
                lv.BeginUpdate();
                lv.Items.Clear();
                foreach (DictionaryEntry de in ht)
                    lv.Items.Add(GuiHelper.BuildListViewItem(de.Key.ToString(), de.Value.ToString()));
                lv.Sort();
                lv.EndUpdate();
                
            }
        }
        private void _lvStatus_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvStatus, lvcsStatus, e.Column);
        }
        private void _lvVariables_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvVariables, lvcsVars, e.Column);
        }
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        private void MySqlDBInfo_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }
    }
}