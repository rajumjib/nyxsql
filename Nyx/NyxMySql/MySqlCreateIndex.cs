/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Nyx.AppDialogs;
using Nyx.Classes;

namespace Nyx.NyxMySql
{
    public partial class MySqlCreateIndex : Form
    {   /// <remarks> event which gets fired when a table was created and the dialog is closed </remarks>
        public event EventHandler<DBContentChangedEventArgs> DBContentChangedEvent;
        // classes
        private string tblName = String.Empty, idxName = String.Empty;
        private TableAction idxAction;
        DBHelper dbh = new DBHelper(NyxSettings.DBType.MySql);
        DBManagement dbm = new DBManagement(NyxSettings.DBType.MySql);

        /// <summary>
        /// Init
        /// </summary>
        public MySqlCreateIndex()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Init
        /// </summary>
        /// <param name="indexAction">indexaction</param>
        /// <param name="table">tablename</param>
        public MySqlCreateIndex(TableAction indexAction, string table)
        {   // init
            InitializeComponent();
            // set
            tblName = table;
            idxAction = indexAction;
        }
        /// <summary>
        /// Init
        /// </summary>
        /// <param name="indexAction">indexaction</param>
        /// <param name="table">tablename</param>
        /// <param name="index">indexname, if index shall be altered</param>
        /// <param name="unique">unique or not?</param>
        /// <param name="fulltext">fulltext or not?</param>
        public MySqlCreateIndex(TableAction indexAction, string table, string index, string unique, string fulltext)
        {   // init
            InitializeComponent();
            // set
            tblName = table;
            idxName = index;
            idxAction = indexAction;
            // set gui
            this._tbIdxName.Text = index;
            if (index != null && index == "PRIMARY")
                this._cbAdd.SelectedItem = "PRIMARY";
            else if (unique != null && unique == "Y")
                this._cbAdd.SelectedItem = "UNIQUE";
            else if (fulltext != null && fulltext == "Y")
                this._cbAdd.SelectedItem = "FULLTEXT";
        }
        
        private void MySqlCreateIndex_Load(object sender, EventArgs e)
        {   // fading
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // connect and retrieve first
            if (!dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), MessageHandler.EnumMsgType.Error);
                this.Close();
            }
            // getting tables
            Queue<string> qTables;
            if (dbm.GetTables(out qTables, true, dbh))
            {   // insert into cb
                foreach (string tbl in qTables)
                    this._cbTables.Items.Add(tbl);
                // if we got a tablename, set selection
                if (tblName.Length > 0)
                    this._cbTables.SelectedItem = tblName;
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CI_TablesRetrieveErr", new string[] { dbm.ErrExc.Message.ToString() }),
                    MessageHandler.EnumMsgType.Error);
                this.Close();
            }
            // alter index?
            if (idxAction == TableAction.IndexAlter)
            {   // retrieve indexdetails
                Queue<IndexDescribe> qIdxDesc = new Queue<IndexDescribe>();
                if (dbm.GetIndexDetails(tblName, idxName, out qIdxDesc, dbh))
                {
                    foreach (IndexDescribe strIdxDesc in qIdxDesc)
                    {   // parse datagridview
                        foreach (DataGridViewRow dgvr in this._dgFields.Rows)
                        {
                            if (dgvr.Cells["FieldName"].Value.ToString() == strIdxDesc.Name)
                                dgvr.Cells[0].Value = true;
                        }
                        // set idx type
                        if (this._cbType.SelectedIndex == -1)
                            this._cbType.SelectedItem = strIdxDesc.Type;
                    }
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CIGetDescribeErr", new string[] { dbm.ErrExc.Message.ToString() }),
                        MessageHandler.EnumMsgType.Error);
                    this.Close();
                }
            }
        }
        private void MySqlCreateIndex_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }
        private void FillIdxListBox()
        {   // cleanup
            this._dgFields.Rows.Clear();
            // adding new elements
            Queue<TableDetails> qTblDescribe;
            if (dbm.GetTableDetails(this._cbTables.SelectedItem.ToString(), out qTblDescribe, dbh))
            {   // add fields
                foreach (TableDetails sTblDescribe in qTblDescribe)
                {
                    this._dgFields.Rows.Add(new object[4] { false, sTblDescribe.Name, sTblDescribe.Type, "" });
                    if (sTblDescribe.Type.ToUpper(NyxMain.NyxCI).Contains("CHAR")
                        || sTblDescribe.Type.ToUpper(NyxMain.NyxCI).Contains("BINARY"))
                    {
                        DataGridViewRow dgvrLast = this._dgFields.Rows[this._dgFields.Rows.Count -1];
                        dgvrLast.Cells[3].ReadOnly = false;
                        dgvrLast.Cells[3].Value = "0";
                    }
                }
            }
            if (this._dgFields.Rows.Count == 0)
            {
                this._btOk.Enabled = false;
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CI_NoFieldsInIndex"), MessageHandler.EnumMsgType.Error);
            }
            else // activate gui
                this._btOk.Enabled = true;
        }

        private void _cbTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._cbTables.SelectedItem.ToString().Length > 0)
                FillIdxListBox();
            else
            {
                this._dgFields.Rows.Clear();
                this._btOk.Enabled = false;
            }
        }
        private void _btCreate_Click(object sender, EventArgs e)
        {   // get checked rows
            System.Collections.ArrayList alCheckedRows = new System.Collections.ArrayList();
            foreach (DataGridViewRow dgvr in this._dgFields.Rows)
            {
                if ((bool)dgvr.Cells[0].Value)
                    alCheckedRows.Add(dgvr);
            }
            // checkupsspending
            if (this._tbIdxName.Text.Length > 1 && this._cbAdd.SelectedItem.ToString() != "PRIMARY")
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CI_NoName"), MessageHandler.EnumMsgType.Information);
                this._tbIdxName.Focus();
            }
            else if (alCheckedRows.Count == 0)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CI_NoFieldsInIndex"), MessageHandler.EnumMsgType.Information);
                this._dgFields.Focus();
            }
            else
            {   // prepare query
                string qry = String.Empty;
                // alter index, drop first?
                if (idxAction == TableAction.IndexAlter)
                    qry = String.Format(NyxMain.NyxCI, "DROP INDEX `{0}` ON `{1}`;ALTER TABLE `{1}` ADD", idxName, tblName);
                else
                    qry = String.Format(NyxMain.NyxCI, "ALTER TABLE `{0}` ADD", tblName); ;
                // check additional attributes
                if (this._cbAdd.SelectedItem != null)
                    qry = String.Format(NyxMain.NyxCI, "{0} {1}", qry, this._cbAdd.Text.ToString());
                // primary?
                if(this._cbAdd.SelectedItem.ToString() == "PRIMARY")
                    qry += " KEY ";
                else
                    qry = String.Format(NyxMain.NyxCI, "{0} KEY `{1}` ", qry, this._tbIdxName.Text.ToString());
                // check type
                if (this._cbType.SelectedItem != null)
                    qry = String.Format(NyxMain.NyxCI, "{0} USING {1} (", qry, this._cbType.Text.ToString());
                else
                    qry += "(";
                // add fields
                string fields = String.Empty;
                foreach (DataGridViewRow dgvr in alCheckedRows)
                {   // preset vars
                    string field = dgvr.Cells["FieldName"].Value.ToString();
                    string useFirst = dgvr.Cells["UseFirst"].Value.ToString();
                    // build field
                    if (useFirst.Length > 0 && useFirst != "0")
                        field = String.Format(NyxMain.NyxCI, "{0}({1})", field, useFirst);
                    else
                        field = String.Format(NyxMain.NyxCI, "{0}", field);
                    // add field
                    if (fields.Length == 0)
                        fields = field;
                    else
                        fields = String.Format(NyxMain.NyxCI, "{0},{1}", fields, field);
                }
                // finalize columns
                qry = String.Format(NyxMain.NyxCI, "{0}{1})", qry, fields);

                // execute query
                if (dbh.ExecuteNonQuery(qry))
                {   // refresh database content
                    OnDBContentChanged();
                    // show dialogresult
                    StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("CI_IndexCreated"),
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                    if (strRD.DRes == NyxDialogResult.Yes)
                    {   // set tablename
                        if (tblName.Length > 0)
                            this._cbTables.SelectedItem = tblName;
                        else
                            this._cbTables.SelectedIndex = 0;
                        // and rest of the gui
                        this._tbIdxName.Text = String.Empty;
                        this._dgFields.Rows.Clear();
                        this._cbType.SelectedIndex = 0;
                        this._cbAdd.SelectedIndex = 0;
                    }
                    else
                        this.Close();
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CI_IndexCreateErr") + qry, 
                        dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
            }
        }
        private void _btClose_Click(object sender, EventArgs e)
        {   
            this.Close();
        }

        /// <summary>
        /// Fire OnDBContentChanged event
        /// </summary>
        protected void OnDBContentChanged()
        {   // fire event
            DBContentChangedEventArgs e = new DBContentChangedEventArgs();
            e.DBContent = DatabaseContent.Indexes;
            if(tblName.Length > 0)
                e.Param1 = tblName;

            if (DBContentChangedEvent != null)
                DBContentChangedEvent(this, e);
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}