/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using System.Windows.Forms;
using Nyx.AppDialogs;

namespace Nyx.NyxMySql
{
    public partial class MySqlCreateTable : Form
    {   /// <remarks> event which gets fired when a table was created and the dialog is closed </remarks>
        public event EventHandler<DBContentChangedEventArgs> DBContentChangedEvent;
        // init dbhelper and mysqlcon-var
        Classes.DBHelper dbh = new Classes.DBHelper(NyxSettings.DBType.MySql);

        /// <summary>
        /// Create table dialog for mysql
        /// </summary>
        public MySqlCreateTable()
        {
            InitializeComponent();
            this._cbTblType.SelectedIndex = 0;
            // init charsets
            foreach (string charset in dbh.Encodings)
                this._cbFldCharset.Items.Add(charset);
            // init collate
            foreach (string collate in dbh.Collations)
            {
                this._cbTblCollate.Items.Add(collate);
                this._cbFldCollate.Items.Add(collate);
            }
        }
        private void MySQLCreateTable_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // focus on tablename
            this._tbTblName.Focus();
        }

        private bool CheckFieldInput()
        {   // check if we got everything
            if (this._tbFldName.Text.Trim().Length == 0)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnCreateTableMissingFieldName"), MessageHandler.EnumMsgType.Error);
                this._tbFldName.Focus();
                return false;
            }
            if (this._cbFldType.SelectedItem.ToString().Trim().Length == 0)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnCreateTableMissingFieldType"), MessageHandler.EnumMsgType.Error);
                this._cbFldType.Focus();
                return false;
            }
            // only check if we add items
            if (this._dgFields.SelectedRows.Count == 0 && this._cbFldIndex.Text.ToString() == "Primary")
            {
                foreach (DataGridViewRow dgvr in this._dgFields.Rows)
                    if (dgvr.Cells["Index"].Value.ToString() == "Primary")
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnCreateTableDoublePrimary"), MessageHandler.EnumMsgType.Error);
                        this._cbFldIndex.Focus();
                        return false;
                    }
            }
            return true;
        }
        private string BuildQuery()
        {   // initalize variables
            string primary = String.Empty, index = String.Empty, unique = String.Empty, fulltext = String.Empty;
            // building query
            System.Text.StringBuilder sbTmp = new System.Text.StringBuilder();
            System.Text.StringBuilder sbQry = 
                new System.Text.StringBuilder("CREATE TABLE `" + this._tbTblName.Text + "` (\n");
            // build column definitions
            foreach (DataGridViewRow dgvr in this._dgFields.Rows)
            {   // examine fildtype, and direct properties
                if (sbTmp.Length == 0)
                    sbTmp.Append(",\n");
                // get column definitions
                sbTmp.Append(MySqlHelper.BuildColumenDefinition(dgvr.Cells["_dgvcFldName"].Value.ToString(),
                    dgvr.Cells["_dgvcFldType"].Value.ToString(), dgvr.Cells["_dgvcFldLength"].Value.ToString(),
                    dgvr.Cells["_dgvcFldUnsigned"].Value.ToString(), dgvr.Cells["_dgvcFldAutoInc"].Value.ToString(),
                    dgvr.Cells["_dgvcFldZerofill"].Value.ToString(), dgvr.Cells["_dgvcFldCharset"].Value.ToString(),
                    dgvr.Cells["_dgvcFldCollate"].Value.ToString(), dgvr.Cells["_dgvcFldBinary"].Value.ToString(),
                    dgvr.Cells["_dgvcFldOnUpdate"].Value.ToString(), dgvr.Cells["_dgvcFldValues"].Value.ToString()));
                // null
                if (dgvr.Cells["_dgvcFldNull"].Value.ToString().Length > 0)
                    sbTmp.Append(String.Format(NyxMain.NyxCI, " {0}", dgvr.Cells["_dgvcFldNull"].Value.ToString().ToUpper(NyxMain.NyxCI)));
                // default?
                if (dgvr.Cells["_dgvcFldDefault"].Value.ToString().Length > 0)
                    sbTmp.Append(String.Format(NyxMain.NyxCI, " DEFAULT '{0}'", dgvr.Cells["_dgvcFldDefault"].Value.ToString()));
                // comment?
                if (dgvr.Cells["_dgvcFldComment"].Value.ToString().Length > 0)
                    sbTmp.Append(String.Format(NyxMain.NyxCI, " COMMENT '{0}'", dgvr.Cells["_dgvcFldComment"].Value.ToString()));

                // fulltext
                if (dgvr.Cells["_dgvcFldFulltext"].Value.ToString() == "Y")
                    if(fulltext == null)
                        fulltext = String.Format(NyxMain.NyxCI, "`{0}`", dgvr.Cells["_dgvcFldName"].Value.ToString());
                    else
                        fulltext = String.Format(NyxMain.NyxCI, "{0},`{1}`", fulltext, dgvr.Cells["_dgvcFldName"].Value.ToString());
                // primary? index? unique?
                switch (dgvr.Cells["_dgvcFldIndex"].Value.ToString())
                {
                    case "Primary":
                        if (primary.Length > 0)
                            primary = String.Format(NyxMain.NyxCI, "`{0}`", dgvr.Cells["_dgvcFldName"].Value.ToString());
                        else
                            primary = String.Format(NyxMain.NyxCI, "{0},`{1}`", primary, dgvr.Cells["_dgvcFldName"].Value.ToString());
                        break;
                    case "Index":
                        if (index.Length > 0)
                            index = String.Format(NyxMain.NyxCI, "`{0}`", dgvr.Cells["_dgvcFldName"].Value.ToString());
                        else
                            index = String.Format(NyxMain.NyxCI, "{0},`{1}`", index, dgvr.Cells["_dgvcFldName"].Value.ToString());
                        break;
                    case "Unique":
                        if (unique.Length > 0)
                            unique = String.Format(NyxMain.NyxCI, "`{0}`", dgvr.Cells["_dgvcFldName"].Value.ToString());
                        else
                            unique = String.Format(NyxMain.NyxCI, "{0},`{1}`", unique, dgvr.Cells["_dgvcFldName"].Value.ToString());
                        break;
                }
                // add to query
                sbQry.Append(sbTmp.ToString());
            }
            // finalize query (add index, unique, fulltext, type...)
            if (primary != null)
                sbQry.Append(",\n PRIMARY KEY (" + primary + ")");
            if (index != null)
                sbQry.Append(",\n INDEX (" + index + ")");
            if (unique != null)
                sbQry.Append(",\n UNIQUE (" + unique + ")");
            if (fulltext != null)
                sbQry.Append(",\n FULLTEXT (" + fulltext + ")");
            // fin ) + table type
            sbQry.Append("\n) TYPE = " + this._cbTblType.SelectedItem.ToString().ToUpper(NyxMain.NyxCI));
            // table collation?
            if (this._cbTblCollate.SelectedItem != null 
                && this._cbTblCollate.SelectedItem.ToString().Length > 0)
                sbQry.Append(" COLLATE " + this._cbTblCollate.SelectedItem.ToString().ToUpper(NyxMain.NyxCI));
            // table comment?
            if (this._tbTblComment.Text.Length > 0)
                sbQry.Append(" COMMENT '" + this._tbTblComment.Text + "'");
            // returning
            return sbQry.ToString();
        }
        private void ResetFields()
        {   // reset fields
            this._tbFldComment.Text = String.Empty;
            this._tbFldDefault.Text = String.Empty;
            this._tbFldName.Text = String.Empty;
            this._mtbFldLength.Text = String.Empty;
            this._tbFldValues.Text = String.Empty;
            this._ckbFldAutoInc.Checked = false;
            this._ckbFldFulltext.Checked = false;
            this._ckbFldOnUpdate.Checked = false;
            this._ckbFldUnsigned.Checked = false;
            this._ckbFldZerofill.Checked = false;
            this._cbFldCollate.SelectedIndex = 0;
            this._cbFldCharset.SelectedIndex = 0;
            this._cbFldIndex.SelectedIndex = 0;
            this._cbFldNull.SelectedIndex = 0;
            this._cbFldType.SelectedIndex = 0;
        }

        private void _msCloseDialog_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _FldAdd_Click(object sender, EventArgs e)
        {   // checkup
            if (!CheckFieldInput())
                return;
            // init strings for type-dependant options
            string sIndex = String.Empty, sLength = String.Empty, sUnsigned = String.Empty, sFulltext = String.Empty,
                sAutoInc = String.Empty, sZerofill = String.Empty, sOnUpdate = String.Empty, sBinary = String.Empty,
                sCharset = String.Empty, sCollate = String.Empty, sValues = String.Empty, sNull = String.Empty;
            // check and set strings
            if (this._cbFldIndex.Text.ToString().Length > 0)
                sIndex = this._cbFldIndex.SelectedItem.ToString();
            if (this._mtbFldLength.Enabled)
                sLength = this._mtbFldLength.Text.ToString().Trim();

            if (this._ckbFldUnsigned.Enabled && this._ckbFldUnsigned.Checked) 
                sUnsigned = "Y";
            if (this._ckbFldFulltext.Enabled && this._ckbFldFulltext.Checked) 
                sFulltext = "Y";
            if (this._ckbFldAutoInc.Enabled && this._ckbFldAutoInc.Checked) 
                sAutoInc = "Y";
            if (this._ckbFldZerofill.Enabled && this._ckbFldZerofill.Checked) 
                sZerofill = "Y";
            if (this._ckbFldOnUpdate.Enabled && this._ckbFldOnUpdate.Checked) 
                sOnUpdate = "Y";
            if (this._ckbFldBinary.Enabled && this._ckbFldBinary.Checked) 
                sBinary = "Y";

            if (this._cbFldCharset.Enabled && this._cbFldCharset.Text.Length > 0) 
                sCharset = this._cbFldCharset.Text.ToString().Trim();
            if (this._cbFldCollate.Enabled && this._cbFldCollate.Text.Length > 0) 
                sCollate = this._cbFldCollate.Text.ToString().Trim();
            if (this._tbFldValues.Enabled && this._tbFldValues.Text.Length > 0)
                sValues = this._tbFldValues.Text.ToString().Trim();
            if (this._cbFldNull.Text.ToString().Length > 0) 
                sNull = this._cbFldNull.SelectedItem.ToString();
            // allready field with this name?
            DataGridViewRow fndRow = null;
            foreach (DataGridViewRow dr in this._dgFields.Rows)
            {
                if (dr.Cells["_dgvcFldName"].Value.ToString() == this._tbFldName.Text.Trim())
                {
                    fndRow = dr;
                    break;
                }
            }
            // it allready exist a field with this name
            if (fndRow != null)
            {
                StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("CT_FieldDouble"),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information, false);
                if (strRD.DRes == NyxDialogResult.Yes)
                {   // edit datarow
                    fndRow.Cells["_dgvcFldIndex"].Value = sIndex;
                    fndRow.Cells["_dgvcFldName"].Value = this._tbFldName.Text.Trim();
                    fndRow.Cells["_dgvcFldType"].Value = this._cbFldType.SelectedItem.ToString().Trim();
                    fndRow.Cells["_dgvcFldLength"].Value = sLength;
                    fndRow.Cells["_dgvcFldUnsigned"].Value = sUnsigned;
                    fndRow.Cells["_dgvcFldFulltext"].Value = sFulltext;
                    fndRow.Cells["_dgvcFldAutoInc"].Value = sAutoInc;
                    fndRow.Cells["_dgvcFldZerofill"].Value = sZerofill;
                    fndRow.Cells["_dgvcFldOnUpdate"].Value = sOnUpdate;
                    fndRow.Cells["_dgvcFldBinary"].Value = sBinary;
                    fndRow.Cells["_dgvcFldNull"].Value = sNull;
                    fndRow.Cells["_dgvcFldDefault"].Value = this._tbFldDefault.Text.Trim();
                    fndRow.Cells["_dgvcFldCharset"].Value = sCharset;
                    fndRow.Cells["_dgvcFldCollate"].Value = sCollate;
                    fndRow.Cells["_dgvcFldValues"].Value = sValues;
                    fndRow.Cells["_dgvcFldComment"].Value = this._tbFldComment.Text.Trim();
                }
            }
            // add new field
            else
            {
                // adding field to datagridview
                string[] row = new string[] { sIndex,   
                        this._tbFldName.Text.ToString().Trim(),
                        this._cbFldType.SelectedItem.ToString().Trim(),
                        sLength, sUnsigned, sFulltext, sAutoInc, sZerofill, sOnUpdate,
                        sBinary, sNull, 
                        this._tbFldDefault.Text.ToString().Trim(),
                        sCharset, sCollate, sValues, 
                        this._tbFldComment.Text.ToString().Trim() };
                this._dgFields.Rows.Add(row);
                // final gui settings
                this._btOk.Enabled = true;
                // reset fields
                ResetFields();
                // focus on fieldname
                this._tbFldName.Focus();
            }            
        }
        private void _FldEdit_Click(object sender, EventArgs e)
        {   // check selection
            if (this._dgFields.SelectedRows.Count != 1)
                return;
            // edit datarow
            DataGridViewRow dgvr = this._dgFields.SelectedRows[0];
            // init strings for type-dependant options
            string sIndex = String.Empty, sLength = String.Empty, sUnsigned = String.Empty, sFulltext = String.Empty,
                sAutoInc = String.Empty, sZerofill = String.Empty, sOnUpdate = String.Empty, sBinary = String.Empty,
                sCharset = String.Empty, sCollate = String.Empty, sValues = String.Empty, sNull = String.Empty;
            // check and set strings
            if (this._cbFldIndex.Text.ToString().Length > 0)
                sIndex = this._cbFldIndex.SelectedItem.ToString();
            if (this._mtbFldLength.Enabled) 
                sLength = this._mtbFldLength.Text.ToString().Trim();

            if (this._ckbFldUnsigned.Checked && this._ckbFldUnsigned.Enabled) 
                sUnsigned = "Y";
            if (this._ckbFldFulltext.Checked && this._ckbFldFulltext.Enabled) 
                sFulltext = "Y";
            if (this._ckbFldAutoInc.Checked && this._ckbFldAutoInc.Enabled) 
                sAutoInc = "Y";
            if (this._ckbFldZerofill.Checked && this._ckbFldZerofill.Enabled) 
                sZerofill = "Y";
            if (this._ckbFldOnUpdate.Checked && this._ckbFldOnUpdate.Enabled) 
                sOnUpdate = "Y";
            if (this._ckbFldBinary.Checked && this._ckbFldBinary.Enabled) 
                sBinary = "Y";

            if (this._cbFldCharset.Enabled) 
                sCharset = this._cbFldCharset.SelectedItem.ToString();
            if (this._cbFldCollate.Enabled) 
                sCollate = this._cbFldCollate.SelectedItem.ToString();
            if (this._tbFldValues.Enabled && this._tbFldValues.Text.Length > 0) 
                sValues = this._tbFldValues.Text.ToString().Trim();
            if (this._cbFldNull.Text.ToString().Length > 0) 
                sNull = this._cbFldNull.SelectedItem.ToString();
            // edit datarow
            dgvr.Cells["_dgvcFldIndex"].Value = sIndex;
            dgvr.Cells["_dgvcFldName"].Value = this._tbFldName.Text.Trim();
            dgvr.Cells["_dgvcFldType"].Value = this._cbFldType.SelectedItem.ToString().Trim();
            dgvr.Cells["_dgvcFldLength"].Value = sLength;
            dgvr.Cells["_dgvcFldUnsigned"].Value = sUnsigned;
            dgvr.Cells["_dgvcFldFulltext"].Value = sFulltext;
            dgvr.Cells["_dgvcFldAutoInc"].Value = sAutoInc;
            dgvr.Cells["_dgvcFldZerofill"].Value = sZerofill;
            dgvr.Cells["_dgvcFldOnUpdate"].Value = sOnUpdate;
            dgvr.Cells["_dgvcFldBinary"].Value = sBinary;
            dgvr.Cells["_dgvcFldNull"].Value = sNull;
            dgvr.Cells["_dgvcFldDefault"].Value = this._tbFldDefault.Text.Trim();
            dgvr.Cells["_dgvcFldCharset"].Value = sCharset;
            dgvr.Cells["_dgvcFldCollate"].Value = sCollate;
            dgvr.Cells["_dgvcFldValues"].Value = sValues;
            dgvr.Cells["_dgvcFldComment"].Value = this._tbFldComment.Text.Trim();
            // reset fields
            ResetFields();
        }
        private void _FldRemove_Click(object sender, EventArgs e)
        {   // got selection?
            if (this._dgFields.SelectedRows.Count == 0)
                return;
            foreach (DataGridViewRow dr in this._dgFields.SelectedRows)
                this._dgFields.Rows.Remove(dr);
            // disable gui?
            if (this._dgFields.Rows.Count == 0)
            {
                this._btFldRemove.Enabled = false;
                this._btFldMoveUp.Enabled = false;
                this._btFldMoveDown.Enabled = false;
                this._btOk.Enabled = false;
            }
        }
        private void _btOk_Click(object sender, EventArgs e)
        {   // minimal check
            if (this._tbTblName.Text.Trim().Length == 0 || this._dgFields.Rows.Count < 1)
                return;
            // build query
            string qry = BuildQuery();
            // execute query
            if (!dbh.ExecuteNonQuery(qry))
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), MessageHandler.EnumMsgType.Error);
            else
            {   // fire refresh event
                OnDBContentChanged();
                // check if to create another view
                StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("TableCreateDone", new string[] { this._tbTblName.Text.ToString() }),
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Information, false);
                if (strRD.DRes == NyxDialogResult.Yes)
                    ResetFields();
                else
                    OnDBContentChanged();
            }
        }
        private void _FldMoveUp_Click(object sender, EventArgs e)
        {   // got selection?
            if (this._dgFields.SelectedRows.Count == 1)
            {   // get selection
                DataGridViewSelectedRowCollection dgvsrColl = this._dgFields.SelectedRows;
                // get first selected row index
                int idx = this._dgFields.Rows.IndexOf(dgvsrColl[0]);
                if (idx - 1 >= 0)
                {   // helpvar
                    DataGridViewRow dgvr = this._dgFields.Rows[idx];
                    this._dgFields.Rows.RemoveAt(idx);
                    this._dgFields.Rows.Insert(idx - 1, dgvr);
                }
                // set selection
                this._dgFields.CurrentCell = this._dgFields.Rows[idx - 1].Cells[0];
                this._dgFields.Rows[idx].Selected = false;
                this._dgFields.Rows[idx - 1].Selected = true;
            }
        }
        private void _FldMoveDown_Click(object sender, EventArgs e)
        {   // got selection?
            if (this._dgFields.SelectedRows.Count == 1)
            {   // get selection
                DataGridViewSelectedRowCollection dgvsrColl = this._dgFields.SelectedRows;
                // get first selected row index
                int idx = this._dgFields.Rows.IndexOf(dgvsrColl[0]);
                if (idx + 1 < this._dgFields.Rows.Count)
                {   // helpvar
                    DataGridViewRow dgvr = this._dgFields.Rows[idx];
                    this._dgFields.Rows.RemoveAt(idx);
                    this._dgFields.Rows.Insert(idx + 1, dgvr);
                }
                // set selection
                this._dgFields.CurrentCell = this._dgFields.Rows[idx + 1].Cells[0];
                this._dgFields.Rows[idx].Selected = false;
                this._dgFields.Rows[idx + 1].Selected = true;
            }
        }
        
        // gui control
        private void _tbTblName_TextChanged(object sender, EventArgs e)
        {
            if (this._tbTblName.Text.Length > 0)
                this._gbFields.Enabled = true;
            else
            {
                this._gbFields.Enabled = false;
                this._btOk.Enabled = false;
                this._msTblCreate.Enabled = false;
            }
        }
        private void _tbFldName_TextChanged(object sender, EventArgs e)
        {
            if (this._tbFldName.Text.Length > 0)
            {
                this._btFldAdd.Enabled = true;
                this._msFldAdd.Enabled = true;
            }
            else
            {
                this._btFldAdd.Enabled = false;
                this._msFldAdd.Enabled = false;
            }
        }
        private void _cbFieldType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selitem = this._cbFldType.SelectedItem.ToString();
            switch (selitem)
            {
                case "BIT":         // BIT          [(length)]
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldFulltext.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    break;
                case "TINYINT":     // TINYINT	    [(length)] [UNSIGNED] [ZEROFILL]
                case "SMALLINT":    // SMALLINT     [(length)] [UNSIGNED] [ZEROFILL]
                case "MEDIUMINT":   // MEDIUMINT    [(length)] [UNSIGNED] [ZEROFILL]
                case "INT":         // INT          [(length)] [UNSIGNED] [ZEROFILL]
                case "INTEGER":     // INTEGER      [(length)] [UNSIGNED] [ZEROFILL]
                case "BIGINT":      // BIGINT       [(length)] [UNSIGNED] [ZEROFILL]
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = true;
                    this._ckbFldFulltext.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = true;
                    this._ckbFldZerofill.Enabled = true;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "REAL":        // REAL         [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "DOUBLE":      // DOUBLE       [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "FLOAT":       // FLOAT        [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "DECIMAL":     // DECIMAL		(length,decimals) [UNSIGNED] [ZEROFILL]
                case "NUMERIC":     // NUMERIC 	    (length,decimals) [UNSIGNED] [ZEROFILL]
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = true;
                    this._ckbFldFulltext.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = true;
                    this._ckbFldZerofill.Enabled = true;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "TIMESTAMP":   // TIMESTAMP
                    this._mtbFldLength.Enabled = false;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldFulltext.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = true;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "DATE":        // DATE
                case "TIME":        // TIME
                case "DATETIME":    // DATETIME
                case "YEAR":        // YEAR
                case "TINYBLOB":    // TINYBLOB
                case "BLOB":        // BLOB
                case "MEDIUMBLOB":  // MEDIUMBLOB
                case "LONGBLOB":    // LONGBLOB
                    this._mtbFldLength.Enabled = false;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldFulltext.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "CHAR":        // CHAR		    (length) [CHARACTER SET charset_name] [COLLATE collation_name] -fulltext
                case "VARCHAR":     // VARCHAR		(length) [CHARACTER SET charset_name] [COLLATE collation_name] -fulltext
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldFulltext.Enabled = true;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = true;
                    this._cbFldCharset.Enabled = true;
                    this._tbFldValues.Enabled = false;
                    break;
                case "BINARY":      // BINARY		(length)
                case "VARBINARY":   // VARBINARY	(length)
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldFulltext.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = false;
                    this._cbFldCharset.Enabled = false;
                    this._tbFldValues.Enabled = false;
                    break;
                case "TINYTEXT":    // TINYTEXT 	[BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext 
                case "TEXT":        // TEXT 		[BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                case "MEDIUMTEXT":  // MEDIUMTEXT   [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                case "LONGTEXT":    // LONGTEXT     [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                    this._mtbFldLength.Enabled = false;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldFulltext.Enabled = true;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = true;
                    this._cbFldCollate.Enabled = true;
                    this._cbFldCharset.Enabled = true;
                    this._tbFldValues.Enabled = false;
                    break;
                case "ENUM":        // ENUM        (value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
                case "SET":         // SET         (value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
                    this._mtbFldLength.Enabled = false;
                    this._ckbFldAutoInc.Enabled = false;
                    this._ckbFldFulltext.Enabled = false;
                    this._ckbFldOnUpdate.Enabled = false;
                    this._ckbFldUnsigned.Enabled = false;
                    this._ckbFldZerofill.Enabled = false;
                    this._ckbFldBinary.Enabled = false;
                    this._cbFldCollate.Enabled = true;
                    this._cbFldCharset.Enabled = true;
                    this._tbFldValues.Enabled = true;
                    break;
            }
        }
        private void _dgFields_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {   // enable only if not enabled till now
            if (!this._msTblCreate.Enabled)
            {
                this._msTblCreate.Enabled = true;
                this._btOk.Enabled = true;
                this._msFldRemove.Enabled = true;
                this._btFldRemove.Enabled = true;
            }
        }
        private void _dgFields_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {   // check if we got no more rows
            if (this._dgFields.Rows.Count == 0)
            {
                this._msTblCreate.Enabled = false;
                this._btOk.Enabled = false;
            }
        }
        private void _dgFields_SelectionChanged(object sender, EventArgs e)
        {
            if (this._dgFields.SelectedRows.Count == 1)
            {
                this._btFldMoveUp.Enabled = true;
                this._btFldMoveDown.Enabled = true;
                this._msFldFieldUp.Enabled = true;
                this._msFldFieldDown.Enabled = true;
                this._btFldRemove.Enabled = true;
                this._msFldRemove.Enabled = true;
                this._btFldEdit.Enabled = true;
                this._msFldEdit.Enabled = true;
                // further we fill up the information of the selected row to the editfields
                DataGridViewRow dgvr = this._dgFields.SelectedRows[0];
                if (dgvr.Cells["_dgvcFldIndex"].Value.ToString().Length > 0)
                    this._cbFldIndex.SelectedItem = dgvr.Cells["_dgvcFldIndex"].Value;
                if (dgvr.Cells["_dgvcFldName"].Value.ToString().Length > 0)
                    this._tbFldName.Text = dgvr.Cells["_dgvcFldName"].Value.ToString();
                if (dgvr.Cells["_dgvcFldType"].Value.ToString().Length > 0)
                    this._cbFldType.SelectedItem = dgvr.Cells["_dgvcFldType"].Value;
                if (dgvr.Cells["_dgvcFldLength"].Value.ToString().Length > 0)
                    this._mtbFldLength.Text = dgvr.Cells["_dgvcFldLength"].Value.ToString();

                if (dgvr.Cells["_dgvcFldUnsigned"].Value.ToString() == "Y")
                    this._ckbFldUnsigned.Checked = true;
                if (dgvr.Cells["_dgvcFldFulltext"].Value.ToString() == "Y")
                    this._ckbFldFulltext.Checked = true;
                if (dgvr.Cells["_dgvcFldAutoInc"].Value.ToString() == "Y")
                    this._ckbFldAutoInc.Checked = true;
                if (dgvr.Cells["_dgvcFldZerofill"].Value.ToString() == "Y")
                    this._ckbFldZerofill.Checked = true;
                if (dgvr.Cells["_dgvcFldOnUpdate"].Value.ToString()== "Y")
                    this._ckbFldOnUpdate.Checked = true;
                if (dgvr.Cells["_dgvcFldBinary"].Value.ToString() == "Y")
                    this._ckbFldBinary.Checked = true;

                if (dgvr.Cells["_dgvcFldNull"].Value.ToString().Length > 0)
                    this._cbFldNull.SelectedItem = dgvr.Cells["_dgvcFldNull"].Value;
                if (dgvr.Cells["_dgvcFldDefault"].Value.ToString().Length > 0)
                    this._tbFldDefault.Text = dgvr.Cells["_dgvcFldDefault"].Value.ToString();
                if (dgvr.Cells["_dgvcFldCharset"].Value.ToString().Length > 0)
                    this._cbFldCharset.SelectedItem = dgvr.Cells["_dgvcFldCharset"].Value;
                if (dgvr.Cells["_dgvcFldCollate"].Value.ToString().Length > 0)
                    this._cbFldCollate.SelectedItem = dgvr.Cells["_dgvcFldCollate"].Value;
                if (dgvr.Cells["_dgvcFldValues"].Value.ToString().Length > 0)
                    this._tbFldValues.Text = dgvr.Cells["_dgvcFldValues"].Value.ToString();
                if (dgvr.Cells["_dgvcFldComment"].Value.ToString().Length > 0)
                    this._tbFldComment.Text = dgvr.Cells["_dgvcFldComment"].Value.ToString();
            }
            else
            {
                this._btFldMoveUp.Enabled = false;
                this._btFldMoveDown.Enabled = false;
                this._msFldFieldUp.Enabled = false;
                this._msFldFieldDown.Enabled = false;
                this._btFldRemove.Enabled = false;
                this._msFldRemove.Enabled = false;
                this._btFldEdit.Enabled = false;
                this._msFldEdit.Enabled = false;
            }
        }

        /// <summary>
        /// Fire OnDBContentChanged event
        /// </summary>
        protected void OnDBContentChanged()
        {   // fire event
            DBContentChangedEventArgs e = new DBContentChangedEventArgs();
            e.DBContent = DatabaseContent.Tables;

            if(DBContentChangedEvent != null)
                DBContentChangedEvent(this, e);
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        private void MySqlCreateTable_FormClosed(object sender, FormClosedEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }
    }
}