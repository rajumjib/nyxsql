namespace Nyx.NyxMySql
{
    partial class MySqlDBImport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlDBImport));
            this._tcImport = new System.Windows.Forms.TabControl();
            this._tpSqlImp = new System.Windows.Forms.TabPage();
            this._btSqlSelFile = new System.Windows.Forms.Button();
            this._tbSqlFile = new System.Windows.Forms.TextBox();
            this._tpCustImp = new System.Windows.Forms.TabPage();
            this._lvCustColumns = new System.Windows.Forms.ListView();
            this.name = new System.Windows.Forms.ColumnHeader();
            this.type = new System.Windows.Forms.ColumnHeader();
            this._cmlv = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmlv_selall = new System.Windows.Forms.ToolStripMenuItem();
            this._cmlv_selnone = new System.Windows.Forms.ToolStripMenuItem();
            this._cmlv_invertsel = new System.Windows.Forms.ToolStripMenuItem();
            this._pnl_citop = new System.Windows.Forms.Panel();
            this._tbCustFile = new System.Windows.Forms.TextBox();
            this._btCustSelFile = new System.Windows.Forms.Button();
            this._btDown = new System.Windows.Forms.Button();
            this._cbCustTable = new System.Windows.Forms.ComboBox();
            this._btUp = new System.Windows.Forms.Button();
            this._cbCustFileType = new System.Windows.Forms.ComboBox();
            this._lblTable = new System.Windows.Forms.Label();
            this._lblFieldSep = new System.Windows.Forms.Label();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._btOpenImp = new System.Windows.Forms.Button();
            this._pnlControl = new System.Windows.Forms.Panel();
            this._ss = new System.Windows.Forms.StatusStrip();
            this._ssPgBar = new System.Windows.Forms.ToolStripProgressBar();
            this._sslblRowImp = new System.Windows.Forms.ToolStripStatusLabel();
            this._tslblCurRowCount = new System.Windows.Forms.ToolStripStatusLabel();
            this._tcImport.SuspendLayout();
            this._tpSqlImp.SuspendLayout();
            this._tpCustImp.SuspendLayout();
            this._cmlv.SuspendLayout();
            this._pnl_citop.SuspendLayout();
            this._pnlControl.SuspendLayout();
            this._ss.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tcImport
            // 
            this._tcImport.Controls.Add(this._tpSqlImp);
            this._tcImport.Controls.Add(this._tpCustImp);
            resources.ApplyResources(this._tcImport, "_tcImport");
            this._tcImport.Name = "_tcImport";
            this._tcImport.SelectedIndex = 0;
            this._tcImport.SelectedIndexChanged += new System.EventHandler(this._tabc_SelectedIndexChanged);
            // 
            // _tpSqlImp
            // 
            this._tpSqlImp.BackColor = System.Drawing.SystemColors.Control;
            this._tpSqlImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tpSqlImp.Controls.Add(this._btSqlSelFile);
            this._tpSqlImp.Controls.Add(this._tbSqlFile);
            resources.ApplyResources(this._tpSqlImp, "_tpSqlImp");
            this._tpSqlImp.Name = "_tpSqlImp";
            this._tpSqlImp.UseVisualStyleBackColor = true;
            // 
            // _btSqlSelFile
            // 
            resources.ApplyResources(this._btSqlSelFile, "_btSqlSelFile");
            this._btSqlSelFile.Image = global::Nyx.Properties.Resources.folder;
            this._btSqlSelFile.Name = "_btSqlSelFile";
            this._btSqlSelFile.UseVisualStyleBackColor = true;
            this._btSqlSelFile.Click += new System.EventHandler(this._btSelFile_Click);
            // 
            // _tbSqlFile
            // 
            this._tbSqlFile.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbSqlFile, "_tbSqlFile");
            this._tbSqlFile.Name = "_tbSqlFile";
            this._tbSqlFile.TextChanged += new System.EventHandler(this._tbSqlFile_TextChanged);
            // 
            // _tpCustImp
            // 
            this._tpCustImp.BackColor = System.Drawing.SystemColors.Control;
            this._tpCustImp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tpCustImp.Controls.Add(this._lvCustColumns);
            this._tpCustImp.Controls.Add(this._pnl_citop);
            resources.ApplyResources(this._tpCustImp, "_tpCustImp");
            this._tpCustImp.Name = "_tpCustImp";
            this._tpCustImp.UseVisualStyleBackColor = true;
            // 
            // _lvCustColumns
            // 
            this._lvCustColumns.BackColor = System.Drawing.Color.GhostWhite;
            this._lvCustColumns.CheckBoxes = true;
            this._lvCustColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name,
            this.type});
            this._lvCustColumns.ContextMenuStrip = this._cmlv;
            resources.ApplyResources(this._lvCustColumns, "_lvCustColumns");
            this._lvCustColumns.FullRowSelect = true;
            this._lvCustColumns.GridLines = true;
            this._lvCustColumns.HideSelection = false;
            this._lvCustColumns.MultiSelect = false;
            this._lvCustColumns.Name = "_lvCustColumns";
            this._lvCustColumns.UseCompatibleStateImageBehavior = false;
            this._lvCustColumns.View = System.Windows.Forms.View.Details;
            this._lvCustColumns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this._lvCustColumns_ItemChecked);
            // 
            // name
            // 
            resources.ApplyResources(this.name, "name");
            // 
            // type
            // 
            resources.ApplyResources(this.type, "type");
            // 
            // _cmlv
            // 
            this._cmlv.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmlv_selall,
            this._cmlv_selnone,
            this._cmlv_invertsel});
            this._cmlv.Name = "_cmlv";
            resources.ApplyResources(this._cmlv, "_cmlv");
            // 
            // _cmlv_selall
            // 
            this._cmlv_selall.Image = global::Nyx.Properties.Resources.arrow_redo;
            resources.ApplyResources(this._cmlv_selall, "_cmlv_selall");
            this._cmlv_selall.Name = "_cmlv_selall";
            this._cmlv_selall.Click += new System.EventHandler(this._cmlvSelAll_Click);
            // 
            // _cmlv_selnone
            // 
            this._cmlv_selnone.Image = global::Nyx.Properties.Resources.arrow_undo;
            resources.ApplyResources(this._cmlv_selnone, "_cmlv_selnone");
            this._cmlv_selnone.Name = "_cmlv_selnone";
            this._cmlv_selnone.Click += new System.EventHandler(this._cmlvSelNone_Click);
            // 
            // _cmlv_invertsel
            // 
            this._cmlv_invertsel.Image = global::Nyx.Properties.Resources.arrow_rotate_clockwise;
            resources.ApplyResources(this._cmlv_invertsel, "_cmlv_invertsel");
            this._cmlv_invertsel.Name = "_cmlv_invertsel";
            this._cmlv_invertsel.Click += new System.EventHandler(this._cmlvInvertSel_Click);
            // 
            // _pnl_citop
            // 
            this._pnl_citop.Controls.Add(this._tbCustFile);
            this._pnl_citop.Controls.Add(this._btCustSelFile);
            this._pnl_citop.Controls.Add(this._btDown);
            this._pnl_citop.Controls.Add(this._cbCustTable);
            this._pnl_citop.Controls.Add(this._btUp);
            this._pnl_citop.Controls.Add(this._cbCustFileType);
            this._pnl_citop.Controls.Add(this._lblTable);
            this._pnl_citop.Controls.Add(this._lblFieldSep);
            resources.ApplyResources(this._pnl_citop, "_pnl_citop");
            this._pnl_citop.Name = "_pnl_citop";
            // 
            // _tbCustFile
            // 
            this._tbCustFile.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbCustFile, "_tbCustFile");
            this._tbCustFile.Name = "_tbCustFile";
            this._tbCustFile.TextChanged += new System.EventHandler(this._tbCustFile_TextChanged);
            // 
            // _btCustSelFile
            // 
            resources.ApplyResources(this._btCustSelFile, "_btCustSelFile");
            this._btCustSelFile.Image = global::Nyx.Properties.Resources.folder;
            this._btCustSelFile.Name = "_btCustSelFile";
            this._btCustSelFile.UseVisualStyleBackColor = true;
            this._btCustSelFile.Click += new System.EventHandler(this._btCustSelFile_Click);
            // 
            // _btDown
            // 
            resources.ApplyResources(this._btDown, "_btDown");
            this._btDown.Image = global::Nyx.Properties.Resources.arrow_down;
            this._btDown.Name = "_btDown";
            this._btDown.UseVisualStyleBackColor = true;
            this._btDown.Click += new System.EventHandler(this._btDown_Click);
            // 
            // _cbCustTable
            // 
            this._cbCustTable.BackColor = System.Drawing.Color.GhostWhite;
            this._cbCustTable.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbCustTable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbCustTable.FormattingEnabled = true;
            resources.ApplyResources(this._cbCustTable, "_cbCustTable");
            this._cbCustTable.Name = "_cbCustTable";
            this._cbCustTable.SelectedIndexChanged += new System.EventHandler(this._cbCustTable_SelectedIndexChanged);
            // 
            // _btUp
            // 
            resources.ApplyResources(this._btUp, "_btUp");
            this._btUp.Image = global::Nyx.Properties.Resources.arrow_up;
            this._btUp.Name = "_btUp";
            this._btUp.UseVisualStyleBackColor = true;
            this._btUp.Click += new System.EventHandler(this._btUp_Click);
            // 
            // _cbCustFileType
            // 
            this._cbCustFileType.BackColor = System.Drawing.Color.GhostWhite;
            this._cbCustFileType.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbCustFileType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbCustFileType.FormattingEnabled = true;
            this._cbCustFileType.Items.AddRange(new object[] {
            resources.GetString("_cbCustFileType.Items"),
            resources.GetString("_cbCustFileType.Items1"),
            resources.GetString("_cbCustFileType.Items2")});
            resources.ApplyResources(this._cbCustFileType, "_cbCustFileType");
            this._cbCustFileType.Name = "_cbCustFileType";
            // 
            // _lblTable
            // 
            resources.ApplyResources(this._lblTable, "_lblTable");
            this._lblTable.Name = "_lblTable";
            // 
            // _lblFieldSep
            // 
            resources.ApplyResources(this._lblFieldSep, "_lblFieldSep");
            this._lblFieldSep.Name = "_lblFieldSep";
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btCancel_Click);
            // 
            // _btOk
            // 
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._msImport_Click);
            // 
            // _btOpenImp
            // 
            resources.ApplyResources(this._btOpenImp, "_btOpenImp");
            this._btOpenImp.Image = global::Nyx.Properties.Resources.folder;
            this._btOpenImp.Name = "_btOpenImp";
            this._btOpenImp.UseVisualStyleBackColor = true;
            this._btOpenImp.Click += new System.EventHandler(this._btLoadFilePrompt_Click);
            // 
            // _pnlControl
            // 
            this._pnlControl.Controls.Add(this._btOpenImp);
            this._pnlControl.Controls.Add(this._btCancel);
            this._pnlControl.Controls.Add(this._btOk);
            resources.ApplyResources(this._pnlControl, "_pnlControl");
            this._pnlControl.Name = "_pnlControl";
            // 
            // _ss
            // 
            this._ss.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ssPgBar,
            this._sslblRowImp,
            this._tslblCurRowCount});
            resources.ApplyResources(this._ss, "_ss");
            this._ss.Name = "_ss";
            // 
            // _ssPgBar
            // 
            this._ssPgBar.Name = "_ssPgBar";
            resources.ApplyResources(this._ssPgBar, "_ssPgBar");
            // 
            // _sslblRowImp
            // 
            this._sslblRowImp.Name = "_sslblRowImp";
            resources.ApplyResources(this._sslblRowImp, "_sslblRowImp");
            // 
            // _tslblCurRowCount
            // 
            this._tslblCurRowCount.Name = "_tslblCurRowCount";
            resources.ApplyResources(this._tslblCurRowCount, "_tslblCurRowCount");
            // 
            // MySqlDBImport
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._tcImport);
            this.Controls.Add(this._pnlControl);
            this.Controls.Add(this._ss);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "MySqlDBImport";
            this.Load += new System.EventHandler(this.FrmDbImport_Load);
            this._tcImport.ResumeLayout(false);
            this._tpSqlImp.ResumeLayout(false);
            this._tpSqlImp.PerformLayout();
            this._tpCustImp.ResumeLayout(false);
            this._cmlv.ResumeLayout(false);
            this._pnl_citop.ResumeLayout(false);
            this._pnl_citop.PerformLayout();
            this._pnlControl.ResumeLayout(false);
            this._ss.ResumeLayout(false);
            this._ss.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _tcImport;
        private System.Windows.Forms.TabPage _tpSqlImp;
        private System.Windows.Forms.TabPage _tpCustImp;
        private System.Windows.Forms.Label _lblFieldSep;
        private System.Windows.Forms.ComboBox _cbCustFileType;
        private System.Windows.Forms.ComboBox _cbCustTable;
        private System.Windows.Forms.ListView _lvCustColumns;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.ColumnHeader type;
        private System.Windows.Forms.Label _lblTable;
        private System.Windows.Forms.Button _btUp;
        private System.Windows.Forms.Button _btDown;
        private System.Windows.Forms.Panel _pnl_citop;
        private System.Windows.Forms.Button _btCustSelFile;
        private System.Windows.Forms.ContextMenuStrip _cmlv;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_selall;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_selnone;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_invertsel;
        private System.Windows.Forms.TextBox _tbSqlFile;
        private System.Windows.Forms.TextBox _tbCustFile;
        private System.Windows.Forms.Button _btSqlSelFile;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.Button _btOpenImp;
        private System.Windows.Forms.Panel _pnlControl;
        private System.Windows.Forms.StatusStrip _ss;
        private System.Windows.Forms.ToolStripProgressBar _ssPgBar;
        private System.Windows.Forms.ToolStripStatusLabel _sslblRowImp;
        private System.Windows.Forms.ToolStripStatusLabel _tslblCurRowCount;        
        
    }
}