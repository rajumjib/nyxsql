namespace Nyx.NyxMySql
{
    /// <summary>
    /// MySql Databaseinformations GUI
    /// </summary>
    partial class MySqlDBInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlDBInfo));
            this.value1 = new System.Windows.Forms.ColumnHeader();
            this._tpStatus = new System.Windows.Forms.TabPage();
            this._lvStatus = new System.Windows.Forms.ListView();
            this.name = new System.Windows.Forms.ColumnHeader();
            this.value = new System.Windows.Forms.ColumnHeader();
            this._tc = new System.Windows.Forms.TabControl();
            this._tpVariables = new System.Windows.Forms.TabPage();
            this._lvVariables = new System.Windows.Forms.ListView();
            this.name1 = new System.Windows.Forms.ColumnHeader();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._btClose = new System.Windows.Forms.Button();
            this._lblFilter = new System.Windows.Forms.Label();
            this._tbFilter = new System.Windows.Forms.TextBox();
            this._tpStatus.SuspendLayout();
            this._tc.SuspendLayout();
            this._tpVariables.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // value1
            // 
            resources.ApplyResources(this.value1, "value1");
            // 
            // _tpStatus
            // 
            this._tpStatus.BackColor = System.Drawing.Color.Transparent;
            this._tpStatus.Controls.Add(this._lvStatus);
            resources.ApplyResources(this._tpStatus, "_tpStatus");
            this._tpStatus.Name = "_tpStatus";
            this._tpStatus.UseVisualStyleBackColor = true;
            // 
            // _lvStatus
            // 
            this._lvStatus.BackColor = System.Drawing.Color.GhostWhite;
            this._lvStatus.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name,
            this.value});
            resources.ApplyResources(this._lvStatus, "_lvStatus");
            this._lvStatus.FullRowSelect = true;
            this._lvStatus.GridLines = true;
            this._lvStatus.Name = "_lvStatus";
            this._lvStatus.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this._lvStatus.UseCompatibleStateImageBehavior = false;
            this._lvStatus.View = System.Windows.Forms.View.Details;
            this._lvStatus.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvStatus_ColumnClick);
            // 
            // name
            // 
            resources.ApplyResources(this.name, "name");
            // 
            // value
            // 
            resources.ApplyResources(this.value, "value");
            // 
            // _tc
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._tc, 3);
            this._tc.Controls.Add(this._tpStatus);
            this._tc.Controls.Add(this._tpVariables);
            resources.ApplyResources(this._tc, "_tc");
            this._tc.Name = "_tc";
            this._tc.SelectedIndex = 0;
            this._tc.SelectedIndexChanged += new System.EventHandler(this._tc_SelectedIndexChanged);
            // 
            // _tpVariables
            // 
            this._tpVariables.Controls.Add(this._lvVariables);
            resources.ApplyResources(this._tpVariables, "_tpVariables");
            this._tpVariables.Name = "_tpVariables";
            this._tpVariables.UseVisualStyleBackColor = true;
            // 
            // _lvVariables
            // 
            this._lvVariables.BackColor = System.Drawing.Color.GhostWhite;
            this._lvVariables.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name1,
            this.value1});
            resources.ApplyResources(this._lvVariables, "_lvVariables");
            this._lvVariables.FullRowSelect = true;
            this._lvVariables.GridLines = true;
            this._lvVariables.Name = "_lvVariables";
            this._lvVariables.Sorting = System.Windows.Forms.SortOrder.Ascending;
            this._lvVariables.UseCompatibleStateImageBehavior = false;
            this._lvVariables.View = System.Windows.Forms.View.Details;
            this._lvVariables.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvVariables_ColumnClick);
            // 
            // name1
            // 
            resources.ApplyResources(this.name1, "name1");
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this._tc, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._btClose, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this._lblFilter, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._tbFilter, 1, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // _lblFilter
            // 
            resources.ApplyResources(this._lblFilter, "_lblFilter");
            this._lblFilter.Name = "_lblFilter";
            // 
            // _tbFilter
            // 
            resources.ApplyResources(this._tbFilter, "_tbFilter");
            this._tbFilter.Name = "_tbFilter";
            this._tbFilter.TextChanged += new System.EventHandler(this._tbFilter_TextChanged);
            // 
            // MySqlDBInfo
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "MySqlDBInfo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MySqlDBInfo_FormClosing);
            this.Load += new System.EventHandler(this.MysqlDBInfo_Load);
            this._tpStatus.ResumeLayout(false);
            this._tc.ResumeLayout(false);
            this._tpVariables.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColumnHeader value1;
        private System.Windows.Forms.TabPage _tpStatus;
        private System.Windows.Forms.ListView _lvStatus;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.ColumnHeader value;
        private System.Windows.Forms.TabControl _tc;
        private System.Windows.Forms.TabPage _tpVariables;
        private System.Windows.Forms.ListView _lvVariables;
        private System.Windows.Forms.ColumnHeader name1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button _btClose;
        private System.Windows.Forms.Label _lblFilter;
        private System.Windows.Forms.TextBox _tbFilter;


    }
}