/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using MySql.Data.MySqlClient;
using System.Windows.Forms;
using System.IO;
using NyxSettings;
using Nyx.Classes;
using Nyx.AppDialogs;

namespace Nyx.NyxMySql
{
    /// <summary> mysql database export dialog </summary>
    public partial class MySqlDBExport : Form
    {
        private struct StructExpSqlOptions
        {
            private bool comments;
            private bool crtTable;
            private bool dropTable;
            private bool tableExists;
            private bool complInserts;
            private bool onlyStructure;

            public bool Comments
            {
                get { return comments; }
                set { comments = value; }
            }
            public bool CrtTable
            {
                get { return crtTable; }
                set { crtTable = value; }
            }
            public bool DropTable
            {
                get { return dropTable; }
                set { dropTable = value; }
            }
            public bool TableExists
            {
                get { return tableExists; }
                set { tableExists = value; }
            }
            public bool ComplInserts
            {
                get { return complInserts; }
                set { complInserts = value; }
            }
            public bool OnlyStructure
            {
                get { return onlyStructure; }
                set { onlyStructure = value; }
            }
        }
        // thread vars
        private System.Threading.Thread thread;
        private string[] expTables, expColumns;
        // comments, crttable, droptable, tableexists, complinserts
        private StructExpSqlOptions strExpSqlOpts;
        private string[] expCustOptions = new string[2] { String.Empty, String.Empty };
        private string expFile = String.Empty;
        private DBException expError;
        private string curExpTable;

        // dbhelper
        DBHelper dbh = new DBHelper(DBType.MySql);

        /// <summary>
        /// MysqlDbExport - Gui for exporting data from mysql into a file.
        /// </summary>
        public MySqlDBExport()
        {
            InitializeComponent();
        }

        private void FrmDbExport_Load(object sender, EventArgs e)
            {// fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // at first we connect to the database
            if (!dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
            // and then we get the tables
            GetTables();
            // combobox
            this._cb_custfiletpye.SelectedIndex = 0;
        }
        private void FrmDbExport_FormClosing(object sender, FormClosingEventArgs e)
        {
            dbh.DisconnectDB();
        }
        private void GetTables()
        {
            if (dbh.ExecuteReader("SHOW TABLES"))
            {
                // init
                ListViewItem lvi = null;
                try
                {
                    // read
                    while (dbh.Read())
                    {
                        lvi = new ListViewItem(dbh.GetValue(0).ToString());
                        this._lv_tables.Items.Add(lvi);
                        this._cb_custtable.Items.Add(dbh.GetValue(0).ToString());
                    }
                }
                finally
                {
                    dbh.Close();
                }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CI_TablesRetrieveErr", new string[] { dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
        }
        private void ExpTablesSQL()
        {   
            // check if file is readable
            if (ChkFile(this._tb_sqlfile.Text))
            {
                // setting options
                strExpSqlOpts = new StructExpSqlOptions();
                strExpSqlOpts.Comments = this._ckb_comments.Checked;
                strExpSqlOpts.TableExists = this._ckb_tableexist.Checked;
                strExpSqlOpts.CrtTable = this._ckb_crttable.Checked;
                strExpSqlOpts.DropTable = this._ckb_droptable.Checked;
                strExpSqlOpts.ComplInserts = this._ckb_complinserts.Checked;
                strExpSqlOpts.OnlyStructure = this._ckb_onlyStructure.Checked;
                // setting array lengt
                expTables = new string[this._lv_tables.CheckedItems.Count];
                // setting export file
                expFile = this._tb_sqlfile.Text;
                // parsing listviewitems
                for (int i = 0; i < this._lv_tables.CheckedItems.Count; i++)
                {   // saving tables in an array
                    expTables[i] = this._lv_tables.CheckedItems[i].Text;
                }
                // gui options
                this._tcExport.Enabled = false;
                this._ssPgBar.Style = ProgressBarStyle.Marquee;
                // starting thread
                thread = new System.Threading.Thread(new System.Threading.ThreadStart(ThrSQLExport));
                thread.IsBackground = true;
                thread.Start();
            } 
        }
        private void ExpTablesCust()
        {
            if (ChkFile(this._tb_custfile.Text))
            {
                // setting options and file
                if (this._cb_custfiletpye.Text.Length > 0)
                {
                    if(this._cb_custfiletpye.Text == "tab")
                        expCustOptions[0] = "\t";
                    else
                        expCustOptions[0] = this._cb_custfiletpye.Text;
                }
                expCustOptions[1] = this._cb_custtable.Text;
                // putting columns into an array
                expColumns = new string[this._lv_custcolumns.CheckedItems.Count];
                for (int i = 0; i < this._lv_custcolumns.CheckedItems.Count; i++)
                {
                    expColumns[i] = this._lv_custcolumns.CheckedItems[i].Text;
                }
                expFile = this._tb_custfile.Text;
                // gui elements
                this._tcExport.Enabled = false;
                this._ssPgBar.Style = ProgressBarStyle.Marquee;
                // starting thread
                thread = new System.Threading.Thread(new System.Threading.ThreadStart(ThrCustExport));
                thread.IsBackground = true;
                thread.Start();
            }
        }
        private bool ChkFile(string filename)
        {   // at first, check if user ahs selected a file to save to
            if (filename.Length < 1)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                return false;
            }
            // check if file is here or has to be craeted and if we can write to it
            FileInfo fiInf = new FileInfo(filename);
            // file is already here
            if(fiInf.Exists)
            {
                if (fiInf.IsReadOnly)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBExp_NoFileSelected", new string[0]),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    this._tb_sqlfile.Focus();
                    return false;
                }
                else
                {   // truncating file
                    FileStream fs = null;
                    // check, if we can truncate the file
                    try 
                    { 
                        fs = fiInf.Open(FileMode.Truncate);
                        return true;
                    }
                    catch (IOException ioexc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileWriteAccess", new string[] { this._tb_sqlfile.Text, ioexc.Message }),
                            ioexc, MessageHandler.EnumMsgType.Warning);
                        this._tb_sqlfile.Focus();
                        return false;
                    }
                    catch (UnauthorizedAccessException uaexc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileWriteAccess", new string[] { this._tb_sqlfile.Text, uaexc.Message }),
                            uaexc, MessageHandler.EnumMsgType.Warning);
                        this._tb_sqlfile.Focus();
                        return false;
                    }
                    // close filestream
                    finally
                    {
                        if(fs != null)
                            fs.Dispose();
                    }
                }
            }
            // file has to be created
            else
            {
                FileStream fs = null;
                try 
                { 
                    fs = fiInf.Create();
                    return true;
                }
                catch (IOException ioexc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileCreate", new string[] { fiInf.FullName, ioexc.Message }),
                            ioexc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
                catch (UnauthorizedAccessException uaexc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileCreate", new string[] { fiInf.FullName, uaexc.Message }),
                            uaexc, MessageHandler.EnumMsgType.Warning);
                    return false;
                }
                // close filestream
                finally
                {
                    if(fs != null)
                        fs.Dispose();
                }
            }
        }
        private void AddComment(string comment)
        {
            if (comment.Length > 0)
            {
                string com = null;
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql: // mysql
                        com = "##";
                        break;
                    default: // all other - ansi sql
                        com = "--";
                        break;
                }
                Write2File(String.Format(NyxMain.NyxCI, "\r\n{0}\r\n{0} {1}\r\n{0}\r\n", com, comment));
            }
        }
        private bool Write2File(string str2write)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(expFile, true);
                sw.Write(str2write);
                sw.Flush();
                return true;
            }
            catch (IOException)
            {
                return false;
            }
            catch (NotSupportedException)
            {
                return false;
            }
            finally
            {
                if(sw != null)
                    sw.Dispose();
            }
        }

        #region threading
        private void ThrSQLExport()
        {   
            // comments, crttable, droptable, tableexists, complinserts
            try
            {
                System.Text.StringBuilder sbQry = new System.Text.StringBuilder();
                // parsing the table array
                foreach (string table in expTables)
                {
                    curExpTable = table;
                    MethodInvoker updThrInf = new MethodInvoker(UpdThrInformations);
                    this.BeginInvoke(updThrInf);
                    // drop table?
                    if (strExpSqlOpts.DropTable)
                    {   // preparing query
                        sbQry = new System.Text.StringBuilder("DROP TABLE");
                        if (strExpSqlOpts.TableExists)
                            sbQry.Append(" IF EXISTS");

                        sbQry.Append(String.Format(NyxMain.NyxCI, " `{0}`;\r\n", table));
                        if (strExpSqlOpts.Comments)
                            AddComment("DROP TABLE " + table);
                        // write
                        Write2File(sbQry.ToString());
                    }
                    // create table?
                    if (strExpSqlOpts.CrtTable)
                    {
                        if (strExpSqlOpts.Comments)
                            AddComment("Dumping Table Structure for " + table);

                        if (!CrtTableStm(table))
                            if (thread.ThreadState == System.Threading.ThreadState.Background)
                            {
                                thread.Abort();
                                thread.Join();
                            }
                    }
                    // export data
                    if (!strExpSqlOpts.OnlyStructure)
                    {
                        if (strExpSqlOpts.Comments)
                            AddComment("dumping data for " + table);
                        if (!CrtInsertStm(table))
                            if (thread.ThreadState == System.Threading.ThreadState.Background)
                            {
                                thread.Abort();
                                thread.Join();
                            }
                    }
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                expError = new DBException(GuiHelper.GetResourceMsg("OperationCanceled"));
            }
            catch (IndexOutOfRangeException exc)
            {
                expError = new DBException(exc.Message.ToString(), exc); ;
            }
            finally
            {   // invoke final method
                MethodInvoker thrstopped = new MethodInvoker(ThrStopped);
                this.BeginInvoke(thrstopped);
            }
        }
        private void ThrCustExport()
        {
            try
            {   // getting datatableinfo
                System.Data.DataTable dtFieldInfo;
                if (!FillDtFieldInfo(expCustOptions[1], out dtFieldInfo))
                {
                    if (thread.ThreadState == System.Threading.ThreadState.Running)
                    {
                        thread.Abort();
                        thread.Join();
                    }
                }
                // building query
                System.Text.StringBuilder sbQry = new System.Text.StringBuilder();
                string qry = String.Empty;
                foreach (string column in expColumns)
                {
                    if (sbQry.Length > 0) 
                        sbQry = new System.Text.StringBuilder("SELECT " + column);
                    else 
                        sbQry.Append(", " + column);
                }
                // finalizing query
                sbQry.Append(" FROM " + expCustOptions[1]);
                // executing qry
                if (dbh.ExecuteReader(qry))
                {   // init
                    int colCnt = dbh.FieldCount, fieldval = 0;
                    string exp_rep = String.Empty, fieldinf = String.Empty;
                    bool isint, isdtt, isdt;
                    System.Text.StringBuilder sbExpStr;
                    // read
                    while (dbh.Read())
                    {
                        // reset expStr
                        sbExpStr = new System.Text.StringBuilder();
                        for (int i = 0; i < colCnt; i++)
                        {   // reset checkvars
                            isint = false;  // is int
                            isdtt = false;  // is datetime
                            isdt = false;   // is date
                            // checking field type
                            fieldinf = dtFieldInfo.Rows[i]["type"].ToString();
                            if (fieldinf.Length >= 3 && fieldinf.Substring(0, 3) == "int")
                                isint = true;
                            else if (fieldinf.Length == 4 && fieldinf.Substring(0, 4) == "date")
                                isdt = true;
                            else if (fieldinf.Length >= 6 && fieldinf.Substring(0, 6) == "bigint")
                                isint = true;
                            else if (fieldinf.Length >= 7 && fieldinf.Substring(0, 7) == "tinyint")
                                isint = true;
                            else if (fieldinf.Length >= 8 && fieldinf.Substring(0, 8) == "smallint")
                                isint = true;
                            else if (fieldinf.Length >= 9 && fieldinf.Substring(0, 9) == "mediumint")
                                isint = true;
                            else if (fieldinf.Length >= 8 && fieldinf.Substring(0, 8) == "datetime")
                                isdtt = true;
                            // string / integer / date?
                            if (isint)
                            {
                                fieldval = dbh.GetInt32(i);
                                sbExpStr.Append(fieldval);
                            }
                            else if (isdt)
                            {
                                string tmp = dbh.GetValue(i).ToString();
                                if (tmp.Length > 0 && tmp != "0000-00-00" && tmp != "00.00.0000")
                                    sbExpStr.Append(dbh.GetDateTime(i).ToString("u", System.Globalization.DateTimeFormatInfo.InvariantInfo).Substring(0, 10));
                                else
                                    sbExpStr.Append("0000-00-00");
                            }
                            else if (isdtt)
                            {
                                string tmp = dbh.GetValue(i).ToString();
                                if (tmp.Length > 0 && tmp != "0000-00-00 00:00:00" && tmp != "00.00.0000 00:00:00")
                                    sbExpStr.Append(dbh.GetDateTime(i).ToString("u", System.Globalization.DateTimeFormatInfo.InvariantInfo).Substring(0, 19));
                                else
                                    sbExpStr.Append("0000-00-00 00:00:00");
                            }
                            else
                            {   // string replacements
                                exp_rep = dbh.GetValue(i).ToString();
                                /*
                                exp_rep = exp_rep.Replace(";", "\\;");
                                exp_rep = exp_rep.Replace("'", "''");
                                exp_rep = exp_rep.Replace("\"", "\"\"");
                                exp_rep = exp_rep.Replace("%", "\\%");
                                exp_rep = exp_rep.Replace("_", "\\_");
                                exp_rep = exp_rep.Replace(@"\", @"\\");
                                exp_rep = exp_rep.Replace("\n", "\\n");
                                */
                                sbExpStr.Append(exp_rep);
                            }
                            if (i < colCnt - 1)
                                sbExpStr.Append(expCustOptions[0]);
                        }
                        sbExpStr.Append("\r\n");
                        // write to file
                        Write2File(sbExpStr.ToString());
                    }
                }
                else
                {
                    expError = new DBException(dbh.ErrExc.Message.ToString());
                    return;
                }
            }
            catch (System.Threading.ThreadAbortException)
            {
                expError = new DBException(GuiHelper.GetResourceMsg("OperationCanceled"));
            }
            catch (IndexOutOfRangeException exc)
            {
                expError = new DBException(exc.Message.ToString(), exc);
            }
            finally
            {   // close reader and writer
                dbh.Close();
                // invoke final method
                MethodInvoker thrstopped = new MethodInvoker(ThrStopped);
                this.BeginInvoke(thrstopped);
            }
        }
        private void ThrStopped()
        {
            // setting gui
            this._ssPgBar.Value = 0; 
            this._ssPgBar.Style = ProgressBarStyle.Blocks;
            this._tcExport.Enabled = true;
            // displaying message
            if (expError == null)
            {
                GuiHelper.SetLastSavePath(expFile);
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBExp_ExportDone", new string[] { expFile } ), MessageHandler.EnumMsgType.Information);
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBExp_ExportErr", new string[] { expError.Message.ToString() }),
                            expError, MessageHandler.EnumMsgType.Warning);
            }
        }
        private void UpdThrInformations()
        {
            this._sslblThrInfo.Text = GuiHelper.GetResourceMsg("DBExp_ExportTable", new string[] { curExpTable } );
        }
        #endregion

        #region create functions
        private bool FillDtFieldInfo(string table, out System.Data.DataTable dt)
        {
            // preparing datatable
            // get field infos from the table
            dt = new System.Data.DataTable("fieldinfo");
            dt.Locale = NyxMain.NyxCI;
            dt.Columns.Add("name");
            dt.Columns.Add("type");
            dt.Columns.Add("null");
            dt.Columns.Add("key");
            dt.Columns.Add("default");
            dt.Columns.Add("extra");
            // getting tablestructure
            if (dbh.ExecuteReader("describe " + table))
            {   // read
                try
                {
                    while (dbh.Read())
                    {
                        System.Data.DataRow dr = dt.Rows.Add();
                        dr["name"] = dbh.GetValue(0).ToString();
                        dr["type"] = dbh.GetValue(1).ToString();
                        dr["null"] = dbh.GetValue(2).ToString();
                        dr["key"] = dbh.GetValue(3).ToString();
                        dr["default"] = dbh.GetValue(4).ToString();
                        dr["extra"] = dbh.GetValue(5).ToString();
                    }
                }
                finally
                {
                    dbh.Close();
                }
                return true;
            }
            else
            {
                expError = new DBException(GuiHelper.GetResourceMsg("CI_TablesRetrieveErr", new string[] { dbh.ErrExc.Message.ToString() }),
                                            dbh.ErrExc);
                return false;
            }
        }
        private bool CrtTableStm(string table)
        {   
            // get the create table stm
            if (dbh.ExecuteReader("SHOW CREATE TABLE " + table))
            {   
                try
                {
                    if (dbh.Read())
                    {
                        string crtstm = dbh.GetValue(1).ToString();
                        // now we write to the file
                        Write2File(crtstm + ";");
                        return true;
                    }
                    else
                        return false;
                }
                catch (IndexOutOfRangeException exc)
                {
                    expError = new DBException(exc.Message.ToString(), exc);
                    return false;
                }
                finally { dbh.Close(); }
            }
            else
            {
                expError = new DBException(dbh.ErrExc.Message.ToString(), dbh.ErrExc);
                return false;
            }
        }
        private bool CrtInsertStm(string table)
        {   // helpvar
            int fieldcnt = -1;
            bool isint, isdtt, isdt;
            string fieldinf = String.Empty;

            System.Text.StringBuilder sbQry = new System.Text.StringBuilder();
            System.Text.StringBuilder sbPreQry = new System.Text.StringBuilder();
            string qry_rep = null;
            // at first, we get the table structure
            System.Data.DataTable dtFieldInfo;
            if (!FillDtFieldInfo(table, out dtFieldInfo))
                return false;
            // executing query
            if (dbh.ExecuteReader("SELECT * FROM " + table))
            {   // building qry
                if (strExpSqlOpts.ComplInserts)
                {
                    foreach (System.Data.DataRow dr in dtFieldInfo.Rows)
                    {
                        if (sbPreQry.Length > 0)
                            sbPreQry = new System.Text.StringBuilder(String.Format(NyxMain.NyxCI, "INSERT INTO {0} ({1}", table, dr["name"].ToString()));
                        else 
                            sbPreQry.Append("," + dr["name"].ToString());
                    }
                    if (sbPreQry.Length > 0)
                        sbPreQry.Append(") VALUES (");
                }
                else
                    sbPreQry = new System.Text.StringBuilder("INSERT INTO " + table + " VALUES (");
                // filling helpvars
                fieldcnt = dbh.FieldCount;
                while (dbh.Read())
                {
                    sbQry = new System.Text.StringBuilder(sbPreQry.ToString());
                    // building the insert
                    for (int i = 0; i < fieldcnt; i++)
                    {
                        isint = false;  // is int
                        isdtt = false;   // is datetime
                        isdt = false;
                        // check if we got a numeric value
                        fieldinf = dtFieldInfo.Rows[i]["type"].ToString();
                        if (fieldinf.Length >= 3 && fieldinf.Substring(0, 3) == "int")
                            isint = true;
                        else if (fieldinf.Length == 4 && fieldinf.Substring(0, 4) == "date")
                            isdt = true;
                        else if (fieldinf.Length >= 6 && fieldinf.Substring(0, 6) == "bigint")
                            isint = true;
                        else if (fieldinf.Length >= 7 && fieldinf.Substring(0, 7) == "tinyint")
                            isint = true;
                        else if (fieldinf.Length >= 8 && fieldinf.Substring(0, 8) == "smallint")
                            isint = true;
                        else if (fieldinf.Length >= 9 && fieldinf.Substring(0, 9) == "mediumint")
                            isint = true;
                        else if (fieldinf.Length >= 8 && fieldinf.Substring(0, 8) == "datetime")
                            isdtt = true;

                        // string / integer / date?
                        if (isint)
                            sbQry.Append(dbh.GetInt32(i));
                        else if (isdt)
                        {
                            string tmp = dbh.GetValue(i).ToString();
                            if (tmp.Length > 0 && tmp != "0000-00-00" && tmp != "00.00.0000")
                                sbQry.Append(String.Format(NyxMain.NyxCI, "'{0}'", dbh.GetDateTime(i).ToString("u", System.Globalization.DateTimeFormatInfo.InvariantInfo).Substring(0, 10)));
                            else
                                sbQry.Append("'0000-00-00'");
                        }
                        else if (isdtt)
                        {
                            string tmp = dbh.GetValue(i).ToString();
                            if (tmp.Length > 0 && tmp != "0000-00-00 00:00:00" && tmp != "00.00.0000 00:00:00")
                                sbQry.Append(String.Format(NyxMain.NyxCI, "'{0}'", dbh.GetDateTime(i).ToString("u", System.Globalization.DateTimeFormatInfo.InvariantInfo).Substring(0, 19)));
                            else
                                sbQry.Append("'0000-00-00 00:00:00'");
                        }
                        else
                        {   // string replacements
                            qry_rep = dbh.GetValue(i).ToString();
                            qry_rep = qry_rep.Replace(@"\'", @"\\''");
                            qry_rep = qry_rep.Replace("\\\"", "\\\\\"\"");
                            qry_rep = qry_rep.Replace("\n", @"\n");
                            qry_rep = qry_rep.Replace("\r", @"\r");
                            // finalize
                            sbQry.Append(String.Format(NyxMain.NyxCI, "'{0}'", qry_rep));
                        }
                        if (i < fieldcnt - 1)
                            sbQry.Append(", ");
                    }
                    sbQry.Append(");\r\n");
                    // writing to file
                    Write2File(sbQry.ToString());
                }
            }
            else
            {
                expError = new DBException(dbh.ErrExc.Message.ToString(), dbh.ErrExc);
                return false;
            }
            // closing readers
            dbh.Close();
            return true;
        }
        #endregion

        #region gui functions
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _btOk_Click(object sender, EventArgs e)
        {   // reset error
            expError = null;
            // start export
            if (this._tcExport.SelectedIndex == 0)
                ExpTablesSQL();
            else if (this._tcExport.SelectedIndex == 1)
                ExpTablesCust();
        }
        private void _btOpenExp_Click(object sender, EventArgs e)
        {
            if (this._tcExport.SelectedIndex == 0)
            {
                // check file
                if (this._tb_sqlfile.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnNoFileSelected"), MessageHandler.EnumMsgType.Warning);
                    return;
                }
                if (!File.Exists(this._tb_sqlfile.Text))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnFileNotFound"), MessageHandler.EnumMsgType.Warning);
                    return;
                }
                // create process to start the default viewapp
                System.Diagnostics.Process.Start(this._tb_sqlfile.Text);
            }
            else if (this._tcExport.SelectedIndex == 1)
            {
                // check file
                if (this._tb_custfile.Text.Length < 1)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnNoFileSelected"), MessageHandler.EnumMsgType.Warning);
                    return;
                }
                if (!File.Exists(this._tb_custfile.Text))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("WrnFileNotFound"), MessageHandler.EnumMsgType.Warning);
                    return;
                }
                // create process to start the default viewapp
                System.Diagnostics.Process.Start(this._tb_custfile.Text);
            }
        }
        private void _bt_sqlselfile_Click(object sender, EventArgs e)
        {
            if (GuiHelper.SaveFileDialog("sql", "Sql (*.sql)|*.sql|Text files (*.txt)|*.txt|All files (*.*)|*.*"))
                this._tb_sqlfile.Text = GuiHelper.Fullpath;
        }
        private void _ckb_droptable_CheckedChanged(object sender, EventArgs e)
        {
            if (this._ckb_droptable.Checked)
            {
                this._ckb_crttable.Checked = true;
                this._ckb_crttable.Enabled = false;
            }
            else
            {
                this._ckb_crttable.Checked = false;
                this._ckb_crttable.Enabled = true;
            }
        }
        private void _tb_filename_TextChanged(object sender, EventArgs e)
        {
            if (this._tb_sqlfile.Text.Length > 0 && this._lv_tables.CheckedItems.Count > 0)
                this._btOk.Enabled = true;
            else
                this._btOk.Enabled = false;
        }
        private void _lv_tables_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (this._tb_sqlfile.Text.Length > 0 && this._lv_tables.CheckedItems.Count > 0)
                this._btOk.Enabled = true;
            else
                this._btOk.Enabled = false;
        }
        private void _bt_custselfile_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.DefaultExt = "csv";
            sfd.Filter = "Coma seperated (*.csv)|*.csv|Tab seperated (*.tsv)|*.tsv|Text files (*.txt)|*.txt|All files (*.*)|*.*";
            sfd.AddExtension = true;
            sfd.CheckPathExists = true;
            sfd.Title = "Select File to save export to...";
            // get last directory
            if (System.IO.Directory.Exists(NyxMain.RTSett.LastSelectedPath))
                sfd.InitialDirectory = NyxMain.RTSett.LastSelectedPath;
            else
                sfd.InitialDirectory = System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            DialogResult dres = sfd.ShowDialog();
            if (dres == DialogResult.OK)
            {
                string file = sfd.FileName;
                this._tb_custfile.Text = file;
            }
        }
        private void _tb_custfile_TextChanged(object sender, EventArgs e)
        {
            if (this._tb_custfile.Text.Length > 0)
            {
                if (this._lv_custcolumns.CheckedItems.Count > 0)
                    this._btOk.Enabled = true;
                this._btOpenExp.Enabled = true;
            }
            else
            {
                if (this._lv_custcolumns.CheckedItems.Count < 1)
                    this._btOk.Enabled = false;
                this._btOpenExp.Enabled = false;
            }
        }
        private void _lv_custcolumns_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (this._tb_custfile.Text.Length > 0 && this._lv_custcolumns.CheckedItems.Count > 0)
                this._btOk.Enabled = true;
            else
                this._btOk.Enabled = false;
        }
        private void _cb_custtable_SelectedIndexChanged(object sender, EventArgs e)
        {   // clearing up listview
            this._lv_custcolumns.Items.Clear();
            // refreshing listview
            if (dbh.ExecuteReader("describe " + this._cb_custtable.Text))
            {   // read
                while (dbh.Read())
                {
                    ListViewItem lvi = GuiHelper.BuildListViewItem(dbh.GetValue(0).ToString(),dbh.GetValue(1).ToString());
                    this._lv_custcolumns.Items.Add(lvi);
                }
                dbh.Close();
            }
            else
                this._lv_custcolumns.Enabled = false;
        }
        private void _tcExport_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._tcExport.SelectedIndex == 0)
            {
                if (this._tb_sqlfile.Text.Length > 0)
                {
                    if (this._lv_tables.CheckedItems.Count > 0)
                        this._btOk.Enabled = true;
                    this._btOpenExp.Enabled = true;
                }
                else
                {
                    if(this._lv_tables.CheckedItems.Count < 1)
                        this._btOk.Enabled = false;
                    this._btOpenExp.Enabled = false;
                }
            }
            else if (this._tcExport.SelectedIndex == 1)
            {
                if (this._tb_custfile.Text.Length > 0)
                {
                    if (this._lv_custcolumns.CheckedItems.Count > 0)
                        this._btOk.Enabled = true;
                    this._btOpenExp.Enabled = true;
                }
                else
                {
                    if (this._lv_custcolumns.CheckedItems.Count < 1)
                        this._btOk.Enabled = false;
                    this._btOpenExp.Enabled = false;
                }
            }
        }
        private void _tbSqlFile_TextChanged(object sender, EventArgs e)
        {
            if (this._tb_sqlfile.Text.Length > 0)
            {
                if (this._lv_tables.CheckedItems.Count > 0)
                    this._btOk.Enabled = true;
                this._btOpenExp.Enabled = true;
            }
            else
            {
                if (this._lv_tables.CheckedItems.Count < 1)
                    this._btOk.Enabled = false;
                this._btOpenExp.Enabled = false;
            }
        }
        private void _btUp_Click(object sender, EventArgs e)
        {
            if (this._lv_custcolumns.SelectedItems.Count == 1)
            {
                int Idx = this._lv_custcolumns.SelectedIndices[0];
                if (Idx - 1 >= 0)
                {   // tmp helpvar
                    string[] tmp = new string[2] { "", "" };
                    tmp[0] = this._lv_custcolumns.Items[Idx - 1].Text;
                    tmp[1] = this._lv_custcolumns.Items[Idx - 1].SubItems[1].Text;
                    // switching items
                    this._lv_custcolumns.Items[Idx - 1].Text = this._lv_custcolumns.Items[Idx].Text;
                    this._lv_custcolumns.Items[Idx - 1].SubItems[1].Text = this._lv_custcolumns.Items[Idx].SubItems[1].Text;
                    this._lv_custcolumns.Items[Idx].Text = tmp[0];
                    this._lv_custcolumns.Items[Idx].SubItems[1].Text = tmp[1];
                    // moving selection up
                    this._lv_custcolumns.Items[Idx - 1].Selected = true;
                }
            }
        }
        private void _btDown_Click(object sender, EventArgs e)
        {
            if (this._lv_custcolumns.SelectedItems.Count == 1)
            {
                int Idx = this._lv_custcolumns.SelectedIndices[0];
                if (this._lv_custcolumns.Items.Count > Idx + 1)
                {   // tmp helpvar
                    string[] tmp = new string[2] { "", "" };
                    tmp[0] = this._lv_custcolumns.Items[Idx + 1].Text;
                    tmp[1] = this._lv_custcolumns.Items[Idx + 1].SubItems[1].Text;
                    // switching items
                    this._lv_custcolumns.Items[Idx + 1].Text = this._lv_custcolumns.Items[Idx].Text;
                    this._lv_custcolumns.Items[Idx + 1].SubItems[1].Text = this._lv_custcolumns.Items[Idx].SubItems[1].Text;
                    this._lv_custcolumns.Items[Idx].Text = tmp[0];
                    this._lv_custcolumns.Items[Idx].SubItems[1].Text = tmp[1];
                    // moving selection down
                    this._lv_custcolumns.Items[Idx + 1].Selected = true;
                }
            }
        }
        #endregion

        #region cm listview
        private void _cmlv_selall_Click(object sender, EventArgs e)
        {   // which listview?
            if(this._tcExport.SelectedIndex == 0)
                GuiHelper.ListViewCheckedState(this._lv_tables, GuiHelper.EnumLVSelection.Select);
            else
                GuiHelper.ListViewCheckedState(this._lv_custcolumns, GuiHelper.EnumLVSelection.Select);
        }
        private void _cmlv_selnone_Click(object sender, EventArgs e)
        {   // which listview?
            if (this._tcExport.SelectedIndex == 0)
                GuiHelper.ListViewCheckedState(this._lv_tables, GuiHelper.EnumLVSelection.Unselect);
            else
                GuiHelper.ListViewCheckedState(this._lv_custcolumns, GuiHelper.EnumLVSelection.Unselect);
        }
        private void _cmlv_invertsel_Click(object sender, EventArgs e)
        {   // which listview?
            if (this._tcExport.SelectedIndex == 0)
                GuiHelper.ListViewCheckedState(this._lv_tables, GuiHelper.EnumLVSelection.Invert);
            else
                GuiHelper.ListViewCheckedState(this._lv_custcolumns, GuiHelper.EnumLVSelection.Invert);
        }
        #endregion

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}