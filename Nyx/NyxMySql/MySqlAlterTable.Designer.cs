namespace Nyx.NyxMySql
{
    /// <summary>
    /// MySql Alter Table Dialog, basic alter table options
    /// </summary>
    partial class MySqlAlterTable
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlAlterTable));
            this._gbTable = new System.Windows.Forms.GroupBox();
            this._gbDefault = new System.Windows.Forms.GroupBox();
            this._lblDefault_Collate = new System.Windows.Forms.Label();
            this._cbDefault_Collate = new System.Windows.Forms.ComboBox();
            this._lblDefault_Charset = new System.Windows.Forms.Label();
            this._cbDefault_Charset = new System.Windows.Forms.ComboBox();
            this._cbOrderBy = new System.Windows.Forms.ComboBox();
            this._lblOrderBy = new System.Windows.Forms.Label();
            this._lblTablespace = new System.Windows.Forms.Label();
            this._gbConvert = new System.Windows.Forms.GroupBox();
            this._lblConvert_Collate = new System.Windows.Forms.Label();
            this._cbConvert_Collate = new System.Windows.Forms.ComboBox();
            this._lblConvert_Charset = new System.Windows.Forms.Label();
            this._cbConvert_Charset = new System.Windows.Forms.ComboBox();
            this._cbTablespace = new System.Windows.Forms.ComboBox();
            this._cbKeys = new System.Windows.Forms.ComboBox();
            this._tbName = new System.Windows.Forms.TextBox();
            this._lblName = new System.Windows.Forms.Label();
            this._lblKeys = new System.Windows.Forms.Label();
            this._btOk = new System.Windows.Forms.Button();
            this._btCancel = new System.Windows.Forms.Button();
            this._gbTable.SuspendLayout();
            this._gbDefault.SuspendLayout();
            this._gbConvert.SuspendLayout();
            this.SuspendLayout();
            // 
            // _gbTable
            // 
            this._gbTable.Controls.Add(this._gbDefault);
            this._gbTable.Controls.Add(this._cbOrderBy);
            this._gbTable.Controls.Add(this._lblOrderBy);
            this._gbTable.Controls.Add(this._lblTablespace);
            this._gbTable.Controls.Add(this._gbConvert);
            this._gbTable.Controls.Add(this._cbTablespace);
            this._gbTable.Controls.Add(this._cbKeys);
            this._gbTable.Controls.Add(this._tbName);
            this._gbTable.Controls.Add(this._lblName);
            this._gbTable.Controls.Add(this._lblKeys);
            resources.ApplyResources(this._gbTable, "_gbTable");
            this._gbTable.Name = "_gbTable";
            this._gbTable.TabStop = false;
            // 
            // _gbDefault
            // 
            this._gbDefault.Controls.Add(this._lblDefault_Collate);
            this._gbDefault.Controls.Add(this._cbDefault_Collate);
            this._gbDefault.Controls.Add(this._lblDefault_Charset);
            this._gbDefault.Controls.Add(this._cbDefault_Charset);
            resources.ApplyResources(this._gbDefault, "_gbDefault");
            this._gbDefault.Name = "_gbDefault";
            this._gbDefault.TabStop = false;
            // 
            // _lblDefault_Collate
            // 
            resources.ApplyResources(this._lblDefault_Collate, "_lblDefault_Collate");
            this._lblDefault_Collate.Name = "_lblDefault_Collate";
            // 
            // _cbDefault_Collate
            // 
            this._cbDefault_Collate.BackColor = System.Drawing.Color.GhostWhite;
            this._cbDefault_Collate.FormattingEnabled = true;
            resources.ApplyResources(this._cbDefault_Collate, "_cbDefault_Collate");
            this._cbDefault_Collate.Name = "_cbDefault_Collate";
            // 
            // _lblDefault_Charset
            // 
            resources.ApplyResources(this._lblDefault_Charset, "_lblDefault_Charset");
            this._lblDefault_Charset.Name = "_lblDefault_Charset";
            // 
            // _cbDefault_Charset
            // 
            this._cbDefault_Charset.BackColor = System.Drawing.Color.GhostWhite;
            this._cbDefault_Charset.FormattingEnabled = true;
            resources.ApplyResources(this._cbDefault_Charset, "_cbDefault_Charset");
            this._cbDefault_Charset.Name = "_cbDefault_Charset";
            // 
            // _cbOrderBy
            // 
            this._cbOrderBy.BackColor = System.Drawing.Color.GhostWhite;
            this._cbOrderBy.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbOrderBy.FormattingEnabled = true;
            this._cbOrderBy.Items.AddRange(new object[] {
            resources.GetString("_cbOrderBy.Items")});
            resources.ApplyResources(this._cbOrderBy, "_cbOrderBy");
            this._cbOrderBy.Name = "_cbOrderBy";
            // 
            // _lblOrderBy
            // 
            resources.ApplyResources(this._lblOrderBy, "_lblOrderBy");
            this._lblOrderBy.Name = "_lblOrderBy";
            // 
            // _lblTablespace
            // 
            resources.ApplyResources(this._lblTablespace, "_lblTablespace");
            this._lblTablespace.Name = "_lblTablespace";
            // 
            // _gbConvert
            // 
            this._gbConvert.Controls.Add(this._lblConvert_Collate);
            this._gbConvert.Controls.Add(this._cbConvert_Collate);
            this._gbConvert.Controls.Add(this._lblConvert_Charset);
            this._gbConvert.Controls.Add(this._cbConvert_Charset);
            resources.ApplyResources(this._gbConvert, "_gbConvert");
            this._gbConvert.Name = "_gbConvert";
            this._gbConvert.TabStop = false;
            // 
            // _lblConvert_Collate
            // 
            resources.ApplyResources(this._lblConvert_Collate, "_lblConvert_Collate");
            this._lblConvert_Collate.Name = "_lblConvert_Collate";
            // 
            // _cbConvert_Collate
            // 
            this._cbConvert_Collate.BackColor = System.Drawing.Color.GhostWhite;
            this._cbConvert_Collate.FormattingEnabled = true;
            resources.ApplyResources(this._cbConvert_Collate, "_cbConvert_Collate");
            this._cbConvert_Collate.Name = "_cbConvert_Collate";
            // 
            // _lblConvert_Charset
            // 
            resources.ApplyResources(this._lblConvert_Charset, "_lblConvert_Charset");
            this._lblConvert_Charset.Name = "_lblConvert_Charset";
            // 
            // _cbConvert_Charset
            // 
            this._cbConvert_Charset.BackColor = System.Drawing.Color.GhostWhite;
            this._cbConvert_Charset.FormattingEnabled = true;
            resources.ApplyResources(this._cbConvert_Charset, "_cbConvert_Charset");
            this._cbConvert_Charset.Name = "_cbConvert_Charset";
            // 
            // _cbTablespace
            // 
            this._cbTablespace.BackColor = System.Drawing.Color.GhostWhite;
            this._cbTablespace.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbTablespace.FormattingEnabled = true;
            this._cbTablespace.Items.AddRange(new object[] {
            resources.GetString("_cbTablespace.Items"),
            resources.GetString("_cbTablespace.Items1"),
            resources.GetString("_cbTablespace.Items2")});
            resources.ApplyResources(this._cbTablespace, "_cbTablespace");
            this._cbTablespace.Name = "_cbTablespace";
            // 
            // _cbKeys
            // 
            this._cbKeys.BackColor = System.Drawing.Color.GhostWhite;
            this._cbKeys.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbKeys.FormattingEnabled = true;
            this._cbKeys.Items.AddRange(new object[] {
            resources.GetString("_cbKeys.Items"),
            resources.GetString("_cbKeys.Items1"),
            resources.GetString("_cbKeys.Items2")});
            resources.ApplyResources(this._cbKeys, "_cbKeys");
            this._cbKeys.Name = "_cbKeys";
            // 
            // _tbName
            // 
            this._tbName.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbName, "_tbName");
            this._tbName.Name = "_tbName";
            // 
            // _lblName
            // 
            resources.ApplyResources(this._lblName, "_lblName");
            this._lblName.Name = "_lblName";
            // 
            // _lblKeys
            // 
            resources.ApplyResources(this._lblKeys, "_lblKeys");
            this._lblKeys.Name = "_lblKeys";
            // 
            // _btOk
            // 
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btOk_Click);
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btCancel_Click);
            // 
            // MySqlAlterTable
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Controls.Add(this._gbTable);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "MySqlAlterTable";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MySqlAlterTable_FormClosing);
            this.Load += new System.EventHandler(this.MySqlAlterTable_Load);
            this._gbTable.ResumeLayout(false);
            this._gbTable.PerformLayout();
            this._gbDefault.ResumeLayout(false);
            this._gbDefault.PerformLayout();
            this._gbConvert.ResumeLayout(false);
            this._gbConvert.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _gbTable;
        private System.Windows.Forms.GroupBox _gbConvert;
        private System.Windows.Forms.Label _lblConvert_Charset;
        private System.Windows.Forms.ComboBox _cbConvert_Charset;
        private System.Windows.Forms.ComboBox _cbKeys;
        private System.Windows.Forms.Label _lblName;
        private System.Windows.Forms.Label _lblKeys;
        private System.Windows.Forms.Label _lblConvert_Collate;
        private System.Windows.Forms.ComboBox _cbConvert_Collate;
        private System.Windows.Forms.Label _lblTablespace;
        private System.Windows.Forms.ComboBox _cbTablespace;
        private System.Windows.Forms.TextBox _tbName;
        private System.Windows.Forms.ComboBox _cbOrderBy;
        private System.Windows.Forms.Label _lblOrderBy;
        private System.Windows.Forms.GroupBox _gbDefault;
        private System.Windows.Forms.Label _lblDefault_Collate;
        private System.Windows.Forms.ComboBox _cbDefault_Collate;
        private System.Windows.Forms.Label _lblDefault_Charset;
        private System.Windows.Forms.ComboBox _cbDefault_Charset;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.Button _btCancel;
    }
}