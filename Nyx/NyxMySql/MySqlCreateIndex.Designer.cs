namespace Nyx.NyxMySql
{
    /// <summary>
    /// MySql Create Index GUI
    /// </summary>
    partial class MySqlCreateIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlCreateIndex));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this._lblType = new System.Windows.Forms.Label();
            this._cbType = new System.Windows.Forms.ComboBox();
            this._lblAdd = new System.Windows.Forms.Label();
            this._lblTable = new System.Windows.Forms.Label();
            this._cbAdd = new System.Windows.Forms.ComboBox();
            this._tbIdxName = new System.Windows.Forms.TextBox();
            this._lblIdxName = new System.Windows.Forms.Label();
            this._cbTables = new System.Windows.Forms.ComboBox();
            this._dgFields = new System.Windows.Forms.DataGridView();
            this.IndexFld = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FieldName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FieldType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.UseFirst = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tooltip = new System.Windows.Forms.ToolTip(this.components);
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._gbGeneral = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this._dgFields)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this._gbGeneral.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lblType
            // 
            resources.ApplyResources(this._lblType, "_lblType");
            this._lblType.Name = "_lblType";
            // 
            // _cbType
            // 
            this._cbType.BackColor = System.Drawing.Color.GhostWhite;
            this._cbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbType.FormattingEnabled = true;
            this._cbType.Items.AddRange(new object[] {
            resources.GetString("_cbType.Items"),
            resources.GetString("_cbType.Items1"),
            resources.GetString("_cbType.Items2")});
            resources.ApplyResources(this._cbType, "_cbType");
            this._cbType.Name = "_cbType";
            // 
            // _lblAdd
            // 
            resources.ApplyResources(this._lblAdd, "_lblAdd");
            this._lblAdd.Name = "_lblAdd";
            // 
            // _lblTable
            // 
            resources.ApplyResources(this._lblTable, "_lblTable");
            this._lblTable.Name = "_lblTable";
            // 
            // _cbAdd
            // 
            this._cbAdd.BackColor = System.Drawing.Color.GhostWhite;
            this._cbAdd.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbAdd.FormattingEnabled = true;
            this._cbAdd.Items.AddRange(new object[] {
            resources.GetString("_cbAdd.Items"),
            resources.GetString("_cbAdd.Items1"),
            resources.GetString("_cbAdd.Items2"),
            resources.GetString("_cbAdd.Items3"),
            resources.GetString("_cbAdd.Items4")});
            resources.ApplyResources(this._cbAdd, "_cbAdd");
            this._cbAdd.Name = "_cbAdd";
            // 
            // _tbIdxName
            // 
            this._tbIdxName.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tbIdxName, "_tbIdxName");
            this._tbIdxName.Name = "_tbIdxName";
            // 
            // _lblIdxName
            // 
            resources.ApplyResources(this._lblIdxName, "_lblIdxName");
            this._lblIdxName.Name = "_lblIdxName";
            // 
            // _cbTables
            // 
            this._cbTables.BackColor = System.Drawing.Color.GhostWhite;
            this._cbTables.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbTables.DropDownWidth = 200;
            this._cbTables.FormattingEnabled = true;
            this._cbTables.Items.AddRange(new object[] {
            resources.GetString("_cbTables.Items")});
            resources.ApplyResources(this._cbTables, "_cbTables");
            this._cbTables.Name = "_cbTables";
            this._cbTables.SelectedIndexChanged += new System.EventHandler(this._cbTables_SelectedIndexChanged);
            // 
            // _dgFields
            // 
            this._dgFields.AllowUserToAddRows = false;
            this._dgFields.AllowUserToDeleteRows = false;
            this._dgFields.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this._dgFields.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dgFields.BackgroundColor = System.Drawing.Color.GhostWhite;
            this._dgFields.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgFields.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._dgFields.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgFields.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IndexFld,
            this.FieldName,
            this.FieldType,
            this.UseFirst});
            this.tableLayoutPanel1.SetColumnSpan(this._dgFields, 2);
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgFields.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this._dgFields, "_dgFields");
            this._dgFields.MultiSelect = false;
            this._dgFields.Name = "_dgFields";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgFields.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this._dgFields.RowHeadersVisible = false;
            this._dgFields.RowTemplate.Height = 18;
            this._dgFields.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._tooltip.SetToolTip(this._dgFields, resources.GetString("_dgFields.ToolTip"));
            // 
            // IndexFld
            // 
            resources.ApplyResources(this.IndexFld, "IndexFld");
            this.IndexFld.Name = "IndexFld";
            // 
            // FieldName
            // 
            resources.ApplyResources(this.FieldName, "FieldName");
            this.FieldName.Name = "FieldName";
            this.FieldName.ReadOnly = true;
            // 
            // FieldType
            // 
            resources.ApplyResources(this.FieldType, "FieldType");
            this.FieldType.Name = "FieldType";
            this.FieldType.ReadOnly = true;
            // 
            // UseFirst
            // 
            resources.ApplyResources(this.UseFirst, "UseFirst");
            this.UseFirst.Name = "UseFirst";
            this.UseFirst.ReadOnly = true;
            // 
            // _btCancel
            // 
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btClose_Click);
            // 
            // _btOk
            // 
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btCreate_Click);
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this._dgFields, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._gbGeneral, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._btOk, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this._btCancel, 1, 2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // _gbGeneral
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._gbGeneral, 2);
            this._gbGeneral.Controls.Add(this._cbAdd);
            this._gbGeneral.Controls.Add(this._lblType);
            this._gbGeneral.Controls.Add(this._cbTables);
            this._gbGeneral.Controls.Add(this._cbType);
            this._gbGeneral.Controls.Add(this._lblIdxName);
            this._gbGeneral.Controls.Add(this._lblAdd);
            this._gbGeneral.Controls.Add(this._tbIdxName);
            this._gbGeneral.Controls.Add(this._lblTable);
            resources.ApplyResources(this._gbGeneral, "_gbGeneral");
            this._gbGeneral.Name = "_gbGeneral";
            this._gbGeneral.TabStop = false;
            // 
            // MySqlCreateIndex
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "MySqlCreateIndex";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MySqlCreateIndex_FormClosing);
            this.Load += new System.EventHandler(this.MySqlCreateIndex_Load);
            ((System.ComponentModel.ISupportInitialize)(this._dgFields)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this._gbGeneral.ResumeLayout(false);
            this._gbGeneral.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox _cbTables;
        private System.Windows.Forms.TextBox _tbIdxName;
        private System.Windows.Forms.Label _lblIdxName;
        private System.Windows.Forms.Label _lblTable;
        private System.Windows.Forms.ComboBox _cbAdd;
        private System.Windows.Forms.Label _lblAdd;
        private System.Windows.Forms.Label _lblType;
        private System.Windows.Forms.ComboBox _cbType;
        private System.Windows.Forms.DataGridView _dgFields;
        private System.Windows.Forms.ToolTip _tooltip;
        private System.Windows.Forms.DataGridViewCheckBoxColumn IndexFld;
        private System.Windows.Forms.DataGridViewTextBoxColumn FieldName;
        private System.Windows.Forms.DataGridViewTextBoxColumn FieldType;
        private System.Windows.Forms.DataGridViewTextBoxColumn UseFirst;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.GroupBox _gbGeneral;
    }
}