/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows.Forms;
using Nyx.Classes;
using Nyx.AppDialogs;

namespace Nyx.NyxMySql
{
    public partial class MySqlCreateView : Form
   {    /// <remarks> event which gets fired when a table was created and the dialog is closed </remarks>
        public event EventHandler<DBContentChangedEventArgs> DBContentChanged;
        /// <remarks> internal classes </remarks>
        private DBHelper dbh = new DBHelper(NyxSettings.DBType.MySql);
        private DBManagement dbm = new DBManagement(NyxSettings.DBType.MySql);

        /// <summary> mysql create view dialog </summary>
        public MySqlCreateView()
        {
            InitializeComponent();
            // format Datagridview
            this._dgView = GuiHelper.DataGridViewFormat(NyxMain.UASett, this._dgView);
        }
        private void MySqlCreateView_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // connect database
            if (dbh.ConnectDB(NyxMain.ConProfile, false))
            {   // get tables
                Queue<string> qTables;
                if (dbm.GetTables(out qTables, true, dbh))
                {
                    foreach (string table in qTables)
                        this._trvTables.Nodes.Add(GuiHelper.SqlBldrGetTableTN(table));
                }
                else
                {
                    MessageHandler.ShowDialog(dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    // disable gui mode
                    this._tcMode.SelectedIndex = 1;
                    this._tblLayoutGUI.Enabled = false;
                }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
        }
        private void MySqlCreateView_FormClosing(object sender, FormClosingEventArgs e)
        {   // finally, disconnect database again
            dbh.DisconnectDB();
        }

        private void _btOk_Click(object sender, EventArgs e)
        {   // got viewname?
            if (this._tbName.Text.ToString().Length > 0)
            {
                string qry = String.Empty;
                if (this._tcMode.SelectedTab == this._tpGUI)
                {
                    if (this._dgView.Rows.Count < 0)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ViewNoFieldsSelected"), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                        this._trvTables.Focus();
                    }
                    else
                        qry = GUIStm();
                }
                else
                {
                    if (this._rtbViewStm.Text.ToString().Length == 0)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ViewNoSqlSTM"), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                        this._rtbViewStm.Focus();
                    }
                    else
                        qry = String.Format(NyxMain.NyxCI, "CREATE VIEW {0} AS {1}", this._tbName.Text.ToString(), this._rtbViewStm.Text.ToString());
                }
                // execute create statement
                if (qry.Length > 0)
                {
                    if (dbh.ExecuteNonQuery(qry))
                    {   // fire refresh event
                        OnDBContentChanged();
                        // ask if we shall create another view
                        StructRetDialog sRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("ViewCreated", new string[] { this._tbName.Text.ToString() }),
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                        if (sRD.DRes == NyxDialogResult.No)
                            this.Close();
                        else
                        {   // cleanup treeview
                            foreach (TreeNode tn in this._trvTables.Nodes)
                            {
                                tn.Collapse();
                                tn.Nodes.Clear();
                                tn.Nodes.Add(GuiHelper.GetResourceMsg("CV_RetrievingString"));
                            }
                            // and rest of gui
                            this._dgView.Rows.Clear();
                            this._tbName.Text = String.Empty;
                            this._tbName.Focus();
                        }
                    }
                    else
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ViewCreateErr", new string[] { dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ViewNameErr"), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this._tbName.Focus();
            }
        }
        private string QueryStm()
        {   // build the simple sql statement
            return String.Format(NyxMain.NyxCI, "CREATE VIEW {0} AS {1}", this._tbName.Text.ToString(), this._rtbViewStm.Text.ToString());
        }
        private string GUIStm()
        {   // init vars
            string qry = String.Empty;
            string selfields = String.Empty, tables = String.Empty;
            Hashtable htWhereStm = new Hashtable();
            // parse the datagridview
            foreach (DataGridViewRow dgvr in this._dgView.Rows)
            {   // check select fields, tables
                if (dgvr.Cells[vShowInView.Index].Value.ToString() == "Checked")
                {
                    string field = dgvr.Cells[vField.Index].Value.ToString();
                    string table = dgvr.Cells[vTable.Index].Value.ToString();
                    if (selfields.Length == 0)
                        selfields = String.Format(NyxMain.NyxCI, "{0}.{1}", table, field);
                    else
                        selfields = String.Format(NyxMain.NyxCI, "{0},{1}.{2}", selfields, table, field);
                    if (tables.IndexOf(table) == -1 || tables.Length == 0)
                    {
                        if (tables.Length == 0)
                            tables = table;
                        else
                            tables = String.Format(NyxMain.NyxCI, "{0},{1}", tables, table);
                    }
                }
                // check filter and group
                if (dgvr.Cells[vFilter.Index].Value.ToString().Length > 0 
                    && dgvr.Cells[vValue.Index].Value.ToString().Length > 0
                    && dgvr.Cells[vGroup.Index].Value.ToString().Length > 0
                    && htWhereStm != null)
                {   // init filtervalue
                    string[] fin = new string[2] { String.Empty, String.Empty };
                    // get the key and filtervalues
                    string key = dgvr.Cells[vGroup.Index].Value.ToString();
                    fin[0] = String.Format(NyxMain.NyxCI, "{0}.{1} {2} {3}",
                                        dgvr.Cells[vTable.Index].Value.ToString(), dgvr.Cells[vField.Index].Value.ToString(),
                                        dgvr.Cells[vFilter.Index].Value.ToString(), dgvr.Cells[vValue.Index].Value.ToString());
                    // check relation (AND/OR...)
                    fin[1] = "AND";
                    if (dgvr.Index > 0)
                    {   // check
                        if (dgvr.Cells[vLOperator.Index].Value != null)
                            fin[1] = dgvr.Cells[vLOperator.Index].Value.ToString();
                    }
                    
                    // check if we allready added this key to the arraylist
                    if (!htWhereStm.ContainsKey(key))
                    {   // create new arraylist and add to hashtable
                        List<string[]> al = new List<string[]>();
                        al.Add(fin);
                        htWhereStm.Add(key, al);
                    }
                    else
                    {   // add to arraylist from hashtable
                        List<string[]> al = (List<string[]>)htWhereStm[key];
                        al.Add(fin);
                    }
                }
            }
            // now let's build the query
            if (selfields.Length > 0 && tables.Length > 0)
            {   // first add the fields
                qry = String.Format(NyxMain.NyxCI, "CREATE VIEW {0} AS SELECT {1} FROM {2} ", this._tbName.Text.ToString(), selfields, tables);
                // got where clause
                if (htWhereStm.Count > 0)
                {
                    string whereCl = String.Empty;
                    // parse the datagridview to get the correct order of the groups
                    foreach (DataGridViewRow dgvr in this._dgView.Rows)
                    {   // have we got a this group?
                        string htKey = dgvr.Cells[vGroup.Index].Value.ToString();
                        if (htWhereStm.ContainsKey(htKey))
                        {   
                            // init filter
                            List<string[]> alFilter = (List<string[]>)htWhereStm[htKey];
                            // we got a group with more than 2 members
                            if (alFilter.Count > 1 && htKey != "0")
                            {
                                string tmpCl = String.Empty, firstLOp = String.Empty;
                                foreach (string[] clause in alFilter)
                                {   // set first logical operator
                                    if (firstLOp.Length == 0)
                                        firstLOp = clause[1];
                                    // parse other filtervalues
                                    if (tmpCl.Length == 0)
                                        tmpCl = String.Format(NyxMain.NyxCI, "({0}", clause[0]);
                                    else
                                        tmpCl = String.Format(NyxMain.NyxCI, "{0} {1} {2}", tmpCl, clause[1], clause[0]);
                                }
                                // add to wherecl
                                if (whereCl.Length == 0)
                                    whereCl = String.Format(NyxMain.NyxCI, "{0})", tmpCl);
                                else
                                    whereCl = String.Format(NyxMain.NyxCI, "{0} {1} {2})", whereCl, firstLOp, tmpCl);
                            }
                            else
                            {
                                string tmpCl = String.Empty, firstLOp = String.Empty; 
                                foreach (string[] clause in alFilter)
                                {   // set first logical operator
                                    if (firstLOp.Length == 0)
                                        firstLOp = clause[1];
                                    // parse other filtervalues
                                    if (tmpCl.Length == 0)
                                        tmpCl = String.Format(NyxMain.NyxCI, "{0}", clause[0]);
                                    else
                                        tmpCl = String.Format(NyxMain.NyxCI, "{0} {1} {2}", tmpCl, clause[1], clause[0]);
                                }
                                // add to wherecl
                                if (whereCl.Length == 0)
                                    whereCl = tmpCl;
                                else
                                    whereCl = String.Format(NyxMain.NyxCI, "{0} {1} {2}", whereCl, firstLOp, tmpCl);
                            }
                            // remove from hashtable
                            htWhereStm.Remove(htKey);
                        }
                    }
                    // add to qry
                    if (whereCl.Length > 0)
                        qry = String.Format(NyxMain.NyxCI, "{0} WHERE {1}", qry, whereCl);
                }
            }
            return qry;
        }

        private void _btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _btMoveRight_Click(object sender, EventArgs e)
        {   // check selection and if .Tag != null (if Tag == null it's a table)
            if (this._trvTables.SelectedNode != null && this._trvTables.SelectedNode.Tag != null)
            {
                if (this._trvTables.SelectedNode.Tag is TableDetails)
                {   // search if we allready got such inside
                    TableDetails strTblDesc = (TableDetails)this._trvTables.SelectedNode.Tag;
                    // table, name, show, relation, filter, value, group
                    this._dgView.Rows.Add(this._trvTables.SelectedNode.Parent.Name.ToString(),
                        strTblDesc.Name, CheckState.Checked, String.Empty, String.Empty, String.Empty, 0);
                    this._dgView.Rows[this._dgView.Rows.Count - 1].Tag = strTblDesc;
                    this._trvTables.SelectedNode.Remove();
                }

            }
        }
        private void _btMoveLeft_Click(object sender, EventArgs e)
        {
            if (this._dgView.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dgvr in this._dgView.SelectedRows)
                {   // add to treeview
                    if (dgvr.Tag is TableDetails)
                    {
                        string table = dgvr.Cells[vTable.Index].Value.ToString();
                        this._trvTables.Nodes[table].Nodes.Add(GuiHelper.SqlBldrGetTableDescTN((TableDetails)dgvr.Tag));
                    }
                    this._dgView.Rows.Remove(dgvr);
                }
            }
        }
        private void _btMoveDown_Click(object sender, EventArgs e)
        {   // got selection?
            if (this._dgView.SelectedRows.Count == 1)
            {   // get selection
                DataGridViewSelectedRowCollection dgvsrColl = this._dgView.SelectedRows;
                // get first selected row index
                int idx = this._dgView.Rows.IndexOf(dgvsrColl[0]);
                if (idx + 1 < this._dgView.Rows.Count)
                {   // helpvar
                    DataGridViewRow dgvr = this._dgView.Rows[idx];
                    this._dgView.Rows.RemoveAt(idx);
                    this._dgView.Rows.Insert(idx + 1, dgvr);
                    // set selection
                    this._dgView.CurrentCell = this._dgView.Rows[idx + 1].Cells[0];
                    this._dgView.Rows[idx].Selected = false;
                    this._dgView.Rows[idx + 1].Selected = true;
                }
            }
        }
        private void _btMoveUp_Click(object sender, EventArgs e)
        {   // got selection?
            if (this._dgView.SelectedRows.Count == 1)
            {   // get selection
                DataGridViewSelectedRowCollection dgvsrColl = this._dgView.SelectedRows;
                // get first selected row index
                int idx = this._dgView.Rows.IndexOf(dgvsrColl[0]);
                if (idx - 1 >= 0)
                {   // helpvar
                    DataGridViewRow dgvr = this._dgView.Rows[idx];
                    this._dgView.Rows.RemoveAt(idx);
                    this._dgView.Rows.Insert(idx - 1, dgvr);
                    // set selection
                    this._dgView.CurrentCell = this._dgView.Rows[idx - 1].Cells[0];
                    this._dgView.Rows[idx].Selected = false;
                    this._dgView.Rows[idx - 1].Selected = true;
                }
            }
        }
        private void _trvTables_AfterExpand(object sender, TreeViewEventArgs e)
        {   // check selection
            if (e.Node != null && e.Node.Nodes.Count == 1
                && e.Node.Nodes[0].Text == GuiHelper.GetResourceMsg("SBReceivingFields"))
            {
                e.Node.Nodes.Clear();
                Queue<TableDetails> qTblDesc;
                if (dbm.GetTableDetails(e.Node.Text, out qTblDesc, dbh))
                {
                    foreach (TableDetails strTblDesc in qTblDesc)
                        e.Node.Nodes.Add(GuiHelper.SqlBldrGetTableDescTN(strTblDesc));
                }
            }
        }
        private void _dgView_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            if (this._dgView.Columns[e.ColumnIndex] == vGroup)
            {
                int tmp;
                if (!Int32.TryParse(e.FormattedValue.ToString(), out tmp))
                {
                    this._dgView.Rows[e.RowIndex].ErrorText = GuiHelper.GetResourceMsg("ViewCreateGroupErr");
                    e.Cancel = true;
                }
            }
        }
        private void _dgView_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {   // clear error when user presses cancel
            this._dgView.Rows[e.RowIndex].ErrorText = String.Empty;
        }

        /// <summary>
        /// Fire OnDBContentChanged event
        /// </summary>
        protected void OnDBContentChanged()
        {   // fire event
            DBContentChangedEventArgs e = new DBContentChangedEventArgs();
            e.DBContent = DatabaseContent.Views;

            if (DBContentChanged != null)
                DBContentChanged(this, e);
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}