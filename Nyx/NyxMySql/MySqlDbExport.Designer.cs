namespace Nyx.NyxMySql
{
    partial class MySqlDBExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MySqlDBExport));
            this._tcExport = new System.Windows.Forms.TabControl();
            this._tpSqlExp = new System.Windows.Forms.TabPage();
            this._lv_tables = new System.Windows.Forms.ListView();
            this.tablename = new System.Windows.Forms.ColumnHeader();
            this._cmlv = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmlv_selall = new System.Windows.Forms.ToolStripMenuItem();
            this._cmlv_selnone = new System.Windows.Forms.ToolStripMenuItem();
            this._cmlv_invertsel = new System.Windows.Forms.ToolStripMenuItem();
            this._gb_expopt = new System.Windows.Forms.GroupBox();
            this._tb_sqlfile = new System.Windows.Forms.TextBox();
            this._ckb_onlyStructure = new System.Windows.Forms.CheckBox();
            this._ckb_complinserts = new System.Windows.Forms.CheckBox();
            this._lbl_savefileas = new System.Windows.Forms.Label();
            this._bt_sqlselfileas = new System.Windows.Forms.Button();
            this._ckb_droptable = new System.Windows.Forms.CheckBox();
            this._ckb_tableexist = new System.Windows.Forms.CheckBox();
            this._ckb_comments = new System.Windows.Forms.CheckBox();
            this._ckb_crttable = new System.Windows.Forms.CheckBox();
            this._tpCustExp = new System.Windows.Forms.TabPage();
            this._lv_custcolumns = new System.Windows.Forms.ListView();
            this.name = new System.Windows.Forms.ColumnHeader();
            this.type = new System.Windows.Forms.ColumnHeader();
            this._pnl = new System.Windows.Forms.Panel();
            this._tb_custfile = new System.Windows.Forms.TextBox();
            this._bt_custselfile = new System.Windows.Forms.Button();
            this._cb_custtable = new System.Windows.Forms.ComboBox();
            this._bt_down = new System.Windows.Forms.Button();
            this._cb_custfiletpye = new System.Windows.Forms.ComboBox();
            this._bt_up = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this._lbl_fldsep = new System.Windows.Forms.Label();
            this._pnlControl = new System.Windows.Forms.Panel();
            this._btOpenExp = new System.Windows.Forms.Button();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this._ssPgBar = new System.Windows.Forms.ToolStripProgressBar();
            this._sslblThrInfo = new System.Windows.Forms.ToolStripStatusLabel();
            this._tcExport.SuspendLayout();
            this._tpSqlExp.SuspendLayout();
            this._cmlv.SuspendLayout();
            this._gb_expopt.SuspendLayout();
            this._tpCustExp.SuspendLayout();
            this._pnl.SuspendLayout();
            this._pnlControl.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tcExport
            // 
            this._tcExport.Controls.Add(this._tpSqlExp);
            this._tcExport.Controls.Add(this._tpCustExp);
            resources.ApplyResources(this._tcExport, "_tcExport");
            this._tcExport.Name = "_tcExport";
            this._tcExport.SelectedIndex = 0;
            this._tcExport.SelectedIndexChanged += new System.EventHandler(this._tcExport_SelectedIndexChanged);
            // 
            // _tpSqlExp
            // 
            this._tpSqlExp.BackColor = System.Drawing.SystemColors.Control;
            this._tpSqlExp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tpSqlExp.Controls.Add(this._lv_tables);
            this._tpSqlExp.Controls.Add(this._gb_expopt);
            resources.ApplyResources(this._tpSqlExp, "_tpSqlExp");
            this._tpSqlExp.Name = "_tpSqlExp";
            this._tpSqlExp.UseVisualStyleBackColor = true;
            // 
            // _lv_tables
            // 
            this._lv_tables.BackColor = System.Drawing.Color.GhostWhite;
            this._lv_tables.CheckBoxes = true;
            this._lv_tables.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.tablename});
            this._lv_tables.ContextMenuStrip = this._cmlv;
            resources.ApplyResources(this._lv_tables, "_lv_tables");
            this._lv_tables.GridLines = true;
            this._lv_tables.Name = "_lv_tables";
            this._lv_tables.UseCompatibleStateImageBehavior = false;
            this._lv_tables.View = System.Windows.Forms.View.Details;
            this._lv_tables.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this._lv_tables_ItemChecked);
            // 
            // tablename
            // 
            resources.ApplyResources(this.tablename, "tablename");
            // 
            // _cmlv
            // 
            this._cmlv.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmlv_selall,
            this._cmlv_selnone,
            this._cmlv_invertsel});
            this._cmlv.Name = "_cmlv";
            resources.ApplyResources(this._cmlv, "_cmlv");
            // 
            // _cmlv_selall
            // 
            this._cmlv_selall.Image = global::Nyx.Properties.Resources.arrow_undo;
            resources.ApplyResources(this._cmlv_selall, "_cmlv_selall");
            this._cmlv_selall.Name = "_cmlv_selall";
            this._cmlv_selall.Click += new System.EventHandler(this._cmlv_selall_Click);
            // 
            // _cmlv_selnone
            // 
            this._cmlv_selnone.Image = global::Nyx.Properties.Resources.arrow_redo;
            resources.ApplyResources(this._cmlv_selnone, "_cmlv_selnone");
            this._cmlv_selnone.Name = "_cmlv_selnone";
            this._cmlv_selnone.Click += new System.EventHandler(this._cmlv_selnone_Click);
            // 
            // _cmlv_invertsel
            // 
            this._cmlv_invertsel.Image = global::Nyx.Properties.Resources.arrow_rotate_clockwise;
            resources.ApplyResources(this._cmlv_invertsel, "_cmlv_invertsel");
            this._cmlv_invertsel.Name = "_cmlv_invertsel";
            this._cmlv_invertsel.Click += new System.EventHandler(this._cmlv_invertsel_Click);
            // 
            // _gb_expopt
            // 
            this._gb_expopt.Controls.Add(this._tb_sqlfile);
            this._gb_expopt.Controls.Add(this._ckb_onlyStructure);
            this._gb_expopt.Controls.Add(this._ckb_complinserts);
            this._gb_expopt.Controls.Add(this._lbl_savefileas);
            this._gb_expopt.Controls.Add(this._bt_sqlselfileas);
            this._gb_expopt.Controls.Add(this._ckb_droptable);
            this._gb_expopt.Controls.Add(this._ckb_tableexist);
            this._gb_expopt.Controls.Add(this._ckb_comments);
            this._gb_expopt.Controls.Add(this._ckb_crttable);
            resources.ApplyResources(this._gb_expopt, "_gb_expopt");
            this._gb_expopt.Name = "_gb_expopt";
            this._gb_expopt.TabStop = false;
            // 
            // _tb_sqlfile
            // 
            this._tb_sqlfile.BackColor = System.Drawing.Color.GhostWhite;
            this._tb_sqlfile.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this._tb_sqlfile, "_tb_sqlfile");
            this._tb_sqlfile.Name = "_tb_sqlfile";
            this._tb_sqlfile.TextChanged += new System.EventHandler(this._tbSqlFile_TextChanged);
            // 
            // _ckb_onlyStructure
            // 
            resources.ApplyResources(this._ckb_onlyStructure, "_ckb_onlyStructure");
            this._ckb_onlyStructure.Name = "_ckb_onlyStructure";
            this._ckb_onlyStructure.UseVisualStyleBackColor = true;
            // 
            // _ckb_complinserts
            // 
            resources.ApplyResources(this._ckb_complinserts, "_ckb_complinserts");
            this._ckb_complinserts.Name = "_ckb_complinserts";
            this._ckb_complinserts.UseVisualStyleBackColor = true;
            // 
            // _lbl_savefileas
            // 
            resources.ApplyResources(this._lbl_savefileas, "_lbl_savefileas");
            this._lbl_savefileas.Name = "_lbl_savefileas";
            // 
            // _bt_sqlselfileas
            // 
            resources.ApplyResources(this._bt_sqlselfileas, "_bt_sqlselfileas");
            this._bt_sqlselfileas.Image = global::Nyx.Properties.Resources.folder;
            this._bt_sqlselfileas.Name = "_bt_sqlselfileas";
            this._bt_sqlselfileas.UseVisualStyleBackColor = true;
            this._bt_sqlselfileas.Click += new System.EventHandler(this._bt_sqlselfile_Click);
            // 
            // _ckb_droptable
            // 
            resources.ApplyResources(this._ckb_droptable, "_ckb_droptable");
            this._ckb_droptable.Checked = true;
            this._ckb_droptable.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ckb_droptable.Name = "_ckb_droptable";
            this._ckb_droptable.UseVisualStyleBackColor = true;
            // 
            // _ckb_tableexist
            // 
            resources.ApplyResources(this._ckb_tableexist, "_ckb_tableexist");
            this._ckb_tableexist.Checked = true;
            this._ckb_tableexist.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ckb_tableexist.Name = "_ckb_tableexist";
            this._ckb_tableexist.UseVisualStyleBackColor = true;
            // 
            // _ckb_comments
            // 
            resources.ApplyResources(this._ckb_comments, "_ckb_comments");
            this._ckb_comments.Checked = true;
            this._ckb_comments.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ckb_comments.Name = "_ckb_comments";
            this._ckb_comments.UseVisualStyleBackColor = true;
            // 
            // _ckb_crttable
            // 
            resources.ApplyResources(this._ckb_crttable, "_ckb_crttable");
            this._ckb_crttable.Checked = true;
            this._ckb_crttable.CheckState = System.Windows.Forms.CheckState.Checked;
            this._ckb_crttable.Name = "_ckb_crttable";
            this._ckb_crttable.UseVisualStyleBackColor = true;
            // 
            // _tpCustExp
            // 
            this._tpCustExp.BackColor = System.Drawing.SystemColors.Control;
            this._tpCustExp.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this._tpCustExp.Controls.Add(this._lv_custcolumns);
            this._tpCustExp.Controls.Add(this._pnl);
            resources.ApplyResources(this._tpCustExp, "_tpCustExp");
            this._tpCustExp.Name = "_tpCustExp";
            this._tpCustExp.UseVisualStyleBackColor = true;
            // 
            // _lv_custcolumns
            // 
            this._lv_custcolumns.BackColor = System.Drawing.Color.GhostWhite;
            this._lv_custcolumns.CheckBoxes = true;
            this._lv_custcolumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.name,
            this.type});
            this._lv_custcolumns.ContextMenuStrip = this._cmlv;
            resources.ApplyResources(this._lv_custcolumns, "_lv_custcolumns");
            this._lv_custcolumns.FullRowSelect = true;
            this._lv_custcolumns.GridLines = true;
            this._lv_custcolumns.HideSelection = false;
            this._lv_custcolumns.MultiSelect = false;
            this._lv_custcolumns.Name = "_lv_custcolumns";
            this._lv_custcolumns.UseCompatibleStateImageBehavior = false;
            this._lv_custcolumns.View = System.Windows.Forms.View.Details;
            this._lv_custcolumns.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this._lv_custcolumns_ItemChecked);
            // 
            // name
            // 
            resources.ApplyResources(this.name, "name");
            // 
            // type
            // 
            resources.ApplyResources(this.type, "type");
            // 
            // _pnl
            // 
            this._pnl.Controls.Add(this._tb_custfile);
            this._pnl.Controls.Add(this._bt_custselfile);
            this._pnl.Controls.Add(this._cb_custtable);
            this._pnl.Controls.Add(this._bt_down);
            this._pnl.Controls.Add(this._cb_custfiletpye);
            this._pnl.Controls.Add(this._bt_up);
            this._pnl.Controls.Add(this.label2);
            this._pnl.Controls.Add(this._lbl_fldsep);
            resources.ApplyResources(this._pnl, "_pnl");
            this._pnl.Name = "_pnl";
            // 
            // _tb_custfile
            // 
            this._tb_custfile.BackColor = System.Drawing.Color.GhostWhite;
            this._tb_custfile.BorderStyle = System.Windows.Forms.BorderStyle.None;
            resources.ApplyResources(this._tb_custfile, "_tb_custfile");
            this._tb_custfile.Name = "_tb_custfile";
            this._tb_custfile.TextChanged += new System.EventHandler(this._tb_custfile_TextChanged);
            // 
            // _bt_custselfile
            // 
            resources.ApplyResources(this._bt_custselfile, "_bt_custselfile");
            this._bt_custselfile.Image = global::Nyx.Properties.Resources.folder;
            this._bt_custselfile.Name = "_bt_custselfile";
            this._bt_custselfile.UseVisualStyleBackColor = true;
            this._bt_custselfile.Click += new System.EventHandler(this._bt_custselfile_Click);
            // 
            // _cb_custtable
            // 
            this._cb_custtable.BackColor = System.Drawing.Color.GhostWhite;
            this._cb_custtable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cb_custtable.FormattingEnabled = true;
            resources.ApplyResources(this._cb_custtable, "_cb_custtable");
            this._cb_custtable.Name = "_cb_custtable";
            this._cb_custtable.SelectedIndexChanged += new System.EventHandler(this._cb_custtable_SelectedIndexChanged);
            // 
            // _bt_down
            // 
            resources.ApplyResources(this._bt_down, "_bt_down");
            this._bt_down.Image = global::Nyx.Properties.Resources.arrow_down;
            this._bt_down.Name = "_bt_down";
            this._bt_down.UseVisualStyleBackColor = true;
            this._bt_down.Click += new System.EventHandler(this._btDown_Click);
            // 
            // _cb_custfiletpye
            // 
            this._cb_custfiletpye.BackColor = System.Drawing.Color.GhostWhite;
            this._cb_custfiletpye.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cb_custfiletpye.FormattingEnabled = true;
            this._cb_custfiletpye.Items.AddRange(new object[] {
            resources.GetString("_cb_custfiletpye.Items"),
            resources.GetString("_cb_custfiletpye.Items1"),
            resources.GetString("_cb_custfiletpye.Items2")});
            resources.ApplyResources(this._cb_custfiletpye, "_cb_custfiletpye");
            this._cb_custfiletpye.Name = "_cb_custfiletpye";
            // 
            // _bt_up
            // 
            resources.ApplyResources(this._bt_up, "_bt_up");
            this._bt_up.Image = global::Nyx.Properties.Resources.arrow_up;
            this._bt_up.Name = "_bt_up";
            this._bt_up.UseVisualStyleBackColor = true;
            this._bt_up.Click += new System.EventHandler(this._btUp_Click);
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // _lbl_fldsep
            // 
            resources.ApplyResources(this._lbl_fldsep, "_lbl_fldsep");
            this._lbl_fldsep.Name = "_lbl_fldsep";
            // 
            // _pnlControl
            // 
            this._pnlControl.Controls.Add(this._btOpenExp);
            this._pnlControl.Controls.Add(this._btCancel);
            this._pnlControl.Controls.Add(this._btOk);
            resources.ApplyResources(this._pnlControl, "_pnlControl");
            this._pnlControl.Name = "_pnlControl";
            // 
            // _btOpenExp
            // 
            resources.ApplyResources(this._btOpenExp, "_btOpenExp");
            this._btOpenExp.Image = global::Nyx.Properties.Resources.folder;
            this._btOpenExp.Name = "_btOpenExp";
            this._btOpenExp.UseVisualStyleBackColor = true;
            this._btOpenExp.Click += new System.EventHandler(this._btOpenExp_Click);
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btClose_Click);
            // 
            // _btOk
            // 
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btOk_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ssPgBar,
            this._sslblThrInfo});
            resources.ApplyResources(this.statusStrip1, "statusStrip1");
            this.statusStrip1.Name = "statusStrip1";
            // 
            // _ssPgBar
            // 
            this._ssPgBar.Name = "_ssPgBar";
            resources.ApplyResources(this._ssPgBar, "_ssPgBar");
            // 
            // _sslblThrInfo
            // 
            this._sslblThrInfo.Name = "_sslblThrInfo";
            resources.ApplyResources(this._sslblThrInfo, "_sslblThrInfo");
            // 
            // MySqlDBExport
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._tcExport);
            this.Controls.Add(this._pnlControl);
            this.Controls.Add(this.statusStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "MySqlDBExport";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmDbExport_FormClosing);
            this.Load += new System.EventHandler(this.FrmDbExport_Load);
            this._tcExport.ResumeLayout(false);
            this._tpSqlExp.ResumeLayout(false);
            this._cmlv.ResumeLayout(false);
            this._gb_expopt.ResumeLayout(false);
            this._gb_expopt.PerformLayout();
            this._tpCustExp.ResumeLayout(false);
            this._pnl.ResumeLayout(false);
            this._pnl.PerformLayout();
            this._pnlControl.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl _tcExport;
        private System.Windows.Forms.TabPage _tpSqlExp;
        private System.Windows.Forms.ListView _lv_tables;
        private System.Windows.Forms.ColumnHeader tablename;
        private System.Windows.Forms.GroupBox _gb_expopt;
        private System.Windows.Forms.CheckBox _ckb_onlyStructure;
        private System.Windows.Forms.CheckBox _ckb_complinserts;
        private System.Windows.Forms.Label _lbl_savefileas;
        private System.Windows.Forms.Button _bt_sqlselfileas;
        private System.Windows.Forms.CheckBox _ckb_droptable;
        private System.Windows.Forms.CheckBox _ckb_tableexist;
        private System.Windows.Forms.CheckBox _ckb_comments;
        private System.Windows.Forms.CheckBox _ckb_crttable;
        private System.Windows.Forms.TabPage _tpCustExp;
        private System.Windows.Forms.ListView _lv_custcolumns;
        private System.Windows.Forms.ColumnHeader name;
        private System.Windows.Forms.ColumnHeader type;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label _lbl_fldsep;
        private System.Windows.Forms.ComboBox _cb_custfiletpye;
        private System.Windows.Forms.ComboBox _cb_custtable;
        private System.Windows.Forms.Button _bt_down;
        private System.Windows.Forms.Button _bt_up;
        private System.Windows.Forms.Panel _pnl;
        private System.Windows.Forms.Button _bt_custselfile;
        private System.Windows.Forms.ContextMenuStrip _cmlv;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_selall;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_selnone;
        private System.Windows.Forms.ToolStripMenuItem _cmlv_invertsel;
        private System.Windows.Forms.TextBox _tb_custfile;
        private System.Windows.Forms.TextBox _tb_sqlfile;
        private System.Windows.Forms.Panel _pnlControl;
        private System.Windows.Forms.Button _btOpenExp;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripProgressBar _ssPgBar;
        private System.Windows.Forms.ToolStripStatusLabel _sslblThrInfo;
    }
}