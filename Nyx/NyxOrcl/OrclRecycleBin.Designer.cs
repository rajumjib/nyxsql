namespace Nyx.NyxOrcl
{
    partial class OrclRecycleBin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrclRecycleBin));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this._cmRB = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmRB_Restore = new System.Windows.Forms.ToolStripMenuItem();
            this._cmRB_Purge = new System.Windows.Forms.ToolStripMenuItem();
            this._cmRB_PurgeAll = new System.Windows.Forms.ToolStripMenuItem();
            this._cmRB_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmRB_SelAll = new System.Windows.Forms.ToolStripMenuItem();
            this._cmRB_SelNone = new System.Windows.Forms.ToolStripMenuItem();
            this._cmRB_InvertSel = new System.Windows.Forms.ToolStripMenuItem();
            this._dgContent = new System.Windows.Forms.DataGridView();
            this.dgvcName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcOrigName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcTableSpace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcCreateTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcDropTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcSpace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcUndropable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tcRB = new System.Windows.Forms.TabControl();
            this._tpRBin = new System.Windows.Forms.TabPage();
            this._tableLayoutContent = new System.Windows.Forms.TableLayoutPanel();
            this._btContent_Restore = new System.Windows.Forms.Button();
            this._btContent_Refresh = new System.Windows.Forms.Button();
            this._btContent_PurgeAll = new System.Windows.Forms.Button();
            this._btContent_Purge = new System.Windows.Forms.Button();
            this._tpRBDetails = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._dgDetails = new System.Windows.Forms.DataGridView();
            this.dgvcDet_Parameter = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcDet_Value = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._btDetail_Refresh = new System.Windows.Forms.Button();
            this._dgTablespace = new System.Windows.Forms.DataGridView();
            this.dgvcDet_Tablespace = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvcDet_Retention = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._gbFb = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this._mtbFb_Retention = new System.Windows.Forms.MaskedTextBox();
            this._btFlb_SetRetention = new System.Windows.Forms.Button();
            this._lblFb_Retention = new System.Windows.Forms.Label();
            this._btFb_Off = new System.Windows.Forms.Button();
            this._lblFb_State = new System.Windows.Forms.Label();
            this._btFb_On = new System.Windows.Forms.Button();
            this._tableLayoutMain = new System.Windows.Forms.TableLayoutPanel();
            this._btClose = new System.Windows.Forms.Button();
            this._cmRB.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgContent)).BeginInit();
            this._tcRB.SuspendLayout();
            this._tpRBin.SuspendLayout();
            this._tableLayoutContent.SuspendLayout();
            this._tpRBDetails.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgDetails)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this._dgTablespace)).BeginInit();
            this._gbFb.SuspendLayout();
            this._tableLayoutMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // _cmRB
            // 
            this._cmRB.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmRB_Restore,
            this._cmRB_Purge,
            this._cmRB_PurgeAll,
            this._cmRB_Sep0,
            this._cmRB_SelAll,
            this._cmRB_SelNone,
            this._cmRB_InvertSel});
            this._cmRB.Name = "_cmlv";
            resources.ApplyResources(this._cmRB, "_cmRB");
            // 
            // _cmRB_Restore
            // 
            resources.ApplyResources(this._cmRB_Restore, "_cmRB_Restore");
            this._cmRB_Restore.Image = global::Nyx.Properties.Resources.database_add;
            this._cmRB_Restore.Name = "_cmRB_Restore";
            // 
            // _cmRB_Purge
            // 
            resources.ApplyResources(this._cmRB_Purge, "_cmRB_Purge");
            this._cmRB_Purge.Image = global::Nyx.Properties.Resources.database_delete;
            this._cmRB_Purge.Name = "_cmRB_Purge";
            this._cmRB_Purge.Click += new System.EventHandler(this._contentPurge_Click);
            // 
            // _cmRB_PurgeAll
            // 
            resources.ApplyResources(this._cmRB_PurgeAll, "_cmRB_PurgeAll");
            this._cmRB_PurgeAll.Image = global::Nyx.Properties.Resources.database_delete;
            this._cmRB_PurgeAll.Name = "_cmRB_PurgeAll";
            this._cmRB_PurgeAll.Click += new System.EventHandler(this._contentPurgeAll_Click);
            // 
            // _cmRB_Sep0
            // 
            this._cmRB_Sep0.Name = "_cmRB_Sep0";
            resources.ApplyResources(this._cmRB_Sep0, "_cmRB_Sep0");
            // 
            // _cmRB_SelAll
            // 
            this._cmRB_SelAll.Image = global::Nyx.Properties.Resources.arrow_redo;
            resources.ApplyResources(this._cmRB_SelAll, "_cmRB_SelAll");
            this._cmRB_SelAll.Name = "_cmRB_SelAll";
            this._cmRB_SelAll.Click += new System.EventHandler(this._cmContent_SelAll_Click);
            // 
            // _cmRB_SelNone
            // 
            this._cmRB_SelNone.Image = global::Nyx.Properties.Resources.arrow_undo;
            resources.ApplyResources(this._cmRB_SelNone, "_cmRB_SelNone");
            this._cmRB_SelNone.Name = "_cmRB_SelNone";
            this._cmRB_SelNone.Click += new System.EventHandler(this._cmContent_SelNone_Click);
            // 
            // _cmRB_InvertSel
            // 
            this._cmRB_InvertSel.Image = global::Nyx.Properties.Resources.arrow_rotate_clockwise;
            resources.ApplyResources(this._cmRB_InvertSel, "_cmRB_InvertSel");
            this._cmRB_InvertSel.Name = "_cmRB_InvertSel";
            this._cmRB_InvertSel.Click += new System.EventHandler(this._cmContent_InvertSel_Click);
            // 
            // _dgContent
            // 
            this._dgContent.AllowUserToAddRows = false;
            this._dgContent.AllowUserToDeleteRows = false;
            this._dgContent.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this._dgContent.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dgContent.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._dgContent.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._dgContent.BackgroundColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgContent.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._dgContent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgContent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcName,
            this.dgvcOrigName,
            this.dgvcType,
            this.dgvcTableSpace,
            this.dgvcCreateTime,
            this.dgvcDropTime,
            this.dgvcSpace,
            this.dgvcUndropable});
            this._tableLayoutContent.SetColumnSpan(this._dgContent, 4);
            this._dgContent.ContextMenuStrip = this._cmRB;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgContent.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this._dgContent, "_dgContent");
            this._dgContent.Name = "_dgContent";
            this._dgContent.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgContent.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this._dgContent.RowHeadersVisible = false;
            this._dgContent.RowTemplate.Height = 20;
            this._dgContent.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this._dgContent.SelectionChanged += new System.EventHandler(this._dgContent_SelectionChanged);
            // 
            // dgvcName
            // 
            this.dgvcName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            resources.ApplyResources(this.dgvcName, "dgvcName");
            this.dgvcName.Name = "dgvcName";
            this.dgvcName.ReadOnly = true;
            // 
            // dgvcOrigName
            // 
            this.dgvcOrigName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.dgvcOrigName, "dgvcOrigName");
            this.dgvcOrigName.Name = "dgvcOrigName";
            this.dgvcOrigName.ReadOnly = true;
            // 
            // dgvcType
            // 
            this.dgvcType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.dgvcType, "dgvcType");
            this.dgvcType.Name = "dgvcType";
            this.dgvcType.ReadOnly = true;
            // 
            // dgvcTableSpace
            // 
            resources.ApplyResources(this.dgvcTableSpace, "dgvcTableSpace");
            this.dgvcTableSpace.Name = "dgvcTableSpace";
            this.dgvcTableSpace.ReadOnly = true;
            // 
            // dgvcCreateTime
            // 
            resources.ApplyResources(this.dgvcCreateTime, "dgvcCreateTime");
            this.dgvcCreateTime.Name = "dgvcCreateTime";
            this.dgvcCreateTime.ReadOnly = true;
            // 
            // dgvcDropTime
            // 
            resources.ApplyResources(this.dgvcDropTime, "dgvcDropTime");
            this.dgvcDropTime.Name = "dgvcDropTime";
            this.dgvcDropTime.ReadOnly = true;
            // 
            // dgvcSpace
            // 
            resources.ApplyResources(this.dgvcSpace, "dgvcSpace");
            this.dgvcSpace.Name = "dgvcSpace";
            this.dgvcSpace.ReadOnly = true;
            // 
            // dgvcUndropable
            // 
            resources.ApplyResources(this.dgvcUndropable, "dgvcUndropable");
            this.dgvcUndropable.Name = "dgvcUndropable";
            this.dgvcUndropable.ReadOnly = true;
            // 
            // _tcRB
            // 
            this._tableLayoutMain.SetColumnSpan(this._tcRB, 2);
            this._tcRB.Controls.Add(this._tpRBin);
            this._tcRB.Controls.Add(this._tpRBDetails);
            resources.ApplyResources(this._tcRB, "_tcRB");
            this._tcRB.Name = "_tcRB";
            this._tcRB.SelectedIndex = 0;
            this._tcRB.SelectedIndexChanged += new System.EventHandler(this._tcRB_SelectedIndexChanged);
            // 
            // _tpRBin
            // 
            this._tpRBin.Controls.Add(this._tableLayoutContent);
            resources.ApplyResources(this._tpRBin, "_tpRBin");
            this._tpRBin.Name = "_tpRBin";
            this._tpRBin.UseVisualStyleBackColor = true;
            // 
            // _tableLayoutContent
            // 
            resources.ApplyResources(this._tableLayoutContent, "_tableLayoutContent");
            this._tableLayoutContent.Controls.Add(this._btContent_Restore, 0, 1);
            this._tableLayoutContent.Controls.Add(this._btContent_Refresh, 0, 1);
            this._tableLayoutContent.Controls.Add(this._dgContent, 0, 0);
            this._tableLayoutContent.Controls.Add(this._btContent_PurgeAll, 3, 1);
            this._tableLayoutContent.Controls.Add(this._btContent_Purge, 2, 1);
            this._tableLayoutContent.Name = "_tableLayoutContent";
            // 
            // _btContent_Restore
            // 
            resources.ApplyResources(this._btContent_Restore, "_btContent_Restore");
            this._btContent_Restore.Image = global::Nyx.Properties.Resources.database_add;
            this._btContent_Restore.Name = "_btContent_Restore";
            this._btContent_Restore.UseVisualStyleBackColor = true;
            this._btContent_Restore.Click += new System.EventHandler(this._btContent_Restore_Click);
            // 
            // _btContent_Refresh
            // 
            this._btContent_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._btContent_Refresh, "_btContent_Refresh");
            this._btContent_Refresh.Name = "_btContent_Refresh";
            this._btContent_Refresh.UseVisualStyleBackColor = true;
            this._btContent_Refresh.Click += new System.EventHandler(this._contentRefresh_Click);
            // 
            // _btContent_PurgeAll
            // 
            resources.ApplyResources(this._btContent_PurgeAll, "_btContent_PurgeAll");
            this._btContent_PurgeAll.Image = global::Nyx.Properties.Resources.delete;
            this._btContent_PurgeAll.Name = "_btContent_PurgeAll";
            this._btContent_PurgeAll.UseVisualStyleBackColor = true;
            this._btContent_PurgeAll.Click += new System.EventHandler(this._contentPurgeAll_Click);
            // 
            // _btContent_Purge
            // 
            resources.ApplyResources(this._btContent_Purge, "_btContent_Purge");
            this._btContent_Purge.Image = global::Nyx.Properties.Resources.delete;
            this._btContent_Purge.Name = "_btContent_Purge";
            this._btContent_Purge.UseVisualStyleBackColor = true;
            this._btContent_Purge.Click += new System.EventHandler(this._contentPurge_Click);
            // 
            // _tpRBDetails
            // 
            this._tpRBDetails.Controls.Add(this.tableLayoutPanel1);
            resources.ApplyResources(this._tpRBDetails, "_tpRBDetails");
            this._tpRBDetails.Name = "_tpRBDetails";
            this._tpRBDetails.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this._dgDetails, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this._btDetail_Refresh, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._dgTablespace, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._gbFb, 2, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // _dgDetails
            // 
            this._dgDetails.AllowUserToAddRows = false;
            this._dgDetails.AllowUserToDeleteRows = false;
            this._dgDetails.AllowUserToOrderColumns = true;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this._dgDetails.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this._dgDetails.BackgroundColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgDetails.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this._dgDetails.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgDetails.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcDet_Parameter,
            this.dgvcDet_Value});
            this._dgDetails.ContextMenuStrip = this._cmRB;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgDetails.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this._dgDetails, "_dgDetails");
            this._dgDetails.Name = "_dgDetails";
            this._dgDetails.ReadOnly = true;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgDetails.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this._dgDetails.RowHeadersVisible = false;
            this._dgDetails.RowTemplate.Height = 20;
            this._dgDetails.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // dgvcDet_Parameter
            // 
            this.dgvcDet_Parameter.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.dgvcDet_Parameter, "dgvcDet_Parameter");
            this.dgvcDet_Parameter.Name = "dgvcDet_Parameter";
            this.dgvcDet_Parameter.ReadOnly = true;
            // 
            // dgvcDet_Value
            // 
            this.dgvcDet_Value.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.dgvcDet_Value, "dgvcDet_Value");
            this.dgvcDet_Value.Name = "dgvcDet_Value";
            this.dgvcDet_Value.ReadOnly = true;
            // 
            // _btDetail_Refresh
            // 
            this._btDetail_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._btDetail_Refresh, "_btDetail_Refresh");
            this._btDetail_Refresh.Name = "_btDetail_Refresh";
            this._btDetail_Refresh.UseVisualStyleBackColor = true;
            this._btDetail_Refresh.Click += new System.EventHandler(this._btDetail_Refresh_Click);
            // 
            // _dgTablespace
            // 
            this._dgTablespace.AllowUserToAddRows = false;
            this._dgTablespace.AllowUserToDeleteRows = false;
            this._dgTablespace.AllowUserToOrderColumns = true;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.WhiteSmoke;
            this._dgTablespace.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this._dgTablespace.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._dgTablespace.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._dgTablespace.BackgroundColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgTablespace.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this._dgTablespace.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgTablespace.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvcDet_Tablespace,
            this.dgvcDet_Retention});
            this._dgTablespace.ContextMenuStrip = this._cmRB;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgTablespace.DefaultCellStyle = dataGridViewCellStyle11;
            resources.ApplyResources(this._dgTablespace, "_dgTablespace");
            this._dgTablespace.Name = "_dgTablespace";
            this._dgTablespace.ReadOnly = true;
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgTablespace.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this._dgTablespace.RowHeadersVisible = false;
            this._dgTablespace.RowTemplate.Height = 20;
            this._dgTablespace.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // dgvcDet_Tablespace
            // 
            this.dgvcDet_Tablespace.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.dgvcDet_Tablespace, "dgvcDet_Tablespace");
            this.dgvcDet_Tablespace.Name = "dgvcDet_Tablespace";
            this.dgvcDet_Tablespace.ReadOnly = true;
            // 
            // dgvcDet_Retention
            // 
            this.dgvcDet_Retention.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.dgvcDet_Retention, "dgvcDet_Retention");
            this.dgvcDet_Retention.Name = "dgvcDet_Retention";
            this.dgvcDet_Retention.ReadOnly = true;
            // 
            // _gbFb
            // 
            this._gbFb.Controls.Add(this.label1);
            this._gbFb.Controls.Add(this._mtbFb_Retention);
            this._gbFb.Controls.Add(this._btFlb_SetRetention);
            this._gbFb.Controls.Add(this._lblFb_Retention);
            this._gbFb.Controls.Add(this._btFb_Off);
            this._gbFb.Controls.Add(this._lblFb_State);
            this._gbFb.Controls.Add(this._btFb_On);
            resources.ApplyResources(this._gbFb, "_gbFb");
            this._gbFb.Name = "_gbFb";
            this._gbFb.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // _mtbFb_Retention
            // 
            resources.ApplyResources(this._mtbFb_Retention, "_mtbFb_Retention");
            this._mtbFb_Retention.Name = "_mtbFb_Retention";
            // 
            // _btFlb_SetRetention
            // 
            resources.ApplyResources(this._btFlb_SetRetention, "_btFlb_SetRetention");
            this._btFlb_SetRetention.Name = "_btFlb_SetRetention";
            this._btFlb_SetRetention.UseVisualStyleBackColor = true;
            this._btFlb_SetRetention.Click += new System.EventHandler(this._btFlb_SetRetention_Click);
            // 
            // _lblFb_Retention
            // 
            resources.ApplyResources(this._lblFb_Retention, "_lblFb_Retention");
            this._lblFb_Retention.Name = "_lblFb_Retention";
            // 
            // _btFb_Off
            // 
            this._btFb_Off.ForeColor = System.Drawing.Color.IndianRed;
            resources.ApplyResources(this._btFb_Off, "_btFb_Off");
            this._btFb_Off.Name = "_btFb_Off";
            this._btFb_Off.UseVisualStyleBackColor = true;
            this._btFb_Off.Click += new System.EventHandler(this._btFlashback_Off_Click);
            // 
            // _lblFb_State
            // 
            resources.ApplyResources(this._lblFb_State, "_lblFb_State");
            this._lblFb_State.Name = "_lblFb_State";
            // 
            // _btFb_On
            // 
            this._btFb_On.ForeColor = System.Drawing.Color.ForestGreen;
            resources.ApplyResources(this._btFb_On, "_btFb_On");
            this._btFb_On.Name = "_btFb_On";
            this._btFb_On.UseVisualStyleBackColor = true;
            this._btFb_On.Click += new System.EventHandler(this._btFlashback_On_Click);
            // 
            // _tableLayoutMain
            // 
            resources.ApplyResources(this._tableLayoutMain, "_tableLayoutMain");
            this._tableLayoutMain.Controls.Add(this._tcRB, 0, 0);
            this._tableLayoutMain.Controls.Add(this._btClose, 1, 1);
            this._tableLayoutMain.Name = "_tableLayoutMain";
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // OrclRecycleBin
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btClose;
            this.Controls.Add(this._tableLayoutMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "OrclRecycleBin";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmOrclRecycleBin_FormClosing);
            this.Load += new System.EventHandler(this.FrmOrclRecycleBin_Load);
            this._cmRB.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dgContent)).EndInit();
            this._tcRB.ResumeLayout(false);
            this._tpRBin.ResumeLayout(false);
            this._tableLayoutContent.ResumeLayout(false);
            this._tpRBDetails.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dgDetails)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this._dgTablespace)).EndInit();
            this._gbFb.ResumeLayout(false);
            this._gbFb.PerformLayout();
            this._tableLayoutMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip _cmRB;
        private System.Windows.Forms.ToolStripMenuItem _cmRB_SelAll;
        private System.Windows.Forms.ToolStripMenuItem _cmRB_SelNone;
        private System.Windows.Forms.ToolStripMenuItem _cmRB_InvertSel;
        private System.Windows.Forms.DataGridView _dgContent;
        private System.Windows.Forms.ToolStripMenuItem _cmRB_Purge;
        private System.Windows.Forms.ToolStripSeparator _cmRB_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _cmRB_PurgeAll;
        private System.Windows.Forms.ToolStripMenuItem _cmRB_Restore;
        private System.Windows.Forms.TabControl _tcRB;
        private System.Windows.Forms.TabPage _tpRBin;
        private System.Windows.Forms.TabPage _tpRBDetails;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutContent;
        private System.Windows.Forms.Button _btContent_Purge;
        private System.Windows.Forms.Button _btContent_PurgeAll;
        private System.Windows.Forms.TableLayoutPanel _tableLayoutMain;
        private System.Windows.Forms.Button _btContent_Refresh;
        private System.Windows.Forms.Button _btClose;
        private System.Windows.Forms.Button _btContent_Restore;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button _btDetail_Refresh;
        private System.Windows.Forms.DataGridView _dgDetails;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDet_Parameter;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDet_Value;
        private System.Windows.Forms.DataGridView _dgTablespace;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDet_Tablespace;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDet_Retention;
        private System.Windows.Forms.GroupBox _gbFb;
        private System.Windows.Forms.Button _btFb_Off;
        private System.Windows.Forms.Label _lblFb_State;
        private System.Windows.Forms.Button _btFb_On;
        private System.Windows.Forms.Label _lblFb_Retention;
        private System.Windows.Forms.MaskedTextBox _mtbFb_Retention;
        private System.Windows.Forms.Button _btFlb_SetRetention;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcOrigName;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcType;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcTableSpace;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcCreateTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcDropTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcSpace;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvcUndropable;
    }
}