/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Oracle.DataAccess.Client;
using System.Windows.Forms;
using Nyx.AppDialogs;

namespace Nyx.NyxOrcl
{
    /// <summary> MySql create tablefield dialog </summary>
    public partial class OrclCreateTableField : Form
    {
        /// <remarks> event which gets fired when a table was created and the dialog is closed </remarks>
        public event EventHandler<DBContentChangedEventArgs> DBContentChangedEvent;
        // private vars
        private TableAction tblAction;
        private string table = null, field = null;
        Classes.DBHelper dbh = new Classes.DBHelper(NyxMain.ConProfile.ProfileDBType);

        /// <summary>
        /// init function
        /// </summary>
        /// <param name="action"></param>
        /// <param name="tableName"></param>
        /// <param name="fieldName"></param>
        public OrclCreateTableField(TableAction action, string tableName, string fieldName)
        {   // transfer vars into local
            tblAction = action;
            table = tableName;
            field = fieldName;
            // check
            InitializeComponent();
            // gui
            if (tblAction == TableAction.FieldAdd)
            {
                this._btOk.Text = GuiHelper.GetResourceMsg("CTF_Create");
                this.Text = GuiHelper.GetResourceMsg("CTFTitleCreate", new string[] { table });
            }
            else
            {
                this._btOk.Text = GuiHelper.GetResourceMsg("CTF_Alter");
                this.Text = GuiHelper.GetResourceMsg("CTFTitleAlter", new string[] { field, table });
            }
        }
        private void CreateTableField_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // alter or create?
            if (tblAction == TableAction.FieldAlter)
            {   // get old field definition
                string qry = "SHOW FULL COLUMNS FROM " + table;
                if (dbh.ConnectDB(NyxMain.ConProfile, false))
                {
                    if (dbh.ExecuteReader(qry))
                    {   // datareader
                        Classes.TableDetails strTblDesc;
                        // read
                        while (dbh.Read())
                        {
                            if (dbh.GetValue(0).ToString() == field)
                            {   // fill tabledescribe
                                strTblDesc = new Classes.TableDetails();
                                strTblDesc.Name = field;
                                strTblDesc.Collation = dbh.GetValue(2).ToString();
                                strTblDesc.Null = dbh.GetValue(3).ToString().ToUpper(NyxMain.NyxCI);
                                strTblDesc.HasIndex = dbh.GetValue(4).ToString().ToUpper(NyxMain.NyxCI);
                                strTblDesc.DefaultValue = dbh.GetValue(5).ToString();
                                // fix for mysql 4.0 (comment didn't exist this long time ago...)
                                if (dbh.FieldCount == 8)
                                    strTblDesc.Comment = dbh.GetValue(8).ToString();
                                strTblDesc.Extra = dbh.GetValue(6).ToString().ToUpper(NyxMain.NyxCI);
                                // formating type
                                string tmp = dbh.GetValue(1).ToString();
                                int strPos = tmp.IndexOf("(");
                                int endPos = tmp.LastIndexOf(")");
                                if (strPos != -1)
                                    strTblDesc.Type = tmp.Substring(0, tmp.Length - (tmp.Length - strPos)).ToUpper(NyxMain.NyxCI);
                                else
                                    strTblDesc.Type = tmp.ToUpper(NyxMain.NyxCI);
                                // setting length
                                if (strPos != -1 && endPos != -1)
                                    strTblDesc.Lenght = tmp.Substring(strPos + 1, endPos - strPos - 1);
                                // cutting tmp for the attributes
                                if (tmp.Length > endPos + 2)
                                    strTblDesc.Attribute = tmp.Substring(endPos + 2).ToUpper(NyxMain.NyxCI);

                                // set gui
                                this._tbFldName.Text = strTblDesc.Name;
                                this._cbFldType.SelectedItem = strTblDesc.Type;
                                this._mtbFldLength.Text = strTblDesc.Lenght;
                                if (strTblDesc.Collation != "NULL")
                                    this._cbFldLengthType.Text = strTblDesc.Collation;
                                this._tbFldDefault.Text = strTblDesc.DefaultValue;
                                this._tbFldComment.Text = strTblDesc.Comment;
                                // null?
                                if (strTblDesc.Null == "YES")
                                    this._cbFldNull.SelectedIndex = 1;
                                else
                                    this._cbFldNull.SelectedIndex = 2;
                                break;
                            }
                        }
                        // enabling button
                        dbh.Close();
                    }
                    else
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CTFRetrieveFieldsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                                            MessageHandler.EnumMsgType.Warning);
                        // disconnect
                        dbh.DisconnectDB();
                        // close
                        this.Close();
                    }
                }
                else
                {   // error creating db connection
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                        dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    // close
                    this.Close();
                }
                // disconnect db again
                dbh.DisconnectDB();
            }
        }
        private void OrclCreateTableField_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }

        private void _btCreate_Click(object sender, EventArgs e)
        {   // init vars
            string qry = String.Empty;
            // build column definition
            string createDef = OrclHelper.BuildColumenDefinition(this._tbFldName.Text.ToString().Trim(),
                    this._cbFldType.Text.ToString(), this._mtbFldLength.Text.ToString().Trim());
            // building db connection
            if (dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                // check what todo
                if (tblAction == TableAction.FieldAdd)
                {
                    qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} ADD COLUMN {1}", table, createDef);
                    // exec query
                    if (dbh.ExecuteNonQuery(qry))
                    {   // fire refresh event
                        OnDBContentChanged();
                        // add another field?
                        StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("CTF_FieldCreated"),
                                                MessageBoxButtons.YesNo, MessageBoxIcon.Information, false);
                        if (strRD.DRes == NyxDialogResult.Yes)
                        {   // reset gui
                            this._tbFldName.Text = String.Empty;
                            this._mtbFldLength.Text = String.Empty;
                            this._cbFldLengthType.SelectedIndex = 0;
                            this._cbFldNull.SelectedIndex = 0;
                            this._cbFldType.SelectedIndex = 0;
                            this._tbFldDefault.Text = String.Empty;
                            this._tbFldComment.Text = String.Empty;
                        }
                        else
                            this.Close();
                    }
                    else
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CTF_CreateErr", new string[] { dbh.ErrExc.Message.ToString(), qry }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
                else if (tblAction == TableAction.FieldAlter)
                {   // execute change column
                    qry = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} CHANGE COLUMN {1} {2}", table, field, createDef);
                    if (dbh.ExecuteNonQuery(qry))
                    {   // close
                        OnDBContentChanged();
                        this.Close();
                    }
                    else
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("CTF_AlterErr", new string[] { dbh.ErrExc.Message.ToString(), qry }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
            }
            else
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString(), qry }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
            // close dbconnection again
            dbh.DisconnectDB();
        }

        private void _tbFldName_TextChanged(object sender, EventArgs e)
        {
            if (this._tbFldName.Text.ToString().Length > 0)
                this._btOk.Enabled = true;
            else
                this._btOk.Enabled = false;
        }
        private void _cbFieldType_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selitem = this._cbFldType.SelectedItem.ToString().ToUpper(NyxMain.NyxCI);
            switch (selitem)
            {
                case "VARCHAR2":    // VARCHAR2(size [BYTE | CHAR])
                case "CHAR":        // CHAR [(size [BYTE | CHAR])]
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._cbFldLengthType.Enabled = true;
                    this._tbFldDefault.Enabled = true;
                    break;
                case "NVARCHAR2":   // NVARCHAR2(size)
                case "RAW":         // RAW(size)
                case "UROWID":      // UROWID [(size)]
                case "NCHAR":       // NCHAR[(size)]
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._cbFldLengthType.Enabled = false;
                    this._tbFldDefault.Enabled = true;
                    break;
                case "NUMBER":  // NUMBER[(precision [, scale]])
                    this._mtbFldLength.Mask = "90000";
                    this._mtbFldLength.Enabled = true;
                    this._cbFldLengthType.Enabled = false;
                    this._tbFldDefault.Enabled = true;
                    break;
                case "LONG":            // LONG
                case "DATE":            // DATE
                case "BINARY_FLOAT":    // BINARY_FLOAT
                case "BINARY_DOUBLE":   // BINARY_DOUBLE
                case "LONG RAW":        // LONG RAW
                case "ROWID":           // ROWID
                case "CLOB":            // CLOB
                case "NCLOB":           // NCLOB
                case "BLOB":            // BLOB
                case "BFILE":           // BFILE
                    this._mtbFldLength.Enabled = false;
                    this._cbFldLengthType.Enabled = false;
                    this._tbFldDefault.Enabled = true;
                    break;
                case "TIMESTAMP":                       // TIMESTAMP
                case "TIMESTAMP WITH TIME ZONE":        // TIMESTAMP WITH TIME ZONE
                case "TIMESTAMP WITH LOCAL TIME ZONE":  // TIMESTAMP WITH LOCAL TIME ZONE
                    this._mtbFldLength.Mask = "0";
                    this._mtbFldLength.Enabled = true;
                    this._mtbFldLength.Text = "6";
                    this._cbFldLengthType.Enabled = false;
                    this._tbFldDefault.Enabled = true;
                    break;


            }
        }
        private void _btCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Fire OnDBContentChanged event
        /// </summary>
        protected void OnDBContentChanged()
        {   // fire event
            DBContentChangedEventArgs e = new DBContentChangedEventArgs();
            e.DBContent = DatabaseContent.TableFields;
            e.Param1 = table;

            if (DBContentChangedEvent != null)
                DBContentChangedEvent(this, e);
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}