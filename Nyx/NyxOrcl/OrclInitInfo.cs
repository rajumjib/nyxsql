/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Nyx.Classes;
using Oracle.DataAccess.Client;
using System.Windows.Forms;
using System.Collections;

namespace Nyx.NyxOrcl
{
    /// <summary>
    /// Retrieve init parameters of oracle database
    /// </summary>
    public partial class OrclInitInfo : Form
    {   // vars
        Hashtable htDefault = new Hashtable();
        Hashtable htNonDefault = new Hashtable();
        ListViewColumnSorter lvcsDefault = new ListViewColumnSorter();
        ListViewColumnSorter lvcsNonDefault = new ListViewColumnSorter();
        // dbhelper
        DBHelper dbh = new DBHelper(NyxSettings.DBType.Oracle);

        /// <summary>
        /// Init
        /// </summary>
        public OrclInitInfo()
        {
            InitializeComponent();
            // add listviewcolumnsorters
            this._lvDefault.ListViewItemSorter = lvcsDefault;
            this._lvNonDefault.ListViewItemSorter = lvcsNonDefault;
        }
        private void FrmInitInfo_Load(object sender, EventArgs e)
        {   // building database connection
            if (!dbh.ConnectDB(NyxMain.ConProfile, false))
            {
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
            /* getting default
             */
            if (dbh.ExecuteReader("SELECT name,value FROM v$parameter WHERE isdefault = 'TRUE' ORDER BY name ASC"))
            {   // read
                string name, value;
                while (dbh.Read())
                {
                    name = dbh.GetValue(0).ToString();
                    value = dbh.GetValue(1).ToString();

                    this._lvDefault.Items.Add(GuiHelper.BuildListViewItem(name, value));
                    htDefault.Add(name, value);
                }
            }
            else
            {
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("OII_DefaultErr", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
            }

            /* getting non-default
             */
            if (dbh.ExecuteReader("SELECT name,value FROM v$parameter WHERE isdefault = 'FALSE' ORDER BY name ASC"))
            {   // read
                string name, value;
                while (dbh.Read())
                {
                    name = dbh.GetValue(0).ToString();
                    value = dbh.GetValue(1).ToString();

                    this._lvNonDefault.Items.Add(GuiHelper.BuildListViewItem(name, value));
                    htNonDefault.Add(name, value);
                }
            }
            else
            {
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrNonDefInitParams", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
            }
            // disconnect database
            dbh.DisconnectDB();
        }

        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _tbFilter_TextChanged(object sender, EventArgs e)
        {   // define vars
            string filter = this._tbFilter.Text.ToUpper(NyxMain.NyxCI);
            System.Collections.Hashtable ht;
            ListView lv;
            ListViewItem lvi;
            // set listview/hashtable
            if (this._tc.SelectedTab == this._tpDefault)
            {
                lv = this._lvDefault;
                ht = htDefault;
            }
            else
            {
                lv = this._lvNonDefault;
                ht = htNonDefault;
            }
            // filter out
            lv.BeginUpdate();
            lv.Items.Clear();
            foreach (System.Collections.DictionaryEntry de in ht)
            {
                string htKey = de.Key.ToString().ToUpper(NyxMain.NyxCI);
                if (htKey.Contains(filter))
                {
                    lvi = GuiHelper.BuildListViewItem(de.Key.ToString(), de.Value.ToString());
                    lv.Items.Add(lvi);
                }
            }
            lv.EndUpdate();
        }
        private void _tc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._tbFilter.Text.Length > 0)
            {
                this._tbFilter.Text = String.Empty;
                ListView lv;
                System.Collections.Hashtable ht;

                if (this._tc.SelectedTab == this._tpDefault)
                {   // reset non default
                    lv = this._lvNonDefault;
                    ht = htNonDefault;
                }
                else 
                {   // reset default
                    lv = this._lvDefault;
                    ht = htDefault;
                }
                lv.BeginUpdate();
                lv.Items.Clear();
                foreach (System.Collections.DictionaryEntry de in ht)
                {
                    lv.Items.Add(GuiHelper.BuildListViewItem(de.Key.ToString(), de.Value.ToString()));
                }
                lv.Sort();
                lv.EndUpdate();
            }
        }

        private void _lvNonDefault_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvNonDefault, lvcsNonDefault, e.Column);
        }
        private void _lvDefault_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvDefault, lvcsDefault, e.Column);
        }

        private void OrclInitInfo_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }
    }
}