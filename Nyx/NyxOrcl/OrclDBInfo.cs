/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Nyx.AppDialogs;
using Oracle.DataAccess.Client;
using System.Windows.Forms;
using System.Collections;
using Nyx.Classes;

namespace Nyx.NyxOrcl
{
    public partial class OrclDBInfo : Form
    {   // dbhelper
        DBHelper dbh = new DBHelper(NyxSettings.DBType.Oracle);
        // listview
        private Hashtable htGeneral = new Hashtable();
        private Hashtable htInstance = new Hashtable();
        private Hashtable htSGA = new Hashtable();
        private Hashtable htOptions = new Hashtable();
        private ListViewColumnSorter lvcsGeneral = new ListViewColumnSorter();
        private ListViewColumnSorter lvcsInstance = new ListViewColumnSorter();
        private ListViewColumnSorter lvcsSGA = new ListViewColumnSorter();
        private ListViewColumnSorter lvcsOptions = new ListViewColumnSorter();
        private ListViewColumnSorter lvcsBanner = new ListViewColumnSorter();

        /// <summary> Init Function </summary>
        public OrclDBInfo()
        {   // init components
            InitializeComponent();
            // add listviewcolumnsorter
            this._lvGeneral.ListViewItemSorter = lvcsGeneral;
            this._lvInstance.ListViewItemSorter = lvcsInstance;
            this._lvSGA.ListViewItemSorter = lvcsSGA;
            this._lvOptions.ListViewItemSorter = lvcsOptions;
            this._lvBanner.ListViewItemSorter = lvcsBanner;
        }
        
        private void FrmDBInfo_Load(object sender, EventArgs e)
        {   // init
            string qryOpt = "SELECT * FROM v$option";
            string qryInf = "SELECT DBID, NAME, TO_CHAR(CREATED,'DD-Mon-YYYY HH24:MI:SS') AS CREATED," +
                "RESETLOGS_CHANGE# AS RLOGCHANGE,TO_CHAR(RESETLOGS_TIME,'DD-Mon-YYYY HH24:MI:SS') AS RLOGTIME," +
                "PRIOR_RESETLOGS_CHANGE# AS RESETLOGS,TO_CHAR(PRIOR_RESETLOGS_TIME,'DD-Mon-YYYY HH24:MI:SS') AS RESETLOGS_TIME," +
                "LOG_MODE,CHECKPOINT_CHANGE# AS CKPOINT_CHG,ARCHIVE_CHANGE# AS ARC_CHG,CONTROLFILE_TYPE,TO_CHAR(CONTROLFILE_CREATED,'DD-Mon-YYYY HH24:MI:SS') AS CONTROLFILE_CREATED," +
                "CONTROLFILE_SEQUENCE# AS CTL_SEQ,CONTROLFILE_CHANGE# AS CTL_CHG,TO_CHAR(CONTROLFILE_TIME,'DD-Mon-YYYY HH24:MI:SS') AS CONTROLFILE_TIME," +
                "OPEN_RESETLOGS,TO_CHAR(VERSION_TIME,'DD-Mon-YYYY HH24:MI:SS') AS VERSION_TIME,OPEN_MODE FROM V$DATABASE";
            string qryInst = "SELECT INSTANCE_NUMBER,INSTANCE_NAME,HOST_NAME,TO_CHAR(STARTUP_TIME,'DD-Mon-YYYY HH24:MI:SS') AS ST," +
                                "STATUS,ARCHIVER,LOGINS,SHUTDOWN_PENDING,DATABASE_STATUS,INSTANCE_ROLE FROM V$INSTANCE";
            string qrySGA = "SELECT NAME,VALUE FROM V$SGA ORDER BY NAME";
            string qryBanner = "SELECT BANNER FROM V$VERSION";

            // connect database
            if (dbh.ConnectDB(NyxMain.ConProfile, false))
            {   // general database info
                if (dbh.ExecuteReader(qryInf))
                {   // read
                    try
                    {
                        if (dbh.Read())
                        {
                            string varName, varValue;

                            this._lvGeneral.BeginUpdate();
                            for (int i = 0; i < dbh.FieldCount; i++)
                            {   // get name and value
                                varName = dbh.GetName(i).ToString();
                                varValue = dbh.GetValue(i).ToString();
                                // add to listview and hashtable
                                this._lvGeneral.Items.Add(GuiHelper.BuildListViewItem(varName, varValue));
                                this.htGeneral.Add(varName, varValue);
                            }
                            this._lvGeneral.EndUpdate();
                        }
                    }
                    catch (IndexOutOfRangeException exc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { exc.Message.ToString() }), exc,
                                                    MessageHandler.EnumMsgType.Error);
                        this._lvGeneral.Items.Clear();
                    }
                    finally { dbh.Close(); }
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                        dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    this._lvGeneral.Items.Clear();
                }

                // instance info
                if (dbh.ExecuteReader(qryInst))
                {   // read
                    try
                    {
                        if (dbh.Read())
                        {
                            string varName, varValue;

                            this._lvInstance.BeginUpdate();
                            for (int i = 0; i < dbh.FieldCount; i++)
                            {   // get name and value
                                varName = dbh.GetName(i).ToString();
                                varValue = dbh.GetValue(i).ToString();
                                // add to listview and hashtable
                                this._lvInstance.Items.Add(GuiHelper.BuildListViewItem(varName, varValue));
                                this.htInstance.Add(varName, varValue);
                            }
                            this._lvInstance.EndUpdate();
                        }
                    }
                    catch (IndexOutOfRangeException exc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { exc.Message.ToString() }), exc,
                                                    MessageHandler.EnumMsgType.Error);
                        this._lvInstance.Items.Clear();
                    }
                    finally { dbh.Close(); }
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { dbh.ErrExc.Message.ToString() }), 
                                                dbh.ErrExc, MessageHandler.EnumMsgType.Error);
                    this._lvInstance.Items.Clear();
                }

                // sga info
                if (dbh.ExecuteReader(qrySGA))
                {   // read
                    try
                    {
                        string varName, varValue;
                        double size;

                        this._lvSGA.BeginUpdate();
                        while (dbh.Read())
                        {
                            varName = dbh.GetValue(0).ToString();
                            if (Double.TryParse(dbh.GetValue(1).ToString(), out size))
                                varValue = (size / 1024 / 1024).ToString("N", NyxMain.NyxCI) + " MB";
                            else
                                varValue = "Err";

                            this._lvSGA.Items.Add(GuiHelper.BuildListViewItem(varName , varValue));
                            this.htSGA.Add(varName, varValue);
                        }
                        this._lvSGA.EndUpdate();
                    }
                    catch (IndexOutOfRangeException exc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { exc.Message.ToString() }), exc,
                                                    MessageHandler.EnumMsgType.Error);
                        this._lvSGA.Items.Clear();
                    }
                    finally { dbh.Close(); }
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                        dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    this._lvSGA.Items.Clear();
                }

                // banner info
                if (dbh.ExecuteReader(qryBanner))
                {   // read
                    try
                    {
                        this._lvBanner.BeginUpdate();
                        while (dbh.Read())
                            this._lvBanner.Items.Add(dbh.GetValue(0).ToString());
                        this._lvBanner.EndUpdate();
                    }
                    catch (IndexOutOfRangeException exc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { exc.Message.ToString() }), exc,
                                                    MessageHandler.EnumMsgType.Error);
                        this._lvBanner.Items.Clear();
                    }
                    finally { dbh.Close(); }
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                                                dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    this._lvBanner.Items.Clear();
                }

                // options
                if (dbh.ExecuteReader(qryOpt))
                {   // read
                    try
                    {
                        if (dbh.Read())
                        {
                            string varName, varValue;

                            this._lvOptions.BeginUpdate();
                            while (dbh.Read())
                            {
                                varName = dbh.GetValue(0).ToString();
                                varValue = dbh.GetValue(1).ToString();

                                this._lvOptions.Items.Add(GuiHelper.BuildListViewItem(varName, varValue));
                                this.htOptions.Add(varName, varValue);
                            }
                            this._lvOptions.EndUpdate();
                        }
                    }
                    catch (IndexOutOfRangeException exc)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { exc.Message.ToString() }), exc,
                                                    MessageHandler.EnumMsgType.Error);
                        this._lvOptions.Items.Clear();
                    }
                    finally { dbh.Close(); }
                }
                else
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBI_InformationsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                                                dbh.ErrExc, MessageHandler.EnumMsgType.Error);
                    this._lvOptions.Items.Clear();
                }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
            dbh.DisconnectDB();
        }
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        // columnsorter
        private void _lvGeneral_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvGeneral, lvcsGeneral, e.Column);
        }
        private void _lvInstance_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvInstance, lvcsInstance, e.Column);
        }
        private void _lvSGA_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvSGA, lvcsSGA, e.Column);
        }
        private void _lvOptions_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvOptions, lvcsOptions, e.Column);
        }
        // filterevents
        private void _tbFilter_TextChanged(object sender, EventArgs e)
        {   // define vars
            string filter = this._tbFilter.Text.ToUpper(NyxMain.NyxCI);
            Hashtable ht;
            ListView lv;
            // set listview/hashtable
            if (this._tc.SelectedTab == this._tpGeneral)
            {
                lv = this._lvGeneral;
                ht = htGeneral;
            }
            else if (this._tc.SelectedTab == this._tpInstance)
            {
                lv = this._lvInstance;
                ht = htInstance;
            }
            else if (this._tc.SelectedTab == this._tpOptions)
            {
                lv = this._lvOptions;
                ht = htOptions;
            }
            else if (this._tc.SelectedTab == this._tpSGA)
            {
                lv = this._lvSGA;
                ht = htSGA;
            }
            else
                return;
            // filter out
            lv.BeginUpdate();
            lv.Items.Clear();
            foreach (DictionaryEntry de in ht)
            {
                string htKey = de.Key.ToString().ToUpper(NyxMain.NyxCI);
                if (htKey.Contains(filter))
                    lv.Items.Add(GuiHelper.BuildListViewItem(de.Key.ToString(), de.Value.ToString()));
            }
            lv.Sort();
            lv.EndUpdate();
        }
        private void _tc_SelectedIndexChanged(object sender, EventArgs e)
        {   // check if selected tab is banner (no filter possible)
            if (this._tc.SelectedTab == this._tpBanner)
            {
                this._tbFilter.Enabled = false;
                return;
            }
            // enable filter
            this._tbFilter.Enabled = true;
            if (this._tbFilter.Text.Length > 0)
            {
                this._tbFilter.Text = String.Empty;
                ListView lv;
                Hashtable ht;
                // set listview/hashtable
                if (this._tc.SelectedTab == this._tpGeneral)
                {
                    lv = this._lvGeneral;
                    ht = htGeneral;
                }
                else if (this._tc.SelectedTab == this._tpInstance)
                {
                    lv = this._lvInstance;
                    ht = htInstance;
                }
                else if (this._tc.SelectedTab == this._tpOptions)
                {
                    lv = this._lvOptions;
                    ht = htOptions;
                }
                else if (this._tc.SelectedTab == this._tpSGA)
                {
                    lv = this._lvSGA;
                    ht = htSGA;
                }
                else
                    return;

                lv.BeginUpdate();
                lv.Items.Clear();
                foreach (DictionaryEntry de in ht)
                    lv.Items.Add(GuiHelper.BuildListViewItem(de.Key.ToString(), de.Value.ToString()));
                lv.Sort();
                lv.EndUpdate();
            }
        }

        private void OrclDBInfo_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }
    }
}