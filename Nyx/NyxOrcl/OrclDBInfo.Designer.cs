namespace Nyx.NyxOrcl
{
    /// <summary>
    /// General Oracle Database Informations
    /// </summary>
    partial class OrclDBInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrclDBInfo));
            this._tslbl_Filter = new System.Windows.Forms.ToolStripLabel();
            this._tstb_Filter = new System.Windows.Forms.ToolStripTextBox();
            this._tstb_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tstb_Close = new System.Windows.Forms.ToolStripButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._btClose = new System.Windows.Forms.Button();
            this._tc = new System.Windows.Forms.TabControl();
            this._tpGeneral = new System.Windows.Forms.TabPage();
            this._lvGeneral = new System.Windows.Forms.ListView();
            this.lvcGnrl_Name = new System.Windows.Forms.ColumnHeader();
            this.lvcGnrl_Value = new System.Windows.Forms.ColumnHeader();
            this._tpInstance = new System.Windows.Forms.TabPage();
            this._lvInstance = new System.Windows.Forms.ListView();
            this.lvcInst_Name = new System.Windows.Forms.ColumnHeader();
            this.lvcInst_Value = new System.Windows.Forms.ColumnHeader();
            this._tpSGA = new System.Windows.Forms.TabPage();
            this._lvSGA = new System.Windows.Forms.ListView();
            this.lvcSGA_Name = new System.Windows.Forms.ColumnHeader();
            this.lvcSGA_MB = new System.Windows.Forms.ColumnHeader();
            this._tpBanner = new System.Windows.Forms.TabPage();
            this._lvBanner = new System.Windows.Forms.ListView();
            this.lvcBnr_Name = new System.Windows.Forms.ColumnHeader();
            this._tpOptions = new System.Windows.Forms.TabPage();
            this._lvOptions = new System.Windows.Forms.ListView();
            this.lvcOpt_Option = new System.Windows.Forms.ColumnHeader();
            this.lvcOpt_Status = new System.Windows.Forms.ColumnHeader();
            this._lblFilter = new System.Windows.Forms.Label();
            this._tbFilter = new System.Windows.Forms.TextBox();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.tableLayoutPanel1.SuspendLayout();
            this._tc.SuspendLayout();
            this._tpGeneral.SuspendLayout();
            this._tpInstance.SuspendLayout();
            this._tpSGA.SuspendLayout();
            this._tpBanner.SuspendLayout();
            this._tpOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tslbl_Filter
            // 
            this._tslbl_Filter.Name = "_tslbl_Filter";
            resources.ApplyResources(this._tslbl_Filter, "_tslbl_Filter");
            // 
            // _tstb_Filter
            // 
            this._tstb_Filter.Name = "_tstb_Filter";
            resources.ApplyResources(this._tstb_Filter, "_tstb_Filter");
            // 
            // _tstb_Sep0
            // 
            this._tstb_Sep0.Name = "_tstb_Sep0";
            resources.ApplyResources(this._tstb_Sep0, "_tstb_Sep0");
            // 
            // _tstb_Close
            // 
            this._tstb_Close.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tstb_Close.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._tstb_Close, "_tstb_Close");
            this._tstb_Close.Name = "_tstb_Close";
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this._btClose, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this._tc, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._lblFilter, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._tbFilter, 1, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // _tc
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._tc, 3);
            this._tc.Controls.Add(this._tpGeneral);
            this._tc.Controls.Add(this._tpInstance);
            this._tc.Controls.Add(this._tpSGA);
            this._tc.Controls.Add(this._tpBanner);
            this._tc.Controls.Add(this._tpOptions);
            resources.ApplyResources(this._tc, "_tc");
            this._tc.Name = "_tc";
            this._tc.SelectedIndex = 0;
            this._tc.SelectedIndexChanged += new System.EventHandler(this._tc_SelectedIndexChanged);
            // 
            // _tpGeneral
            // 
            this._tpGeneral.BackColor = System.Drawing.SystemColors.Control;
            this._tpGeneral.Controls.Add(this._lvGeneral);
            resources.ApplyResources(this._tpGeneral, "_tpGeneral");
            this._tpGeneral.Name = "_tpGeneral";
            this._tpGeneral.UseVisualStyleBackColor = true;
            // 
            // _lvGeneral
            // 
            this._lvGeneral.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvcGnrl_Name,
            this.lvcGnrl_Value});
            resources.ApplyResources(this._lvGeneral, "_lvGeneral");
            this._lvGeneral.FullRowSelect = true;
            this._lvGeneral.GridLines = true;
            this._lvGeneral.Name = "_lvGeneral";
            this._lvGeneral.UseCompatibleStateImageBehavior = false;
            this._lvGeneral.View = System.Windows.Forms.View.Details;
            this._lvGeneral.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvGeneral_ColumnClick);
            // 
            // lvcGnrl_Name
            // 
            resources.ApplyResources(this.lvcGnrl_Name, "lvcGnrl_Name");
            // 
            // lvcGnrl_Value
            // 
            resources.ApplyResources(this.lvcGnrl_Value, "lvcGnrl_Value");
            // 
            // _tpInstance
            // 
            this._tpInstance.Controls.Add(this._lvInstance);
            resources.ApplyResources(this._tpInstance, "_tpInstance");
            this._tpInstance.Name = "_tpInstance";
            this._tpInstance.UseVisualStyleBackColor = true;
            // 
            // _lvInstance
            // 
            this._lvInstance.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvcInst_Name,
            this.lvcInst_Value});
            resources.ApplyResources(this._lvInstance, "_lvInstance");
            this._lvInstance.FullRowSelect = true;
            this._lvInstance.GridLines = true;
            this._lvInstance.Name = "_lvInstance";
            this._lvInstance.UseCompatibleStateImageBehavior = false;
            this._lvInstance.View = System.Windows.Forms.View.Details;
            this._lvInstance.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvInstance_ColumnClick);
            // 
            // lvcInst_Name
            // 
            resources.ApplyResources(this.lvcInst_Name, "lvcInst_Name");
            // 
            // lvcInst_Value
            // 
            resources.ApplyResources(this.lvcInst_Value, "lvcInst_Value");
            // 
            // _tpSGA
            // 
            this._tpSGA.Controls.Add(this._lvSGA);
            resources.ApplyResources(this._tpSGA, "_tpSGA");
            this._tpSGA.Name = "_tpSGA";
            this._tpSGA.UseVisualStyleBackColor = true;
            // 
            // _lvSGA
            // 
            this._lvSGA.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvcSGA_Name,
            this.lvcSGA_MB});
            resources.ApplyResources(this._lvSGA, "_lvSGA");
            this._lvSGA.FullRowSelect = true;
            this._lvSGA.GridLines = true;
            this._lvSGA.Name = "_lvSGA";
            this._lvSGA.UseCompatibleStateImageBehavior = false;
            this._lvSGA.View = System.Windows.Forms.View.Details;
            this._lvSGA.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvSGA_ColumnClick);
            // 
            // lvcSGA_Name
            // 
            resources.ApplyResources(this.lvcSGA_Name, "lvcSGA_Name");
            // 
            // lvcSGA_MB
            // 
            resources.ApplyResources(this.lvcSGA_MB, "lvcSGA_MB");
            // 
            // _tpBanner
            // 
            this._tpBanner.Controls.Add(this._lvBanner);
            resources.ApplyResources(this._tpBanner, "_tpBanner");
            this._tpBanner.Name = "_tpBanner";
            this._tpBanner.UseVisualStyleBackColor = true;
            // 
            // _lvBanner
            // 
            this._lvBanner.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvcBnr_Name});
            resources.ApplyResources(this._lvBanner, "_lvBanner");
            this._lvBanner.FullRowSelect = true;
            this._lvBanner.GridLines = true;
            this._lvBanner.Name = "_lvBanner";
            this._lvBanner.UseCompatibleStateImageBehavior = false;
            this._lvBanner.View = System.Windows.Forms.View.Details;
            // 
            // lvcBnr_Name
            // 
            resources.ApplyResources(this.lvcBnr_Name, "lvcBnr_Name");
            // 
            // _tpOptions
            // 
            this._tpOptions.Controls.Add(this._lvOptions);
            resources.ApplyResources(this._tpOptions, "_tpOptions");
            this._tpOptions.Name = "_tpOptions";
            this._tpOptions.UseVisualStyleBackColor = true;
            // 
            // _lvOptions
            // 
            this._lvOptions.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvcOpt_Option,
            this.lvcOpt_Status});
            resources.ApplyResources(this._lvOptions, "_lvOptions");
            this._lvOptions.FullRowSelect = true;
            this._lvOptions.GridLines = true;
            this._lvOptions.Name = "_lvOptions";
            this._lvOptions.UseCompatibleStateImageBehavior = false;
            this._lvOptions.View = System.Windows.Forms.View.Details;
            this._lvOptions.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvOptions_ColumnClick);
            // 
            // lvcOpt_Option
            // 
            resources.ApplyResources(this.lvcOpt_Option, "lvcOpt_Option");
            // 
            // lvcOpt_Status
            // 
            resources.ApplyResources(this.lvcOpt_Status, "lvcOpt_Status");
            // 
            // _lblFilter
            // 
            resources.ApplyResources(this._lblFilter, "_lblFilter");
            this._lblFilter.Name = "_lblFilter";
            // 
            // _tbFilter
            // 
            resources.ApplyResources(this._tbFilter, "_tbFilter");
            this._tbFilter.Name = "_tbFilter";
            this._tbFilter.TextChanged += new System.EventHandler(this._tbFilter_TextChanged);
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // columnHeader2
            // 
            resources.ApplyResources(this.columnHeader2, "columnHeader2");
            // 
            // OrclDBInfo
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btClose;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "OrclDBInfo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrclDBInfo_FormClosing);
            this.Load += new System.EventHandler(this.FrmDBInfo_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this._tc.ResumeLayout(false);
            this._tpGeneral.ResumeLayout(false);
            this._tpInstance.ResumeLayout(false);
            this._tpSGA.ResumeLayout(false);
            this._tpBanner.ResumeLayout(false);
            this._tpOptions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ToolStripLabel _tslbl_Filter;
        private System.Windows.Forms.ToolStripTextBox _tstb_Filter;
        private System.Windows.Forms.ToolStripSeparator _tstb_Sep0;
        private System.Windows.Forms.ToolStripButton _tstb_Close;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button _btClose;
        private System.Windows.Forms.Label _lblFilter;
        private System.Windows.Forms.TextBox _tbFilter;
        private System.Windows.Forms.TabControl _tc;
        private System.Windows.Forms.TabPage _tpGeneral;
        private System.Windows.Forms.ListView _lvGeneral;
        private System.Windows.Forms.ColumnHeader lvcGnrl_Name;
        private System.Windows.Forms.ColumnHeader lvcGnrl_Value;
        private System.Windows.Forms.TabPage _tpInstance;
        private System.Windows.Forms.ListView _lvInstance;
        private System.Windows.Forms.ColumnHeader lvcInst_Name;
        private System.Windows.Forms.ColumnHeader lvcInst_Value;
        private System.Windows.Forms.TabPage _tpSGA;
        private System.Windows.Forms.ListView _lvSGA;
        private System.Windows.Forms.ColumnHeader lvcSGA_Name;
        private System.Windows.Forms.ColumnHeader lvcSGA_MB;
        private System.Windows.Forms.TabPage _tpBanner;
        private System.Windows.Forms.TabPage _tpOptions;
        private System.Windows.Forms.ColumnHeader lvcOpt_Option;
        private System.Windows.Forms.ColumnHeader lvcOpt_Status;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ListView _lvOptions;
        private System.Windows.Forms.ListView _lvBanner;
        private System.Windows.Forms.ColumnHeader lvcBnr_Name;
    }
}