namespace Nyx.NyxOrcl
{
    partial class OrclCreateTableField
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrclCreateTableField));
            this._gbField = new System.Windows.Forms.GroupBox();
            this._tbFldComment = new System.Windows.Forms.TextBox();
            this._lblFldComment = new System.Windows.Forms.Label();
            this._lblFldDefault = new System.Windows.Forms.Label();
            this._tbFldDefault = new System.Windows.Forms.TextBox();
            this._lblFldNull = new System.Windows.Forms.Label();
            this._cbFldNull = new System.Windows.Forms.ComboBox();
            this._lblFldLength = new System.Windows.Forms.Label();
            this._mtbFldLength = new System.Windows.Forms.MaskedTextBox();
            this._lblFldName = new System.Windows.Forms.Label();
            this._lblFldType = new System.Windows.Forms.Label();
            this._tbFldName = new System.Windows.Forms.TextBox();
            this._cbFldType = new System.Windows.Forms.ComboBox();
            this._cbFldLengthType = new System.Windows.Forms.ComboBox();
            this._btCancel = new System.Windows.Forms.Button();
            this._btOk = new System.Windows.Forms.Button();
            this._gbField.SuspendLayout();
            this.SuspendLayout();
            // 
            // _gbField
            // 
            this._gbField.Controls.Add(this._tbFldComment);
            this._gbField.Controls.Add(this._lblFldComment);
            this._gbField.Controls.Add(this._lblFldDefault);
            this._gbField.Controls.Add(this._tbFldDefault);
            this._gbField.Controls.Add(this._lblFldNull);
            this._gbField.Controls.Add(this._cbFldNull);
            this._gbField.Controls.Add(this._lblFldLength);
            this._gbField.Controls.Add(this._mtbFldLength);
            this._gbField.Controls.Add(this._lblFldName);
            this._gbField.Controls.Add(this._lblFldType);
            this._gbField.Controls.Add(this._tbFldName);
            this._gbField.Controls.Add(this._cbFldType);
            this._gbField.Controls.Add(this._cbFldLengthType);
            resources.ApplyResources(this._gbField, "_gbField");
            this._gbField.Name = "_gbField";
            this._gbField.TabStop = false;
            // 
            // _tbFldComment
            // 
            resources.ApplyResources(this._tbFldComment, "_tbFldComment");
            this._tbFldComment.Name = "_tbFldComment";
            // 
            // _lblFldComment
            // 
            resources.ApplyResources(this._lblFldComment, "_lblFldComment");
            this._lblFldComment.Name = "_lblFldComment";
            // 
            // _lblFldDefault
            // 
            resources.ApplyResources(this._lblFldDefault, "_lblFldDefault");
            this._lblFldDefault.Name = "_lblFldDefault";
            // 
            // _tbFldDefault
            // 
            resources.ApplyResources(this._tbFldDefault, "_tbFldDefault");
            this._tbFldDefault.Name = "_tbFldDefault";
            // 
            // _lblFldNull
            // 
            resources.ApplyResources(this._lblFldNull, "_lblFldNull");
            this._lblFldNull.Name = "_lblFldNull";
            // 
            // _cbFldNull
            // 
            this._cbFldNull.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldNull.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbFldNull.FormattingEnabled = true;
            this._cbFldNull.Items.AddRange(new object[] {
            resources.GetString("_cbFldNull.Items"),
            resources.GetString("_cbFldNull.Items1"),
            resources.GetString("_cbFldNull.Items2")});
            resources.ApplyResources(this._cbFldNull, "_cbFldNull");
            this._cbFldNull.Name = "_cbFldNull";
            // 
            // _lblFldLength
            // 
            resources.ApplyResources(this._lblFldLength, "_lblFldLength");
            this._lblFldLength.Name = "_lblFldLength";
            // 
            // _mtbFldLength
            // 
            this._mtbFldLength.HidePromptOnLeave = true;
            resources.ApplyResources(this._mtbFldLength, "_mtbFldLength");
            this._mtbFldLength.Name = "_mtbFldLength";
            // 
            // _lblFldName
            // 
            resources.ApplyResources(this._lblFldName, "_lblFldName");
            this._lblFldName.Name = "_lblFldName";
            // 
            // _lblFldType
            // 
            resources.ApplyResources(this._lblFldType, "_lblFldType");
            this._lblFldType.Name = "_lblFldType";
            // 
            // _tbFldName
            // 
            resources.ApplyResources(this._tbFldName, "_tbFldName");
            this._tbFldName.Name = "_tbFldName";
            this._tbFldName.TextChanged += new System.EventHandler(this._tbFldName_TextChanged);
            // 
            // _cbFldType
            // 
            this._cbFldType.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbFldType.FormattingEnabled = true;
            this._cbFldType.Items.AddRange(new object[] {
            resources.GetString("_cbFldType.Items"),
            resources.GetString("_cbFldType.Items1"),
            resources.GetString("_cbFldType.Items2"),
            resources.GetString("_cbFldType.Items3"),
            resources.GetString("_cbFldType.Items4"),
            resources.GetString("_cbFldType.Items5"),
            resources.GetString("_cbFldType.Items6"),
            resources.GetString("_cbFldType.Items7"),
            resources.GetString("_cbFldType.Items8"),
            resources.GetString("_cbFldType.Items9"),
            resources.GetString("_cbFldType.Items10"),
            resources.GetString("_cbFldType.Items11"),
            resources.GetString("_cbFldType.Items12"),
            resources.GetString("_cbFldType.Items13"),
            resources.GetString("_cbFldType.Items14"),
            resources.GetString("_cbFldType.Items15"),
            resources.GetString("_cbFldType.Items16"),
            resources.GetString("_cbFldType.Items17"),
            resources.GetString("_cbFldType.Items18"),
            resources.GetString("_cbFldType.Items19"),
            resources.GetString("_cbFldType.Items20"),
            resources.GetString("_cbFldType.Items21"),
            resources.GetString("_cbFldType.Items22"),
            resources.GetString("_cbFldType.Items23"),
            resources.GetString("_cbFldType.Items24"),
            resources.GetString("_cbFldType.Items25"),
            resources.GetString("_cbFldType.Items26"),
            resources.GetString("_cbFldType.Items27"),
            resources.GetString("_cbFldType.Items28"),
            resources.GetString("_cbFldType.Items29"),
            resources.GetString("_cbFldType.Items30"),
            resources.GetString("_cbFldType.Items31"),
            resources.GetString("_cbFldType.Items32"),
            resources.GetString("_cbFldType.Items33"),
            resources.GetString("_cbFldType.Items34"),
            resources.GetString("_cbFldType.Items35"),
            resources.GetString("_cbFldType.Items36"),
            resources.GetString("_cbFldType.Items37"),
            resources.GetString("_cbFldType.Items38"),
            resources.GetString("_cbFldType.Items39")});
            resources.ApplyResources(this._cbFldType, "_cbFldType");
            this._cbFldType.Name = "_cbFldType";
            this._cbFldType.SelectedIndexChanged += new System.EventHandler(this._cbFieldType_SelectedIndexChanged);
            // 
            // _cbFldLengthType
            // 
            this._cbFldLengthType.Cursor = System.Windows.Forms.Cursors.Hand;
            this._cbFldLengthType.FormattingEnabled = true;
            this._cbFldLengthType.Items.AddRange(new object[] {
            resources.GetString("_cbFldLengthType.Items"),
            resources.GetString("_cbFldLengthType.Items1"),
            resources.GetString("_cbFldLengthType.Items2")});
            resources.ApplyResources(this._cbFldLengthType, "_cbFldLengthType");
            this._cbFldLengthType.Name = "_cbFldLengthType";
            // 
            // _btCancel
            // 
            this._btCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            this._btCancel.Click += new System.EventHandler(this._btCancel_Click);
            // 
            // _btOk
            // 
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            this._btOk.Click += new System.EventHandler(this._btCreate_Click);
            // 
            // OrclCreateTableField
            // 
            this.AcceptButton = this._btOk;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btCancel;
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Controls.Add(this._gbField);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "OrclCreateTableField";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrclCreateTableField_FormClosing);
            this.Load += new System.EventHandler(this.CreateTableField_Load);
            this._gbField.ResumeLayout(false);
            this._gbField.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox _gbField;
        private System.Windows.Forms.TextBox _tbFldComment;
        private System.Windows.Forms.Label _lblFldComment;
        private System.Windows.Forms.Label _lblFldDefault;
        private System.Windows.Forms.TextBox _tbFldDefault;
        private System.Windows.Forms.Label _lblFldNull;
        private System.Windows.Forms.ComboBox _cbFldNull;
        private System.Windows.Forms.Label _lblFldLength;
        private System.Windows.Forms.MaskedTextBox _mtbFldLength;
        private System.Windows.Forms.Label _lblFldName;
        private System.Windows.Forms.Label _lblFldType;
        private System.Windows.Forms.TextBox _tbFldName;
        private System.Windows.Forms.ComboBox _cbFldType;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.ComboBox _cbFldLengthType;
    }
}