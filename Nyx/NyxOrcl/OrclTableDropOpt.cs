/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace Nyx.NyxOrcl
{
    public partial class OrclDropOpt : Form
    {
        /// <summary>
        /// Init
        /// </summary>
        public OrclDropOpt()
        {
            InitializeComponent();
        }
        private void _tb_adddo_Enter(object sender, EventArgs e)
        {
            this._tb_adddo.Text = String.Empty;
        }
        /// <summary>
        /// public static DropOptions GUI
        /// </summary>
        /// <returns>string of dropoptions</returns>
        public static string GetDropOpt
        {
            get
            {
                /* retunrs:
                 * null - at cancel
                 * "" - at ok with no options
                 * options to built in into the sql query
                 */
                string res = String.Empty;
                OrclDropOpt DO = new OrclDropOpt();

                DialogResult dRes = DO.ShowDialog();
                if (dRes == DialogResult.OK)
                {
                    if (DO._cb_cc.Checked)
                        res = "cascade constraints";
                    if (DO._cb_purge.Checked)
                    {
                        if (res.Length == 0) 
                            res = "purge";
                        else 
                            res += " purge";
                    }
                    if (DO._tb_adddo.Text.Length > 0 && DO._tb_adddo.Text != "additional drop options...")
                    {
                        if (res.Length == 0) 
                            res = DO._tb_adddo.Text;
                        else 
                            res += DO._tb_adddo.Text;
                    }
                }
                // returning
                return res;
            }
        }
    }
}