/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Nyx.Classes;
using Oracle.DataAccess.Client;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Nyx.NyxOrcl
{
    public partial class OrclTablespaces : Form
    {   // threading vars
        private System.Threading.Thread doWork;
        private delegate void ThrdEndPDelegate(StructThreadCom param);
        // misc vars
        private DBHelper dbh = new DBHelper(NyxMain.ConProfile.ProfileDBType);
        private enum EnumAction { tablespaces, datafiles }
        
        // querys
        private static string qryTSInfo
        {
            get
            {
                return "SELECT d.tablespace_name \"name\", " +
                        "d.block_size \"blocksize\", "+
                        "d.contents \"type\", " +
                        "d.extent_management \"extmng\", " +
                        "d.status \"status\", " +
                        "DECODE(d.force_logging,'YES','Yes','No') \"flog\", "+
                        "TO_CHAR(NVL((a.bytes - NVL(f.bytes, 0)) / a.bytes * 100, 0), '990.00') \"perused\", " +
                        "NVL(a.bytes / 1024 / 1024, 0) \"size\", " +
                        "NVL((a.bytes - f.bytes) / 1024 / 1024, 0) \"used\", " +
                        "NVL(((a.bytes - (a.bytes - NVL(f.bytes,0)))) / 1024 / 1024, 0) \"free\", " +
                        "DECODE(d.logging,'LOGGING','Yes','No') \"logging\", " +
                        "DECODE(d.contents,'TEMPORARY','Yes','No') \"temp\" " +
                       "FROM sys.dba_tablespaces d, " +
                          "(select tablespace_name, sum(bytes) bytes FROM dba_data_files GROUP BY tablespace_name) a, " +
                          "(select tablespace_name, sum(bytes) bytes FROM dba_free_space GROUP BY tablespace_name) f " +
                         "WHERE d.tablespace_name = a.tablespace_name(+) "+
                          "AND d.tablespace_name = f.tablespace_name(+) " +
                          "AND NOT (d.extent_management LIKE 'LOCAL' AND d.contents LIKE 'TEMPORARY') " +
                        "UNION ALL " +
                        "SELECT d.tablespace_name \"name\", " +
                         "d.block_size \"blocksize\", " +
                         "d.contents \"type\", " +
                         "d.extent_management \"extmng\", " +
                         "d.status \"status\", " +
                         "DECODE(d.force_logging,'YES','Yes','No') \"flog\", " +
                         "TO_CHAR(NVL(t.bytes / a.bytes * 100, 0), '990.00') \"perused\", " +
                         "NVL(a.bytes / 1024 / 1024, 0) \"size\", " +
                         "NVL(t.bytes / 1024 / 1024, 0) \"used\", " +
                         "NVL((a.bytes - NVL(t.bytes,0)) / 1024 / 1024, 0) \"free\", " +
                         "DECODE(d.logging,'LOGGING','Yes','No') \"logging\", " +
                         "DECODE(d.contents,'TEMPORARY','Yes','No') \"temp\" " +
                        "FROM sys.dba_tablespaces d, " +
                         "(select tablespace_name, sum(bytes) bytes FROM dba_temp_files GROUP BY tablespace_name) a, " +
                         "(select tablespace_name, sum(bytes_cached) bytes FROM v$temp_extent_pool GROUP BY tablespace_name) t " +
                         "WHERE d.tablespace_name = a.tablespace_name(+) "+
                          "AND d.tablespace_name = t.tablespace_name(+) " +
                          "AND d.extent_management LIKE 'LOCAL' AND d.contents LIKE 'TEMPORARY'";
            }
        }
        private static string qryDFInfo
        {
            get
            {
                return "SELECT v.status \"status\", " +
                        "d.file_name \"fname\", " +
                        "d.file_id \"fid\", " +
                        "d.autoextensible \"autoext\", "+
                        "d.tablespace_name \"tsname\", "+
                        "d.increment_by \"incby\", " +
                        "TO_CHAR(NVL(d.bytes / 1024 / 1024, 0), '99999990.000') \"size\", " +
	                    "TO_CHAR(NVL((d.bytes - NVL(s.bytes, 0)) / 1024 / 1024, 0),'99999999.999') \"usize\", " +
	                    "TO_CHAR(NVL((d.bytes - NVL(s.bytes, 0)) / d.bytes * 100, 0), '990.00') \"usedper\" " +
                            "FROM sys.dba_data_files d, v$datafile v, " +
                             "(SELECT file_id, SUM(bytes) bytes  FROM sys.dba_free_space GROUP BY file_id) s " +
                              "WHERE (s.file_id (+)= d.file_id) " +
                            "AND (d.file_name = v.name) " +
                        "UNION ALL " +
                        "SELECT v.status \"status\", " +
                        "d.file_name \"fname\", " +
                        "d.file_id \"fid\", " +
                        "d.autoextensible \"autoext\", " +
                        "d.tablespace_name \"tsname\", " +
                        "d.increment_by \"incby\", " +
	                    "TO_CHAR(NVL(d.bytes / 1024 / 1024, 0), '99999990.000') \"size\", " +
	                    "TO_CHAR(NVL(t.bytes_cached / 1024 / 1024, 0),'99999999.999') \"usize\", " +
                        "TO_CHAR(NVL(t.bytes_cached / d.bytes * 100, 0), '990.00') \"usedper\" " +
                            "FROM sys.dba_temp_files d, v$temp_extent_pool t, v$tempfile v " +
                            "WHERE (t.file_id (+)= d.file_id) " +
                            "AND (d.file_id = v.file#) ";
            }
        }

        private struct StructThreadCom
        {
            public Queue<StructDFInfo> qDFInfo;
            public Queue<StructTSInfo> qTSInfo;

            public DBException thrExc;
        }
        

        /// <summary> Tablespace info struct </summary>
        private struct StructTSInfo
        {
            public string Name;
            public string Type;
            public string Status;
            public string Logging;
            public string Flog;
            public double UsedPer;
            public double Size;
            public double UsedSpace;
            public double FreeSpace;
            public string ExternalManaged;
            public string Temporary;
        }
        /// <summary> Datafile info struct </summary>
        private struct StructDFInfo
        {
            /// <remarks> status </remarks>
            public string Status;
            /// <remarks> name </remarks>
            public string Name;
            /// <remarks> fld </remarks>
            public string Fld;
            /// <remarks> AutoExtension </remarks>
            public string AutoExt;
            /// <remarks> Tablespace </remarks>
            public string Tablespace;
            /// <remarks> Increment by </remarks>
            public double IncBy;
            /// <remarks> Size </remarks>
            public double Size;
            /// <remarks> Used Size </remarks>
            public double UsedSize;
            /// <remarks> Used Percentage </remarks>
            public double UsedPer;
        }
        
        /// <summary> 
        /// Initial function
        /// </summary>
        public OrclTablespaces()
        {
            InitializeComponent();
            // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }
        /// <summary>
        /// OnLoad Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Tablespaces_Load(object sender, System.EventArgs e)
        {   // fading
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // gui
            this._btRefresh.Select();
            // getting ts informations
            GetTSInfo();
        }

        private void ThreadStart(object oThrParam)
        {
            try
            {   // init return structs
                EnumAction action = (EnumAction)oThrParam;
                StructThreadCom thrdCom = new StructThreadCom();
                // connect to database
                if (dbh.ConnectDB(NyxMain.ConProfile, false))
                {
                    if (action == EnumAction.tablespaces)
                    {
                        if (dbh.ExecuteReader(qryTSInfo))
                        {   // read
                            thrdCom.qTSInfo = new Queue<StructTSInfo>();
                            try
                            {
                                while (dbh.Read())
                                {
                                    // filling struct && enqueue
                                    StructTSInfo tsInfo = new StructTSInfo();
                                    tsInfo.Name = dbh.GetValue(0).ToString();
                                    // blocksize dbh.GetValue(1).ToString();
                                    tsInfo.Type = dbh.GetValue(2).ToString();
                                    tsInfo.ExternalManaged = dbh.GetValue(3).ToString();
                                    tsInfo.Status = dbh.GetValue(4).ToString();
                                    tsInfo.Flog = dbh.GetValue(5).ToString();
                                    Double.TryParse(dbh.GetValue(6).ToString().TrimStart().Substring(0, 2), out tsInfo.UsedPer);
                                    Double.TryParse(dbh.GetValue(7).ToString(), out tsInfo.Size);
                                    Double.TryParse(dbh.GetValue(8).ToString(), out tsInfo.UsedSpace);
                                    Double.TryParse(dbh.GetValue(9).ToString(), out tsInfo.FreeSpace);
                                    tsInfo.Logging = dbh.GetValue(10).ToString();
                                    tsInfo.Temporary = dbh.GetValue(11).ToString();
                                    // adding tablespace informations
                                    thrdCom.qTSInfo.Enqueue(tsInfo);
                                }
                            }
                            catch (IndexOutOfRangeException exc) { thrdCom.thrExc = new DBException(exc.Message.ToString(), exc); }
                            finally { dbh.Close(); }
                        }
                        else
                            thrdCom.thrExc = dbh.ErrExc;
                    }
                    else
                    {
                        if (dbh.ExecuteReader(qryDFInfo))
                        {
                            thrdCom.qDFInfo = new Queue<StructDFInfo>();
                            try
                            {
                                while (dbh.Read())
                                {
                                    StructDFInfo dfInfo = new StructDFInfo();
                                    dfInfo.Status = dbh.GetValue(0).ToString();
                                    dfInfo.Name = dbh.GetValue(1).ToString().Substring(
                                        dbh.GetValue(1).ToString().LastIndexOf("\\") + 1);
                                    dfInfo.Fld = dbh.GetValue(2).ToString();
                                    dfInfo.AutoExt = dbh.GetValue(3).ToString();
                                    dfInfo.Tablespace = dbh.GetValue(4).ToString();
                                    Double.TryParse(dbh.GetValue(5).ToString(), out dfInfo.IncBy);
                                    Double.TryParse(dbh.GetValue(6).ToString(), out dfInfo.Size);
                                    Double.TryParse(dbh.GetValue(7).ToString(), out dfInfo.UsedSize);
                                    Double.TryParse(dbh.GetValue(8).ToString(), out dfInfo.UsedPer);
                                    // adding datafile informations
                                    thrdCom.qDFInfo.Enqueue(dfInfo);
                                }
                            }
                            catch (IndexOutOfRangeException exc) { thrdCom.thrExc = new DBException(exc.Message.ToString(), exc); }
                            finally { dbh.Close(); }
                        }
                        else
                            thrdCom.thrExc = dbh.ErrExc;
                    }
                }
                else // if (dbh.ConnectDb(NyxMain.ConProfile))
                    thrdCom.thrExc = dbh.ErrExc;
                // invoke final method
                if (action == EnumAction.tablespaces)
                    this.Invoke(new ThrdEndPDelegate(GetTSInfoDone), thrdCom);
                else
                    this.Invoke(new ThrdEndPDelegate(GetDFInfoDone), thrdCom);
            }
            catch (System.Threading.ThreadAbortException texc)
            {   // show messagehandler
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrThreadBreak", new string[] { texc.Message }),
                    texc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                // close
                this.Close();
            }
            finally { dbh.DisconnectDB(); } 
        }

        private void GetTSInfo()
        {   
            this._lvTS.Items.Clear();
            CtrlGUI(false);
            // start thread
            doWork = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(ThreadStart));
            doWork.IsBackground = true;
            doWork.Start(EnumAction.tablespaces);
        }
        private void GetTSInfoDone(StructThreadCom thrdCom)
        {
            if (thrdCom.thrExc == null)
            {
                this._lvTS.BeginUpdate();
                foreach (StructTSInfo tsInfo in thrdCom.qTSInfo)
                {
                    this._lvTS.Items.Add(GuiHelper.BuildListViewItem(
                        tsInfo.Name, new string[] { tsInfo.Type, tsInfo.Status, tsInfo.Logging,
                        tsInfo.Flog, tsInfo.UsedPer.ToString("N", NyxMain.NyxCI), 
                            tsInfo.Size.ToString("N", NyxMain.NyxCI), tsInfo.UsedSpace.ToString("N", NyxMain.NyxCI),
                            tsInfo.FreeSpace.ToString("N", NyxMain.NyxCI), tsInfo.ExternalManaged }));
                    if (tsInfo.UsedPer > 90)
                        this._lvTS.Items[this._lvTS.Items.Count-1].ForeColor = System.Drawing.Color.Red;
                }
                this._lvTS.EndUpdate();
            }
            else
            {
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("TablespacesErrTSRtv", new string[] { dbh.ErrExc.Message.ToString() }),
                    AppDialogs.MessageHandler.EnumMsgType.Error);
            }
            // activate gui again
            CtrlGUI(true);
        }

        private void GetDFInfo()
        {   // cleanup gui
            this._lvDF.Items.Clear();
            CtrlGUI(false);
            // start thread
            doWork = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(ThreadStart));
            doWork.IsBackground = true;
            doWork.Start(EnumAction.datafiles);
        }
        private void GetDFInfoDone(StructThreadCom thrdCom)
        {
            if (thrdCom.thrExc == null)
            {
                string tmpAExt;
                this._lvDF.BeginUpdate();
                foreach (StructDFInfo dfInfo in thrdCom.qDFInfo)
                {
                    if (String.Compare(dfInfo.AutoExt.ToString(), "NO", true, NyxMain.NyxCI) == 0)
                        tmpAExt = dfInfo.IncBy.ToString("N", NyxMain.NyxCI);
                    else
                        tmpAExt = "N/A";
                    this._lvDF.Items.Add(GuiHelper.BuildListViewItem(
                            dfInfo.Name, new string[] { dfInfo.Fld, dfInfo.AutoExt, dfInfo.Status,
                            dfInfo.Size.ToString("N", NyxMain.NyxCI), dfInfo.UsedSize.ToString("N", NyxMain.NyxCI),
                            dfInfo.UsedPer.ToString("N", NyxMain.NyxCI), tmpAExt, dfInfo.Tablespace}));
                    if (dfInfo.UsedPer > 90)
                        this._lvTS.Items[this._lvTS.Items.Count-1].ForeColor = System.Drawing.Color.Red;
                }
                this._lvDF.EndUpdate();
            }
            else
            {
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("TablespacesErrTSRtv", new string[] { dbh.ErrExc.Message.ToString() }),
                    AppDialogs.MessageHandler.EnumMsgType.Error);
            }
            // activate gui again
            CtrlGUI(true);
        }

        private void CtrlGUI(bool state)
        {
            this._tc.Enabled = state;
            this._btRefresh.Enabled = state;
            this._btClose.Enabled = state;
            if (state)
            {
                this._pgBar.Style = ProgressBarStyle.Blocks;
                this._pgBar.Value = 0;                
            }
            else
            {
                this._pgBar.Style = ProgressBarStyle.Marquee;
                this._pgBar.MarqueeAnimationSpeed = 100;
            }
        }
        private void _btRefresh_Click(object sender, EventArgs e)
        {
            if (this._tc.SelectedTab == this._tpTableSpaces)
                GetTSInfo();
            else
                GetDFInfo();
        }
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _tc_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._tc.SelectedTab == this._tpDataFiles && this._lvDF.Items.Count < 1)
                GetDFInfo();
            else if (this._lvTS.Items.Count < 1)
                GetTSInfo();
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        private void OrclTablespaces_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }
    }
}