namespace Nyx.NyxOrcl
{
    partial class OrclInitInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrclInitInfo));
            this._tc = new System.Windows.Forms.TabControl();
            this._tpDefault = new System.Windows.Forms.TabPage();
            this._lvDefault = new System.Windows.Forms.ListView();
            this.lvcDef_Name = new System.Windows.Forms.ColumnHeader();
            this.lvcDef_Value = new System.Windows.Forms.ColumnHeader();
            this._tpNonDefault = new System.Windows.Forms.TabPage();
            this._lvNonDefault = new System.Windows.Forms.ListView();
            this.lvcNDef_Name = new System.Windows.Forms.ColumnHeader();
            this.lvcNDef_Value = new System.Windows.Forms.ColumnHeader();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this._lblFilter = new System.Windows.Forms.Label();
            this._btClose = new System.Windows.Forms.Button();
            this._tbFilter = new System.Windows.Forms.TextBox();
            this._tc.SuspendLayout();
            this._tpDefault.SuspendLayout();
            this._tpNonDefault.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tc
            // 
            this.tableLayoutPanel1.SetColumnSpan(this._tc, 3);
            this._tc.Controls.Add(this._tpDefault);
            this._tc.Controls.Add(this._tpNonDefault);
            resources.ApplyResources(this._tc, "_tc");
            this._tc.Name = "_tc";
            this._tc.SelectedIndex = 0;
            this._tc.SelectedIndexChanged += new System.EventHandler(this._tc_SelectedIndexChanged);
            // 
            // _tpDefault
            // 
            this._tpDefault.Controls.Add(this._lvDefault);
            resources.ApplyResources(this._tpDefault, "_tpDefault");
            this._tpDefault.Name = "_tpDefault";
            this._tpDefault.UseVisualStyleBackColor = true;
            // 
            // _lvDefault
            // 
            this._lvDefault.BackColor = System.Drawing.Color.GhostWhite;
            this._lvDefault.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvcDef_Name,
            this.lvcDef_Value});
            resources.ApplyResources(this._lvDefault, "_lvDefault");
            this._lvDefault.FullRowSelect = true;
            this._lvDefault.GridLines = true;
            this._lvDefault.Name = "_lvDefault";
            this._lvDefault.UseCompatibleStateImageBehavior = false;
            this._lvDefault.View = System.Windows.Forms.View.Details;
            this._lvDefault.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvDefault_ColumnClick);
            // 
            // lvcDef_Name
            // 
            resources.ApplyResources(this.lvcDef_Name, "lvcDef_Name");
            // 
            // lvcDef_Value
            // 
            resources.ApplyResources(this.lvcDef_Value, "lvcDef_Value");
            // 
            // _tpNonDefault
            // 
            this._tpNonDefault.Controls.Add(this._lvNonDefault);
            resources.ApplyResources(this._tpNonDefault, "_tpNonDefault");
            this._tpNonDefault.Name = "_tpNonDefault";
            this._tpNonDefault.UseVisualStyleBackColor = true;
            // 
            // _lvNonDefault
            // 
            this._lvNonDefault.BackColor = System.Drawing.Color.GhostWhite;
            this._lvNonDefault.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvcNDef_Name,
            this.lvcNDef_Value});
            resources.ApplyResources(this._lvNonDefault, "_lvNonDefault");
            this._lvNonDefault.FullRowSelect = true;
            this._lvNonDefault.GridLines = true;
            this._lvNonDefault.Name = "_lvNonDefault";
            this._lvNonDefault.UseCompatibleStateImageBehavior = false;
            this._lvNonDefault.View = System.Windows.Forms.View.Details;
            this._lvNonDefault.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvNonDefault_ColumnClick);
            // 
            // lvcNDef_Name
            // 
            resources.ApplyResources(this.lvcNDef_Name, "lvcNDef_Name");
            // 
            // lvcNDef_Value
            // 
            resources.ApplyResources(this.lvcNDef_Value, "lvcNDef_Value");
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this._lblFilter, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this._tc, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this._btClose, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this._tbFilter, 1, 1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // _lblFilter
            // 
            resources.ApplyResources(this._lblFilter, "_lblFilter");
            this._lblFilter.Name = "_lblFilter";
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // _tbFilter
            // 
            resources.ApplyResources(this._tbFilter, "_tbFilter");
            this._tbFilter.Name = "_tbFilter";
            this._tbFilter.TextChanged += new System.EventHandler(this._tbFilter_TextChanged);
            // 
            // OrclInitInfo
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btClose;
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "OrclInitInfo";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrclInitInfo_FormClosing);
            this.Load += new System.EventHandler(this.FrmInitInfo_Load);
            this._tc.ResumeLayout(false);
            this._tpDefault.ResumeLayout(false);
            this._tpNonDefault.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl _tc;
        private System.Windows.Forms.TabPage _tpDefault;
        private System.Windows.Forms.TabPage _tpNonDefault;
        private System.Windows.Forms.ListView _lvDefault;
        private System.Windows.Forms.ColumnHeader lvcDef_Name;
        private System.Windows.Forms.ColumnHeader lvcDef_Value;
        private System.Windows.Forms.ListView _lvNonDefault;
        private System.Windows.Forms.ColumnHeader lvcNDef_Name;
        private System.Windows.Forms.ColumnHeader lvcNDef_Value;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button _btClose;
        private System.Windows.Forms.TextBox _tbFilter;
        private System.Windows.Forms.Label _lblFilter;
    }
}