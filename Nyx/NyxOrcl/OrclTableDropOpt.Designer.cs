namespace Nyx.NyxOrcl
{
    /// <summary>
    /// Oracle DropOptions GUI
    /// </summary>
    partial class OrclDropOpt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrclDropOpt));
            this._bt_ok = new System.Windows.Forms.Button();
            this._bt_cancel = new System.Windows.Forms.Button();
            this._cb_cc = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this._tb_adddo = new System.Windows.Forms.TextBox();
            this._cb_purge = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // _bt_ok
            // 
            this._bt_ok.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this._bt_ok, "_bt_ok");
            this._bt_ok.Image = global::Nyx.Properties.Resources.accept;
            this._bt_ok.Name = "_bt_ok";
            // 
            // _bt_cancel
            // 
            this._bt_cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this._bt_cancel, "_bt_cancel");
            this._bt_cancel.Image = global::Nyx.Properties.Resources.delete;
            this._bt_cancel.Name = "_bt_cancel";
            // 
            // _cb_cc
            // 
            resources.ApplyResources(this._cb_cc, "_cb_cc");
            this._cb_cc.Name = "_cb_cc";
            this._cb_cc.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this._tb_adddo);
            this.groupBox1.Controls.Add(this._cb_purge);
            this.groupBox1.Controls.Add(this._cb_cc);
            this.groupBox1.Controls.Add(this._bt_cancel);
            this.groupBox1.Controls.Add(this._bt_ok);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // _tb_adddo
            // 
            this._tb_adddo.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._tb_adddo, "_tb_adddo");
            this._tb_adddo.Name = "_tb_adddo";
            this._tb_adddo.Enter += new System.EventHandler(this._tb_adddo_Enter);
            // 
            // _cb_purge
            // 
            resources.ApplyResources(this._cb_purge, "_cb_purge");
            this._cb_purge.Name = "_cb_purge";
            this._cb_purge.UseVisualStyleBackColor = true;
            // 
            // OrclDropOpt
            // 
            this.AcceptButton = this._bt_ok;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._bt_cancel;
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "OrclDropOpt";
            this.ShowInTaskbar = false;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        private System.Windows.Forms.Button _bt_ok;
        private System.Windows.Forms.Button _bt_cancel;
        private System.Windows.Forms.CheckBox _cb_cc;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox _cb_purge;
        private System.Windows.Forms.TextBox _tb_adddo;

    }
}