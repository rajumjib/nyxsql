namespace Nyx.NyxOrcl
{
    /// <summary>
    /// Oracle Tablespaces Overview
    /// </summary>
    partial class OrclTablespaces
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrclTablespaces));
            this._lvTS = new System.Windows.Forms.ListView();
            this._lvcTSName = new System.Windows.Forms.ColumnHeader();
            this._lvcTSType = new System.Windows.Forms.ColumnHeader();
            this._lvcTSStatus = new System.Windows.Forms.ColumnHeader();
            this._lvcTSLog = new System.Windows.Forms.ColumnHeader();
            this._lvcTSFLog = new System.Windows.Forms.ColumnHeader();
            this._lvcTSUsedPer = new System.Windows.Forms.ColumnHeader();
            this._lvcTSSize = new System.Windows.Forms.ColumnHeader();
            this._lvcTSUsed = new System.Windows.Forms.ColumnHeader();
            this._lvcTSFree = new System.Windows.Forms.ColumnHeader();
            this._lvcTSExt = new System.Windows.Forms.ColumnHeader();
            this._tc = new System.Windows.Forms.TabControl();
            this._tpTableSpaces = new System.Windows.Forms.TabPage();
            this._tpDataFiles = new System.Windows.Forms.TabPage();
            this._lvDF = new System.Windows.Forms.ListView();
            this._lvcDFName = new System.Windows.Forms.ColumnHeader();
            this._lvcDFFileID = new System.Windows.Forms.ColumnHeader();
            this._lvcDFAExt = new System.Windows.Forms.ColumnHeader();
            this._lvcDFStatus = new System.Windows.Forms.ColumnHeader();
            this._lvcDFMSize = new System.Windows.Forms.ColumnHeader();
            this._lvcDFUsedSize = new System.Windows.Forms.ColumnHeader();
            this._lvcDFUsedPer = new System.Windows.Forms.ColumnHeader();
            this._lvcDFInc = new System.Windows.Forms.ColumnHeader();
            this._lvcDFTablespace = new System.Windows.Forms.ColumnHeader();
            this._tblLayout = new System.Windows.Forms.TableLayoutPanel();
            this._btRefresh = new System.Windows.Forms.Button();
            this._pgBar = new System.Windows.Forms.ProgressBar();
            this._btClose = new System.Windows.Forms.Button();
            this._tc.SuspendLayout();
            this._tpTableSpaces.SuspendLayout();
            this._tpDataFiles.SuspendLayout();
            this._tblLayout.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lvTS
            // 
            this._lvTS.BackColor = System.Drawing.Color.GhostWhite;
            this._lvTS.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._lvcTSName,
            this._lvcTSType,
            this._lvcTSStatus,
            this._lvcTSLog,
            this._lvcTSFLog,
            this._lvcTSUsedPer,
            this._lvcTSSize,
            this._lvcTSUsed,
            this._lvcTSFree,
            this._lvcTSExt});
            this._lvTS.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lvTS, "_lvTS");
            this._lvTS.FullRowSelect = true;
            this._lvTS.GridLines = true;
            this._lvTS.HideSelection = false;
            this._lvTS.MultiSelect = false;
            this._lvTS.Name = "_lvTS";
            this._lvTS.UseCompatibleStateImageBehavior = false;
            this._lvTS.View = System.Windows.Forms.View.Details;
            // 
            // _lvcTSName
            // 
            resources.ApplyResources(this._lvcTSName, "_lvcTSName");
            // 
            // _lvcTSType
            // 
            resources.ApplyResources(this._lvcTSType, "_lvcTSType");
            // 
            // _lvcTSStatus
            // 
            resources.ApplyResources(this._lvcTSStatus, "_lvcTSStatus");
            // 
            // _lvcTSLog
            // 
            resources.ApplyResources(this._lvcTSLog, "_lvcTSLog");
            // 
            // _lvcTSFLog
            // 
            resources.ApplyResources(this._lvcTSFLog, "_lvcTSFLog");
            // 
            // _lvcTSUsedPer
            // 
            resources.ApplyResources(this._lvcTSUsedPer, "_lvcTSUsedPer");
            // 
            // _lvcTSSize
            // 
            resources.ApplyResources(this._lvcTSSize, "_lvcTSSize");
            // 
            // _lvcTSUsed
            // 
            resources.ApplyResources(this._lvcTSUsed, "_lvcTSUsed");
            // 
            // _lvcTSFree
            // 
            resources.ApplyResources(this._lvcTSFree, "_lvcTSFree");
            // 
            // _lvcTSExt
            // 
            resources.ApplyResources(this._lvcTSExt, "_lvcTSExt");
            // 
            // _tc
            // 
            this._tblLayout.SetColumnSpan(this._tc, 3);
            this._tc.Controls.Add(this._tpTableSpaces);
            this._tc.Controls.Add(this._tpDataFiles);
            resources.ApplyResources(this._tc, "_tc");
            this._tc.Name = "_tc";
            this._tc.SelectedIndex = 0;
            this._tc.SelectedIndexChanged += new System.EventHandler(this._tc_SelectedIndexChanged);
            // 
            // _tpTableSpaces
            // 
            this._tpTableSpaces.Controls.Add(this._lvTS);
            resources.ApplyResources(this._tpTableSpaces, "_tpTableSpaces");
            this._tpTableSpaces.Name = "_tpTableSpaces";
            this._tpTableSpaces.UseVisualStyleBackColor = true;
            // 
            // _tpDataFiles
            // 
            this._tpDataFiles.Controls.Add(this._lvDF);
            resources.ApplyResources(this._tpDataFiles, "_tpDataFiles");
            this._tpDataFiles.Name = "_tpDataFiles";
            this._tpDataFiles.UseVisualStyleBackColor = true;
            // 
            // _lvDF
            // 
            this._lvDF.BackColor = System.Drawing.Color.GhostWhite;
            this._lvDF.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._lvcDFName,
            this._lvcDFFileID,
            this._lvcDFAExt,
            this._lvcDFStatus,
            this._lvcDFMSize,
            this._lvcDFUsedSize,
            this._lvcDFUsedPer,
            this._lvcDFInc,
            this._lvcDFTablespace});
            this._lvDF.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._lvDF, "_lvDF");
            this._lvDF.FullRowSelect = true;
            this._lvDF.GridLines = true;
            this._lvDF.HideSelection = false;
            this._lvDF.MultiSelect = false;
            this._lvDF.Name = "_lvDF";
            this._lvDF.UseCompatibleStateImageBehavior = false;
            this._lvDF.View = System.Windows.Forms.View.Details;
            // 
            // _lvcDFName
            // 
            resources.ApplyResources(this._lvcDFName, "_lvcDFName");
            // 
            // _lvcDFFileID
            // 
            resources.ApplyResources(this._lvcDFFileID, "_lvcDFFileID");
            // 
            // _lvcDFAExt
            // 
            resources.ApplyResources(this._lvcDFAExt, "_lvcDFAExt");
            // 
            // _lvcDFStatus
            // 
            resources.ApplyResources(this._lvcDFStatus, "_lvcDFStatus");
            // 
            // _lvcDFMSize
            // 
            resources.ApplyResources(this._lvcDFMSize, "_lvcDFMSize");
            // 
            // _lvcDFUsedSize
            // 
            resources.ApplyResources(this._lvcDFUsedSize, "_lvcDFUsedSize");
            // 
            // _lvcDFUsedPer
            // 
            resources.ApplyResources(this._lvcDFUsedPer, "_lvcDFUsedPer");
            // 
            // _lvcDFInc
            // 
            resources.ApplyResources(this._lvcDFInc, "_lvcDFInc");
            // 
            // _lvcDFTablespace
            // 
            resources.ApplyResources(this._lvcDFTablespace, "_lvcDFTablespace");
            // 
            // _tblLayout
            // 
            resources.ApplyResources(this._tblLayout, "_tblLayout");
            this._tblLayout.Controls.Add(this._btRefresh, 1, 1);
            this._tblLayout.Controls.Add(this._tc, 0, 0);
            this._tblLayout.Controls.Add(this._pgBar, 0, 1);
            this._tblLayout.Controls.Add(this._btClose, 2, 1);
            this._tblLayout.Name = "_tblLayout";
            // 
            // _btRefresh
            // 
            resources.ApplyResources(this._btRefresh, "_btRefresh");
            this._btRefresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            this._btRefresh.Name = "_btRefresh";
            this._btRefresh.UseVisualStyleBackColor = true;
            this._btRefresh.Click += new System.EventHandler(this._btRefresh_Click);
            // 
            // _pgBar
            // 
            resources.ApplyResources(this._pgBar, "_pgBar");
            this._pgBar.Name = "_pgBar";
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // OrclTablespaces
            // 
            this.AcceptButton = this._btRefresh;
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btClose;
            this.Controls.Add(this._tblLayout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "OrclTablespaces";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OrclTablespaces_FormClosing);
            this.Load += new System.EventHandler(this.Tablespaces_Load);
            this._tc.ResumeLayout(false);
            this._tpTableSpaces.ResumeLayout(false);
            this._tpDataFiles.ResumeLayout(false);
            this._tblLayout.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView _lvTS;
        private System.Windows.Forms.ColumnHeader _lvcTSName;
        private System.Windows.Forms.ColumnHeader _lvcTSType;
        private System.Windows.Forms.ColumnHeader _lvcTSStatus;
        private System.Windows.Forms.ColumnHeader _lvcTSUsedPer;
        private System.Windows.Forms.ColumnHeader _lvcTSSize;
        private System.Windows.Forms.ColumnHeader _lvcTSUsed;
        private System.Windows.Forms.ColumnHeader _lvcTSFree;
        private System.Windows.Forms.ColumnHeader _lvcTSLog;
        private System.Windows.Forms.TabControl _tc;
        private System.Windows.Forms.TabPage _tpTableSpaces;
        private System.Windows.Forms.TabPage _tpDataFiles;
        private System.Windows.Forms.ColumnHeader _lvcTSExt;
        private System.Windows.Forms.ColumnHeader _lvcTSFLog;
        private System.Windows.Forms.ListView _lvDF;
        private System.Windows.Forms.ColumnHeader _lvcDFName;
        private System.Windows.Forms.ColumnHeader _lvcDFFileID;
        private System.Windows.Forms.ColumnHeader _lvcDFMSize;
        private System.Windows.Forms.ColumnHeader _lvcDFStatus;
        private System.Windows.Forms.ColumnHeader _lvcDFAExt;
        private System.Windows.Forms.ColumnHeader _lvcDFUsedSize;
        private System.Windows.Forms.ColumnHeader _lvcDFUsedPer;
        private System.Windows.Forms.ColumnHeader _lvcDFTablespace;
        private System.Windows.Forms.ColumnHeader _lvcDFInc;
        private System.Windows.Forms.TableLayoutPanel _tblLayout;
        private System.Windows.Forms.ProgressBar _pgBar;
        private System.Windows.Forms.Button _btClose;
        private System.Windows.Forms.Button _btRefresh;
    }
}