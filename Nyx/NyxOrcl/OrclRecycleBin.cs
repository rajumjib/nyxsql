/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Oracle.DataAccess.Client;
using System.Windows.Forms;
using Nyx.AppDialogs;

namespace Nyx.NyxOrcl
{
    /// <summary> Oracle recyclebin dialog </summary>
    public partial class OrclRecycleBin : Form
    {   // declaring some vars
        Classes.DBHelper dbh = new Classes.DBHelper(NyxSettings.DBType.Oracle);

        /// <summary> init function </summary>
        public OrclRecycleBin()
        {
            InitializeComponent();
            // format Datagridview
            this._dgContent = GuiHelper.DataGridViewFormat(NyxMain.UASett, this._dgContent);
        }
        private void FrmOrclRecycleBin_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // check oracle version
            int iVnr;
            Int32.TryParse(NyxMain.DBVersion.Substring(0, 2), out iVnr);
            if (iVnr < 10)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_NotSupported"), MessageHandler.EnumMsgType.Information);
                this.Close();
            }

            if (dbh.ConnectDB(NyxMain.ConProfile, false))
                GetRBinContent();
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                                 dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Information);
                this.Close();
            }
        }
        private void FrmOrclRecycleBin_FormClosing(object sender, FormClosingEventArgs e)
        {
            dbh.DisconnectDB();
        }

        private void _dgContent_SelectionChanged(object sender, EventArgs e)
        {
            if (this._dgContent.Rows.Count > 0)
            {
                this._btContent_Purge.Enabled = true;
                this._btContent_Restore.Enabled = true;
                this._cmRB_Purge.Enabled = true;
                // check if there is an undropable selected?
                foreach (DataGridViewRow dgvr in this._dgContent.SelectedRows)
                {
                    if (String.Compare(dgvr.Cells["dgvcUndropable"].Value.ToString(), "NO", true, NyxMain.NyxCI) == 0)
                        this._btContent_Restore.Enabled = false;
                }
            }
            else
            {
                this._btContent_Purge.Enabled = false;
                this._btContent_Restore.Enabled = false;
                this._cmRB_Purge.Enabled = false;
            }
        }
        private void _cmContent_SelAll_Click(object sender, EventArgs e)
        {
            GuiHelper.DataGridViewRowSelectState(this._dgContent, GuiHelper.EnumLVSelection.Select);
        }
        private void _cmContent_SelNone_Click(object sender, EventArgs e)
        {
            GuiHelper.DataGridViewRowSelectState(this._dgContent, GuiHelper.EnumLVSelection.Unselect);
        }
        private void _cmContent_InvertSel_Click(object sender, EventArgs e)
        {
            GuiHelper.DataGridViewRowSelectState(this._dgContent, GuiHelper.EnumLVSelection.Invert);
        }
        private void _contentRefresh_Click(object sender, EventArgs e)
        {
            GetRBinContent();
        }
        private void GetRBinContent()
        {   // clearing combobox and listview
            this._dgContent.Rows.Clear();
            // getting purged objects
            if (dbh.ExecuteReader("select object_name,original_name,type,ts_name,createtime,droptime,space,can_undrop from recyclebin"))
            {
                try
                {
                    while (dbh.Read())
                    {
                        this._dgContent.Rows.Add(new string[] { dbh.GetValue(0).ToString(), dbh.GetValue(1).ToString(), 
                            dbh.GetValue(2).ToString(), dbh.GetValue(3).ToString(), dbh.GetValue(4).ToString(),
                            dbh.GetValue(5).ToString(), dbh.GetValue(6).ToString(), dbh.GetValue(7).ToString() });
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_ContentErr", new string[] { exc.Message.ToString() }),
                            exc, MessageHandler.EnumMsgType.Warning);
                }
                finally { dbh.Close(); }
                // gui settings
                if (this._dgContent.Rows.Count > 0)
                {
                    this._btContent_PurgeAll.Enabled = true;
                    this._cmRB_PurgeAll.Enabled = true;
                }
                else
                {
                    this._btContent_PurgeAll.Enabled = false;
                    this._cmRB_PurgeAll.Enabled = false;
                }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_NotSupported", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
        }
        private void _contentPurge_Click(object sender, EventArgs e)
        {
            if (this._dgContent.SelectedRows.Count > 0)
            {   // build row list
                string[] items = new string[this._dgContent.SelectedRows.Count];
                for (int i = 0; i < this._dgContent.SelectedRows.Count; i++)
                    items[i] = this._dgContent.SelectedRows[i].Cells[1].Value.ToString();
                // show dialogresult
                DialogResult dRes = ListMessageBox.ShowListMessageBox("Purge", "Purge?", items);
                if (dRes == DialogResult.OK)
                {
                    DataGridViewRow[] dgvrs = new DataGridViewRow[this._dgContent.SelectedRows.Count];
                    this._dgContent.SelectedRows.CopyTo(dgvrs, 0);
                    if (DropObject(dgvrs))
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_Purged"),
                                    MessageHandler.EnumMsgType.Information);
                    }
                    GetRBinContent();
                }
            }
        }
        private void _contentPurgeAll_Click(object sender, EventArgs e)
        {
            if (this._dgContent.Rows.Count > 0)
            {   // show dialogresult
                StructRetDialog srd = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("ORB_PurgeAll"), 
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                if (srd.DRes == NyxDialogResult.Yes)
                {
                    DataGridViewRow[] dgvrs = new DataGridViewRow[this._dgContent.Rows.Count];
                    this._dgContent.Rows.CopyTo(dgvrs, 0);
                    if (DropObject(dgvrs))
                    {
                        GetRBinContent();
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_Purged"),
                                    MessageHandler.EnumMsgType.Information);
                    }
                    GetRBinContent();
                }
            }
        }
        private bool DropObject(DataGridViewRow[] dgvrs)
        {
            foreach (DataGridViewRow dgvr in dgvrs)
            {
                string type = dgvr.Cells[2].Value.ToString();
                string table = dgvr.Cells[1].Value.ToString();
                // query
                string qry = String.Format(NyxMain.NyxCI, "PURGE {0} {1}", type, table);
                if (!dbh.ExecuteNonQuery(qry))
                {   // we'll only handle non index errors here
                    if (String.Compare(type, "INDEX", true, NyxMain.NyxCI) == 0)
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_PurgeError", new string[] { table, dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                        return false;
                    }
                }
            }
            return true;
        }
        private void _btContent_Restore_Click(object sender, EventArgs e)
        {
            if (this._dgContent.SelectedRows.Count > 0)
            {   // build row list
                string[] items = new string[this._dgContent.SelectedRows.Count];
                for (int i = 0; i < this._dgContent.SelectedRows.Count; i++)
                    items[i] = this._dgContent.SelectedRows[i].Cells[1].Value.ToString();
                // show dialogresult
                DialogResult dRes = ListMessageBox.ShowListMessageBox("Restore", "Restore?", items);
                if (dRes == DialogResult.OK)
                {
                    DataGridViewRow[] dgvrs = new DataGridViewRow[this._dgContent.SelectedRows.Count];
                    this._dgContent.SelectedRows.CopyTo(dgvrs, 0);
                    if(RestoreObjects(dgvrs))
                    {
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_ObjectRestored"),
                                    MessageHandler.EnumMsgType.Information);
                    }
                    GetRBinContent();
                }
            }
        }
        private bool RestoreObjects(DataGridViewRow[] dgvrs)
        {
            foreach (DataGridViewRow dgvr in dgvrs)
            {   // check type again, to be sure that we don't get an index inside here
                string type = dgvr.Cells[2].Value.ToString();
                if (String.Compare(type, "INDEX", true, NyxMain.NyxCI) == 0)
                    continue;
                string table = dgvr.Cells[1].Value.ToString();
                // query
                string qry = String.Format(NyxMain.NyxCI, "FLASHBACK {0} {1} TO BEFORE DROP", type, table);
                if (!dbh.ExecuteNonQuery(qry))
                {   
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_PurgeError", new string[] { table, dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                        return false;
                }
            }
            return true;
        }

        private void _tcRB_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._tcRB.SelectedTab == this._tpRBDetails)
            {
                if (this._dgTablespace.Rows.Count == 0)
                    GetFlashBackDetails();
            }
        }
        private void _btDetail_Refresh_Click(object sender, EventArgs e)
        {
            GetFlashBackDetails();
        }
        private void GetFlashBackDetails()
        {   // cleanup
            this._dgTablespace.Rows.Clear();
            this._dgDetails.Rows.Clear();
            // fill tablespaces
            if (dbh.ExecuteReader("SELECT tablespace_name, retention FROM dba_tablespaces"))
            {
                try
                {
                    while (dbh.Read())
                    {
                        this._dgTablespace.Rows.Add(new string[] { dbh.GetValue(0).ToString(), dbh.GetValue(1).ToString() });
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { exc.Message.ToString() }),
                            exc, MessageHandler.EnumMsgType.Warning);
                    this._gbFb.Enabled = false;
                    return;
                }
                finally { dbh.Close(); }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this._gbFb.Enabled = false;
                return;
            }
            // get name and value of db_flachback_retenttaion target
            if (dbh.ExecuteReader("SELECT name, value FROM gv$parameter WHERE name LIKE '%flashback%'"))
            {
                try
                {
                    while (dbh.Read())
                    {
                        string name = dbh.GetValue(0).ToString().Replace("_", " ");
                        string val = dbh.GetValue(1).ToString();
                        if (name == "db flashback retention target")
                            this._mtbFb_Retention.Text = val;

                        this._dgDetails.Rows.Add(new string[] { name, val });
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { exc.Message.ToString() }),
                            exc, MessageHandler.EnumMsgType.Warning);
                    this._gbFb.Enabled = false;
                    return;
                }
                finally { dbh.Close(); }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this._gbFb.Enabled = false;
                return;
            }
            // get flashback on? logmode
            if (dbh.ExecuteReader("SELECT flashback_on, log_mode FROM gv$database"))
            {
                try
                {
                    if (dbh.Read())
                    {
                        string flashback_on = dbh.GetValue(0).ToString();
                        string log_mode = dbh.GetValue(1).ToString();

                        if (flashback_on == "ON")
                        {
                            this._btFb_Off.Enabled = true;
                            this._btFb_On.Enabled = false;
                        }
                        else
                        {
                            this._btFb_Off.Enabled = false;
                            this._btFb_On.Enabled = true;
                        }

                        this._dgDetails.Rows.Add(new string[] { "status", flashback_on });
                        this._dgDetails.Rows.Add(new string[] { "log mode", log_mode });
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { exc.Message.ToString() }),
                            exc, MessageHandler.EnumMsgType.Warning);
                    this._gbFb.Enabled = false;
                    return;
                }
                finally { dbh.Close(); }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this._gbFb.Enabled = false;
                return;
            }
            // get current scn
            if (dbh.ExecuteReader("SELECT current_scn FROM gv$database"))
            {
                try
                {
                    if (dbh.Read())
                    {
                        string current_scn = dbh.GetValue(0).ToString();
                        this._dgDetails.Rows.Add(new string[] { "current scn", current_scn });
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { exc.Message.ToString() }),
                            exc, MessageHandler.EnumMsgType.Warning);
                    this._gbFb.Enabled = false;
                    return;
                }
                finally { dbh.Close(); }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this._gbFb.Enabled = false;
                return;
            }
            // get oldest flashback scn and time
            if (dbh.ExecuteReader("SELECT oldest_flashback_scn, oldest_flashback_time FROM gv$flashback_database_log"))
            {
                try
                {
                    if (dbh.Read())
                    {
                        string oldest_flashback_scn = String.Empty;
                        string oldest_flashback_time = String.Empty;
                        if (dbh.HasRows)
                        {
                            oldest_flashback_scn = dbh.GetValue(0).ToString();
                            oldest_flashback_time = dbh.GetValue(1).ToString();
                        }

                        this._dgDetails.Rows.Add(new string[] { "oldest flashback scn", oldest_flashback_scn });
                        this._dgDetails.Rows.Add(new string[] { "oldest flashback time", oldest_flashback_time });
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { exc.Message.ToString() }),
                            exc, MessageHandler.EnumMsgType.Warning);
                    this._gbFb.Enabled = false;
                    return;
                }
                finally { dbh.Close(); }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_DetailsErr", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this._gbFb.Enabled = false;
                return;
            }
            // enable gui
            this._gbFb.Enabled = true;

        }
        private void _btFlashback_On_Click(object sender, EventArgs e)
        {
            SetFlashbackState(true);
        }
        private void _btFlashback_Off_Click(object sender, EventArgs e)
        {
            SetFlashbackState(false);
        }
        private void SetFlashbackState(bool enable)
        {
            string qry = "ALTER DATABASE FLASHBACK ";
            if (enable)
                qry += "ON";
            else
                qry += "OFF";
            // try to execute
            if (!dbh.ExecuteNonQuery(qry))
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_AlterDatabaseErr", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
            }
            // refresh
            GetFlashBackDetails();
        }
        private void _btFlb_SetRetention_Click(object sender, EventArgs e)
        {


            int retTarg = -1;
            if (Int32.TryParse(this._mtbFb_Retention.Text.ToString(), out retTarg))
            {
                string qry = "ALTER SYSTEM SET db_flashback_retention_target = " + retTarg;

                // try to execute
                if (!dbh.ExecuteNonQuery(qry))
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ORB_AlterDatabaseErr", new string[] { dbh.ErrExc.Message.ToString() }),
                        dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
                // refresh
                GetFlashBackDetails();
            }
        }
        
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}