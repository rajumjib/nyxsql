/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace Nyx.NyxOrcl
{
    class OrclHelper
    {
        private OrclHelper() { }

        public static string BuildColumenDefinition(string sFldName, string sFldType, string sFldLength)
        {
            string coldef = null;
            switch (sFldType)
            {
                case "BIT":         // BIT          [(length)]
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    break;
                case "TINYINT":     // TINYINT	    [(length)] [UNSIGNED] [ZEROFILL]
                case "SMALLINT":    // SMALLINT     [(length)] [UNSIGNED] [ZEROFILL]
                case "MEDIUMINT":   // MEDIUMINT    [(length)] [UNSIGNED] [ZEROFILL]
                case "INT":         // INT          [(length)] [UNSIGNED] [ZEROFILL]
                case "INTEGER":     // INTEGER      [(length)] [UNSIGNED] [ZEROFILL]
                case "BIGINT":      // BIGINT       [(length)] [UNSIGNED] [ZEROFILL]
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    break;
                case "REAL":        // REAL         [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "DOUBLE":      // DOUBLE       [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "FLOAT":       // FLOAT        [(length,decimals)] [UNSIGNED] [ZEROFILL]
                case "DECIMAL":     // DECIMAL		(length,decimals) [UNSIGNED] [ZEROFILL]
                case "NUMERIC":     // NUMERIC 	    (length,decimals) [UNSIGNED] [ZEROFILL]
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    break;
                case "TIMESTAMP":   // TIMESTAMP
                    // name, type
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1}", sFldName, sFldType);
                    break;
                case "DATE":        // DATE
                case "TIME":        // TIME
                case "DATETIME":    // DATETIME
                case "YEAR":        // YEAR
                case "TINYBLOB":    // TINYBLOB
                case "BLOB":        // BLOB
                case "MEDIUMBLOB":  // MEDIUMBLOB
                case "LONGBLOB":    // LONGBLOB
                    // name, type
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1}", sFldName, sFldType);
                    break;
                case "CHAR":        // CHAR		    (length) [CHARACTER SET charset_name] [COLLATE collation_name] -fulltext
                case "VARCHAR":     // VARCHAR		(length) [CHARACTER SET charset_name] [COLLATE collation_name] -fulltext
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    break;
                case "BINARY":      // BINARY		(length)
                case "VARBINARY":   // VARBINARY	(length)
                    // name, type, length
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1} ({2})", sFldName, sFldType, sFldLength);
                    break;
                case "TINYTEXT":    // TINYTEXT 	[BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext 
                case "TEXT":        // TEXT 		[BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                case "MEDIUMTEXT":  // MEDIUMTEXT   [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                case "LONGTEXT":    // LONGTEXT     [BINARY] [CHARACTER SET charset_name] [COLLATE collation_name]  -fulltext
                    // name, type
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1}", sFldName, sFldType);
                    break;
                case "ENUM":        // ENUM        (value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
                case "SET":         // SET         (value1,value2,value3,...) [CHARACTER SET charset_name] [COLLATE collation_name]
                    // name, type
                    coldef = String.Format(NyxMain.NyxCI, "`{0}` {1}", sFldName, sFldType);
                    break;
            }
            return coldef;
        }
    }
}
