/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Nyx.AppDialogs;

namespace Nyx.GeneralDB
{
    public partial class RetrieveDbs : Form
    {
        private string errMsg;
        private NyxSettings.ProfileSettings profile;
        private Queue<string> qDbs;
        private System.Threading.Thread doWork;

        private delegate void ThrdParameterizedDelegate(Queue<string> param);
        /// <remarks> Error Message </remarks>
        public string ErrMsg { get { return errMsg; } }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        /// <summary>
        /// Init
        /// </summary>
        public RetrieveDbs()
        {
            InitializeComponent();
        }
        private void RetrieveDbs_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // start thread
            doWork = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(ThreadWork));
            doWork.IsBackground = true;
            doWork.Start(profile);
        }

        /// <summary>
        /// custom ShowDialog
        /// </summary>
        /// <param name="prfSettings">profilesettings</param>
        /// <param name="dbs">databaseque</param>
        /// <returns>true/false</returns>
        public bool ShowDialog(NyxSettings.ProfileSettings prfSettings, ref Queue<string> dbs)
        {
            this.profile = prfSettings;
            DialogResult dres = this.ShowDialog();
            if (dres == DialogResult.OK)
            {
                dbs = qDbs;
                return true;
            }
            else
            {
                MessageHandler.ShowDialog("Error retrieving Databases:" + this.errMsg, 
                    MessageHandler.EnumMsgType.Error);
                return false;
            }
        }

        private void ThreadWork(object oProfile)
        {
            NyxSettings.ProfileSettings profile = (NyxSettings.ProfileSettings)oProfile;
            // init some vars
            qDbs = new Queue<string>();
            Classes.DBHelper dbh = new Classes.DBHelper(profile.ProfileDBType);
            Classes.DBManagement dbm = new Classes.DBManagement(profile.ProfileDBType);
            // getting databases
            profile.ProfileDB = String.Empty;
            if (dbh.ConnectDB(profile, false))
            {
                // adding new items
                if (!dbm.GetDatabases(out qDbs, dbh))
                    MessageHandler.ShowDialog("Error retrieving Databases:" + dbm.ErrExc, 
                        MessageHandler.EnumMsgType.Error);
            }
            else
                this.errMsg = dbh.ErrExc.Message.ToString();
            // disconnect db
            dbh.DisconnectDB();
            // invoke final
            this.Invoke(new ThrdParameterizedDelegate(ThreadWorkDone), qDbs);
        }
        private void ThreadWorkDone(Queue<string> qDbs)
        {
            if (this.errMsg == null)
                this.DialogResult = DialogResult.OK;
            else
                this.DialogResult = DialogResult.Abort;
            this.Close();
        }
    }
}