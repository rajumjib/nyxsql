namespace Nyx.GeneralDB
{
    partial class TableStatistics
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TableStatistics));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tsTableStats = new System.Windows.Forms.ToolStrip();
            this._tsStart = new System.Windows.Forms.ToolStripButton();
            this._tsSave = new System.Windows.Forms.ToolStripButton();
            this._tsSep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tsBreak = new System.Windows.Forms.ToolStripButton();
            this._sS = new System.Windows.Forms.StatusStrip();
            this._ssPGBar = new System.Windows.Forms.ToolStripProgressBar();
            this._ssThrInf = new System.Windows.Forms.ToolStripStatusLabel();
            this._dgResult = new System.Windows.Forms.DataGridView();
            this.Tablename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RowCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TSize = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._tblLayoutPnl = new System.Windows.Forms.TableLayoutPanel();
            this._btClose = new System.Windows.Forms.Button();
            this._tsTableStats.SuspendLayout();
            this._sS.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgResult)).BeginInit();
            this._tblLayoutPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tsTableStats
            // 
            this._tsTableStats.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsTableStats.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsStart,
            this._tsSave,
            this._tsSep0,
            this._tsBreak});
            resources.ApplyResources(this._tsTableStats, "_tsTableStats");
            this._tsTableStats.Name = "_tsTableStats";
            // 
            // _tsStart
            // 
            this._tsStart.Image = global::Nyx.Properties.Resources.control_play_blue;
            resources.ApplyResources(this._tsStart, "_tsStart");
            this._tsStart.Name = "_tsStart";
            this._tsStart.Click += new System.EventHandler(this._tsStart_Click);
            // 
            // _tsSave
            // 
            resources.ApplyResources(this._tsSave, "_tsSave");
            this._tsSave.Image = global::Nyx.Properties.Resources.disk;
            this._tsSave.Name = "_tsSave";
            this._tsSave.Click += new System.EventHandler(this._tsSave_Click);
            // 
            // _tsSep0
            // 
            this._tsSep0.Name = "_tsSep0";
            resources.ApplyResources(this._tsSep0, "_tsSep0");
            // 
            // _tsBreak
            // 
            resources.ApplyResources(this._tsBreak, "_tsBreak");
            this._tsBreak.Image = global::Nyx.Properties.Resources.cancel;
            this._tsBreak.Name = "_tsBreak";
            this._tsBreak.Click += new System.EventHandler(this._tsBreak_Click);
            // 
            // _sS
            // 
            this._sS.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ssPGBar,
            this._ssThrInf});
            resources.ApplyResources(this._sS, "_sS");
            this._sS.Name = "_sS";
            // 
            // _ssPGBar
            // 
            this._ssPGBar.MarqueeAnimationSpeed = 0;
            this._ssPGBar.Name = "_ssPGBar";
            resources.ApplyResources(this._ssPGBar, "_ssPGBar");
            this._ssPGBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // _ssThrInf
            // 
            this._ssThrInf.Name = "_ssThrInf";
            resources.ApplyResources(this._ssThrInf, "_ssThrInf");
            // 
            // _dgResult
            // 
            this._dgResult.AllowUserToAddRows = false;
            this._dgResult.AllowUserToDeleteRows = false;
            this._dgResult.AllowUserToOrderColumns = true;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.WhiteSmoke;
            this._dgResult.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dgResult.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._dgResult.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._dgResult.BackgroundColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgResult.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._dgResult.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgResult.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Tablename,
            this.RowCount,
            this.TSize});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgResult.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this._dgResult, "_dgResult");
            this._dgResult.Name = "_dgResult";
            this._dgResult.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgResult.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this._dgResult.RowHeadersVisible = false;
            this._dgResult.RowTemplate.Height = 20;
            this._dgResult.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // Tablename
            // 
            this.Tablename.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            resources.ApplyResources(this.Tablename, "Tablename");
            this.Tablename.Name = "Tablename";
            this.Tablename.ReadOnly = true;
            // 
            // RowCount
            // 
            this.RowCount.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.RowCount, "RowCount");
            this.RowCount.Name = "RowCount";
            this.RowCount.ReadOnly = true;
            // 
            // TSize
            // 
            this.TSize.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            resources.ApplyResources(this.TSize, "TSize");
            this.TSize.Name = "TSize";
            this.TSize.ReadOnly = true;
            // 
            // _tblLayoutPnl
            // 
            resources.ApplyResources(this._tblLayoutPnl, "_tblLayoutPnl");
            this._tblLayoutPnl.Controls.Add(this._btClose, 0, 1);
            this._tblLayoutPnl.Controls.Add(this._dgResult, 0, 0);
            this._tblLayoutPnl.Name = "_tblLayoutPnl";
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // TableStatistics
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btClose;
            this.Controls.Add(this._tblLayoutPnl);
            this.Controls.Add(this._sS);
            this.Controls.Add(this._tsTableStats);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "TableStatistics";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TableStatistics_FormClosing);
            this.Load += new System.EventHandler(this.TableStatistics_Load);
            this._tsTableStats.ResumeLayout(false);
            this._tsTableStats.PerformLayout();
            this._sS.ResumeLayout(false);
            this._sS.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgResult)).EndInit();
            this._tblLayoutPnl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip _tsTableStats;
        private System.Windows.Forms.ToolStripButton _tsStart;
        private System.Windows.Forms.ToolStripButton _tsSave;
        private System.Windows.Forms.StatusStrip _sS;
        private System.Windows.Forms.ToolStripProgressBar _ssPGBar;
        private System.Windows.Forms.ToolStripStatusLabel _ssThrInf;
        private System.Windows.Forms.ToolStripSeparator _tsSep0;
        private System.Windows.Forms.ToolStripButton _tsBreak;
        private System.Windows.Forms.DataGridView _dgResult;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tablename;
        private System.Windows.Forms.DataGridViewTextBoxColumn RowCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn TSize;
        private System.Windows.Forms.TableLayoutPanel _tblLayoutPnl;
        private System.Windows.Forms.Button _btClose;
    }
}