/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using NyxSettings;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using Nyx.AppDialogs;

namespace Nyx.GeneralDB
{
    public partial class ProcessMng : Form
    {   // vars
        private Classes.DBHelper dbh = new Classes.DBHelper(NyxMain.ConProfile.ProfileDBType);

        /// <summary>
        /// Init
        /// </summary>
        public ProcessMng()
        {
            InitializeComponent();
            // set listview 
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case NyxSettings.DBType.MySql: // mysql is the default, therefor we dont have to do here anything
                    break;
                case NyxSettings.DBType.Oracle:
                    this._lvProcesses.Clear();
                    ColumnHeader sid = new ColumnHeader();
                    ColumnHeader serial = new ColumnHeader();
                    ColumnHeader pid = new ColumnHeader();
                    ColumnHeader user = new ColumnHeader();
                    ColumnHeader program = new ColumnHeader();
                    ColumnHeader status = new ColumnHeader();
                    ColumnHeader loggedon = new ColumnHeader();
                    ColumnHeader type = new ColumnHeader();
                    ColumnHeader osuser = new ColumnHeader();
                    // sett the columns
                    sid.Text = GuiHelper.GetResourceMsg("PM_SID");
                    sid.Width = 0;
                    serial.Text = GuiHelper.GetResourceMsg("PM_Serial");
                    serial.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
                    pid.Text = GuiHelper.GetResourceMsg("PM_PID");
                    pid.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
                    pid.Width = 75;
                    user.Text = GuiHelper.GetResourceMsg("PM_User");
                    user.Width = 100;
                    program.Text = GuiHelper.GetResourceMsg("PM_Program");
                    program.Width = 150;
                    status.Text = GuiHelper.GetResourceMsg("PM_Status");
                    status.Width = 75;
                    loggedon.Text = GuiHelper.GetResourceMsg("PM_Logontime");
                    loggedon.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
                    loggedon.Width = 125;
                    type.Text = GuiHelper.GetResourceMsg("PM_Type");
                    type.Width = 100;
                    osuser.Text = GuiHelper.GetResourceMsg("PM_OSUser");
                    osuser.Width = 150;
                    this._lvProcesses.Columns.AddRange(
                        new ColumnHeader[] { sid, serial, pid, user, program, status, loggedon, type, osuser});
                    break;
                case DBType.MSSql:
                    this._lvProcesses.Clear();
                    ColumnHeader id = new ColumnHeader();
                    ColumnHeader login = new ColumnHeader();
                    ColumnHeader host = new ColumnHeader();
                    ColumnHeader db = new ColumnHeader();
                    ColumnHeader cmd = new ColumnHeader();
                    ColumnHeader cputime = new ColumnHeader();
                    ColumnHeader state = new ColumnHeader();
                    ColumnHeader diskio = new ColumnHeader();
                    // sett the columns
                    id.Text = GuiHelper.GetResourceMsg("PMID");
                    id.Width = 50;
                    login.Text = GuiHelper.GetResourceMsg("PM_Login");
                    login.Width = 75;
                    host.Text = GuiHelper.GetResourceMsg("PM_Host");
                    host.Width = 125;
                    db.Text = GuiHelper.GetResourceMsg("PM_Database");
                    db.Width = 100;
                    cmd.Text = GuiHelper.GetResourceMsg("PM_Command");
                    cmd.Width = 150;
                    cputime.Text = GuiHelper.GetResourceMsg("PMCPUTime");
                    cputime.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
                    cputime.Width = 100;
                    state.Text = GuiHelper.GetResourceMsg("PM_State");
                    state.Width = 100;
                    this._lvProcesses.Columns.AddRange(new ColumnHeader[] { id, login, host, db, cmd, state, cputime, diskio});
                    break;
            }
        }
    	private void ProcessMng_Load(object sender, System.EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
            // init gui
            this._lvProcesses.Enabled = false;
            // building db connection
            if (dbh.ConnectDB(NyxMain.ConProfile, false))
            {	// gui
                this._lvProcesses.Enabled = true;
                this._msRefresh.Enabled = true;
                this._msRefresh.Select();
                // getting data
                GetProc();
            }
            else
            {	// closing 
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, 
                    dbh.ErrExc.Message.ToString() }), dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                this.Close();
            }
		}
        private void FrmProcessMng_FormClosing(object sender, FormClosingEventArgs e)
        {
            dbh.DisconnectDB();
        }

        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _msKill_Click(object sender, EventArgs e)
        {
            ListView.SelectedListViewItemCollection lviColl = this._lvProcesses.SelectedItems;
            if (lviColl.Count == 1)
            {
                ListViewItem lvi = lviColl[0];
                // really kill?
                StructRetDialog strRD = 
                    MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("PM_KillSession", new string[] { lvi.SubItems[0].Text }),
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                if (strRD.DRes == NyxDialogResult.Yes)
                {   // building query
                    string qry = String.Empty;
                    switch (NyxMain.ConProfile.ProfileDBType)
                    {
                        case DBType.MySql: qry = "kill '" + lvi.SubItems[0].Text + "'"; break;
                        case DBType.Oracle: qry = "ALTER SYSTEM KILL SESSION '" + lvi.SubItems[0].Text + "," + lvi.SubItems[1].Text + "'"; break;
                        case DBType.MSSql: qry = "KILL " + lvi.SubItems[0].Text + ""; break;
                    }
                    if (dbh.ExecuteNonQuery(qry))
                        GetProc();
                    else
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrProcessKill", new string[] { lvi.SubItems[0].Text, dbh.ErrExc.Message.ToString() }),
                            dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
            } 
        }
        private void _msRefresh_Click(object sender, EventArgs e)
        {
            GetProc();
        }
        private void _lvProcesses_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._lvProcesses.SelectedItems.Count == 1)
                this._msKill.Enabled = true;
            else
                this._msKill.Enabled = false;
        }
        private void GetProc()
        {   // init vars and chekc query
            string qry = null;
            ListViewItem lvi = null;
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql: qry = "SHOW FULL PROCESSLIST"; break;
                case DBType.Oracle: qry = "SELECT sid, serial#, process, username, program, status, TO_CHAR(logon_time,'HH24:MI:SS DD.MM.YY') AS logon_time, type , osuser FROM v$session ORDER BY process DESC"; break;
                case DBType.MSSql: qry = "EXEC sp_who2"; break;
            }
            // clearing listview
            this._lvProcesses.BeginUpdate();
            this._lvProcesses.Items.Clear();
            // filling dataset
            if (dbh.ExecuteReader(qry))
            {
                try
                {
                    switch (NyxMain.ConProfile.ProfileDBType)
                    {
                        case DBType.MySql:
                            while (dbh.Read())
                            {
                                lvi = GuiHelper.BuildListViewItem(dbh.GetValue(0).ToString(),
                                        new string[] { dbh.GetValue(1).ToString(), 
                                        dbh.GetValue(2).ToString(), dbh.GetValue(3).ToString(), 
                                        dbh.GetValue(4).ToString(), dbh.GetValue(5).ToString(), 
                                        dbh.GetValue(6).ToString(), dbh.GetValue(7).ToString() });
                                this._lvProcesses.Items.Add(lvi);
                            }
                            break;
                        case DBType.Oracle:
                            while (dbh.Read())
                            {
                                lvi = GuiHelper.BuildListViewItem(dbh.GetValue(0).ToString(),
                                    new string[] { dbh.GetValue(1).ToString(),
                                    dbh.GetValue(2).ToString(), dbh.GetValue(3).ToString(),
                                    dbh.GetValue(4).ToString(), dbh.GetValue(5).ToString(),
                                    dbh.GetValue(6).ToString(), dbh.GetValue(7).ToString(),
                                    dbh.GetValue(8).ToString() });
                                this._lvProcesses.Items.Add(lvi);
                            }
                            break;
                        case DBType.MSSql:
                            while (dbh.Read())
                            {
                                lvi = GuiHelper.BuildListViewItem(dbh.GetValue(0).ToString(),
                                    new string[] { dbh.GetValue(2).ToString(),
                                dbh.GetValue(3).ToString(), dbh.GetValue(5).ToString(),
                                dbh.GetValue(6).ToString(), dbh.GetValue(1).ToString(),
                                dbh.GetValue(7).ToString(), dbh.GetValue(8).ToString() });
                                this._lvProcesses.Items.Add(lvi);
                            }
                            break;
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrProcessRetrieve", new string[] { exc.Message.ToString() }),
                        exc, MessageHandler.EnumMsgType.Warning);
                }
                finally { dbh.Close(); }
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrProcessRetrieve", new string[] { dbh.ErrExc.Message.ToString() }),
                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
            }
            // endupdate
            this._lvProcesses.EndUpdate();
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion
    }
}