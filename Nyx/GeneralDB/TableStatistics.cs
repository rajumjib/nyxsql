/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Nyx.Classes;
using NyxSettings;
using System.Windows.Forms;
using System.Collections.Generic;

namespace Nyx.GeneralDB
{
    /// <summary>
    /// Retrieves the Size and Rows from all Tables from inside the user schema
    /// </summary>
    public partial class TableStatistics : Form
    {
        private System.Threading.Thread doWork;
        private DBHelper dbh;

        /// <summary> TableStatistics </summary>
        public TableStatistics()
        {
            InitializeComponent();
            // format Datagridview
            this._dgResult = GuiHelper.DataGridViewFormat(NyxMain.UASett, this._dgResult);
            // init own
            dbh = new DBHelper(NyxMain.ConProfile.ProfileDBType);
            DataGridViewTextBoxColumn dgvc = new DataGridViewTextBoxColumn ();
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MSSql:
                    this._dgResult.Columns[2].HeaderText = "DataSize/IndexSize";
                    // add new column
                    dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    dgvc.Width = 100;
                    dgvc.HeaderText = "Unused/Reserved Space";
                    dgvc.Name = "Space";
                    
                    this._dgResult.Columns.Add(dgvc);
                    this.Width = 465;
                    break;
                case DBType.Oracle:
                    // add new column
                    dgvc.AutoSizeMode = DataGridViewAutoSizeColumnMode.None;
                    dgvc.Width = 100;
                    dgvc.HeaderText = "Tablefields";
                    dgvc.Name = "Tablefields";
                    this._dgResult.Columns.Add(dgvc);
                    this.Width = 465;
                    break;
            }
        }
        /// <summary> TableStatistics load </summary>
        private void TableStatistics_Load(object sender, EventArgs e)
        {   // fading?
            if (NyxMain.UASett.FadeWindows)
                FadeWindow();
        }

        /// <summary> Thread start </summary>
        private void ThreadStart()
        {
            try
            {
                // init return structs
                DBThreadReturnParam strThrReturn = new DBThreadReturnParam();
                // connect to database
                if (dbh.ConnectDB(NyxMain.ConProfile, false))
                {   // get the stats / table
                    DBManagement dbm = new DBManagement(NyxMain.ConProfile.ProfileDBType);
                    System.Collections.Generic.Queue<TableStatistic> qTableStat;
                    if (dbm.GetTableStat(out qTableStat, dbh))
                        strThrReturn.Data1 = qTableStat;
                    else
                        strThrReturn.DBException = dbm.ErrExc;
                }
                else
                    strThrReturn.DBException = dbh.ErrExc;
                // disconnect again
                dbh.DisconnectDB();
                // invoke final method
                this.Invoke(new DBManagement.DbmThrdEndPDelegate(TsStartStatDone), strThrReturn);
            }
            catch (System.Threading.ThreadAbortException texc)
            {   // disconnect?
                dbh.DisconnectDB();
                // show messagehandler
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrThreadBreak", new string[] { texc.Message }),
                    dbh.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
                // invoke reset thread
                this.Invoke(new ThrdCom(ThreadAborted));
            }
        }
        /// <summary> Thread got aborted, here we have to handle it </summary>
        public void ThreadAborted()
        {   // set errortext
            AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrThreadBreak"), AppDialogs.MessageHandler.EnumMsgType.Warning);
            this.Close();
        }
        
        // gui
        private void _tsStart_Click(object sender, EventArgs e)
        {   // clean datagridview
            this._dgResult.Rows.Clear();
            // set toolstrip
            this._tsStart.Enabled = false;
            this._tsBreak.Enabled = true;
            this._btClose.Enabled = false;
            this._tsSave.Enabled = false;
            // progressbar and text
            this._ssPGBar.MarqueeAnimationSpeed = 100;
            this._ssPGBar.Visible = true;
            this._ssThrInf.Text = GuiHelper.GetResourceMsg("TStat_Retrieve");
            
            // start thread
            doWork = new System.Threading.Thread(new System.Threading.ThreadStart(ThreadStart));
            doWork.IsBackground = true;
            doWork.Start();
        }
        private void TsStartStatDone(DBThreadReturnParam strThrReturn)
        {   // cast
            if (strThrReturn.DBException == null)
            {   // fill listview
                object[] row;
                foreach (TableStatistic strTblStat in (Queue<TableStatistic>)strThrReturn.Data1)
                {
                    switch (NyxMain.ConProfile.ProfileDBType)
                    {
                        case DBType.MSSql:
                            row = new object[] { strTblStat.Name, 
                                strTblStat.Rows,
                                strTblStat.MSSize,
                                strTblStat.AddInfo };
                            break;
                        case DBType.Oracle:
                            row = new object[] { strTblStat.Name, 
                                strTblStat.Rows,
                                strTblStat.Size,
                                strTblStat.FieldCount };
                            break;
                        default:
                            row = new object[] { strTblStat.Name, 
                                strTblStat.Rows,
                                strTblStat.Size };
                            break;
                    }
                    this._dgResult.Rows.Add(row);
                }
                // gui
                this._tsStart.Enabled = true;
                this._tsBreak.Enabled = false;
                this._btClose.Enabled = true;
                if (this._dgResult.Rows.Count > 0)
                    this._tsSave.Enabled = true;
                else
                    this._tsSave.Enabled = false;
                // progressbar and text
                this._ssPGBar.MarqueeAnimationSpeed = 0;
                this._ssPGBar.Visible = false;
                this._ssThrInf.Text = String.Format(NyxMain.NyxCI, "{0}Done.", this._ssThrInf.Text);
            }
            else
            {   // err
                AppDialogs.MessageHandler.ShowDialog(dbh.ErrExc, AppDialogs.MessageHandler.EnumMsgType.Warning);
            }
        }
        private void _btClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        private void _tsBreak_Click(object sender, EventArgs e)
        {   // abort & reset gui
            if (doWork.ThreadState == System.Threading.ThreadState.Background)
            {
                doWork.Abort();
                // gui
                this._tsStart.Enabled = true;
                this._tsBreak.Enabled = false;
                this._btClose.Enabled = true;
                this._tsSave.Enabled = false;
                this._dgResult.Rows.Clear();
                // progressbar and text
                this._ssPGBar.MarqueeAnimationSpeed = 0;
                this._ssPGBar.Visible = false;
                this._ssThrInf.Text = GuiHelper.GetResourceMsg("TStat_RetrieveError");
            }
        }
        private void _tsSave_Click(object sender, EventArgs e)
        {
            ExportDataGridView.DataGridView2File(this._dgResult, "TableStatistics");
        }

        #region fading
        private Timer fadeTimer;
        private void FadeWindow()
        {   // change opacity
            this.Opacity = 0;
            // init timer
            fadeTimer = new Timer();
            fadeTimer.Interval = NyxMain.UASett.FadeWindowDelay;
            fadeTimer.Tick += new EventHandler(fadeTimer_Tick);
            fadeTimer.Enabled = true;
        }
        private void fadeTimer_Tick(object sender, EventArgs e)
        {
            if (this.Opacity < 1.0f)
                this.Opacity += 0.1f;
            else
                fadeTimer.Enabled = false;
        }
        #endregion

        private void TableStatistics_FormClosing(object sender, FormClosingEventArgs e)
        {   // be sure that the connection used is closed
            dbh.DisconnectDB();
        }
    }
}