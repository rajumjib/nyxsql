namespace Nyx.GeneralDB
{
    /// <summary>
    /// ProcessManager, can handle multiple databases
    /// </summary>
    partial class ProcessMng
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProcessMng));
            this._lvProcesses = new System.Windows.Forms.ListView();
            this.id = new System.Windows.Forms.ColumnHeader();
            this.user = new System.Windows.Forms.ColumnHeader();
            this.host = new System.Windows.Forms.ColumnHeader();
            this.db = new System.Windows.Forms.ColumnHeader();
            this.cmd = new System.Windows.Forms.ColumnHeader();
            this.time = new System.Windows.Forms.ColumnHeader();
            this.state = new System.Windows.Forms.ColumnHeader();
            this._ms = new System.Windows.Forms.MenuStrip();
            this._msRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this._msKill = new System.Windows.Forms.ToolStripMenuItem();
            this._tblLayoutPnl = new System.Windows.Forms.TableLayoutPanel();
            this._btClose = new System.Windows.Forms.Button();
            this._ms.SuspendLayout();
            this._tblLayoutPnl.SuspendLayout();
            this.SuspendLayout();
            // 
            // _lvProcesses
            // 
            this._lvProcesses.BackColor = System.Drawing.Color.GhostWhite;
            this._lvProcesses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.id,
            this.user,
            this.host,
            this.db,
            this.cmd,
            this.time,
            this.state});
            resources.ApplyResources(this._lvProcesses, "_lvProcesses");
            this._lvProcesses.FullRowSelect = true;
            this._lvProcesses.GridLines = true;
            this._lvProcesses.MultiSelect = false;
            this._lvProcesses.Name = "_lvProcesses";
            this._lvProcesses.UseCompatibleStateImageBehavior = false;
            this._lvProcesses.View = System.Windows.Forms.View.Details;
            this._lvProcesses.SelectedIndexChanged += new System.EventHandler(this._lvProcesses_SelectedIndexChanged);
            // 
            // id
            // 
            resources.ApplyResources(this.id, "id");
            // 
            // user
            // 
            resources.ApplyResources(this.user, "user");
            // 
            // host
            // 
            resources.ApplyResources(this.host, "host");
            // 
            // db
            // 
            resources.ApplyResources(this.db, "db");
            // 
            // cmd
            // 
            resources.ApplyResources(this.cmd, "cmd");
            // 
            // time
            // 
            resources.ApplyResources(this.time, "time");
            // 
            // state
            // 
            resources.ApplyResources(this.state, "state");
            // 
            // _ms
            // 
            this._ms.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msRefresh,
            this._msKill});
            resources.ApplyResources(this._ms, "_ms");
            this._ms.Name = "_ms";
            // 
            // _msRefresh
            // 
            this._msRefresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._msRefresh, "_msRefresh");
            this._msRefresh.Name = "_msRefresh";
            this._msRefresh.Click += new System.EventHandler(this._msRefresh_Click);
            // 
            // _msKill
            // 
            this._msKill.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._msKill, "_msKill");
            this._msKill.Name = "_msKill";
            this._msKill.Click += new System.EventHandler(this._msKill_Click);
            // 
            // _tblLayoutPnl
            // 
            resources.ApplyResources(this._tblLayoutPnl, "_tblLayoutPnl");
            this._tblLayoutPnl.Controls.Add(this._btClose, 0, 1);
            this._tblLayoutPnl.Controls.Add(this._lvProcesses, 0, 0);
            this._tblLayoutPnl.Name = "_tblLayoutPnl";
            // 
            // _btClose
            // 
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            this._btClose.Click += new System.EventHandler(this._btClose_Click);
            // 
            // ProcessMng
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this._btClose;
            this.Controls.Add(this._tblLayoutPnl);
            this.Controls.Add(this._ms);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "ProcessMng";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmProcessMng_FormClosing);
            this.Load += new System.EventHandler(this.ProcessMng_Load);
            this._ms.ResumeLayout(false);
            this._ms.PerformLayout();
            this._tblLayoutPnl.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView _lvProcesses;
        private System.Windows.Forms.ColumnHeader user;
        private System.Windows.Forms.ColumnHeader host;
        private System.Windows.Forms.ColumnHeader db;
        private System.Windows.Forms.ColumnHeader cmd;
        private System.Windows.Forms.ColumnHeader time;
        private System.Windows.Forms.ColumnHeader state;
        private System.Windows.Forms.ColumnHeader id;
        private System.Windows.Forms.MenuStrip _ms;
        private System.Windows.Forms.ToolStripMenuItem _msRefresh;
        private System.Windows.Forms.ToolStripMenuItem _msKill;
        private System.Windows.Forms.TableLayoutPanel _tblLayoutPnl;
        private System.Windows.Forms.Button _btClose;
    }
}