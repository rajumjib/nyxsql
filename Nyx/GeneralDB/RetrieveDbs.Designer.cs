namespace Nyx.GeneralDB
{
    /// <summary>
    /// Retrieve Databases for MySql/MSSql
    /// </summary>
    partial class RetrieveDbs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RetrieveDbs));
            this._pgBar = new System.Windows.Forms.ProgressBar();
            this._lblRetrv = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // _pgBar
            // 
            resources.ApplyResources(this._pgBar, "_pgBar");
            this._pgBar.Name = "_pgBar";
            this._pgBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            // 
            // _lblRetrv
            // 
            resources.ApplyResources(this._lblRetrv, "_lblRetrv");
            this._lblRetrv.Name = "_lblRetrv";
            // 
            // RetrieveDbs
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._lblRetrv);
            this.Controls.Add(this._pgBar);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "RetrieveDbs";
            this.Load += new System.EventHandler(this.RetrieveDbs_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar _pgBar;
        private System.Windows.Forms.Label _lblRetrv;
    }
}