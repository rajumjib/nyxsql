namespace Nyx
{
    partial class NyxMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(NyxMain));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this._tsCont = new System.Windows.Forms.ToolStripContainer();
            this._tcMain = new System.Windows.Forms.TabControl();
            this._tpSql = new System.Windows.Forms.TabPage();
            this._splitSqlBldr = new System.Windows.Forms.SplitContainer();
            this._tcSql = new System.Windows.Forms.TabControl();
            this._trvSqlBldr = new System.Windows.Forms.TreeView();
            this._cmSqlBldr = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmSqlBldr_Insert = new System.Windows.Forms.ToolStripMenuItem();
            this._cmSqlBldr_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmSqlBldr_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._cbSqlHistory = new System.Windows.Forms.ComboBox();
            this._tpPreview = new System.Windows.Forms.TabPage();
            this._pnlPreview = new System.Windows.Forms.Panel();
            this._splitPreview = new System.Windows.Forms.SplitContainer();
            this._lvPrvObjects = new System.Windows.Forms.ListView();
            this.lvcPrvObjTable = new System.Windows.Forms.ColumnHeader();
            this.lvcPrvObjType = new System.Windows.Forms.ColumnHeader();
            this._cmPrvObj = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmPrvObj_Get = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObj_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmPrvObj_DropView = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObj_DropTable = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObj_Truncate = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObjDiag = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObjDiag_Check = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObjDiag_Analyze = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObjDiag_Optimize = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObjDiag_Repair = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvObj_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._cmPrvObj_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._tsPrvObj = new System.Windows.Forms.ToolStrip();
            this._tsPrvObj_Refresh = new System.Windows.Forms.ToolStripButton();
            this._tsPrvObj_lblFilter = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvObj_Filter = new System.Windows.Forms.ToolStripTextBox();
            this._tsPrvObj_lblScope = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvObj_Scope = new System.Windows.Forms.ToolStripComboBox();
            this._splitPreviewDta = new System.Windows.Forms.SplitContainer();
            this._dgPreview = new System.Windows.Forms.DataGridView();
            this._cmPrvDta = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmPrvDta_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDta_Update = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDta_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._cmPrvDtaCp = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDtaCp_Field = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDtaCp_Cells = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDtaCp_Table = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDtaSv = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDtaSv_Cells = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDtaSv_Table = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvDta_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmPrvDta_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._dgPreview_Filter = new System.Windows.Forms.DataGridView();
            this.vField = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vSelect = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.vLOperator = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.vFilter = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.vValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vGroup = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this._cmPrvFilter = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmPrvFilter_Set = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvFilter_Reset = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvFilter_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmPrvFilter_FieldUp = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvFilter_FieldDown = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvFilter_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._cmPrvFilter_DupField = new System.Windows.Forms.ToolStripMenuItem();
            this._cmPrvFilter_RmDupField = new System.Windows.Forms.ToolStripMenuItem();
            this._tsPrvDta = new System.Windows.Forms.ToolStrip();
            this._tsPrvDta_Refresh = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrvDta_ShowFilter = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_SetFilter = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_ResetFilter = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_Sep7 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrvDta_Edit = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_Update = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrvDta_Start = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_Prev = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_Next = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_End = new System.Windows.Forms.ToolStripButton();
            this._tsPrvDta_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrvDta_RowsPerPage = new System.Windows.Forms.ToolStripComboBox();
            this._tsPrvDta_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrvDta_lblPage = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_lblActPg = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_lblDiv0 = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_lblPgCnt = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_Sep5 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrvDta_lblRows = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_lblActRows = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_lblDiv1 = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_lblRowCnt = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_Sep6 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrvDta_lblCurTable = new System.Windows.Forms.ToolStripLabel();
            this._tsPrvDta_curObject = new System.Windows.Forms.ToolStripLabel();
            this._lblNotSupportedPreview = new System.Windows.Forms.Label();
            this._tpTables = new System.Windows.Forms.TabPage();
            this._pnlTables = new System.Windows.Forms.Panel();
            this._splitTables = new System.Windows.Forms.SplitContainer();
            this._lbTables = new System.Windows.Forms.ListBox();
            this._cmTable = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmTable_Describe = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmTable_Create = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_Alter = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_Drop = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_Truncate = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_Diag = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTableDiag_Check = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTableDiag_Analyze = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTableDiag_OptReb = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTableDiag_Repair = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._cmTable_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this._cmTable_SelAll = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_SelNone = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTable_SelInv = new System.Windows.Forms.ToolStripMenuItem();
            this._splitIndexes = new System.Windows.Forms.SplitContainer();
            this._lvTableDesc = new System.Windows.Forms.ListView();
            this.field = new System.Windows.Forms.ColumnHeader();
            this.type = new System.Windows.Forms.ColumnHeader();
            this.length = new System.Windows.Forms.ColumnHeader();
            this.precission = new System.Windows.Forms.ColumnHeader();
            this._cmTblField = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmTblField_Create = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTblField_Alter = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTblField_Drop = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTblField_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmTblField_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTblField_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._cmTblField_SelAll = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTblField_SelNone = new System.Windows.Forms.ToolStripMenuItem();
            this._cmTblField_SelInv = new System.Windows.Forms.ToolStripMenuItem();
            this._splitIndexDetails = new System.Windows.Forms.SplitContainer();
            this._lvIndexes = new System.Windows.Forms.ListView();
            this.idxname = new System.Windows.Forms.ColumnHeader();
            this.tblname = new System.Windows.Forms.ColumnHeader();
            this.tblstatus = new System.Windows.Forms.ColumnHeader();
            this.analyse = new System.Windows.Forms.ColumnHeader();
            this._cmIndex = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmIndex_Describe = new System.Windows.Forms.ToolStripMenuItem();
            this._cmIndex_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmIndex_Create = new System.Windows.Forms.ToolStripMenuItem();
            this._cmIndex_Alter = new System.Windows.Forms.ToolStripMenuItem();
            this._cmIndex_Drop = new System.Windows.Forms.ToolStripMenuItem();
            this._cmIndex_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._cmIndex_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._cmIndex_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this._cmIndex_SelAll = new System.Windows.Forms.ToolStripMenuItem();
            this._cmIndex_SelNone = new System.Windows.Forms.ToolStripMenuItem();
            this._cmIndex_SelInv = new System.Windows.Forms.ToolStripMenuItem();
            this._lvIndexDesc = new System.Windows.Forms.ListView();
            this.cname = new System.Windows.Forms.ColumnHeader();
            this.dtype = new System.Windows.Forms.ColumnHeader();
            this.cpos = new System.Windows.Forms.ColumnHeader();
            this._tsTables = new System.Windows.Forms.ToolStrip();
            this._tsTables_Refresh = new System.Windows.Forms.ToolStripButton();
            this._tsTables_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tsTables_lblFilter = new System.Windows.Forms.ToolStripLabel();
            this._tsTables_Filter = new System.Windows.Forms.ToolStripTextBox();
            this._tsTables_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this._tsTables_lblDescTable = new System.Windows.Forms.ToolStripLabel();
            this._tsTables_tbDescTable = new System.Windows.Forms.ToolStripTextBox();
            this._tsTables_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsTables_lblRetrieve = new System.Windows.Forms.ToolStripLabel();
            this._tsTables_cbScope = new System.Windows.Forms.ToolStripComboBox();
            this._tsTables_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this._tsTables_lblcurTable = new System.Windows.Forms.ToolStripLabel();
            this._tsTables_curTable = new System.Windows.Forms.ToolStripLabel();
            this._lblNotSupportedTables = new System.Windows.Forms.Label();
            this._tpViews = new System.Windows.Forms.TabPage();
            this._pnlViews = new System.Windows.Forms.Panel();
            this._splitViews = new System.Windows.Forms.SplitContainer();
            this._lbViews = new System.Windows.Forms.ListBox();
            this._cmViews = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmViews_Create = new System.Windows.Forms.ToolStripMenuItem();
            this._cmViews_Drop = new System.Windows.Forms.ToolStripMenuItem();
            this._cmViews_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmViews_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._splitViews2 = new System.Windows.Forms.SplitContainer();
            this._lvView_Details = new System.Windows.Forms.ListView();
            this._lvcView_Variable = new System.Windows.Forms.ColumnHeader();
            this._lvcView_Value = new System.Windows.Forms.ColumnHeader();
            this._rtbView_CreateStm = new System.Windows.Forms.RichTextBox();
            this._tsViews = new System.Windows.Forms.ToolStrip();
            this._tsViews_Refresh = new System.Windows.Forms.ToolStripButton();
            this._tsViews_Create = new System.Windows.Forms.ToolStripButton();
            this._tsViews_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tsViews_lblFilter = new System.Windows.Forms.ToolStripLabel();
            this._tsViews_Filter = new System.Windows.Forms.ToolStripTextBox();
            this._tsViews_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsViews_lblRetrieve = new System.Windows.Forms.ToolStripLabel();
            this._tsViews_cbScopeve = new System.Windows.Forms.ToolStripComboBox();
            this._tsViews_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this._tsViews_lblCurTable = new System.Windows.Forms.ToolStripLabel();
            this._tsViews_curView = new System.Windows.Forms.ToolStripLabel();
            this._lblNotSupportedViews = new System.Windows.Forms.Label();
            this._tpProcedures = new System.Windows.Forms.TabPage();
            this._pnlProcedures = new System.Windows.Forms.Panel();
            this._splitProcedures = new System.Windows.Forms.SplitContainer();
            this._lbPrcdrs = new System.Windows.Forms.ListBox();
            this._splitProcedures2 = new System.Windows.Forms.SplitContainer();
            this._lvPrcdr_Details = new System.Windows.Forms.ListView();
            this._lvcPrcdrs_Variable = new System.Windows.Forms.ColumnHeader();
            this._lvcPrcdrs_Value = new System.Windows.Forms.ColumnHeader();
            this._rtbPrcdr_CreateStm = new System.Windows.Forms.RichTextBox();
            this._tsPrcdrs = new System.Windows.Forms.ToolStrip();
            this._tsPrcdrs_Refresh = new System.Windows.Forms.ToolStripButton();
            this._tsPrcdrs_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrcdrs_lblFilter = new System.Windows.Forms.ToolStripLabel();
            this._tsPrcdrs_Filter = new System.Windows.Forms.ToolStripTextBox();
            this._tsPrcdrs_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrcdrs_lblRetrieve = new System.Windows.Forms.ToolStripLabel();
            this._tsPrcdrs_cbScope = new System.Windows.Forms.ToolStripComboBox();
            this._tsPrcdrs_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this._tsPrcdrs_lblCurTable = new System.Windows.Forms.ToolStripLabel();
            this._tsPrcdrs_curProcedure = new System.Windows.Forms.ToolStripLabel();
            this._lblNotSupportedProcedures = new System.Windows.Forms.Label();
            this._tpTriggers = new System.Windows.Forms.TabPage();
            this._pnlTriggers = new System.Windows.Forms.Panel();
            this._splitTriggers = new System.Windows.Forms.SplitContainer();
            this._lbTriggers = new System.Windows.Forms.ListBox();
            this._splitTriggers2 = new System.Windows.Forms.SplitContainer();
            this._lvTrigger_Details = new System.Windows.Forms.ListView();
            this._lvcTrigger_Variable = new System.Windows.Forms.ColumnHeader();
            this._lvcTrigger_Value = new System.Windows.Forms.ColumnHeader();
            this._rtbTrigger_CrtStm = new System.Windows.Forms.RichTextBox();
            this._tsTriggers = new System.Windows.Forms.ToolStrip();
            this._tsTriggers_Refresh = new System.Windows.Forms.ToolStripButton();
            this._tsTriggers_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tsTriggers_lblFilter = new System.Windows.Forms.ToolStripLabel();
            this._tsTriggers_Filter = new System.Windows.Forms.ToolStripTextBox();
            this._tsTriggers_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._tsTriggers_lblRetrieve = new System.Windows.Forms.ToolStripLabel();
            this._tsTriggers_cbScope = new System.Windows.Forms.ToolStripComboBox();
            this._tsTriggers_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this._tsTriggers_lblCurTable = new System.Windows.Forms.ToolStripLabel();
            this._tsTriggers_curTriggers = new System.Windows.Forms.ToolStripLabel();
            this._lblNotSupportedTriggers = new System.Windows.Forms.Label();
            this._tsMain = new System.Windows.Forms.ToolStrip();
            this._tsMain_SaveQC = new System.Windows.Forms.ToolStripButton();
            this._tsMain_QuickConnect = new System.Windows.Forms.ToolStripButton();
            this._tsMain_cbProfiles = new System.Windows.Forms.ToolStripComboBox();
            this._tsMain_Connect = new System.Windows.Forms.ToolStripButton();
            this._tsMain_Disconnect = new System.Windows.Forms.ToolStripButton();
            this._tsMain_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._tsMain_cbChangeDb = new System.Windows.Forms.ToolStripComboBox();
            this.sql1 = new System.Windows.Forms.TabPage();
            this._menu = new System.Windows.Forms.MenuStrip();
            this._msFile = new System.Windows.Forms.ToolStripMenuItem();
            this._msFile_Connect = new System.Windows.Forms.ToolStripMenuItem();
            this._msFile_Disconnect = new System.Windows.Forms.ToolStripMenuItem();
            this._msFile_QuickConnect = new System.Windows.Forms.ToolStripMenuItem();
            this._msFile_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this._msFile_Profiles = new System.Windows.Forms.ToolStripMenuItem();
            this._msFile_Settings = new System.Windows.Forms.ToolStripMenuItem();
            this._msFile_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._msFile_ChkUpdate = new System.Windows.Forms.ToolStripMenuItem();
            this._msFile_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._msFile_Exit = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_EditData = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_UpdData = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_Sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._msSql_ExecSql = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_ExecSel = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_Sep2 = new System.Windows.Forms.ToolStripSeparator();
            this._msSql_ClearSql = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_ClearHistory = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_Sep3 = new System.Windows.Forms.ToolStripSeparator();
            this._msSql_SqlTabs = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_SqlTabs_NewTab = new System.Windows.Forms.ToolStripMenuItem();
            this._msSql_SqlTabs_CloseTab = new System.Windows.Forms.ToolStripMenuItem();
            this._msPrv = new System.Windows.Forms.ToolStripMenuItem();
            this._msPrvObj_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._msPrvDta_Refresh = new System.Windows.Forms.ToolStripMenuItem();
            this._msPrv_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._msPrvDta_Edit = new System.Windows.Forms.ToolStripMenuItem();
            this._msPrv_Update = new System.Windows.Forms.ToolStripMenuItem();
            this._msView = new System.Windows.Forms.ToolStripMenuItem();
            this._msView_Sql = new System.Windows.Forms.ToolStripMenuItem();
            this._msView_Tables = new System.Windows.Forms.ToolStripMenuItem();
            this._msView_Preview = new System.Windows.Forms.ToolStripMenuItem();
            this._msView_Views = new System.Windows.Forms.ToolStripMenuItem();
            this._msView_Prcdrs = new System.Windows.Forms.ToolStripMenuItem();
            this._msView_Triggers = new System.Windows.Forms.ToolStripMenuItem();
            this._msView_sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._msViewTStrips = new System.Windows.Forms.ToolStripMenuItem();
            this._msViewTStrips_Main = new System.Windows.Forms.ToolStripMenuItem();
            this._msView_sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._msView_SysTray = new System.Windows.Forms.ToolStripMenuItem();
            this._msBookmarks = new System.Windows.Forms.ToolStripMenuItem();
            this._msBookmarks_Mng = new System.Windows.Forms.ToolStripMenuItem();
            this._msBookmarks_New = new System.Windows.Forms.ToolStripMenuItem();
            this._msBookmarks_NewEntry = new System.Windows.Forms.ToolStripMenuItem();
            this._msBookmarks_NewCat = new System.Windows.Forms.ToolStripMenuItem();
            this._msBookmarks_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._msDbT = new System.Windows.Forms.ToolStripMenuItem();
            this._msDbT_Processes = new System.Windows.Forms.ToolStripMenuItem();
            this._msDbT_TableStats = new System.Windows.Forms.ToolStripMenuItem();
            this._msDbT_Sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._msDbT_SqlBldr = new System.Windows.Forms.ToolStripMenuItem();
            this._msDbT_ShowAC = new System.Windows.Forms.ToolStripMenuItem();
            this._msOrcl = new System.Windows.Forms.ToolStripMenuItem();
            this._msOrcl_Privileges = new System.Windows.Forms.ToolStripMenuItem();
            this._msOrcl_RecBin = new System.Windows.Forms.ToolStripMenuItem();
            this._msOrcl_sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._msOrcl_InitInfo = new System.Windows.Forms.ToolStripMenuItem();
            this._msOrcl_DbInfo = new System.Windows.Forms.ToolStripMenuItem();
            this._msOrcl_TSInfo = new System.Windows.Forms.ToolStripMenuItem();
            this._msMySql = new System.Windows.Forms.ToolStripMenuItem();
            this._msMySql_Privileges = new System.Windows.Forms.ToolStripMenuItem();
            this._msMySql_sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._msMySql_Import = new System.Windows.Forms.ToolStripMenuItem();
            this._msMySql_Export = new System.Windows.Forms.ToolStripMenuItem();
            this._msMySql_sep1 = new System.Windows.Forms.ToolStripSeparator();
            this._msMySql_CrtDb = new System.Windows.Forms.ToolStripMenuItem();
            this._msMySql_DropDb = new System.Windows.Forms.ToolStripMenuItem();
            this._msMySql_sep2 = new System.Windows.Forms.ToolStripSeparator();
            this._msMySql_DbInfo = new System.Windows.Forms.ToolStripMenuItem();
            this._msHelp = new System.Windows.Forms.ToolStripMenuItem();
            this._msHelp_About = new System.Windows.Forms.ToolStripMenuItem();
            this._tsep5 = new System.Windows.Forms.ToolStripSeparator();
            this._ss = new System.Windows.Forms.StatusStrip();
            this._ssProgress = new System.Windows.Forms.ToolStripProgressBar();
            this._sS_lblStatusMsg = new System.Windows.Forms.ToolStripStatusLabel();
            this._NI = new System.Windows.Forms.NotifyIcon(this.components);
            this._cmNI = new System.Windows.Forms.ContextMenuStrip(this.components);
            this._cmNI_show = new System.Windows.Forms.ToolStripMenuItem();
            this._cmNI_hide = new System.Windows.Forms.ToolStripMenuItem();
            this._cmNI_sep0 = new System.Windows.Forms.ToolStripSeparator();
            this._cmNI_exit = new System.Windows.Forms.ToolStripMenuItem();
            this._ilAutoComplete = new System.Windows.Forms.ImageList(this.components);
            this._tsCont.ContentPanel.SuspendLayout();
            this._tsCont.TopToolStripPanel.SuspendLayout();
            this._tsCont.SuspendLayout();
            this._tcMain.SuspendLayout();
            this._tpSql.SuspendLayout();
            this._splitSqlBldr.Panel1.SuspendLayout();
            this._splitSqlBldr.Panel2.SuspendLayout();
            this._splitSqlBldr.SuspendLayout();
            this._cmSqlBldr.SuspendLayout();
            this._tpPreview.SuspendLayout();
            this._pnlPreview.SuspendLayout();
            this._splitPreview.Panel1.SuspendLayout();
            this._splitPreview.Panel2.SuspendLayout();
            this._splitPreview.SuspendLayout();
            this._cmPrvObj.SuspendLayout();
            this._tsPrvObj.SuspendLayout();
            this._splitPreviewDta.Panel1.SuspendLayout();
            this._splitPreviewDta.Panel2.SuspendLayout();
            this._splitPreviewDta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgPreview)).BeginInit();
            this._cmPrvDta.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._dgPreview_Filter)).BeginInit();
            this._cmPrvFilter.SuspendLayout();
            this._tsPrvDta.SuspendLayout();
            this._tpTables.SuspendLayout();
            this._pnlTables.SuspendLayout();
            this._splitTables.Panel1.SuspendLayout();
            this._splitTables.Panel2.SuspendLayout();
            this._splitTables.SuspendLayout();
            this._cmTable.SuspendLayout();
            this._splitIndexes.Panel1.SuspendLayout();
            this._splitIndexes.Panel2.SuspendLayout();
            this._splitIndexes.SuspendLayout();
            this._cmTblField.SuspendLayout();
            this._splitIndexDetails.Panel1.SuspendLayout();
            this._splitIndexDetails.Panel2.SuspendLayout();
            this._splitIndexDetails.SuspendLayout();
            this._cmIndex.SuspendLayout();
            this._tsTables.SuspendLayout();
            this._tpViews.SuspendLayout();
            this._pnlViews.SuspendLayout();
            this._splitViews.Panel1.SuspendLayout();
            this._splitViews.Panel2.SuspendLayout();
            this._splitViews.SuspendLayout();
            this._cmViews.SuspendLayout();
            this._splitViews2.Panel1.SuspendLayout();
            this._splitViews2.Panel2.SuspendLayout();
            this._splitViews2.SuspendLayout();
            this._tsViews.SuspendLayout();
            this._tpProcedures.SuspendLayout();
            this._pnlProcedures.SuspendLayout();
            this._splitProcedures.Panel1.SuspendLayout();
            this._splitProcedures.Panel2.SuspendLayout();
            this._splitProcedures.SuspendLayout();
            this._splitProcedures2.Panel1.SuspendLayout();
            this._splitProcedures2.Panel2.SuspendLayout();
            this._splitProcedures2.SuspendLayout();
            this._tsPrcdrs.SuspendLayout();
            this._tpTriggers.SuspendLayout();
            this._pnlTriggers.SuspendLayout();
            this._splitTriggers.Panel1.SuspendLayout();
            this._splitTriggers.Panel2.SuspendLayout();
            this._splitTriggers.SuspendLayout();
            this._splitTriggers2.Panel1.SuspendLayout();
            this._splitTriggers2.Panel2.SuspendLayout();
            this._splitTriggers2.SuspendLayout();
            this._tsTriggers.SuspendLayout();
            this._tsMain.SuspendLayout();
            this._menu.SuspendLayout();
            this._ss.SuspendLayout();
            this._cmNI.SuspendLayout();
            this.SuspendLayout();
            // 
            // _tsCont
            // 
            this._tsCont.BottomToolStripPanelVisible = false;
            // 
            // _tsCont.ContentPanel
            // 
            this._tsCont.ContentPanel.BackColor = System.Drawing.Color.Transparent;
            this._tsCont.ContentPanel.Controls.Add(this._tcMain);
            resources.ApplyResources(this._tsCont.ContentPanel, "_tsCont.ContentPanel");
            resources.ApplyResources(this._tsCont, "_tsCont");
            this._tsCont.LeftToolStripPanelVisible = false;
            this._tsCont.Name = "_tsCont";
            this._tsCont.RightToolStripPanelVisible = false;
            // 
            // _tsCont.TopToolStripPanel
            // 
            this._tsCont.TopToolStripPanel.Controls.Add(this._tsMain);
            // 
            // _tcMain
            // 
            this._tcMain.Controls.Add(this._tpSql);
            this._tcMain.Controls.Add(this._tpPreview);
            this._tcMain.Controls.Add(this._tpTables);
            this._tcMain.Controls.Add(this._tpViews);
            this._tcMain.Controls.Add(this._tpProcedures);
            this._tcMain.Controls.Add(this._tpTriggers);
            resources.ApplyResources(this._tcMain, "_tcMain");
            this._tcMain.HotTrack = true;
            this._tcMain.Name = "_tcMain";
            this._tcMain.SelectedIndex = 0;
            this._tcMain.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this._tcMain.SelectedIndexChanged += new System.EventHandler(this._tcMain_SelectedIndexChanged);
            // 
            // _tpSql
            // 
            this._tpSql.Controls.Add(this._splitSqlBldr);
            this._tpSql.Controls.Add(this._cbSqlHistory);
            resources.ApplyResources(this._tpSql, "_tpSql");
            this._tpSql.Name = "_tpSql";
            this._tpSql.UseVisualStyleBackColor = true;
            // 
            // _splitSqlBldr
            // 
            this._splitSqlBldr.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            resources.ApplyResources(this._splitSqlBldr, "_splitSqlBldr");
            this._splitSqlBldr.Name = "_splitSqlBldr";
            // 
            // _splitSqlBldr.Panel1
            // 
            this._splitSqlBldr.Panel1.Controls.Add(this._tcSql);
            // 
            // _splitSqlBldr.Panel2
            // 
            this._splitSqlBldr.Panel2.Controls.Add(this._trvSqlBldr);
            this._splitSqlBldr.Panel2Collapsed = true;
            // 
            // _tcSql
            // 
            resources.ApplyResources(this._tcSql, "_tcSql");
            this._tcSql.HotTrack = true;
            this._tcSql.Multiline = true;
            this._tcSql.Name = "_tcSql";
            this._tcSql.SelectedIndex = 0;
            // 
            // _trvSqlBldr
            // 
            this._trvSqlBldr.BackColor = System.Drawing.Color.GhostWhite;
            this._trvSqlBldr.ContextMenuStrip = this._cmSqlBldr;
            resources.ApplyResources(this._trvSqlBldr, "_trvSqlBldr");
            this._trvSqlBldr.HideSelection = false;
            this._trvSqlBldr.HotTracking = true;
            this._trvSqlBldr.Name = "_trvSqlBldr";
            this._trvSqlBldr.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            ((System.Windows.Forms.TreeNode)(resources.GetObject("_trvSqlBldr.Nodes"))),
            ((System.Windows.Forms.TreeNode)(resources.GetObject("_trvSqlBldr.Nodes1"))),
            ((System.Windows.Forms.TreeNode)(resources.GetObject("_trvSqlBldr.Nodes2"))),
            ((System.Windows.Forms.TreeNode)(resources.GetObject("_trvSqlBldr.Nodes3")))});
            this._trvSqlBldr.ShowNodeToolTips = true;
            this._trvSqlBldr.NodeMouseDoubleClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this._trvSqlBldr_NodeMouseDoubleClick);
            this._trvSqlBldr.MouseClick += new System.Windows.Forms.MouseEventHandler(this._trvSqlBldr_MouseClick);
            this._trvSqlBldr.KeyUp += new System.Windows.Forms.KeyEventHandler(this._trvSqlBldr_KeyUp);
            this._trvSqlBldr.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this._trvSqlBldr_AfterExpand);
            // 
            // _cmSqlBldr
            // 
            this._cmSqlBldr.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmSqlBldr_Insert,
            this._cmSqlBldr_Sep0,
            this._cmSqlBldr_Refresh});
            this._cmSqlBldr.Name = "_cmsqlbldr";
            resources.ApplyResources(this._cmSqlBldr, "_cmSqlBldr");
            // 
            // _cmSqlBldr_Insert
            // 
            this._cmSqlBldr_Insert.Image = global::Nyx.Properties.Resources.database_edit;
            resources.ApplyResources(this._cmSqlBldr_Insert, "_cmSqlBldr_Insert");
            this._cmSqlBldr_Insert.Name = "_cmSqlBldr_Insert";
            this._cmSqlBldr_Insert.Click += new System.EventHandler(this._cmSqlBldr_Insert_Click);
            // 
            // _cmSqlBldr_Sep0
            // 
            this._cmSqlBldr_Sep0.Name = "_cmSqlBldr_Sep0";
            resources.ApplyResources(this._cmSqlBldr_Sep0, "_cmSqlBldr_Sep0");
            // 
            // _cmSqlBldr_Refresh
            // 
            this._cmSqlBldr_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._cmSqlBldr_Refresh, "_cmSqlBldr_Refresh");
            this._cmSqlBldr_Refresh.Name = "_cmSqlBldr_Refresh";
            this._cmSqlBldr_Refresh.Click += new System.EventHandler(this._cmSqlBldr_Refresh_Click);
            // 
            // _cbSqlHistory
            // 
            this._cbSqlHistory.BackColor = System.Drawing.Color.GhostWhite;
            this._cbSqlHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            resources.ApplyResources(this._cbSqlHistory, "_cbSqlHistory");
            this._cbSqlHistory.DropDownHeight = 200;
            this._cbSqlHistory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._cbSqlHistory.Items.AddRange(new object[] {
            resources.GetString("_cbSqlHistory.Items"),
            resources.GetString("_cbSqlHistory.Items1")});
            this._cbSqlHistory.Name = "_cbSqlHistory";
            this._cbSqlHistory.SelectedIndexChanged += new System.EventHandler(this._cbSqlHistory_SelectionChangeCommitted);
            // 
            // _tpPreview
            // 
            this._tpPreview.Controls.Add(this._pnlPreview);
            this._tpPreview.Controls.Add(this._lblNotSupportedPreview);
            resources.ApplyResources(this._tpPreview, "_tpPreview");
            this._tpPreview.Name = "_tpPreview";
            this._tpPreview.UseVisualStyleBackColor = true;
            // 
            // _pnlPreview
            // 
            this._pnlPreview.Controls.Add(this._splitPreview);
            resources.ApplyResources(this._pnlPreview, "_pnlPreview");
            this._pnlPreview.Name = "_pnlPreview";
            // 
            // _splitPreview
            // 
            resources.ApplyResources(this._splitPreview, "_splitPreview");
            this._splitPreview.Name = "_splitPreview";
            // 
            // _splitPreview.Panel1
            // 
            this._splitPreview.Panel1.Controls.Add(this._lvPrvObjects);
            this._splitPreview.Panel1.Controls.Add(this._tsPrvObj);
            // 
            // _splitPreview.Panel2
            // 
            this._splitPreview.Panel2.Controls.Add(this._splitPreviewDta);
            this._splitPreview.Panel2.Controls.Add(this._tsPrvDta);
            // 
            // _lvPrvObjects
            // 
            this._lvPrvObjects.BackColor = System.Drawing.Color.GhostWhite;
            this._lvPrvObjects.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.lvcPrvObjTable,
            this.lvcPrvObjType});
            this._lvPrvObjects.ContextMenuStrip = this._cmPrvObj;
            resources.ApplyResources(this._lvPrvObjects, "_lvPrvObjects");
            this._lvPrvObjects.FullRowSelect = true;
            this._lvPrvObjects.HideSelection = false;
            this._lvPrvObjects.MultiSelect = false;
            this._lvPrvObjects.Name = "_lvPrvObjects";
            this._lvPrvObjects.UseCompatibleStateImageBehavior = false;
            this._lvPrvObjects.View = System.Windows.Forms.View.Details;
            this._lvPrvObjects.DoubleClick += new System.EventHandler(this._previewInitPreview_Click);
            this._lvPrvObjects.SelectedIndexChanged += new System.EventHandler(this._lvPrvObjects_SelectedIndexChanged);
            this._lvPrvObjects.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvPrvObj_ColumnClick);
            // 
            // lvcPrvObjTable
            // 
            resources.ApplyResources(this.lvcPrvObjTable, "lvcPrvObjTable");
            // 
            // lvcPrvObjType
            // 
            resources.ApplyResources(this.lvcPrvObjType, "lvcPrvObjType");
            // 
            // _cmPrvObj
            // 
            this._cmPrvObj.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmPrvObj_Get,
            this._cmPrvObj_Sep0,
            this._cmPrvObj_DropView,
            this._cmPrvObj_DropTable,
            this._cmPrvObj_Truncate,
            this._cmPrvObjDiag,
            this._cmPrvObj_Sep1,
            this._cmPrvObj_Refresh});
            this._cmPrvObj.Name = "_cm3";
            resources.ApplyResources(this._cmPrvObj, "_cmPrvObj");
            // 
            // _cmPrvObj_Get
            // 
            resources.ApplyResources(this._cmPrvObj_Get, "_cmPrvObj_Get");
            this._cmPrvObj_Get.Image = global::Nyx.Properties.Resources.control_play_blue;
            this._cmPrvObj_Get.Name = "_cmPrvObj_Get";
            this._cmPrvObj_Get.Click += new System.EventHandler(this._previewInitPreview_Click);
            // 
            // _cmPrvObj_Sep0
            // 
            this._cmPrvObj_Sep0.Name = "_cmPrvObj_Sep0";
            resources.ApplyResources(this._cmPrvObj_Sep0, "_cmPrvObj_Sep0");
            // 
            // _cmPrvObj_DropView
            // 
            resources.ApplyResources(this._cmPrvObj_DropView, "_cmPrvObj_DropView");
            this._cmPrvObj_DropView.Image = global::Nyx.Properties.Resources.table_delete;
            this._cmPrvObj_DropView.Name = "_cmPrvObj_DropView";
            this._cmPrvObj_DropView.Click += new System.EventHandler(this._cmPrvObj_Actions);
            // 
            // _cmPrvObj_DropTable
            // 
            resources.ApplyResources(this._cmPrvObj_DropTable, "_cmPrvObj_DropTable");
            this._cmPrvObj_DropTable.Image = global::Nyx.Properties.Resources.table_delete;
            this._cmPrvObj_DropTable.Name = "_cmPrvObj_DropTable";
            this._cmPrvObj_DropTable.Click += new System.EventHandler(this._cmPrvObj_Actions);
            // 
            // _cmPrvObj_Truncate
            // 
            resources.ApplyResources(this._cmPrvObj_Truncate, "_cmPrvObj_Truncate");
            this._cmPrvObj_Truncate.Image = global::Nyx.Properties.Resources.delete_blackwhite;
            this._cmPrvObj_Truncate.Name = "_cmPrvObj_Truncate";
            this._cmPrvObj_Truncate.Click += new System.EventHandler(this._cmPrvObj_Actions);
            // 
            // _cmPrvObjDiag
            // 
            this._cmPrvObjDiag.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmPrvObjDiag_Check,
            this._cmPrvObjDiag_Analyze,
            this._cmPrvObjDiag_Optimize,
            this._cmPrvObjDiag_Repair});
            this._cmPrvObjDiag.Image = global::Nyx.Properties.Resources.table_gear;
            resources.ApplyResources(this._cmPrvObjDiag, "_cmPrvObjDiag");
            this._cmPrvObjDiag.Name = "_cmPrvObjDiag";
            // 
            // _cmPrvObjDiag_Check
            // 
            resources.ApplyResources(this._cmPrvObjDiag_Check, "_cmPrvObjDiag_Check");
            this._cmPrvObjDiag_Check.Image = global::Nyx.Properties.Resources.table_key;
            this._cmPrvObjDiag_Check.Name = "_cmPrvObjDiag_Check";
            this._cmPrvObjDiag_Check.Click += new System.EventHandler(this._cmPrvObj_Actions);
            // 
            // _cmPrvObjDiag_Analyze
            // 
            resources.ApplyResources(this._cmPrvObjDiag_Analyze, "_cmPrvObjDiag_Analyze");
            this._cmPrvObjDiag_Analyze.Image = global::Nyx.Properties.Resources.table_lightning;
            this._cmPrvObjDiag_Analyze.Name = "_cmPrvObjDiag_Analyze";
            this._cmPrvObjDiag_Analyze.Click += new System.EventHandler(this._cmPrvObj_Actions);
            // 
            // _cmPrvObjDiag_Optimize
            // 
            resources.ApplyResources(this._cmPrvObjDiag_Optimize, "_cmPrvObjDiag_Optimize");
            this._cmPrvObjDiag_Optimize.Image = global::Nyx.Properties.Resources.table_gear;
            this._cmPrvObjDiag_Optimize.Name = "_cmPrvObjDiag_Optimize";
            this._cmPrvObjDiag_Optimize.Click += new System.EventHandler(this._cmPrvObj_Actions);
            // 
            // _cmPrvObjDiag_Repair
            // 
            resources.ApplyResources(this._cmPrvObjDiag_Repair, "_cmPrvObjDiag_Repair");
            this._cmPrvObjDiag_Repair.Image = global::Nyx.Properties.Resources.table_error;
            this._cmPrvObjDiag_Repair.Name = "_cmPrvObjDiag_Repair";
            this._cmPrvObjDiag_Repair.Click += new System.EventHandler(this._cmPrvObj_Actions);
            // 
            // _cmPrvObj_Sep1
            // 
            this._cmPrvObj_Sep1.Name = "_cmPrvObj_Sep1";
            resources.ApplyResources(this._cmPrvObj_Sep1, "_cmPrvObj_Sep1");
            // 
            // _cmPrvObj_Refresh
            // 
            this._cmPrvObj_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._cmPrvObj_Refresh, "_cmPrvObj_Refresh");
            this._cmPrvObj_Refresh.Name = "_cmPrvObj_Refresh";
            this._cmPrvObj_Refresh.Click += new System.EventHandler(this._previewRefreshObjects_Click);
            // 
            // _tsPrvObj
            // 
            this._tsPrvObj.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsPrvObj.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsPrvObj_Refresh,
            this._tsPrvObj_lblFilter,
            this._tsPrvObj_Filter,
            this._tsPrvObj_lblScope,
            this._tsPrvObj_Scope});
            resources.ApplyResources(this._tsPrvObj, "_tsPrvObj");
            this._tsPrvObj.Name = "_tsPrvObj";
            // 
            // _tsPrvObj_Refresh
            // 
            this._tsPrvObj_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsPrvObj_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._tsPrvObj_Refresh, "_tsPrvObj_Refresh");
            this._tsPrvObj_Refresh.Name = "_tsPrvObj_Refresh";
            this._tsPrvObj_Refresh.Click += new System.EventHandler(this._previewRefreshObjects_Click);
            // 
            // _tsPrvObj_lblFilter
            // 
            this._tsPrvObj_lblFilter.Name = "_tsPrvObj_lblFilter";
            resources.ApplyResources(this._tsPrvObj_lblFilter, "_tsPrvObj_lblFilter");
            // 
            // _tsPrvObj_Filter
            // 
            this._tsPrvObj_Filter.Name = "_tsPrvObj_Filter";
            resources.ApplyResources(this._tsPrvObj_Filter, "_tsPrvObj_Filter");
            this._tsPrvObj_Filter.TextChanged += new System.EventHandler(this._tsPrvObj_Filter_TextChanged);
            // 
            // _tsPrvObj_lblScope
            // 
            this._tsPrvObj_lblScope.Name = "_tsPrvObj_lblScope";
            resources.ApplyResources(this._tsPrvObj_lblScope, "_tsPrvObj_lblScope");
            // 
            // _tsPrvObj_Scope
            // 
            this._tsPrvObj_Scope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tsPrvObj_Scope.Items.AddRange(new object[] {
            resources.GetString("_tsPrvObj_Scope.Items"),
            resources.GetString("_tsPrvObj_Scope.Items1")});
            this._tsPrvObj_Scope.Name = "_tsPrvObj_Scope";
            resources.ApplyResources(this._tsPrvObj_Scope, "_tsPrvObj_Scope");
            this._tsPrvObj_Scope.KeyDown += new System.Windows.Forms.KeyEventHandler(this._tsPrvObj_Scope_KeyDown);
            // 
            // _splitPreviewDta
            // 
            resources.ApplyResources(this._splitPreviewDta, "_splitPreviewDta");
            this._splitPreviewDta.Name = "_splitPreviewDta";
            // 
            // _splitPreviewDta.Panel1
            // 
            this._splitPreviewDta.Panel1.Controls.Add(this._dgPreview);
            // 
            // _splitPreviewDta.Panel2
            // 
            this._splitPreviewDta.Panel2.Controls.Add(this._dgPreview_Filter);
            // 
            // _dgPreview
            // 
            this._dgPreview.AllowUserToAddRows = false;
            this._dgPreview.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.SteelBlue;
            this._dgPreview.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this._dgPreview.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this._dgPreview.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._dgPreview.BackgroundColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgPreview.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this._dgPreview.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this._dgPreview.ContextMenuStrip = this._cmPrvDta;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.SteelBlue;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgPreview.DefaultCellStyle = dataGridViewCellStyle3;
            resources.ApplyResources(this._dgPreview, "_dgPreview");
            this._dgPreview.Name = "_dgPreview";
            this._dgPreview.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.AliceBlue;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgPreview.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this._dgPreview.RowTemplate.Height = 23;
            this._dgPreview.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this._dgPreview_DataError);
            // 
            // _cmPrvDta
            // 
            this._cmPrvDta.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmPrvDta_Edit,
            this._cmPrvDta_Update,
            this._cmPrvDta_Sep1,
            this._cmPrvDtaCp,
            this._cmPrvDtaSv,
            this._cmPrvDta_Sep0,
            this._cmPrvDta_Refresh});
            this._cmPrvDta.Name = "_cmPreview";
            resources.ApplyResources(this._cmPrvDta, "_cmPrvDta");
            // 
            // _cmPrvDta_Edit
            // 
            resources.ApplyResources(this._cmPrvDta_Edit, "_cmPrvDta_Edit");
            this._cmPrvDta_Edit.Image = global::Nyx.Properties.Resources.database_edit;
            this._cmPrvDta_Edit.Name = "_cmPrvDta_Edit";
            this._cmPrvDta_Edit.Click += new System.EventHandler(this._previewEditData_Click);
            // 
            // _cmPrvDta_Update
            // 
            resources.ApplyResources(this._cmPrvDta_Update, "_cmPrvDta_Update");
            this._cmPrvDta_Update.Image = global::Nyx.Properties.Resources.database_save;
            this._cmPrvDta_Update.Name = "_cmPrvDta_Update";
            this._cmPrvDta_Update.Click += new System.EventHandler(this._previewUpdData_Click);
            // 
            // _cmPrvDta_Sep1
            // 
            this._cmPrvDta_Sep1.Name = "_cmPrvDta_Sep1";
            resources.ApplyResources(this._cmPrvDta_Sep1, "_cmPrvDta_Sep1");
            // 
            // _cmPrvDtaCp
            // 
            this._cmPrvDtaCp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmPrvDtaCp_Field,
            this._cmPrvDtaCp_Cells,
            this._cmPrvDtaCp_Table});
            resources.ApplyResources(this._cmPrvDtaCp, "_cmPrvDtaCp");
            this._cmPrvDtaCp.Image = global::Nyx.Properties.Resources.page_copy;
            this._cmPrvDtaCp.Name = "_cmPrvDtaCp";
            // 
            // _cmPrvDtaCp_Field
            // 
            this._cmPrvDtaCp_Field.Name = "_cmPrvDtaCp_Field";
            resources.ApplyResources(this._cmPrvDtaCp_Field, "_cmPrvDtaCp_Field");
            this._cmPrvDtaCp_Field.Click += new System.EventHandler(this._cmPrvDtaCp_Field_Click);
            // 
            // _cmPrvDtaCp_Cells
            // 
            this._cmPrvDtaCp_Cells.Name = "_cmPrvDtaCp_Cells";
            resources.ApplyResources(this._cmPrvDtaCp_Cells, "_cmPrvDtaCp_Cells");
            this._cmPrvDtaCp_Cells.Click += new System.EventHandler(this._cmPrvDtaCp_Cells_Click);
            // 
            // _cmPrvDtaCp_Table
            // 
            this._cmPrvDtaCp_Table.Name = "_cmPrvDtaCp_Table";
            resources.ApplyResources(this._cmPrvDtaCp_Table, "_cmPrvDtaCp_Table");
            this._cmPrvDtaCp_Table.Click += new System.EventHandler(this._cmPrvDtaCp_Table_Click);
            // 
            // _cmPrvDtaSv
            // 
            this._cmPrvDtaSv.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmPrvDtaSv_Cells,
            this._cmPrvDtaSv_Table});
            resources.ApplyResources(this._cmPrvDtaSv, "_cmPrvDtaSv");
            this._cmPrvDtaSv.Image = global::Nyx.Properties.Resources.disk;
            this._cmPrvDtaSv.Name = "_cmPrvDtaSv";
            // 
            // _cmPrvDtaSv_Cells
            // 
            this._cmPrvDtaSv_Cells.Name = "_cmPrvDtaSv_Cells";
            resources.ApplyResources(this._cmPrvDtaSv_Cells, "_cmPrvDtaSv_Cells");
            this._cmPrvDtaSv_Cells.Click += new System.EventHandler(this._cmPrvDtaSv_Cells_Click);
            // 
            // _cmPrvDtaSv_Table
            // 
            this._cmPrvDtaSv_Table.Name = "_cmPrvDtaSv_Table";
            resources.ApplyResources(this._cmPrvDtaSv_Table, "_cmPrvDtaSv_Table");
            this._cmPrvDtaSv_Table.Click += new System.EventHandler(this._cmPrvDtaSv_Table_Click);
            // 
            // _cmPrvDta_Sep0
            // 
            this._cmPrvDta_Sep0.Name = "_cmPrvDta_Sep0";
            resources.ApplyResources(this._cmPrvDta_Sep0, "_cmPrvDta_Sep0");
            // 
            // _cmPrvDta_Refresh
            // 
            this._cmPrvDta_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            this._cmPrvDta_Refresh.Name = "_cmPrvDta_Refresh";
            resources.ApplyResources(this._cmPrvDta_Refresh, "_cmPrvDta_Refresh");
            this._cmPrvDta_Refresh.Click += new System.EventHandler(this._previewRefreshPreview_Click);
            // 
            // _dgPreview_Filter
            // 
            this._dgPreview_Filter.AllowUserToAddRows = false;
            this._dgPreview_Filter.AllowUserToDeleteRows = false;
            this._dgPreview_Filter.AllowUserToOrderColumns = true;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.WhiteSmoke;
            this._dgPreview_Filter.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle5;
            this._dgPreview_Filter.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this._dgPreview_Filter.BackgroundColor = System.Drawing.Color.GhostWhite;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgPreview_Filter.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this._dgPreview_Filter.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this._dgPreview_Filter.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.vField,
            this.vSelect,
            this.vLOperator,
            this.vFilter,
            this.vValue,
            this.vGroup});
            this._dgPreview_Filter.ContextMenuStrip = this._cmPrvFilter;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.ControlLight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this._dgPreview_Filter.DefaultCellStyle = dataGridViewCellStyle7;
            resources.ApplyResources(this._dgPreview_Filter, "_dgPreview_Filter");
            this._dgPreview_Filter.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this._dgPreview_Filter.Name = "_dgPreview_Filter";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this._dgPreview_Filter.RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this._dgPreview_Filter.RowTemplate.Height = 20;
            this._dgPreview_Filter.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            // 
            // vField
            // 
            resources.ApplyResources(this.vField, "vField");
            this.vField.Name = "vField";
            this.vField.ReadOnly = true;
            // 
            // vSelect
            // 
            this.vSelect.FalseValue = "false";
            resources.ApplyResources(this.vSelect, "vSelect");
            this.vSelect.Name = "vSelect";
            this.vSelect.TrueValue = "true";
            // 
            // vLOperator
            // 
            resources.ApplyResources(this.vLOperator, "vLOperator");
            this.vLOperator.Items.AddRange(new object[] {
            "",
            "AND",
            "OR",
            "XOR"});
            this.vLOperator.Name = "vLOperator";
            // 
            // vFilter
            // 
            resources.ApplyResources(this.vFilter, "vFilter");
            this.vFilter.Items.AddRange(new object[] {
            "",
            "=",
            "<>",
            "<",
            ">",
            "<=",
            ">=",
            "LIKE",
            "NOT LIKE",
            "NOT"});
            this.vFilter.Name = "vFilter";
            // 
            // vValue
            // 
            resources.ApplyResources(this.vValue, "vValue");
            this.vValue.Name = "vValue";
            // 
            // vGroup
            // 
            resources.ApplyResources(this.vGroup, "vGroup");
            this.vGroup.Name = "vGroup";
            // 
            // _cmPrvFilter
            // 
            this._cmPrvFilter.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmPrvFilter_Set,
            this._cmPrvFilter_Reset,
            this._cmPrvFilter_Sep0,
            this._cmPrvFilter_FieldUp,
            this._cmPrvFilter_FieldDown,
            this._cmPrvFilter_Sep1,
            this._cmPrvFilter_DupField,
            this._cmPrvFilter_RmDupField});
            this._cmPrvFilter.Name = "_cmPrvFilter";
            resources.ApplyResources(this._cmPrvFilter, "_cmPrvFilter");
            // 
            // _cmPrvFilter_Set
            // 
            this._cmPrvFilter_Set.Image = global::Nyx.Properties.Resources.control_play;
            resources.ApplyResources(this._cmPrvFilter_Set, "_cmPrvFilter_Set");
            this._cmPrvFilter_Set.Name = "_cmPrvFilter_Set";
            this._cmPrvFilter_Set.Click += new System.EventHandler(this._preview_SetFilter_Click);
            // 
            // _cmPrvFilter_Reset
            // 
            this._cmPrvFilter_Reset.Image = global::Nyx.Properties.Resources.page_white;
            resources.ApplyResources(this._cmPrvFilter_Reset, "_cmPrvFilter_Reset");
            this._cmPrvFilter_Reset.Name = "_cmPrvFilter_Reset";
            this._cmPrvFilter_Reset.Click += new System.EventHandler(this._preview_ResetFilter_Click);
            // 
            // _cmPrvFilter_Sep0
            // 
            this._cmPrvFilter_Sep0.Name = "_cmPrvFilter_Sep0";
            resources.ApplyResources(this._cmPrvFilter_Sep0, "_cmPrvFilter_Sep0");
            // 
            // _cmPrvFilter_FieldUp
            // 
            this._cmPrvFilter_FieldUp.Image = global::Nyx.Properties.Resources.arrow_up;
            resources.ApplyResources(this._cmPrvFilter_FieldUp, "_cmPrvFilter_FieldUp");
            this._cmPrvFilter_FieldUp.Name = "_cmPrvFilter_FieldUp";
            this._cmPrvFilter_FieldUp.Click += new System.EventHandler(this._cmPrvFilter_FieldUp_Click);
            // 
            // _cmPrvFilter_FieldDown
            // 
            this._cmPrvFilter_FieldDown.Image = global::Nyx.Properties.Resources.arrow_down;
            resources.ApplyResources(this._cmPrvFilter_FieldDown, "_cmPrvFilter_FieldDown");
            this._cmPrvFilter_FieldDown.Name = "_cmPrvFilter_FieldDown";
            this._cmPrvFilter_FieldDown.Click += new System.EventHandler(this._cmPrvFilter_FieldDown_Click);
            // 
            // _cmPrvFilter_Sep1
            // 
            this._cmPrvFilter_Sep1.Name = "_cmPrvFilter_Sep1";
            resources.ApplyResources(this._cmPrvFilter_Sep1, "_cmPrvFilter_Sep1");
            // 
            // _cmPrvFilter_DupField
            // 
            this._cmPrvFilter_DupField.Image = global::Nyx.Properties.Resources.page_copy;
            resources.ApplyResources(this._cmPrvFilter_DupField, "_cmPrvFilter_DupField");
            this._cmPrvFilter_DupField.Name = "_cmPrvFilter_DupField";
            this._cmPrvFilter_DupField.Click += new System.EventHandler(this._cmPrvFilter_DupField_Click);
            // 
            // _cmPrvFilter_RmDupField
            // 
            resources.ApplyResources(this._cmPrvFilter_RmDupField, "_cmPrvFilter_RmDupField");
            this._cmPrvFilter_RmDupField.Name = "_cmPrvFilter_RmDupField";
            this._cmPrvFilter_RmDupField.Click += new System.EventHandler(this._cmPrvFilter_RmDupField_Click);
            // 
            // _tsPrvDta
            // 
            this._tsPrvDta.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsPrvDta.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsPrvDta_Refresh,
            this._tsPrvDta_Sep0,
            this._tsPrvDta_ShowFilter,
            this._tsPrvDta_SetFilter,
            this._tsPrvDta_ResetFilter,
            this._tsPrvDta_Sep7,
            this._tsPrvDta_Edit,
            this._tsPrvDta_Update,
            this._tsPrvDta_Sep1,
            this._tsPrvDta_Start,
            this._tsPrvDta_Prev,
            this._tsPrvDta_Next,
            this._tsPrvDta_End,
            this._tsPrvDta_Sep2,
            this._tsPrvDta_RowsPerPage,
            this._tsPrvDta_Sep3,
            this._tsPrvDta_lblPage,
            this._tsPrvDta_lblActPg,
            this._tsPrvDta_lblDiv0,
            this._tsPrvDta_lblPgCnt,
            this._tsPrvDta_Sep5,
            this._tsPrvDta_lblRows,
            this._tsPrvDta_lblActRows,
            this._tsPrvDta_lblDiv1,
            this._tsPrvDta_lblRowCnt,
            this._tsPrvDta_Sep6,
            this._tsPrvDta_lblCurTable,
            this._tsPrvDta_curObject});
            resources.ApplyResources(this._tsPrvDta, "_tsPrvDta");
            this._tsPrvDta.Name = "_tsPrvDta";
            // 
            // _tsPrvDta_Refresh
            // 
            this._tsPrvDta_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_Refresh, "_tsPrvDta_Refresh");
            this._tsPrvDta_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            this._tsPrvDta_Refresh.Name = "_tsPrvDta_Refresh";
            this._tsPrvDta_Refresh.Click += new System.EventHandler(this._previewRefreshPreview_Click);
            // 
            // _tsPrvDta_Sep0
            // 
            this._tsPrvDta_Sep0.Name = "_tsPrvDta_Sep0";
            resources.ApplyResources(this._tsPrvDta_Sep0, "_tsPrvDta_Sep0");
            // 
            // _tsPrvDta_ShowFilter
            // 
            this._tsPrvDta_ShowFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_ShowFilter, "_tsPrvDta_ShowFilter");
            this._tsPrvDta_ShowFilter.Image = global::Nyx.Properties.Resources.application_tile_vertical;
            this._tsPrvDta_ShowFilter.Name = "_tsPrvDta_ShowFilter";
            this._tsPrvDta_ShowFilter.Click += new System.EventHandler(this._tsPrvDta_ShowFilter_Click);
            // 
            // _tsPrvDta_SetFilter
            // 
            this._tsPrvDta_SetFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_SetFilter, "_tsPrvDta_SetFilter");
            this._tsPrvDta_SetFilter.Image = global::Nyx.Properties.Resources.control_play;
            this._tsPrvDta_SetFilter.Name = "_tsPrvDta_SetFilter";
            this._tsPrvDta_SetFilter.Click += new System.EventHandler(this._preview_SetFilter_Click);
            // 
            // _tsPrvDta_ResetFilter
            // 
            this._tsPrvDta_ResetFilter.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_ResetFilter, "_tsPrvDta_ResetFilter");
            this._tsPrvDta_ResetFilter.Image = global::Nyx.Properties.Resources.page_white;
            this._tsPrvDta_ResetFilter.Name = "_tsPrvDta_ResetFilter";
            this._tsPrvDta_ResetFilter.Click += new System.EventHandler(this._preview_ResetFilter_Click);
            // 
            // _tsPrvDta_Sep7
            // 
            this._tsPrvDta_Sep7.Name = "_tsPrvDta_Sep7";
            resources.ApplyResources(this._tsPrvDta_Sep7, "_tsPrvDta_Sep7");
            // 
            // _tsPrvDta_Edit
            // 
            this._tsPrvDta_Edit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_Edit, "_tsPrvDta_Edit");
            this._tsPrvDta_Edit.Image = global::Nyx.Properties.Resources.database_edit;
            this._tsPrvDta_Edit.Name = "_tsPrvDta_Edit";
            this._tsPrvDta_Edit.Click += new System.EventHandler(this._previewEditData_Click);
            // 
            // _tsPrvDta_Update
            // 
            this._tsPrvDta_Update.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_Update, "_tsPrvDta_Update");
            this._tsPrvDta_Update.Image = global::Nyx.Properties.Resources.database_save;
            this._tsPrvDta_Update.Name = "_tsPrvDta_Update";
            this._tsPrvDta_Update.Click += new System.EventHandler(this._previewUpdData_Click);
            // 
            // _tsPrvDta_Sep1
            // 
            this._tsPrvDta_Sep1.Name = "_tsPrvDta_Sep1";
            resources.ApplyResources(this._tsPrvDta_Sep1, "_tsPrvDta_Sep1");
            // 
            // _tsPrvDta_Start
            // 
            this._tsPrvDta_Start.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_Start, "_tsPrvDta_Start");
            this._tsPrvDta_Start.Image = global::Nyx.Properties.Resources.resultset_first;
            this._tsPrvDta_Start.Name = "_tsPrvDta_Start";
            this._tsPrvDta_Start.Click += new System.EventHandler(this._previewRefreshPreview_Click);
            // 
            // _tsPrvDta_Prev
            // 
            this._tsPrvDta_Prev.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_Prev, "_tsPrvDta_Prev");
            this._tsPrvDta_Prev.Image = global::Nyx.Properties.Resources.resultset_previous;
            this._tsPrvDta_Prev.Name = "_tsPrvDta_Prev";
            this._tsPrvDta_Prev.Click += new System.EventHandler(this._previewPrev_Click);
            // 
            // _tsPrvDta_Next
            // 
            this._tsPrvDta_Next.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_Next, "_tsPrvDta_Next");
            this._tsPrvDta_Next.Image = global::Nyx.Properties.Resources.resultset_next;
            this._tsPrvDta_Next.Name = "_tsPrvDta_Next";
            this._tsPrvDta_Next.Click += new System.EventHandler(this._previewNext_Click);
            // 
            // _tsPrvDta_End
            // 
            this._tsPrvDta_End.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsPrvDta_End, "_tsPrvDta_End");
            this._tsPrvDta_End.Image = global::Nyx.Properties.Resources.resultset_last;
            this._tsPrvDta_End.Name = "_tsPrvDta_End";
            this._tsPrvDta_End.Click += new System.EventHandler(this._previewEnd_Click);
            // 
            // _tsPrvDta_Sep2
            // 
            this._tsPrvDta_Sep2.Name = "_tsPrvDta_Sep2";
            resources.ApplyResources(this._tsPrvDta_Sep2, "_tsPrvDta_Sep2");
            // 
            // _tsPrvDta_RowsPerPage
            // 
            this._tsPrvDta_RowsPerPage.BackColor = System.Drawing.Color.GhostWhite;
            this._tsPrvDta_RowsPerPage.Items.AddRange(new object[] {
            resources.GetString("_tsPrvDta_RowsPerPage.Items"),
            resources.GetString("_tsPrvDta_RowsPerPage.Items1"),
            resources.GetString("_tsPrvDta_RowsPerPage.Items2"),
            resources.GetString("_tsPrvDta_RowsPerPage.Items3"),
            resources.GetString("_tsPrvDta_RowsPerPage.Items4"),
            resources.GetString("_tsPrvDta_RowsPerPage.Items5"),
            resources.GetString("_tsPrvDta_RowsPerPage.Items6"),
            resources.GetString("_tsPrvDta_RowsPerPage.Items7")});
            this._tsPrvDta_RowsPerPage.Name = "_tsPrvDta_RowsPerPage";
            resources.ApplyResources(this._tsPrvDta_RowsPerPage, "_tsPrvDta_RowsPerPage");
            this._tsPrvDta_RowsPerPage.KeyDown += new System.Windows.Forms.KeyEventHandler(this._tsPrvDta_RowsPerPage_KeyDown);
            this._tsPrvDta_RowsPerPage.SelectedIndexChanged += new System.EventHandler(this._previewRefreshPreview_Click);
            // 
            // _tsPrvDta_Sep3
            // 
            this._tsPrvDta_Sep3.Name = "_tsPrvDta_Sep3";
            resources.ApplyResources(this._tsPrvDta_Sep3, "_tsPrvDta_Sep3");
            // 
            // _tsPrvDta_lblPage
            // 
            this._tsPrvDta_lblPage.Name = "_tsPrvDta_lblPage";
            resources.ApplyResources(this._tsPrvDta_lblPage, "_tsPrvDta_lblPage");
            // 
            // _tsPrvDta_lblActPg
            // 
            this._tsPrvDta_lblActPg.Name = "_tsPrvDta_lblActPg";
            resources.ApplyResources(this._tsPrvDta_lblActPg, "_tsPrvDta_lblActPg");
            // 
            // _tsPrvDta_lblDiv0
            // 
            this._tsPrvDta_lblDiv0.Name = "_tsPrvDta_lblDiv0";
            resources.ApplyResources(this._tsPrvDta_lblDiv0, "_tsPrvDta_lblDiv0");
            // 
            // _tsPrvDta_lblPgCnt
            // 
            this._tsPrvDta_lblPgCnt.Name = "_tsPrvDta_lblPgCnt";
            resources.ApplyResources(this._tsPrvDta_lblPgCnt, "_tsPrvDta_lblPgCnt");
            // 
            // _tsPrvDta_Sep5
            // 
            this._tsPrvDta_Sep5.Name = "_tsPrvDta_Sep5";
            resources.ApplyResources(this._tsPrvDta_Sep5, "_tsPrvDta_Sep5");
            // 
            // _tsPrvDta_lblRows
            // 
            this._tsPrvDta_lblRows.Name = "_tsPrvDta_lblRows";
            resources.ApplyResources(this._tsPrvDta_lblRows, "_tsPrvDta_lblRows");
            // 
            // _tsPrvDta_lblActRows
            // 
            this._tsPrvDta_lblActRows.Name = "_tsPrvDta_lblActRows";
            resources.ApplyResources(this._tsPrvDta_lblActRows, "_tsPrvDta_lblActRows");
            // 
            // _tsPrvDta_lblDiv1
            // 
            this._tsPrvDta_lblDiv1.Name = "_tsPrvDta_lblDiv1";
            resources.ApplyResources(this._tsPrvDta_lblDiv1, "_tsPrvDta_lblDiv1");
            // 
            // _tsPrvDta_lblRowCnt
            // 
            this._tsPrvDta_lblRowCnt.Name = "_tsPrvDta_lblRowCnt";
            resources.ApplyResources(this._tsPrvDta_lblRowCnt, "_tsPrvDta_lblRowCnt");
            // 
            // _tsPrvDta_Sep6
            // 
            this._tsPrvDta_Sep6.Name = "_tsPrvDta_Sep6";
            resources.ApplyResources(this._tsPrvDta_Sep6, "_tsPrvDta_Sep6");
            // 
            // _tsPrvDta_lblCurTable
            // 
            this._tsPrvDta_lblCurTable.Name = "_tsPrvDta_lblCurTable";
            resources.ApplyResources(this._tsPrvDta_lblCurTable, "_tsPrvDta_lblCurTable");
            // 
            // _tsPrvDta_curObject
            // 
            this._tsPrvDta_curObject.Name = "_tsPrvDta_curObject";
            resources.ApplyResources(this._tsPrvDta_curObject, "_tsPrvDta_curObject");
            // 
            // _lblNotSupportedPreview
            // 
            resources.ApplyResources(this._lblNotSupportedPreview, "_lblNotSupportedPreview");
            this._lblNotSupportedPreview.ForeColor = System.Drawing.Color.Firebrick;
            this._lblNotSupportedPreview.Name = "_lblNotSupportedPreview";
            // 
            // _tpTables
            // 
            this._tpTables.Controls.Add(this._pnlTables);
            this._tpTables.Controls.Add(this._lblNotSupportedTables);
            resources.ApplyResources(this._tpTables, "_tpTables");
            this._tpTables.Name = "_tpTables";
            this._tpTables.UseVisualStyleBackColor = true;
            // 
            // _pnlTables
            // 
            this._pnlTables.Controls.Add(this._splitTables);
            this._pnlTables.Controls.Add(this._tsTables);
            resources.ApplyResources(this._pnlTables, "_pnlTables");
            this._pnlTables.Name = "_pnlTables";
            // 
            // _splitTables
            // 
            resources.ApplyResources(this._splitTables, "_splitTables");
            this._splitTables.Name = "_splitTables";
            // 
            // _splitTables.Panel1
            // 
            this._splitTables.Panel1.Controls.Add(this._lbTables);
            // 
            // _splitTables.Panel2
            // 
            this._splitTables.Panel2.Controls.Add(this._splitIndexes);
            // 
            // _lbTables
            // 
            this._lbTables.BackColor = System.Drawing.Color.GhostWhite;
            this._lbTables.ContextMenuStrip = this._cmTable;
            resources.ApplyResources(this._lbTables, "_lbTables");
            this._lbTables.FormattingEnabled = true;
            this._lbTables.Name = "_lbTables";
            this._lbTables.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this._lbTables.DoubleClick += new System.EventHandler(this._lbTables_DoubleClick);
            this._lbTables.SelectedIndexChanged += new System.EventHandler(this._lbTables_SelectedIndexChanged);
            // 
            // _cmTable
            // 
            this._cmTable.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmTable_Describe,
            this._cmTable_Sep0,
            this._cmTable_Create,
            this._cmTable_Alter,
            this._cmTable_Drop,
            this._cmTable_Truncate,
            this._cmTable_Diag,
            this._cmTable_Sep1,
            this._cmTable_Refresh,
            this._cmTable_Sep2,
            this._cmTable_SelAll,
            this._cmTable_SelNone,
            this._cmTable_SelInv});
            this._cmTable.Name = "_cm3";
            resources.ApplyResources(this._cmTable, "_cmTable");
            // 
            // _cmTable_Describe
            // 
            resources.ApplyResources(this._cmTable_Describe, "_cmTable_Describe");
            this._cmTable_Describe.Image = global::Nyx.Properties.Resources.magnifier;
            this._cmTable_Describe.Name = "_cmTable_Describe";
            this._cmTable_Describe.Click += new System.EventHandler(this._lbTables_DoubleClick);
            // 
            // _cmTable_Sep0
            // 
            this._cmTable_Sep0.Name = "_cmTable_Sep0";
            resources.ApplyResources(this._cmTable_Sep0, "_cmTable_Sep0");
            // 
            // _cmTable_Create
            // 
            resources.ApplyResources(this._cmTable_Create, "_cmTable_Create");
            this._cmTable_Create.Image = global::Nyx.Properties.Resources.table_add;
            this._cmTable_Create.Name = "_cmTable_Create";
            this._cmTable_Create.Click += new System.EventHandler(this._cmTable_Create_Click);
            // 
            // _cmTable_Alter
            // 
            resources.ApplyResources(this._cmTable_Alter, "_cmTable_Alter");
            this._cmTable_Alter.Image = global::Nyx.Properties.Resources.table_edit;
            this._cmTable_Alter.Name = "_cmTable_Alter";
            this._cmTable_Alter.Click += new System.EventHandler(this._cmTable_Alter_Click);
            // 
            // _cmTable_Drop
            // 
            resources.ApplyResources(this._cmTable_Drop, "_cmTable_Drop");
            this._cmTable_Drop.Image = global::Nyx.Properties.Resources.table_delete;
            this._cmTable_Drop.Name = "_cmTable_Drop";
            this._cmTable_Drop.Click += new System.EventHandler(this._cmTable_Actions);
            // 
            // _cmTable_Truncate
            // 
            resources.ApplyResources(this._cmTable_Truncate, "_cmTable_Truncate");
            this._cmTable_Truncate.Image = global::Nyx.Properties.Resources.delete_blackwhite;
            this._cmTable_Truncate.Name = "_cmTable_Truncate";
            this._cmTable_Truncate.Click += new System.EventHandler(this._cmTable_Actions);
            // 
            // _cmTable_Diag
            // 
            this._cmTable_Diag.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmTableDiag_Check,
            this._cmTableDiag_Analyze,
            this._cmTableDiag_OptReb,
            this._cmTableDiag_Repair});
            this._cmTable_Diag.Image = global::Nyx.Properties.Resources.table_gear;
            resources.ApplyResources(this._cmTable_Diag, "_cmTable_Diag");
            this._cmTable_Diag.Name = "_cmTable_Diag";
            // 
            // _cmTableDiag_Check
            // 
            resources.ApplyResources(this._cmTableDiag_Check, "_cmTableDiag_Check");
            this._cmTableDiag_Check.Image = global::Nyx.Properties.Resources.table_key;
            this._cmTableDiag_Check.Name = "_cmTableDiag_Check";
            this._cmTableDiag_Check.Click += new System.EventHandler(this._cmTable_Actions);
            // 
            // _cmTableDiag_Analyze
            // 
            resources.ApplyResources(this._cmTableDiag_Analyze, "_cmTableDiag_Analyze");
            this._cmTableDiag_Analyze.Image = global::Nyx.Properties.Resources.table_lightning;
            this._cmTableDiag_Analyze.Name = "_cmTableDiag_Analyze";
            this._cmTableDiag_Analyze.Click += new System.EventHandler(this._cmTable_Actions);
            // 
            // _cmTableDiag_OptReb
            // 
            resources.ApplyResources(this._cmTableDiag_OptReb, "_cmTableDiag_OptReb");
            this._cmTableDiag_OptReb.Image = global::Nyx.Properties.Resources.table_gear;
            this._cmTableDiag_OptReb.Name = "_cmTableDiag_OptReb";
            this._cmTableDiag_OptReb.Click += new System.EventHandler(this._cmTable_Actions);
            // 
            // _cmTableDiag_Repair
            // 
            resources.ApplyResources(this._cmTableDiag_Repair, "_cmTableDiag_Repair");
            this._cmTableDiag_Repair.Image = global::Nyx.Properties.Resources.table_error;
            this._cmTableDiag_Repair.Name = "_cmTableDiag_Repair";
            this._cmTableDiag_Repair.Click += new System.EventHandler(this._cmTable_Actions);
            // 
            // _cmTable_Sep1
            // 
            this._cmTable_Sep1.Name = "_cmTable_Sep1";
            resources.ApplyResources(this._cmTable_Sep1, "_cmTable_Sep1");
            // 
            // _cmTable_Refresh
            // 
            this._cmTable_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._cmTable_Refresh, "_cmTable_Refresh");
            this._cmTable_Refresh.Name = "_cmTable_Refresh";
            this._cmTable_Refresh.Click += new System.EventHandler(this._cmTable_Refresh_Click);
            // 
            // _cmTable_Sep2
            // 
            this._cmTable_Sep2.Name = "_cmTable_Sep2";
            resources.ApplyResources(this._cmTable_Sep2, "_cmTable_Sep2");
            // 
            // _cmTable_SelAll
            // 
            this._cmTable_SelAll.Image = global::Nyx.Properties.Resources.arrow_redo;
            resources.ApplyResources(this._cmTable_SelAll, "_cmTable_SelAll");
            this._cmTable_SelAll.Name = "_cmTable_SelAll";
            this._cmTable_SelAll.Click += new System.EventHandler(this._cmTable_Selection);
            // 
            // _cmTable_SelNone
            // 
            this._cmTable_SelNone.Image = global::Nyx.Properties.Resources.arrow_undo;
            resources.ApplyResources(this._cmTable_SelNone, "_cmTable_SelNone");
            this._cmTable_SelNone.Name = "_cmTable_SelNone";
            this._cmTable_SelNone.Click += new System.EventHandler(this._cmTable_Selection);
            // 
            // _cmTable_SelInv
            // 
            this._cmTable_SelInv.Image = global::Nyx.Properties.Resources.arrow_rotate_clockwise;
            resources.ApplyResources(this._cmTable_SelInv, "_cmTable_SelInv");
            this._cmTable_SelInv.Name = "_cmTable_SelInv";
            this._cmTable_SelInv.Click += new System.EventHandler(this._cmTable_Selection);
            // 
            // _splitIndexes
            // 
            resources.ApplyResources(this._splitIndexes, "_splitIndexes");
            this._splitIndexes.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
            this._splitIndexes.Name = "_splitIndexes";
            // 
            // _splitIndexes.Panel1
            // 
            this._splitIndexes.Panel1.Controls.Add(this._lvTableDesc);
            // 
            // _splitIndexes.Panel2
            // 
            this._splitIndexes.Panel2.Controls.Add(this._splitIndexDetails);
            // 
            // _lvTableDesc
            // 
            this._lvTableDesc.BackColor = System.Drawing.Color.GhostWhite;
            this._lvTableDesc.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.field,
            this.type,
            this.length,
            this.precission});
            this._lvTableDesc.ContextMenuStrip = this._cmTblField;
            resources.ApplyResources(this._lvTableDesc, "_lvTableDesc");
            this._lvTableDesc.FullRowSelect = true;
            this._lvTableDesc.GridLines = true;
            this._lvTableDesc.Name = "_lvTableDesc";
            this._lvTableDesc.UseCompatibleStateImageBehavior = false;
            this._lvTableDesc.View = System.Windows.Forms.View.Details;
            this._lvTableDesc.DoubleClick += new System.EventHandler(this._cmTblField_Alter_Click);
            this._lvTableDesc.SelectedIndexChanged += new System.EventHandler(this._lvTableDesc_SelectedIndexChanged);
            this._lvTableDesc.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvTableDesc_ColumnClick);
            // 
            // field
            // 
            resources.ApplyResources(this.field, "field");
            // 
            // type
            // 
            resources.ApplyResources(this.type, "type");
            // 
            // length
            // 
            resources.ApplyResources(this.length, "length");
            // 
            // precission
            // 
            resources.ApplyResources(this.precission, "precission");
            // 
            // _cmTblField
            // 
            this._cmTblField.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmTblField_Create,
            this._cmTblField_Alter,
            this._cmTblField_Drop,
            this._cmTblField_Sep0,
            this._cmTblField_Refresh,
            this._cmTblField_Sep1,
            this._cmTblField_SelAll,
            this._cmTblField_SelNone,
            this._cmTblField_SelInv});
            this._cmTblField.Name = "_cmtttable";
            resources.ApplyResources(this._cmTblField, "_cmTblField");
            // 
            // _cmTblField_Create
            // 
            this._cmTblField_Create.Image = global::Nyx.Properties.Resources.table_add;
            resources.ApplyResources(this._cmTblField_Create, "_cmTblField_Create");
            this._cmTblField_Create.Name = "_cmTblField_Create";
            this._cmTblField_Create.Click += new System.EventHandler(this._cmTblField_Create_Click);
            // 
            // _cmTblField_Alter
            // 
            this._cmTblField_Alter.Image = global::Nyx.Properties.Resources.table_edit;
            resources.ApplyResources(this._cmTblField_Alter, "_cmTblField_Alter");
            this._cmTblField_Alter.Name = "_cmTblField_Alter";
            this._cmTblField_Alter.Click += new System.EventHandler(this._cmTblField_Alter_Click);
            // 
            // _cmTblField_Drop
            // 
            this._cmTblField_Drop.Image = global::Nyx.Properties.Resources.table_delete;
            resources.ApplyResources(this._cmTblField_Drop, "_cmTblField_Drop");
            this._cmTblField_Drop.Name = "_cmTblField_Drop";
            this._cmTblField_Drop.Click += new System.EventHandler(this._cmTblField_Drop_Click);
            // 
            // _cmTblField_Sep0
            // 
            this._cmTblField_Sep0.Name = "_cmTblField_Sep0";
            resources.ApplyResources(this._cmTblField_Sep0, "_cmTblField_Sep0");
            // 
            // _cmTblField_Refresh
            // 
            this._cmTblField_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._cmTblField_Refresh, "_cmTblField_Refresh");
            this._cmTblField_Refresh.Name = "_cmTblField_Refresh";
            this._cmTblField_Refresh.Click += new System.EventHandler(this._cmTblField_Refresh_Click);
            // 
            // _cmTblField_Sep1
            // 
            this._cmTblField_Sep1.Name = "_cmTblField_Sep1";
            resources.ApplyResources(this._cmTblField_Sep1, "_cmTblField_Sep1");
            // 
            // _cmTblField_SelAll
            // 
            this._cmTblField_SelAll.Image = global::Nyx.Properties.Resources.arrow_redo;
            resources.ApplyResources(this._cmTblField_SelAll, "_cmTblField_SelAll");
            this._cmTblField_SelAll.Name = "_cmTblField_SelAll";
            this._cmTblField_SelAll.Click += new System.EventHandler(this._cmTblField_Selection);
            // 
            // _cmTblField_SelNone
            // 
            this._cmTblField_SelNone.Image = global::Nyx.Properties.Resources.arrow_undo;
            resources.ApplyResources(this._cmTblField_SelNone, "_cmTblField_SelNone");
            this._cmTblField_SelNone.Name = "_cmTblField_SelNone";
            // 
            // _cmTblField_SelInv
            // 
            this._cmTblField_SelInv.Image = global::Nyx.Properties.Resources.arrow_rotate_clockwise;
            resources.ApplyResources(this._cmTblField_SelInv, "_cmTblField_SelInv");
            this._cmTblField_SelInv.Name = "_cmTblField_SelInv";
            // 
            // _splitIndexDetails
            // 
            resources.ApplyResources(this._splitIndexDetails, "_splitIndexDetails");
            this._splitIndexDetails.Name = "_splitIndexDetails";
            // 
            // _splitIndexDetails.Panel1
            // 
            this._splitIndexDetails.Panel1.Controls.Add(this._lvIndexes);
            // 
            // _splitIndexDetails.Panel2
            // 
            this._splitIndexDetails.Panel2.Controls.Add(this._lvIndexDesc);
            // 
            // _lvIndexes
            // 
            this._lvIndexes.BackColor = System.Drawing.Color.GhostWhite;
            this._lvIndexes.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.idxname,
            this.tblname,
            this.tblstatus,
            this.analyse});
            this._lvIndexes.ContextMenuStrip = this._cmIndex;
            this._lvIndexes.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this._lvIndexes, "_lvIndexes");
            this._lvIndexes.FullRowSelect = true;
            this._lvIndexes.GridLines = true;
            this._lvIndexes.HideSelection = false;
            this._lvIndexes.Name = "_lvIndexes";
            this._lvIndexes.UseCompatibleStateImageBehavior = false;
            this._lvIndexes.View = System.Windows.Forms.View.Details;
            this._lvIndexes.DoubleClick += new System.EventHandler(this._lvIndexes_DoubleClick);
            this._lvIndexes.SelectedIndexChanged += new System.EventHandler(this._lvIndexes_SelectedIndexChanged);
            this._lvIndexes.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvIndexes_ColumnClick);
            // 
            // idxname
            // 
            resources.ApplyResources(this.idxname, "idxname");
            // 
            // tblname
            // 
            resources.ApplyResources(this.tblname, "tblname");
            // 
            // tblstatus
            // 
            resources.ApplyResources(this.tblstatus, "tblstatus");
            // 
            // analyse
            // 
            resources.ApplyResources(this.analyse, "analyse");
            // 
            // _cmIndex
            // 
            this._cmIndex.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmIndex_Describe,
            this._cmIndex_Sep0,
            this._cmIndex_Create,
            this._cmIndex_Alter,
            this._cmIndex_Drop,
            this._cmIndex_Sep1,
            this._cmIndex_Refresh,
            this._cmIndex_Sep2,
            this._cmIndex_SelAll,
            this._cmIndex_SelNone,
            this._cmIndex_SelInv});
            this._cmIndex.Name = "_cmttidx";
            resources.ApplyResources(this._cmIndex, "_cmIndex");
            // 
            // _cmIndex_Describe
            // 
            this._cmIndex_Describe.Image = global::Nyx.Properties.Resources.magnifier;
            resources.ApplyResources(this._cmIndex_Describe, "_cmIndex_Describe");
            this._cmIndex_Describe.Name = "_cmIndex_Describe";
            this._cmIndex_Describe.Click += new System.EventHandler(this._lvIndexes_DoubleClick);
            // 
            // _cmIndex_Sep0
            // 
            this._cmIndex_Sep0.Name = "_cmIndex_Sep0";
            resources.ApplyResources(this._cmIndex_Sep0, "_cmIndex_Sep0");
            // 
            // _cmIndex_Create
            // 
            this._cmIndex_Create.Image = global::Nyx.Properties.Resources.table_add;
            resources.ApplyResources(this._cmIndex_Create, "_cmIndex_Create");
            this._cmIndex_Create.Name = "_cmIndex_Create";
            this._cmIndex_Create.Click += new System.EventHandler(this._cmIndex_Create_Click);
            // 
            // _cmIndex_Alter
            // 
            this._cmIndex_Alter.Image = global::Nyx.Properties.Resources.table_edit;
            resources.ApplyResources(this._cmIndex_Alter, "_cmIndex_Alter");
            this._cmIndex_Alter.Name = "_cmIndex_Alter";
            this._cmIndex_Alter.Click += new System.EventHandler(this._cmIndex_Alter_Click);
            // 
            // _cmIndex_Drop
            // 
            this._cmIndex_Drop.Image = global::Nyx.Properties.Resources.table_delete;
            resources.ApplyResources(this._cmIndex_Drop, "_cmIndex_Drop");
            this._cmIndex_Drop.Name = "_cmIndex_Drop";
            this._cmIndex_Drop.Click += new System.EventHandler(this._cmIndex_Drop_Click);
            // 
            // _cmIndex_Sep1
            // 
            this._cmIndex_Sep1.Name = "_cmIndex_Sep1";
            resources.ApplyResources(this._cmIndex_Sep1, "_cmIndex_Sep1");
            // 
            // _cmIndex_Refresh
            // 
            this._cmIndex_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._cmIndex_Refresh, "_cmIndex_Refresh");
            this._cmIndex_Refresh.Name = "_cmIndex_Refresh";
            this._cmIndex_Refresh.Click += new System.EventHandler(this._cmIndex_Refresh_Click);
            // 
            // _cmIndex_Sep2
            // 
            this._cmIndex_Sep2.Name = "_cmIndex_Sep2";
            resources.ApplyResources(this._cmIndex_Sep2, "_cmIndex_Sep2");
            // 
            // _cmIndex_SelAll
            // 
            this._cmIndex_SelAll.Image = global::Nyx.Properties.Resources.arrow_redo;
            resources.ApplyResources(this._cmIndex_SelAll, "_cmIndex_SelAll");
            this._cmIndex_SelAll.Name = "_cmIndex_SelAll";
            this._cmIndex_SelAll.Click += new System.EventHandler(this._cmIndex_Selection);
            // 
            // _cmIndex_SelNone
            // 
            this._cmIndex_SelNone.Image = global::Nyx.Properties.Resources.arrow_undo;
            resources.ApplyResources(this._cmIndex_SelNone, "_cmIndex_SelNone");
            this._cmIndex_SelNone.Name = "_cmIndex_SelNone";
            // 
            // _cmIndex_SelInv
            // 
            this._cmIndex_SelInv.Image = global::Nyx.Properties.Resources.arrow_rotate_clockwise;
            resources.ApplyResources(this._cmIndex_SelInv, "_cmIndex_SelInv");
            this._cmIndex_SelInv.Name = "_cmIndex_SelInv";
            // 
            // _lvIndexDesc
            // 
            this._lvIndexDesc.BackColor = System.Drawing.Color.GhostWhite;
            this._lvIndexDesc.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.cname,
            this.dtype,
            this.cpos});
            this._lvIndexDesc.Cursor = System.Windows.Forms.Cursors.Default;
            resources.ApplyResources(this._lvIndexDesc, "_lvIndexDesc");
            this._lvIndexDesc.FullRowSelect = true;
            this._lvIndexDesc.GridLines = true;
            this._lvIndexDesc.Name = "_lvIndexDesc";
            this._lvIndexDesc.UseCompatibleStateImageBehavior = false;
            this._lvIndexDesc.View = System.Windows.Forms.View.Details;
            this._lvIndexDesc.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this._lvIndexDesc_ColumnClick);
            // 
            // cname
            // 
            resources.ApplyResources(this.cname, "cname");
            // 
            // dtype
            // 
            resources.ApplyResources(this.dtype, "dtype");
            // 
            // cpos
            // 
            resources.ApplyResources(this.cpos, "cpos");
            // 
            // _tsTables
            // 
            this._tsTables.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsTables.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsTables_Refresh,
            this._tsTables_Sep0,
            this._tsTables_lblFilter,
            this._tsTables_Filter,
            this._tsTables_Sep3,
            this._tsTables_lblDescTable,
            this._tsTables_tbDescTable,
            this._tsTables_Sep1,
            this._tsTables_lblRetrieve,
            this._tsTables_cbScope,
            this._tsTables_Sep2,
            this._tsTables_lblcurTable,
            this._tsTables_curTable});
            resources.ApplyResources(this._tsTables, "_tsTables");
            this._tsTables.Name = "_tsTables";
            // 
            // _tsTables_Refresh
            // 
            this._tsTables_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsTables_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._tsTables_Refresh, "_tsTables_Refresh");
            this._tsTables_Refresh.Name = "_tsTables_Refresh";
            this._tsTables_Refresh.Click += new System.EventHandler(this._tsTables_Refresh_Click);
            // 
            // _tsTables_Sep0
            // 
            this._tsTables_Sep0.Name = "_tsTables_Sep0";
            resources.ApplyResources(this._tsTables_Sep0, "_tsTables_Sep0");
            // 
            // _tsTables_lblFilter
            // 
            this._tsTables_lblFilter.Name = "_tsTables_lblFilter";
            resources.ApplyResources(this._tsTables_lblFilter, "_tsTables_lblFilter");
            // 
            // _tsTables_Filter
            // 
            this._tsTables_Filter.Name = "_tsTables_Filter";
            resources.ApplyResources(this._tsTables_Filter, "_tsTables_Filter");
            this._tsTables_Filter.TextChanged += new System.EventHandler(this._tsTables_Filter_TextChanged);
            // 
            // _tsTables_Sep3
            // 
            this._tsTables_Sep3.Name = "_tsTables_Sep3";
            resources.ApplyResources(this._tsTables_Sep3, "_tsTables_Sep3");
            // 
            // _tsTables_lblDescTable
            // 
            this._tsTables_lblDescTable.Name = "_tsTables_lblDescTable";
            resources.ApplyResources(this._tsTables_lblDescTable, "_tsTables_lblDescTable");
            // 
            // _tsTables_tbDescTable
            // 
            this._tsTables_tbDescTable.Name = "_tsTables_tbDescTable";
            resources.ApplyResources(this._tsTables_tbDescTable, "_tsTables_tbDescTable");
            this._tsTables_tbDescTable.KeyUp += new System.Windows.Forms.KeyEventHandler(this._tsTables_DescTable_KeyUp);
            // 
            // _tsTables_Sep1
            // 
            this._tsTables_Sep1.Name = "_tsTables_Sep1";
            resources.ApplyResources(this._tsTables_Sep1, "_tsTables_Sep1");
            // 
            // _tsTables_lblRetrieve
            // 
            this._tsTables_lblRetrieve.Name = "_tsTables_lblRetrieve";
            resources.ApplyResources(this._tsTables_lblRetrieve, "_tsTables_lblRetrieve");
            // 
            // _tsTables_cbScope
            // 
            this._tsTables_cbScope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tsTables_cbScope.Items.AddRange(new object[] {
            resources.GetString("_tsTables_cbScope.Items"),
            resources.GetString("_tsTables_cbScope.Items1")});
            this._tsTables_cbScope.Name = "_tsTables_cbScope";
            resources.ApplyResources(this._tsTables_cbScope, "_tsTables_cbScope");
            // 
            // _tsTables_Sep2
            // 
            this._tsTables_Sep2.Name = "_tsTables_Sep2";
            resources.ApplyResources(this._tsTables_Sep2, "_tsTables_Sep2");
            // 
            // _tsTables_lblcurTable
            // 
            this._tsTables_lblcurTable.Name = "_tsTables_lblcurTable";
            resources.ApplyResources(this._tsTables_lblcurTable, "_tsTables_lblcurTable");
            // 
            // _tsTables_curTable
            // 
            this._tsTables_curTable.Name = "_tsTables_curTable";
            resources.ApplyResources(this._tsTables_curTable, "_tsTables_curTable");
            this._tsTables_curTable.TextChanged += new System.EventHandler(this._tsTables_curTable_TextChanged);
            // 
            // _lblNotSupportedTables
            // 
            resources.ApplyResources(this._lblNotSupportedTables, "_lblNotSupportedTables");
            this._lblNotSupportedTables.ForeColor = System.Drawing.Color.Firebrick;
            this._lblNotSupportedTables.Name = "_lblNotSupportedTables";
            // 
            // _tpViews
            // 
            this._tpViews.Controls.Add(this._pnlViews);
            this._tpViews.Controls.Add(this._lblNotSupportedViews);
            resources.ApplyResources(this._tpViews, "_tpViews");
            this._tpViews.Name = "_tpViews";
            this._tpViews.UseVisualStyleBackColor = true;
            // 
            // _pnlViews
            // 
            this._pnlViews.Controls.Add(this._splitViews);
            this._pnlViews.Controls.Add(this._tsViews);
            resources.ApplyResources(this._pnlViews, "_pnlViews");
            this._pnlViews.Name = "_pnlViews";
            // 
            // _splitViews
            // 
            resources.ApplyResources(this._splitViews, "_splitViews");
            this._splitViews.Name = "_splitViews";
            // 
            // _splitViews.Panel1
            // 
            this._splitViews.Panel1.Controls.Add(this._lbViews);
            // 
            // _splitViews.Panel2
            // 
            this._splitViews.Panel2.Controls.Add(this._splitViews2);
            // 
            // _lbViews
            // 
            this._lbViews.BackColor = System.Drawing.Color.GhostWhite;
            this._lbViews.ContextMenuStrip = this._cmViews;
            resources.ApplyResources(this._lbViews, "_lbViews");
            this._lbViews.FormattingEnabled = true;
            this._lbViews.Name = "_lbViews";
            this._lbViews.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this._lbViews.DoubleClick += new System.EventHandler(this._lbViews_DoubleClick);
            this._lbViews.SelectedIndexChanged += new System.EventHandler(this._lbViews_SelectedIndexChanged);
            // 
            // _cmViews
            // 
            this._cmViews.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmViews_Create,
            this._cmViews_Drop,
            this._cmViews_Sep0,
            this._cmViews_Refresh});
            this._cmViews.Name = "_cmViews";
            resources.ApplyResources(this._cmViews, "_cmViews");
            // 
            // _cmViews_Create
            // 
            this._cmViews_Create.Image = global::Nyx.Properties.Resources.table_add;
            resources.ApplyResources(this._cmViews_Create, "_cmViews_Create");
            this._cmViews_Create.Name = "_cmViews_Create";
            this._cmViews_Create.Click += new System.EventHandler(this._views_Create_Click);
            // 
            // _cmViews_Drop
            // 
            resources.ApplyResources(this._cmViews_Drop, "_cmViews_Drop");
            this._cmViews_Drop.Image = global::Nyx.Properties.Resources.table_delete;
            this._cmViews_Drop.Name = "_cmViews_Drop";
            this._cmViews_Drop.Click += new System.EventHandler(this._cmViews_Drop_Click);
            // 
            // _cmViews_Sep0
            // 
            this._cmViews_Sep0.Name = "_cmViews_Sep0";
            resources.ApplyResources(this._cmViews_Sep0, "_cmViews_Sep0");
            // 
            // _cmViews_Refresh
            // 
            this._cmViews_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._cmViews_Refresh, "_cmViews_Refresh");
            this._cmViews_Refresh.Name = "_cmViews_Refresh";
            this._cmViews_Refresh.Click += new System.EventHandler(this._views_Refresh_Click);
            // 
            // _splitViews2
            // 
            resources.ApplyResources(this._splitViews2, "_splitViews2");
            this._splitViews2.Name = "_splitViews2";
            // 
            // _splitViews2.Panel1
            // 
            this._splitViews2.Panel1.Controls.Add(this._lvView_Details);
            // 
            // _splitViews2.Panel2
            // 
            this._splitViews2.Panel2.Controls.Add(this._rtbView_CreateStm);
            // 
            // _lvView_Details
            // 
            this._lvView_Details.BackColor = System.Drawing.Color.GhostWhite;
            this._lvView_Details.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._lvcView_Variable,
            this._lvcView_Value});
            resources.ApplyResources(this._lvView_Details, "_lvView_Details");
            this._lvView_Details.FullRowSelect = true;
            this._lvView_Details.GridLines = true;
            this._lvView_Details.Name = "_lvView_Details";
            this._lvView_Details.UseCompatibleStateImageBehavior = false;
            this._lvView_Details.View = System.Windows.Forms.View.Details;
            // 
            // _lvcView_Variable
            // 
            resources.ApplyResources(this._lvcView_Variable, "_lvcView_Variable");
            // 
            // _lvcView_Value
            // 
            resources.ApplyResources(this._lvcView_Value, "_lvcView_Value");
            // 
            // _rtbView_CreateStm
            // 
            this._rtbView_CreateStm.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._rtbView_CreateStm, "_rtbView_CreateStm");
            this._rtbView_CreateStm.EnableAutoDragDrop = true;
            this._rtbView_CreateStm.HideSelection = false;
            this._rtbView_CreateStm.Name = "_rtbView_CreateStm";
            this._rtbView_CreateStm.ReadOnly = true;
            // 
            // _tsViews
            // 
            this._tsViews.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsViews.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsViews_Refresh,
            this._tsViews_Create,
            this._tsViews_Sep0,
            this._tsViews_lblFilter,
            this._tsViews_Filter,
            this._tsViews_Sep1,
            this._tsViews_lblRetrieve,
            this._tsViews_cbScopeve,
            this._tsViews_Sep3,
            this._tsViews_lblCurTable,
            this._tsViews_curView});
            resources.ApplyResources(this._tsViews, "_tsViews");
            this._tsViews.Name = "_tsViews";
            // 
            // _tsViews_Refresh
            // 
            this._tsViews_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsViews_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._tsViews_Refresh, "_tsViews_Refresh");
            this._tsViews_Refresh.Name = "_tsViews_Refresh";
            this._tsViews_Refresh.Click += new System.EventHandler(this._views_Refresh_Click);
            // 
            // _tsViews_Create
            // 
            this._tsViews_Create.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsViews_Create, "_tsViews_Create");
            this._tsViews_Create.Image = global::Nyx.Properties.Resources.table_add;
            this._tsViews_Create.Name = "_tsViews_Create";
            this._tsViews_Create.Click += new System.EventHandler(this._views_Create_Click);
            // 
            // _tsViews_Sep0
            // 
            this._tsViews_Sep0.Name = "_tsViews_Sep0";
            resources.ApplyResources(this._tsViews_Sep0, "_tsViews_Sep0");
            // 
            // _tsViews_lblFilter
            // 
            this._tsViews_lblFilter.Name = "_tsViews_lblFilter";
            resources.ApplyResources(this._tsViews_lblFilter, "_tsViews_lblFilter");
            // 
            // _tsViews_Filter
            // 
            this._tsViews_Filter.Name = "_tsViews_Filter";
            resources.ApplyResources(this._tsViews_Filter, "_tsViews_Filter");
            this._tsViews_Filter.TextChanged += new System.EventHandler(this._tsViews_Filter_TextChanged);
            // 
            // _tsViews_Sep1
            // 
            this._tsViews_Sep1.Name = "_tsViews_Sep1";
            resources.ApplyResources(this._tsViews_Sep1, "_tsViews_Sep1");
            // 
            // _tsViews_lblRetrieve
            // 
            this._tsViews_lblRetrieve.Name = "_tsViews_lblRetrieve";
            resources.ApplyResources(this._tsViews_lblRetrieve, "_tsViews_lblRetrieve");
            // 
            // _tsViews_cbScopeve
            // 
            this._tsViews_cbScopeve.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tsViews_cbScopeve.Items.AddRange(new object[] {
            resources.GetString("_tsViews_cbScopeve.Items"),
            resources.GetString("_tsViews_cbScopeve.Items1")});
            this._tsViews_cbScopeve.Name = "_tsViews_cbScopeve";
            resources.ApplyResources(this._tsViews_cbScopeve, "_tsViews_cbScopeve");
            // 
            // _tsViews_Sep3
            // 
            this._tsViews_Sep3.Name = "_tsViews_Sep3";
            resources.ApplyResources(this._tsViews_Sep3, "_tsViews_Sep3");
            // 
            // _tsViews_lblCurTable
            // 
            this._tsViews_lblCurTable.Name = "_tsViews_lblCurTable";
            resources.ApplyResources(this._tsViews_lblCurTable, "_tsViews_lblCurTable");
            // 
            // _tsViews_curView
            // 
            this._tsViews_curView.Name = "_tsViews_curView";
            resources.ApplyResources(this._tsViews_curView, "_tsViews_curView");
            // 
            // _lblNotSupportedViews
            // 
            resources.ApplyResources(this._lblNotSupportedViews, "_lblNotSupportedViews");
            this._lblNotSupportedViews.ForeColor = System.Drawing.Color.Firebrick;
            this._lblNotSupportedViews.Name = "_lblNotSupportedViews";
            // 
            // _tpProcedures
            // 
            this._tpProcedures.BackColor = System.Drawing.Color.Transparent;
            this._tpProcedures.Controls.Add(this._pnlProcedures);
            this._tpProcedures.Controls.Add(this._lblNotSupportedProcedures);
            resources.ApplyResources(this._tpProcedures, "_tpProcedures");
            this._tpProcedures.Name = "_tpProcedures";
            this._tpProcedures.UseVisualStyleBackColor = true;
            // 
            // _pnlProcedures
            // 
            this._pnlProcedures.Controls.Add(this._splitProcedures);
            this._pnlProcedures.Controls.Add(this._tsPrcdrs);
            resources.ApplyResources(this._pnlProcedures, "_pnlProcedures");
            this._pnlProcedures.Name = "_pnlProcedures";
            // 
            // _splitProcedures
            // 
            resources.ApplyResources(this._splitProcedures, "_splitProcedures");
            this._splitProcedures.Name = "_splitProcedures";
            // 
            // _splitProcedures.Panel1
            // 
            this._splitProcedures.Panel1.Controls.Add(this._lbPrcdrs);
            // 
            // _splitProcedures.Panel2
            // 
            this._splitProcedures.Panel2.Controls.Add(this._splitProcedures2);
            // 
            // _lbPrcdrs
            // 
            this._lbPrcdrs.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._lbPrcdrs, "_lbPrcdrs");
            this._lbPrcdrs.FormattingEnabled = true;
            this._lbPrcdrs.Name = "_lbPrcdrs";
            this._lbPrcdrs.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this._lbPrcdrs.DoubleClick += new System.EventHandler(this._lbProcedures_DoubleClick);
            // 
            // _splitProcedures2
            // 
            resources.ApplyResources(this._splitProcedures2, "_splitProcedures2");
            this._splitProcedures2.Name = "_splitProcedures2";
            // 
            // _splitProcedures2.Panel1
            // 
            this._splitProcedures2.Panel1.Controls.Add(this._lvPrcdr_Details);
            // 
            // _splitProcedures2.Panel2
            // 
            this._splitProcedures2.Panel2.Controls.Add(this._rtbPrcdr_CreateStm);
            // 
            // _lvPrcdr_Details
            // 
            this._lvPrcdr_Details.BackColor = System.Drawing.Color.GhostWhite;
            this._lvPrcdr_Details.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._lvcPrcdrs_Variable,
            this._lvcPrcdrs_Value});
            resources.ApplyResources(this._lvPrcdr_Details, "_lvPrcdr_Details");
            this._lvPrcdr_Details.FullRowSelect = true;
            this._lvPrcdr_Details.GridLines = true;
            this._lvPrcdr_Details.Name = "_lvPrcdr_Details";
            this._lvPrcdr_Details.UseCompatibleStateImageBehavior = false;
            this._lvPrcdr_Details.View = System.Windows.Forms.View.Details;
            // 
            // _lvcPrcdrs_Variable
            // 
            resources.ApplyResources(this._lvcPrcdrs_Variable, "_lvcPrcdrs_Variable");
            // 
            // _lvcPrcdrs_Value
            // 
            resources.ApplyResources(this._lvcPrcdrs_Value, "_lvcPrcdrs_Value");
            // 
            // _rtbPrcdr_CreateStm
            // 
            this._rtbPrcdr_CreateStm.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._rtbPrcdr_CreateStm, "_rtbPrcdr_CreateStm");
            this._rtbPrcdr_CreateStm.EnableAutoDragDrop = true;
            this._rtbPrcdr_CreateStm.HideSelection = false;
            this._rtbPrcdr_CreateStm.Name = "_rtbPrcdr_CreateStm";
            this._rtbPrcdr_CreateStm.ReadOnly = true;
            // 
            // _tsPrcdrs
            // 
            this._tsPrcdrs.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsPrcdrs.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsPrcdrs_Refresh,
            this._tsPrcdrs_Sep0,
            this._tsPrcdrs_lblFilter,
            this._tsPrcdrs_Filter,
            this._tsPrcdrs_Sep1,
            this._tsPrcdrs_lblRetrieve,
            this._tsPrcdrs_cbScope,
            this._tsPrcdrs_Sep3,
            this._tsPrcdrs_lblCurTable,
            this._tsPrcdrs_curProcedure});
            resources.ApplyResources(this._tsPrcdrs, "_tsPrcdrs");
            this._tsPrcdrs.Name = "_tsPrcdrs";
            // 
            // _tsPrcdrs_Refresh
            // 
            this._tsPrcdrs_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsPrcdrs_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._tsPrcdrs_Refresh, "_tsPrcdrs_Refresh");
            this._tsPrcdrs_Refresh.Name = "_tsPrcdrs_Refresh";
            this._tsPrcdrs_Refresh.Click += new System.EventHandler(this._tsPrcdrs_Refresh_Click);
            // 
            // _tsPrcdrs_Sep0
            // 
            this._tsPrcdrs_Sep0.Name = "_tsPrcdrs_Sep0";
            resources.ApplyResources(this._tsPrcdrs_Sep0, "_tsPrcdrs_Sep0");
            // 
            // _tsPrcdrs_lblFilter
            // 
            this._tsPrcdrs_lblFilter.Name = "_tsPrcdrs_lblFilter";
            resources.ApplyResources(this._tsPrcdrs_lblFilter, "_tsPrcdrs_lblFilter");
            // 
            // _tsPrcdrs_Filter
            // 
            this._tsPrcdrs_Filter.Name = "_tsPrcdrs_Filter";
            resources.ApplyResources(this._tsPrcdrs_Filter, "_tsPrcdrs_Filter");
            this._tsPrcdrs_Filter.TextChanged += new System.EventHandler(this._tsPrcdrs_Filter_TextChanged);
            // 
            // _tsPrcdrs_Sep1
            // 
            this._tsPrcdrs_Sep1.Name = "_tsPrcdrs_Sep1";
            resources.ApplyResources(this._tsPrcdrs_Sep1, "_tsPrcdrs_Sep1");
            // 
            // _tsPrcdrs_lblRetrieve
            // 
            this._tsPrcdrs_lblRetrieve.Name = "_tsPrcdrs_lblRetrieve";
            resources.ApplyResources(this._tsPrcdrs_lblRetrieve, "_tsPrcdrs_lblRetrieve");
            // 
            // _tsPrcdrs_cbScope
            // 
            this._tsPrcdrs_cbScope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tsPrcdrs_cbScope.Items.AddRange(new object[] {
            resources.GetString("_tsPrcdrs_cbScope.Items"),
            resources.GetString("_tsPrcdrs_cbScope.Items1")});
            this._tsPrcdrs_cbScope.Name = "_tsPrcdrs_cbScope";
            resources.ApplyResources(this._tsPrcdrs_cbScope, "_tsPrcdrs_cbScope");
            // 
            // _tsPrcdrs_Sep3
            // 
            this._tsPrcdrs_Sep3.Name = "_tsPrcdrs_Sep3";
            resources.ApplyResources(this._tsPrcdrs_Sep3, "_tsPrcdrs_Sep3");
            // 
            // _tsPrcdrs_lblCurTable
            // 
            this._tsPrcdrs_lblCurTable.Name = "_tsPrcdrs_lblCurTable";
            resources.ApplyResources(this._tsPrcdrs_lblCurTable, "_tsPrcdrs_lblCurTable");
            // 
            // _tsPrcdrs_curProcedure
            // 
            this._tsPrcdrs_curProcedure.Name = "_tsPrcdrs_curProcedure";
            resources.ApplyResources(this._tsPrcdrs_curProcedure, "_tsPrcdrs_curProcedure");
            // 
            // _lblNotSupportedProcedures
            // 
            resources.ApplyResources(this._lblNotSupportedProcedures, "_lblNotSupportedProcedures");
            this._lblNotSupportedProcedures.ForeColor = System.Drawing.Color.Firebrick;
            this._lblNotSupportedProcedures.Name = "_lblNotSupportedProcedures";
            // 
            // _tpTriggers
            // 
            this._tpTriggers.Controls.Add(this._pnlTriggers);
            this._tpTriggers.Controls.Add(this._lblNotSupportedTriggers);
            resources.ApplyResources(this._tpTriggers, "_tpTriggers");
            this._tpTriggers.Name = "_tpTriggers";
            this._tpTriggers.UseVisualStyleBackColor = true;
            // 
            // _pnlTriggers
            // 
            this._pnlTriggers.Controls.Add(this._splitTriggers);
            this._pnlTriggers.Controls.Add(this._tsTriggers);
            resources.ApplyResources(this._pnlTriggers, "_pnlTriggers");
            this._pnlTriggers.Name = "_pnlTriggers";
            // 
            // _splitTriggers
            // 
            resources.ApplyResources(this._splitTriggers, "_splitTriggers");
            this._splitTriggers.Name = "_splitTriggers";
            // 
            // _splitTriggers.Panel1
            // 
            this._splitTriggers.Panel1.Controls.Add(this._lbTriggers);
            // 
            // _splitTriggers.Panel2
            // 
            this._splitTriggers.Panel2.Controls.Add(this._splitTriggers2);
            // 
            // _lbTriggers
            // 
            this._lbTriggers.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._lbTriggers, "_lbTriggers");
            this._lbTriggers.FormattingEnabled = true;
            this._lbTriggers.Name = "_lbTriggers";
            this._lbTriggers.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this._lbTriggers.DoubleClick += new System.EventHandler(this._lbTriggers_DoubleClick);
            // 
            // _splitTriggers2
            // 
            resources.ApplyResources(this._splitTriggers2, "_splitTriggers2");
            this._splitTriggers2.Name = "_splitTriggers2";
            // 
            // _splitTriggers2.Panel1
            // 
            this._splitTriggers2.Panel1.Controls.Add(this._lvTrigger_Details);
            // 
            // _splitTriggers2.Panel2
            // 
            this._splitTriggers2.Panel2.Controls.Add(this._rtbTrigger_CrtStm);
            // 
            // _lvTrigger_Details
            // 
            this._lvTrigger_Details.BackColor = System.Drawing.Color.GhostWhite;
            this._lvTrigger_Details.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this._lvcTrigger_Variable,
            this._lvcTrigger_Value});
            resources.ApplyResources(this._lvTrigger_Details, "_lvTrigger_Details");
            this._lvTrigger_Details.FullRowSelect = true;
            this._lvTrigger_Details.GridLines = true;
            this._lvTrigger_Details.Name = "_lvTrigger_Details";
            this._lvTrigger_Details.UseCompatibleStateImageBehavior = false;
            this._lvTrigger_Details.View = System.Windows.Forms.View.Details;
            // 
            // _lvcTrigger_Variable
            // 
            resources.ApplyResources(this._lvcTrigger_Variable, "_lvcTrigger_Variable");
            // 
            // _lvcTrigger_Value
            // 
            resources.ApplyResources(this._lvcTrigger_Value, "_lvcTrigger_Value");
            // 
            // _rtbTrigger_CrtStm
            // 
            this._rtbTrigger_CrtStm.BackColor = System.Drawing.Color.GhostWhite;
            resources.ApplyResources(this._rtbTrigger_CrtStm, "_rtbTrigger_CrtStm");
            this._rtbTrigger_CrtStm.EnableAutoDragDrop = true;
            this._rtbTrigger_CrtStm.HideSelection = false;
            this._rtbTrigger_CrtStm.Name = "_rtbTrigger_CrtStm";
            this._rtbTrigger_CrtStm.ReadOnly = true;
            // 
            // _tsTriggers
            // 
            this._tsTriggers.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this._tsTriggers.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsTriggers_Refresh,
            this._tsTriggers_Sep0,
            this._tsTriggers_lblFilter,
            this._tsTriggers_Filter,
            this._tsTriggers_Sep1,
            this._tsTriggers_lblRetrieve,
            this._tsTriggers_cbScope,
            this._tsTriggers_Sep3,
            this._tsTriggers_lblCurTable,
            this._tsTriggers_curTriggers});
            resources.ApplyResources(this._tsTriggers, "_tsTriggers");
            this._tsTriggers.Name = "_tsTriggers";
            // 
            // _tsTriggers_Refresh
            // 
            this._tsTriggers_Refresh.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsTriggers_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._tsTriggers_Refresh, "_tsTriggers_Refresh");
            this._tsTriggers_Refresh.Name = "_tsTriggers_Refresh";
            this._tsTriggers_Refresh.Click += new System.EventHandler(this._tsTriggers_Refresh_Click);
            // 
            // _tsTriggers_Sep0
            // 
            this._tsTriggers_Sep0.Name = "_tsTriggers_Sep0";
            resources.ApplyResources(this._tsTriggers_Sep0, "_tsTriggers_Sep0");
            // 
            // _tsTriggers_lblFilter
            // 
            this._tsTriggers_lblFilter.Name = "_tsTriggers_lblFilter";
            resources.ApplyResources(this._tsTriggers_lblFilter, "_tsTriggers_lblFilter");
            // 
            // _tsTriggers_Filter
            // 
            this._tsTriggers_Filter.Name = "_tsTriggers_Filter";
            resources.ApplyResources(this._tsTriggers_Filter, "_tsTriggers_Filter");
            this._tsTriggers_Filter.TextChanged += new System.EventHandler(this._tsTriggers_Filter_TextChanged);
            // 
            // _tsTriggers_Sep1
            // 
            this._tsTriggers_Sep1.Name = "_tsTriggers_Sep1";
            resources.ApplyResources(this._tsTriggers_Sep1, "_tsTriggers_Sep1");
            // 
            // _tsTriggers_lblRetrieve
            // 
            this._tsTriggers_lblRetrieve.Name = "_tsTriggers_lblRetrieve";
            resources.ApplyResources(this._tsTriggers_lblRetrieve, "_tsTriggers_lblRetrieve");
            // 
            // _tsTriggers_cbScope
            // 
            this._tsTriggers_cbScope.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tsTriggers_cbScope.Items.AddRange(new object[] {
            resources.GetString("_tsTriggers_cbScope.Items"),
            resources.GetString("_tsTriggers_cbScope.Items1")});
            this._tsTriggers_cbScope.Name = "_tsTriggers_cbScope";
            resources.ApplyResources(this._tsTriggers_cbScope, "_tsTriggers_cbScope");
            // 
            // _tsTriggers_Sep3
            // 
            this._tsTriggers_Sep3.Name = "_tsTriggers_Sep3";
            resources.ApplyResources(this._tsTriggers_Sep3, "_tsTriggers_Sep3");
            // 
            // _tsTriggers_lblCurTable
            // 
            this._tsTriggers_lblCurTable.Name = "_tsTriggers_lblCurTable";
            resources.ApplyResources(this._tsTriggers_lblCurTable, "_tsTriggers_lblCurTable");
            // 
            // _tsTriggers_curTriggers
            // 
            this._tsTriggers_curTriggers.Name = "_tsTriggers_curTriggers";
            resources.ApplyResources(this._tsTriggers_curTriggers, "_tsTriggers_curTriggers");
            // 
            // _lblNotSupportedTriggers
            // 
            resources.ApplyResources(this._lblNotSupportedTriggers, "_lblNotSupportedTriggers");
            this._lblNotSupportedTriggers.ForeColor = System.Drawing.Color.Firebrick;
            this._lblNotSupportedTriggers.Name = "_lblNotSupportedTriggers";
            // 
            // _tsMain
            // 
            this._tsMain.AccessibleRole = System.Windows.Forms.AccessibleRole.ToolBar;
            resources.ApplyResources(this._tsMain, "_tsMain");
            this._tsMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._tsMain_SaveQC,
            this._tsMain_QuickConnect,
            this._tsMain_cbProfiles,
            this._tsMain_Connect,
            this._tsMain_Disconnect,
            this._tsMain_Sep0,
            this._tsMain_cbChangeDb});
            this._tsMain.Name = "_tsMain";
            this._tsMain.TabStop = true;
            // 
            // _tsMain_SaveQC
            // 
            this._tsMain_SaveQC.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsMain_SaveQC, "_tsMain_SaveQC");
            this._tsMain_SaveQC.Image = global::Nyx.Properties.Resources.lightning_add;
            this._tsMain_SaveQC.Name = "_tsMain_SaveQC";
            this._tsMain_SaveQC.Click += new System.EventHandler(this._tsMain_SaveQC_Click);
            // 
            // _tsMain_QuickConnect
            // 
            this._tsMain_QuickConnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsMain_QuickConnect.Image = global::Nyx.Properties.Resources.lightning_go;
            resources.ApplyResources(this._tsMain_QuickConnect, "_tsMain_QuickConnect");
            this._tsMain_QuickConnect.Name = "_tsMain_QuickConnect";
            this._tsMain_QuickConnect.Click += new System.EventHandler(this._Main_QuickConnect_Click);
            // 
            // _tsMain_cbProfiles
            // 
            this._tsMain_cbProfiles.BackColor = System.Drawing.Color.GhostWhite;
            this._tsMain_cbProfiles.DropDownHeight = 150;
            this._tsMain_cbProfiles.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this._tsMain_cbProfiles.DropDownWidth = 150;
            resources.ApplyResources(this._tsMain_cbProfiles, "_tsMain_cbProfiles");
            this._tsMain_cbProfiles.Name = "_tsMain_cbProfiles";
            this._tsMain_cbProfiles.KeyDown += new System.Windows.Forms.KeyEventHandler(this._tsMain_cbProfiles_KeyDown);
            this._tsMain_cbProfiles.SelectedIndexChanged += new System.EventHandler(this._tsMain_Profiles_SelectedIndexChanged);
            // 
            // _tsMain_Connect
            // 
            this._tsMain_Connect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this._tsMain_Connect.Image = global::Nyx.Properties.Resources.connect;
            resources.ApplyResources(this._tsMain_Connect, "_tsMain_Connect");
            this._tsMain_Connect.Name = "_tsMain_Connect";
            this._tsMain_Connect.Click += new System.EventHandler(this._Main_Connect_Click);
            // 
            // _tsMain_Disconnect
            // 
            this._tsMain_Disconnect.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            resources.ApplyResources(this._tsMain_Disconnect, "_tsMain_Disconnect");
            this._tsMain_Disconnect.Image = global::Nyx.Properties.Resources.disconnect;
            this._tsMain_Disconnect.Name = "_tsMain_Disconnect";
            this._tsMain_Disconnect.Click += new System.EventHandler(this._Main_Disconnect_Click);
            // 
            // _tsMain_Sep0
            // 
            this._tsMain_Sep0.Name = "_tsMain_Sep0";
            resources.ApplyResources(this._tsMain_Sep0, "_tsMain_Sep0");
            // 
            // _tsMain_cbChangeDb
            // 
            this._tsMain_cbChangeDb.BackColor = System.Drawing.Color.GhostWhite;
            this._tsMain_cbChangeDb.Name = "_tsMain_cbChangeDb";
            resources.ApplyResources(this._tsMain_cbChangeDb, "_tsMain_cbChangeDb");
            this._tsMain_cbChangeDb.KeyDown += new System.Windows.Forms.KeyEventHandler(this._tsMain_cbChgDb_KeyDown);
            this._tsMain_cbChangeDb.SelectedIndexChanged += new System.EventHandler(this._tsMain_cbChgDb_SelectedIndexChanged);
            // 
            // sql1
            // 
            resources.ApplyResources(this.sql1, "sql1");
            this.sql1.Name = "sql1";
            // 
            // _menu
            // 
            resources.ApplyResources(this._menu, "_menu");
            this._menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msFile,
            this._msSql,
            this._msPrv,
            this._msView,
            this._msBookmarks,
            this._msDbT,
            this._msOrcl,
            this._msMySql,
            this._msHelp});
            this._menu.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.HorizontalStackWithOverflow;
            this._menu.Name = "_menu";
            // 
            // _msFile
            // 
            this._msFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msFile_Connect,
            this._msFile_Disconnect,
            this._msFile_QuickConnect,
            this._msFile_Sep2,
            this._msFile_Profiles,
            this._msFile_Settings,
            this._msFile_Sep0,
            this._msFile_ChkUpdate,
            this._msFile_Sep1,
            this._msFile_Exit});
            this._msFile.Name = "_msFile";
            resources.ApplyResources(this._msFile, "_msFile");
            // 
            // _msFile_Connect
            // 
            this._msFile_Connect.Image = global::Nyx.Properties.Resources.connect;
            resources.ApplyResources(this._msFile_Connect, "_msFile_Connect");
            this._msFile_Connect.Name = "_msFile_Connect";
            this._msFile_Connect.Click += new System.EventHandler(this._Main_Connect_Click);
            // 
            // _msFile_Disconnect
            // 
            resources.ApplyResources(this._msFile_Disconnect, "_msFile_Disconnect");
            this._msFile_Disconnect.Image = global::Nyx.Properties.Resources.disconnect;
            this._msFile_Disconnect.Name = "_msFile_Disconnect";
            this._msFile_Disconnect.Click += new System.EventHandler(this._Main_Disconnect_Click);
            // 
            // _msFile_QuickConnect
            // 
            this._msFile_QuickConnect.Image = global::Nyx.Properties.Resources.lightning_go;
            resources.ApplyResources(this._msFile_QuickConnect, "_msFile_QuickConnect");
            this._msFile_QuickConnect.Name = "_msFile_QuickConnect";
            this._msFile_QuickConnect.Click += new System.EventHandler(this._Main_QuickConnect_Click);
            // 
            // _msFile_Sep2
            // 
            this._msFile_Sep2.Name = "_msFile_Sep2";
            resources.ApplyResources(this._msFile_Sep2, "_msFile_Sep2");
            // 
            // _msFile_Profiles
            // 
            this._msFile_Profiles.Image = global::Nyx.Properties.Resources.lightning;
            resources.ApplyResources(this._msFile_Profiles, "_msFile_Profiles");
            this._msFile_Profiles.Name = "_msFile_Profiles";
            this._msFile_Profiles.Click += new System.EventHandler(this._msProfiles_Click);
            // 
            // _msFile_Settings
            // 
            this._msFile_Settings.Image = global::Nyx.Properties.Resources.overlays;
            resources.ApplyResources(this._msFile_Settings, "_msFile_Settings");
            this._msFile_Settings.Name = "_msFile_Settings";
            this._msFile_Settings.Click += new System.EventHandler(this._msSettings_Click);
            // 
            // _msFile_Sep0
            // 
            this._msFile_Sep0.Name = "_msFile_Sep0";
            resources.ApplyResources(this._msFile_Sep0, "_msFile_Sep0");
            // 
            // _msFile_ChkUpdate
            // 
            this._msFile_ChkUpdate.Image = global::Nyx.Properties.Resources.world_link;
            resources.ApplyResources(this._msFile_ChkUpdate, "_msFile_ChkUpdate");
            this._msFile_ChkUpdate.Name = "_msFile_ChkUpdate";
            this._msFile_ChkUpdate.Click += new System.EventHandler(this._msCheckUpdate_Click);
            // 
            // _msFile_Sep1
            // 
            this._msFile_Sep1.Name = "_msFile_Sep1";
            resources.ApplyResources(this._msFile_Sep1, "_msFile_Sep1");
            // 
            // _msFile_Exit
            // 
            this._msFile_Exit.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._msFile_Exit, "_msFile_Exit");
            this._msFile_Exit.Name = "_msFile_Exit";
            this._msFile_Exit.Click += new System.EventHandler(this._ExitApp);
            // 
            // _msSql
            // 
            this._msSql.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msSql_EditData,
            this._msSql_UpdData,
            this._msSql_Sep1,
            this._msSql_ExecSql,
            this._msSql_ExecSel,
            this._msSql_Sep2,
            this._msSql_ClearSql,
            this._msSql_ClearHistory,
            this._msSql_Sep3,
            this._msSql_SqlTabs});
            this._msSql.Name = "_msSql";
            resources.ApplyResources(this._msSql, "_msSql");
            // 
            // _msSql_EditData
            // 
            resources.ApplyResources(this._msSql_EditData, "_msSql_EditData");
            this._msSql_EditData.Image = global::Nyx.Properties.Resources.database_edit;
            this._msSql_EditData.Name = "_msSql_EditData";
            this._msSql_EditData.Click += new System.EventHandler(this._previewEditData_Click);
            // 
            // _msSql_UpdData
            // 
            resources.ApplyResources(this._msSql_UpdData, "_msSql_UpdData");
            this._msSql_UpdData.Image = global::Nyx.Properties.Resources.database_save;
            this._msSql_UpdData.Name = "_msSql_UpdData";
            this._msSql_UpdData.Click += new System.EventHandler(this._previewUpdData_Click);
            // 
            // _msSql_Sep1
            // 
            this._msSql_Sep1.Name = "_msSql_Sep1";
            resources.ApplyResources(this._msSql_Sep1, "_msSql_Sep1");
            // 
            // _msSql_ExecSql
            // 
            this._msSql_ExecSql.Image = global::Nyx.Properties.Resources.control_play_blue;
            resources.ApplyResources(this._msSql_ExecSql, "_msSql_ExecSql");
            this._msSql_ExecSql.Name = "_msSql_ExecSql";
            this._msSql_ExecSql.Click += new System.EventHandler(this._msDb_ExecSql_Click);
            // 
            // _msSql_ExecSel
            // 
            resources.ApplyResources(this._msSql_ExecSel, "_msSql_ExecSel");
            this._msSql_ExecSel.Image = global::Nyx.Properties.Resources.control_play;
            this._msSql_ExecSel.Name = "_msSql_ExecSel";
            this._msSql_ExecSel.Click += new System.EventHandler(this._msDb_ExecSel_Click);
            // 
            // _msSql_Sep2
            // 
            this._msSql_Sep2.Name = "_msSql_Sep2";
            resources.ApplyResources(this._msSql_Sep2, "_msSql_Sep2");
            // 
            // _msSql_ClearSql
            // 
            this._msSql_ClearSql.Image = global::Nyx.Properties.Resources.book_delete;
            resources.ApplyResources(this._msSql_ClearSql, "_msSql_ClearSql");
            this._msSql_ClearSql.Name = "_msSql_ClearSql";
            this._msSql_ClearSql.Click += new System.EventHandler(this._msDb_ClearSql_Click);
            // 
            // _msSql_ClearHistory
            // 
            this._msSql_ClearHistory.Image = global::Nyx.Properties.Resources.book_delete;
            resources.ApplyResources(this._msSql_ClearHistory, "_msSql_ClearHistory");
            this._msSql_ClearHistory.Name = "_msSql_ClearHistory";
            // 
            // _msSql_Sep3
            // 
            this._msSql_Sep3.Name = "_msSql_Sep3";
            resources.ApplyResources(this._msSql_Sep3, "_msSql_Sep3");
            // 
            // _msSql_SqlTabs
            // 
            this._msSql_SqlTabs.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msSql_SqlTabs_NewTab,
            this._msSql_SqlTabs_CloseTab});
            this._msSql_SqlTabs.Name = "_msSql_SqlTabs";
            resources.ApplyResources(this._msSql_SqlTabs, "_msSql_SqlTabs");
            // 
            // _msSql_SqlTabs_NewTab
            // 
            this._msSql_SqlTabs_NewTab.Image = global::Nyx.Properties.Resources.tab_add;
            resources.ApplyResources(this._msSql_SqlTabs_NewTab, "_msSql_SqlTabs_NewTab");
            this._msSql_SqlTabs_NewTab.Name = "_msSql_SqlTabs_NewTab";
            this._msSql_SqlTabs_NewTab.Click += new System.EventHandler(this._msDb_SqlTabs_NewTab_Click);
            // 
            // _msSql_SqlTabs_CloseTab
            // 
            this._msSql_SqlTabs_CloseTab.Image = global::Nyx.Properties.Resources.tab_delete;
            resources.ApplyResources(this._msSql_SqlTabs_CloseTab, "_msSql_SqlTabs_CloseTab");
            this._msSql_SqlTabs_CloseTab.Name = "_msSql_SqlTabs_CloseTab";
            this._msSql_SqlTabs_CloseTab.Click += new System.EventHandler(this._msDb_SqlTabs_CloseTab_Click);
            // 
            // _msPrv
            // 
            this._msPrv.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msPrvObj_Refresh,
            this._msPrvDta_Refresh,
            this._msPrv_Sep0,
            this._msPrvDta_Edit,
            this._msPrv_Update});
            resources.ApplyResources(this._msPrv, "_msPrv");
            this._msPrv.Name = "_msPrv";
            // 
            // _msPrvObj_Refresh
            // 
            this._msPrvObj_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._msPrvObj_Refresh, "_msPrvObj_Refresh");
            this._msPrvObj_Refresh.Name = "_msPrvObj_Refresh";
            this._msPrvObj_Refresh.Click += new System.EventHandler(this._previewRefreshObjects_Click);
            // 
            // _msPrvDta_Refresh
            // 
            resources.ApplyResources(this._msPrvDta_Refresh, "_msPrvDta_Refresh");
            this._msPrvDta_Refresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            this._msPrvDta_Refresh.Name = "_msPrvDta_Refresh";
            this._msPrvDta_Refresh.Click += new System.EventHandler(this._previewRefreshPreview_Click);
            // 
            // _msPrv_Sep0
            // 
            this._msPrv_Sep0.Name = "_msPrv_Sep0";
            resources.ApplyResources(this._msPrv_Sep0, "_msPrv_Sep0");
            // 
            // _msPrvDta_Edit
            // 
            resources.ApplyResources(this._msPrvDta_Edit, "_msPrvDta_Edit");
            this._msPrvDta_Edit.Image = global::Nyx.Properties.Resources.database_edit;
            this._msPrvDta_Edit.Name = "_msPrvDta_Edit";
            // 
            // _msPrv_Update
            // 
            resources.ApplyResources(this._msPrv_Update, "_msPrv_Update");
            this._msPrv_Update.Image = global::Nyx.Properties.Resources.database_add;
            this._msPrv_Update.Name = "_msPrv_Update";
            this._msPrv_Update.Click += new System.EventHandler(this._previewUpdData_Click);
            // 
            // _msView
            // 
            this._msView.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msView_Sql,
            this._msView_Tables,
            this._msView_Preview,
            this._msView_Views,
            this._msView_Prcdrs,
            this._msView_Triggers,
            this._msView_sep0,
            this._msViewTStrips,
            this._msView_sep1,
            this._msView_SysTray});
            this._msView.Name = "_msView";
            resources.ApplyResources(this._msView, "_msView");
            // 
            // _msView_Sql
            // 
            this._msView_Sql.Checked = true;
            this._msView_Sql.CheckOnClick = true;
            this._msView_Sql.CheckState = System.Windows.Forms.CheckState.Checked;
            resources.ApplyResources(this._msView_Sql, "_msView_Sql");
            this._msView_Sql.Name = "_msView_Sql";
            this._msView_Sql.Click += new System.EventHandler(this._msView_Click);
            // 
            // _msView_Tables
            // 
            this._msView_Tables.CheckOnClick = true;
            resources.ApplyResources(this._msView_Tables, "_msView_Tables");
            this._msView_Tables.Name = "_msView_Tables";
            this._msView_Tables.Click += new System.EventHandler(this._msView_Click);
            // 
            // _msView_Preview
            // 
            this._msView_Preview.CheckOnClick = true;
            resources.ApplyResources(this._msView_Preview, "_msView_Preview");
            this._msView_Preview.Name = "_msView_Preview";
            this._msView_Preview.Click += new System.EventHandler(this._msView_Click);
            // 
            // _msView_Views
            // 
            this._msView_Views.CheckOnClick = true;
            resources.ApplyResources(this._msView_Views, "_msView_Views");
            this._msView_Views.Name = "_msView_Views";
            this._msView_Views.Click += new System.EventHandler(this._msView_Click);
            // 
            // _msView_Prcdrs
            // 
            this._msView_Prcdrs.CheckOnClick = true;
            resources.ApplyResources(this._msView_Prcdrs, "_msView_Prcdrs");
            this._msView_Prcdrs.Name = "_msView_Prcdrs";
            this._msView_Prcdrs.Click += new System.EventHandler(this._msView_Click);
            // 
            // _msView_Triggers
            // 
            this._msView_Triggers.CheckOnClick = true;
            resources.ApplyResources(this._msView_Triggers, "_msView_Triggers");
            this._msView_Triggers.Name = "_msView_Triggers";
            this._msView_Triggers.Click += new System.EventHandler(this._msView_Click);
            // 
            // _msView_sep0
            // 
            this._msView_sep0.Name = "_msView_sep0";
            resources.ApplyResources(this._msView_sep0, "_msView_sep0");
            // 
            // _msViewTStrips
            // 
            this._msViewTStrips.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msViewTStrips_Main});
            this._msViewTStrips.Name = "_msViewTStrips";
            resources.ApplyResources(this._msViewTStrips, "_msViewTStrips");
            // 
            // _msViewTStrips_Main
            // 
            this._msViewTStrips_Main.Checked = true;
            this._msViewTStrips_Main.CheckOnClick = true;
            this._msViewTStrips_Main.CheckState = System.Windows.Forms.CheckState.Checked;
            this._msViewTStrips_Main.Name = "_msViewTStrips_Main";
            resources.ApplyResources(this._msViewTStrips_Main, "_msViewTStrips_Main");
            this._msViewTStrips_Main.Click += new System.EventHandler(this._msView_Click);
            // 
            // _msView_sep1
            // 
            this._msView_sep1.Name = "_msView_sep1";
            resources.ApplyResources(this._msView_sep1, "_msView_sep1");
            // 
            // _msView_SysTray
            // 
            this._msView_SysTray.Image = global::Nyx.Properties.Resources.application_add;
            resources.ApplyResources(this._msView_SysTray, "_msView_SysTray");
            this._msView_SysTray.Name = "_msView_SysTray";
            this._msView_SysTray.Click += new System.EventHandler(this._HideApp);
            // 
            // _msBookmarks
            // 
            this._msBookmarks.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msBookmarks_Mng,
            this._msBookmarks_New,
            this._msBookmarks_Sep0});
            this._msBookmarks.Name = "_msBookmarks";
            resources.ApplyResources(this._msBookmarks, "_msBookmarks");
            // 
            // _msBookmarks_Mng
            // 
            this._msBookmarks_Mng.Image = global::Nyx.Properties.Resources.script_edit;
            resources.ApplyResources(this._msBookmarks_Mng, "_msBookmarks_Mng");
            this._msBookmarks_Mng.Name = "_msBookmarks_Mng";
            this._msBookmarks_Mng.Click += new System.EventHandler(this._msBookmarksMng_Click);
            // 
            // _msBookmarks_New
            // 
            this._msBookmarks_New.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msBookmarks_NewEntry,
            this._msBookmarks_NewCat});
            this._msBookmarks_New.Image = global::Nyx.Properties.Resources.script_add;
            resources.ApplyResources(this._msBookmarks_New, "_msBookmarks_New");
            this._msBookmarks_New.Name = "_msBookmarks_New";
            // 
            // _msBookmarks_NewEntry
            // 
            this._msBookmarks_NewEntry.Image = global::Nyx.Properties.Resources.script_code;
            resources.ApplyResources(this._msBookmarks_NewEntry, "_msBookmarks_NewEntry");
            this._msBookmarks_NewEntry.Name = "_msBookmarks_NewEntry";
            this._msBookmarks_NewEntry.Click += new System.EventHandler(this._msBookmarks_newentry_Click);
            // 
            // _msBookmarks_NewCat
            // 
            this._msBookmarks_NewCat.Image = global::Nyx.Properties.Resources.script_code_red;
            resources.ApplyResources(this._msBookmarks_NewCat, "_msBookmarks_NewCat");
            this._msBookmarks_NewCat.Name = "_msBookmarks_NewCat";
            this._msBookmarks_NewCat.Click += new System.EventHandler(this._msBookmarks_newcat_Click);
            // 
            // _msBookmarks_Sep0
            // 
            this._msBookmarks_Sep0.Name = "_msBookmarks_Sep0";
            resources.ApplyResources(this._msBookmarks_Sep0, "_msBookmarks_Sep0");
            // 
            // _msDbT
            // 
            this._msDbT.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msDbT_Processes,
            this._msDbT_TableStats,
            this._msDbT_Sep0,
            this._msDbT_SqlBldr,
            this._msDbT_ShowAC});
            resources.ApplyResources(this._msDbT, "_msDbT");
            this._msDbT.Name = "_msDbT";
            // 
            // _msDbT_Processes
            // 
            this._msDbT_Processes.Image = global::Nyx.Properties.Resources.cog;
            resources.ApplyResources(this._msDbT_Processes, "_msDbT_Processes");
            this._msDbT_Processes.Name = "_msDbT_Processes";
            this._msDbT_Processes.Click += new System.EventHandler(this._msDbProcesses_Click);
            // 
            // _msDbT_TableStats
            // 
            this._msDbT_TableStats.Image = global::Nyx.Properties.Resources.report;
            resources.ApplyResources(this._msDbT_TableStats, "_msDbT_TableStats");
            this._msDbT_TableStats.Name = "_msDbT_TableStats";
            this._msDbT_TableStats.Click += new System.EventHandler(this._msDbTableStats_Click);
            // 
            // _msDbT_Sep0
            // 
            this._msDbT_Sep0.Name = "_msDbT_Sep0";
            resources.ApplyResources(this._msDbT_Sep0, "_msDbT_Sep0");
            // 
            // _msDbT_SqlBldr
            // 
            this._msDbT_SqlBldr.Image = global::Nyx.Properties.Resources.database_edit;
            resources.ApplyResources(this._msDbT_SqlBldr, "_msDbT_SqlBldr");
            this._msDbT_SqlBldr.Name = "_msDbT_SqlBldr";
            this._msDbT_SqlBldr.Click += new System.EventHandler(this._SqlBldr_Click);
            // 
            // _msDbT_ShowAC
            // 
            this._msDbT_ShowAC.Image = global::Nyx.Properties.Resources.database_edit;
            resources.ApplyResources(this._msDbT_ShowAC, "_msDbT_ShowAC");
            this._msDbT_ShowAC.Name = "_msDbT_ShowAC";
            this._msDbT_ShowAC.Click += new System.EventHandler(this._msDbT_ShowAC_Click);
            // 
            // _msOrcl
            // 
            this._msOrcl.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msOrcl_Privileges,
            this._msOrcl_RecBin,
            this._msOrcl_sep0,
            this._msOrcl_InitInfo,
            this._msOrcl_DbInfo,
            this._msOrcl_TSInfo});
            resources.ApplyResources(this._msOrcl, "_msOrcl");
            this._msOrcl.Name = "_msOrcl";
            // 
            // _msOrcl_Privileges
            // 
            resources.ApplyResources(this._msOrcl_Privileges, "_msOrcl_Privileges");
            this._msOrcl_Privileges.Image = global::Nyx.Properties.Resources.group_gear;
            this._msOrcl_Privileges.Name = "_msOrcl_Privileges";
            this._msOrcl_Privileges.Click += new System.EventHandler(this._msDbUser_Click);
            // 
            // _msOrcl_RecBin
            // 
            this._msOrcl_RecBin.Image = global::Nyx.Properties.Resources.bin;
            resources.ApplyResources(this._msOrcl_RecBin, "_msOrcl_RecBin");
            this._msOrcl_RecBin.Name = "_msOrcl_RecBin";
            this._msOrcl_RecBin.Click += new System.EventHandler(this._msOrcl_recbin_Click);
            // 
            // _msOrcl_sep0
            // 
            this._msOrcl_sep0.Name = "_msOrcl_sep0";
            resources.ApplyResources(this._msOrcl_sep0, "_msOrcl_sep0");
            // 
            // _msOrcl_InitInfo
            // 
            this._msOrcl_InitInfo.Image = global::Nyx.Properties.Resources.information;
            resources.ApplyResources(this._msOrcl_InitInfo, "_msOrcl_InitInfo");
            this._msOrcl_InitInfo.Name = "_msOrcl_InitInfo";
            this._msOrcl_InitInfo.Click += new System.EventHandler(this._msOrcl_initinfo_Click);
            // 
            // _msOrcl_DbInfo
            // 
            this._msOrcl_DbInfo.Image = global::Nyx.Properties.Resources.information;
            resources.ApplyResources(this._msOrcl_DbInfo, "_msOrcl_DbInfo");
            this._msOrcl_DbInfo.Name = "_msOrcl_DbInfo";
            this._msOrcl_DbInfo.Click += new System.EventHandler(this._msDbInfo_Click);
            // 
            // _msOrcl_TSInfo
            // 
            this._msOrcl_TSInfo.Image = global::Nyx.Properties.Resources.information;
            resources.ApplyResources(this._msOrcl_TSInfo, "_msOrcl_TSInfo");
            this._msOrcl_TSInfo.Name = "_msOrcl_TSInfo";
            this._msOrcl_TSInfo.Click += new System.EventHandler(this._msOrcl_tsinfo_Click);
            // 
            // _msMySql
            // 
            this._msMySql.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msMySql_Privileges,
            this._msMySql_sep0,
            this._msMySql_Import,
            this._msMySql_Export,
            this._msMySql_sep1,
            this._msMySql_CrtDb,
            this._msMySql_DropDb,
            this._msMySql_sep2,
            this._msMySql_DbInfo});
            resources.ApplyResources(this._msMySql, "_msMySql");
            this._msMySql.Name = "_msMySql";
            // 
            // _msMySql_Privileges
            // 
            this._msMySql_Privileges.Image = global::Nyx.Properties.Resources.group_gear;
            resources.ApplyResources(this._msMySql_Privileges, "_msMySql_Privileges");
            this._msMySql_Privileges.Name = "_msMySql_Privileges";
            this._msMySql_Privileges.Click += new System.EventHandler(this._msDbUser_Click);
            // 
            // _msMySql_sep0
            // 
            this._msMySql_sep0.Name = "_msMySql_sep0";
            resources.ApplyResources(this._msMySql_sep0, "_msMySql_sep0");
            // 
            // _msMySql_Import
            // 
            this._msMySql_Import.Image = global::Nyx.Properties.Resources.database_add;
            resources.ApplyResources(this._msMySql_Import, "_msMySql_Import");
            this._msMySql_Import.Name = "_msMySql_Import";
            this._msMySql_Import.Click += new System.EventHandler(this._msMySql_import_Click);
            // 
            // _msMySql_Export
            // 
            this._msMySql_Export.Image = global::Nyx.Properties.Resources.database_save;
            resources.ApplyResources(this._msMySql_Export, "_msMySql_Export");
            this._msMySql_Export.Name = "_msMySql_Export";
            this._msMySql_Export.Click += new System.EventHandler(this._msMySql_export_Click);
            // 
            // _msMySql_sep1
            // 
            this._msMySql_sep1.Name = "_msMySql_sep1";
            resources.ApplyResources(this._msMySql_sep1, "_msMySql_sep1");
            // 
            // _msMySql_CrtDb
            // 
            this._msMySql_CrtDb.Image = global::Nyx.Properties.Resources.database_add;
            resources.ApplyResources(this._msMySql_CrtDb, "_msMySql_CrtDb");
            this._msMySql_CrtDb.Name = "_msMySql_CrtDb";
            this._msMySql_CrtDb.Click += new System.EventHandler(this._msMySql_crtdb_Click);
            // 
            // _msMySql_DropDb
            // 
            this._msMySql_DropDb.Image = global::Nyx.Properties.Resources.database_delete;
            resources.ApplyResources(this._msMySql_DropDb, "_msMySql_DropDb");
            this._msMySql_DropDb.Name = "_msMySql_DropDb";
            this._msMySql_DropDb.Click += new System.EventHandler(this._msMySql_dropdb_Click);
            // 
            // _msMySql_sep2
            // 
            this._msMySql_sep2.Name = "_msMySql_sep2";
            resources.ApplyResources(this._msMySql_sep2, "_msMySql_sep2");
            // 
            // _msMySql_DbInfo
            // 
            this._msMySql_DbInfo.Image = global::Nyx.Properties.Resources.information;
            resources.ApplyResources(this._msMySql_DbInfo, "_msMySql_DbInfo");
            this._msMySql_DbInfo.Name = "_msMySql_DbInfo";
            this._msMySql_DbInfo.Click += new System.EventHandler(this._msDbInfo_Click);
            // 
            // _msHelp
            // 
            this._msHelp.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._msHelp_About});
            this._msHelp.Name = "_msHelp";
            resources.ApplyResources(this._msHelp, "_msHelp");
            // 
            // _msHelp_About
            // 
            this._msHelp_About.Image = global::Nyx.Properties.Resources.help;
            resources.ApplyResources(this._msHelp_About, "_msHelp_About");
            this._msHelp_About.Name = "_msHelp_About";
            this._msHelp_About.Click += new System.EventHandler(this._msAbout_Click);
            // 
            // _tsep5
            // 
            this._tsep5.Name = "_tsep5";
            resources.ApplyResources(this._tsep5, "_tsep5");
            // 
            // _ss
            // 
            this._ss.BackColor = System.Drawing.SystemColors.Control;
            this._ss.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._ssProgress,
            this._sS_lblStatusMsg});
            this._ss.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Flow;
            resources.ApplyResources(this._ss, "_ss");
            this._ss.Name = "_ss";
            // 
            // _ssProgress
            // 
            this._ssProgress.MarqueeAnimationSpeed = 0;
            this._ssProgress.Name = "_ssProgress";
            resources.ApplyResources(this._ssProgress, "_ssProgress");
            this._ssProgress.Step = 5;
            // 
            // _sS_lblStatusMsg
            // 
            this._sS_lblStatusMsg.Name = "_sS_lblStatusMsg";
            resources.ApplyResources(this._sS_lblStatusMsg, "_sS_lblStatusMsg");
            // 
            // _NI
            // 
            this._NI.ContextMenuStrip = this._cmNI;
            resources.ApplyResources(this._NI, "_NI");
            this._NI.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this._NI_MouseDoubleClick);
            // 
            // _cmNI
            // 
            this._cmNI.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this._cmNI_show,
            this._cmNI_hide,
            this._cmNI_sep0,
            this._cmNI_exit});
            this._cmNI.Name = "_cmNI";
            resources.ApplyResources(this._cmNI, "_cmNI");
            // 
            // _cmNI_show
            // 
            this._cmNI_show.Image = global::Nyx.Properties.Resources.application_delete;
            this._cmNI_show.Name = "_cmNI_show";
            resources.ApplyResources(this._cmNI_show, "_cmNI_show");
            this._cmNI_show.Click += new System.EventHandler(this._ShowApp);
            // 
            // _cmNI_hide
            // 
            this._cmNI_hide.Image = global::Nyx.Properties.Resources.application_add;
            resources.ApplyResources(this._cmNI_hide, "_cmNI_hide");
            this._cmNI_hide.Name = "_cmNI_hide";
            this._cmNI_hide.Click += new System.EventHandler(this._HideApp);
            // 
            // _cmNI_sep0
            // 
            this._cmNI_sep0.Name = "_cmNI_sep0";
            resources.ApplyResources(this._cmNI_sep0, "_cmNI_sep0");
            // 
            // _cmNI_exit
            // 
            this._cmNI_exit.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._cmNI_exit, "_cmNI_exit");
            this._cmNI_exit.Name = "_cmNI_exit";
            this._cmNI_exit.Click += new System.EventHandler(this._ExitApp);
            // 
            // _ilAutoComplete
            // 
            this._ilAutoComplete.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("_ilAutoComplete.ImageStream")));
            this._ilAutoComplete.TransparentColor = System.Drawing.Color.Transparent;
            this._ilAutoComplete.Images.SetKeyName(0, "template_source.png");
            this._ilAutoComplete.Images.SetKeyName(1, "loupe.png");
            this._ilAutoComplete.Images.SetKeyName(2, "gear.png");
            this._ilAutoComplete.Images.SetKeyName(3, "fonts.png");
            // 
            // NyxMain
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.Controls.Add(this._tsCont);
            this.Controls.Add(this._ss);
            this.Controls.Add(this._menu);
            this.DoubleBuffered = true;
            this.MainMenuStrip = this._menu;
            this.Name = "NyxMain";
            this.Load += new System.EventHandler(this.Nyx_Load);
            this._tsCont.ContentPanel.ResumeLayout(false);
            this._tsCont.TopToolStripPanel.ResumeLayout(false);
            this._tsCont.TopToolStripPanel.PerformLayout();
            this._tsCont.ResumeLayout(false);
            this._tsCont.PerformLayout();
            this._tcMain.ResumeLayout(false);
            this._tpSql.ResumeLayout(false);
            this._splitSqlBldr.Panel1.ResumeLayout(false);
            this._splitSqlBldr.Panel2.ResumeLayout(false);
            this._splitSqlBldr.ResumeLayout(false);
            this._cmSqlBldr.ResumeLayout(false);
            this._tpPreview.ResumeLayout(false);
            this._tpPreview.PerformLayout();
            this._pnlPreview.ResumeLayout(false);
            this._splitPreview.Panel1.ResumeLayout(false);
            this._splitPreview.Panel1.PerformLayout();
            this._splitPreview.Panel2.ResumeLayout(false);
            this._splitPreview.Panel2.PerformLayout();
            this._splitPreview.ResumeLayout(false);
            this._cmPrvObj.ResumeLayout(false);
            this._tsPrvObj.ResumeLayout(false);
            this._tsPrvObj.PerformLayout();
            this._splitPreviewDta.Panel1.ResumeLayout(false);
            this._splitPreviewDta.Panel2.ResumeLayout(false);
            this._splitPreviewDta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dgPreview)).EndInit();
            this._cmPrvDta.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this._dgPreview_Filter)).EndInit();
            this._cmPrvFilter.ResumeLayout(false);
            this._tsPrvDta.ResumeLayout(false);
            this._tsPrvDta.PerformLayout();
            this._tpTables.ResumeLayout(false);
            this._tpTables.PerformLayout();
            this._pnlTables.ResumeLayout(false);
            this._pnlTables.PerformLayout();
            this._splitTables.Panel1.ResumeLayout(false);
            this._splitTables.Panel2.ResumeLayout(false);
            this._splitTables.ResumeLayout(false);
            this._cmTable.ResumeLayout(false);
            this._splitIndexes.Panel1.ResumeLayout(false);
            this._splitIndexes.Panel2.ResumeLayout(false);
            this._splitIndexes.ResumeLayout(false);
            this._cmTblField.ResumeLayout(false);
            this._splitIndexDetails.Panel1.ResumeLayout(false);
            this._splitIndexDetails.Panel2.ResumeLayout(false);
            this._splitIndexDetails.ResumeLayout(false);
            this._cmIndex.ResumeLayout(false);
            this._tsTables.ResumeLayout(false);
            this._tsTables.PerformLayout();
            this._tpViews.ResumeLayout(false);
            this._tpViews.PerformLayout();
            this._pnlViews.ResumeLayout(false);
            this._pnlViews.PerformLayout();
            this._splitViews.Panel1.ResumeLayout(false);
            this._splitViews.Panel2.ResumeLayout(false);
            this._splitViews.ResumeLayout(false);
            this._cmViews.ResumeLayout(false);
            this._splitViews2.Panel1.ResumeLayout(false);
            this._splitViews2.Panel2.ResumeLayout(false);
            this._splitViews2.ResumeLayout(false);
            this._tsViews.ResumeLayout(false);
            this._tsViews.PerformLayout();
            this._tpProcedures.ResumeLayout(false);
            this._tpProcedures.PerformLayout();
            this._pnlProcedures.ResumeLayout(false);
            this._pnlProcedures.PerformLayout();
            this._splitProcedures.Panel1.ResumeLayout(false);
            this._splitProcedures.Panel2.ResumeLayout(false);
            this._splitProcedures.ResumeLayout(false);
            this._splitProcedures2.Panel1.ResumeLayout(false);
            this._splitProcedures2.Panel2.ResumeLayout(false);
            this._splitProcedures2.ResumeLayout(false);
            this._tsPrcdrs.ResumeLayout(false);
            this._tsPrcdrs.PerformLayout();
            this._tpTriggers.ResumeLayout(false);
            this._tpTriggers.PerformLayout();
            this._pnlTriggers.ResumeLayout(false);
            this._pnlTriggers.PerformLayout();
            this._splitTriggers.Panel1.ResumeLayout(false);
            this._splitTriggers.Panel2.ResumeLayout(false);
            this._splitTriggers.ResumeLayout(false);
            this._splitTriggers2.Panel1.ResumeLayout(false);
            this._splitTriggers2.Panel2.ResumeLayout(false);
            this._splitTriggers2.ResumeLayout(false);
            this._tsTriggers.ResumeLayout(false);
            this._tsTriggers.PerformLayout();
            this._tsMain.ResumeLayout(false);
            this._tsMain.PerformLayout();
            this._menu.ResumeLayout(false);
            this._menu.PerformLayout();
            this._ss.ResumeLayout(false);
            this._ss.PerformLayout();
            this._cmNI.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip _menu;
        private System.Windows.Forms.ToolStripMenuItem _msFile;
        private System.Windows.Forms.ToolStripMenuItem _msSql;
        private System.Windows.Forms.ToolStripMenuItem _msHelp;
        private System.Windows.Forms.ToolStripMenuItem _msHelp_About;
        private System.Windows.Forms.ToolStripMenuItem _msOrcl;
        private System.Windows.Forms.ToolStripMenuItem _msOrcl_TSInfo;
        private System.Windows.Forms.ToolStrip _tsMain;
        private System.Windows.Forms.ToolStripButton _tsMain_Connect;
        private System.Windows.Forms.ToolStripButton _tsMain_Disconnect;
        private System.Windows.Forms.ToolStripComboBox _tsMain_cbProfiles;
        private System.Windows.Forms.ToolStripSeparator _tsep5;
        private System.Windows.Forms.ToolStripMenuItem _msFile_Exit;
        private System.Windows.Forms.ToolStripMenuItem _msFile_Profiles;
        private System.Windows.Forms.ToolStripMenuItem _msOrcl_DbInfo;
        private System.Windows.Forms.ToolStripMenuItem _msOrcl_InitInfo;
        private System.Windows.Forms.ToolStripMenuItem _msMySql;
        private System.Windows.Forms.StatusStrip _ss;
        private System.Windows.Forms.ToolStripStatusLabel _sS_lblStatusMsg;
        private System.Windows.Forms.ToolStripMenuItem _msOrcl_Privileges;
        private System.Windows.Forms.ToolStripSeparator _msOrcl_sep0;
        private System.Windows.Forms.ToolStripMenuItem _msFile_Settings;
        private System.Windows.Forms.ToolStripMenuItem _msMySql_DbInfo;
        private System.Windows.Forms.ToolStripSeparator _msMySql_sep0;
        private System.Windows.Forms.ToolStripMenuItem _msMySql_Privileges;
        private System.Windows.Forms.ToolStripMenuItem _msMySql_Import;
        private System.Windows.Forms.ToolStripMenuItem _msMySql_Export;
        private System.Windows.Forms.ToolStripSeparator _msMySql_sep1;
        private System.Windows.Forms.TabControl _tcMain;
        private System.Windows.Forms.TabPage _tpSql;
        private System.Windows.Forms.ToolStripMenuItem _msOrcl_RecBin;
        private System.Windows.Forms.ContextMenuStrip _cmTable;
        private System.Windows.Forms.ToolStripSeparator _msMySql_sep2;
        private System.Windows.Forms.ToolStripMenuItem _msMySql_CrtDb;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_Refresh;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_Create;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_Truncate;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_Drop;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_Diag;
        private System.Windows.Forms.ToolStripMenuItem _cmTableDiag_Check;
        private System.Windows.Forms.ToolStripMenuItem _cmTableDiag_Analyze;
        private System.Windows.Forms.ToolStripMenuItem _cmTableDiag_OptReb;
        private System.Windows.Forms.ToolStripMenuItem _cmTableDiag_Repair;
        private System.Windows.Forms.ToolStripMenuItem _msView;
        private System.Windows.Forms.ToolStripMenuItem _msView_Sql;
        private System.Windows.Forms.ToolStripMenuItem _msView_Tables;
        private System.Windows.Forms.ToolStripSeparator _msView_sep0;
        private System.Windows.Forms.ToolStripContainer _tsCont;
        private System.Windows.Forms.ToolStripMenuItem _msViewTStrips;
        private System.Windows.Forms.ToolStripMenuItem _msViewTStrips_Main;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_Alter;
        private System.Windows.Forms.ContextMenuStrip _cmTblField;
        private System.Windows.Forms.ToolStripMenuItem _cmTblField_Alter;
        private System.Windows.Forms.ToolStripMenuItem _cmTblField_Drop;
        private System.Windows.Forms.ToolStripMenuItem _cmTblField_Create;
        private System.Windows.Forms.ContextMenuStrip _cmSqlBldr;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlBldr_Refresh;
        private System.Windows.Forms.ToolStripMenuItem _cmSqlBldr_Insert;
        private System.Windows.Forms.TabPage _tpViews;
        private System.Windows.Forms.ToolStripMenuItem _msView_Views;
        private System.Windows.Forms.ToolStripSeparator _msView_sep1;
        private System.Windows.Forms.ToolStripSeparator _msSql_Sep1;
        private System.Windows.Forms.ToolStripButton _tsMain_SaveQC;
        private System.Windows.Forms.ToolStripSeparator _tsMain_Sep0;
        private System.Windows.Forms.ToolStripComboBox _tsMain_cbChangeDb;
        private System.Windows.Forms.ContextMenuStrip _cmIndex;
        private System.Windows.Forms.ToolStripMenuItem _cmIndex_Create;
        private System.Windows.Forms.ToolStripMenuItem _cmIndex_Alter;
        private System.Windows.Forms.ToolStripMenuItem _cmIndex_Drop;
        private System.Windows.Forms.ToolStripMenuItem _cmIndex_Refresh;
        private System.Windows.Forms.ToolStripMenuItem _msMySql_DropDb;
        private System.Windows.Forms.ToolStripProgressBar _ssProgress;
        private System.Windows.Forms.NotifyIcon _NI;
        private System.Windows.Forms.ToolStripMenuItem _msView_SysTray;
        private System.Windows.Forms.ContextMenuStrip _cmNI;
        private System.Windows.Forms.ToolStripMenuItem _cmNI_show;
        private System.Windows.Forms.ToolStripMenuItem _cmNI_hide;
        private System.Windows.Forms.ToolStripSeparator _cmNI_sep0;
        private System.Windows.Forms.ToolStripMenuItem _cmNI_exit;
        private System.Windows.Forms.ToolStripMenuItem _msFile_ChkUpdate;
        private System.Windows.Forms.ToolStripSeparator _msFile_Sep1;
        private System.Windows.Forms.ToolStripMenuItem _msBookmarks;
        private System.Windows.Forms.ToolStripMenuItem _msBookmarks_Mng;
        private System.Windows.Forms.ToolStripMenuItem _msBookmarks_New;
        private System.Windows.Forms.ToolStripSeparator _msBookmarks_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _msBookmarks_NewEntry;
        private System.Windows.Forms.ToolStripMenuItem _msBookmarks_NewCat;
        private System.Windows.Forms.TabPage _tpProcedures;
        private System.Windows.Forms.ToolStripMenuItem _msView_Prcdrs;
        private System.Windows.Forms.ToolStripSeparator _msSql_Sep2;
        private System.Windows.Forms.ToolStripMenuItem _msSql_ClearSql;
        private System.Windows.Forms.ToolStripMenuItem _msSql_ClearHistory;
        private System.Windows.Forms.Panel _pnlProcedures;
        private System.Windows.Forms.ListView _lvPrcdr_Details;
        private System.Windows.Forms.ColumnHeader _lvcPrcdrs_Variable;
        private System.Windows.Forms.ColumnHeader _lvcPrcdrs_Value;
        private System.Windows.Forms.RichTextBox _rtbPrcdr_CreateStm;
        private System.Windows.Forms.ToolStrip _tsPrcdrs;
        private System.Windows.Forms.ToolStripButton _tsPrcdrs_Refresh;
        private System.Windows.Forms.ToolStripSeparator _tsPrcdrs_Sep0;
        private System.Windows.Forms.ToolStrip _tsViews;
        private System.Windows.Forms.ToolStripButton _tsViews_Refresh;
        private System.Windows.Forms.ToolStripSeparator _tsViews_Sep0;
        private System.Windows.Forms.ListView _lvView_Details;
        private System.Windows.Forms.ColumnHeader _lvcView_Variable;
        private System.Windows.Forms.ColumnHeader _lvcView_Value;
        private System.Windows.Forms.RichTextBox _rtbView_CreateStm;
        private System.Windows.Forms.ToolStripButton _tsMain_QuickConnect;
        private System.Windows.Forms.ToolStripMenuItem _msFile_QuickConnect;
        private System.Windows.Forms.TabPage _tpTriggers;
        private System.Windows.Forms.Panel _pnlTriggers;
        private System.Windows.Forms.ListView _lvTrigger_Details;
        private System.Windows.Forms.ColumnHeader _lvcTrigger_Variable;
        private System.Windows.Forms.ColumnHeader _lvcTrigger_Value;
        private System.Windows.Forms.RichTextBox _rtbTrigger_CrtStm;
        private System.Windows.Forms.ToolStrip _tsTriggers;
        private System.Windows.Forms.ToolStripButton _tsTriggers_Refresh;
        private System.Windows.Forms.ToolStripSeparator _tsTriggers_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _msView_Triggers;
        private System.Windows.Forms.ToolStripMenuItem _msSql_ExecSel;
        private System.Windows.Forms.ToolStripMenuItem _msDbT;
        private System.Windows.Forms.ToolStripMenuItem _msDbT_TableStats;
        private System.Windows.Forms.ToolStripMenuItem _msDbT_SqlBldr;
        private System.Windows.Forms.ToolStripSeparator _msDbT_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _msDbT_Processes;
        private System.Windows.Forms.TreeView _trvSqlBldr;
        private System.Windows.Forms.ToolStripSeparator _cmSqlBldr_Sep0;
        private System.Windows.Forms.ToolStripSeparator _tsViews_Sep1;
        private System.Windows.Forms.ToolStripLabel _tsViews_lblRetrieve;
        private System.Windows.Forms.ToolStripComboBox _tsViews_cbScopeve;
        private System.Windows.Forms.ToolStripSeparator _tsPrcdrs_Sep1;
        private System.Windows.Forms.ToolStripLabel _tsPrcdrs_lblRetrieve;
        private System.Windows.Forms.ToolStripComboBox _tsPrcdrs_cbScope;
        private System.Windows.Forms.ToolStripSeparator _tsTriggers_Sep1;
        private System.Windows.Forms.ToolStripLabel _tsTriggers_lblRetrieve;
        private System.Windows.Forms.ToolStripComboBox _tsTriggers_cbScope;
        private System.Windows.Forms.TabPage _tpPreview;
        private System.Windows.Forms.TabPage _tpTables;
        private System.Windows.Forms.Panel _pnlTables;
        private System.Windows.Forms.ToolStrip _tsTables;
        private System.Windows.Forms.ToolStripButton _tsTables_Refresh;
        private System.Windows.Forms.ToolStripSeparator _tsTables_Sep0;
        private System.Windows.Forms.ToolStripLabel _tsTables_lblRetrieve;
        private System.Windows.Forms.ToolStripComboBox _tsTables_cbScope;
        private System.Windows.Forms.Panel _pnlPreview;
        private System.Windows.Forms.SplitContainer _splitPreview;
        private System.Windows.Forms.ToolStrip _tsPrvDta;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_Start;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_Prev;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_Next;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_End;
        private System.Windows.Forms.ToolStripSeparator _tsPrvDta_Sep2;
        private System.Windows.Forms.ToolStripComboBox _tsPrvDta_RowsPerPage;
        private System.Windows.Forms.ToolStripSeparator _tsPrvDta_Sep3;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblPage;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblActPg;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblDiv0;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblPgCnt;
        private System.Windows.Forms.ToolStripSeparator _tsPrvDta_Sep5;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblRows;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblActRows;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblDiv1;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblRowCnt;
        private System.Windows.Forms.SplitContainer _splitTables;
        private System.Windows.Forms.SplitContainer _splitIndexes;
        private System.Windows.Forms.ListView _lvTableDesc;
        private System.Windows.Forms.ColumnHeader field;
        private System.Windows.Forms.ColumnHeader type;
        private System.Windows.Forms.ColumnHeader length;
        private System.Windows.Forms.ColumnHeader precission;
        private System.Windows.Forms.SplitContainer _splitIndexDetails;
        private System.Windows.Forms.ListView _lvIndexes;
        private System.Windows.Forms.ColumnHeader idxname;
        private System.Windows.Forms.ColumnHeader tblname;
        private System.Windows.Forms.ColumnHeader tblstatus;
        private System.Windows.Forms.ColumnHeader analyse;
        private System.Windows.Forms.ListView _lvIndexDesc;
        private System.Windows.Forms.ColumnHeader cname;
        private System.Windows.Forms.ColumnHeader dtype;
        private System.Windows.Forms.ColumnHeader cpos;
        private System.Windows.Forms.ListBox _lbTables;
        private System.Windows.Forms.ToolStripMenuItem _msView_Preview;
        private System.Windows.Forms.DataGridView _dgPreview;
        private System.Windows.Forms.ToolStripSeparator _tsTables_Sep1;
        private System.Windows.Forms.ToolStripLabel _tsTables_lblDescTable;
        private System.Windows.Forms.ToolStripTextBox _tsTables_tbDescTable;
        private System.Windows.Forms.ImageList _ilAutoComplete;
        private System.Windows.Forms.ToolStripSeparator _tsTables_Sep2;
        private System.Windows.Forms.ToolStripLabel _tsTables_lblcurTable;
        private System.Windows.Forms.ToolStripLabel _tsTables_curTable;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_Describe;
        private System.Windows.Forms.ToolStripMenuItem _cmIndex_Describe;
        private System.Windows.Forms.Label _lblNotSupportedProcedures;
        private System.Windows.Forms.Label _lblNotSupportedTriggers;
        private System.Windows.Forms.Panel _pnlViews;
        private System.Windows.Forms.Label _lblNotSupportedViews;
        private System.Windows.Forms.Label _lblNotSupportedPreview;
        private System.Windows.Forms.Label _lblNotSupportedTables;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_SelAll;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_SelNone;
        private System.Windows.Forms.ToolStripMenuItem _cmTable_SelInv;
        private System.Windows.Forms.ToolStripMenuItem _cmTblField_Refresh;
        private System.Windows.Forms.ToolStripMenuItem _cmTblField_SelAll;
        private System.Windows.Forms.ToolStripMenuItem _cmTblField_SelNone;
        private System.Windows.Forms.ToolStripMenuItem _cmTblField_SelInv;
        private System.Windows.Forms.ToolStripMenuItem _cmIndex_SelAll;
        private System.Windows.Forms.ToolStripMenuItem _cmIndex_SelNone;
        private System.Windows.Forms.ToolStripMenuItem _cmIndex_SelInv;
        private System.Windows.Forms.SplitContainer _splitSqlBldr;
        private System.Windows.Forms.ToolStripMenuItem _msSql_ExecSql;
        private System.Windows.Forms.ToolStripMenuItem _msSql_EditData;
        private System.Windows.Forms.ToolStripMenuItem _msSql_UpdData;
        private System.Windows.Forms.ToolStripSeparator _msFile_Sep0;
        private System.Windows.Forms.ToolStripLabel _tsTables_lblFilter;
        private System.Windows.Forms.ToolStripTextBox _tsTables_Filter;
        private System.Windows.Forms.ToolStripSeparator _tsTables_Sep3;
        private System.Windows.Forms.ListBox _lbViews;
        private System.Windows.Forms.ListBox _lbPrcdrs;
        private System.Windows.Forms.ListBox _lbTriggers;
        private System.Windows.Forms.ToolStripLabel _tsViews_lblFilter;
        private System.Windows.Forms.ToolStripTextBox _tsViews_Filter;
        private System.Windows.Forms.ToolStripTextBox _tsPrcdrs_Filter;
        private System.Windows.Forms.ToolStripLabel _tsPrcdrs_lblFilter;
        private System.Windows.Forms.ToolStripLabel _tsTriggers_lblFilter;
        private System.Windows.Forms.ToolStripTextBox _tsTriggers_Filter;
        private System.Windows.Forms.ToolStripSeparator _tsPrvDta_Sep6;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_lblCurTable;
        private System.Windows.Forms.ToolStripLabel _tsPrvDta_curObject;
        private System.Windows.Forms.ToolStripSeparator _tsViews_Sep3;
        private System.Windows.Forms.ToolStripLabel _tsViews_lblCurTable;
        private System.Windows.Forms.ToolStripSeparator _tsPrcdrs_Sep3;
        private System.Windows.Forms.ToolStripLabel _tsPrcdrs_lblCurTable;
        private System.Windows.Forms.ToolStripSeparator _tsTriggers_Sep3;
        private System.Windows.Forms.ToolStripLabel _tsTriggers_lblCurTable;
        private System.Windows.Forms.ToolStripLabel _tsTriggers_curTriggers;
        private System.Windows.Forms.ToolStripLabel _tsViews_curView;
        private System.Windows.Forms.ToolStripLabel _tsPrcdrs_curProcedure;
        private System.Windows.Forms.SplitContainer _splitViews;
        private System.Windows.Forms.SplitContainer _splitViews2;
        private System.Windows.Forms.SplitContainer _splitProcedures;
        private System.Windows.Forms.SplitContainer _splitProcedures2;
        private System.Windows.Forms.SplitContainer _splitTriggers2;
        private System.Windows.Forms.SplitContainer _splitTriggers;
        private System.Windows.Forms.TabPage sql1;
        private System.Windows.Forms.ComboBox _cbSqlHistory;
        private System.Windows.Forms.TabControl _tcSql;
        private System.Windows.Forms.ToolStripSeparator _msSql_Sep3;
        private System.Windows.Forms.ToolStripMenuItem _msSql_SqlTabs;
        private System.Windows.Forms.ToolStripMenuItem _msSql_SqlTabs_NewTab;
        private System.Windows.Forms.ToolStripMenuItem _msSql_SqlTabs_CloseTab;
        private System.Windows.Forms.ToolStripMenuItem _msDbT_ShowAC;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_Edit;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_Update;
        private System.Windows.Forms.ToolStripSeparator _tsPrvDta_Sep1;
        private System.Windows.Forms.ContextMenuStrip _cmPrvDta;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDta_Refresh;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDta_Edit;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDta_Update;
        private System.Windows.Forms.ToolStripSeparator _cmPrvDta_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _msFile_Connect;
        private System.Windows.Forms.ToolStripMenuItem _msFile_Disconnect;
        private System.Windows.Forms.ToolStripSeparator _msFile_Sep2;
        private System.Windows.Forms.ToolStripMenuItem _msPrv;
        private System.Windows.Forms.ToolStripMenuItem _msPrvObj_Refresh;
        private System.Windows.Forms.ToolStripMenuItem _msPrvDta_Refresh;
        private System.Windows.Forms.ToolStripSeparator _msPrv_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _msPrvDta_Edit;
        private System.Windows.Forms.ToolStripMenuItem _msPrv_Update;
        private System.Windows.Forms.ToolStripSeparator _cmTable_Sep1;
        private System.Windows.Forms.ToolStripSeparator _cmTblField_Sep0;
        private System.Windows.Forms.ToolStripSeparator _cmTblField_Sep1;
        private System.Windows.Forms.ToolStripSeparator _cmTable_Sep0;
        private System.Windows.Forms.ToolStripSeparator _cmTable_Sep2;
        private System.Windows.Forms.ToolStripSeparator _cmIndex_Sep0;
        private System.Windows.Forms.ToolStripSeparator _cmIndex_Sep1;
        private System.Windows.Forms.ToolStripSeparator _cmIndex_Sep2;
        private System.Windows.Forms.ToolStrip _tsPrvObj;
        private System.Windows.Forms.ToolStripButton _tsPrvObj_Refresh;
        private System.Windows.Forms.ToolStripLabel _tsPrvObj_lblFilter;
        private System.Windows.Forms.ToolStripTextBox _tsPrvObj_Filter;
        private System.Windows.Forms.ToolStripLabel _tsPrvObj_lblScope;
        private System.Windows.Forms.ToolStripComboBox _tsPrvObj_Scope;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_Refresh;
        private System.Windows.Forms.ToolStripSeparator _tsPrvDta_Sep0;
        private System.Windows.Forms.ToolStripSeparator _cmPrvDta_Sep1;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDtaCp;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDtaCp_Field;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDtaCp_Cells;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDtaCp_Table;
        private System.Windows.Forms.ContextMenuStrip _cmPrvObj;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObj_Get;
        private System.Windows.Forms.ToolStripSeparator _cmPrvObj_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObj_DropTable;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObj_Truncate;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObjDiag;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObjDiag_Check;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObjDiag_Analyze;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObjDiag_Optimize;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObjDiag_Repair;
        private System.Windows.Forms.ToolStripSeparator _cmPrvObj_Sep1;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObj_Refresh;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDtaSv;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDtaSv_Cells;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvDtaSv_Table;
        private System.Windows.Forms.ToolStripButton _tsViews_Create;
        private System.Windows.Forms.ListView _lvPrvObjects;
        private System.Windows.Forms.ColumnHeader lvcPrvObjTable;
        private System.Windows.Forms.ColumnHeader lvcPrvObjType;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvObj_DropView;
        private System.Windows.Forms.ContextMenuStrip _cmViews;
        private System.Windows.Forms.ToolStripMenuItem _cmViews_Create;
        private System.Windows.Forms.ToolStripMenuItem _cmViews_Drop;
        private System.Windows.Forms.ToolStripSeparator _cmViews_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _cmViews_Refresh;
        private System.Windows.Forms.SplitContainer _splitPreviewDta;
        private System.Windows.Forms.DataGridView _dgPreview_Filter;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_ShowFilter;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_SetFilter;
        private System.Windows.Forms.ToolStripButton _tsPrvDta_ResetFilter;
        private System.Windows.Forms.ToolStripSeparator _tsPrvDta_Sep7;
        private System.Windows.Forms.ContextMenuStrip _cmPrvFilter;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvFilter_Set;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvFilter_Reset;
        private System.Windows.Forms.ToolStripSeparator _cmPrvFilter_Sep0;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvFilter_FieldUp;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvFilter_FieldDown;
        private System.Windows.Forms.DataGridViewTextBoxColumn vField;
        private System.Windows.Forms.DataGridViewCheckBoxColumn vSelect;
        private System.Windows.Forms.DataGridViewComboBoxColumn vLOperator;
        private System.Windows.Forms.DataGridViewComboBoxColumn vFilter;
        private System.Windows.Forms.DataGridViewTextBoxColumn vValue;
        private System.Windows.Forms.DataGridViewTextBoxColumn vGroup;
        private System.Windows.Forms.ToolStripSeparator _cmPrvFilter_Sep1;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvFilter_DupField;
        private System.Windows.Forms.ToolStripMenuItem _cmPrvFilter_RmDupField;
    }
}

