using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Nyx.Classes
{
    /// <summary>
    /// default buttons, just a template for copy/pasting the buttons
    /// </summary>
    public partial class DefaultButtons : Form
    {
        /// <summary>
        /// 
        /// </summary>
        public DefaultButtons()
        {
            this.Close();
        }
    }
}