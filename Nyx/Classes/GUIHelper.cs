/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Text;
using System.Drawing;
using NyxSettings;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Collections;

namespace Nyx
{
    /// <summary> GUIHelper </summary>
    public sealed class GuiHelper
    {
        private static string fullpath = String.Empty;
        /// <summary> fullpath </summary>
        public static string Fullpath { get { string fp = fullpath; fullpath = String.Empty; return fp; } }

        /// <summary> ListViewSelection </summary>
        public enum EnumLVSelection {
            /// <remarks> Select items </remarks>
            Select,
            /// <remarks> Unselect items </remarks>
            Unselect,
            /// <remarks> Invert selection </remarks>
            Invert 
        };
        /// <summary> private constructor to prevent the compiler from creating a default constructor </summary>
        private GuiHelper() { }

        /// <summary> Get the last save path from the runtimesettings </summary>
        /// <returns>the last save path</returns>
        public static string GetLastSavePath()
        {
            if (System.IO.Directory.Exists(NyxMain.RTSett.LastSelectedPath))
                return NyxMain.RTSett.LastSelectedPath;
            else
                return System.Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        }
        /// <summary> Set the last save path inside the runtimesettings </summary>
        /// <param name="filename">filename which path shall get set</param>
        public static void SetLastSavePath(string filename)
        {   // validate
            if (filename == null)
                throw new ArgumentNullException("filename", GuiHelper.GetResourceMsg("ArgumentNull"));
            // extract path
            int lastSlashPos = filename.LastIndexOf('\\');
            if (lastSlashPos != -1)
                NyxMain.RTSett.LastSelectedPath = filename.Substring(0, lastSlashPos + 1);
        }

        /// <summary> show default nyx savefiledialog </summary>
        /// <param name="extension">extension to set</param>
        /// <param name="filter">filefilter to show</param>
        /// <returns>true/false</returns>
        public static bool SaveFileDialog(string extension, string filter)
        {   // validate
            if (extension == null)
                throw new ArgumentNullException("extension", GuiHelper.GetResourceMsg("ArgumentNull"));
            if(filter == null)
                throw new ArgumentNullException("filter", GuiHelper.GetResourceMsg("ArgumentNull"));
            // init
            fullpath = String.Empty;
            // init savefiledialog
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.AddExtension = true;
            sfd.CheckPathExists = true;
            sfd.CreatePrompt = false;
            sfd.OverwritePrompt = true;
            sfd.ValidateNames = true;
            sfd.Title = GuiHelper.GetResourceMsg("SaveFilePrompt");
            sfd.DefaultExt = extension;
            sfd.Filter = filter;
            sfd.InitialDirectory = GetLastSavePath();
            // check if ok and valid filename passed
            if (sfd.ShowDialog() == DialogResult.OK && sfd.FileName.Length > 0)
            {   // extract the directory
                SetLastSavePath(sfd.FileName);
                fullpath = sfd.FileName;
                return true;
            }
            return false;
        }
        /// <summary> default open file dialog </summary>
        /// <param name="filter">filter to use</param>
        /// <returns></returns>
        public static bool OpenFileDialog(string filter)
        {   // validate
            if (filter == null)
                throw new ArgumentNullException("filter", GuiHelper.GetResourceMsg("ArgumentNull"));
            // init openfiledialog
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.AddExtension = true;
            ofd.CheckPathExists = true;
            ofd.CheckFileExists = true;
            ofd.Multiselect = false;
            ofd.ValidateNames = true;
            ofd.Filter = filter;
            ofd.Title = GuiHelper.GetResourceMsg("LoadFilePrompt");
            ofd.InitialDirectory = GetLastSavePath();
            // check if ok and valid filename passed
            if (ofd.ShowDialog() == DialogResult.OK && ofd.FileName.Length > 0)
            {   // extract the directory
                SetLastSavePath(ofd.FileName);
                fullpath = ofd.FileName;
                return true;
            }
            return false;
        }
        /// <summary> set listviewchecked state </summary>
        /// <param name="listview"></param>
        /// <param name="lvAction"></param>
        public static void ListViewCheckedState(ListView listview, EnumLVSelection lvAction)
        {   // validate
            if (listview == null)
                throw new ArgumentNullException("listview", GuiHelper.GetResourceMsg("ArgumentNull"));
            // switch listviewaction
            switch (lvAction)
            {
                case EnumLVSelection.Select:
                    foreach (ListViewItem lvi in listview.Items)
                        lvi.Checked = true;
                    break;
                case EnumLVSelection.Unselect:
                    foreach (ListViewItem lvi in listview.Items)
                        lvi.Checked = false;
                    break;
                case EnumLVSelection.Invert:
                    foreach (ListViewItem lvi in listview.Items)
                        if (lvi.Checked)
                            lvi.Checked = false;
                        else
                            lvi.Checked = true;
                    break;
            }
        }
        /// <summary> set listviewselected state </summary>
        /// <param name="listview"></param>
        /// <param name="lvAction"></param>
        public static void ListViewSelectededState(ListView listview, EnumLVSelection lvAction)
        {   // validate
            if (listview == null)
                throw new ArgumentNullException("listview", GuiHelper.GetResourceMsg("ArgumentNull"));
            // switch listviewaction
            switch (lvAction)
            {
                case EnumLVSelection.Select:
                    foreach (ListViewItem lvi in listview.Items)
                        lvi.Selected = true;
                    break;
                case EnumLVSelection.Unselect:
                    foreach (ListViewItem lvi in listview.Items)
                        lvi.Selected = false;
                    break;
                case EnumLVSelection.Invert:
                    foreach (ListViewItem lvi in listview.Items)
                        if (lvi.Selected)
                            lvi.Selected = false;
                        else
                            lvi.Selected = true;
                    break;
            }
        }
        /// <summary> set listboxselected state </summary>
        /// <param name="listbox"></param>
        /// <param name="lvAction"></param>
        public static void ListBoxSelectState(ListBox listbox, EnumLVSelection lvAction)
        {   // validate
            if (listbox == null)
                throw new ArgumentNullException("listbox", GuiHelper.GetResourceMsg("ArgumentNull"));

            switch (lvAction)
            {
                case EnumLVSelection.Select:
                    for (int i = 0; i < listbox.Items.Count; i++)
                        listbox.SetSelected(i, true);
                    break;
                case EnumLVSelection.Unselect:
                    for (int i = 0; i < listbox.Items.Count; i++)
                        listbox.SetSelected(i, false);
                    break;
                case EnumLVSelection.Invert:
                    for (int i = 0; i < listbox.Items.Count; i++)
                    {
                        if (listbox.GetSelected(i) == true)
                            listbox.SetSelected(i, false);
                        else
                            listbox.SetSelected(i, true);
                    }
                    break;
            }
        }
        /// <summary> set datagridview state </summary>
        /// <param name="dgv"></param>
        /// <param name="lvAction"></param>
        public static void DataGridViewRowSelectState(DataGridView dgv, EnumLVSelection lvAction)
        {   // validate
            if (dgv == null)
                throw new ArgumentNullException("dgv", GuiHelper.GetResourceMsg("ArgumentNull"));

            switch (lvAction)
            {
                case EnumLVSelection.Select:
                    dgv.SelectAll();
                    break;
                case EnumLVSelection.Unselect:
                    foreach (DataGridViewRow dgvr in dgv.Rows)
                        dgvr.Selected = false;
                    break;
                case EnumLVSelection.Invert:
                    foreach (DataGridViewRow dgvr in dgv.Rows)
                    {
                        if (dgvr.Selected)
                            dgvr.Selected = false;
                        else
                            dgvr.Selected = true;
                    }
                    break;
            }
        }
        /// <summary> get resource message </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        public static string GetResourceMsg(string variable)
        {   // validate
            if (variable == null)
                throw new ArgumentNullException("variable", GuiHelper.GetResourceMsg("ArgumentNull"));

            System.Resources.ResourceManager resmng = 
                new System.Resources.ResourceManager("Nyx.Nyx", System.Reflection.Assembly.GetExecutingAssembly());
            if (resmng != null) 
                return resmng.GetString(variable, NyxMain.NyxCI).Replace("\\lf", "\n");
            else 
                return String.Empty;
        }
        /// <summary> get resource message </summary>
        /// <param name="variable"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public static string GetResourceMsg(string variable, string[] parameters)
        {   // validate
            if (variable == null)
                throw new ArgumentNullException("variable", GuiHelper.GetResourceMsg("ArgumentNull"));
            if(parameters == null)
                throw new ArgumentNullException("parameters", GuiHelper.GetResourceMsg("ArgumentNull"));

            System.Resources.ResourceManager resmng = 
                new System.Resources.ResourceManager("Nyx.Nyx", System.Reflection.Assembly.GetExecutingAssembly());
            if (resmng != null)
                return String.Format(NyxMain.NyxCI, resmng.GetString(variable, NyxMain.NyxCI), parameters).Replace("\\lf", "\n");
            else
                return String.Empty;
        }
        /// <summary> build listview item </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static ListViewItem BuildListViewItem(string text)
        {   // validate
            if (text == null)
                throw new ArgumentNullException("text", GuiHelper.GetResourceMsg("ArgumentNull"));
            
            ListViewItem lvi = null;
            lvi = new ListViewItem(text);
            lvi.Name = text;
            return lvi;
        }
        /// <summary> build listview item </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public static ListViewItem BuildListViewItem(string[] items)
        {   // validate
            if (items == null)
                throw new ArgumentNullException("items", GuiHelper.GetResourceMsg("ArgumentNull"));

            ListViewItem lvi = new ListViewItem();
            if (items.Length > 0)
            {
                lvi.Text = items[0];
                lvi.Name = items[0];
                for (int i = 1; i < items.Length; i++)
                    lvi.SubItems.Add(items[i]);
            }
            return lvi;
        }
        /// <summary> build listview item </summary>
        /// <param name="text"></param>
        /// <param name="subitem"></param>
        /// <returns></returns>
        public static ListViewItem BuildListViewItem(string text, string subitem)
        {   // validate
            if (text == null)
                throw new ArgumentNullException("text", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (subitem == null)
                throw new ArgumentNullException("subitem", GuiHelper.GetResourceMsg("ArgumentNull"));

            ListViewItem lvi = new ListViewItem(text);
            lvi.Name = text;
            lvi.SubItems.Add(subitem);
            return lvi;
        }
        /// <summary> build iistview item </summary>
        /// <param name="text"></param>
        /// <param name="subitems"></param>
        /// <returns></returns>
        public static ListViewItem BuildListViewItem(string text, string[] subitems)
        {   // validate
            if (text == null)
                throw new ArgumentNullException("text", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (subitems == null)
                throw new ArgumentNullException("subitem", GuiHelper.GetResourceMsg("ArgumentNull"));

            ListViewItem lvi = new ListViewItem();
            lvi.Text = text;
            lvi.Name = text;
            foreach (string subitem in subitems)
                lvi.SubItems.Add(subitem);
            return lvi;
        }
        /// <summary> Sort listview by column </summary>
        /// <param name="lv"></param>
        /// <param name="lvcs"></param>
        /// <param name="colIndex"></param>
        public static void ListViewSort(ListView lv, Classes.ListViewColumnSorter lvcs, int colIndex)
        {   // validate
            if (lvcs == null)
                throw new ArgumentNullException("lvcs", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (lv == null)
                throw new ArgumentNullException("listview", GuiHelper.GetResourceMsg("ArgumentNull"));
            // Determine if clicked column is already the column that is being sorted.
            if (colIndex == lvcs.SortColumn)
            {
                // Reverse the current sort direction for this column.
                if (lvcs.Order == SortOrder.Ascending)
                    lvcs.Order = SortOrder.Descending;
                else
                    lvcs.Order = SortOrder.Ascending;
            }
            else
            {
                // Set the column number that is to be sorted; default to ascending.
                lvcs.SortColumn = colIndex;
                lvcs.Order = SortOrder.Ascending;
            }
            // Perform the sort with these new sort options.
            lv.Sort();
        }
        /// <summary> get selected items from listview inside a string[] </summary>
        /// <param name="listview"></param>
        /// <param name="subitem"></param>
        /// <returns></returns>
        public static string[] GetSelectedItems(ListView listview, int subitem)
        {   // validation
            if (listview == null)
                throw new ArgumentNullException("listview", GuiHelper.GetResourceMsg("ArgumentNull"));
            // selection?
            if(listview.SelectedItems.Count == 0 || subitem < 0)
                return new string[0];
            else
            {
                // collecting selected tables
                string[] tables = new string[listview.SelectedItems.Count];
                for (int i = 0; i < tables.Length; i++)
                    tables[i] = listview.SelectedItems[i].SubItems[subitem].Text.ToString();

                return tables;
            }
        }
        /// <summary> filter listbox with list </summary>
        /// <param name="filter"></param>
        /// <param name="list"></param>
        public static string[] FilterList(string filter, ArrayList list)
        {   // validate
            if (filter == null)
                throw new ArgumentNullException("filter", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (list == null)
                throw new ArgumentNullException("list", GuiHelper.GetResourceMsg("ArgumentNull"));
            // filter
            filter = filter.ToUpper(NyxMain.NyxCI);
            ArrayList alItems = new ArrayList();
            foreach (string item in list)
            {   // we got no more data...
                if (item == null || !item.ToUpper(NyxMain.NyxCI).Contains(filter))
                    continue;
                // add items
                alItems.Add(item);
            }
            // create return string[]
            string[] items = new string[alItems.Count];
            alItems.CopyTo(items);

            return items;
        }
        /// <summary> filter listview </summary>
        /// <param name="filter"></param>
        /// <param name="lpo"></param>
        public static System.Collections.ObjectModel.Collection<Classes.PreviewObject> FilterList(string filter,
            System.Collections.ObjectModel.Collection<Classes.PreviewObject> lpo)
        {   // validate
            if (filter == null)
                throw new ArgumentNullException("filter", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (lpo == null)
                throw new ArgumentNullException("lpo", GuiHelper.GetResourceMsg("ArgumentNull"));
            // filter
            filter = filter.ToUpper(NyxMain.NyxCI);
            System.Collections.ObjectModel.Collection<Classes.PreviewObject> alItems = 
                new System.Collections.ObjectModel.Collection<Classes.PreviewObject>();
            foreach (Classes.PreviewObject po in lpo)
            {
                if (!po.Name.ToUpper(NyxMain.NyxCI).Contains(filter))
                    continue;
                // add
                Classes.PreviewObject poFiltered = po;
                alItems.Add(poFiltered);
            }
            return alItems;
        }
        /// <summary> get sqlbuilder treenode </summary>
        /// <param name="tablename"></param>
        /// <returns></returns>
        public static TreeNode SqlBldrGetTableTN(string tablename)
        {   // validate
            if (tablename == null)
                throw new ArgumentNullException("tablename", GuiHelper.GetResourceMsg("ArgumentNull"));
            // building treenode
            TreeNode tn = new TreeNode();
            tn.Text = tablename;
            tn.Name = tablename;
            tn.ToolTipText = "Table: " + tablename;
            TreeNode stn = new TreeNode();
            stn.Text = GuiHelper.GetResourceMsg("SBReceivingFields");
            stn.Name = GuiHelper.GetResourceMsg("SBReceivingFields");
            tn.Nodes.Add(stn);

            return tn;
        }
        /// <summary> get sqlbuilder describe tabletreenode </summary>
        /// <param name="strTblDesc"></param>
        /// <returns></returns>
        public static TreeNode SqlBldrGetTableDescTN(Classes.TableDetails strTblDesc)
        {   // build treenode
            TreeNode tn = new TreeNode();
            tn.Name = strTblDesc.Name;

            // determine which index
            if (strTblDesc.HasIndex == null)
                tn.Text = String.Format(NyxMain.NyxCI, "{0} / {1}", strTblDesc.Name, strTblDesc.Type);
            else
            {
                string index = String.Empty;
                switch (strTblDesc.HasIndex)
                {
                    case "P ": 
                        index = "P / ";
                        tn.ForeColor = Color.Green;
                        break;
                    case "I ": 
                        index = "I / ";
                        tn.ForeColor = Color.YellowGreen;
                        break;
                    case "U": 
                        index = "U / ";
                        tn.ForeColor = Color.Red;
                        break;
                    case "P I ": 
                        index = "PI / ";
                        tn.ForeColor = Color.Green;
                        break;
                    case "P U": 
                        index = "PU / ";
                        tn.ForeColor = Color.Green;
                        break;
                    case "I U": 
                        index = "IU / ";
                        tn.ForeColor = Color.YellowGreen;
                        break;
                    case "P I U": 
                        index = "PIU / ";
                        tn.ForeColor = Color.Green;
                        break;
                }
                tn.Text = String.Format(NyxMain.NyxCI, "{0}{1} / {2}", index, strTblDesc.Name, strTblDesc.Type);
            }
            // tooltips
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql:
                    tn.ToolTipText = String.Format(NyxMain.NyxCI, "Index: {0}\nName: {1}\nType: {2}\nNull: {3}\nDefault: {4}\nExtra: {5}", 
                                        strTblDesc.HasIndex, strTblDesc.Name, strTblDesc.Type,
                                        strTblDesc.Null, strTblDesc.DefaultValue, strTblDesc.Extra);
                    break;
                case DBType.Oracle:
                    tn.ToolTipText = String.Format(NyxMain.NyxCI, "Index: {0}\nName: {1}\nType: {2}\nLength: {3}\nPrecision: {4}",
                                        strTblDesc.HasIndex, strTblDesc.Name, strTblDesc.Type, strTblDesc.Lenght, strTblDesc.Precision);
                    break;
                case DBType.MSSql:
                    tn.ToolTipText = String.Format(NyxMain.NyxCI, "Index: {0}\nName: {1}\nType: {2}\nLength: {3}\nPrecision: {4}\nIsNullable: {5}",
                                        strTblDesc.HasIndex, strTblDesc.Name, strTblDesc.Type, strTblDesc.Lenght, 
                                        strTblDesc.Precision, strTblDesc.Null);
                    break;
                case DBType.PgSql:
                    tn.ToolTipText = String.Format(NyxMain.NyxCI, "Index: {0}\nName: {1}\nType: {2}\nLength: {3}\nVar. Length: {4}\nNotNull: {5}",
                                        strTblDesc.HasIndex, strTblDesc.Name, strTblDesc.Type, strTblDesc.Lenght,
                                        strTblDesc.Precision, strTblDesc.Null);
                    break;
            }
            tn.Tag = strTblDesc;
            return tn;
        }
        /// <summary> Format referenced datagridview with the userappsettings </summary>
        /// <param name="uaSett">current loaded userappsettings</param>
        /// <param name="dgv">datagridview to format</param>
        public static DataGridView DataGridViewFormat(UserAppSettings uaSett, DataGridView dgv)
        {   // validate
            if (uaSett == null)
                throw new ArgumentNullException("uaSett", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (dgv == null)
                throw new ArgumentNullException("dgv", GuiHelper.GetResourceMsg("ArgumentNull"));
            // format
            Color dataGridForeColor = Color.Black;
            Color dataGridBackColor = Color.White;
            Color dataGridAltBackColor = Color.GhostWhite;
            Color dataGridAltForeColor = Color.Black;
            Color dataGridSelForeColor = Color.AliceBlue;
            Color dataGridSelBackColor = Color.Gray;
            DataGridViewCellStyle dgvcs = new DataGridViewCellStyle();

            if (uaSett.DataGridFont.Length > 0)
            {
                Font font;
                GuiHelper.GetFont(uaSett.DataGridFont, out font);
                dgv.Font = font;
            }
            // forecolor
            Color color = Color.FromArgb(uaSett.DataGridForeColor);
            if (!color.IsEmpty)
                dataGridForeColor = color;
            dgvcs.ForeColor = dataGridForeColor;
            // backcolor
            color = Color.FromArgb(uaSett.DataGridBackColor);
            if (!color.IsEmpty)
                dataGridBackColor = color;
            dgvcs.BackColor = dataGridBackColor;
            // selection forecolor
            color = Color.FromArgb(uaSett.DataGridSelForeColor);
            if (!color.IsEmpty)
                dataGridSelForeColor = color;
            dgvcs.SelectionForeColor = dataGridSelForeColor;
            // selection backcolor
            color = Color.FromArgb(uaSett.DataGridSelBackColor);
            if (!color.IsEmpty)
                dataGridSelBackColor = color;
            dgvcs.SelectionBackColor = dataGridSelBackColor;
            // apply style
            dgv.RowsDefaultCellStyle = dgvcs;
            // reset style
            dgvcs = new DataGridViewCellStyle();
            // init alternate datagridviewcellstyle
            color = Color.FromArgb(uaSett.DataGridSelBackColor);
            dgvcs.SelectionForeColor = dataGridSelForeColor;
            dgvcs.SelectionBackColor = dataGridSelBackColor;
            // alternate forecolor
            color = Color.FromArgb(uaSett.DataGridAltForeColor);
            if (!color.IsEmpty)
                dataGridAltForeColor = color;
            dgvcs.ForeColor = dataGridAltForeColor;
            // alternate backcolor
            color = Color.FromArgb(uaSett.DataGridAltBackColor);
            if (!color.IsEmpty)
                dataGridAltBackColor = color;
            dgvcs.BackColor = dataGridAltBackColor;
            // apply style
            dgv.AlternatingRowsDefaultCellStyle = dgvcs;
            // return 
            return dgv;
        }
        /// <summary> Build up a font object from the given string </summary>
        /// <param name="fontStr">font to build as string, seperated by :</param>
        /// <param name="convFont">build font</param>
        /// <returns>true / false</returns>
        public static bool GetFont(string fontStr, out Font convFont)
        {   // validate
            if (fontStr == null)
                throw new ArgumentNullException("fontStr", GuiHelper.GetResourceMsg("ArgumentNull"));

            // if false, we return the default
            string[] fontarray = fontStr.ToString().Split(';');
            if (fontarray.Length == 3)
            {
                FontStyle fs;
                // check font style
                switch (fontarray[2])
                {
                    case "Bold, Italic":
                        fs = FontStyle.Bold | FontStyle.Italic;
                        break;
                    case "Bold":
                        fs = FontStyle.Bold;
                        break;
                    case "Italic":
                        fs = FontStyle.Italic;
                        break;
                    default:
                        fs = FontStyle.Regular;
                        break;
                }
                // check font size
                float size = 0;
                if (!float.TryParse(fontarray[1], out size))
                    size = 8;
                // return
                convFont = new Font(fontarray[0], size, fs);
                return true;
            }
            convFont = new Font("Verdana", (float)8, FontStyle.Regular);
            return false;
        }

        #region guiinitfunctions
        // REMOVING COLUMN INDEX FROM LISTVIEWS NEEDS CODE UPDATE IN NyxMain.cs/TableActionFieldDropDone
        /// <summary>
        /// initialize listviews
        /// </summary>
        /// <param name="lvTableDesc"></param>
        /// <param name="lvIndexes"></param>
        /// <param name="lvIndexDesc"></param>
        public static void InitListView(ListView lvTableDesc,
            ListView lvIndexes,
            ListView lvIndexDesc)
        {
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql: SetMySqlLView(lvTableDesc, lvIndexes, lvIndexDesc); break;
                case DBType.Oracle: SetOracleLView(lvTableDesc, lvIndexes, lvIndexDesc); break;
                case DBType.MSSql: SetMSSqlLView(lvTableDesc, lvIndexes, lvIndexDesc); break;
                case DBType.PgSql: SetPGSqlLView(lvTableDesc, lvIndexes, lvIndexDesc); break;
            }
        }
        // init listviews for the preview 
        // init listviews for the tabletool
        private static void SetMySqlLView(ListView lvTableDescribe,
            ListView lvIndexDetails,
            ListView lvIndexDescribe)
        {
            /* tabledescripe listview 
             */
            ColumnHeader idx = new ColumnHeader();
            ColumnHeader field = new ColumnHeader();
            ColumnHeader type = new ColumnHeader();
            ColumnHeader nullv = new ColumnHeader();
            ColumnHeader defaultv = new ColumnHeader();
            ColumnHeader extra = new ColumnHeader();
            ColumnHeader collation = new ColumnHeader();
            ColumnHeader comment = new ColumnHeader();
            // setting columns
            idx.Text = GuiHelper.GetResourceMsg("GHLVIndexes");
            idx.Width = 25;
            idx.Name = "idx";
            field.Text = GuiHelper.GetResourceMsg("GHLVField");
            field.Width = 100;
            field.Name = "field";
            type.Text = GuiHelper.GetResourceMsg("GHLVType");
            type.Width = 80;
            type.Name = "type";
            nullv.Text = GuiHelper.GetResourceMsg("GHLVNotNull");
            nullv.Width = 75;
            nullv.Name = "nullv";
            defaultv.Text = GuiHelper.GetResourceMsg("GHLVDefault");
            defaultv.Width = 75;
            defaultv.Name = "defaultv";
            extra.Text = GuiHelper.GetResourceMsg("GHLVExtra");
            extra.Name = "extra";
            collation.Text = GuiHelper.GetResourceMsg("GHLVCollation");
            collation.Width = 100;
            collation.Name = "collation";
            comment.Text = GuiHelper.GetResourceMsg("GHLVComment");
            comment.Width = 250;
            comment.Name = "comment";
            // clear and add
            lvTableDescribe.Clear();
            lvTableDescribe.Columns.AddRange(new ColumnHeader[] { idx, field, type, nullv, defaultv, extra, collation, comment });
            /* idxdesc1 listview 
             */
            ColumnHeader idxName = new ColumnHeader();
            ColumnHeader idxIsUnique = new ColumnHeader();
            ColumnHeader idxIsFulltext = new ColumnHeader();
            // setting columns
            idxName.Text = GuiHelper.GetResourceMsg("GHLVIndex");
            idxName.Width = 100;
            idxName.Name = "index";
            idxIsUnique.Text = GuiHelper.GetResourceMsg("GHLVUnique");
            idxIsUnique.Width = 75;
            idxIsUnique.Name = "isunique";
            idxIsFulltext.Text = GuiHelper.GetResourceMsg("GHLVFulltext");
            idxIsFulltext.Width = 75;
            idxIsFulltext.Name = "isfulltext";
            // clear and add
            lvIndexDetails.Clear();
            lvIndexDetails.Columns.AddRange(new ColumnHeader[] { idxName, idxIsUnique, idxIsFulltext });
            /* idxdescribe listview 
             */
            ColumnHeader cname = new ColumnHeader();
            ColumnHeader ktype = new ColumnHeader();
            ColumnHeader cpos = new ColumnHeader();
            ColumnHeader subpart = new ColumnHeader();
            // setting columns
            cname.Text = GuiHelper.GetResourceMsg("GHLVColumnName");
            cname.Width = 100;
            cname.Name = "columnname";
            ktype.Text = GuiHelper.GetResourceMsg("GHLVType");
            ktype.Width = 75;
            ktype.Name = "type";
            cpos.Text = GuiHelper.GetResourceMsg("GHLVPosition");
            cpos.TextAlign = HorizontalAlignment.Right;
            cpos.Width = 75;
            cpos.Name = "position";
            subpart.Text = GuiHelper.GetResourceMsg("GHLVSubpart");
            subpart.Width = 75;
            subpart.TextAlign = HorizontalAlignment.Right;
            subpart.Name = "subpart";
            // clear and add
            lvIndexDescribe.Clear();
            lvIndexDescribe.Columns.AddRange(new ColumnHeader[] { cname, ktype, cpos, subpart });
        }
        private static void SetOracleLView(ListView lvTableDescribe,
            ListView lvIndexDetails,
            ListView lvIndexDescribe)
        {
            /* tabledescripe listview 
             */
            ColumnHeader idx = new ColumnHeader();
            ColumnHeader field = new ColumnHeader();
            ColumnHeader type = new ColumnHeader();
            ColumnHeader length = new ColumnHeader();
            ColumnHeader precission = new ColumnHeader();
            // setting columns
            idx.Text = GuiHelper.GetResourceMsg("GHLVIndexes");
            idx.Width = 25;
            idx.Name = "indexes";
            field.Text = GuiHelper.GetResourceMsg("GHLVField");
            field.Width = 100;
            field.Name = "field";
            type.Text = GuiHelper.GetResourceMsg("GHLVType");
            type.Width = 80;
            type.Name = "type";
            length.Text = GuiHelper.GetResourceMsg("GHLVLength");
            length.TextAlign = HorizontalAlignment.Right;
            length.Width = 75;
            length.Name = "length";
            precission.Text = GuiHelper.GetResourceMsg("GHLVPrecission");
            precission.TextAlign = HorizontalAlignment.Right;
            precission.Width = 75;
            precission.Name = "precission";
            // clear and add
            lvTableDescribe.Clear();
            lvTableDescribe.Columns.AddRange(new ColumnHeader[] { idx, field, type, length, precission });
            /* indexes listview
             */
            ColumnHeader idxName = new ColumnHeader();
            ColumnHeader idxIsPrimary = new ColumnHeader();
            ColumnHeader idxIsUnique = new ColumnHeader();
            ColumnHeader idxStatus = new ColumnHeader();
            ColumnHeader idxAnalyzed = new ColumnHeader();
            // setting columns
            idxName.Text = GuiHelper.GetResourceMsg("GHLVIndex");
            idxName.Width = 100;
            idxName.Name = "index";
            idxIsPrimary.Text = GuiHelper.GetResourceMsg("GHLVPrimary");
            idxIsPrimary.Width = 50;
            idxIsPrimary.Name = "isprimary";
            idxIsUnique.Text = GuiHelper.GetResourceMsg("GHLVUnique");
            idxIsUnique.Width = 50;
            idxIsUnique.Name = "isunique";
            idxStatus.Text = GuiHelper.GetResourceMsg("GHLVStatus");
            idxStatus.Width = 50;
            idxStatus.Name = "status";
            idxAnalyzed.Text = GuiHelper.GetResourceMsg("GHLVAnalyzed");
            idxAnalyzed.Width = 125;
            idxAnalyzed.Name = "analyzed";
            // clear and add
            lvIndexDetails.Clear();
            lvIndexDetails.Columns.AddRange(new ColumnHeader[] { idxName, idxIsPrimary, idxIsUnique, idxStatus, idxAnalyzed });
            /* indexdescribe listview 
             */
            ColumnHeader idxdCName = new ColumnHeader();
            ColumnHeader idxdCType = new ColumnHeader();
            ColumnHeader idxdCPos = new ColumnHeader();
            ColumnHeader idxdCDescend = new ColumnHeader();
            // setting columns
            idxdCName.Text = GuiHelper.GetResourceMsg("GHLVColumnName");
            idxdCName.Width = 100;
            idxdCName.Name = "columnname";
            idxdCType.Text = GuiHelper.GetResourceMsg("GHLVType");
            idxdCType.Width = 75;
            idxdCType.Name = "datatype";
            idxdCPos.Text = GuiHelper.GetResourceMsg("GHLVPosition");
            idxdCPos.TextAlign = HorizontalAlignment.Right;
            idxdCPos.Width = 50;
            idxdCPos.Name = "position";
            idxdCDescend.Text = GuiHelper.GetResourceMsg("GHLVDescending");
            idxdCDescend.Width = 50;
            idxdCDescend.Name = "descend";
            // clear and add
            lvIndexDescribe.Clear();
            lvIndexDescribe.Columns.AddRange(new ColumnHeader[] { idxdCName, idxdCType, idxdCPos, idxdCDescend });
        }
        private static void SetMSSqlLView(ListView lvTableDescribe,
            ListView lvIndexDetails,
            ListView lvIndexDescribe)
        {
            /* tabledescripe listview 
             */
            ColumnHeader idx = new ColumnHeader();
            ColumnHeader field = new ColumnHeader();
            ColumnHeader type = new ColumnHeader();
            ColumnHeader length = new ColumnHeader();
            ColumnHeader precission = new ColumnHeader();
            ColumnHeader isnull = new ColumnHeader();
            // setting columns
            idx.Text = GuiHelper.GetResourceMsg("GHLVIndexes");
            idx.Width = 25;
            idx.Name = "indexes";
            field.Text = GuiHelper.GetResourceMsg("GHLVField");
            field.Width = 100;
            field.Name = "field";
            type.Text = GuiHelper.GetResourceMsg("GHLVType");
            type.Width = 80;
            type.Name = "type";
            length.Text = GuiHelper.GetResourceMsg("GHLVLength");
            length.TextAlign = HorizontalAlignment.Right;
            length.Width = 75;
            length.Name = "length";
            precission.Text = GuiHelper.GetResourceMsg("GHLVPrecission");
            precission.TextAlign = HorizontalAlignment.Right;
            precission.Width = 75;
            precission.Name = "precission";
            isnull.Text = GuiHelper.GetResourceMsg("GHLVIsNull");
            isnull.Width = 75;
            isnull.Name = "isnull";
            // clear and add
            lvTableDescribe.Clear();
            lvTableDescribe.Columns.AddRange(new ColumnHeader[] { idx, field, type, length, precission, isnull });
            /* idxdesc1 listview 
             */
            ColumnHeader idxName = new ColumnHeader();
            ColumnHeader idxStatus = new ColumnHeader();
            ColumnHeader idxFulltext = new ColumnHeader();
            ColumnHeader idxDesc = new ColumnHeader();
            // setting columns
            idxName.Text = GuiHelper.GetResourceMsg("GHLVIndex"); 
            idxName.Width = 100;
            idxName.Name = "index";
            idxStatus.Text = GuiHelper.GetResourceMsg("GHLVStatus"); 
            idxStatus.Width = 100;
            idxStatus.Name = "status";
            idxFulltext.Text = GuiHelper.GetResourceMsg("GHLVFulltext");
            idxFulltext.Width = 75;
            idxFulltext.Name = "fulltext";
            idxDesc.Text = GuiHelper.GetResourceMsg("GHLVDescending");
            idxDesc.Width = 75;
            idxDesc.Name = "descending";
            // clear and add
            lvIndexDetails.Clear();
            lvIndexDetails.Columns.AddRange(new ColumnHeader[] { idxName, idxStatus, idxFulltext, idxDesc });
            /* idxdesc2 listview 
             */
            ColumnHeader cname = new ColumnHeader();
            // setting columns
            cname.Text = GuiHelper.GetResourceMsg("GHLVColumnName");
            cname.Width = 250;
            cname.Name = "columnname";
            // clear and add
            lvIndexDescribe.Clear();
            lvIndexDescribe.Columns.AddRange(new ColumnHeader[] { cname });
        }
        private static void SetPGSqlLView(ListView lvTableDescribe,
            ListView lvIndexDetails,
            ListView lvIndexDescribe)
        {
            /* tabledescripe listview 
             */
            ColumnHeader idx = new ColumnHeader();
            ColumnHeader field = new ColumnHeader();
            ColumnHeader type = new ColumnHeader();
            ColumnHeader length = new ColumnHeader();
            ColumnHeader lengthvar = new ColumnHeader();
            ColumnHeader notnull = new ColumnHeader();
            ColumnHeader defaultcol = new ColumnHeader();
            // setting columns
            idx.Text = GuiHelper.GetResourceMsg("GHLVIndexes");
            idx.Width = 25;
            idx.Name = "indexes";
            field.Text = GuiHelper.GetResourceMsg("GHLVField");
            field.Width = 100;
            field.Name = "field";
            type.Text = GuiHelper.GetResourceMsg("GHLVType");
            type.Width = 80;
            type.Name = "type";
            length.Text = GuiHelper.GetResourceMsg("GHLVLength");
            length.TextAlign = HorizontalAlignment.Right;
            length.Width = 75;
            length.Name = "length";
            lengthvar.Text = GuiHelper.GetResourceMsg("GHLVVLength");
            lengthvar.TextAlign = HorizontalAlignment.Right;
            lengthvar.Width = 75;
            lengthvar.Name = "varlength";
            notnull.Text = GuiHelper.GetResourceMsg("GHLVNotNull");
            notnull.Width = 75;
            notnull.Name = "notnull";
            defaultcol.Text = GuiHelper.GetResourceMsg("GHLVDefault");
            defaultcol.Width = 100;
            defaultcol.Name = "default";
            // clear & add
            lvTableDescribe.Clear();
            lvTableDescribe.Columns.AddRange(new ColumnHeader[] { idx, field, type, length, lengthvar, notnull, defaultcol });
            /* idxdexes listview
             */
            ColumnHeader idxName = new ColumnHeader();
            ColumnHeader idxIsPrimary = new ColumnHeader();
            ColumnHeader idxIsUnique = new ColumnHeader();
            ColumnHeader idxIsClustered = new ColumnHeader();
            // setting columns
            idxName.Text = GuiHelper.GetResourceMsg("GHLVIndex");
            idxName.Width = 100;
            idxName.Name = "index";
            idxIsPrimary.Text = GuiHelper.GetResourceMsg("GHLVPrimary");
            idxIsPrimary.Width = 75;
            idxIsPrimary.Name = "primary";
            idxIsUnique.Text = GuiHelper.GetResourceMsg("GHLVUnique");
            idxIsUnique.Width = 75;
            idxIsUnique.Name = "unique";
            idxIsClustered.Text = GuiHelper.GetResourceMsg("GHLVClustered");
            idxIsClustered.Width = 75;
            idxIsClustered.Name = "clustered";
            // clear & add
            lvIndexDetails.Clear();
            lvIndexDetails.Columns.AddRange(new ColumnHeader[] { idxName, idxIsPrimary, idxIsUnique, idxIsClustered });
            /* indexdescribe 
             */
            ColumnHeader cname = new ColumnHeader();
            // setting columns
            cname.Text = GuiHelper.GetResourceMsg("GHLVColumnName");
            cname.Width = 250;
            cname.Name = "columnname";
            // clear & add
            lvIndexDescribe.Clear();
            lvIndexDescribe.Columns.AddRange(new ColumnHeader[] { cname });
        }
        #endregion
    }
}
