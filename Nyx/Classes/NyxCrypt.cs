/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.IO;
using System.Security.Cryptography;

namespace Nyx.Classes
{
    class NyxCrypt
    {
        private readonly string Key = "_�f_��A^��O2�(�n�'AOŷ���#PM[3��?h�E!M|~(P�o�mD�����od��B(���Z�V��AI�z���r��m��!�U�a ��a(ƹbR�1B�q������w� �����ڤM!=(��#di��!9H�fePuTTYPuTTYPuTTYPuTTYPuTTY��_Aiܳ�";
        private RijndaelManaged rij = null;

        private Exception errExc;
        public Exception ErrExc { get { return errExc; } }
        
        public NyxCrypt()
        {   // set init values for rijndael
            rij = new RijndaelManaged();
            rij.KeySize = 256;
            rij.BlockSize = 256;
            rij.IV = GetInitKeyVektor(Key);
            rij.Key = GetFrmtByteKey(Key);
            rij.Padding = PaddingMode.PKCS7;
        }

        private static byte[] GetFrmtByteKey(string key)
        {
            /* keysize = unicodestringsize(keysize) / 16
             *  16 cause of unicodes DBCS
             *  normaly use 8 to get the keysize
             */
            int keylen = 256 / 16;
            /* fill/cut the key up to length of keylen
             *  1. cut
             *  2. fill with #
             */
            if (key.Length > keylen) 
                key = key.Substring(0, keylen); 
            else if (key.Length < keylen) 
                key = key.PadRight(keylen, '#'); 

            // auf 256bit getrimmtes Password in einen Unicodekonformen Schluessel wandeln
            return System.Text.Encoding.Unicode.GetBytes(key);
        }
        private static byte[] GetInitKeyVektor(string key)
        {
            // here we just use the key as init vektor
            return GetFrmtByteKey(key);
        }

        /// <summary>
        /// Encrypt <paramref name="strIntput"/>
        /// </summary>
        /// <param name="strInput">string to encrypt</param>
        /// <returns>encrypted string or null (if failed)</returns>
        public string EncryptString(string strInput)
        {
            // return string
            string strRes = String.Empty;
            if (strInput.Length == 0)
                return strRes;

            // streams
            MemoryStream msEncrypt = null;
            CryptoStream csCrypt = null;
            // working...
            try
            {
                // init the output stream...
                msEncrypt = new MemoryStream();

                // now we create the middle-crypto-strema for encrypting
                csCrypt = new CryptoStream(msEncrypt, rij.CreateEncryptor(), CryptoStreamMode.Write);

                // convert the input to a bytearray
                System.Text.UnicodeEncoding encoder = new System.Text.UnicodeEncoding();
                byte[] toencrypt = encoder.GetBytes(strInput);

                // now we write into the stream
                csCrypt.Write(toencrypt, 0, toencrypt.Length);
                csCrypt.FlushFinalBlock();

                // and convert the output to a byte array again
                byte[] encrypted = msEncrypt.ToArray();

                // now we convert the encryptet string to hex for being able to write it to a file
                System.Text.StringBuilder sbRes = new System.Text.StringBuilder();
                foreach (byte singleByte in encrypted)
                    sbRes.Append(singleByte.ToString("X2", NyxMain.NyxCI));
                // add to final result
                strRes = sbRes.ToString();
            }
            catch (ArgumentException aexc) { errExc = aexc; }
            finally
            {   // cleanup (dispose will call close)
                if (csCrypt != null)
                    csCrypt.Dispose();
                if (msEncrypt != null)
                    msEncrypt.Dispose();
            }
            // return
            return strRes;
        }
        /// <summary>
        /// Decrypt <paramref name="input"/>
        /// </summary>
        /// <param name="input">string to decrypt</param>
        /// <param name="encstr">decrypted string</param>
        /// <returns>true/false</returns>
        public bool DecryptString(string input, ref string encstr)
        {
            if (input.Length == 0)
            {
                encstr = String.Empty;
                return true;
            }
            // streams
            MemoryStream msDecrypt = null;
            CryptoStream csDecrypt = null;
            // working
            try
            {
                // converting from hex back to bytes (divide with 2 cause of hex display)
                byte[] decrypted = new byte[input.Length / 2];
                int j = 0;
                for (int i = 0; i < input.Length && i + 1 < input.Length; i += 2)
                {
                    decrypted[j] = byte.Parse(input.Substring(i, 2), System.Globalization.NumberStyles.AllowHexSpecifier, NyxMain.NyxCI);
                    j++;
                }
                // filling stream
                msDecrypt = new MemoryStream(decrypted);
                // now we create the middle-crypto-strema for decrypting
                csDecrypt = new CryptoStream(msDecrypt, rij.CreateDecryptor(), CryptoStreamMode.Read);
                // create output byte array and afterwards decrypt
                byte[] encrypted = new byte[decrypted.Length];
                csDecrypt.Read(encrypted, 0, encrypted.Length);
                // now we convert the streamoutput to byte and afterwards to string
                encstr = System.Text.Encoding.Unicode.GetString(encrypted).Replace("\0", "");
            }
            catch (System.FormatException)
            {
                encstr = String.Empty;
            }
            catch (CryptographicException ex)
            {
                errExc = ex;
                return false;
            }
            finally
            {   // cleanup (dispose will call close)
                if (csDecrypt != null)
                    csDecrypt.Dispose();
                if(msDecrypt != null)
                    msDecrypt.Dispose();
            }
            return true;
        }



        public static string DecryptPasw(string password)
        {   // init
            string result = null;
            NyxCrypt nc = new NyxCrypt();
            // try to decrypt
            if (!nc.DecryptString(password, ref result))
                AppDialogs.MessageHandler.ShowDialog(
                    GuiHelper.GetResourceMsg("ErrDecryptPW", new string[] { nc.ErrExc.Message.ToString() }),
                    nc.ErrExc,
                    AppDialogs.MessageHandler.EnumMsgType.Warning);
            // return result
            return result;
        }
        public static string EncryptPasw(string password)
        {   // init
            NyxCrypt nc = new NyxCrypt();
            // return encrypted
            return nc.EncryptString(password);
        }

    }
}
