/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;

namespace NyxAutoComplete
{
    /// <summary>
    /// Represents an <i>item</i> in the Intellisense (AutoComplete) box.
    /// Each item consists of a text description and an (optional) image
    /// associated with it.
    /// </summary>
    public class ACListBoxItem
    {
        private string retText;
        private int imgIndex;

        // public properties 
        /// <summary>
        /// Gets or sets the descriptive text for the item.
        /// </summary>
        /// <value>The text.</value>
        public string Text
        {
            get { return retText; }
            set { retText = value; }
        }
        /// <summary>
        /// Gets or sets the index of the image.
        /// </summary>
        /// <value>The index of the image for the item.</value>
        public int ImageIndex
        {
            get { return imgIndex; }
            set { imgIndex = value; }
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ACListBoxItem"/> 
        /// Intellisense (AutoComplete) item, when <b>both</b> the description
        /// and the image index to be used are specified.
        /// </summary>
        /// <param name="text">The descriptive text for the item.</param>
        /// <param name="imgindex">The index of the image for the item.</param>
        public ACListBoxItem(string text, int imgindex)
        {
            retText = text;
            imgIndex = imgindex;
        }
        /// <summary>
        /// Initializes a new instance of the <see cref="ACListBoxItem"/> 
        /// Intellisense (AutoComplete) item, when <b>only</b> the description
        /// is specified.
        /// </summary>
        /// <param name="text">The descriptive text for the item.</param>
        public ACListBoxItem(string text) : this(text, -1) { }
        /// <summary>
        /// Initializes a new <b>empty</b> instance of the <see cref="ACListBoxItem"/> 
        /// Intellisense (AutoComplete) item.
        /// </summary>
        public ACListBoxItem() : this(String.Empty) { }

        /// <summary>
        /// Returns a <see cref="T:System.String"></see> that represents the current <see cref="ACListBoxItem"> item </see>.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.String"></see> that represents the current <see cref="ACListBoxItem"> item </see>.
        /// </returns>
        public override string ToString()
        {
            return retText;
        }
    }

    /// <summary>
    /// Represents the Intellisense (AutoComplete) box.
    /// </summary>

	public class ACListBox : ListBox
	{
		private ImageList imageList;
        /// <summary>
        /// Gets or sets the image list.
        /// </summary>
        /// <value>The image list.</value>
        public ImageList ImageList
        {
            get { return imageList; }
            set { imageList = value; }
        }

        /// <summary>
        /// Initializes a new instance of the Intellisense (AutoComplete) box <see cref="ACListBox"/>.
        /// </summary>
		public ACListBox()
		{   // we have to draw by hand..
			this.DrawMode = DrawMode.OwnerDrawFixed;
		}
        // so... :)
        /// <summary>
        /// Draws the AutoComplete box by raising the <see cref="E:System.Windows.Forms.ListBox.DrawItem"></see> event.
        /// </summary>
        /// <param name="e">A <see cref="T:System.Windows.Forms.DrawItemEventArgs"></see> that contains the event data.</param>
		protected override void OnDrawItem(System.Windows.Forms.DrawItemEventArgs e)
		{   
			e.DrawBackground();
			e.DrawFocusRectangle();
			ACListBoxItem item;
            System.Drawing.Rectangle bounds = e.Bounds;
            System.Drawing.Size imageSize = imageList.ImageSize;
            if (Items[e.Index] is ACListBoxItem)
            {
                item = (ACListBoxItem)Items[e.Index];
                if (item.ImageIndex != -1)
                {
                    imageList.Draw(e.Graphics, bounds.Left, bounds.Top, item.ImageIndex);
                    e.Graphics.DrawString(item.Text, e.Font, new System.Drawing.SolidBrush(e.ForeColor),
                        bounds.Left + imageSize.Width, bounds.Top);
                }
                else
                    e.Graphics.DrawString(item.Text, e.Font, new System.Drawing.SolidBrush(e.ForeColor),
                        bounds.Left, bounds.Top);
            }
            else
            {
                if (e.Index != -1)
                    e.Graphics.DrawString(Items[e.Index].ToString(), e.Font,
                        new System.Drawing.SolidBrush(e.ForeColor), bounds.Left, bounds.Top);
                else
                    e.Graphics.DrawString(Text, e.Font, new System.Drawing.SolidBrush(e.ForeColor),
                        bounds.Left, bounds.Top);
            }
			base.OnDrawItem(e);
		}
	}
}
