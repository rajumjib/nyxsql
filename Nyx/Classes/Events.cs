/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Text;

namespace Nyx
{
    /// <summary>
    /// EventArguments for notifying restart requests
    /// </summary>
    public class ControlAppEventArgs : EventArgs
    {   // private
        private bool restartApp;
        /// <remarks> RestartApp? </remarks>
        public bool RestartApp { get { return restartApp; } set { restartApp = value; } }
    }

    /// <summary>
    /// EventArguments for notifying change of settings
    /// </summary>
    public class SettingsChangedEventArgs : EventArgs
    {   // private
        private NyxSettings.SettingsType settingsType;
        /// <remarks> Type of changed settings </remarks>
        public NyxSettings.SettingsType SettingsType { get { return settingsType; } set { settingsType = value; } }
    }
    /// <summary>
    /// EventArguments for notifying database content changed
    /// </summary>
    public class DBContentChangedEventArgs: EventArgs
    {   // private
        private DatabaseContent dbContent;
        private string param1;
        /// <remarks> DbContent which changed </remarks>
        public DatabaseContent DBContent { get { return dbContent; } set { dbContent = value; } }
        /// <remarks> Parameter1, for example the table for Tablefield actions </remarks>
        public string Param1 { get { return param1; } set { param1 = value; } }
    }
}
