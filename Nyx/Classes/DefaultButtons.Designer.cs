namespace Nyx.Classes
{
    partial class DefaultButtons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DefaultButtons));
            this._btOk = new System.Windows.Forms.Button();
            this._btCancel = new System.Windows.Forms.Button();
            this._btClose = new System.Windows.Forms.Button();
            this._btContinue = new System.Windows.Forms.Button();
            this._btExit = new System.Windows.Forms.Button();
            this._btNo = new System.Windows.Forms.Button();
            this._btYes = new System.Windows.Forms.Button();
            this._btRefresh = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _btOk
            // 
            this._btOk.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btOk, "_btOk");
            this._btOk.Name = "_btOk";
            this._btOk.UseVisualStyleBackColor = true;
            // 
            // _btCancel
            // 
            this._btCancel.Image = global::Nyx.Properties.Resources.cancel;
            resources.ApplyResources(this._btCancel, "_btCancel");
            this._btCancel.Name = "_btCancel";
            this._btCancel.UseVisualStyleBackColor = true;
            // 
            // _btClose
            // 
            this._btClose.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._btClose, "_btClose");
            this._btClose.Name = "_btClose";
            this._btClose.UseVisualStyleBackColor = true;
            // 
            // _btContinue
            // 
            this._btContinue.Image = global::Nyx.Properties.Resources.control_play_blue;
            resources.ApplyResources(this._btContinue, "_btContinue");
            this._btContinue.Name = "_btContinue";
            this._btContinue.UseVisualStyleBackColor = true;
            // 
            // _btExit
            // 
            this._btExit.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._btExit, "_btExit");
            this._btExit.Name = "_btExit";
            this._btExit.UseVisualStyleBackColor = true;
            // 
            // _btNo
            // 
            this._btNo.Image = global::Nyx.Properties.Resources.delete;
            resources.ApplyResources(this._btNo, "_btNo");
            this._btNo.Name = "_btNo";
            this._btNo.UseVisualStyleBackColor = true;
            // 
            // _btYes
            // 
            this._btYes.Image = global::Nyx.Properties.Resources.accept;
            resources.ApplyResources(this._btYes, "_btYes");
            this._btYes.Name = "_btYes";
            this._btYes.UseVisualStyleBackColor = true;
            // 
            // _btRefresh
            // 
            this._btRefresh.Image = global::Nyx.Properties.Resources.arrow_refresh;
            resources.ApplyResources(this._btRefresh, "_btRefresh");
            this._btRefresh.Name = "_btRefresh";
            this._btRefresh.UseVisualStyleBackColor = true;
            // 
            // DefaultButtons
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this._btRefresh);
            this.Controls.Add(this._btNo);
            this.Controls.Add(this._btYes);
            this.Controls.Add(this._btExit);
            this.Controls.Add(this._btContinue);
            this.Controls.Add(this._btClose);
            this.Controls.Add(this._btCancel);
            this.Controls.Add(this._btOk);
            this.Name = "DefaultButtons";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button _btOk;
        private System.Windows.Forms.Button _btCancel;
        private System.Windows.Forms.Button _btClose;
        private System.Windows.Forms.Button _btContinue;
        private System.Windows.Forms.Button _btExit;
        private System.Windows.Forms.Button _btNo;
        private System.Windows.Forms.Button _btYes;
        private System.Windows.Forms.Button _btRefresh;
    }
}