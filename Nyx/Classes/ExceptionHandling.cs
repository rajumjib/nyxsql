/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Windows.Forms;
using System.Runtime.Serialization;

namespace Nyx.Classes
{
    /// <summary> custom exception for database errors </summary>
    [Serializable]
    public class DBException : Exception
    {
        /// <summary> default constructor </summary>
        public DBException() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected DBException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            // not implemented
        }
        /// <summary>
        /// create exception with message
        /// </summary>
        /// <param name="message"></param>
        public DBException(string message) : base (message)
        {   // helplink
            base.HelpLink = "https://nyx.sigterm.eu/cgi-bin/trac.cgi/newticket";
            // stacktrace
            System.Diagnostics.StackTrace str = new System.Diagnostics.StackTrace(true);
            base.Source = str.ToString();
        }
        /// <summary>
        /// create exception with message and innerexception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public DBException(string message, Exception innerException)
            : base(message, innerException)
        {   // helplink
            base.HelpLink = "https://nyx.sigterm.eu/cgi-bin/trac.cgi/newticket";
            // stacktrace
            System.Diagnostics.StackTrace str = new System.Diagnostics.StackTrace(true);
            base.Source = str.ToString();
        }
    }

    /// <summary> ThreadExceptionHandler </summary>
    internal class ThreadExceptionHandler
    {
        /// <summary> threadexception handling </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e)
        {   // Exit the program if the user clicks Abort.
            DialogResult dres = AppDialogs.MessageHandler.ShowDialog(e.Exception, AppDialogs.MessageHandler.EnumMsgType.Exception);
            if (dres == DialogResult.Cancel)
                Application.Exit();
        }
        /// <summary> unhandled exception handling </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {   // Exit the program if the user clicks Abort.
            DialogResult dres = AppDialogs.MessageHandler.ShowDialog(e.ToString(), AppDialogs.MessageHandler.EnumMsgType.Exception);
            if (dres == DialogResult.Cancel)
                Application.Exit();

            if (e.IsTerminating)
                Application.Exit();
        }
    }
}
