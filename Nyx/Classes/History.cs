/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using NyxSettings;

namespace Nyx
{
    class History
    {
        private Exception errExc;
        /// <summary>
        /// Exception if an action fails
        /// </summary>
        public Exception ErrExc
        {
            get { return errExc; }
        }
        /// <summary>
        /// Fill history combobox 
        /// </summary>
        /// <param name="qryHis">combobox to fill</param>
        public void SetHis(System.Windows.Forms.ComboBox qryHis)
        {	// resetting combobox
            qryHis.BeginUpdate();
            qryHis.Items.Clear();
            // tempvar
            System.Collections.Queue qEntrys;
            if (this.ReadHis(out qEntrys))
            {
                // set default text
                if (qEntrys.Count == 0)
                    qryHis.Items.Add("No history logged.");
                else
                {
                    qryHis.Items.Add("select query (history)...");
                    foreach (string entry in qEntrys)
                        qryHis.Items.Add(entry);
                }
            }
            // selecting index 0
            qryHis.SelectedIndex = 0;
            qryHis.EndUpdate();
        }
        /// <summary>
        /// Read history from file and output into queue
        /// </summary>
        /// <param name="qEntrys">output queue</param>
        /// <returns>successfull or not</returns>
        public bool ReadHis(out System.Collections.Queue qEntrys)
        {
            // array for saving the history entrys
            string path = Settings.GetFolder(SettingsType.History) + Settings.FileNameHistory;
            qEntrys = new System.Collections.Queue();
            System.Xml.XmlTextReader xmlTRdr = null;

            try
            {
                if (System.IO.File.Exists(path))
                {
                    xmlTRdr = new System.Xml.XmlTextReader(path);
                    // going through all entrys
                    int i = 0;
                    string entry;
                    while (xmlTRdr.Read() && i < NyxMain.UASett.HistorySaveQueries)
                    {	// have we found an entry?					
                        if (xmlTRdr.LocalName.ToString() == "entry")
                        {	// if we got a value
                            entry = xmlTRdr.ReadString();
                            if (entry != null)
                                qEntrys.Enqueue(entry);
                        }
                    }
                }
                return true;
            }
            catch (System.IO.FileNotFoundException fnfexc)
            {   // err
                errExc = fnfexc;
                return false;
            }
            catch (System.IO.DirectoryNotFoundException dnfexc)
            {   // err
                errExc = dnfexc;
                return false;
            }
            catch (System.Xml.XmlException xexc)
            {	// err
                errExc = xexc;
                return false;
            }
            finally
            {
                if (xmlTRdr != null)
                    xmlTRdr.Close();
            }
        }
        /// <summary>
        /// Clear history
        /// </summary>
        /// <returns>successfull or not</returns>
        public bool ClearHis()
        {
            string path = Settings.GetFolder(SettingsType.History) + Settings.FileNameHistory;
            // init some vars
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            // creating xml dec
            System.Xml.XmlNode xmlNode = xmlDoc.CreateNode(System.Xml.XmlNodeType.XmlDeclaration, "", "");
            // appending xml dec to xmldoc and afterwards adding a root element
            xmlDoc.AppendChild(xmlNode);
            System.Xml.XmlElement xmlElem = xmlDoc.CreateElement("describtion");
            xmlDoc.AppendChild(xmlElem);
            // saving empty xml doc
            try
            {
                xmlDoc.Save(path);
                return true;
            }
            catch (System.Xml.XmlException xexc)
            {   // err
                errExc = xexc;
                return false;
            }
        }
        /// <summary>
        /// Add to history
        /// </summary>
        /// <param name="qry">querystring to add</param>
        /// <returns>successfull or not</returns>
        public bool AddHis(string qry)
        {   // init some vars
            System.Xml.XmlDocument xmlDoc = new System.Xml.XmlDocument();
            System.Xml.XmlNode xmlNode = null;
            System.Xml.XmlElement xmlElem = null, xmlElem2 = null;
            System.Xml.XmlText xmlVal = null;
            string strHisFullPath =
                    System.Windows.Forms.Application.StartupPath.ToString()
                    + "\\"
                    + Settings.FileNameHistory;
            // xml declaration
            xmlNode = xmlDoc.CreateNode(System.Xml.XmlNodeType.XmlDeclaration, "", "");
            xmlDoc.AppendChild(xmlNode);
            // adding root
            xmlElem = xmlDoc.CreateElement("", "describtion", "");
            xmlDoc.AppendChild(xmlElem);
            // readin of old-history
            System.Collections.Queue qOldHis;
            if (ReadHis(out qOldHis))
            {
                // adding the entry items
                xmlElem2 = xmlDoc.CreateElement("", "entry", "");
                xmlVal = xmlDoc.CreateTextNode(qry);
                xmlElem2.AppendChild(xmlVal);
                xmlDoc.ChildNodes.Item(1).AppendChild(xmlElem2);
                // now writing the oldhis again
                if (qOldHis.Count > 0)
                {	// if we found the entry we have to read all other
                    int i = 0;
                    foreach (string oldEntry in qOldHis)
                    {
                        if (i >= NyxMain.UASett.HistorySaveQueries
                            || oldEntry == null)
                        {
                            i++;
                            break;
                        }
                        if (qry == oldEntry)
                        {
                            i++;
                            continue;
                        }
                        // add entry
                        xmlElem2 = xmlDoc.CreateElement("", "entry", "");
                        xmlVal = xmlDoc.CreateTextNode(oldEntry);
                        xmlElem2.AppendChild(xmlVal);
                        xmlDoc.ChildNodes.Item(1).AppendChild(xmlElem2);
                        // inc i
                        i++;
                    }
                }
            }
            try
            {   // saving xmldocument
                xmlDoc.Save(strHisFullPath);
                return true;
            }
            catch (System.Xml.XmlException xexc)
            {   // err
                errExc = xexc;
                return false;
            }
        }
    }
}
