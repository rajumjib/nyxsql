/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Text;

namespace Nyx
{
    /// <summary> enum for the different actions which are executed inside a thread </summary>
    public enum DatabaseAction
    {
        /// <remarks> connect database </remarks>
        Connect,
        /// <remarks> disconnect database </remarks>
        Disconnect,
        /// <remarks> execute 'select' (data retrieving) query </remarks>
        SelectQuery,
        /// <remarks> execute data retrieving query </remarks>
        GetQuery,
        /// <remarks> execute 'non-select' (no data retrieving) query </remarks>
        ExecNonQry,
        /// <remarks> update changed dataset into the database </remarks>
        UpdateDS,
        /// <remarks> change database (mysql/mssql) </remarks>
        ChangeDB,
        /// <remarks> initialize the sqlbuilder </remarks>
        DBContentInit,
        /// <remarks> retrieve the tabledescripe for the sqlbuilder </remarks>
        SqlBldrGetTableDescribe,
        /// <remarks> initialize preview </remarks>
        PreviewInit,
        /// <remarks> initialize preview filter </remarks>
        PreviewInitFilter,
        /// <remarks> preview, retrieve next results </remarks>
        PrvGetNext,
        /// <remarks> preview, retrieve previous results </remarks>
        PrvGetPrev,
        /// <remarks> preview, retrieve last results </remarks>
        PreviewGetLast,
        /// <remarks> get preview objects (tables, views...) </remarks>
        GetPreviewObjects,
        /// <remarks> retrieve tables </remarks>
        GetTables,
        /// <remarks> retrieve full tabledescription (including indexes and so on...) </remarks>
        GetTableFullDesc,
        /// <remarks> retrieve views </remarks>
        GetViews,
        /// <remarks> retrieve view details </remarks>
        GetViewDetails,
        /// <remarks> retrieve procedures </remarks>
        GetProcedures,
        /// <remarks> retrieve procedure details </remarks>
        GetProcedureDetails,
        /// <remarks> retrieve triggers </remarks>
        GetTriggers,
        /// <remarks> retrieve trigger details </remarks>
        GetTriggerDetails,
        /// <remarks> retrieve indexes </remarks>
        GetIndexes,
        /// <remarks> retrieve index descripe </remarks>
        GetIndexDescribe
    }
    /// <summary> Again another enum for declaring thread actions, but this one special for table/index actions </summary>
    public enum TableAction
    {   /// <remarks> drop table(s) </remarks>
        TableDrop,
        /// <remarks> truncate table(s) </remarks>
        TableTruncate,
        /// <remarks> analyze table(s) </remarks>
        TableAnalyze,
        /// <remarks> repair table(s) </remarks>
        TableRepair,
        /// <remarks> optimize table(s) </remarks>
        TableOptimize,
        /// <remarks> check table(s) </remarks>
        TableCheck,
        /// <remarks> add field </remarks>
        FieldAdd,
        /// <remarks> alter field </remarks>
        FieldAlter,
        /// <remarks> drop field(s) </remarks>
        FieldDrop,
        /// <remarks> add index </remarks>
        IndexAdd,
        /// <remarks> alter index </remarks>
        IndexAlter,
        /// <remarks> rebuild index(es) </remarks>
        IndexRebuild,
        /// <remarks> analyze index(es) </remarks>
        IndexAnalyze,
        /// <remarks> drop index(es) </remarks>
        IndexDrop,
        /// <remarks> drop view(s) </remarks>
        ViewDrop
    }
    /// <summary> enum for database objects </summary>
    public enum DatabaseContent
    {
        /// <remarks> represents tables </remarks>
        Tables,
        /// <remarks> represents tablefields </remarks>
        TableFields,
        /// <remarks> represents indexes </remarks>
        Indexes,
        /// <remarks> represents views </remarks>
        Views,
        /// <remarks> represents procedures </remarks>
        Procedures,
        /// <remarks> represents triggers </remarks>
        Triggers,
        /// <remarks> represents tables, views an procedures </remarks>
        TablesViewsProcedures
    }
    /// <summary> Return Dialog enum </summary>
    public enum NyxDialogResult
    {
        /// <remarks> yes/ok </remarks>
        Yes,
        /// <remarks> no </remarks>
        No,
        /// <remarks> cancel </remarks>
        Cancel
    }
}
