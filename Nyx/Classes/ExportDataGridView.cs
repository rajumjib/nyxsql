/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using System.Windows.Forms;
using Nyx.AppDialogs;
using System.IO;

namespace Nyx.Classes
{
    /// <summary>
    /// Export datarow class
    /// </summary>
    class ExportDataGridView
    {   
        /// <summary>
        /// init
        /// </summary>
        private ExportDataGridView()
        {
        }

        /// <summary>
        /// export selected cells row to clipboard
        /// </summary>
        /// <param name="dgvscColl">datagridview</param>
        /// <param name="identifier">table</param>
        public static void Cells2Clipboard(DataGridViewSelectedCellCollection dgvscColl, string identifier)
        {   // check current rows
            if (dgvscColl.Count > 0)
            {
                DataTable dt = GetDGVCellsInDataTable(dgvscColl);
                if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
                    DoExport2Clipboard(dt, identifier);
            }
        }
        /// <summary>
        /// export the complete datagrid table to clipboard 
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="identifier"></param>
        public static void Table2Clipboard(DataTable dt, string identifier)
        {
            if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
                DoExport2Clipboard(dt, identifier);
        }

        private static void DoExport2Clipboard(DataTable dt, string identifier)
        {   // get export options
            StructExpOpt expopt = new StructExpOpt();
            DialogResult dres = ExportOptionsDia.ShowDialog("Field", ref expopt);
            if (dres == DialogResult.OK)
            {
                string expstr = String.Empty;
                switch (expopt.ExpType)
                {
                    case EnumExpType.Xml:
                        expstr = Export2Xml(dt, expopt, identifier);
                        break;
                    case EnumExpType.Csv:
                    case EnumExpType.Tsv:
                    case EnumExpType.Semicolon:
                        expstr = Export2Seperated(dt, expopt);
                        break;
                    case EnumExpType.InsertStm:
                        expstr = Export2InsertSqlStm(dt, expopt, identifier);
                        break;
                    case EnumExpType.DelStm:
                        expstr = Export2DeleteSqlStm(dt, expopt, identifier);
                        break;
                    case EnumExpType.Html:
                        expstr = Export2Html(dt, identifier);
                        break;
                }
                if (expstr.Length > 0)
                    Clipboard.SetText(expstr);
            }
        }

        /// <summary>
        /// export selected cells to file
        /// </summary>
        /// <param name="dgvscColl">datagridview</param>
        /// <param name="identifier">identifier</param>
        public static void Cells2File(DataGridViewSelectedCellCollection dgvscColl, string identifier)
        {   // check content
            if (dgvscColl.Count > 0)
            {
                DataTable dt = GetDGVCellsInDataTable(dgvscColl);
                if(dt.Rows.Count > 0 && dt.Columns.Count > 0)
                    DoExport2File(dt, identifier);
            }
        }
        /// <summary>
        /// export selected cells to file
        /// </summary>
        /// <param name="dgv">datagridview</param>
        /// <param name="identifier">identifier</param>
        public static void DataGridView2File(DataGridView dgv, string identifier)
        {   // check content
            if (dgv.Rows.Count > 0 && dgv.Columns.Count > 0)
            {
                DataTable dt = GetDGVasDataTable(dgv);
                if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
                    DoExport2File(dt, identifier);
            }
        }
        /// <summary>
        /// export datatable to file
        /// </summary>
        /// <param name="dt">datatable</param>
        /// <param name="identifier">identifier</param>
        public static void Table2File(DataTable dt, string identifier)
        {   // check content
            if (dt.Rows.Count > 0 && dt.Columns.Count > 0)
                DoExport2File(dt, identifier);
        }

        private static void DoExport2File(DataTable dt, string identifier)
        {
            // get export options
            StructExpOpt expopt = new StructExpOpt();
            DialogResult dres = ExportOptionsDia.ShowDialog("Field", ref expopt);
            if (dres == DialogResult.OK)
            {
                string expstr = String.Empty, filter = String.Empty, extension = String.Empty;
                switch (expopt.ExpType)
                {
                    case EnumExpType.Xml:
                        filter = "Xml-File (*.xml)|*.xml";
                        extension = "xml";
                        expstr = Export2Xml(dt, expopt, identifier);
                        break;
                    case EnumExpType.Csv:
                        filter = "Comma-Seperated-Value (*.csv)|*.csv";
                        extension = "csv";
                        expstr = Export2Seperated(dt, expopt);
                        break;
                    case EnumExpType.Tsv:
                        filter = "Tab-Seperated-Value (*.tsv)|*.tsv";
                        extension = "tsv";
                        expstr = Export2Seperated(dt, expopt);
                        break;
                    case EnumExpType.Semicolon:
                        filter = "Semicolon-Seperated-Value (*.csv)|*.csv";
                        extension = "csv";
                        expstr = Export2Seperated(dt, expopt);
                        break;
                    case EnumExpType.InsertStm:
                        filter = "Sql (*.sql)|*.sql";
                        extension = "sql";
                        expstr = Export2InsertSqlStm(dt, expopt, identifier);
                        break;
                    case EnumExpType.DelStm:
                        filter = "Sql (*.sql)|*.sql";
                        extension = "sql";
                        expstr = Export2DeleteSqlStm(dt, expopt, identifier);
                        break;
                    case EnumExpType.Html:
                        filter = "Html (*.html)|*.html";
                        extension = "html";
                        expstr = Export2Html(dt, identifier);
                        break;
                }
                if (expstr.Length > 0)
                {
                    if (GuiHelper.SaveFileDialog(extension, filter))
                    {
                        string path = GuiHelper.Fullpath;
                        try { File.WriteAllText(path, expstr); }
                        catch (System.Security.SecurityException sexc)
                        {   // err
                            MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileSave", new string[] { path, sexc.Message }),
                                        sexc, MessageHandler.EnumMsgType.Warning);
                        }
                        catch (IOException ioexc)
                        {   // err
                            MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileSave", new string[] { path, ioexc.Message }),
                                        ioexc, MessageHandler.EnumMsgType.Warning);
                        }
                        catch (UnauthorizedAccessException uaexc)
                        {   // err
                            MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileSave", new string[] { path, uaexc.Message }),
                                        uaexc, MessageHandler.EnumMsgType.Warning);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// get datagridviewcells (selected) as a sorted datatable
        /// </summary>
        /// <param name="dgvscColl">selected datagridviewcells</param>
        /// <returns>sorted (by rows) datatable</returns>
        private static DataTable GetDGVCellsInDataTable(DataGridViewSelectedCellCollection dgvscColl)
        {   // ok, a complete new start... first fill the rows, then work with them
            DataTable dt = new DataTable();
            dt.Locale = NyxMain.NyxCI;
            dt.Columns.Add("origDGVIdx", typeof(Int32));
            // collect cells into own datatable
            foreach (DataGridViewCell dgvc in dgvscColl)
            {
                string column = dgvc.DataGridView.Columns[dgvc.ColumnIndex].Name;
                // check if we got this columnindex
                if (!dt.Columns.Contains(column))
                    dt.Columns.Add(column, dgvc.ValueType);

                // check if we got the rowindex?
                int rowidx = dgvc.RowIndex;
                DataRow[] drSel = dt.Select("origDGVIdx = '" + rowidx + "'");
                if (drSel.Length > 0)
                    drSel[0][column] = dgvc.Value;
                else
                {
                    DataRow dr = dt.NewRow();
                    dr["origDGVIdx"] = rowidx;
                    dr[column] = dgvc.Value;
                    dt.Rows.Add(dr);
                    dt.AcceptChanges();
                }
            }
            dt.Columns.Remove("origDGVIdx");
            return dt;
        }
        /// <summary>
        /// get datagridview as datatable
        /// </summary>
        /// <param name="dgv">datagridview to convert</param>
        /// <returns>datatable</returns>
        private static DataTable GetDGVasDataTable(DataGridView dgv)
        {   // build datatable
            DataTable dt = new DataTable();
            dt.Locale = NyxMain.NyxCI;
            // collect cells into own datatable
            foreach (DataGridViewRow dgvr in dgv.Rows)
            {
                DataRow dr = dt.NewRow();
                foreach (DataGridViewCell dgvc in dgvr.Cells)
                {
                    string column = dgvc.DataGridView.Columns[dgvc.ColumnIndex].Name;
                    // check if we got this columnindex
                    if (!dt.Columns.Contains(column))
                        dt.Columns.Add(column, dgvc.ValueType);

                    // check if we got the rowindex?
                    dr[column] = dgvc.Value;
                }
                dt.Rows.Add(dr);
            }
            dt.AcceptChanges();
            return dt;
        }

        /// <summary>
        /// export to xml
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="expopt"></param>
        /// <param name="identifier"></param>
        /// <returns></returns>
        private static string Export2Xml(DataTable dt, StructExpOpt expopt, string identifier)
        {   // add initial xml header
            System.Text.StringBuilder sbExpStr = new System.Text.StringBuilder();
            sbExpStr.Append(String.Format(NyxMain.NyxCI, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                        "<{0} xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">\n", identifier));
            // convert to bool chars, for faster access inside the foreach
            bool lineBreakRow = false, lineBreakCol = false;
            if (expopt.LineBreak == EnumLineBreak.Column)
                lineBreakCol = true;
            else
                lineBreakRow = true;
            // add rows
            System.Text.StringBuilder sbRow = new System.Text.StringBuilder();
            foreach (DataRow dr in dt.Rows)
            {
                sbRow.Append("<row>");
                // linebreak
                if (lineBreakCol)
                    sbRow.Append("\n");
                // parse columns
                foreach (DataColumn dc in dt.Columns)
                {   // if linebreakcolumns
                    if (lineBreakCol)
                        sbRow.Append("   ");
                    // column width
                    string value = SetStrWidth(dr[dc].ToString(), expopt.ColumnWidth);
                    // encapsulate
                    value = EncapsStr(value, expopt.EncapsStr);
                    // add column
                    if (value.Length > 0)
                        sbRow.Append(String.Format(NyxMain.NyxCI, "<{0}>{1}</{0}>",dc.ColumnName,value));
                    else
                        sbRow.Append(String.Format(NyxMain.NyxCI, "<{0} />", dc.ColumnName));
                    // linebreak
                    if (lineBreakCol)
                        sbRow.Append("\n");
                }
                // close xml header
                sbRow.Append("</row>");
                // linebreak
                if (lineBreakRow || lineBreakCol)
                    sbRow.Append("\n");

                sbExpStr.Append(sbRow.ToString());
            }
            // finalize
            return String.Format(NyxMain.NyxCI, "{0}</{1}>", sbExpStr.ToString(), identifier); 
        }
        /// <summary>
        /// export to seperated values (csv/tsv/...)
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="expopt"></param>
        /// <returns></returns>
        private static string Export2Seperated(DataTable dt, StructExpOpt expopt)
        {   // init
            string expstr = String.Empty;
            // get csv header and seperator
            string seperator = GetSeperator(expopt.ExpType);
            // parse the columns to build the header
            foreach (DataColumn dc in dt.Columns)
            {   
                if (expstr.Length == 0)
                    expstr = EncapsStr(SetStrWidth(dc.ColumnName, expopt.ColumnWidth), expopt.EncapsStr);
                else
                    expstr = String.Format(NyxMain.NyxCI, "{0}{1}{2}", expstr, seperator, EncapsStr(SetStrWidth(dc.ColumnName, expopt.ColumnWidth), expopt.EncapsStr));
            }
            // finalize
            expstr += "\n";
            // add rows
            foreach (DataRow dr in dt.Rows)
            {   // init
                string rowstr = String.Empty;
                // parse rows
                foreach (DataColumn dc in dt.Columns)
                {   // column width
                    string value = SetStrWidth(dr[dc].ToString(), expopt.ColumnWidth);
                    // encapsulate
                    value = EncapsStr(value, expopt.EncapsStr);
                    // add column
                    if (rowstr.Length > 0)
                        rowstr = String.Format(NyxMain.NyxCI, "{0}{1}{2}", rowstr, seperator, value);
                    else
                        rowstr = String.Format(NyxMain.NyxCI, "{0}", value);
                }
                // linebreak
                expstr = String.Format(NyxMain.NyxCI, "{0}{1}\n", expstr, rowstr);
            }
            // return
            return expstr;
        }
        /// <summary>
        /// export to insert sql statement
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="expopt"></param>
        /// <param name="identifier"></param>
        /// <returns></returns>
        private static string Export2InsertSqlStm(DataTable dt, StructExpOpt expopt, string identifier)
        {
            string expstr = String.Empty, columns = String.Empty;
            // build columns
            foreach (DataColumn dc in dt.Columns)
            {   
                if (columns.Length == 0)
                    columns = dc.ColumnName.ToString();
                else
                    columns = String.Format(NyxMain.NyxCI, "{0},{1}", columns, dc.ColumnName.ToString());
            }
            foreach (DataRow dr in dt.Rows)
            {   // init vars
                string insrstm = String.Empty;

                foreach (DataColumn dc in dt.Columns)
                {   // build column value
                    string value = string.Empty;
                    bool vl = true;
                    bool en = false;
                    if (dc.AllowDBNull && dr[dc].GetType() == typeof(DBNull))
                    {
                        value = "NULL";
                        vl = false;
                        en = false;
                    }
                    if (vl)
                    {
                        if (dc.DataType == typeof(DateTime))
                        {
                            value = Convert.ToDateTime(dr[dc]).ToString("d-MMM-yyyy");
                            en = true;
                        }
                        else if (dc.DataType != typeof(string) && dc.DataType != typeof(char))
                        {
                            value = ((dc.DataType != typeof(bool)) ? dr[dc].ToString() : (Convert.ToBoolean(dr[dc]) ? "1" : "0"));
                        }
                        else
                        {
                            value = dr[dc].ToString();
                            en = true;
                        }
                    }
                    if (en)
                    {
                        value = ((expopt.EncapsStr.Length <= 0) ? EncapsStr(value, "'") : EncapsStr(value, expopt.EncapsStr));
                    }

                    if (insrstm.Length == 0)
                        insrstm = String.Format(NyxMain.NyxCI, "INSERT INTO {0} ({1}) VALUES ({2}", identifier, columns, value);
                    else
                        insrstm = String.Format(NyxMain.NyxCI, "{0},{1}", insrstm, value);
                }
                // finalize in expstr
                expstr = String.Format(NyxMain.NyxCI, "{0}{1});\n", expstr, insrstm);
            }
            return expstr;
        }
        /// <summary>
        /// export to delete sql statement
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="expopt"></param>
        /// <param name="identifier"></param>
        /// <returns></returns>
        private static string Export2DeleteSqlStm(DataTable dt, StructExpOpt expopt, string identifier)
        {
            string expstr = String.Empty;
            foreach (DataRow dr in dt.Rows)
            {   // init vars
                string delstm = String.Empty;

                foreach (DataColumn dc in dt.Columns)
                {   // build column value
                    string value = dr[dc].ToString() ;
                    if (expopt.EncapsStr.Length > 0)
                        value = EncapsStr(value, expopt.EncapsStr);
                    // build where clause
                    if (delstm.Length == 0)
                        delstm = String.Format(NyxMain.NyxCI, "{0} = {1}", dc.ColumnName, value);
                    else
                        delstm = String.Format(NyxMain.NyxCI, "{0} AND {1} = {2}", delstm, dc.ColumnName, value);
                        
                }
                expstr = String.Format(NyxMain.NyxCI, "{0}DELETE FROM {1} WHERE {2};\n", expstr, identifier, delstm);
            }
            return expstr;
        }
        /// <summary>
        /// export to html
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="identifier"></param>
        /// <returns></returns>
        private static string Export2Html(DataTable dt, string identifier)
        {   // parse the columns to build the tableheader
            string header = String.Empty;
            foreach (DataColumn dc in dt.Columns)
            {
                if (header.Length == 0)
                    header = String.Format(NyxMain.NyxCI, "<tr><th>{0}</th>", dc.ColumnName);
                else
                    header = String.Format(NyxMain.NyxCI, "{0}<th>{1}</th>", header, dc.ColumnName);
            }
            // finalize
            header += "</tr>\n";
            // add rows
            string rows = String.Empty;
            bool color = false;
            foreach (DataRow dr in dt.Rows)
            {
                string rowstr = String.Empty;
                if (!color)
                {
                    rowstr = "<tr>";
                    color = true;
                }
                else
                {
                    rowstr = "<tr class=\"crow\">";
                    color = false;
                }
                // parse rows
                foreach (DataColumn dc in dt.Columns)
                {   // add column
                    rowstr = String.Format(NyxMain.NyxCI, "{0}<td>{1}</td>", rowstr, dr[dc].ToString());
                }
                // add to rows
                rows = String.Format(NyxMain.NyxCI, "{0}{1}</tr>\n", rows, rowstr);
            }
            // return
            return FinalizeHtmlExp(identifier, header, rows);
        }

        /// <summary>
        /// retrieve the selected seperator
        /// </summary>
        /// <param name="exptype">export type</param>
        /// <returns>seperator as string</returns>
        private static string GetSeperator(EnumExpType exptype)
        {
            switch (exptype)
            {
                default:
                case EnumExpType.Csv:
                    return ",";
                case EnumExpType.Tsv:
                    return "\t";
                case EnumExpType.Semicolon:
                    return ";";
            }
        }
        /// <summary>
        /// set string width
        /// </summary>
        /// <param name="value">string</param>
        /// <param name="width">width</param>
        /// <returns>the string sized to width (filled with blanks)</returns>
        private static string SetStrWidth(string value, int width)
        {   // colwidth greater than colname
            if (width > value.Length)
            {   // calc
                int spaces = width - value.Length;
                for (int i = 0; i < spaces; i++)
                    value += " ";
            }
            return value;
        }
        /// <summary>
        /// encapsulate the string
        /// </summary>
        /// <param name="value">string</param>
        /// <param name="encapsulateStr">encapsulating string</param>
        /// <returns>string encapsulated by encapsulatestr</returns>
        private static string EncapsStr(string value, string encapsulateStr)
        {
            if (encapsulateStr.Length > 0)
            {
                if (encapsulateStr.Equals("'"))
                {
                    string replaceStr = encapsulateStr + encapsulateStr;
                    value = value.Replace(encapsulateStr, replaceStr).Trim();
                }
                else
                {
                    value = value.Replace(encapsulateStr, "\\" + encapsulateStr).Trim();
                }
                return string.Format(NyxMain.NyxCI, "{0}{1}{0}", encapsulateStr, value);
            }
            return value;
        }

        private static string FinalizeHtmlExp(string identifier, string header, string rows)
        {
            return String.Format(NyxMain.NyxCI, "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">\n" +
                                "<html>\n" +
                                "<head>\n" +
                                "  <title>Nyx export - {0}</title>\n" +
                                "  <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n" +
                                "  <style type=\"text/css\">\n" +
                                "  <!--\n" +
                                "  body {{ font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; }} \n" +
                                "  table {{ font-size: 10px; }} \n" +
                                "  th {{ font-weight: bold; }} \n" +
                                "  .crow {{ background-color: #E9E9E9; }} \n" +
                                "  -->\n" +
                                "  </style>\n" +
                                "</head>\n" +
                                "<body>\n" +
                                "  <fieldset style=\"width:75%;margin:0 auto;\">\n" +
                                "    <legend>{0}</legend>\n" +
                                "    <table style=\"width:100%\">\n" +
                                "            {1}\n" +
                                "            {2}\n" +
                                "    </table>\n" +
                                "  </fieldset>\n" +
                                "</body>\n" +
                                "</html>", identifier, header, rows);
        }
    }
}
