/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using System.Collections;
using System.Windows.Forms;
using System.Threading;
using Nyx.Classes;
using Nyx.AppDialogs;
using NyxSettings;

namespace Nyx
{   
    public partial class SqlTab : UserControl
    {   /// <remarks> event which gets fired when a table was created and the dialog is closed </remarks>
        public event EventHandler<DBContentChangedEventArgs> DBContentChangedEvent;
        // delegates for threading
        private delegate void ThrdCom();
        private delegate void ThrdParamCom(object param);
        // local vars
        private DBHelper dbh;
        
        private System.Threading.Thread doWork;
        private DataSet dsResult = new DataSet();
        /// <remarks> Public access method for the result dataset </remarks>
        public DataSet DSResult { get { return dsResult; } set { dsResult = value; } }
        /// <remarks> Public access method for the result datagrid </remarks>
        public DataGridView DGResult{ get { return _dgSql; } set { this._dgSql = value; } }

        private DatabaseAction sqlAction;
        /// autocomplete vars
        private NyxAutoComplete.ACListBox _lbAutoComplete;
        private bool wordFound, fieldsInListbox = true;
        
        /// <summary>
        /// SqlMultiTabPage Usercontrol
        /// </summary>
        public SqlTab(DBHelper initDbh)
        {
            InitializeComponent();
            // init 
            dsResult.Locale = NyxMain.NyxCI;
            dbh = initDbh;
            ResetGui();
            ApplySettings(NyxMain.UASett);
            // init autocompletition
            if (NyxMain.UASett.AutoCompletition)
            {
                this._lbAutoComplete = new NyxAutoComplete.ACListBox();
                this._lbAutoComplete.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
                this._lbAutoComplete.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
                this._lbAutoComplete.ImageList = this._ilAutoComplete;
                this._lbAutoComplete.Location = new System.Drawing.Point(136, 288);
                this._lbAutoComplete.Name = "_lbAutoComplete";
                this._lbAutoComplete.Size = new System.Drawing.Size(208, 54);
                this._lbAutoComplete.TabIndex = 3;
                this._lbAutoComplete.Visible = false;
                this._lbAutoComplete.KeyDown += new System.Windows.Forms.KeyEventHandler(this._lbAutoComplete_KeyDown);
                this._lbAutoComplete.DoubleClick += new System.EventHandler(this._lbAutoComplete_DoubleClick);
                this._lbAutoComplete.SelectedIndexChanged += new System.EventHandler(this._lbAutoComplete_SelectedIndexChanged);
                this.Controls.Add(this._lbAutoComplete);
            }
        }
        /// <summary>
        /// Replaces the sqlqueryeditor text with the given query
        /// </summary>
        /// <param name="query">query</param>
        public void ReplaceSqlRtbTxt(string query)
        {
            this._rtbSqlQry.Text = query;
        }
        /// <summary>
        /// Adds the selected SqlBuilderelement to the query editor
        /// </summary>
        /// <param name="msg">sqlbuilderelement</param>
        public void AddSqlBldr2SqlRtb(string msg)
        {   // focus, replace text, set selectionlength to avoid exception
            this._rtbSqlQry.Focus();
            this._rtbSqlQry.SelectedText = msg;
            this._rtbSqlQry.SelectionLength = 0;
        }
        /// <summary>
        /// Add string msg to current error log
        /// </summary>
        /// <param name="msg">message</param>
        public void AddErrorLogText(string msg)
        {
            this._rtbErrorDsp.Text = msg;
        }
        /// <summary>
        /// ResetGUI
        /// </summary>
        public void ResetGui()
        {   // dataset
            if (dsResult != null)
                dsResult.Reset();
            // aclistbox
            if (this._lbAutoComplete != null)
                this._lbAutoComplete.Items.Clear();
            // gui
            this._tsSql_cbLimit.Text = NyxMain.UASett.SqlDefaultQryLimit.ToString(NyxMain.NyxCI);
            this._tsSql_cbQryIdent.SelectedIndex = 0;
            this._dgSql.DataSource = null;
            if (NyxMain.ConProfile.ProfileDBType == DBType.Odbc)
            {   // sql toolstrip
                this._tsSql_SqlBldr.Enabled = false;
                this._tsSql_lblLimit.Enabled = false;
                this._tsSql_cbLimit.Enabled = false;
            }
            else
            {
                // sql toolsetip
                this._tsSql_SqlBldr.Enabled = true;
                this._tsSql_lblLimit.Enabled = true;
                this._tsSql_cbLimit.Enabled = true;
            }
        }
        /// <summary>
        /// Apply user-application settings
        /// </summary>
        /// <param name="uaSett">UserAppSettings</param>
        public void ApplySettings(UserAppSettings uaSett)
        {
            if (uaSett == null)
                return;
            // sql query limit
            this._tsSql_cbLimit.Text = uaSett.SqlDefaultQryLimit.ToString(NyxMain.NyxCI);
            // editor font
            System.Drawing.Font ft;
            if (uaSett.SqlEditorFont.Length > 0)
            {
                GuiHelper.GetFont(uaSett.SqlEditorFont, out ft);
                this._rtbSqlQry.Font = ft;
            }
            // logging font
            if (uaSett.LogFont.Length > 0)
            {
                GuiHelper.GetFont(uaSett.LogFont, out ft);
                this._rtbErrorDsp.Font = ft;
            }
            // format datagridview
            this._dgSql = GuiHelper.DataGridViewFormat(uaSett, this._dgSql);
        }
        /// <summary>
        /// Re-Init DbHelper and DbManagement. Is getting called, when the dbconnection changes
        /// </summary>
        public void InitClasses(DBHelper dbhInit)
        {
            if (dbhInit != null)
            {
                // empty
                this.dbh = null;
                // re-set
                this.dbh = dbhInit;
            }
        }
        /// <summary>
        /// Clear the sql query inside this sql tabpage, public cause of hotkey from nyxmain
        /// </summary>
        public void ClearSqlQuery()
        {
            this._rtbSqlQry.Text = String.Empty;
            // clearing resources
            dsResult.Reset();
            // disabling contextmenue
            this._cmSqlDta.Enabled = false;
        }
        /// <summary>
        /// Save sql query from the current sql tabpage, public cause of hotkey from nyxmain
        /// </summary>
        public void SaveSql()
        {   // got text to save?
            if (this._rtbSqlQry.Text.Length > 0)
            {
                if (GuiHelper.SaveFileDialog("sql", "SQL-Files (*.sql)|*.sql|Text-Files (*.txt)|*.txt|All Files (*.*)|*.*"))
                {
                    string path = GuiHelper.Fullpath;
                    try { this._rtbSqlQry.SaveFile(path, RichTextBoxStreamType.PlainText); }
                    catch (System.IO.IOException exc)
                    {   // err
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileSave", new string[] { path, exc.Message }),
                                    exc, MessageHandler.EnumMsgType.Warning);
                    }
                }
            }
        }
        /// <summary>
        /// Load sql from file into query edit box, public cause of hotkey from nyxmain
        /// </summary>
        public void LoadSql()
        {
            if(GuiHelper.OpenFileDialog("SQL-Files (*.sql)|*.sql|Text-Files (*.txt)|*.txt|All Files (*.*)|*.*"))
            {   // read-in of file content
                string fullpath = GuiHelper.Fullpath;
                try
                {   // checkup file size
                    bool import = false;
                    System.IO.FileInfo fi = new System.IO.FileInfo(fullpath);
                    // check if we shall import, dependant on filesize
                    if (fi.Length < 50000)
                        import = true;
                    else
                    {
                        StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("FileSizeWarning", new string[] { "50 kB" }),
                            MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning, false);
                        if (strRD.DRes == NyxDialogResult.Yes)
                            import = true;
                    }
                    // filesize ok?
                    if (import)
                    {
                        using (System.IO.StreamReader sr = new System.IO.StreamReader(fullpath))
                        {
                            string line = null;
                            this._rtbSqlQry.Text = String.Empty; ;
                            while ((line = sr.ReadLine()) != null)
                            {
                                if (this._rtbSqlQry.Text.Length == 0)
                                    this._rtbSqlQry.Text = line;
                                else
                                    this._rtbSqlQry.Text = String.Format(NyxMain.NyxCI, "{0}\n{1}", this._rtbSqlQry.Text, line);
                            }
                        }
                    }
                }
                catch (System.IO.IOException ioexc)
                {   // err
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileRead", new string[] { fullpath, ioexc.Message }),
                                ioexc, MessageHandler.EnumMsgType.Warning);
                }
                catch (OutOfMemoryException oomexc)
                {   // err
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrFileRead", new string[] { fullpath, oomexc.Message }),
                                oomexc, MessageHandler.EnumMsgType.Warning);
                }
            }
        }
        /// <summary>
        /// Put datagrid into edit mode, public cause of hotkey from nyxmain
        /// </summary>
        public void EditDataGrid()
        {   // check if we disable the datagrid readonly property
            if (this._dgSql.ReadOnly)
            {
                this._dgSql.ReadOnly = false;
                this._dgSql.AllowUserToAddRows = true;
                this._dgSql.AllowUserToDeleteRows = true;
                this._dgSql.AllowUserToOrderColumns = true;
                // set buttons
                SwitchEditButtons(false, true);
            }
            else
            {
                this._dgSql.ReadOnly = true;
                this._dgSql.AllowUserToAddRows = false;
                this._dgSql.AllowUserToDeleteRows = false;
                this._dgSql.AllowUserToOrderColumns = false;
                SwitchEditButtons(true, false);
            }
        }
        /// <summary>
        /// Update listbox content
        /// </summary>
        public void ACUpdateListBox()
        {
            NyxAutoComplete.ACListBox lb = NyxMain.LBAutoComplete;
            if (lb.Items.Count != this._lbAutoComplete.Items.Count)
            {   // copy items
                foreach (NyxAutoComplete.ACListBoxItem lbi in lb.Items)
                    this._lbAutoComplete.Items.Add(lbi);
                if (this._lbAutoComplete.Items.Count > 0)
                {   // set and show
                    this._lbAutoComplete.SelectedIndex = 0;
                    this._lbAutoComplete.BringToFront();
                }
            }
        }
        /// <summary> Clear the SqlMultiTab querytext and dataset </summary>
        public void Clear()
        {   // clean up sqlquery-text and retrieved data
            this._rtbSqlQry.Text = String.Empty;
            dsResult.Reset();
            this._cmSqlDta.Enabled = false;

        }
        // thread controls
        private void DbThreadInit(object oThrStrVars)
        {
            doWork = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(DbThreadStart));
            doWork.IsBackground = true;
            doWork.Start(oThrStrVars);
        }
        private void DbThreadStart(object threadParam)
        {
            try
            {   // init start and return structs
                DBThreadStartParam strThrdStart = (DBThreadStartParam)threadParam;
                DBThreadReturnParam strThrdReturn = new DBThreadReturnParam();
                // set return action
                strThrdReturn.ThrAction = strThrdStart.ThrAction;
                strThrdStart.Dbh = this.dbh;
                // switch action
                switch (strThrdStart.ThrAction)
                {   // 'special' case
                    case DatabaseAction.UpdateDS:
                        if (!dbh.Update(strThrdStart.Param1, dsResult))
                            strThrdReturn.DBException = dbh.ErrExc;
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(UpdateDGridDone), strThrdReturn);
                        break;
                    // actions defined through threadhelper
                    case DatabaseAction.ExecNonQry:
                        ThreadHelper.ExecNonQuery(strThrdStart, ref strThrdReturn);
                        this.BeginInvoke(new DBManagement.DbmThrdEndPDelegate(SqlQryQryDone), strThrdReturn);
                        break;
                    case DatabaseAction.GetQuery:
                    case DatabaseAction.SelectQuery:
                        ThreadHelper.ExecSelQuery(strThrdStart, ref strThrdReturn);
                        dsResult = (DataSet)strThrdReturn.Data1;
                        this.BeginInvoke(new ThrdCom(SqlQryBindDataSet));
                        this.BeginInvoke(new DBManagement.DbmThrdEndPDelegate(SqlQryQryDone), strThrdReturn);
                        break;
                    default: // exit due to fatal error
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ThreadIdentErr", new string[] { strThrdStart.ThrAction.ToString() }),
                            MessageHandler.EnumMsgType.Error);
                        break;
                }
            }
            catch (System.Threading.ThreadAbortException texc)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrThreadBreak", new string[] { texc.Message }),
                    texc, MessageHandler.EnumMsgType.Warning);
                // invoke reset thread
                this.Invoke(new ThrdCom(DbThreadAborted));
            }
        }
        private void DbThreadAborted()
        {   // reset gui
            ((NyxMain)this.ParentForm).CtrlAppDisplay(String.Empty);
            // set errortext
            this._rtbErrorDsp.Text = GuiHelper.GetResourceMsg("OperationCanceled");
            this._rtbErrorDsp.ForeColor = System.Drawing.Color.Red;
        }
        // sql contols
        private void SqlMultiTab_Enter(object sender, EventArgs e)
        {   // focus
            this._rtbSqlQry.Focus();
        }
        private int SqlQryGetLimit()
        {
            if (this._tsSql_cbLimit.Text == "no Limit")
                return 0;
            else
            {
                int iconv;
                if (Int32.TryParse(this._tsSql_cbLimit.Text, out iconv))
                    return iconv;
                else
                    return 0;
            }
        }
        /// <summary>
        /// Execute query, public cause of hotkey from nyxmain
        /// </summary>
        public void ExecuteSql()
        {
            if (this._rtbSqlQry.Text.Length > 3)
            {
                // disabling result-related
                this._cmSqlDta.Enabled = false;
                // exec query
                SqlQryExecQry(this._rtbSqlQry.Text);
            }
        }
        /// <summary>
        /// Execute only selected string as query, public cause of hotkey from nyxmain
        /// </summary>
        public void ExecuteSelectedSql()
        {
            if (this._rtbSqlQry.SelectedText.Length > 3)
            {
                // disabling result-related
                this._cmSqlDta.Enabled = false;
                // exec query
                SqlQryExecQry(this._rtbSqlQry.SelectedText);
            }
        }
        private void SqlQryExecQry(string qryStr)
        {	// resetting dberror checkvar and init qryIdent var
            if (dsResult != null)
                dsResult.Reset();
            // manage datagrid propertys
            this._dgSql.DataSource = null;
            if (!this._dgSql.ReadOnly)
                this._dgSql.ReadOnly = true;
            // reset edit/apply buttons
            SwitchEditButtons(false, false);
            /* pre-checks
             */
            if (this._tsSql_cbQryIdent.SelectedIndex == 1)
                sqlAction = DatabaseAction.SelectQuery;
            else if (this._tsSql_cbQryIdent.SelectedIndex == 2)
                sqlAction = DatabaseAction.ExecNonQry;
            else
            {
                string trimqry = qryStr.Trim();
                if (trimqry.Length > 6 && String.Compare(trimqry.Substring(0, 6), "SELECT", true, NyxMain.NyxCI) == 0)
                    sqlAction = DatabaseAction.SelectQuery;

                else if (trimqry.Length > 4 && (String.Compare(trimqry.Substring(0, 4), "SHOW", true, NyxMain.NyxCI) == 0
                        || String.Compare(trimqry.Substring(0, 4), "EXEC", true, NyxMain.NyxCI) == 0))
                    sqlAction = DatabaseAction.GetQuery;

                else if (trimqry.Length > 7 && String.Compare(trimqry.Substring(0, 7), "EXPLAIN", true, NyxMain.NyxCI) == 0)
                    sqlAction = DatabaseAction.SelectQuery;

                else if (trimqry.Length > 8 && String.Compare(trimqry.Substring(0, 8), "DESCRIBE", true, NyxMain.NyxCI) == 0)
                    sqlAction = DatabaseAction.GetQuery;

                else
                    sqlAction = DatabaseAction.ExecNonQry;
            }


            if (sqlAction == DatabaseAction.SelectQuery || sqlAction == DatabaseAction.GetQuery)
            {   // pre-set some vars and clear datagridview
                ((NyxMain)this.ParentForm).CtrlAppDisplay(GuiHelper.GetResourceMsg("ST_QueryExec"));
                // re-set the sqlaction to select. every query which runs inside here is a select in truth...
                this._rtbErrorDsp.Text = String.Empty;
                // setting thread parameters
                DBThreadStartParam strThrStart = new DBThreadStartParam();
                if (sqlAction == DatabaseAction.SelectQuery)
                    strThrStart.Param1 = dbh.ParseSqlQuery(qryStr, SqlQryGetLimit(), true);
                else
                    strThrStart.Param1 = dbh.ParseSqlQuery(qryStr, 0, true);
                strThrStart.ThrAction = sqlAction;
                // start thread...
                DbThreadInit(strThrStart);
            }
            else
            {   // show window? - 'really submit query?'
                if (NyxMain.UASett.WindowExecuteQuery)
                {
                    StructRetDialog ret = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("SqlSubmitQueryQst"),
                                                                MessageBoxButtons.YesNo, MessageBoxIcon.Question, true);
                    if (!NyxMain.UASett.WindowExecuteQuery.Equals(ret.ShowAgain))
                    {
                        NyxMain.UASett.WindowExecuteQuery = ret.ShowAgain;
                        if (!Settings.SerializeUserSettings(NyxMain.UASett, Settings.GetFolder(SettingsType.AppSettings)))
                            MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("UserSettingsSaveErr",
                                new string[] { Settings.ErrExc.Message.ToString() }), 
                                Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
                    }
                    // check the dialogresult and exit if needed
                    if (ret.DRes == NyxDialogResult.Yes)
                    {   // set progress
                        ((NyxMain)this.ParentForm).CtrlAppDisplay("Query: '" + sqlAction.ToString() + "' sent...");
                        // setting threadparameters
                        DBThreadStartParam strThrStart = new DBThreadStartParam();
                        strThrStart.ThrAction = sqlAction;
                        strThrStart.Param1 = qryStr;
                        // starting thread
                        DbThreadInit(strThrStart);
                    }
                }
                else
                {   // set progress
                    ((NyxMain)this.ParentForm).CtrlAppDisplay("Query: '" + sqlAction.ToString() + "' sent...");
                    // setting threadparameters
                    DBThreadStartParam strThrStart = new DBThreadStartParam();
                    strThrStart.ThrAction = sqlAction;
                    strThrStart.Param1 = qryStr;
                    // starting thread
                    DbThreadInit(strThrStart);
                }
            }
        }
        private void SqlQryQryDone(DBThreadReturnParam strThrReturn)
        {   // temp var, for checking what to refresh after a drop|create on table|view|procedure was exectued (see initsqlbldr)
            bool[] getObjectsOptions = new bool[3] { false, false, false };
            // error checkup
            if (strThrReturn.DBException == null)
            {   // refresh historybombobox
                ((NyxMain)this.ParentForm).AddHistory(this._rtbSqlQry.Text, SqlQryGetLimit());
                // set result display color
                this._rtbErrorDsp.ForeColor = System.Drawing.Color.Black;
                // settings dependent on the querytype
                if (sqlAction == DatabaseAction.SelectQuery || sqlAction == DatabaseAction.GetQuery)
                {
                    if (dsResult != null)
                    {   // enable contextmenue & edit-buttons ? 
                        this._cmSqlDta.Enabled = true;
                        SwitchEditButtons(true, false);
                        // check save result
                        if (dsResult.Tables[0].Rows.Count > 0)
                        {
                            this._cmSqlDtaSv.Enabled = true;
                            this._cmSqlDtaCp.Enabled = true;
                        }
                        else
                        {
                            this._cmSqlDtaSv.Enabled = false;
                            this._cmSqlDtaCp.Enabled = false;
                        }
                        // setting affected rows 
                        int limit = SqlQryGetLimit();
                        if (limit > 0 && dsResult.Tables[0].Rows.Count == limit)
                            this._rtbErrorDsp.Text = "Rows selected: " + dsResult.Tables[0].Rows.Count + " (limited to " + limit + ")" +
                                  "\nColumns selected: " + dsResult.Tables[0].Columns.Count;
                        else
                            this._rtbErrorDsp.Text = "Rows selected: " + dsResult.Tables[0].Rows.Count +
                              "\nColumns selected: " + dsResult.Tables[0].Columns.Count;
                    }
                }
                else
                {   // doing result display and disabling save result
                    this._rtbErrorDsp.Text = "affected Rows: " + strThrReturn.AffRows;
                    this._cmSqlDtaSv.Enabled = false;
                    this._cmSqlDtaCp.Enabled = false;
                    string qry = strThrReturn.Data2.ToString();
                    /* now we check what had been done
                     */
                    System.Text.RegularExpressions.Regex rx;
                    rx = new System.Text.RegularExpressions.Regex(@"(\s+)?([DROP|CREATE|ALTER]\s+TABLE)\s+(\w+)",
                            System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (rx.IsMatch(qry))
                    {   // refresh tables
                        getObjectsOptions[0] = true;
                    }

                    rx = new System.Text.RegularExpressions.Regex(@"(\s+)?([DROP|CREATE|ALTER]\s+VIEW)\s+(\w+)",
                            System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (rx.IsMatch(qry))
                    {   // refresh views
                        getObjectsOptions[1] = true;
                    }

                    rx = new System.Text.RegularExpressions.Regex(@"(\s+)?([DROP|CREATE|ALTER]\s+[PROCEDURE|FUNCTION])\s+(\w+)",
                           System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                    if (rx.IsMatch(qry))
                    {   // refresh procedures
                        getObjectsOptions[2] = true;
                    }
                }
                // get action
                string dspqry = (string)strThrReturn.Data2;
                if(dspqry.Length > 50)
                    ((NyxMain)this.ParentForm).SqlSetTab(dspqry.Substring(0, 50), dspqry);
                else
                    ((NyxMain)this.ParentForm).SqlSetTab(dspqry, dspqry);
            }
            else
            {   // set error text
                this._rtbErrorDsp.ForeColor = System.Drawing.Color.Red;
                this._rtbErrorDsp.Text = strThrReturn.DBException.Message.ToString();
            }
            // selecting rtb_sqlquery again and reset gui
            ((NyxMain)this.ParentForm).CtrlAppDisplay(String.Empty);
            this._rtbSqlQry.Focus();
            // check if we shall refresh
            if (getObjectsOptions[0] || getObjectsOptions[1] || getObjectsOptions[2])
            {   // check if we shall show?
                if (NyxMain.UASett.WindowDBContentChanged)
                {
                    StructRetDialog ret = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("DBContentChanged"),
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question, true);

                    if (!NyxMain.UASett.WindowDBContentChanged.Equals(ret.ShowAgain))
                    {
                        NyxMain.UASett.WindowDBContentChanged = ret.ShowAgain;
                        if (!Settings.SerializeUserSettings(NyxMain.UASett, Settings.GetFolder(SettingsType.AppSettings)))
                            MessageHandler.ShowDialog(
                                GuiHelper.GetResourceMsg("UserSettingsSaveErr", new string[] { Settings.ErrExc.Message.ToString() }),
                                Settings.ErrExc, 
                                MessageHandler.EnumMsgType.Warning);
                    }
                    // check the dialogresult and fire event if needed
                    if (ret.DRes == NyxDialogResult.Yes)
                    {   // fire event
                        DBContentChangedEventArgs e = new DBContentChangedEventArgs();
                        e.DBContent = DatabaseContent.TablesViewsProcedures;

                        if (DBContentChangedEvent != null)
                            DBContentChangedEvent(this, e);
                    }
                }
                else
                {   // fire event
                    DBContentChangedEventArgs e = new DBContentChangedEventArgs();
                    e.DBContent = DatabaseContent.TablesViewsProcedures;

                    if (DBContentChangedEvent != null)
                        DBContentChangedEvent(this, e);
                }
            }
        }
        private void SqlQryBindDataSet()
        {
            if (dsResult != null && dsResult.Tables.Count == 1)
                this._dgSql.DataSource = dsResult.Tables[0];
        }
        // gui elements
        private void _tsSql_NewTab_Click(object sender, EventArgs e)
        {
            ((NyxMain)this.ParentForm).SqlCreateNewTab(true);
        }
        private void _tsSql_CloseTab_Click(object sender, EventArgs e)
        {
            ((NyxMain)this.ParentForm).SqlCloseTab();
        }
        private void _tsSql_SqlBldr_Click(object sender, EventArgs e)
        {
            ((NyxMain)this.ParentForm).SqlBldrActivate();
        }
        private void _tsSql_Clear_Click(object sender, System.EventArgs e)
        {
            ClearSqlQuery();
        }
        private void _tsSql_Exec_Click(object sender, EventArgs e)
        {
            ExecuteSql();
        }
        private void _tsSql_ExecSel_Click(object sender, EventArgs e)
        {
            ExecuteSelectedSql();
        }
        private void _tsSql_LoadSql_Click(object sender, EventArgs e)
        {
            LoadSql();
        }
        private void _tsSql_SaveSql_Click(object sender, EventArgs e)
        {
            SaveSql();
        }
        private void _cmSql_DelRow_Click(object sender, EventArgs e)
        {

        }
        private void _cmSqlQry_CBCopy_Click(object sender, EventArgs e)
        {   // adding to clipboard
            if (this._rtbSqlQry.SelectedText.Length > 0)
                Clipboard.SetText(this._rtbSqlQry.SelectedText);
        }
        private void _cmSqlQry_CBPaste_Click(object sender, EventArgs e)
        {   // adding text to textbox
            if (Clipboard.ContainsText())
            {   // have we got a selection?
                if (this._rtbSqlQry.SelectedText.Length > 0)
                    this._rtbSqlQry.SelectedText = Clipboard.GetText();
                else
                    this._rtbSqlQry.Text = String.Format(NyxMain.NyxCI, "{0}{1}", this._rtbSqlQry.Text, Clipboard.GetText());
            }
        }
        private void _cmSqlQry_CBCut_Click(object sender, EventArgs e)
        {
            if (this._rtbSqlQry.SelectedText.Length > 0)
            {   // adding to clipboard, afterwards deleting selection
                Clipboard.SetText(this._rtbSqlQry.SelectedText);
                this._rtbSqlQry.SelectedText = String.Empty;
            }
        }
        private void _cmSqlQry_LoadSql_Click(object sender, EventArgs e)
        {
            LoadSql();
        }
        private void _cmSqlQry_SaveSql_Click(object sender, EventArgs e)
        {
            SaveSql();
        }
        private void _cmSqlDtaCp_Field_Click(object sender, EventArgs e)
        {
            if (this._dgSql.CurrentCell != null)
                Clipboard.SetText(this._dgSql.CurrentCell.Value.ToString());
        }
        private void _cmSqlDtaCp_Cells_Click(object sender, EventArgs e) 
        { 
            ExportDataGridView.Cells2Clipboard(this._dgSql.SelectedCells, "SelectQuery");
        }
        private void _cmSqlDtaCp_Table_Click(object sender, EventArgs e)
        {
            if (dsResult.Tables.Count == 1)
                ExportDataGridView.Table2Clipboard(dsResult.Tables[0], "SelectQuery");
        }
        private void _cmSqlDtaSv_Cells_Click(object sender, EventArgs e)
        {
            ExportDataGridView.Cells2File(this._dgSql.SelectedCells, "SelectQuery");
        }
        private void _cmSqlDtaSv_Table_Click(object sender, EventArgs e)
        {
            if (dsResult.Tables.Count == 1)
                ExportDataGridView.Table2File(dsResult.Tables[0], "SelectQuery");
        }

        private void _dg_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {   // check the context
            if (e.Context == DataGridViewDataErrorContexts.Formatting)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrDataGridFormat", new string[] { e.Exception.Message.ToString() }),
                    e.Exception, MessageHandler.EnumMsgType.Error);
            }
            if (e.Context == DataGridViewDataErrorContexts.Commit)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrDataGridCommit", new string[] { e.Exception.Message.ToString() }),
                    e.Exception, MessageHandler.EnumMsgType.Error);
            }

            // check the exception
            if (e.Exception is ConstraintException)
            {
                DataGridView view = (DataGridView)sender;
                view.Rows[e.RowIndex].ErrorText = "Error: " + e.Context.ToString();
                view.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Error: " + e.Context.ToString();

                e.ThrowException = false;
            }
        }
        private void _rtbSqlQry_SelectionChanged(object sender, EventArgs e)
        {
            if (this._rtbSqlQry.SelectedText.Length > 0)
            {
                this._tsSql_ExecSel.Enabled = true;
                ((NyxMain)this.ParentForm).MenuSqlExecSel = true;
            }
            else
            {
                this._tsSql_ExecSel.Enabled = false;
                ((NyxMain)this.ParentForm).MenuSqlExecSel = false;
            }
        }
        private void _rtbSqlQry_KeyDown(object sender, KeyEventArgs e)
        {   // no autocompletition used
           if (!NyxMain.UASett.AutoCompletition || this._lbAutoComplete == null)
                return;

            string lastChar = String.Empty;
            // get the currently typed char
            string currentTypedChar = String.Empty;
            char val = (char)e.KeyValue;
            byte[] bt = System.Text.Encoding.Default.GetBytes(val.ToString());
            currentTypedChar = System.Text.Encoding.UTF8.GetString(bt);

            // get lastchar
            int selStart = this._rtbSqlQry.SelectionStart;
            if (selStart > 0)
                lastChar = this._rtbSqlQry.Text.Substring(selStart - 1, 1);

            // . pressed
            if (e.KeyData == Keys.OemPeriod)
            {   // check visibility
                if (!this._lbAutoComplete.Visible)
                {   // add items to the lsitbox
                    string lastWord = ACGetLastWord("");
                    if (lastWord.Length > 0)
                    {   // reset listview
                        this._lbAutoComplete.Items.Clear();
                        // check if the global hashtable contains the table
                        string[] fields = ((NyxMain)this.ParentForm).SqlBldrGetTableFields(lastWord);
                        if (fields.Length > 0)
                        {   
                            // fill aclistbox
                            for (int i = 0; i < fields.Length; i++)
                                this._lbAutoComplete.Items.Add(new NyxAutoComplete.ACListBoxItem(fields[i], 0));
                            this.fieldsInListbox = true;

                            if (this._lbAutoComplete.Items.Count > 0)
                            {   // sort, select first entry and show
                                //ACSortListBox();
                                this._lbAutoComplete.SelectedIndex = 0;
                                ACShowListBox();
                                this.wordFound = true;
                            }
                        }
                    }
                }
                else
                {   // hide
                    this._lbAutoComplete.Hide();
                }
            }
            // hide
            else if (e.KeyCode == Keys.Back)
            {   // hide the listbox popup
                if (lastChar == "." || lastChar == " ") // edited
                    this._lbAutoComplete.Hide();
            }
            // move up inside listbox, if visible
            else if (this._lbAutoComplete.Visible && e.KeyCode == Keys.Up)
            {
                this.wordFound = true;
                if (this._lbAutoComplete.SelectedIndex > 0)
                    this._lbAutoComplete.SelectedIndex--;
                // cancel further handling
                e.Handled = true;
            }
            // move down inside listbox, if visible
            else if (this._lbAutoComplete.Visible && e.KeyCode == Keys.Down)
            {
                this.wordFound = true;
                if (this._lbAutoComplete.SelectedIndex < this._lbAutoComplete.Items.Count - 1)
                    this._lbAutoComplete.SelectedIndex++;
                // cancel further handling
                e.Handled = true;
            }
            else if (this._lbAutoComplete.Visible && e.KeyCode == Keys.PageUp)
            {
                this.wordFound = true;
                if (this._lbAutoComplete.SelectedIndex > 10)
                    this._lbAutoComplete.SelectedIndex -= 10;
                // cancel further handling
                e.Handled = true;
            }
            else if (this._lbAutoComplete.Visible && e.KeyCode == Keys.PageDown)
            {
                this.wordFound = true;
                if (this._lbAutoComplete.SelectedIndex < this._lbAutoComplete.Items.Count - 10)
                    this._lbAutoComplete.SelectedIndex += 10;
                // cancel further handling
                e.Handled = true;
            }
            // non-alphanumeric keys will hide the listbox also
            else if (e.KeyValue < 48
           || (e.KeyValue >= 58 && e.KeyValue <= 64)
           || (e.KeyValue >= 91 && e.KeyValue <= 96)
           || (e.KeyValue > 122 && e.KeyValue != 189))
            {
                if (this._lbAutoComplete.Visible)
                {   // insert on tab/space/return
                    if (e.KeyCode == Keys.Return
                        || e.KeyCode == Keys.Tab
                        || e.KeyCode == Keys.Space)
                    {
                        this.ACSelectItem();
                        // reset
                        this.wordFound = false;
                        // cancel further handling
                        e.Handled = true;
                    }
                    // hide the listbox
                    this._lbAutoComplete.Hide();
                }
            }
            /* alphanumeric key typed, check our listbox content
             */
            else if (!e.Control && !e.Alt)
            {   // check visibility
                if (!this._lbAutoComplete.Visible)
                {   // prepare the listbox if the members are listed currently
                    if (this.fieldsInListbox)
                    {
                        ACUpdateListBox();
                        this.fieldsInListbox = false;
                    }
                    if (this._lbAutoComplete.Items.Count > 0)
                    {   // got such a item?
                        string srchStr = ACGetLastWord(currentTypedChar);
                        if (ACSearchItem(srchStr))
                            ACShowListBox();        // yes, display
                        else
                            this._lbAutoComplete.Hide(); // no, hide
                    }
                }
                else if (this._lbAutoComplete.Visible)
                {   // check the listbox content if it got something inside which starts with typed
                    string srchStr = ACGetLastWord(currentTypedChar);
                    if (!ACSearchItem(srchStr))
                        this._lbAutoComplete.Hide();
                }
            }
        }
        private void _rtbSqlQry_MouseDown(object sender, MouseEventArgs e)
        {   // no autocompletition used
            if (!NyxMain.UASett.AutoCompletition || this._lbAutoComplete == null)
                return;
            // hide listview
            if (this._lbAutoComplete.Visible)
                this._lbAutoComplete.Hide();
        }
        /// <summary> Update changed datagrid to database, public cause of hotkey from nyxmain </summary>
        public void UpdateDataGrid()
        {
            // got changes?
            if (dsResult.HasChanges())
            {   // send changes?
                StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("ST_UpdateDataToDatabase"),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                if (strRD.DRes == NyxDialogResult.Yes)
                {   // building thread
                    DBThreadStartParam strThrStr = new DBThreadStartParam();
                    strThrStr.ThrAction = DatabaseAction.UpdateDS;
                    strThrStr.Param1 = dbh.ParseSqlQuery(this._rtbSqlQry.Text.ToString(), SqlQryGetLimit(), true);
                    // control gui
                    ((NyxMain)this.ParentForm).CtrlAppDisplay(GuiHelper.GetResourceMsg("ST_UpdateData"));
                    // start thread
                    DbThreadInit(strThrStr);
                }
            }
        }
        private void UpdateDGridDone(DBThreadReturnParam strThrReturn)
        {   // checkup
            if (strThrReturn.DBException == null)
            {   // set dgrid to readonly again
                this._dgSql.ReadOnly = true;
                this._dgSql.AllowUserToAddRows = false;
                this._dgSql.AllowUserToDeleteRows = false;
                this._dgSql.AllowUserToOrderColumns = false;
                // set buttons
                SwitchEditButtons(true, false);
            }
            else
            {   // error
                dsResult.Reset();
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ST_UpdateDataSetErr", 
                    new string[] { strThrReturn.DBException.Message.ToString() }), strThrReturn.DBException, 
                    MessageHandler.EnumMsgType.Warning);
            }
            ((NyxMain)this.ParentForm).CtrlAppDisplay(String.Empty);
        }
        private void _tsSql_EditData_Click(object sender, EventArgs e)
        {
            EditDataGrid();
        }
        private void _tsSql_UpdateData_Click(object sender, EventArgs e)
        {
            UpdateDataGrid();
        }

        private void SwitchEditButtons(bool editEnabled, bool updEnabled)
        {  
            this._tsSql_EditData.Enabled = editEnabled;
            this._cmSqlDta_Edit.Enabled = editEnabled;
            ((NyxMain)this.ParentForm).MenuSqlSqlEditData = editEnabled;
            this._cmSqlDta_Update.Enabled = updEnabled;
            this._tsSql_UpdateData.Enabled = updEnabled;
            ((NyxMain)this.ParentForm).MenuSqlUpdData = updEnabled;
        }
        /// <summary> autocompletition </summary>
        private void _lbAutoComplete_SelectedIndexChanged(object sender, System.EventArgs e)
        {   // set control back to the rtb after selected
            this._rtbSqlQry.Focus();
        }
        private void _lbAutoComplete_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {   // ignore all pressed keys on the autocomplete listview (that way they get sent to the rtb)
            this._rtbSqlQry.Focus();
        }
        private void _lbAutoComplete_DoubleClick(object sender, System.EventArgs e)
        {   // select
            if (this._lbAutoComplete.SelectedItems.Count == 1)
            {   // set word and hide
                this.wordFound = true;
                this.ACSelectItem();
                this._lbAutoComplete.Hide();
                this._rtbSqlQry.Focus();
                this.wordFound = false;
            }
        }
        private void ACShowListBox()
        {
            // find the location for showing up the listview
            System.Drawing.Point point;
            point = this._rtbSqlQry.GetPositionFromCharIndex(this._rtbSqlQry.SelectionStart);
            point.Y += (int)Math.Ceiling(this._rtbSqlQry.Font.GetHeight()) + 25;
            point.X += 10;
            // set and show
            this._lbAutoComplete.Height = 100;
            this._lbAutoComplete.Location = point;
            this._lbAutoComplete.BringToFront();
            this._lbAutoComplete.Show();
        }
        private bool ACSearchItem(string srchStr)
        {
            if (srchStr.Length == 0)
                return false;
            // check the listbox content if it got something inside which starts with typed
            this.wordFound = false;
            for (int i = 0; i < this._lbAutoComplete.Items.Count; i++)
            {
                if (this._lbAutoComplete.Items[i].ToString().ToUpper(NyxMain.NyxCI).StartsWith(srchStr.ToUpper(NyxMain.NyxCI)))
                {   // word found, set index and break
                    this.wordFound = true;
                    this._lbAutoComplete.SelectedIndex = i;
                    return true;
                }
            }
            return false;
        }
        private void ACSelectItem()
        {
            if (this.wordFound)
            {
                string lastWord = ACGetLastWord("");
                int selStart = this._rtbSqlQry.SelectionStart;
                // get the suffix (word after the current selection)
                int suffixStart = selStart;
                if (suffixStart >= this._rtbSqlQry.Text.Length)
                    suffixStart = this._rtbSqlQry.Text.Length;
                string suffix = this._rtbSqlQry.Text.Substring(suffixStart, this._rtbSqlQry.Text.Length - suffixStart);
                // get the prefix 
                string prefix = String.Empty;
                if (lastWord.Length > 0 && lastWord.Substring(0, 1) == ".")
                    prefix = this._rtbSqlQry.Text.Substring(0, this._rtbSqlQry.SelectionStart - lastWord.Length + 1);
                else
                    prefix = this._rtbSqlQry.Text.Substring(0, this._rtbSqlQry.SelectionStart - lastWord.Length);
                // the item to fill
                string fill = this._lbAutoComplete.SelectedItem.ToString();
                // set the text and selectionstart
                this._rtbSqlQry.Text = prefix + fill + suffix;
                this._rtbSqlQry.SelectionStart = prefix.Length + fill.Length;
            }
        }
        private string ACGetLastWord(string currentTypedChar)
        {   // get the last word
            string word = String.Empty;

            int selStart = this._rtbSqlQry.SelectionStart;
            if (selStart > 0)
            {
                string tmpStr = String.Empty;
                char tmpChar = new char();
                while (tmpChar != ' '
                    && tmpChar != '.'// dot, we add
                    && tmpChar != 10
                    && selStart > 0)
                {
                    selStart--;
                    tmpStr = _rtbSqlQry.Text.Substring(selStart, 1);
                    tmpChar = (char)tmpStr[0];
                    if (tmpChar == ',' || tmpChar == '.' || tmpChar == '\'')
                        break;
                    word = String.Format(NyxMain.NyxCI, "{0}{1}", word, tmpChar);
                }
                // reverse char array, cause we got it from behind
                char[] ca = word.ToCharArray();
                Array.Reverse(ca);
                word = new String(ca).Trim().ToUpper(NyxMain.NyxCI);
                word = String.Format(NyxMain.NyxCI, "{0}{1}", word, currentTypedChar);
                return word;
            }
            else
                return currentTypedChar;
        }
        /// <summary> Activate autocompletition through hotkey </summary>
        public void ACHotkeyActivation()
        {
            // only do something if visible
            if (!this._lbAutoComplete.Visible)
            {   // prepare the listbox if the members are listed currently
                if (this.fieldsInListbox)
                {
                    ACUpdateListBox();
                    this.fieldsInListbox = false;
                }
                if (this._lbAutoComplete.Items.Count > 0)
                {   ACShowListBox();        // yes, display
                }
            }
        }
        private string ACGetLastWord()
        {   // get the last word
            string word = String.Empty;

            int selStart = this._rtbSqlQry.SelectionStart;
            if (selStart > 0)
            {
                string tmpStr = String.Empty;
                char tmpChar = new char();
                while (tmpChar != ' '
                    && tmpChar != '.'// dot, we add
                    && tmpChar != 10
                    && selStart > 0)
                {
                    selStart--;
                    tmpStr = _rtbSqlQry.Text.Substring(selStart, 1);
                    tmpChar = (char)tmpStr[0];
                    if (tmpChar == ',' || tmpChar == '.' || tmpChar == '\'')
                        break;
                    word = String.Format(NyxMain.NyxCI, "{0}{1}", word, tmpChar);
                }
                // reverse char array, cause we got it from behind
                char[] ca = word.ToCharArray();
                Array.Reverse(ca);
                word = new String(ca).Trim().ToUpper(NyxMain.NyxCI);
                return word;
            }
            else
                return "";
        }
    }
}
