/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using Nyx.Classes;
using Nyx.AppDialogs;
using NyxSettings;
using System.Data;
using System.Windows.Forms;
using System.Reflection;
using System.Globalization;
using System.Resources;
using System.Collections;
using System.Collections.Generic;

namespace Nyx
{   
    // thread delegates (parameterless delegate, parameter delegate)
    /// <summary> ThreadDelegate, gets invoked after a thread action finished </summary>
    public delegate void ThrdCom();
    
    /// <summary>
    /// Main Program.
    /// </summary>
    public partial class NyxMain : Form
    {
        #region Variables, Enums, Events
        // db connectionvariables
        private static string dbVersion;
        // 'storage' vars
        private System.Collections.ObjectModel.Collection<PreviewObject> prvFilterList =
            new System.Collections.ObjectModel.Collection<PreviewObject>();
        private ArrayList tblFilterList = new ArrayList();
        private ArrayList viewFilterList = new ArrayList();
        private ArrayList prcdrFilterList = new ArrayList();
        private ArrayList triggerFilterList = new ArrayList();
        // misc vars
        private System.Threading.Thread doWork;
        // connected profile
        private static ProfileSettings conProfile = new ProfileSettings();
        // settings
        private static UserAppSettings uaSett = new UserAppSettings();
        private static Hashtable htProfiles;
        private static DataTable dtBookmarks;
        private static RuntimeSettings rtSett = new RuntimeSettings();
        // preview dataset and current query
        private static DataSet dsPreview;
        private static PreviewQuery prvQry;
        // listviewcolumnsorter for inital listviews
        ListViewColumnSorter lvcsTableDetails, lvcsIndexes, lvcsIndexDetails, lvcsPrvObjects;
        // init dbhelper
        private DBHelper dbh;
        private DBManagement dbm;
        /// cultureinfo
        private static System.Globalization.CultureInfo nyxCI = System.Globalization.CultureInfo.InvariantCulture;
        /// <remarks> global cultureinfo </remarks>
        public static System.Globalization.CultureInfo NyxCI { get { return nyxCI; } }
        /// autocomplete vars
        private static NyxAutoComplete.ACListBox lbAutoComplete;
        /// <remarks> global autocompletelistbox </remarks>
        public static NyxAutoComplete.ACListBox LBAutoComplete { get { return lbAutoComplete; } set { lbAutoComplete = value; } }
        /// <remarks> Control application restart event handler </remarks>
        public event EventHandler<ControlAppEventArgs> OnControlAppHandler;
        /// <remarks> global database name </remarks>
        public static string DBName
        {
            get
            {
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.PgSql:
                    case DBType.MySql: return NyxMain.conProfile.ProfileDB;
                    case DBType.MSSql: return NyxMain.conProfile.ProfileParam1;
                    default: return NyxMain.conProfile.ProfileTarget;
                }
            }
            set
            {
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql: NyxMain.conProfile.ProfileDB = value; break;
                    case DBType.MSSql: NyxMain.conProfile.ProfileParam1 = value; break;
                    default: NyxMain.conProfile.ProfileTarget = value; break;
                }
            }
        }
        /// <remarks> global database version </remarks>
        public static string DBVersion
        {
            get
            {
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql: return "MySql v" + NyxMain.dbVersion;
                    case DBType.MSSql: return "MSSql v" + NyxMain.dbVersion;
                    case DBType.Oracle: return NyxMain.dbVersion;
                    case DBType.PgSql: return "PostgreSql v" + NyxMain.dbVersion;
                    case DBType.Odbc: return "(ODBC) " + NyxMain.dbVersion;
                    default: return "Unknown Database-Version";
                }
            }
            set { dbVersion = value; }
        }
        /// <remarks> global user application settings </remarks>
        public static UserAppSettings UASett { get { return uaSett; } set { uaSett = value; } }
        /// <remarks> global profiles </remarks>
        public static Hashtable HTProfiles { get { return htProfiles; } }
        /// <remarks> global bookmarks </remarks>
        public static DataTable DTBookmarks { get { return dtBookmarks; } }
        /// <remarks> global runtimesettings </remarks>
        public static RuntimeSettings RTSett { get { return rtSett; } }
        /// <remarks> global current connected profile </remarks>
        public static ProfileSettings ConProfile { get { return conProfile; } set { conProfile = value; } }
        #endregion

        #region Con- & Destructor, MainFormEvents
        /// <summary> MainApplicationForm </summary>
        public NyxMain()
        {   // subscribe events
            OnControlAppHandler += new EventHandler<ControlAppEventArgs>(ControlAppEvent);
            // 1st start setup
            if (!System.IO.File.Exists(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData) + "\\Nyx\\nyx-main.xml")
                && !System.IO.File.Exists(Application.StartupPath + "\\nyx-main.xml"))
            {
                Tools.FirstStartSetup ss = new Tools.FirstStartSetup();
                ss.ShowDialog();
            }
            // load user app settings
            uaSett = new UserAppSettings();
            if (!Settings.DeserializeUserSettings(ref uaSett, Settings.GetFolder(SettingsType.AppSettings)))
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrUserSettingsLoad", new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
            // check language
            switch (NyxMain.uaSett.Language)
            {
                case Language.Auto: break;// let the system itself choose the language
                case Language.enUS: System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US"); break;
                case Language.deDE: System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("de-DE"); break;
            }
            // intialize components
            InitializeComponent();
            // assign listviewcolumnsorter
            lvcsTableDetails = new ListViewColumnSorter();
            this._lvTableDesc.ListViewItemSorter = lvcsTableDetails;
            lvcsIndexes = new ListViewColumnSorter();
            this._lvIndexes.ListViewItemSorter = lvcsIndexes;
            lvcsIndexDetails = new ListViewColumnSorter();
            this._lvIndexDesc.ListViewItemSorter = lvcsIndexDetails;
            lvcsPrvObjects = new ListViewColumnSorter();
            this._lvPrvObjects.ListViewItemSorter = lvcsPrvObjects;
            // set some gui-presets
            string version = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString();
            this._sS_lblStatusMsg.Text = "NyxSql v" + version + ". (c) 2005-2007 D. Schischma - USE AT YOUR OWN RISK!";
            this.Text += version;
            this._tsPrvDta_RowsPerPage.SelectedItem = "50";
            // load runtime, set settings and hotkeys
            LoadRuntimeSettings();
            SetUserAppSettings();
            LoadHotKeys();
            // init autocompletition
            if (NyxMain.UASett.AutoCompletition)
                NyxMain.lbAutoComplete = new NyxAutoComplete.ACListBox();
        }
        /// <summary> MainApplicationFormLoadEvent </summary>
        private void Nyx_Load(object sender, EventArgs e)
        {   // load runtimesettings
            
            SetRuntimeSettings();
            // profiles
            LoadProfiles();
            // bookmarks
            LoadBookmarks();
            // history
            History his = new History();
            his.SetHis(this._cbSqlHistory);
            // init first sql tab
            SqlCreateNewTab(false);
        }
        /// <summary>
        /// Override OnClosing for saving settings
        /// </summary>
        /// <param name="e">CancelEventArgs</param>
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {   // minimize or exitapp?
            if (this._NI.Visible)
            {
                // overridden close-function
                e.Cancel = true;	// canceling close
                HideApp();			// hiding app
            }
            else
            {   // show the exit dialog? and if no canceled don't exit
                bool exit = PrepareExit();
                if (!exit)
                    e.Cancel = true;
            }
        }
        #endregion

        #region Profiles, Bookmarks, HotKeys, RuntimeSettings, UserSettings
        private void LoadProfiles()
        {
            // profiles + settings
            htProfiles = new Hashtable();
            if (Settings.DeserializeProfiles(ref htProfiles, Settings.GetFolder(SettingsType.Profile)))
                SetProfiles(true);
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrProfilesLoad", new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
                // reset profiles
                this._tsMain_cbProfiles.BeginUpdate();
                this._tsMain_cbProfiles.Items.Clear();
                this._tsMain_cbProfiles.Items.Add("QuickConnect");
                this._tsMain_cbProfiles.SelectedItem = "QuickConnect";
                this._tsMain_cbProfiles.EndUpdate();
            }
        }
        private void SetProfiles(bool startup)
        {   // refresh profiles
            if (htProfiles.Count > 0)
            {   // save current selected profile
                object selItem = this._tsMain_cbProfiles.SelectedItem;
                // resetting to default
                this._tsMain_cbProfiles.BeginUpdate();
                this._tsMain_cbProfiles.Items.Clear();
                this._tsMain_cbProfiles.Items.Add("QuickConnect");
                this._tsMain_cbProfiles.SelectedIndex = 0;
                // adding profiles
                if (NyxMain.htProfiles != null)
                    foreach (DictionaryEntry de in NyxMain.htProfiles)
                        this._tsMain_cbProfiles.Items.Add(de.Key);
                // re-set selection
                if (selItem != null)
                    this._tsMain_cbProfiles.SelectedItem = selItem;
                this._tsMain_cbProfiles.EndUpdate();

                // have we got any profiles
                if (startup && HTProfiles.Count > 0)
                {   // pre-select profile got through cmdline?
                    string[] args = Environment.GetCommandLineArgs();
                    if (args.Length > 1 && args[1] != null)
                    {
                        this._tsMain_cbProfiles.SelectedItem = args[1];
                        // found the profile??
                        if (this._tsMain_cbProfiles.SelectedItem.ToString() == args[1])
                            DbConnect();
                        else
                        {
                            MessageHandler.ShowDialog("Unable to find profile:" + args[1] + "!",
                                MessageHandler.EnumMsgType.Warning);
                        }
                    }
                    // check default profile
                    else if (uaSett.DefaultProfile.Length > 0)
                    {
                        this._tsMain_cbProfiles.SelectedItem = uaSett.DefaultProfile;
                        // found the profile??
                        if (this._tsMain_cbProfiles.SelectedItem.ToString() == uaSett.DefaultProfile)
                            DbConnect();
                        else
                        {
                            MessageHandler.ShowDialog(
                                "Unable to find the profile " + UASett.DefaultProfile + ".",
                                MessageHandler.EnumMsgType.Warning);
                        }
                    }
                }
            }
            else
            {   // set default text 
                this._tsMain_cbProfiles.BeginUpdate();
                this._tsMain_cbProfiles.Items.Clear();
                this._tsMain_cbProfiles.Items.Add("QuickConnect");
                this._tsMain_cbProfiles.SelectedItem = "QuickConnect";
                this._tsMain_cbProfiles.EndUpdate();
            }
        }
        private void LoadBookmarks()
        {   // load bookmarks
            dtBookmarks = new DataTable("Bookmarks");
            dtBookmarks.Locale = NyxMain.nyxCI;
            if (Settings.DeserializeBookmarks(ref dtBookmarks, Settings.GetFolder(SettingsType.Bookmarks)))
                SetBookmarks();
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("BM_LoadErr",
                    new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
            }
        }
        private void SetBookmarks()
        {   // init the menue
            BookmarkManager.SetBookmarks(this._msBookmarks);
            // add the clickeventhandler
            foreach (ToolStripItem tsi in this._msBookmarks.DropDownItems)
            {
                if (tsi.Tag != null)
                {
                    if (tsi.Tag.ToString() == "entry")
                        tsi.Click += new EventHandler(this._msBookmark_Click);
                    // we got a cat, we have to check lvl2
                    else if (tsi.Tag.ToString() == "cat")
                    {
                        ToolStripMenuItem tsmi1 = (ToolStripMenuItem)tsi;
                        foreach (ToolStripMenuItem tsmi2 in tsmi1.DropDownItems)
                        {
                            if (tsmi2.Tag.ToString() == "entry")
                                tsmi2.Click += new EventHandler(this._msBookmark_Click);
                            // we got a cat, we have to check lvl3
                            else if (tsmi2.Tag.ToString() == "cat")
                            {
                                foreach (ToolStripMenuItem tsmi3 in tsmi2.DropDownItems)
                                {
                                    if (tsmi3.Tag.ToString() == "entry")
                                        tsmi3.Click += new EventHandler(this._msBookmark_Click);
                                }
                            }
                        }
                    } // else if (tsi.Tag.ToString() == "cat")
                } // if (tsi.Tag != null)
            }
        }
        private void LoadHotKeys()
        {   // load
            HotKeys hkd = new HotKeys();
            if (Settings.DeserializeHotkeys(ref hkd, Settings.GetFolder(SettingsType.Hotkeys)))
            {   // reset all hotkeys
                this._msFile_Connect.ShortcutKeys = (Keys)Shortcut.None;
                this._msFile_Disconnect.ShortcutKeys = (Keys)Shortcut.None;
                this._msSql_ExecSql.ShortcutKeys = (Keys)Shortcut.None;
                this._msSql_ExecSel.ShortcutKeys = (Keys)Shortcut.None;
                this._msSql_ClearSql.ShortcutKeys = (Keys)Shortcut.None;
                this._msSql_EditData.ShortcutKeys = (Keys)Shortcut.None;
                this._msSql_UpdData.ShortcutKeys = (Keys)Shortcut.None;
                this._msDbT_SqlBldr.ShortcutKeys = (Keys)Shortcut.None;
                this._msDbT_ShowAC.ShortcutKeys = (Keys)Shortcut.None;
                this._msSql_SqlTabs_NewTab.ShortcutKeys = (Keys)Shortcut.None;
                this._msSql_SqlTabs_CloseTab.ShortcutKeys = (Keys)Shortcut.None;
                this._msView_Sql.ShortcutKeys = (Keys)Shortcut.None;
                this._msView_Tables.ShortcutKeys = (Keys)Shortcut.None;
                this._msView_Preview.ShortcutKeys = (Keys)Shortcut.None;
                this._msView_Views.ShortcutKeys = (Keys)Shortcut.None;
                this._msView_Prcdrs.ShortcutKeys = (Keys)Shortcut.None;
                this._msView_Triggers.ShortcutKeys = (Keys)Shortcut.None;
                this._msView_SysTray.ShortcutKeys = (Keys)Shortcut.None;
                this._msFile_Profiles.ShortcutKeys = (Keys)Shortcut.None;
                this._msFile_Settings.ShortcutKeys = (Keys)Shortcut.None;
                this._msFile_ChkUpdate.ShortcutKeys = (Keys)Shortcut.None;
                this._msBookmarks_Mng.ShortcutKeys = (Keys)Shortcut.None;
                this._msFile_QuickConnect.ShortcutKeys = (Keys)Shortcut.None;
                this._msFile_Exit.ShortcutKeys = (Keys)Shortcut.None;
                // set hotkeys
                this._msFile_Connect.ShortcutKeys = (Keys)hkd.Connect;
                this._msFile_Disconnect.ShortcutKeys = (Keys)hkd.Disconnect;
                this._msSql_ExecSql.ShortcutKeys = (Keys)hkd.ExecuteSql;
                this._msSql_ExecSel.ShortcutKeys = (Keys)hkd.ExecuteSelectedSql;
                this._msSql_ClearSql.ShortcutKeys = (Keys)hkd.ClearSql;
                this._msDbT_SqlBldr.ShortcutKeys = (Keys)hkd.ShowSqlBuilder;
                this._msDbT_ShowAC.ShortcutKeys = (Keys)hkd.ShowAutoCompletition;
                this._msSql_SqlTabs_NewTab.ShortcutKeys = (Keys)hkd.NewSqlTab;
                this._msSql_SqlTabs_CloseTab.ShortcutKeys = (Keys)hkd.CloseSqlTab;
                this._msView_Sql.ShortcutKeys = (Keys)hkd.ViewSql;
                this._msView_Tables.ShortcutKeys = (Keys)hkd.ViewTables;
                this._msView_Preview.ShortcutKeys = (Keys)hkd.ViewPreview;
                this._msView_Views.ShortcutKeys = (Keys)hkd.ViewViews;
                this._msView_Prcdrs.ShortcutKeys = (Keys)hkd.ViewPrcdrs;
                this._msView_Triggers.ShortcutKeys = (Keys)hkd.ViewTriggers;
                this._msView_SysTray.ShortcutKeys = (Keys)hkd.MinimizeToSystray;
                this._msFile_Profiles.ShortcutKeys = (Keys)hkd.ProfileDialog;
                this._msFile_Settings.ShortcutKeys = (Keys)hkd.SettingsDialog;
                this._msFile_ChkUpdate.ShortcutKeys = (Keys)hkd.CheckForUpdate;
                this._msBookmarks_Mng.ShortcutKeys = (Keys)hkd.BookmarkDialog;
                this._msFile_QuickConnect.ShortcutKeys = (Keys)hkd.QuickConnect;
                this._msFile_Exit.ShortcutKeys = (Keys)hkd.Exit;
            }
            else
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrHotkeysLoad", new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
            }
        }
        private void LoadRuntimeSettings()
        {   // load runtimesettings
            if (!Settings.DeserializeRuntime(ref rtSett, Settings.GetFolder(SettingsType.Runtime)))
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrRuntimeSettingsLoad", new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
            }
        }
        private void SetRuntimeSettings()
        {   // apply the runtime settings
            this.WindowState = rtSett.WindowState;
            this.Location = new System.Drawing.Point(rtSett.WindowPosition.Location.X, rtSett.WindowPosition.Location.Y);
            this.Width = rtSett.WindowPosition.Width;
            this.Height = rtSett.WindowPosition.Height;
            // further apply runtime settings
            this._tsMain.Visible = rtSett.TBVisibilityMain;
            this._tsMain.Location = rtSett.TBLocationMain;
            // splitterdistances
            if (rtSett.DistanceSqlBldrSplitter > this._splitSqlBldr.Panel1MinSize && rtSett.DistanceSqlBldrSplitter < this._splitSqlBldr.Height)
                this._splitSqlBldr.SplitterDistance = rtSett.DistanceSqlBldrSplitter;
            if (rtSett.DistancePreviewSplitter > this._splitPreview.Panel1MinSize && rtSett.DistancePreviewSplitter < this._splitPreview.Width)
                this._splitPreview.SplitterDistance = rtSett.DistancePreviewSplitter;
            if (rtSett.DistancePreviewDataSplitter > this._splitPreviewDta.Panel1MinSize && rtSett.DistancePreviewDataSplitter < this._splitPreviewDta.Height)
                this._splitPreviewDta.SplitterDistance = rtSett.DistancePreviewDataSplitter;
            if (rtSett.DistanceTablesSplitter > this._splitTables.Panel1MinSize && rtSett.DistanceTablesSplitter < this._splitTables.Width)
                this._splitTables.SplitterDistance = rtSett.DistanceTablesSplitter;
            if (rtSett.DistanceIndexesSplitter > this._splitIndexes.Panel1MinSize && rtSett.DistanceIndexesSplitter < this._splitIndexes.Width)
                this._splitIndexes.SplitterDistance = rtSett.DistanceIndexesSplitter;
            if (rtSett.DistanceIndexDetailsSplitter > this._splitIndexDetails.Panel1MinSize && rtSett.DistanceIndexDetailsSplitter < this._splitIndexDetails.Height)
                this._splitIndexDetails.SplitterDistance = rtSett.DistanceIndexDetailsSplitter;
            if (rtSett.DistanceViewsSplitter > this._splitViews.Panel1MinSize && rtSett.DistanceViewsSplitter < this._splitViews.Width)
                this._splitViews.SplitterDistance = rtSett.DistanceViewsSplitter;
            if (rtSett.DistanceTriggersSplitter > this._splitTriggers.Panel1MinSize && rtSett.DistanceTriggersSplitter < this._splitTriggers.Width)
                this._splitTriggers.SplitterDistance = rtSett.DistanceTriggersSplitter;
            if (rtSett.DistancePrcdrsSplitter > this._splitProcedures.Panel1MinSize && rtSett.DistancePrcdrsSplitter < this._splitProcedures.Width)
                this._splitProcedures.SplitterDistance = rtSett.DistancePrcdrsSplitter;
            if (rtSett.DistanceViews2Splitter > this._splitViews2.Panel1MinSize && rtSett.DistanceViews2Splitter < this._splitViews2.Width)
                this._splitViews2.SplitterDistance = rtSett.DistanceViews2Splitter;
            if (rtSett.DistancePrcdrs2Splitter > this._splitProcedures2.Panel1MinSize && rtSett.DistancePrcdrs2Splitter < this._splitProcedures2.Width)
                this._splitProcedures2.SplitterDistance = rtSett.DistancePrcdrs2Splitter;
            if (rtSett.DistanceTriggers2Splitter > this._splitTriggers2.Panel1MinSize && rtSett.DistanceTriggers2Splitter < this._splitTriggers2.Width)
                this._splitTriggers2.SplitterDistance = rtSett.DistanceTriggers2Splitter;
        }
        private void SetUserAppSettings()
        {   // check if settings had been loaded
            if (uaSett == null)
                return;
            // system tray icon
            this._NI.Visible = uaSett.ShowSystrayIcon;
            // historyfont
            System.Drawing.Font font;
            if (uaSett.HistoryFont.Length > 0)
            {
                GuiHelper.GetFont(uaSett.HistoryFont, out font);
                this._cbSqlHistory.Font = font;
            }
            
            // auto update
            if (uaSett.AutoCheckUpdate)
            {
                DateTime dtNow = DateTime.Now;
                TimeSpan chkDelay = dtNow - rtSett.LastUpdatecheck;
                int aChkDelay = 0;
                // get delay
                switch (uaSett.AutoCheckDelay)
                {
                    case DefaultTimeSpan.EveryWeek:
                        aChkDelay = 7;
                        break;
                    case DefaultTimeSpan.EveryMonth:
                        aChkDelay = 30;
                        break;
                    case DefaultTimeSpan.EveryDay:
                        aChkDelay = 1;
                        break;
                    case DefaultTimeSpan.Every2Weeks:
                        aChkDelay = 14;
                        break;
                }
                if (chkDelay.Days > aChkDelay)
                    StartUpdater();
            }
            // format preview datagridview
            this._dgPreview = GuiHelper.DataGridViewFormat(uaSett, this._dgPreview);
            // format sqlmultitab datagrids
            foreach (TabPage tp in this._tcSql.TabPages)
            {
                SqlTab smt = (SqlTab)tp.Tag;
                smt.ApplySettings(uaSett);
            }
        }
        private void SaveRuntimeSettings()
        {   // set runtime settings
            rtSett.WindowState = this.WindowState;
            if (this.WindowState != FormWindowState.Maximized)
                rtSett.WindowPosition = new System.Drawing.Rectangle(this.Location.X, this.Location.Y, this.Width, this.Height);
            rtSett.TBVisibilityMain = this._tsMain.Visible;
            rtSett.TBLocationMain = this._tsMain.Location;
            if (this._splitSqlBldr.SplitterDistance > 15)
                rtSett.DistanceSqlBldrSplitter = this._splitSqlBldr.SplitterDistance;
            if (this._splitPreview.SplitterDistance > 15)
                rtSett.DistancePreviewSplitter = this._splitPreview.SplitterDistance;
            if (this._splitPreviewDta.SplitterDistance > 15)
                rtSett.DistancePreviewDataSplitter = this._splitPreviewDta.SplitterDistance;
            if (this._splitTables.SplitterDistance > 15)
                rtSett.DistanceTablesSplitter = this._splitTables.SplitterDistance;
            if (this._splitIndexes.SplitterDistance > 15)
                rtSett.DistanceIndexesSplitter = this._splitIndexes.SplitterDistance;
            if (this._splitIndexDetails.SplitterDistance > 15)
                rtSett.DistanceIndexDetailsSplitter = this._splitIndexDetails.SplitterDistance;
            if (this._splitViews.SplitterDistance > 15)
                rtSett.DistanceViewsSplitter = this._splitViews.SplitterDistance;
            if (this._splitProcedures.SplitterDistance > 15)
                rtSett.DistancePrcdrsSplitter = this._splitProcedures.SplitterDistance;
            if (this._splitTriggers.SplitterDistance > 15)
                rtSett.DistanceTriggersSplitter = this._splitTriggers.SplitterDistance;
            if (this._splitViews2.SplitterDistance > 15)
                rtSett.DistanceViews2Splitter = this._splitViews2.SplitterDistance;
            if (this._splitProcedures2.SplitterDistance > 15)
                rtSett.DistancePrcdrs2Splitter = this._splitProcedures2.SplitterDistance;
            if (this._splitTriggers2.SplitterDistance > 15)
                rtSett.DistanceTriggers2Splitter = this._splitTriggers2.SplitterDistance;
            // save runtime settings
            if (!Settings.SerializeRuntime(rtSett, Settings.GetFolder(SettingsType.Runtime)))
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrRuntimeSettingsSave", new string[] { Settings.ErrExc.Message.ToString() }),
                    Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
        }
        #endregion

        #region Threading (DatabaseActions/TableActions)
        private void DBThreadInit(DBThreadStartParam thrStrVars)
        {
            doWork = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(DBThreadStart));
            doWork.IsBackground = true;
            doWork.Start(thrStrVars);
        }
        private void DBThreadStart(object threadParam)
        {
            try
            {   // init start and return structs
                DBThreadStartParam strThrdStart = (DBThreadStartParam)threadParam;
                DBThreadReturnParam strThrdReturn = new DBThreadReturnParam();
                ThreadHelper thrdHlp = new ThreadHelper(dbm);
                // set return action
                strThrdReturn.ThrAction = strThrdStart.ThrAction;
                strThrdStart.Dbh = this.dbh;
                // switch action
                switch (strThrdStart.ThrAction)
                {   // 'special' case
                    case DatabaseAction.Connect:
                        // opening database connection
                        if (dbh.ConnectDB(NyxMain.conProfile, true))
                        {   // init dbm
                            this.dbm = new DBManagement(NyxMain.ConProfile.ProfileDBType);
                            // get version?
                            NyxMain.dbVersion = dbh.ServerVersion;
                            // mysql/mssql, so get databases
                            switch (NyxMain.conProfile.ProfileDBType)
                            {
                                case DBType.MySql:
                                case DBType.MSSql:
                                    Queue<string> qstr = new Queue<string>();
                                    if (dbm.GetDatabases(out qstr, this.dbh))
                                        strThrdReturn.SetGQString1(qstr);
                                    else
                                        strThrdReturn.DBException = dbm.ErrExc;
                                    break;
                            }
                        }
                        else
                            strThrdReturn.DBException = dbh.ErrExc;
                        this.BeginInvoke(new DBManagement.DbmThrdEndPDelegate(DbConnectDone), strThrdReturn);
                        break;
                    // actions defined through threadhelper
                    case DatabaseAction.ChangeDB:
                        ThreadHelper.ExecNonQuery(strThrdStart, ref strThrdReturn);
                        this.BeginInvoke(new DBManagement.DbmThrdEndPDelegate(ChangeDbDone), strThrdReturn);
                        break;
                    // short actions, directrly defined
                    case DatabaseAction.Disconnect:
                        if (!dbh.DisconnectDB())
                            strThrdReturn.DBException = dbh.ErrExc;
                        this.BeginInvoke(new DBManagement.DbmThrdEndPDelegate(DbDisconnectDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetPreviewObjects:
                        thrdHlp.GetPreviewObjects(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetPreviewObjectsDone), strThrdReturn);
                        break;

                    case DatabaseAction.PreviewInit:
                    case DatabaseAction.PreviewInitFilter:
                        thrdHlp.InitPreview(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetFirstPreviewDataDone), strThrdReturn);
                        break;

                    case DatabaseAction.PrvGetNext:
                    case DatabaseAction.PreviewGetLast:
                    case DatabaseAction.PrvGetPrev:
                        thrdHlp.GetPreviewNextLastPrev(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetPreviewDataDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetTables:
                        thrdHlp.GetTables(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetTablesDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetViews:
                        thrdHlp.GetViews(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetViewsDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetViewDetails:
                        thrdHlp.GetViewDetails(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetViewDetailsDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetProcedures:
                        thrdHlp.GetProcedures(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetProceduresDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetProcedureDetails:
                        thrdHlp.GetProcedureDetails(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetProcedureDetailsDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetTriggers:
                        thrdHlp.GetTriggers(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetTriggersDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetTriggerDetails:
                        thrdHlp.GetTriggerDetails(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetTriggerDetailsDone), strThrdReturn);
                        break;

                    case DatabaseAction.DBContentInit:
                        thrdHlp.InitSqlBldr(strThrdStart, ref strThrdReturn);
                        strThrdReturn.Data1 = strThrdStart.Obj;
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(DbContentInitDone), strThrdReturn);
                        break;

                    case DatabaseAction.SqlBldrGetTableDescribe:
                        strThrdReturn.Data2 = strThrdStart.Obj;  // transfer treenode
                        thrdHlp.GetTableDetails(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(SqlBldrGetTableFullDescDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetTableFullDesc:
                        thrdHlp.GetTableFullDesc(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetTableFullDescDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetIndexes:
                        thrdHlp.GetIndexes(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetIndexesDone), strThrdReturn);
                        break;

                    case DatabaseAction.GetIndexDescribe:
                        thrdHlp.GetIndexDescribe(strThrdStart, ref strThrdReturn);
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(GetIndexDescribeDone), strThrdReturn);
                        break;

                    // 'special' case
                    case DatabaseAction.UpdateDS:
                        if (!dbh.Update(strThrdStart.Param1, dsPreview))
                            strThrdReturn.DBException = dbh.ErrExc;
                        this.Invoke(new DBManagement.DbmThrdEndPDelegate(UpdatePrvDGridDone), strThrdReturn);
                        break;

                    default: // exit due to fatal error
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ThreadIdentErr", new string[] { strThrdStart.ThrAction.ToString() }),
                            MessageHandler.EnumMsgType.Error);
                        break;
                }
            }
            
            catch (System.Threading.ThreadAbortException texc)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrThreadBreak", new string[] { texc.Message }),
                    texc, MessageHandler.EnumMsgType.Warning);
                // invoke reset thread
                this.Invoke(new ThrdCom(DBThreadAborted));
            }
        }
        private void TblThreadInit(TableManagement.StructTableOptions thrStrVars)
        {
            doWork = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(TblThreadstart));
            doWork.IsBackground = true;
            doWork.Start(thrStrVars);
        }
        private void TblThreadstart(object oStrThrStart)
        {
            try
            {   // init start and return structs
                TableManagement.StructTableOptions strThrdStart = (TableManagement.StructTableOptions)oStrThrStart;
                TableManagement.TblThreadReturnVars strThrdReturn = new TableManagement.TblThreadReturnVars();
                TableManagement tm = new TableManagement(dbh);
                // set action
                strThrdReturn.tblAction = strThrdStart.tblAction;
                // start thread
                if (!tm.ExecTableQuery(strThrdStart))
                    strThrdReturn.errExc = tm.ErrExc;
                // invoke final thread
                switch (strThrdStart.tblAction)
                {
                    case TableAction.TableDrop:
                        this.Invoke(new TableManagement.TblThrdPDelegate(TableActionDropDone), strThrdReturn);
                        break;
                    case TableAction.FieldDrop:
                        this.Invoke(new TableManagement.TblThrdPDelegate(TableActionFieldDropDone), strThrdReturn);
                        break;
                    case TableAction.IndexDrop:
                        this.Invoke(new TableManagement.TblThrdPDelegate(TableActionIndexDropDone), strThrdReturn);
                        break;
                    case TableAction.ViewDrop:
                        this.Invoke(new TableManagement.TblThrdPDelegate(TableActionViewDropDone), strThrdReturn);
                        break;
                    default:
                        this.Invoke(new TableManagement.TblThrdPDelegate(TableActionDone), strThrdReturn);
                        break;
                }
            }
            catch (System.Threading.ThreadAbortException texc)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrThreadBreak", new string[] { texc.Message }),
                    MessageHandler.EnumMsgType.Warning);
                // invoke reset thread
                this.Invoke(new ThrdCom(DBThreadAborted));
            }
        }
        /// <summary> Threadabort handle function </summary>
        public void DBThreadAborted()
        {   // reset gui
            CtrlAppDisplay(String.Empty);
            // set errortext
            this._sS_lblStatusMsg.Text = GuiHelper.GetResourceMsg("OperationCanceled");
        }        
        #endregion

        #region Database: Connect / Disconnect
        private void DbConnect()
        {   // checkup
            if (this._tsMain_cbProfiles.SelectedItem.ToString() == GuiHelper.GetResourceMsg("QuickConnect"))
            {
                if (NyxMain.ConProfile.ProfileDBType == DBType.NotSet)
                {
                    ProfileSettings prf = new ProfileSettings();
                    QuickConnect qc = new QuickConnect();
                    DialogResult dRes = qc.GetQuickConnect(ref prf);
                    if (dRes == DialogResult.OK)
                        NyxMain.conProfile = prf;
                    else
                        return;
                }
            }
            // init dbhelper
            dbh = new DBHelper(NyxMain.ConProfile.ProfileDBType);
            // display thread
            CtrlAppDisplay("Connecting database " + NyxMain.DBName + "...");
            // starting thread
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.Connect;
            // start thread...
            DBThreadInit(strThrStart);
        }
        private void DbConnectDone(DBThreadReturnParam strThrReturn)
        {   // check threadreturn
            if (strThrReturn.DBException == null)
            {   // mysql/mssql? got databases?
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql:
                    case DBType.MSSql:
                        // update cb
                        this._tsMain_cbChangeDb.BeginUpdate();
                        foreach (string db in strThrReturn.GQString1)
                            this._tsMain_cbChangeDb.Items.Add(db);
                        // another checkup
                        if (this._tsMain_cbChangeDb.Items.Count > 1)
                        {
                            this._tsMain_cbChangeDb.Visible = true;
                            this._tsMain_cbChangeDb.Text = DBName;
                        }
                        // end update
                        this._tsMain_cbChangeDb.EndUpdate();
                        break;
                    case DBType.Oracle:
                        // set globalization
                        string[] splitEncoding = NyxMain.conProfile.Encoding.Split(new Char[] { ';' });
                        if (splitEncoding.Length == 2)
                        {
                            Oracle.DataAccess.Client.OracleGlobalization orclGlob =
                                Oracle.DataAccess.Client.OracleGlobalization.GetThreadInfo();
                            orclGlob.Language = splitEncoding[1].Trim();
                            Oracle.DataAccess.Client.OracleGlobalization.SetThreadInfo(orclGlob);
                        }
                        break;
                }
                // init listviews
                GuiHelper.InitListView(this._lvTableDesc, this._lvIndexes, this._lvIndexDesc);
                // window title
                this.Text = String.Format(NyxMain.NyxCI, "NyxSql | Connected to {0} | {1}", NyxMain.DBName, NyxMain.DBVersion);
                // activate gui again
                CtrlAppDisplay(String.Empty);
                // connect buttons
                CtrlConnectButtons(false);
                // first disable scope boxes
                CtrlScopeBoxes(false);
                // disable related contextmenues
                DisableContextMenu();
                // disable dbrelated menustripts
                this._msOrcl.Enabled = false;
                this._msMySql.Enabled = false;
                // show panels, will be checked afterwards also
                CtrlPanelVisiblity(true);
                // misc settings
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.Oracle:
                        // menustrip
                        this._msOrcl.Enabled = true;
                        // enable scope
                        CtrlScopeBoxes(true);
                        // enable table contextmenu
                        this._cmTableDiag_Analyze.Enabled = true;
                        this._cmPrvObjDiag_Analyze.Enabled = true;
                        // set rest of default values
                        goto default;
                    case DBType.MySql:
                        // check after correct mysql-version
                        int iVnr;
                        if (Int32.TryParse(NyxMain.dbVersion.Substring(0, 1), out iVnr) && iVnr < 5)
                        {
                            this._pnlProcedures.Hide();
                            this._pnlViews.Hide();
                            this._pnlTriggers.Hide();
                        }
                        // menustrip
                        this._msMySql.Enabled = true;
                        // enable table contextmenu
                        this._cmTable_Create.Enabled = true;
                        this._cmTableDiag_Analyze.Enabled = true;
                        this._cmTableDiag_Check.Enabled = true;
                        this._cmTableDiag_OptReb.Enabled = true;
                        this._cmTableDiag_Repair.Enabled = true;
                        // preview
                        this._cmPrvObjDiag_Analyze.Enabled = true;
                        this._cmPrvObjDiag_Check.Enabled = true;
                        this._cmPrvObjDiag_Optimize.Enabled = true;
                        this._cmPrvObjDiag_Repair.Enabled = true;
                        // indexes
                        this._cmIndex_Create.Enabled = true;
                        // tablefields
                        this._cmTblField_Create.Enabled = true;
                        // views
                        this._tsViews_Create.Enabled = true;
                        // set rest of default values
                        goto default;
                    case DBType.MSSql:
                        // set rest of default values
                        goto default;
                    case DBType.PgSql:
                        // enable scope
                        CtrlScopeBoxes(true);
                        this._tsTriggers_cbScope.Enabled = false;
                        this._tsPrcdrs_cbScope.Enabled = false;
                        // set rest of default values
                        goto default;
                    case DBType.Odbc:
                    case DBType.NotSet:
                        // disable dbtools, preview
                        this._msDbT.Enabled = false;
                        this._msPrv.Enabled = false;
                        // hide panels
                        CtrlPanelVisiblity(false);
                        // view menustrip
                        CtrlViewMenuStrip(false);
                        break;
                    // default is only reched when goto called and
                    // this happens only on dbtype == pgsql/mysql/oracle/mssql
                    default:
                        // fill the treenode reserved words
                        if (NyxMain.uaSett.AutoCompletition)
                            foreach (string word in dbh.ReservedWords)
                                this._trvSqlBldr.Nodes["reservedWords"].Nodes.Add(word, word);
                        // db menustrip
                        this._msPrv.Enabled = true;
                        this._msDbT.Enabled = true;
                        // view menustrip
                        CtrlViewMenuStrip(true);
                        break;
                }
                // tabcontrol
                this._tcMain.Enabled = true;
                // init autocompletition
                if (NyxMain.UASett.AutoCompletition)
                    ACPrepareListBox();
                // reinit sqlmultitabs
                foreach (TabPage tp in this._tcSql.TabPages)
                {
                    SqlTab smt = (SqlTab)tp.Tag;
                    smt.InitClasses(dbh);
                }
                // sqlbuilder prefetch after connect
                DbContentInit(NyxMain.uaSett.SqlBuilderPrefetchTables, NyxMain.uaSett.SqlBuilderPrefetchViews, NyxMain.uaSett.SqlBuilderPrefetchProcedures);
            }
            else // if (strThrReturn.errMsg == null)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBConnectErr", new string[] { NyxMain.ConProfile.ProfileName, strThrReturn.DBException.Message.ToString() }),
                                            strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
                this._sS_lblStatusMsg.Text = "Error connecting " + NyxMain.ConProfile.ProfileName + ".";
                // activate gui again, special way cause of tabctrl
                this._ssProgress.Style = ProgressBarStyle.Blocks;
                this._ssProgress.Value = 0;
                this.Cursor = Cursors.Default;
                this._tsMain_QuickConnect.Enabled = true;
                // reinit sqlmultitabs
                foreach (TabPage tp in this._tcSql.TabPages)
                {
                    SqlTab smt = (SqlTab)tp.Tag;
                    smt.InitClasses(dbh);
                }
                CtrlConnectButtons(true);
            }
        }
        private void DbDisconnect()
        {
            // display thread
            CtrlAppDisplay("Disconnecting database " + NyxMain.DBName + "...");
            // starting thread
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.Disconnect;
            // start thread...
            DBThreadInit(strThrStart);
        }
        private void DbDisconnectDone(DBThreadReturnParam strThrReturn)
        {   // activate gui again
            CtrlAppDisplay(String.Empty);
            // equal if ok or not, we return to disconnected gui
            ResetGui();
            // db checkbox
            this._tsMain_cbChangeDb.Items.Clear();
            // window title
            this.Text = GuiHelper.GetResourceMsg("NS_DisconnectedHl");
            // further cleanup
            dbh = null;
            dbm = null;
            NyxMain.dbVersion = null;
            // contextmenus
            DisableContextMenu();
            // connectbuttons
            CtrlConnectButtons(true);
            // view menuscript
            CtrlViewMenuStrip(false);
            //
            this._msPrv.Enabled = false;
            this._msSql.Enabled = false;
            this._msOrcl.Enabled = false;
            this._msMySql.Enabled = false;
            this._msDbT.Enabled = false;
            // disable main tabcontrol
            this._tcMain.Enabled = false;
            // re-set save disconnect button
            this._tsMain_SaveQC.Enabled = false;
            // print errror message
            if (strThrReturn.DBException != null)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("DBDisconnectErr", new string[] { DBName, strThrReturn.DBException.Message.ToString() }),
                                                    strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
                this._sS_lblStatusMsg.Text = GuiHelper.GetResourceMsg("DBDisconnectErr", new string[] { DBName });
            }
            // set focus
            this._tsMain_cbProfiles.Focus();
        }
        #endregion

        #region Tabpage 1, SqlQuery & SqlBuilder & AutoCompletition
        /// <summary> public _msSql_EditData.Enabled </summary>
        public bool MenuSqlSqlEditData { get { return this._msSql_EditData.Enabled; } set { this._msSql_EditData.Enabled = value; } }
        /// <summary> public _msSql_UpdData.Enabled </summary>
        public bool MenuSqlUpdData { get { return this._msSql_UpdData.Enabled; } set { this._msSql_UpdData.Enabled = value; } }
        /// <summary> public _msSql_ExecSel.Enabled </summary>
        public bool MenuSqlExecSel { get { return this._msSql_ExecSel.Enabled; } set { this._msSql_ExecSel.Enabled = value; } }

        private void _msDb_ExecSql_Click(object sender, EventArgs e)
        {
            SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
            smt.ExecuteSql();
        }
        private void _msDb_ExecSel_Click(object sender, EventArgs e)
        {
            SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
            smt.ExecuteSelectedSql();
        }
        private void _msDb_ClearSql_Click(object sender, EventArgs e)
        {
            SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
            smt.ClearSqlQuery();
        }
        private void _msDbT_ShowAC_Click(object sender, EventArgs e)
        {
            SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
            smt.Select();
            smt.ACHotkeyActivation();
        }

        /// <summary>
        /// Create new SqlTabpage
        /// </summary>
        /// <param name="foreground">bring to front or not</param>
        public void SqlCreateNewTab(bool foreground)
        {   // init
            TabPage tp = new TabPage();
            SqlTab smt = new SqlTab(dbh);
            // format usercontrol
            smt.Dock = DockStyle.Fill;
            smt.ACUpdateListBox();
            // assign event handler
            smt.DBContentChangedEvent += new EventHandler<DBContentChangedEventArgs>(DBContentChanged);
            // format tabpage
            tp.Text = "unused";
            tp.Tag = smt;
            tp.UseVisualStyleBackColor = true;
            // add tabpage and usercontrol to tabpage
            this._tcSql.TabPages.Add(tp);
            this._tcSql.TabPages[this._tcSql.TabPages.Count - 1].Controls.Add(smt);
            if (foreground)
                this._tcSql.SelectedIndex = this._tcSql.TabPages.Count - 1;
        }
        /// <summary>
        /// Close currently open SqlTabpage
        /// </summary>
        public void SqlCloseTab()
        {
            if (this._tcSql.TabPages.Count > 1)
                this._tcSql.TabPages.Remove(this._tcSql.SelectedTab);
        }
        /// <summary>
        /// Change properties of the currently selected SqlTabpage
        /// </summary>
        /// <param name="name">name to set</param>
        /// <param name="tooltip">tooltip to set</param>
        public void SqlSetTab(string name, string tooltip)
        {
            this._tcSql.SelectedTab.Text = name;
            this._tcSql.SelectedTab.ToolTipText = tooltip;
        }
        /// <summary>
        /// Activate/show sql builder treeview
        /// </summary>
        public void SqlBldrActivate()
        {
            if (this._splitSqlBldr.Panel2Collapsed)
            {   // gui settings
                this._splitSqlBldr.Panel2Collapsed = false;
                this._trvSqlBldr.Focus();
                // init sqlbuilder
                bool gettables = false, getviews = false, getprocedures = false;
                if (this._trvSqlBldr.Nodes["Tables"].Nodes.Count == 0 && NyxMain.uaSett.SqlBuilderPrefetchTables)
                    gettables = true;
                if (this._trvSqlBldr.Nodes["Views"].Nodes.Count == 0 && NyxMain.uaSett.SqlBuilderPrefetchViews)
                    getviews = true;
                if (this._trvSqlBldr.Nodes["Procedures"].Nodes.Count == 0 && NyxMain.uaSett.SqlBuilderPrefetchProcedures)
                    getprocedures = true;
                DbContentInit(gettables, getviews, getprocedures);
            }
            else
            {
                this._splitSqlBldr.Panel2Collapsed = true;
                SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                smt.Select();
            }
        }
        /// <summary>
        /// Searches SqlBuilderTreeview for the current table and returns the tablefields if found
        /// </summary>
        /// <param name="table">table to get fields from</param>
        /// <returns>string[] fields</returns>
        public string[] SqlBldrGetTableFields(string table)
        {   
            string[] fields = new string[0];
            // validate
            if (table == null)
                return fields;
            // search the treenode for the table 
            string srchStr = "TABLES\\" + table.ToUpper(NyxMain.NyxCI);
            foreach (TreeNode tn in this._trvSqlBldr.Nodes["Tables"].Nodes)
            {
                if (String.Compare(tn.FullPath.ToString(), srchStr, true, NyxMain.nyxCI) == 0)
                {   // we got fields
                    if (tn.Nodes.Count > 0)
                    {   // init string[]
                        fields = new string[tn.Nodes.Count];
                        // fill string[]
                        for (int i = 0; i < tn.Nodes.Count; i++)
                            fields[i] = tn.Nodes[i].Name;
                    }
                    // break foreach
                    break;
                }
            }
            return fields;
        }

        private void _SqlBldr_Click(object sender, EventArgs e)
        {
            SqlBldrActivate();
        }
        private void _cmSqlBldr_Insert_Click(object sender, EventArgs e)
        {
            SqlBldr_Insert(this._trvSqlBldr.SelectedNode);
        }
        private void _cmSqlBldr_Refresh_Click(object sender, EventArgs e)
        {
            GetTables();
        }
        private void _trvSqlBldr_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                this._trvSqlBldr.SelectedNode = this._trvSqlBldr.GetNodeAt(e.X, e.Y);
        }
        private void _trvSqlBldr_NodeMouseDoubleClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            SqlBldr_Insert(this._trvSqlBldr.SelectedNode);
        }
        private void _trvSqlBldr_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SqlBldr_Insert(this._trvSqlBldr.SelectedNode);
        }
        private void SqlBldr_Insert(TreeNode tn)
        {
            if (tn != null
                && tn.Name != "Tables"
                && tn.Name != "Views"
                && tn.Name != "Procedures"
                && tn.Name != "reservedWords")
            {
                SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                smt.Select();
                smt.AddSqlBldr2SqlRtb(tn.Name);
            }
        }
        private void _trvSqlBldr_AfterExpand(object sender, TreeViewEventArgs e)
        {   // check if we have to get them?
            if (e.Node != null && e.Node.Nodes.Count == 1
                && e.Node.Nodes[0].Text == GuiHelper.GetResourceMsg("SBReceivingFields"))
            {   // thread start parameters
                DBThreadStartParam strThrStart = new DBThreadStartParam();
                strThrStart.Param1 = e.Node.Text;
                strThrStart.Obj = e.Node;
                strThrStart.ThrAction = DatabaseAction.SqlBldrGetTableDescribe;
                // start thread
                DBThreadInit(strThrStart);
            }
        }
        private void SqlBldrGetTableFullDescDone(DBThreadReturnParam strThrReturn)
        {
            if (strThrReturn.DBException == null)
            {   // find treenode to add fields
                TreeNode selTn = (TreeNode)strThrReturn.Data2;
                this._trvSqlBldr.BeginUpdate();
                selTn.Nodes.Clear();
                foreach (TableDetails strTblDesc in (Queue<TableDetails>)strThrReturn.Data1)
                    selTn.Nodes.Add(GuiHelper.SqlBldrGetTableDescTN(strTblDesc));
                // endupdate treeview
                this._trvSqlBldr.EndUpdate();
            }
            else
            {   // err
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrSqlBuilderGetTables", new string[] { strThrReturn.DBException.Message.ToString() }),
                            strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            }
        }

        private void DbContentInit(bool getTables, bool getViews, bool getProcedures)
        {
            if (getTables || getViews || getProcedures)
            {   // disable gui
                CtrlAppDisplay("Prefetching database content...");
                // cleanup gui
                if (getTables)
                {
                    this._trvSqlBldr.Nodes["Tables"].Nodes.Clear();
                    this._lbTables.Items.Clear();
                    tblFilterList.Clear();
                    this._tsTables_curTable.Text = String.Empty;
                    this._lvTableDesc.Items.Clear();
                    this._lvIndexes.Items.Clear();
                    this._lvIndexDesc.Items.Clear();
                    // also reset preview
                    this._lvPrvObjects.Items.Clear();
                    prvFilterList.Clear();
                    this._dgPreview.DataSource = null;
                    this._tsPrvDta_curObject.Text = String.Empty;
                    this._dgPreview_Filter.Rows.Clear();
                }
                if (getProcedures)
                {
                    this._lbPrcdrs.Items.Clear();
                    this._lvPrcdr_Details.Items.Clear();
                    this._rtbPrcdr_CreateStm.Text = String.Empty;
                    this._trvSqlBldr.Nodes["Procedures"].Nodes.Clear();
                    this._tsPrcdrs_curProcedure.Text = String.Empty;
                    prcdrFilterList.Clear();
                }
                if (getViews)
                {
                    this._lbViews.Items.Clear();
                    this._lvView_Details.Items.Clear();
                    this._rtbView_CreateStm.Text = String.Empty;
                    this._trvSqlBldr.Nodes["Views"].Nodes.Clear();
                    viewFilterList.Clear();
                    // also reset preview
                    this._lvPrvObjects.Items.Clear();
                    prvFilterList.Clear();
                    this._dgPreview.DataSource = null;
                    this._tsPrvDta_curObject.Text = String.Empty;
                    this._dgPreview_Filter.Rows.Clear();
                }
                
                DBThreadStartParam strThrStart = new DBThreadStartParam();
                strThrStart.ThrAction = DatabaseAction.DBContentInit;
                bool[] options = new bool[] { getTables, getViews, getProcedures };
                strThrStart.Obj = options;
                // further check, if db version supports views/procedures
                if (NyxMain.ConProfile.ProfileDBType == DBType.MySql)
                {
                    int iVnr;
                    if (Int32.TryParse(NyxMain.dbVersion.Substring(0, 1), out iVnr) && iVnr < 5)
                    {
                        options[1] = false;
                        options[2] = false;
                    }
                }
                // start thread
                DBThreadInit(strThrStart);
            }
        }
        private void DbContentInitDone(DBThreadReturnParam strThrReturn)
        {   // chkup
            if (strThrReturn.DBException == null)
            {   // unbox options ([0] == tables, [1] == views, [2] == procedures)
                bool[] retOpt = (bool[])strThrReturn.Data1;
                // check if table refresh
                if (retOpt[0])
                {   // update
                    this._trvSqlBldr.BeginUpdate();
                    this._lbTables.BeginUpdate();
                    foreach (string table in strThrReturn.GQString1)
                    {
                        this._trvSqlBldr.Nodes["Tables"].Nodes.Add(GuiHelper.SqlBldrGetTableTN(table));
                        this._lbTables.Items.Add(table);
                        tblFilterList.Add(table);
                    }
                    this._lbTables.EndUpdate();
                    this._trvSqlBldr.EndUpdate();
                }
                if (retOpt[1])
                {   // update
                    this._trvSqlBldr.BeginUpdate();
                    this._lbViews.BeginUpdate();
                    foreach (string view in strThrReturn.GQString2)
                    {
                        this._trvSqlBldr.Nodes["Views"].Nodes.Add(view, view);
                        this._lbViews.Items.Add(view);
                        viewFilterList.Add(view);
                    }
                    this._trvSqlBldr.EndUpdate();
                    this._lbViews.EndUpdate();
                }
                if (retOpt[2])
                {   // update
                    this._lbPrcdrs.BeginUpdate();
                    this._trvSqlBldr.BeginUpdate();
                    foreach (string prcdr in strThrReturn.GQString3)
                    {
                        this._trvSqlBldr.Nodes["Procedures"].Nodes.Add(prcdr, prcdr);
                        this._lbPrcdrs.Items.Add(prcdr);
                        prcdrFilterList.Add(prcdr);
                    }
                    this._lbPrcdrs.EndUpdate();
                    this._trvSqlBldr.EndUpdate();
                }
                // reinit auticompletition
                if (NyxMain.uaSett.AutoCompletition)
                    ACPrepareListBox();
            }
            else
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrSqlBuilderInit"), 
                    strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            // activate gui again, set focues
            CtrlAppDisplay(String.Empty);
            // quickconnect
            if (this._tsMain_cbProfiles.SelectedItem.ToString() == "QuickConnect")
                this._tsMain_SaveQC.Enabled = true;
            else this._tsMain_SaveQC.Enabled = false;

            SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
            smt.Select();
        }

        private void ACPrepareListBox()
        {
            NyxMain.lbAutoComplete.Items.Clear();
            if (NyxMain.UASett.AutoCompletitionUseTables)
                foreach (TreeNode tn in this._trvSqlBldr.Nodes["Tables"].Nodes)
                    NyxMain.lbAutoComplete.Items.Add(new NyxAutoComplete.ACListBoxItem(tn.Name, 0));

            if (NyxMain.UASett.AutoCompletitionUseViews)
                foreach (TreeNode tn in this._trvSqlBldr.Nodes["Views"].Nodes)
                    NyxMain.lbAutoComplete.Items.Add(new NyxAutoComplete.ACListBoxItem(tn.Name, 1));

            if (NyxMain.UASett.AutoCompletitionUseProcedures)
                foreach (TreeNode tn in this._trvSqlBldr.Nodes["Procedures"].Nodes)
                    NyxMain.lbAutoComplete.Items.Add(new NyxAutoComplete.ACListBoxItem(tn.Name, 2));

            if (NyxMain.UASett.AutoCompletitionUseWords)
                foreach (TreeNode tn in this._trvSqlBldr.Nodes["reservedWords"].Nodes)
                    NyxMain.lbAutoComplete.Items.Add(new NyxAutoComplete.ACListBoxItem(tn.Name, 3));

            if (NyxMain.lbAutoComplete.Items.Count > 0)
            {
                ACSortListBox();
                SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                smt.ACUpdateListBox();
            }
        }
        private static void ACSortListBox()
        {
            string[] items = new string[NyxMain.lbAutoComplete.Items.Count];
            Hashtable tmpHT = new Hashtable();
            // add to array (for sorting) and to 
            for (int i = 0; i < NyxMain.lbAutoComplete.Items.Count; i++)
            {
                NyxAutoComplete.ACListBoxItem tmpItem = (NyxAutoComplete.ACListBoxItem)NyxMain.lbAutoComplete.Items[i];
                // add array
                items[i] = tmpItem.Text;
                if (!tmpHT.Contains(tmpItem.Text))
                    tmpHT.Add(tmpItem.Text, tmpItem.ImageIndex);
            }
            // sort
            Array.Sort(items);
            // add again to listbox
            NyxMain.lbAutoComplete.Items.Clear();
            foreach (string item in items)
            {
                if (tmpHT.Contains(item))
                {
                    int imgIdx = -1;
                    if (Int32.TryParse(tmpHT[item].ToString(), out imgIdx))
                        NyxMain.lbAutoComplete.Items.Add(new NyxAutoComplete.ACListBoxItem(item, imgIdx));
                    else
                        NyxMain.lbAutoComplete.Items.Add(new NyxAutoComplete.ACListBoxItem(item));
                }
                else
                    NyxMain.lbAutoComplete.Items.Add(new NyxAutoComplete.ACListBoxItem(item));
            }
        }
        #endregion

        #region Tabpage 2, Preview
        /// <summary> context menue strips </summary>
        private void _cmPrvDtaCp_Field_Click(object sender, EventArgs e)
        {
            if (this._dgPreview.CurrentCell != null)
                Clipboard.SetText(this._dgPreview.CurrentCell.Value.ToString());
        }
        private void _cmPrvDtaCp_Cells_Click(object sender, EventArgs e)
        {
            ExportDataGridView.Cells2Clipboard(this._dgPreview.SelectedCells, this._tsPrvDta_curObject.Text.ToString());
        }
        private void _cmPrvDtaCp_Table_Click(object sender, EventArgs e)
        {
            if (dsPreview.Tables.Count == 1)
                ExportDataGridView.Table2Clipboard(dsPreview.Tables[0], this._tsPrvDta_curObject.Text.ToString());
        }
        private void _cmPrvDtaSv_Cells_Click(object sender, EventArgs e)
        {
            ExportDataGridView.Cells2File(this._dgPreview.SelectedCells, this._tsPrvDta_curObject.Text.ToString());
        }
        private void _cmPrvDtaSv_Table_Click(object sender, EventArgs e)
        {
            if (dsPreview.Tables.Count == 1)
                ExportDataGridView.Table2File(dsPreview.Tables[0], this._tsPrvDta_curObject.Text.ToString());
        }
        private void _cmPrvFilter_FieldUp_Click(object sender, EventArgs e)
        {   // got selection?
            if (this._dgPreview_Filter.SelectedRows.Count == 1)
            {   // get selection
                DataGridViewSelectedRowCollection dgvsrColl = this._dgPreview_Filter.SelectedRows;
                // get first selected row index
                int idx = this._dgPreview_Filter.Rows.IndexOf(dgvsrColl[0]);
                if (idx - 1 >= 0)
                {   // helpvar
                    DataGridViewRow dgvr = this._dgPreview_Filter.Rows[idx];
                    this._dgPreview_Filter.Rows.RemoveAt(idx);
                    this._dgPreview_Filter.Rows.Insert(idx - 1, dgvr);
                    // set selection
                    this._dgPreview_Filter.CurrentCell = this._dgPreview_Filter.Rows[idx - 1].Cells[0];
                    this._dgPreview_Filter.Rows[idx].Selected = false;
                    this._dgPreview_Filter.Rows[idx - 1].Selected = true;
                }
            }
        }
        private void _cmPrvFilter_FieldDown_Click(object sender, EventArgs e)
        {   // got selection?
            if (this._dgPreview_Filter.SelectedRows.Count == 1)
            {   // get selection
                DataGridViewSelectedRowCollection dgvsrColl = this._dgPreview_Filter.SelectedRows;
                // get first selected row index
                int idx = this._dgPreview_Filter.Rows.IndexOf(dgvsrColl[0]);
                if (idx + 1 < this._dgPreview_Filter.Rows.Count)
                {   // helpvar
                    DataGridViewRow dgvr = this._dgPreview_Filter.Rows[idx];
                    this._dgPreview_Filter.Rows.RemoveAt(idx);
                    this._dgPreview_Filter.Rows.Insert(idx + 1, dgvr);
                    // set selection
                    this._dgPreview_Filter.CurrentCell = this._dgPreview_Filter.Rows[idx + 1].Cells[0];
                    this._dgPreview_Filter.Rows[idx].Selected = false;
                    this._dgPreview_Filter.Rows[idx + 1].Selected = true;
                }
            }
        }
        private void _cmPrvFilter_DupField_Click(object sender, EventArgs e)
        {   // duplicate roles
            if (this._dgPreview_Filter.SelectedRows.Count >= 1)
            {
                foreach (DataGridViewRow dgvr in this._dgPreview_Filter.SelectedRows)
                {
                    this._dgPreview_Filter.Rows.InsertCopy(dgvr.Index, dgvr.Index + 1);
                    foreach (DataGridViewCell dgvc in dgvr.Cells)
                        this._dgPreview_Filter.Rows[dgvr.Index + 1].Cells[dgvc.ColumnIndex].Value = dgvc.Value;
                }
            }
        }
        private void _cmPrvFilter_RmDupField_Click(object sender, EventArgs e)
        {   // check if row is duplicate
            if (this._dgPreview_Filter.SelectedRows.Count == 1)
            {
                int foundCnt = 0;
                string fieldName = this._dgPreview_Filter.SelectedRows[0].Cells[vField.Index].Value.ToString();
                foreach (DataGridViewRow dgvr in this._dgPreview_Filter.Rows)
                {
                    if (dgvr.Cells[vField.Index].Value.ToString() == fieldName)
                        foundCnt++;
                }
                // found more than 1?
                if (foundCnt > 1)
                    this._dgPreview_Filter.Rows.RemoveAt(this._dgPreview_Filter.SelectedRows[0].Index);
            }
        }
        private void _cmPrvObj_Actions(object sender, EventArgs e)
        {   /*  We start an Action here, which first starts up at showing the ListMessageBox
             *  (perhaps with additional options, dependant on the action and database type, check the ListMessageBoxSource)
             *  and afterwards the TableThreadHelper
             */
            if (this._lvPrvObjects.SelectedItems.Count == 1)
            {
                string[] items = new string[1] { this._lvPrvObjects.SelectedItems[0].Text.ToString() };
                    ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
                    if (tsmi == this._cmPrvObj_DropTable)
                        ExecuteTableAction(tsmi.Text, items, TableAction.TableDrop);
                    else if (tsmi == this._cmPrvObj_Truncate)
                        ExecuteTableAction(tsmi.Text, items, TableAction.TableTruncate);
                    else if (tsmi == this._cmPrvObjDiag_Analyze)
                        ExecuteTableAction(tsmi.Text, items, TableAction.TableAnalyze);
                    else if (tsmi == this._cmPrvObjDiag_Check)
                        ExecuteTableAction(tsmi.Text, items, TableAction.TableCheck);
                    else if (tsmi == this._cmPrvObjDiag_Optimize)
                        ExecuteTableAction(tsmi.Text, items, TableAction.TableOptimize);
                    else if (tsmi == this._cmPrvObjDiag_Repair)
                        ExecuteTableAction(tsmi.Text, items, TableAction.TableRepair);
                    else if (tsmi == this._cmPrvObj_DropView)
                        ExecuteTableAction(tsmi.Text, items, TableAction.ViewDrop);
            }
        }
        /// <summary> toolstrips </summary>
        private void _tsPrvObj_Filter_TextChanged(object sender, EventArgs e)
        {
            this._lvPrvObjects.BeginUpdate();
            this._lvPrvObjects.Items.Clear();
            System.Collections.ObjectModel.Collection<PreviewObject> cpo =
                GuiHelper.FilterList(this._tsPrvObj_Filter.Text, prvFilterList);
            foreach (PreviewObject po in cpo)
            {
                this._lvPrvObjects.Items.Add(GuiHelper.BuildListViewItem(po.Name, po.Type));
            }
            this._lvPrvObjects.EndUpdate();
        }
        private void _tsPrvDta_RowsPerPage_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                GetFirstPreviewData(this._tsPrvDta_curObject.Text.ToString());
        }
        private void _tsPrvDta_ShowFilter_Click(object sender, EventArgs e)
        {
            if (this._splitPreviewDta.Panel2Collapsed)
                this._splitPreviewDta.Panel2Collapsed = false;
            else
                this._splitPreviewDta.Panel2Collapsed = true;
        }
        private void _tsPrvObj_Scope_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                GetPreviewObjects();
        }
        private void _lvPrvObj_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvPrvObjects, lvcsPrvObjects, e.Column);
        }
        private void _lvPrvObjects_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._lvPrvObjects.SelectedItems.Count == 0)
            {
                this._cmPrvObjDiag.Enabled = false;
                this._cmPrvObj_DropTable.Enabled = false;
                this._cmPrvObj_DropView.Enabled = false;
                this._cmPrvObj_Get.Enabled = false;
                this._cmPrvObj_Truncate.Enabled = false;
            }
            else
            {
                this._cmPrvObj_Get.Enabled = true;
                // check if view or table
                if (this._lvPrvObjects.SelectedItems[0].SubItems[1].Text.ToString() != "Table")
                {
                    this._cmPrvObjDiag.Enabled = false;
                    this._cmPrvObj_DropTable.Enabled = false;
                    this._cmPrvObj_Truncate.Enabled = false;
                    this._cmPrvObj_DropView.Enabled = true;
                }
                else
                {
                    this._cmPrvObjDiag.Enabled = true;
                    this._cmPrvObj_DropTable.Enabled = true;
                    this._cmPrvObj_Truncate.Enabled = true;
                    this._cmPrvObj_DropView.Enabled = false;
                }
            }
        }
        /// <summary> general gui events </summary>
        private void _preview_SetFilter_Click(object sender, EventArgs e)
        {   // disable gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("PreviewRetrieveData"));
            this._dgPreview_Filter.EndEdit();
            // init vars
            Hashtable htWhereStm = new Hashtable();
            System.Text.StringBuilder sbColumns = new System.Text.StringBuilder(),
                                      sbWhereCl = new System.Text.StringBuilder();
            // parse the datagridview
            foreach (DataGridViewRow dgvr in this._dgPreview_Filter.Rows)
            {   // check select fields
                if (dgvr.Cells[vSelect.Index].Value.ToString() == "true")
                {
                    string field = dgvr.Cells[vField.Index].Value.ToString();
                    if (sbColumns.Length == 0)
                        sbColumns.Append(field);
                    else
                        sbColumns.Append(String.Format(NyxMain.NyxCI, ",{0}", field));
                }
                // check filter and group
                if (dgvr.Cells[vFilter.Index].Value.ToString().Length > 0
                    && dgvr.Cells[vValue.Index].Value.ToString().Length > 0
                    && dgvr.Cells[vGroup.Index].Value.ToString().Length > 0
                    && htWhereStm != null)
                {   // init filtervalue
                    string[] fin = new string[2] { String.Empty, String.Empty };
                    // get the key and filtervalues
                    string key = dgvr.Cells[vGroup.Index].Value.ToString();
                    fin[0] = String.Format(NyxMain.NyxCI, "{0} {1} {2}", dgvr.Cells[vField.Index].Value.ToString(),
                                        dgvr.Cells[vFilter.Index].Value.ToString(), dgvr.Cells[vValue.Index].Value.ToString());
                    // check relation (AND/OR...)
                    fin[1] = "AND";
                    if (dgvr.Index > 0)
                    {   // check
                        if (dgvr.Cells[vLOperator.Index].Value != null)
                            fin[1] = dgvr.Cells[vLOperator.Index].Value.ToString();
                    }

                    // check if we allready added this key to the arraylist
                    if (!htWhereStm.ContainsKey(key))
                    {   // create new arraylist and add to hashtable
                        List<string[]> al = new List<string[]>();
                        al.Add(fin);
                        htWhereStm.Add(key, al);
                    }
                    else
                    {   // add to arraylist from hashtable
                        List<string[]> al = (List<string[]>)htWhereStm[key];
                        al.Add(fin);
                    }
                }
            }
            // build where clause
            if (htWhereStm.Count > 0)
            {   // parse the datagridview to get the correct order of the groups
                foreach (DataGridViewRow dgvr in this._dgPreview_Filter.Rows)
                {   // have we got a this group?
                    string htKey = dgvr.Cells[vGroup.Index].Value.ToString();
                    if (htWhereStm.ContainsKey(htKey))
                    {
                        // init filter
                        List<string[]> alFilter = (List<string[]>)htWhereStm[htKey];
                        // we got a group with more than 2 members
                        if (alFilter.Count > 1 && htKey != "0")
                        {
                            string tmpCl = String.Empty, firstLOp = String.Empty;
                            foreach (string[] clause in alFilter)
                            {   // set first logical operator
                                if (firstLOp.Length == 0)
                                    firstLOp = clause[1];
                                // parse other filtervalues
                                if (tmpCl.Length == 0)
                                    tmpCl = String.Format(NyxMain.NyxCI, "({0}", clause[0]);
                                else
                                    tmpCl = String.Format(NyxMain.NyxCI, "{0} {1} {2}", tmpCl, clause[1], clause[0]);
                            }
                            // add to wherecl
                            if (sbWhereCl.Length == 0)
                                sbWhereCl.Append(String.Format(NyxMain.NyxCI, "{0})", tmpCl));
                            else
                                sbWhereCl.Append(String.Format(NyxMain.NyxCI, " {0} {1})", firstLOp, tmpCl));
                        }
                        else
                        {
                            string tmpCl = String.Empty, firstLOp = String.Empty;
                            foreach (string[] clause in alFilter)
                            {   // set first logical operator
                                if (firstLOp.Length == 0)
                                    firstLOp = clause[1];
                                // parse other filtervalues
                                if (tmpCl.Length == 0)
                                    tmpCl = String.Format(NyxMain.NyxCI, "{0}", clause[0]);
                                else
                                    tmpCl = String.Format(NyxMain.NyxCI, "{0} {1} {2}", tmpCl, clause[1], clause[0]);
                            }
                            // add to wherecl
                            if (sbWhereCl.Length == 0)
                                sbWhereCl.Append(tmpCl);
                            else
                                sbWhereCl.Append(String.Format(NyxMain.NyxCI, " {0} {1}", firstLOp, tmpCl));
                        }
                        // remove from hashtable
                        htWhereStm.Remove(htKey);
                    }
                }
            }
            // check limit
            int rowsPerPage;
            if (this._tsPrvDta_RowsPerPage.Text.ToString() != "no Limit")
                Int32.TryParse(this._tsPrvDta_RowsPerPage.Text.ToString(), out rowsPerPage);
            else
                rowsPerPage = -1;
            // set previewquery
            prvQry = new PreviewQuery(this._tsPrvDta_curObject.Text.ToString());
            if (sbWhereCl.Length > 0)
                prvQry.WhereStm = sbWhereCl.ToString();
            if (sbColumns.Length > 0)
                prvQry.Columns = sbColumns.ToString();
            else
                prvQry.Columns = "*";
            prvQry.Table = this._tsPrvDta_curObject.Text.ToString();
            prvQry.RowsPerPage = rowsPerPage;
            // reset gui
            this._tsPrvDta_curObject.Text = String.Empty;
            this._dgPreview.DataSource = null;
            SetPreviewButtons(true);
            // start thread
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.PreviewInitFilter;
            strThrStart.Obj = prvQry;
            DBThreadInit(strThrStart);
        }
        private void _preview_ResetFilter_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dgvr in this._dgPreview_Filter.Rows)
            {
                dgvr.Cells[1].Value = true;
                dgvr.Cells[2].Value = String.Empty;
                dgvr.Cells[3].Value = String.Empty;
                dgvr.Cells[4].Value = String.Empty;
                dgvr.Cells[5].Value = 0;
            }
        }
        private void _previewInitPreview_Click(object sender, EventArgs e)
        {   // set gui
            if(this._lvPrvObjects.SelectedItems.Count > 0)
                GetFirstPreviewData(this._lvPrvObjects.SelectedItems[0].Text.ToString());
        }
        private void _previewNext_Click(object sender, EventArgs e)
        {   // got selected item?
            if (this._tsPrvDta_curObject.Text.Length < 1)
                return;
            // ctrl progress
            CtrlAppDisplay(GuiHelper.GetResourceMsg("PreviewRetrieveData"));
            // cleanup
            this._dgPreview.DataSource = null;
            SetPreviewButtons(true);
            // init vars
            int curPage, rowStart, rowsPerPage;
            Int32.TryParse(this._tsPrvDta_lblActPg.Text, out curPage);
            Int32.TryParse(this._tsPrvDta_RowsPerPage.Text, out rowsPerPage);
            // start from row?
            rowStart = curPage * rowsPerPage;
            // set previewquery // new PreviewQuery(this._tsPrvDta_curObject.Text.ToString(), rowStart, rowsPerPage)
            prvQry.RowStart = rowStart;
            prvQry.RowsPerPage = rowsPerPage;
            // set thread parameter
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.PrvGetNext;
            strThrStart.Obj = prvQry; 
            // starting thread
            DBThreadInit(strThrStart);
        }
        private void _previewPrev_Click(object sender, EventArgs e)
        {   // got selected item?
            if (this._tsPrvDta_curObject.Text.Length < 1)
                return;
            // control gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("PreviewRetrieveData"));
            // cleanup
            this._dgPreview.DataSource = null;
            SetPreviewButtons(true);
            // init vars
            int curPage, pageCnt, rowStart, rowsPerPage;
            Int32.TryParse(this._tsPrvDta_lblActPg.Text, out curPage);
            Int32.TryParse(this._tsPrvDta_lblPgCnt.Text, out pageCnt);
            Int32.TryParse(this._tsPrvDta_RowsPerPage.Text, out rowsPerPage);
            // start from row?
            if (curPage - 1 == 1)
            {
                curPage--;
                rowStart = 0;
            }
            else
            {
                curPage--;
                rowStart = curPage * rowsPerPage - rowsPerPage;
            }
            // set previewquery // new PreviewQuery(this._tsPrvDta_curObject.Text.ToString(), rowStart, rowsPerPage)
            prvQry.RowStart = rowStart;
            prvQry.RowsPerPage = rowsPerPage;
            // set threadparameter
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.PrvGetPrev;
            strThrStart.Obj = prvQry; 
            // start thread
            DBThreadInit(strThrStart);
        }
        private void _previewEnd_Click(object sender, EventArgs e)
        {   // got selected item?
            if (this._tsPrvDta_curObject.Text.Length < 1)
                return;
            // control gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("PreviewRetrieveData"));
            // cleanup
            this._dgPreview.DataSource = null;
            SetPreviewButtons(true);
            // init vars
            int curPage, pageCnt, rowsPerPage, rowStart;
            Int32.TryParse(this._tsPrvDta_lblActPg.Text, out curPage);
            Int32.TryParse(this._tsPrvDta_lblPgCnt.Text, out pageCnt);
            Int32.TryParse(this._tsPrvDta_RowsPerPage.Text, out rowsPerPage);
            // row to start from
            rowStart = pageCnt * rowsPerPage - rowsPerPage;
            // calc new current page
            curPage = pageCnt;
            // set previewquery // new PreviewQuery(this._tsPrvDta_curObject.Text.ToString(), rowStart, rowsPerPage)
            prvQry.RowStart = rowStart;
            prvQry.RowsPerPage = rowsPerPage;
            // set threadparameter
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.PreviewGetLast;
            strThrStart.Obj = prvQry;
            // start thread
            DBThreadInit(strThrStart);
        }
        private void _previewRefreshObjects_Click(object sender, EventArgs e)
        {
            GetPreviewObjects();
        }
        private void _previewRefreshPreview_Click(object sender, EventArgs e)
        {
            GetFirstPreviewData(this._tsPrvDta_curObject.Text.ToString());
        }
        private void _previewEditData_Click(object sender, EventArgs e)
        {
            EditPrvDataGrid(true);
        }
        private void _previewUpdData_Click(object sender, EventArgs e)
        {
            UpdatePrvDGrid();
        }
        /// <summary> Set menustrip/contextmenustrip/toolstrip to enable/disable depending on error </summary>
        /// <param name="error">true/false</param>
        private void SetPreviewButtons(bool error)
        {   // error occured
            if (error)
            {   // reset datagrid, current tabletext and dataset
                EditPrvDataGrid(false);
                this._dgPreview.DataSource = null;
                if(dsPreview != null)
                    dsPreview.Reset();
                // set gui
                this._tsPrvDta_Edit.Enabled = false;
                this._cmPrvDta_Edit.Enabled = false;
                this._msPrvDta_Edit.Enabled = false;
                this._tsPrvDta_Refresh.Enabled = false;
                this._cmPrvDta_Refresh.Enabled = false;
                this._msPrvDta_Refresh.Enabled = false;
                this._cmPrvDtaCp.Enabled = false;
                this._cmPrvDtaSv.Enabled = false;
            }
            else
            {
                this._tsPrvDta_Edit.Enabled = true;
                this._cmPrvDta_Edit.Enabled = true;
                this._msPrvDta_Edit.Enabled = true;
                this._tsPrvDta_Update.Enabled = false;
                this._cmPrvDta_Update.Enabled = false;
                this._msPrv_Update.Enabled = false;
                this._tsPrvDta_Refresh.Enabled = true;
                this._cmPrvDta_Refresh.Enabled = true;
                this._msPrvDta_Refresh.Enabled = true;
                if (this._dgPreview.Rows.Count > 0)
                {
                    this._cmPrvDtaCp.Enabled = true;
                    this._cmPrvDtaSv.Enabled = true;
                }
                else
                {
                    this._cmPrvDtaCp.Enabled = false;
                    this._cmPrvDtaSv.Enabled = false;
                }
            }
        }
        /// <summary> preview object functions </summary>
        private void GetPreviewObjects()
        {   // gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("PreviewRetrieveData"));
            // cleanup gui
            prvFilterList.Clear();
            this._tsPrvObj_Filter.Text = String.Empty;
            this._lvPrvObjects.Items.Clear();
            this._dgPreview.DataSource = null;
            this._dgPreview_Filter.Rows.Clear();
            this._tsPrvDta_curObject.Text = String.Empty;
            // thread parameter
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.GetPreviewObjects;
            // get only user objects (false) or all objects (true)
            if (this._tsPrvObj_Scope.SelectedIndex == 0)
                strThrStart.Obj = (object)true;
            else strThrStart.Obj = (object)false;
            // start thread
            DBThreadInit(strThrStart);
        }
        private void GetPreviewObjectsDone(DBThreadReturnParam strThrReturn)
        {   // parse and add into listbox
            if (strThrReturn.DBException == null)
            {
                if (strThrReturn.Data1 is List<PreviewObject>)
                {
                    List<PreviewObject> lpo = (List<PreviewObject>)strThrReturn.Data1;
                    this._lvPrvObjects.BeginUpdate();
                    foreach (PreviewObject po in lpo)
                    {
                        this._lvPrvObjects.Items.Add(GuiHelper.BuildListViewItem(po.Name, po.Type));
                        prvFilterList.Add(new PreviewObject(po.Name, po.Type));
                    }
                    this._lvPrvObjects.EndUpdate();
                }
            }
            else
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            // re-set gui
            CtrlAppDisplay(String.Empty);
            this._lvPrvObjects.Focus();
        }
        /// <summary> preview  data options </summary>
        private void GetFirstPreviewData(string table)
        {   // check if we got a table
            if (table.Length < 1)
                return;
            // gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("PreviewRetrieveData"));
            this._tsPrvDta_curObject.Text = String.Empty;
            this._dgPreview.DataSource = null;
            this._dgPreview_Filter.Rows.Clear();
            SetPreviewButtons(true);
            // check limit
            int rowsPerPage;
            if (this._tsPrvDta_RowsPerPage.Text.ToString() != "no Limit")
                Int32.TryParse(this._tsPrvDta_RowsPerPage.Text.ToString(), out rowsPerPage);
            else
                rowsPerPage = -1;
            // init previewquery
            prvQry = new PreviewQuery(table, rowsPerPage);
            // threadstart
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.PreviewInit;
            strThrStart.Obj = prvQry;
            // start thread
            DBThreadInit(strThrStart);
        }
        private void GetFirstPreviewDataDone(DBThreadReturnParam strThrReturn)
        {   // cast returned vars  data1 == tablename, data1 == queue<tabledetails>
            if (strThrReturn.DBException == null)
            {   // tabledetails
                if (strThrReturn.GQString1.Count > 0)
                {
                    foreach (string table in strThrReturn.GQString1)
                        this._dgPreview_Filter.Rows.Add(table, "true", String.Empty, String.Empty, String.Empty, 0);

                    this._tsPrvDta_ShowFilter.Enabled = true;
                    this._tsPrvDta_ResetFilter.Enabled = true;
                    this._tsPrvDta_SetFilter.Enabled = true;
                }
                else if(strThrReturn.ThrAction != DatabaseAction.PreviewInitFilter)
                {
                    this._tsPrvDta_ShowFilter.Enabled = false;
                    this._tsPrvDta_ResetFilter.Enabled = false;
                    this._tsPrvDta_SetFilter.Enabled = false;
                    this._splitPreviewDta.Panel2Collapsed = true;
                }
                // setting rowcount
                this._tsPrvDta_lblRowCnt.Text = strThrReturn.AffRows.ToString(nyxCI);
                // set new datasource
                dsPreview = strThrReturn.DS;
                this._dgPreview.DataSource = dsPreview.Tables[0];
                this._tsPrvDta_curObject.Text = prvQry.Table;
                // setting pagedisplay
                this._tsPrvDta_lblActRows.Text = "1 - " + this._tsPrvDta_RowsPerPage.Text;
                this._tsPrvDta_lblActPg.Text = "1";
                // calculating all pages
                double dtmp0;
                Double.TryParse(this._tsPrvDta_RowsPerPage.Text, out dtmp0);
                double pages = Math.Ceiling(strThrReturn.AffRows / dtmp0);
                // setting all pages
                this._tsPrvDta_lblPgCnt.Text = Convert.ToString(pages, nyxCI);
                // enable/disable buttons
                if (pages > 1)
                {
                    this._tsPrvDta_End.Enabled = true;
                    this._tsPrvDta_Next.Enabled = true;
                }
                else
                {
                    this._tsPrvDta_End.Enabled = false;
                    this._tsPrvDta_Next.Enabled = false;
                }
                this._tsPrvDta_Start.Enabled = false;
                this._tsPrvDta_Prev.Enabled = false;
                // set query
                this._dgPreview.Tag = strThrReturn.Query;
                SetPreviewButtons(false);
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
                SetPreviewButtons(true);
            }
            CtrlAppDisplay(String.Empty);
            this._lvPrvObjects.Focus();
        }
        private void GetPreviewDataDone(DBThreadReturnParam strThrReturn)
        {
            if (strThrReturn.DBException == null)
            {   // set new datasource
                dsPreview = strThrReturn.DS;
                this._dgPreview.DataSource = dsPreview.Tables[0];
                // init vars
                int pagecnt, curpage, rowspage;
                Int32.TryParse(this._tsPrvDta_lblPgCnt.Text, out pagecnt);
                Int32.TryParse(this._tsPrvDta_lblActPg.Text, out curpage);
                Int32.TryParse(this._tsPrvDta_RowsPerPage.Text, out rowspage);
                // increase current page
                switch (strThrReturn.ThrAction)
                {
                    case DatabaseAction.PrvGetNext:
                        curpage++;
                        break;
                    case DatabaseAction.PreviewGetLast:
                        curpage = pagecnt;
                        break;
                    case DatabaseAction.PrvGetPrev:
                        curpage--;
                        break;
                }
                // gui settings
                this._tsPrvDta_lblActRows.Text = (curpage * rowspage - rowspage) + " - " + (curpage * rowspage);
                this._tsPrvDta_lblActPg.Text = curpage.ToString(nyxCI);
                if (curpage == 1)
                {
                    this._tsPrvDta_Start.Enabled = false;
                    this._tsPrvDta_Prev.Enabled = false;
                }
                else
                {
                    this._tsPrvDta_Start.Enabled = true;
                    this._tsPrvDta_Prev.Enabled = true;
                }
                if (curpage == pagecnt)
                {
                    this._tsPrvDta_End.Enabled = false;
                    this._tsPrvDta_Next.Enabled = false;
                }
                else
                {
                    this._tsPrvDta_End.Enabled = true;
                    this._tsPrvDta_Next.Enabled = true;
                }
                SetPreviewButtons(false);
            }
            else
            {   // err
                SetPreviewButtons(true);
                this._tsPrvDta_curObject.Text = String.Empty;
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            }
            // activate gui again
            CtrlAppDisplay(String.Empty);
        }
        private void _dgPreview_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            // check the context
            if (e.Context == DataGridViewDataErrorContexts.Formatting)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrDataGridFormat", new string[] { e.Exception.Message.ToString() }),
                    e.Exception, MessageHandler.EnumMsgType.Error);
            }
            if (e.Context == DataGridViewDataErrorContexts.Commit)
            {
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrDataGridCommit", new string[] { e.Exception.Message.ToString() }),
                    e.Exception, MessageHandler.EnumMsgType.Error);
            }

            // check the exception
            if (e.Exception is ConstraintException)
            {
                DataGridView view = (DataGridView)sender;
                view.Rows[e.RowIndex].ErrorText = "Error: " + e.Context.ToString();
                view.Rows[e.RowIndex].Cells[e.ColumnIndex].ErrorText = "Error: " + e.Context.ToString();

                e.ThrowException = false;
            }
        }
        /// <summary> Put datagrid into edit mode, public cause of hotkey from nyxmain </summary>
        public void EditPrvDataGrid(bool editable)
        {   // check if we disable the datagrid readonly property
            if (editable)
            {
                this._dgPreview.ReadOnly = false;
                this._dgPreview.AllowUserToAddRows = true;
                this._dgPreview.AllowUserToDeleteRows = true;
                this._dgPreview.AllowUserToOrderColumns = true;
                // set buttons
                this._tsPrvDta_Edit.Enabled = false;
                this._cmPrvDta_Edit.Enabled = false;
                this._msPrvDta_Edit.Enabled = false;
                this._cmPrvDta_Update.Enabled = true;
                this._tsPrvDta_Update.Enabled = true;
                this._msPrv_Update.Enabled = true;
            }
            else
            {
                this._dgPreview.ReadOnly = true;
                this._dgPreview.AllowUserToAddRows = false;
                this._dgPreview.AllowUserToDeleteRows = false;
                this._dgPreview.AllowUserToOrderColumns = false;
                this._tsPrvDta_Edit.Enabled = true;
                this._cmPrvDta_Edit.Enabled = true;
                this._msPrvDta_Edit.Enabled = true;
                this._cmPrvDta_Update.Enabled = false;
                this._tsPrvDta_Update.Enabled = false;
                this._msPrv_Update.Enabled = false;
            }
        }
        /// <summary> Update changed datagrid to database, public cause of hotkey from nyxmain </summary>
        public void UpdatePrvDGrid()
        {   // got changes?
            if (dsPreview.HasChanges())
            {   // send changes?
                StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("DataGridUpdateExec"),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                if (strRD.DRes == NyxDialogResult.Yes)
                {   // building thread
                    DBThreadStartParam strThrStr = new DBThreadStartParam();
                    strThrStr.ThrAction = DatabaseAction.UpdateDS;
                    strThrStr.Param1 = this._dgPreview.Tag.ToString();
                    // control gui
                    CtrlAppDisplay(GuiHelper.GetResourceMsg("DataGridUpdate"));
                    // start thread
                    DBThreadInit(strThrStr);
                }
            }
        }
        private void UpdatePrvDGridDone(DBThreadReturnParam strThrReturn)
        {   // checkup
            if (strThrReturn.DBException == null)
            {   // set dgrid to readonly again
                this._dgPreview.ReadOnly = true;
                this._dgPreview.AllowUserToAddRows = false;
                this._dgPreview.AllowUserToDeleteRows = false;
                this._dgPreview.AllowUserToOrderColumns = false;
                // set buttons
                this._tsPrvDta_Edit.Enabled = true;
                this._cmPrvDta_Edit.Enabled = true;
                this._cmPrvDta_Update.Enabled = false;
                this._tsPrvDta_Update.Enabled = false;
            }
            else
            {   // check error, if invalidoperationexception, try alternating
                if (strThrReturn.DBException.InnerException.TargetSite.ToString() == "Void GenerateSchema()"
                    || strThrReturn.DBException.InnerException.TargetSite.ToString() == "Void CheckPrimaryKey()")
                {
                    StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("DataSetAltUpdate"),
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
                    if (strRD.DRes == NyxDialogResult.Yes)
                    {
                        if (dbh.AlternateDGridUpdate(this._tsPrvDta_curObject.Text, dsPreview))
                        {   // set dgrid to readonly again
                            this._dgPreview.ReadOnly = true;
                            this._dgPreview.AllowUserToAddRows = false;
                            this._dgPreview.AllowUserToDeleteRows = false;
                            this._dgPreview.AllowUserToOrderColumns = false;
                            // set buttons
                            this._tsPrvDta_Edit.Enabled = true;
                            this._cmPrvDta_Edit.Enabled = true;
                            this._cmPrvDta_Update.Enabled = false;
                            this._tsPrvDta_Update.Enabled = false;
                        }
                        else
                        {   // error
                            SetPreviewButtons(true);
                            MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("PreviewUpdateDataSetErr", new string[] { strThrReturn.DBException.Message.ToString() }),
                                        strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
                        }
                    }
                }
                else
                {   // error
                    SetPreviewButtons(true);
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("PreviewUpdateDataSetErr", new string[] { strThrReturn.DBException.Message.ToString() }),
                                strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
                }
            }
            CtrlAppDisplay(String.Empty);
        }
        #endregion

        #region Tabpage 3, Tables
        /* tollstrip
         */ 
        private void _tsTables_Refresh_Click(object sender, EventArgs e)
        {   // get tables
            GetTables();
        }
        private void _tsTables_curTable_TextChanged(object sender, EventArgs e)
        {
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql:
                    if (this._tsTables_curTable.Text.Length > 0)
                        this._cmTblField_Create.Enabled = true;
                    else
                        this._cmTblField_Create.Enabled = false;
                    break;
            }
        }
        /// <summary> get full tabledescribe on enterkey in toolstrip alternate table textbox </summary>
        private void _tsTables_DescTable_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this._tsTables_tbDescTable.Text.Length > 0)
            {
                this._tsTables_curTable.Text = this._tsTables_tbDescTable.Text;
                GetTableFullDesc(this._tsTables_tbDescTable.Text);
            }
        }
        private void _tsTables_Filter_TextChanged(object sender, EventArgs e)
        {
            this._lbTables.BeginUpdate();
            this._lbTables.Items.Clear();
            this._lbTables.Items.AddRange(GuiHelper.FilterList(this._tsTables_Filter.Text, tblFilterList));
            this._lbTables.EndUpdate();
        }
        /* listbox / listviews
         */ 
        private void _lbTables_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selItCnt = this._lbTables.SelectedItems.Count;
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql:
                    if (selItCnt > 0)
                    {
                        if (selItCnt == 1)
                        {
                            this._cmTable_Describe.Enabled = true;
                            this._cmTable_Alter.Enabled = true;
                        }
                        else
                        {
                            this._cmTable_Describe.Enabled = false;
                            this._cmTable_Alter.Enabled = false;
                        }
                        this._cmTable_Drop.Enabled = true;
                        this._cmTable_Truncate.Enabled = true;
                        this._cmTable_Diag.Enabled = true;
                    }
                    else
                    {
                        this._cmTable_Describe.Enabled = false;
                        this._cmTable_Alter.Enabled = false;
                        this._cmTable_Describe.Enabled = false;
                        this._cmTable_Drop.Enabled = false;
                        this._cmTable_Truncate.Enabled = false;
                        this._cmTable_Diag.Enabled = false;
                    }
                    break;
                case DBType.Oracle:
                    if (selItCnt > 0)
                    {
                        if (selItCnt == 1)
                            this._cmTable_Describe.Enabled = true;
                        else
                            this._cmTable_Describe.Enabled = false;

                        this._cmTable_Drop.Enabled = true;
                        this._cmTable_Truncate.Enabled = true;
                        this._cmTable_Diag.Enabled = true;
                    }
                    else
                    {
                        this._cmTable_Describe.Enabled = false;
                        this._cmTable_Drop.Enabled = false;
                        this._cmTable_Truncate.Enabled = false;
                        this._cmTable_Diag.Enabled = false;
                    }
                    break;
                case DBType.MSSql:
                case DBType.PgSql:
                    if (selItCnt > 0)
                    {
                        if (selItCnt == 1)
                            this._cmTable_Describe.Enabled = true;
                        else
                            this._cmTable_Describe.Enabled = false;
                        this._cmTable_Drop.Enabled = true;
                        this._cmTable_Truncate.Enabled = true;
                    }
                    else
                    {
                        this._cmTable_Describe.Enabled = false;
                        this._cmTable_Drop.Enabled = false;
                        this._cmTable_Truncate.Enabled = false;
                    }
                    break;
            }
        }
        /// <summary> get full tabledescribe on doubleclick </summary>
        private void _lbTables_DoubleClick(object sender, EventArgs e)
        {
            if (this._lbTables.SelectedItems.Count != 1)
                return;

            this._tsTables_curTable.Text = this._lbTables.SelectedItems[0].ToString();
            this._tsTables_tbDescTable.Text = String.Empty;
            GetTableFullDesc(this._lbTables.SelectedItems[0].ToString());
        }
        private void _lvTableDesc_SelectedIndexChanged(object sender, EventArgs e)
        {
            int selItCnt = this._lvTableDesc.SelectedItems.Count;
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql:
                    if (selItCnt > 0)
                    {
                        if (selItCnt == 1)
                            this._cmTblField_Alter.Enabled = true;
                        else
                            this._cmTblField_Alter.Enabled = false;
                        this._cmTblField_Drop.Enabled = true;
                    }
                    else
                    {
                        this._cmTblField_Alter.Enabled = false;
                        this._cmTblField_Drop.Enabled = false;
                    }
                    break;
                case DBType.Oracle:
                case DBType.MSSql:
                case DBType.PgSql:
                    if (selItCnt > 0)
                        this._cmTblField_Drop.Enabled = true;
                    else
                        this._cmTblField_Drop.Enabled = false;
                    break;
            }
        }
        private void _lvTableDesc_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvTableDesc, lvcsTableDetails, e.Column);
        }
        /* contectmenu table
         */ 
        private void _cmTable_Create_Click(object sender, EventArgs e)
        {
            NyxMySql.MySqlCreateTable crtTbl = new NyxMySql.MySqlCreateTable();
            crtTbl.DBContentChangedEvent += new EventHandler<DBContentChangedEventArgs>(DBContentChanged);
            crtTbl.Show();
        }
        private void _cmTable_Alter_Click(object sender, EventArgs e)
        {
            if (this._lbTables.SelectedItems.Count == 1)
            {
                NyxMySql.MySqlAlterTable altTbl = new NyxMySql.MySqlAlterTable(this._lbTables.SelectedItems[0].ToString());
                DialogResult dres = altTbl.ShowDialog();
                // refresh tables?
                if (dres == DialogResult.OK)
                    GetTables();
                // cleanup view?
                if (this._tsTables_curTable.Text == this._lbTables.SelectedItems[0].ToString())
                {
                    this._lvTableDesc.Items.Clear();
                    this._lvIndexes.Items.Clear();
                    this._lvIndexDesc.Items.Clear();
                }
            }
        }
        private void _cmTable_Refresh_Click(object sender, EventArgs e)
        {
            GetTables();
        }
        private void _cmTable_Selection(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            if (tsmi == this._cmTable_SelAll)
                GuiHelper.ListBoxSelectState(this._lbTables, GuiHelper.EnumLVSelection.Select);
            else if (tsmi == this._cmTable_SelNone)
                GuiHelper.ListBoxSelectState(this._lbTables, GuiHelper.EnumLVSelection.Unselect);
            else if (tsmi == this._cmTable_SelInv)
                GuiHelper.ListBoxSelectState(this._lbTables, GuiHelper.EnumLVSelection.Invert);
        }
        private void _cmTable_Actions(object sender, EventArgs e)
        {   /*  We start an Action here, which first starts up at showing the ListMessageBox
             *  (perhaps with additional options, dependant on the action and database type, check the ListMessageBoxSource)
             *  and afterwards the TableThreadHelper
             */
            string[] tables = new string[this._lbTables.SelectedItems.Count];
            this._lbTables.SelectedItems.CopyTo(tables, 0);
            if (tables.Length > 0)
            {
                ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
                if (tsmi == this._cmTable_Drop)
                    ExecuteTableAction(tsmi.Text, tables, TableAction.TableDrop);
                else if (tsmi == this._cmTable_Truncate)
                    ExecuteTableAction(tsmi.Text, tables, TableAction.TableTruncate);
                else if (tsmi == this._cmTableDiag_Analyze)
                    ExecuteTableAction(tsmi.Text, tables, TableAction.TableAnalyze);
                else if (tsmi == this._cmTableDiag_Check)
                    ExecuteTableAction(tsmi.Text, tables, TableAction.TableCheck);
                else if (tsmi == this._cmTableDiag_OptReb)
                    ExecuteTableAction(tsmi.Text, tables, TableAction.TableOptimize);
                else if (tsmi == this._cmTableDiag_Repair)
                    ExecuteTableAction(tsmi.Text, tables, TableAction.TableRepair);
            }
        }
        /* contextmenu tablefield
         */
        private void _cmTblField_Create_Click(object sender, EventArgs e)
        {
            NyxMySql.MySqlCreateTableField ctf = new NyxMySql.MySqlCreateTableField(
                TableAction.FieldAdd,
                this._tsTables_curTable.Text, "");
            ctf.DBContentChangedEvent += new EventHandler<DBContentChangedEventArgs>(DBContentChanged);
            ctf.Show();

        }
        private void _cmTblField_Alter_Click(object sender, EventArgs e)
        {
            if (this._lvTableDesc.SelectedItems.Count != 1)
                return;
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql:
                    NyxMySql.MySqlCreateTableField ctf = new NyxMySql.MySqlCreateTableField(
                                TableAction.FieldAlter,
                                this._tsTables_curTable.Text,
                                this._lvTableDesc.SelectedItems[0].SubItems[1].Text);
                    ctf.DBContentChangedEvent += new EventHandler<DBContentChangedEventArgs>(DBContentChanged);
                    ctf.Show();
                    break;
                case DBType.Oracle:
                    break;
            }
        }
        private void _cmTblField_Drop_Click(object sender, EventArgs e)
        {   // presettings
            TableManagement.StructTableOptions tblOptions = new TableManagement.StructTableOptions();
            // get the selected items
            tblOptions.objectList = GuiHelper.GetSelectedItems(this._lvTableDesc, 1);
            if (tblOptions.objectList.Length > 0)
            {
                tblOptions.tblAction = TableAction.FieldDrop;
                tblOptions.table = this._tsTables_curTable.Text;
                // show dialog
                DialogResult dRes = ListMessageBox.ShowListMessageBox("Drop Field(s)?", "Drop Field(s)?", tblOptions.objectList);
                if (dRes == DialogResult.OK)
                {
                    CtrlAppDisplay("Dropping Fields...");
                    TblThreadInit(tblOptions);
                }
            }
        }
        private void _cmTblField_Selection(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            if (tsmi == this._cmTblField_SelAll)
                GuiHelper.ListViewSelectededState(this._lvTableDesc, GuiHelper.EnumLVSelection.Select);
            else if (tsmi == this._cmTblField_SelNone)
                GuiHelper.ListViewSelectededState(this._lvTableDesc, GuiHelper.EnumLVSelection.Unselect);
            else if (tsmi == this._cmTblField_SelInv)
                GuiHelper.ListViewSelectededState(this._lvTableDesc, GuiHelper.EnumLVSelection.Invert);
        }
        private void _cmTblField_Refresh_Click(object sender, EventArgs e)
        {
            if (this._tsTables_curTable.Text.Length > 0)
                GetTableFullDesc(this._tsTables_curTable.Text);
        }
        /* data retrieve functions
         */        
        /// <summary> retrieve all tables </summary>
        private void GetTables()
        {   // control gui
            CtrlAppDisplay("Global: Retrieving tables...");
            // cleanup
            this._lbTables.Items.Clear();
            this._lvTableDesc.Items.Clear();
            this._lvIndexes.Items.Clear();
            this._lvIndexDesc.Items.Clear();
            this._lvPrvObjects.Items.Clear();

            this._tsTables_Filter.Text = "";
            tblFilterList.Clear();
            this._tsPrvObj_Filter.Text = "";
            prvFilterList.Clear();
            this._tsTables_curTable.Text = "";
            this._trvSqlBldr.Nodes["Tables"].Nodes.Clear();

            // set start parameter
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.GetTables;
            // get only user tables (false) or all tables (true)
            if (this._tsTables_cbScope.SelectedIndex == 0)
                strThrStart.Obj = (object)true;
            else strThrStart.Obj = (object)false;
            // start thread
            DBThreadInit(strThrStart);
        }
        /// <summary> retrieve tables done </summary>
        private void GetTablesDone(DBThreadReturnParam strThrReturn)
        {   // checkup if we have to fill new?
            if (strThrReturn.DBException == null && strThrReturn.GQString1 != null)
            {
                this._lbTables.BeginUpdate();
                this._trvSqlBldr.BeginUpdate();
                foreach (string table in strThrReturn.GQString1)
                {
                    this._trvSqlBldr.Nodes["Tables"].Nodes.Add(GuiHelper.SqlBldrGetTableTN(table));
                    this._lbTables.Items.Add(table);
                    tblFilterList.Add(table);
                }
                this._lbTables.EndUpdate();
                this._trvSqlBldr.EndUpdate();
                // reinit auticompletition
                if (NyxMain.uaSett.AutoCompletition && NyxMain.uaSett.AutoCompletitionUseTables)
                    ACPrepareListBox();
            }
            else
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            // activate gui again
            CtrlAppDisplay(String.Empty);
            // focus settings
            if (this._tcMain.SelectedTab == this._tpSql)
            {
                SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                smt.Select();
            }
            else if (this._tcMain.SelectedTab == this._tpTables)
            {
                SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                smt.Select();
            }
        }
        /// <summary> retrieve full table desc</summary>
        private void GetTableFullDesc(string table)
        {   // control gui
            CtrlAppDisplay("Tables: Retrieving Tabledescribe, Indexes...");
            // cleanup
            this._lvTableDesc.Items.Clear();
            this._lvIndexes.Items.Clear();
            this._lvIndexDesc.Items.Clear();
            // set threadstart
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.Param1 = table;
            strThrStart.ThrAction = DatabaseAction.GetTableFullDesc;
            // start thread
            DBThreadInit(strThrStart);
        }
        /// <summary> retrieve full table desc done</summary>
        private void GetTableFullDescDone(DBThreadReturnParam strThrReturn)
        {   // checkup
            if (strThrReturn.DBException == null)
            {   /* tables
                 */
                ListViewItem lvi;
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql:
                        // tabledescribe
                        this._lvTableDesc.BeginUpdate();
                        foreach (TableDetails strTblDesc in (Queue<TableDetails>)strThrReturn.Data1)
                        {
                            lvi = GuiHelper.BuildListViewItem(strTblDesc.HasIndex,
                                new string[] { strTblDesc.Name, strTblDesc.Type, strTblDesc.Null, strTblDesc.DefaultValue, strTblDesc.Extra });
                            this._lvTableDesc.Items.Add(lvi);
                        }
                        this._lvTableDesc.EndUpdate();
                        // indexes
                        this._lvIndexes.BeginUpdate();
                        foreach (IndexDetails strIdxDet in (Queue<IndexDetails>)strThrReturn.Data2)
                        {
                            string unique = "N", fulltext = "N";
                            if(strIdxDet.IsUnique)
                                unique = "Y";
                            if (strIdxDet.IsFulltext)
                                fulltext = "Y";
                            lvi = GuiHelper.BuildListViewItem(strIdxDet.Name,
                                new string[] { unique, fulltext });
                            this._lvIndexes.Items.Add(lvi);
                        }
                        this._lvIndexes.EndUpdate();
                        break;
                    case DBType.Oracle:
                        // tabledescribe
                        this._lvTableDesc.BeginUpdate();
                        foreach (TableDetails strTblDesc in (Queue<TableDetails>)strThrReturn.Data1)
                        {
                            lvi = GuiHelper.BuildListViewItem(strTblDesc.HasIndex,
                                new string[] { strTblDesc.Name, strTblDesc.Type, strTblDesc.Lenght, strTblDesc.Precision });
                            this._lvTableDesc.Items.Add(lvi);
                        }
                        this._lvTableDesc.EndUpdate();
                        // indexes
                        this._lvIndexes.BeginUpdate();
                        foreach (IndexDetails strIdxDet in (Queue<IndexDetails>)strThrReturn.Data2)
                        {
                            string unique = "N", primary = "N";
                            if (strIdxDet.IsUnique)
                                unique = "Y";
                            if (strIdxDet.IsPrimary)
                                primary = "Y";
                            lvi = GuiHelper.BuildListViewItem(strIdxDet.Name,
                                new string[] { primary, unique, strIdxDet.Status, strIdxDet.Analyzed });
                            this._lvIndexes.Items.Add(lvi);
                        }
                        this._lvIndexes.EndUpdate();
                        break;
                    case DBType.MSSql:
                        // tabledescribe
                        this._lvTableDesc.BeginUpdate();
                        foreach (TableDetails strTblDesc in (Queue<TableDetails>)strThrReturn.Data1)
                        {
                            lvi = GuiHelper.BuildListViewItem(strTblDesc.HasIndex,
                                new string[] { strTblDesc.Name, strTblDesc.Type, strTblDesc.Lenght, strTblDesc.Precision, strTblDesc.Null });
                            this._lvTableDesc.Items.Add(lvi);
                        }
                        this._lvTableDesc.EndUpdate();
                        // indexes
                        this._lvIndexes.BeginUpdate();
                        foreach (IndexDetails strIdxDet in (Queue<IndexDetails>)strThrReturn.Data2)
                        {
                            string fulltext = "N", descending = "N";
                            if (strIdxDet.IsFulltext)
                                fulltext = "Y";
                            if (strIdxDet.Descending)
                                descending = "Y";
                            lvi = GuiHelper.BuildListViewItem(strIdxDet.Name,
                                new string[] { strIdxDet.Status, fulltext, descending });
                            this._lvIndexes.Items.Add(lvi);
                        }
                        this._lvIndexes.EndUpdate();
                        break;
                    case DBType.PgSql:
                        // tabledescribe
                        this._lvTableDesc.BeginUpdate();
                        foreach (TableDetails strTblDesc in (Queue<TableDetails>)strThrReturn.Data1)
                        {
                            lvi = GuiHelper.BuildListViewItem(strTblDesc.HasIndex,
                                new string[] { strTblDesc.Name, strTblDesc.Type, strTblDesc.Lenght, strTblDesc.Precision, strTblDesc.Null });
                            this._lvTableDesc.Items.Add(lvi);
                        }
                        this._lvTableDesc.EndUpdate();
                        // indexes
                        this._lvIndexes.BeginUpdate();
                        foreach (IndexDetails strIdxDet in (Queue<IndexDetails>)strThrReturn.Data2)
                        {
                            string unique = "N", primary = "N", clustered = "N";
                            if (strIdxDet.IsUnique)
                                unique = "Y";
                            if (strIdxDet.IsPrimary)
                                primary = "Y";
                            if (strIdxDet.IsClustered)
                                clustered = "Y";
                            lvi = GuiHelper.BuildListViewItem(strIdxDet.Name,
                                new string[] { primary, unique, clustered });
                            this._lvIndexes.Items.Add(lvi);
                        }
                        this._lvIndexes.EndUpdate();
                        break;
                }
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
                this._tsTables_curTable.Text = "";
            }
            // endupdate
            this._lvIndexes.EndUpdate();
            
            this._lvIndexDesc.EndUpdate();
            // enable gui again
            CtrlAppDisplay(String.Empty);
            if (this._tcMain.SelectedTab == this._tpTables)
                this._lbTables.Focus();
        }
        private void ExecuteTableAction(string msg, string[] tblList, TableAction tblact)
        {   // got selected items?
            if (tblList.Length > 0)
            {   // init tableoptions struct
                TableManagement.StructTableOptions tblOptions = new TableManagement.StructTableOptions();
                tblOptions.tblAction = tblact;
                tblOptions.objectList = tblList;
                // show dialog
                DialogResult dRes;
                if (NyxMain.ConProfile.ProfileDBType == DBType.Oracle &&
                    (tblOptions.tblAction == TableAction.TableDrop || tblOptions.tblAction == TableAction.TableAnalyze))
                {
                    dRes = ListMessageBox.ShowListMessageBox(msg + " Table(s)?", msg + "?", tblOptions.objectList, tblOptions.tblAction, ref tblOptions.addOpt);
                }
                else
                    dRes = ListMessageBox.ShowListMessageBox(msg + "Table(s)?", msg + "?", tblOptions.objectList);
                // if ok, we start
                if (dRes == DialogResult.OK)
                {
                    CtrlAppDisplay(msg + " Tables...");
                    TblThreadInit(tblOptions);
                }
            }
        }
        /// <summary> misc. table action done </summary>
        private void TableActionDone(TableManagement.TblThreadReturnVars strThrReturn)
        {   // check error
            if (strThrReturn.errExc == null)
            {
                if (strThrReturn.tblAction == TableAction.TableTruncate)
                {
                    if (this._tcMain.SelectedTab == this._tpPreview)
                    {
                        if (this._lvPrvObjects.SelectedItems.IndexOfKey(this._tsPrvDta_curObject.Text) > -1)
                        {
                            SetPreviewButtons(true);
                            this._tsPrvDta_curObject.Text = "";
                        }
                    }
                }
            }
            else
                MessageHandler.ShowDialog(strThrReturn.errExc, MessageHandler.EnumMsgType.Warning);
            // activate gui again
            CtrlAppDisplay(String.Empty);
        }
        private void TableActionDropDone(TableManagement.TblThreadReturnVars strThrReturn)
        {   // check error
            if (strThrReturn.errExc == null)
            {   // item array
                string[] items;
                // currently viewing in describeview? then release dataresources
                if (this._tcMain.SelectedTab == this._tpTables)
                {
                    items = new string[this._lbTables.SelectedItems.Count];
                    this._lbTables.SelectedItems.CopyTo(items, 0);
                    if (this._lbTables.SelectedItems.IndexOf(this._tsTables_curTable.Text) > -1)
                    {
                        this._lvTableDesc.Items.Clear();
                        this._lvIndexes.Items.Clear();
                        this._lvIndexDesc.Items.Clear();
                        this._tsTables_curTable.Text = "";
                    }
                }
                // currently viewing in preview? then release dataresources
                else
                {
                    
                    items = new string[1] { this._lvPrvObjects.SelectedItems[0].Text.ToString() };
                    if (this._lvPrvObjects.SelectedItems[0].Text.ToString() == this._tsPrvDta_curObject.Text.ToString())
                    {
                        SetPreviewButtons(true);
                        this._tsPrvDta_curObject.Text = "";
                    }
                }
                // remove from lists
                this._lvPrvObjects.BeginUpdate();
                this._lbTables.BeginUpdate();
                foreach (string item in items)
                {   // remove from filterlist
                    tblFilterList.Remove(item);
                    prvFilterList.Remove(new PreviewObject(item, "Table"));
                    // remove items from objects
                    this._lvPrvObjects.Items.RemoveByKey(item);
                    this._lbTables.Items.Remove(item);
                    // remove from treeview
                    this._trvSqlBldr.Nodes["Tables"].Nodes.RemoveByKey(item);
                }
                this._lvPrvObjects.EndUpdate();
                this._lbTables.EndUpdate();
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.errExc, MessageHandler.EnumMsgType.Warning);
                // reinit dbcontent
                DbContentInit(true, true, false);
            }
            CtrlAppDisplay(String.Empty);
        }
        private void TableActionFieldDropDone(TableManagement.TblThreadReturnVars strThrReturn)
        {
            if (strThrReturn.errExc == null)
            {   // get selected 
                ListView.SelectedListViewItemCollection lviCol = this._lvTableDesc.SelectedItems;
                // cleanup
                this._lvIndexDesc.Items.Clear();
                this._lvTableDesc.BeginUpdate();
                foreach (ListViewItem lvi in lviCol)
                {
                    if (this._trvSqlBldr.Nodes["Tables"].Nodes.ContainsKey(this._tsTables_curTable.Text.ToString())
                        && this._trvSqlBldr.Nodes["Tables"].Nodes[this._tsTables_curTable.Text].Nodes.ContainsKey(lvi.SubItems[1].Text.ToString()))
                        this._trvSqlBldr.Nodes["Tables"].Nodes[this._tsTables_curTable.Text].Nodes[lvi.SubItems[1].Text].Remove();
                    this._lvTableDesc.Items.Remove(lvi);
                }
                // endupdate
                this._lvTableDesc.EndUpdate();
                // check preview
                if (this._tsTables_curTable.Text == this._tsPrvDta_curObject.Text)
                {
                    SetPreviewButtons(true);
                    this._tsPrvDta_curObject.Text = "";
                }
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.errExc, MessageHandler.EnumMsgType.Warning);
                // reinit dbcontent
                DbContentInit(true, true, false);
            }
            CtrlAppDisplay(String.Empty);
        }
        private void TableActionIndexDropDone(TableManagement.TblThreadReturnVars strThrReturn)
        {
            if (strThrReturn.errExc == null)
            {   // get selected 
                ListView.SelectedListViewItemCollection lviCol = this._lvIndexes.SelectedItems;
                // cleanup
                this._lvIndexDesc.Items.Clear();
                this._lvIndexes.BeginUpdate();
                foreach (ListViewItem lvi in lviCol)
                    this._lvIndexes.Items.Remove(lvi);
                // endupdate
                this._lvIndexes.EndUpdate();
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.errExc, MessageHandler.EnumMsgType.Warning);
            }
            CtrlAppDisplay(String.Empty);
        }
        private void TableActionViewDropDone(TableManagement.TblThreadReturnVars strThrReturn)
        {   // check error
            if (strThrReturn.errExc == null)
            {   // item array
                string[] items;
                // check where the user selected items
                if (this._tcMain.SelectedTab == this._tpViews)
                {
                    items = new string[this._lbViews.SelectedItems.Count];
                    this._lbViews.SelectedItems.CopyTo(items, 0);
                }
                // 
                else
                    items = new string[1] { this._lvPrvObjects.SelectedItems[0].Text.ToString() };
                // cleanup?
                if (this._lbViews.SelectedItems.IndexOf(this._tsViews_curView.Text) > -1)
                {
                    this._lvView_Details.Items.Clear();
                    this._tsViews_curView.Text = "";
                    this._rtbView_CreateStm.Text = "";
                }
                if (this._lvPrvObjects.SelectedItems[0].Text.ToString() == this._tsPrvDta_curObject.Text.ToString())
                {
                    SetPreviewButtons(true);
                    this._tsPrvDta_curObject.Text = "";
                }
                // remove from lists
                this._lvPrvObjects.BeginUpdate();
                this._lbViews.BeginUpdate();
                foreach (string item in items)
                {   // remove from filterlist
                    viewFilterList.Remove(item);
                    prvFilterList.Remove(new PreviewObject(item, "View"));
                    // remove items from objects
                    this._lvPrvObjects.Items.RemoveByKey(item);
                    this._lbViews.Items.Remove(item);
                    // remove from treeview
                    this._trvSqlBldr.Nodes["Views"].Nodes.RemoveByKey(item);
                }
                this._lvPrvObjects.EndUpdate();
                this._lbViews.EndUpdate();
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.errExc, MessageHandler.EnumMsgType.Warning);
                // reinit dbcontent
                DbContentInit(true, true, false);
            }
            CtrlAppDisplay(String.Empty);
        }
        #endregion

        #region Tabpage 3, Indexes
        /// <summary> sort indexdescription listview </summary>
        private void _lvIndexDesc_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvIndexDesc, lvcsIndexDetails, e.Column);
        }
        /// <summary> sort indexes listview </summary>
        private void _lvIndexes_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            GuiHelper.ListViewSort(this._lvIndexes, lvcsIndexes, e.Column);
        }
        /// <summary> retrieve indexes </summary>
        private void GetIndexes()
        {
            if (this._tsTables_curTable.Text.Length < 0)
                return;
            // control gui
            CtrlAppDisplay("Indexes: Retrieving Indexes...");
            // cleanup
            this._lvIndexes.Items.Clear();
            this._lvIndexDesc.Items.Clear();
            // init thread vars
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.Param1 = this._tsTables_curTable.Text;
            strThrStart.ThrAction = DatabaseAction.GetIndexes;
            // start thread
            DBThreadInit(strThrStart);
        }
        /// <summary> retrieve indexes done </summary>
        private void GetIndexesDone(DBThreadReturnParam strThrReturn)
        {   // checkup
            if (strThrReturn.DBException == null)
            {   // add to listview
                ListViewItem lvi;
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql:
                        this._lvIndexes.BeginUpdate();
                        foreach (IndexDetails strIdxDet in (Queue<IndexDetails>)strThrReturn.Data1)
                        {
                            string unique = "N", fulltext = "N";
                            if (strIdxDet.IsUnique)
                                unique = "Y";
                            if (strIdxDet.IsFulltext)
                                fulltext = "Y";
                            lvi = GuiHelper.BuildListViewItem(strIdxDet.Name,
                                new string[] { unique, fulltext });
                            this._lvIndexes.Items.Add(lvi);
                        }
                        this._lvIndexes.EndUpdate();
                        break;
                    case DBType.Oracle:
                        this._lvIndexes.BeginUpdate();
                        foreach (IndexDetails strIdxDet in (Queue<IndexDetails>)strThrReturn.Data1)
                        {
                            string unique = "N", primary = "N";
                            if (strIdxDet.IsUnique)
                                unique = "Y";
                            if (strIdxDet.IsPrimary)
                                primary = "Y";
                            lvi = GuiHelper.BuildListViewItem(strIdxDet.Name,
                                new string[] { primary, unique, strIdxDet.Status, strIdxDet.Analyzed });
                            this._lvIndexes.Items.Add(lvi);
                        }
                        this._lvIndexes.EndUpdate();
                        break;
                    case DBType.MSSql:
                        this._lvIndexes.BeginUpdate();
                        foreach (IndexDetails strIdxDet in (Queue<IndexDetails>)strThrReturn.Data1)
                        {
                            string fulltext = "N", descending = "N";
                            if (strIdxDet.IsFulltext)
                                fulltext = "Y";
                            if (strIdxDet.Descending)
                                descending = "Y";
                            lvi = GuiHelper.BuildListViewItem(strIdxDet.Name,
                                new string[] { strIdxDet.Status, fulltext, descending });
                            this._lvIndexes.Items.Add(lvi);
                        }
                        this._lvIndexes.EndUpdate();
                        break;
                    case DBType.PgSql:
                        this._lvIndexes.BeginUpdate();
                        foreach (IndexDetails strIdxDet in (Queue<IndexDetails>)strThrReturn.Data1)
                        {
                            string unique = "N", primary = "N", clustered = "N";
                            if (strIdxDet.IsUnique)
                                unique = "Y";
                            if (strIdxDet.IsPrimary)
                                primary = "Y";
                            if (strIdxDet.IsClustered)
                                clustered = "Y";
                            lvi = GuiHelper.BuildListViewItem(strIdxDet.Name,
                                new string[] { strIdxDet.Name, primary, unique, clustered });
                            this._lvIndexes.Items.Add(lvi);
                        }
                        this._lvIndexes.EndUpdate();
                        break;
                }
            }
            else
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            // re-set gui
            CtrlAppDisplay(String.Empty);
        }
        /// <summary> doubleclick in index listview will retrieve index desc </summary>
        private void _lvIndexes_DoubleClick(object sender, EventArgs e)
        {
            if (this._lvIndexes.SelectedItems.Count != 1)
                return;
            // control gui
            CtrlAppDisplay("Indexes: Retrieving Indexdescription...");
            // cleanup
            this._lvIndexDesc.Items.Clear();
            // init threadstartvars
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.Param1 = this._tsTables_curTable.Text;
            strThrStart.Param2 = this._lvIndexes.SelectedItems[0].Text;
            strThrStart.ThrAction = DatabaseAction.GetIndexDescribe;
            // start thread
            DBThreadInit(strThrStart);
        }
        /// <summary> retrieve index desc done </summary>
        private void GetIndexDescribeDone(DBThreadReturnParam strThrReturn)
        {   // checkup
            if (strThrReturn.DBException == null)
            {
                ListViewItem lvi;
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql:
                        this._lvIndexDesc.BeginUpdate();
                        foreach (IndexDescribe strIdxDesc in (Queue<IndexDescribe>)strThrReturn.Data1)
                        {
                            lvi = GuiHelper.BuildListViewItem(strIdxDesc.Name,
                                new string[] { strIdxDesc.Type, strIdxDesc.Position, strIdxDesc.Subpart });
                            this._lvIndexDesc.Items.Add(lvi);
                        }
                        this._lvIndexDesc.EndUpdate();
                        break;
                    case DBType.Oracle:
                        this._lvIndexDesc.BeginUpdate();
                        foreach (IndexDescribe strIdxDesc in (Queue<IndexDescribe>)strThrReturn.Data1)
                        {
                            lvi = GuiHelper.BuildListViewItem(strIdxDesc.Name,
                                new string[] { strIdxDesc.Type, strIdxDesc.Position, strIdxDesc.Descend });
                            this._lvIndexDesc.Items.Add(lvi);
                        }
                        this._lvIndexDesc.EndUpdate();
                        break;
                    case DBType.MSSql:
                        this._lvIndexDesc.BeginUpdate();
                        foreach (IndexDescribe strIdxDesc in (Queue<IndexDescribe>)strThrReturn.Data1)
                        {
                            lvi = GuiHelper.BuildListViewItem(strIdxDesc.Name);
                            this._lvIndexDesc.Items.Add(lvi);
                        }
                        this._lvIndexDesc.EndUpdate();
                        break;
                }
            }
            else
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Error);
            // re-set gui
            CtrlAppDisplay(String.Empty);
        }
        /// <summary> indexchanged event handler </summary>
        private void _lvIndexes_SelectedIndexChanged(object sender, EventArgs e)
        {   // get selected count
            int selItCnt = this._lvIndexes.SelectedItems.Count;
            // activate normal functions
            if (selItCnt > 0)
            {   // db specific
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql:
                        this._cmIndex_Alter.Enabled = true;
                        break;
                    case DBType.Oracle:
                    case DBType.MSSql:
                    case DBType.PgSql:
                        break;
                }
                // all 
                if (selItCnt == 1)
                    this._cmIndex_Describe.Enabled = true;
                else
                    this._cmIndex_Describe.Enabled = false;
                this._cmIndex_Drop.Enabled = true;
            }
            else
            {
                this._cmIndex_Alter.Enabled = false;
                this._cmIndex_Describe.Enabled = false;
                this._cmIndex_Drop.Enabled = false;
            }
        }
        /// <summary> contextmenu, index selection handling </summary>
        private void _cmIndex_Selection(object sender, EventArgs e)
        {
            ToolStripMenuItem tsmi = (ToolStripMenuItem)sender;
            if (tsmi == this._cmIndex_SelAll)
                GuiHelper.ListViewSelectededState(this._lvIndexes, GuiHelper.EnumLVSelection.Select);
            else if (tsmi == this._cmIndex_SelNone)
                GuiHelper.ListViewSelectededState(this._lvIndexes, GuiHelper.EnumLVSelection.Unselect);
            else if (tsmi == this._cmIndex_SelInv)
                GuiHelper.ListViewSelectededState(this._lvIndexes, GuiHelper.EnumLVSelection.Invert);
        }
        /// <summary> contextmenu, create index </summary>
        private void _cmIndex_Create_Click(object sender, EventArgs e)
        {
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql:
                    NyxMySql.MySqlCreateIndex ci;
                    if (this._tsTables_curTable.Text.Length > 0)
                        ci = new NyxMySql.MySqlCreateIndex(TableAction.IndexAdd, this._lbTables.SelectedItem.ToString());
                    else
                        ci = new NyxMySql.MySqlCreateIndex();
                    ci.DBContentChangedEvent += new EventHandler<DBContentChangedEventArgs>(DBContentChanged);
                    ci.Show();
                    break;
            }
        }
        /// <summary> contextmenu, drop index </summary>
        private void _cmIndex_Drop_Click(object sender, EventArgs e)
        {   // presettings
            TableManagement.StructTableOptions tblOptions = new TableManagement.StructTableOptions();
            // get the selected items
            tblOptions.objectList = GuiHelper.GetSelectedItems(this._lvIndexes, 0);
            if (tblOptions.objectList.Length > 0)
            {
                tblOptions.tblAction = TableAction.IndexDrop;
                tblOptions.table = this._tsTables_curTable.Text;
                // show dialog
                string msg = GuiHelper.GetResourceMsg("IndexDropQst");
                DialogResult dRes = ListMessageBox.ShowListMessageBox(msg, msg, tblOptions.objectList);

                if (dRes == DialogResult.OK)
                {
                    CtrlAppDisplay("Dropping Indexes...");
                    TblThreadInit(tblOptions);
                }
            }
        }
        /// <summary> contextmenu, alter index </summary>
        private void _cmIndex_Alter_Click(object sender, EventArgs e)
        {
            if (this._lvIndexes.SelectedItems.Count == 1)
            {
                switch (NyxMain.ConProfile.ProfileDBType)
                {
                    case DBType.MySql:
                        NyxMySql.MySqlCreateIndex ci = new NyxMySql.MySqlCreateIndex(TableAction.IndexAlter,
                                this._tsTables_curTable.Text.ToString(),
                                this._lvIndexes.SelectedItems[0].SubItems[0].Text.ToString(),
                                this._lvIndexes.SelectedItems[0].SubItems[1].Text.ToString(),
                                this._lvIndexes.SelectedItems[0].SubItems[2].Text.ToString());
                        ci.DBContentChangedEvent += new EventHandler<DBContentChangedEventArgs>(DBContentChanged);
                        ci.Show();
                        break;
                }
            }
        }
        /// <summary> refresh indexes </summary>
        private void _cmIndex_Refresh_Click(object sender, EventArgs e)
        {
            GetIndexes();
        }
        #endregion

        #region Tabpage 4, Views
        private void _tsViews_Filter_TextChanged(object sender, EventArgs e)
        {
            this._lbViews.BeginUpdate();
            this._lbViews.Items.Clear();
            this._lbViews.Items.AddRange(GuiHelper.FilterList(this._tsViews_Filter.Text, viewFilterList));
            this._lbViews.EndUpdate();
        }
        private void _views_Refresh_Click(object sender, EventArgs e)
        {
            GetViews();
        }
        private void _lbViews_DoubleClick(object sender, EventArgs e)
        {
            GetViewDetails();
        }
        private void _views_Create_Click(object sender, EventArgs e)
        {
            switch (conProfile.ProfileDBType)
            {
                case DBType.MySql:
                    NyxMySql.MySqlCreateView mcv = new NyxMySql.MySqlCreateView();
                    mcv.DBContentChanged += new EventHandler<DBContentChangedEventArgs>(DBContentChanged);
                    mcv.Show();
                    break;
            }
        }
        private void _cmViews_Drop_Click(object sender, EventArgs e)
        {
            if (this._lbViews.SelectedItems.Count > 0)
            {
                string[] items = new string[this._lbViews.SelectedItems.Count];
                this._lbViews.SelectedItems.CopyTo(items, 0);
                ExecuteTableAction(this._cmViews_Drop.Text, items, TableAction.ViewDrop);
            }
        }
        private void _lbViews_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._lbViews.SelectedItems.Count > 0)
                this._cmViews_Drop.Enabled = true;
            else
                this._cmViews_Drop.Enabled = false;
        }
        private void GetViews()
        {   // gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("ViewsRetrieve"));
            // cleanup
            this._trvSqlBldr.Nodes["Views"].Nodes.Clear();
            this._lbViews.Items.Clear();
            this._lvView_Details.Items.Clear();
            this._rtbView_CreateStm.Text = String.Empty;
            this._tsViews_Filter.Text = String.Empty;
            this._tsViews_curView.Text = String.Empty;
            viewFilterList.Clear();
            // set threadstart params
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.GetViews;
            // get only user views (false) or all views (true)
            if (this._tsViews_cbScopeve.SelectedIndex == 0)
                strThrStart.Obj = (object)true;
            else strThrStart.Obj = (object)false;
            // start thread
            DBThreadInit(strThrStart);
        }
        private void GetViewsDone(DBThreadReturnParam strThrReturn)
        {   // init vars
            if (strThrReturn.DBException == null)
            {   // parse and add into cb
                this._trvSqlBldr.BeginUpdate();
                this._lbViews.BeginUpdate();
                foreach (string view in strThrReturn.GQString1)
                {
                    this._trvSqlBldr.Nodes["Views"].Nodes.Add(view, view);
                    this._lbViews.Items.Add(view);
                    viewFilterList.Add(view);
                }
                this._trvSqlBldr.EndUpdate();
                this._lbViews.EndUpdate();
                // reinit auticompletition
                if (NyxMain.uaSett.AutoCompletition && NyxMain.uaSett.AutoCompletitionUseViews)
                    ACPrepareListBox();
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            }
            // activate gui again
            CtrlAppDisplay(String.Empty);
        }
        private void GetViewDetails()
        {
            if (this._lbViews.SelectedItems.Count != 1)
                return;
            // gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("ViewsDetailRetrieve"));
            // cleanup
            this._lvView_Details.Items.Clear();
            this._rtbView_CreateStm.Text = String.Empty;
            // init threadvars
            DBThreadStartParam thrStrVar = new DBThreadStartParam();
            thrStrVar.Param1 = this._lbViews.SelectedItem.ToString();
            this._tsViews_curView.Text = this._lbViews.SelectedItem.ToString();
            thrStrVar.ThrAction = DatabaseAction.GetViewDetails;
            // go on with the thread
            DBThreadInit(thrStrVar);
        }
        private void GetViewDetailsDone(DBThreadReturnParam strThrReturn)
        {   // init vars
            if (strThrReturn.DBException == null)
            {   // cast
                ViewDetails strViewDet = (ViewDetails)strThrReturn.Data1;
                // fill listview
                ListViewItem lvi;
                this._lvView_Details.BeginUpdate();
                foreach (string[] procDet in strViewDet.Info)
                {
                    lvi = GuiHelper.BuildListViewItem(procDet);
                    this._lvView_Details.Items.Add(lvi);
                }
                this._lvView_Details.EndUpdate();
                // fill rtb
                this._rtbView_CreateStm.Text = strViewDet.CreateStm;
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
                this._tsTriggers_curTriggers.Text = String.Empty;
            }
            // activate gui again
            CtrlAppDisplay(String.Empty);
            this._lbViews.Focus();
        }
        #endregion

        #region Tabpage 5, Triggers
        private void _tsTriggers_Filter_TextChanged(object sender, EventArgs e)
        {
            this._lbTriggers.BeginUpdate();
            this._lbTriggers.Items.Clear();
            this._lbTriggers.Items.AddRange(GuiHelper.FilterList(this._tsTriggers_Filter.Text, triggerFilterList));
            this._lbTriggers.EndUpdate();
        }
        private void _tsTriggers_Refresh_Click(object sender, EventArgs e)
        {
            GetTriggers();
        }
        private void _lbTriggers_DoubleClick(object sender, EventArgs e)
        {
            GetTriggerDetails();
        }
        private void GetTriggers()
        {   // disable gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("TriggersRetrieve"));
            // cleanup
            this._lbTriggers.Items.Clear();
            this._lvTrigger_Details.Items.Clear();
            this._rtbTrigger_CrtStm.Text = String.Empty;
            this._tsTriggers_Filter.Text = String.Empty;
            this._tsTriggers_curTriggers.Text = String.Empty;
            triggerFilterList.Clear();
            // set threadstart params
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.GetTriggers;
            // get only user triggers (false) or all triggers (true)
            if (this._tsTriggers_cbScope.SelectedIndex == 0)
                strThrStart.Obj = (object)true;
            else 
                strThrStart.Obj = (object)false;
            // start thread
            DBThreadInit(strThrStart);
        }
        private void GetTriggersDone(DBThreadReturnParam strThrReturn)
        {   // parse and add into cb
            if (strThrReturn.DBException == null)
            {
                this._lbTriggers.BeginUpdate();
                foreach (string trigger in strThrReturn.GQString1)
                {
                    this._lbTriggers.Items.Add(trigger);
                    triggerFilterList.Add(trigger);
                }
                this._lbTriggers.EndUpdate();
            }
            else
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            // activate gui again
            CtrlAppDisplay(String.Empty);
        }
        private void GetTriggerDetails()
        {   // checkup
            if (this._lbTriggers.SelectedItems.Count != 1)
                return;
            // disable gui
            CtrlAppDisplay(GuiHelper.GetResourceMsg("TriggersDetailRetrieve"));
            // cleanup
            this._lvTrigger_Details.Items.Clear();
            this._rtbTrigger_CrtStm.Text = String.Empty;
            this._tsTriggers_curTriggers.Text = String.Empty;
            // threadstart params
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.GetTriggerDetails;
            strThrStart.Param1 = this._lbTriggers.SelectedItem.ToString();
            this._tsTriggers_curTriggers.Text = this._lbTriggers.SelectedItem.ToString();
            // start thread
            DBThreadInit(strThrStart);
        }
        private void GetTriggerDetailsDone(DBThreadReturnParam strThrReturn)
        {   // init vars
            if (strThrReturn.DBException == null)
            {   // cast
                TriggerDetails strTriggerDet = (TriggerDetails)strThrReturn.Data1;
                this._lvTrigger_Details.BeginUpdate();
                foreach (string[] procDet in strTriggerDet.Info)
                    this._lvTrigger_Details.Items.Add(GuiHelper.BuildListViewItem(procDet));
                this._lvTrigger_Details.EndUpdate();
                // set text and fill rtb
                this._tsTriggers_curTriggers.Text = strTriggerDet.Name;
                this._rtbTrigger_CrtStm.Text = strTriggerDet.CreateStm;
            }
            else
            {   // err
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
                this._tsTriggers_curTriggers.Text = String.Empty;
            }
            // activate gui again
            CtrlAppDisplay(String.Empty);
            this._lbTriggers.Focus();
        }
        #endregion

        #region Tabpage 6, Procedures
        private void _tsPrcdrs_Filter_TextChanged(object sender, EventArgs e)
        {
            this._lbPrcdrs.BeginUpdate();
            this._lbPrcdrs.Items.Clear();
            this._lbPrcdrs.Items.AddRange(GuiHelper.FilterList(this._tsPrcdrs_Filter.Text, prcdrFilterList));
            this._lbPrcdrs.EndUpdate();
        }
        private void _tsPrcdrs_Refresh_Click(object sender, EventArgs e)
        {
            GetProcedures();
        }
        private void _lbProcedures_DoubleClick(object sender, EventArgs e)
        {
            GetProcedureDetails();
        }
        private void GetProcedures()
        {   // disable gui
            CtrlAppDisplay("ProceduresRetrieve");
            // cleanup
            this._tsPrcdrs_curProcedure.Text = String.Empty;
            this._trvSqlBldr.Nodes["Procedures"].Nodes.Clear();
            this._lbPrcdrs.Items.Clear();
            this._lvPrcdr_Details.Items.Clear();
            this._rtbPrcdr_CreateStm.Text = String.Empty;
            this._tsPrcdrs_Filter.Text = String.Empty;
            prcdrFilterList.Clear();
            // set threadstart params
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.GetProcedures;
            // get only user procedures (false) or all procedures (true)
            if (this._tsPrcdrs_cbScope.SelectedIndex == 0)
                strThrStart.Obj = (object)true;
            else strThrStart.Obj = (object)false;
            // gui + start thread
            DBThreadInit(strThrStart);
        }
        private void GetProceduresDone(DBThreadReturnParam strThrReturn)
        {   // init vars
            if (strThrReturn.DBException == null)
            {
                this._trvSqlBldr.BeginUpdate();
                this._lbPrcdrs.BeginUpdate();
                foreach (string procedure in strThrReturn.GQString1)
                {
                    this._lbPrcdrs.Items.Add(procedure);
                    this._trvSqlBldr.Nodes["Procedures"].Nodes.Add(procedure, procedure);
                    prcdrFilterList.Add(procedure);
                }
                this._trvSqlBldr.EndUpdate();
                this._lbPrcdrs.EndUpdate();
                // reinit auticompletition
                if (NyxMain.uaSett.AutoCompletition && NyxMain.uaSett.AutoCompletitionUseProcedures)
                    ACPrepareListBox();
            }
            else
            {
                this._tsPrcdrs_curProcedure.Text = "";
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            }
            // activate gui again
            CtrlAppDisplay(String.Empty);
        }
        private void GetProcedureDetails()
        {   // checkup?
            if (this._lbPrcdrs.SelectedItems.Count != 1)
                return;
            // threadstart params
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            strThrStart.ThrAction = DatabaseAction.GetProcedureDetails;
            strThrStart.Param1 = this._lbPrcdrs.SelectedItem.ToString();
            this._tsPrcdrs_curProcedure.Text = this._lbPrcdrs.SelectedItem.ToString();
            // gui + start thread
            CtrlAppDisplay("ProceduresDetailRetrieve");
            DBThreadInit(strThrStart);
        }
        private void GetProcedureDetailsDone(DBThreadReturnParam strThrReturn)
        {   // cleanup
            this._lvPrcdr_Details.BeginUpdate();
            this._lvPrcdr_Details.Items.Clear();
            this._rtbPrcdr_CreateStm.Text = String.Empty;
            // init vars
            if (strThrReturn.DBException == null)
            {   // fill listview
                ProcedureDetails strProcedureDet = (ProcedureDetails)strThrReturn.Data1;
                foreach (string[] procDet in strProcedureDet.Info)
                    this._lvPrcdr_Details.Items.Add(GuiHelper.BuildListViewItem(procDet));
                // fill rtb
                this._rtbPrcdr_CreateStm.Text = strProcedureDet.CreateStm;
            }
            else
            {   // err
                this._tsPrcdrs_curProcedure.Text = String.Empty;
                MessageHandler.ShowDialog(strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            }
            // activate gui again
            this._lvPrcdr_Details.EndUpdate();
            CtrlAppDisplay(String.Empty);
            this._lbPrcdrs.Focus();
        }
        #endregion

        #region SystemTray and Restart
        private void _NI_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.Visible) HideApp();
            else ShowApp();
        }
        private void HideApp()
        {	// hide application
            this.ShowInTaskbar = false;
            this.WindowState = FormWindowState.Minimized;
            if (!this._NI.Visible)
                this._NI.Visible = true;
            this.Hide();
            // manage menue
            this._cmNI_hide.Enabled = false;
            this._cmNI_show.Enabled = true;
        }
        private void ShowApp()
        {
            // show application
            this.Show();
            this.WindowState = FormWindowState.Normal;
            this.ShowInTaskbar = true;
            if (!uaSett.ShowSystrayIcon)
                this._NI.Visible = false;
            // manage menu
            this._cmNI_show.Enabled = false;
            this._cmNI_hide.Enabled = true;
        }
        private bool PrepareExit()
        {	// init eventargs
            ControlAppEventArgs cae = new ControlAppEventArgs();
            cae.RestartApp = false;
            // check if we shall show?
            if (NyxMain.uaSett.WindowExit)
            {
                // new exit point
                string exitmsg = GuiHelper.GetResourceMsg("ExitNyx");
                StructRetDialog ret = MessageDialog.ShowMsgDialog(exitmsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question, true);
                if (!NyxMain.uaSett.WindowExit.Equals(ret.ShowAgain))
                {
                    NyxMain.uaSett.WindowExit = ret.ShowAgain;
                    if (!Settings.SerializeUserSettings(NyxMain.UASett, Settings.GetFolder(SettingsType.AppSettings)))
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("UserSettingsSaveErr", new string[] { Settings.ErrExc.Message.ToString() }),
                            Settings.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
                // check the dialogresult and exit if needed
                if (ret.DRes == NyxDialogResult.Yes)
                {   // fire event, return true
                    OnControlApp(cae);
                    return true;
                }
                else
                    return false;
            }
            else
            {   // fire event, return true
                OnControlApp(cae);
                return true;
            }
        }
        private void _HideApp(object sender, EventArgs e)
        {
            HideApp();
        }
        private void _ShowApp(object sender, EventArgs e)
        {
            ShowApp();
        }
        private void _ExitApp(object sender, System.EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized)
                ShowApp();
            PrepareExit();
        }
        #endregion

        #region History
        /// <summary>
        /// Add query to history (format / save / refresh)
        /// </summary>
        /// <param name="qry">querystring to add</param>
        /// <param name="limit">query limit, if used</param>
        public void AddHistory(string qry, int limit)
        {   // format query before saving 
            if (limit > 0 && NyxMain.UASett.HistorySaveQueryLimit)
                qry = dbh.ParseSqlQuery(qry, limit, false);
            else
                qry = dbh.ParseSqlQuery(qry, 0, false);
            // save now
            History his = new History();
            if (!his.AddHis(qry))
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrHistorySave", new string[] { his.ErrExc.ToString() }),
                            his.ErrExc, MessageHandler.EnumMsgType.Warning);
            // finally refresh
            his.SetHis(this._cbSqlHistory);
        }
        private void _cbSqlHistory_SelectionChangeCommitted(object sender, EventArgs e)
        {   // got a selection?
            if (this._cbSqlHistory.SelectedIndex > -1 && this._tcSql.SelectedIndex >= 0)
            {   // no default items?
                if (this._cbSqlHistory.SelectedItem.ToString() != "No history logged."
                    && this._cbSqlHistory.SelectedItem.ToString() != "select query (history)...")
                {   // set selection to non-default item
                    SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                    smt.ReplaceSqlRtbTxt(this._cbSqlHistory.SelectedItem.ToString());
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Fire OnControl-Event
        /// </summary>
        /// <param name="e"></param>
        protected void OnControlApp(ControlAppEventArgs e)
        {   // fire event
            if (OnControlAppHandler != null)
                OnControlAppHandler(this, e);
        }
        /// <summary>
        /// EventHandlerfunctions for ControlApp
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ControlAppEvent(object sender, ControlAppEventArgs e)
        {   // disconnect database
            if (dbh != null)
            {
                dbh.DisconnectDB();
                dbh = null;
            }
            // save
            SaveRuntimeSettings();
            // removce icon from the systray
            this._NI.Visible = false;
            // restart or exit
            if (e.RestartApp)
                Application.Restart();
            else
                Application.Exit();
        }
        /// <summary>
        /// EventHandlerfunctions for SettingsChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SettingsChanged(object sender, SettingsChangedEventArgs e)
        {
            if (e == null)
                return;
            switch (e.SettingsType)
            {
                case SettingsType.Bookmarks:
                    SetBookmarks();         // refresh
                    break;
                case SettingsType.AppSettings:
                    SetUserAppSettings();   // refresh
                    LoadHotKeys();
                    break;
                case SettingsType.Profile:
                    SetProfiles(false);     // refresh
                    break;
                case SettingsType.Hotkeys:
                    LoadHotKeys();          // refresh
                    break;
            }
        }
        /// <summary>
        /// EventHandlerfunctions for DBContentChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DBContentChanged(object sender, DBContentChangedEventArgs e)
        {   // check what has changed
            switch (e.DBContent)
            {
                case DatabaseContent.TablesViewsProcedures:
                    DbContentInit(true, true, true);
                    break;
                case DatabaseContent.Tables:
                    DbContentInit(true, false, false);
                    break;
                case DatabaseContent.TableFields:
                    if (this._tsTables_curTable.Text.ToString() == e.Param1)
                        GetTableFullDesc(e.Param1);
                    break;
                case DatabaseContent.Indexes:
                    if (this._tsTables_curTable.Text.ToString() == e.Param1)
                        GetIndexes();
                    break;
                case DatabaseContent.Views:
                    DbContentInit(false, true, false);
                    break;
                case DatabaseContent.Procedures:
                    DbContentInit(false, false, true);
                    break;
            }
        }
        #endregion

        #region MainToolStrip, TabControl
        private void ChangeDb()
        {   // new database check
            string newDb = this._tsMain_cbChangeDb.Text.ToString();
            if (newDb.Length <= 0 || newDb == NyxMain.DBName)
                return;
            // display thread
            CtrlAppDisplay("Changing database to " + newDb + "...");
            // init thread
            DBThreadStartParam strThrStart = new DBThreadStartParam();
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql: strThrStart.Param1 = "use `" + newDb + "`"; break;
                case DBType.MSSql: strThrStart.Param1 = "use " + newDb + ""; break;
            }
            strThrStart.ThrAction = DatabaseAction.ChangeDB;
            // start thread...
            DBThreadInit(strThrStart);
        }
        private void ChangeDbDone(DBThreadReturnParam strThrReturn)
        {
            if (strThrReturn.DBException == null)
            {
                NyxMain.DBName = this._tsMain_cbChangeDb.Text.ToString();
                this.Text = GuiHelper.GetResourceMsg("NS_ConnectedHl", new string[] { NyxMain.DBName, NyxMain.DBVersion });
                // reset the gui
                ResetGui();
            }
            else
            {   // set database back
                this._tsMain_cbChangeDb.SelectedItem = DBName;
                // err
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrDbChange", new string[] { strThrReturn.Data1.ToString(), strThrReturn.DBException.Message.ToString() }),
                            strThrReturn.DBException, MessageHandler.EnumMsgType.Warning);
            }
            // activate control again
            CtrlAppDisplay(String.Empty);
        }

        private void _tcMain_SelectedIndexChanged(object sender, EventArgs e)
        {   // first disable preview and sql
            this._msSql.Enabled = false;
            this._msPrv.Enabled = false;
            //
            // normal sql select
            //
            if (this._tcMain.SelectedTab == this._tpSql)
            {   // set view menustrip
                this._msSql.Enabled = true;
                CtrlMenuStripViewChecked("query");
                // select multitab
                SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                smt.Select();
            }
            //
            // we don't support the tables and so on on odbc databases
            //
            else if (NyxMain.ConProfile.ProfileDBType == DBType.Odbc)
            {
                MessageHandler.ShowDialog(
                    "Sorry, this feature isn't supported on this database at the moment.",
                    MessageHandler.EnumMsgType.Information);
                this._tcMain.SelectedTab = this._tpSql;
                return;
            }
            //
            // tables
            //
            else if (this._tcMain.SelectedTab == this._tpTables
                && this._pnlTables.Visible)
            {
                CtrlMenuStripViewChecked("tables");
                if (this._lbTables.Items.Count == 0)
                    GetTables();
            }
            //
            // preview
            //
            else if (this._tcMain.SelectedTab == this._tpPreview
                && this._pnlPreview.Visible)
            {   // dataset allready initiated?
                if (dsPreview == null)
                {
                    dsPreview = new DataSet();
                    dsPreview.Locale = NyxMain.nyxCI;
                }
                // set view menustrip
                this._msPrv.Enabled = true;
                CtrlMenuStripViewChecked("preview");
                // retrieve initial
                if (this._lvPrvObjects.Items.Count == 0)
                    GetPreviewObjects();
            }
            //
            // views
            //
            else if (this._tcMain.SelectedTab == this._tpViews)
            {
                CtrlMenuStripViewChecked("views");
                // allready retrieved views?
                if (this._lbViews.Items.Count == 0 && this._pnlViews.Visible)
                    GetViews();
            }
            //
            // procedures
            //
            else if (this._tcMain.SelectedTab == this._tpProcedures)
            {
                CtrlMenuStripViewChecked("procedures");
                // allready retrieved procedures?
                if (this._lbPrcdrs.Items.Count == 0 && this._pnlProcedures.Visible)
                    GetProcedures();
            }
            // 
            // triggers
            //
            else if (this._tcMain.SelectedTab == this._tpTriggers)
            {
                CtrlMenuStripViewChecked("triggers");
                // allready retrieved triggers?
                if (this._lbTriggers.Items.Count == 0 && this._pnlTriggers.Visible)
                    GetTriggers();
            }
        }
        private void _Main_Connect_Click(object sender, EventArgs e)
        {
            DbConnect();
        }
        private void _tsMain_cbProfiles_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                DbConnect();
        }
        private void _Main_Disconnect_Click(object sender, System.EventArgs e)
        {
            DbDisconnect();
        }
        private void _Main_Break_Click(object sender, EventArgs e)
        {   // if thread is running, abort...
            if (doWork.ThreadState == System.Threading.ThreadState.Background)
                doWork.Abort();
        }
        private void _Main_QuickConnect_Click(object sender, EventArgs e)
        {   // vars
            ProfileSettings prf = new ProfileSettings();
            QuickConnect qc = new QuickConnect();
            // gui
            this._tsMain_cbProfiles.SelectedIndex = 0;
            // get quickconnect
            DialogResult dres = qc.GetQuickConnect(ref prf);
            if (dres == DialogResult.OK)
            {   // set the quickconnect
                NyxMain.conProfile = prf;
                // gui
                this._tsMain_SaveQC.Enabled = false;
                this._tsMain_cbProfiles.SelectedItem = "QuickConnect";
                // finally the dbconnect
                DbConnect();
            }
            else
                this._tsMain_SaveQC.Enabled = false;
        }
        private void _tsMain_SaveQC_Click(object sender, EventArgs e)
        {   
            bool showInptBox = true;
            while (showInptBox)
            {
                // check if we already got the profile name
                string prfName = String.Empty, prfInput = GuiHelper.GetResourceMsg("Profile_NameInput");
                DialogResult dresInput = InputBox.ShowInputBox(GuiHelper.GetResourceMsg("Profile_Name"), prfInput, ref prfName);
                if (dresInput == DialogResult.OK)
                {   // invalid name
                    if (prfName.Length == 0 || prfName == prfInput)
                    {
                        StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("Profile_NameInvalid"),
                            MessageBoxButtons.RetryCancel, MessageBoxIcon.Error, false);
                        if (strRD.DRes == NyxDialogResult.Cancel)
                            showInptBox = false;
                    }
                    // allready here
                    else if (NyxMain.htProfiles.ContainsKey(prfName))
                    {
                        StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("Profile_ProfileExist"),
                            MessageBoxButtons.RetryCancel, MessageBoxIcon.Warning, false);
                        if (strRD.DRes == NyxDialogResult.Cancel)
                            showInptBox = false;
                    }
                    else
                    {   // crt temp profile
                        ProfileSettings tmpPrf = NyxMain.conProfile;
                        tmpPrf.ProfileName = prfName;
                        // add / edit hashtable and afterwards serialize
                        NyxMain.HTProfiles[prfName] = tmpPrf;
                        Settings.SerializeProfiles(NyxMain.HTProfiles, Settings.GetFolder(SettingsType.Profile));
                        // refreshing profiles
                        SetProfiles(false);
                        // setting new text
                        this._tsMain_cbProfiles.SelectedItem = prfName;
                        this._tsMain_SaveQC.Enabled = false;
                        // everything done
                        showInptBox = false;
                    }
                }
            }
        }
        private void _tsMain_Profiles_SelectedIndexChanged(object sender, EventArgs e)
        {   // checkup
            if (NyxMain.htProfiles.ContainsKey(this._tsMain_cbProfiles.SelectedItem))
            {   // get profile //refactor
                ProfileSettings profile = (ProfileSettings)NyxMain.htProfiles[this._tsMain_cbProfiles.SelectedItem];
                NyxMain.conProfile = profile;
                // set statustext
                this._tsMain_SaveQC.Enabled = false;
                this._sS_lblStatusMsg.Text = GuiHelper.GetResourceMsg("Profile_Loaded", new string[] { profile.ProfileName });
            }
            // reset
            else
                NyxMain.conProfile = new ProfileSettings();
        }
        private void _tsMain_cbChgDb_SelectedIndexChanged(object sender, EventArgs e) { ChangeDb(); }
        private void _tsMain_cbChgDb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                ChangeDb();
        }
        private void _msBookmark_Click(object sender, EventArgs e)
        {   // add bookmark
            ToolStripMenuItem tiSender = (ToolStripMenuItem)sender;
            // search after
            DataRow[] dr = dtBookmarks.Select("name = '" + tiSender.Name + "'");
            if (dr.Length == 1)
            {
                SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                smt.ReplaceSqlRtbTxt(dr[0]["query"].ToString());
            }
        }
        private void _msBookmarksMng_Click(object sender, EventArgs e)
        {
            BookmarkManager bm = new BookmarkManager();
            bm.SettingChangedEvent += new EventHandler<SettingsChangedEventArgs>(SettingsChanged);
            bm.Show();
        }
        private void _msBookmarks_newentry_Click(object sender, EventArgs e)
        {
            BookmarkManager bm = new BookmarkManager();
            bm.AddEntry(false);
            // refresh
            SetBookmarks();
        }
        private void _msBookmarks_newcat_Click(object sender, EventArgs e)
        {   // init
            BookmarkManager bm = new BookmarkManager();
            bm.AddCategory(false);
            // refresh
            SetBookmarks();
        }
        private void _msAbout_Click(object sender, System.EventArgs e)
        {
            About fa = new About();
            fa.ShowDialog();
        }
        private void _msClear_Click(object sender, System.EventArgs e)
        {
            SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
            // clearing resources
            smt.Clear();
        }
        private void _msProfiles_Click(object sender, EventArgs e)
        {
            ProfileManager fp = new ProfileManager();
            fp.SettingsChangedEvent += new EventHandler<SettingsChangedEventArgs>(SettingsChanged);
            fp.Show();
        }
        private void _msCheckUpdate_Click(object sender, EventArgs e)
        {
            if (System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major == 666)
                MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("NyxDevRelease"), MessageHandler.EnumMsgType.Information);
            else
                StartUpdater();
        }
        private void _msClearHis_Click(object sender, EventArgs e)
        {   // first clear, then reload
            History his = new History();
            his.ClearHis();
            his.SetHis(this._cbSqlHistory);
        }
        private void _msSettings_Click(object sender, EventArgs e)
        {
            UserSettingsManager fo = new UserSettingsManager();
            fo.ControlAppEvent += new EventHandler<ControlAppEventArgs>(ControlAppEvent); 
            fo.SettingChangedEvent += new EventHandler<SettingsChangedEventArgs>(SettingsChanged);
            fo.ShowDialog();
        }
        private void _msDbUser_Click(object sender, EventArgs e)
        {
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.MySql:
                    NyxMySql.MySqlUser fmu = new NyxMySql.MySqlUser();
                    fmu.Show();
                    break;
            }
        }
        private void _msDbProcesses_Click(object sender, System.EventArgs e)
        {
            GeneralDB.ProcessMng procMng = new GeneralDB.ProcessMng();
            procMng.Show();
        }
        private void _msDbInfo_Click(object sender, EventArgs e)
        {
            switch (NyxMain.ConProfile.ProfileDBType)
            {
                case DBType.Oracle:
                    NyxOrcl.OrclDBInfo fdbi = new NyxOrcl.OrclDBInfo();
                    fdbi.Show();
                    break;
                case DBType.MySql:
                    NyxMySql.MySqlDBInfo mysqlDbInfo = new NyxMySql.MySqlDBInfo();
                    mysqlDbInfo.Show();
                    break;
            }
        }
        private void _msDbTableStats_Click(object sender, EventArgs e)
        {   // show dialog
            GeneralDB.TableStatistics tStat = new GeneralDB.TableStatistics();
            tStat.Show();
        }
        private void _msOrcl_tsinfo_Click(object sender, System.EventArgs e)
        {
            NyxOrcl.OrclTablespaces ts = new NyxOrcl.OrclTablespaces();
            ts.Show();
        }
        private void _msOrcl_initinfo_Click(object sender, EventArgs e)
        {
            NyxOrcl.OrclInitInfo fii = new NyxOrcl.OrclInitInfo();
            fii.Show();
        }
        private void _msOrcl_recbin_Click(object sender, EventArgs e)
        {
            NyxOrcl.OrclRecycleBin recbin = new NyxOrcl.OrclRecycleBin();
            recbin.Show();
        }
        private void _msMySql_crtdb_Click(object sender, EventArgs e)
        {
            string newDB = null;
            DialogResult dres = InputBox.ShowInputBox("create Database", GuiHelper.GetResourceMsg("DatabaseNamePrompt"), ref newDB);
            if (dres == DialogResult.OK)
            {
                if (newDB == GuiHelper.GetResourceMsg("DatabaseNamePrompt"))
                {
                    // err
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrDbInvalid", new string[] { newDB }),
                                MessageHandler.EnumMsgType.Warning);
                }
                else
                {
                    string qry = String.Format(NyxMain.nyxCI, "CREATE DATABASE `{0}`;use `{0}`", newDB);
                    if (dbh.ExecuteNonQuery(qry))
                    {   // set internal var and gui
                        this._tsMain_cbChangeDb.Items.Add(newDB);
                        this._tsMain_cbChangeDb.SelectedItem = newDB;
                        SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                        smt.AddErrorLogText("Database " + newDB + " created.");
                        ChangeDb();
                    }
                    else
                    {
                        SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                        smt.AddErrorLogText(GuiHelper.GetResourceMsg("DatabaseCreateErr", new string[] { dbh.ErrExc.Message.ToString() }));
                        this._sS_lblStatusMsg.Text = GuiHelper.GetResourceMsg("DatabaseCreateErr", new string[] { dbh.ErrExc.Message.ToString() });
                        // err
                        MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrDbCreate", new string[] { newDB, dbm.ErrExc.Message.ToString() }),
                                    dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                    }
                }
            }
        }
        private void _msMySql_dropdb_Click(object sender, EventArgs e)
        {
            StructRetDialog strRD = MessageDialog.ShowMsgDialog(GuiHelper.GetResourceMsg("DatabaseDrop", new string[] { DBName }),
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, false);
            if (strRD.DRes == NyxDialogResult.Yes)
            {
                string qry = String.Format(NyxMain.nyxCI, "DROP DATABASE `{0}`;use mysql", this._tsMain_cbChangeDb.SelectedText.ToString());
                if (dbh.ExecuteNonQuery(qry))
                {   // set to default mysql database
                    SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                    this._tsMain_cbChangeDb.Items.Remove(NyxMain.conProfile.ProfileDB);
                    NyxMain.conProfile.ProfileDB = "mysql";
                    this._tsMain_cbChangeDb.SelectedItem = "mysql";

                    smt.AddErrorLogText(GuiHelper.GetResourceMsg("DatabaseDropped", new string[] { DBName } ));
                }
                else
                {
                    SqlTab smt = (SqlTab)this._tcSql.SelectedTab.Tag;
                    string errmsg = GuiHelper.GetResourceMsg("DatabaseDropErr", new string[] { DBName, dbh.ErrExc.Message.ToString() });
                    smt.AddErrorLogText(errmsg);
                    MessageHandler.ShowDialog(errmsg, dbh.ErrExc, MessageHandler.EnumMsgType.Warning);
                }
            }
        }
        private void _msMySql_export_Click(object sender, EventArgs e)
        {
            NyxMySql.MySqlDBExport fdbe = new NyxMySql.MySqlDBExport();
            fdbe.Show();
        }
        private void _msMySql_import_Click(object sender, EventArgs e)
        {
            NyxMySql.MySqlDBImport dbi = new NyxMySql.MySqlDBImport();
            dbi.Show();
        }
        private void _msView_Click(object sender, EventArgs e)
        {
            ToolStripItem tsi = (ToolStripItem)sender;
            if (tsi.Name == this._msView_Sql.Name)
                this._tcMain.SelectedTab = this._tpSql;

            else if (tsi.Name == this._msView_Tables.Name)
                this._tcMain.SelectedTab = this._tpTables;

            else if (tsi.Name == this._msView_Preview.Name)
                this._tcMain.SelectedTab = this._tpPreview;

            else if (tsi.Name == this._msView_Views.Name)
                this._tcMain.SelectedTab = this._tpViews;

            else if (tsi.Name == this._msView_Prcdrs.Name)
                this._tcMain.SelectedTab = this._tpProcedures;

            else if (tsi.Name == this._msView_Triggers.Name)
                this._tcMain.SelectedTab = this._tpTriggers;

            else if (tsi.Name == this._msViewTStrips_Main.Name)
                this._tsMain.Visible = this._msViewTStrips_Main.Checked;
        }
        private void _msDb_SqlTabs_NewTab_Click(object sender, EventArgs e)
        {
            SqlCreateNewTab(true);
        }
        private void _msDb_SqlTabs_CloseTab_Click(object sender, EventArgs e)
        {
            SqlCloseTab();
        }
        #endregion

        #region misc. Helpfunctions
        private void ResetGui()
        {   // general
            this._tcMain.SelectedTab = this._tpSql;
            // sqlmultitab
            SqlTab smt;
            foreach (TabPage tp in this._tcSql.TabPages)
            {
                smt = (SqlTab)tp.Tag;
                smt.ResetGui();
            }
            // sqlbuilder
            this._splitSqlBldr.Panel2Collapsed = true;
            this._trvSqlBldr.Nodes["Tables"].Nodes.Clear();
            this._trvSqlBldr.Nodes["Views"].Nodes.Clear();
            this._trvSqlBldr.Nodes["Procedures"].Nodes.Clear();
            this._trvSqlBldr.Nodes["reservedWords"].Nodes.Clear();
            // tables
            this._lbTables.Items.Clear();
            this._lvTableDesc.Items.Clear();
            this._tsTables_curTable.Text = "";
            this._tsTables_Filter.Text = "";
            this._tsTables_tbDescTable.Text = "";
            tblFilterList.Clear();
            this._lvIndexes.Items.Clear();
            this._lvIndexDesc.Items.Clear();
            // scopes
            CtrlScopeBoxes(true);
            // preview
            this._tsPrvDta_RowsPerPage.SelectedItem = "50";
            this._lvPrvObjects.Items.Clear();
            this._tsPrvObj_Filter.Text = "";
            prvFilterList.Clear();
            this._dgPreview.DataSource = null;
            SetPreviewButtons(true);
            // triggers
            this._lbTriggers.Items.Clear();
            this._lvTrigger_Details.Items.Clear();
            this._rtbTrigger_CrtStm.Text = "";
            this._tsTriggers_Filter.Text = "";
            triggerFilterList.Clear();
            // views
            this._lbViews.Items.Clear();
            this._lvView_Details.Items.Clear();
            this._rtbView_CreateStm.Text = "";
            this._tsViews_Filter.Text = "";
            viewFilterList.Clear();
            // procedures
            this._lbPrcdrs.Items.Clear();
            this._lvPrcdr_Details.Items.Clear();
            this._rtbPrcdr_CreateStm.Text = "";
            this._tsPrcdrs_Filter.Text = "";
            prcdrFilterList.Clear();
        }
        private static void StartUpdater()
        {
            if (System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.Major != 666)
            {
                if (System.IO.File.Exists("NyxUpdate.exe"))
                {
                    System.Diagnostics.Process.Start("NyxUpdate.exe");
                    // set lastcheckdatetime
                    rtSett.LastUpdatecheck = DateTime.Now;
                }
                else
                    MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("NyxNoUpdater"), MessageHandler.EnumMsgType.Warning);
            }
        }

        /// <summary>
        /// Enables / Disables the GUI while Thread is running
        /// </summary>
        /// <param name="message">Statusbar message</param>
        public void CtrlAppDisplay(object message)
        {   // if ProgressBarStyle.Marquee GUI disabled, we'll enable it
            if (this._ssProgress.Style == ProgressBarStyle.Marquee)
            {   // set progressbar
                this._ssProgress.Style = ProgressBarStyle.Blocks;
                this._ssProgress.Value = 0;
                // statusbar
                this._sS_lblStatusMsg.Text = String.Format(NyxMain.NyxCI, "{0}{1}", this._sS_lblStatusMsg.Text, "done");
                // cursor
                this.Cursor = Cursors.Default;
                // toolstrips
                this._tsMain.Enabled = true;
                if (!this._tsMain_Connect.Enabled)
                    this._tsMain_Disconnect.Enabled = true;
                if (NyxMain.ConProfile.ProfileDBType == DBType.MySql || NyxMain.ConProfile.ProfileDBType == DBType.MSSql)
                    this._tsMain_cbChangeDb.Enabled = true;
                if (this._tsMain_cbProfiles.Text.ToString() == GuiHelper.GetResourceMsg("QuickConnect"))
                    this._tsMain_SaveQC.Enabled = true;
                // finally the tabcontrol
                this._tcMain.Enabled = true;
            }
            else
            {
                // at first the tabcontrol
                this._tcMain.Enabled = false;
                // set progress things
                this._ssProgress.Style = ProgressBarStyle.Marquee;
                this._ssProgress.MarqueeAnimationSpeed = 100;
                this._sS_lblStatusMsg.Text = (string)message;
                this.Cursor = Cursors.AppStarting;
                // tsmain
                foreach (ToolStripItem tsi in this._tsMain.Items)
                    tsi.Enabled = false;
                // mainmenustrips
                this._msView.Enabled = false;
                if (this._tcMain.SelectedTab == this._tpSql)
                    this._msSql.Enabled = true;
                else
                    this._msSql.Enabled = false;

                if (this._tcMain.SelectedTab == this._tpPreview)
                    this._msPrv.Enabled = true;
                else
                    this._msPrv.Enabled = false;
            }
        }
        private void CtrlMenuStripViewChecked(string menustrip)
        {   // first reset all to false
            this._msView_Sql.Checked = false;
            this._msView_Tables.Checked = false;
            this._msView_Preview.Checked = false;
            this._msView_Views.Checked = false;
            this._msView_Prcdrs.Checked = false;
            this._msView_Triggers.Checked = false;
            // check which to enable
            switch (menustrip)
            {
                case "query": this._msView_Sql.Checked = true; break;
                case "tables": this._msView_Tables.Checked = true; break;
                case "preview": this._msView_Preview.Checked = true; break;
                case "views": this._msView_Views.Checked = true; break;
                case "procedures": this._msView_Prcdrs.Checked = true; break;
                case "triggers": this._msView_Triggers.Checked = true; break;
            }
        }
        private void CtrlConnectButtons(bool connectEnabled)
        {
            bool disconEnabled = true;
            if (connectEnabled)
                disconEnabled = false;
            // connect/disconnect buttons
            this._tsMain_Connect.Enabled = connectEnabled;
            this._msFile_Connect.Enabled = connectEnabled;
            this._tsMain_Disconnect.Enabled = disconEnabled;
            this._msFile_Disconnect.Enabled = disconEnabled;
            this._tsMain_cbProfiles.Enabled = connectEnabled;
            this._tsMain_QuickConnect.Enabled = connectEnabled;
            this._tsMain_cbChangeDb.Visible = disconEnabled;
            if (connectEnabled)
            {
                if (this._tsMain_cbProfiles.SelectedItem.ToString() == "QuickConnect")
                    this._tsMain_SaveQC.Enabled = true;
                else
                    this._tsMain_SaveQC.Enabled = false;
            }
            else
                this._tsMain_cbProfiles.Focus();
        }
        private void CtrlViewMenuStrip(bool enableState)
        {
            this._msView_Sql.Enabled = enableState;
            this._msView_Tables.Enabled = enableState;
            this._msView_Preview.Enabled = enableState;
            this._msView_Views.Enabled = enableState;
            this._msView_Prcdrs.Enabled = enableState;
            this._msView_Triggers.Enabled = enableState;
        }
        private void CtrlScopeBoxes(bool enableState)
        {
            this._tsTables_cbScope.Enabled = enableState;
            this._tsPrvObj_Scope.Enabled = enableState;
            this._tsViews_cbScopeve.Enabled = enableState;
            this._tsTriggers_cbScope.Enabled = enableState;
            this._tsPrcdrs_cbScope.Enabled = enableState;
            // set selectedindex to 0 if enable
            if (enableState)
            {
                this._tsTables_cbScope.SelectedIndex = 0;
                this._tsPrvObj_Scope.SelectedIndex = 0;
                this._tsViews_cbScopeve.SelectedIndex = 0;
                this._tsTriggers_cbScope.SelectedIndex = 0;
                this._tsPrcdrs_cbScope.SelectedIndex = 0;
            }
        }
        private void CtrlPanelVisiblity(bool visible)
        {
            if (visible)
            {   // show the panels
                this._pnlTables.Show();
                this._pnlPreview.Show();
                this._pnlProcedures.Show();
                this._pnlViews.Show();
                this._pnlTriggers.Show();
            }
            else
            {
                this._pnlTables.Hide();
                this._pnlPreview.Hide();
                this._pnlProcedures.Hide();
                this._pnlViews.Hide();
                this._pnlTriggers.Hide();
            }
        }
        private void DisableContextMenu()
        {   // table contextmenu
            this._cmTable_Alter.Enabled = false;
            this._cmTable_Create.Enabled = false;
            this._cmTable_Describe.Enabled = false;
            this._cmTable_Diag.Enabled = false;
            this._cmTable_Drop.Enabled = false;
            this._cmTable_Truncate.Enabled = false;
            this._cmTableDiag_Analyze.Enabled = false;
            this._cmTableDiag_Check.Enabled = false;
            this._cmTableDiag_OptReb.Enabled = false;
            this._cmTableDiag_Repair.Enabled = false;
            // tablefield contextmenu
            this._cmTblField_Alter.Enabled = false;
            this._cmTblField_Create.Enabled = false;
            this._cmTblField_Drop.Enabled = false;
            // index contextmenu
            this._cmIndex_Alter.Enabled = false;
            this._cmIndex_Create.Enabled = false;
            this._cmIndex_Describe.Enabled = false;
            this._cmIndex_Drop.Enabled = false;
        }
        #endregion

    }
}