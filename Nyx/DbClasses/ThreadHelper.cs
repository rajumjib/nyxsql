/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using Nyx.Classes;

namespace Nyx.Classes
{
    class ThreadHelper
    {
        DBManagement dbm = null;
     
        public ThreadHelper(DBManagement dbm)
        {
            this.dbm = dbm;
        }

        public static void ExecSelQuery(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {
            DBHelper dbh = strThrStart.Dbh;
            System.Data.DataSet ds = new System.Data.DataSet("NyxQueryResult");
            ds.Locale = NyxMain.NyxCI;

            if (!dbh.FillDS(strThrStart.Param1, ref ds))
                strThrReturn.DBException = dbh.ErrExc;
            else
            {
                strThrReturn.Data1 = (object)ds;
                strThrReturn.Data2 = strThrStart.Param1;
            }
        }
        public static void ExecNonQuery(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // preset some vars
            strThrReturn.Data1 = strThrStart.Param1;
            DBHelper dbh = strThrStart.Dbh;
            // change database
            if (!strThrStart.Dbh.ExecuteNonQuery(strThrStart.Param1))
                strThrReturn.DBException = dbh.ErrExc;
            else
            {
                strThrReturn.Data2 = strThrStart.Param1;
                strThrReturn.AffRows = dbh.RecordsAffected;
            }
        }
        public void GetPreviewObjects(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get the objects for preview (views,tables and so on...)
            List<PreviewObject> lpo;
            if (!dbm.PreviewGetObjects((bool)strThrStart.Obj, strThrStart.Dbh, out lpo))
                strThrReturn.DBException = dbm.ErrExc;
            else
                strThrReturn.Data1 = lpo;
        }
        public void InitPreview(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // check
            PreviewQuery prvQry = strThrStart.Obj as PreviewQuery;
            if (prvQry != null)
            {   // init vars
                int rowCnt;
                System.Data.DataSet ds;
                Queue<String> qTblFields;
                // retrieve data
                if (dbm.PreviewInit(prvQry, out rowCnt, out ds, out qTblFields, strThrStart.Dbh, strThrStart.ThrAction))
                {
                    strThrReturn.DS = ds;
                    strThrReturn.AffRows = rowCnt;
                    strThrReturn.SetGQString1(qTblFields);
                }
                else
                    strThrReturn.DBException = dbm.ErrExc;
                // finally save query
                strThrReturn.Data1 = strThrStart.Param1;
                strThrReturn.Query = dbm.LastQuery;
            }
            else
                strThrReturn.DBException = new DBException();
        }
        public void GetPreviewNextLastPrev(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get data
            System.Data.DataSet ds;
            PreviewQuery prvQry = strThrStart.Obj as PreviewQuery;
            if (prvQry != null)
            {
                if (dbm.PreviewGet(prvQry, out ds, strThrStart.Dbh))
                    strThrReturn.DS = ds;
                else
                    strThrReturn.DBException = dbm.ErrExc;
                strThrReturn.Data1 = strThrStart.Param1;
            }
        }
        public void InitSqlBldr(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // init vars
            bool[] options = (bool[])strThrStart.Obj;
            // get tables
            if (options[0])
            {
                Queue<string> qstr = new Queue<string>();
                if (dbm.GetTables(out qstr, true, strThrStart.Dbh))
                    strThrReturn.SetGQString1(qstr);
                else
                    strThrReturn.DBException = dbm.ErrExc;
            }
            // get views
            if (options[1] && strThrReturn.DBException == null)
            {
                Queue<string> qstr = new Queue<string>();
                if (dbm.GetViews(out qstr, true, strThrStart.Dbh))
                    strThrReturn.SetGQString2(qstr);
                else
                    strThrReturn.DBException = dbm.ErrExc;
            }
            // get procedures
            if (options[2] && strThrReturn.DBException == null)
            {
                Queue<string> qstr = new Queue<string>();
                if (dbm.GetProcedures(out qstr, true, strThrStart.Dbh))
                    strThrReturn.SetGQString3(qstr);
                else
                    strThrReturn.DBException = dbm.ErrExc;
            }
        }
        public void GetTables(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get the tables
            Queue<string> qstr = new Queue<string>();
            if (dbm.GetTables(out qstr, (bool)strThrStart.Obj, strThrStart.Dbh))
                strThrReturn.SetGQString1(qstr);
            else
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetTableDetails(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get the describe
            Queue<TableDetails> qTblDesc;
            if (dbm.GetTableDetails(strThrStart.Param1, out qTblDesc, strThrStart.Dbh))
                strThrReturn.Data1 = qTblDesc;
            else 
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetTableFullDesc(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get the describe
            Queue<TableDetails> qTblDesc2;
            Queue<IndexDetails> qIndexes2;
            if (dbm.GetTableDetails(strThrStart.Param1, out qTblDesc2, strThrStart.Dbh))
                strThrReturn.Data1 = qTblDesc2;
            else 
                strThrReturn.DBException = dbm.ErrExc;
            // get the indexes
            if (dbm.GetIndexes(strThrStart.Param1, out qIndexes2, strThrStart.Dbh))
                strThrReturn.Data2 = qIndexes2;
            else 
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetIndexes(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get the indexes
            Queue<IndexDetails> qIndexes;
            if (dbm.GetIndexes(strThrStart.Param1, out qIndexes, strThrStart.Dbh))
                strThrReturn.Data1 = qIndexes;
            else 
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetIndexDescribe(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get the indexdescribe
            Queue<IndexDescribe> qIndexDesc;
            if (dbm.GetIndexDetails(strThrStart.Param1, strThrStart.Param2, out qIndexDesc, strThrStart.Dbh))
                strThrReturn.Data1 = qIndexDesc;
            else 
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetViews(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get the view
            Queue<string> qstr = new Queue<string>();
            if (dbm.GetViews(out qstr, (bool)strThrStart.Obj, strThrStart.Dbh))
                strThrReturn.SetGQString1(qstr);
            else
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetViewDetails(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get view details
            ViewDetails strViewDet;
            if (dbm.GetViewDetails(strThrStart.Param1, out strViewDet, strThrStart.Dbh))
                strThrReturn.Data1 = strViewDet;
            else 
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetProcedures(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get procedures
            Queue<string> qstr = new Queue<string>();
            if (dbm.GetProcedures(out qstr, (bool)strThrStart.Obj, strThrStart.Dbh))
                strThrReturn.SetGQString1(qstr);
            else
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetProcedureDetails(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get procedure details
            ProcedureDetails strProcedureDet;
            if (dbm.GetProcedureDetails(strThrStart.Param1, out strProcedureDet, strThrStart.Dbh))
                strThrReturn.Data1 = strProcedureDet;
            else strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetTriggers(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get triggers
            Queue<string> qstr = new Queue<string>();
            if (dbm.GetTriggers(out qstr, (bool)strThrStart.Obj, strThrStart.Dbh))
                strThrReturn.SetGQString1(qstr);
            else
                strThrReturn.DBException = dbm.ErrExc;
        }
        public void GetTriggerDetails(DBThreadStartParam strThrStart, ref DBThreadReturnParam strThrReturn)
        {   // get trigger details
            TriggerDetails strTriggerDet;
            if (dbm.GetTriggerDetails(strThrStart.Param1, out strTriggerDet, strThrStart.Dbh))
                strThrReturn.Data1 = strTriggerDet;
            else strThrReturn.DBException = dbm.ErrExc;
        }
    }
}
