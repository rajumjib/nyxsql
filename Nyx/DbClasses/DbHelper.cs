/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using Nyx.Classes;
using NyxSettings;
using System.Data.SqlClient;
using Oracle.DataAccess.Client;
using MySql.Data.MySqlClient;
using Npgsql;
using System.Data.Odbc;

using System.Data.Common;

namespace Nyx.Classes
{
    /// <summary> Database helper(wrapper) class. calls underlaying database classes </summary>
    public class DBHelper
    {
        /// <summary> querytype, used for getting the query statement </summary>
        public enum EnumGetQueryType { 
            /// <remarks> databases query </remarks>
            Databases,
            /// <remarks> usertables query </remarks>
            UserTables,
            /// <remarks> alltables query </remarks>
            AllTables,
            /// <remarks> tabledetails query </remarks>
            TableColumns,
            /// <remarks> table create statement </remarks>
            TableCreateStm,
            /// <remarks> userviews query </remarks>
            UserViews,
            /// <remarks> allviews query </remarks>
            AllViews,
            /// <remarks> viewdetails query </remarks>
            ViewDetails,
            /// <remarks> viewdetails statement query </remarks>
            ViewDetailsStm,
            /// <remarks> indexes query </remarks>
            Indexes,
            /// <remarks> indexdetails query </remarks>
            IndexDetails,
            /// <remarks> indexed columns query </remarks>
            IndexedColumns,
            /// <remarks> usertriggers query </remarks>
            UserTriggers,
            /// <remarks> alltriggers query </remarks>
            AllTriggers,
            /// <remarks> triggerdetails query </remarks>
            TriggerDetails,
            /// <remarks> userprocedures query </remarks>
            UserProcedures,
            /// <remarks> allprocedures query </remarks>
            AllProcedures,
            /// <remarks> proceduredetails query </remarks>
            ProcedureDetails,
            /// <remarks> proceduredetails statement query </remarks>
            ProcedureDetailsStm
        }
        /// <summary> datareaderfieldcount </summary>
        public int FieldCount
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql:
                        return dbhmysql.FieldCount;
                    case DBType.Oracle:
                        return dbhorcl.FieldCount;
                    case DBType.MSSql:
                        return dbhmssql.FieldCount;
                    case DBType.PgSql:
                        return dbhpgsql.FieldCount;
                    case DBType.Odbc:
                        return dbhodbc.FieldCount;
                }
            }
        }
        /// <summary> datareader recodesaffected </summary>
        public int RecordsAffected
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql:
                        return dbhmysql.RecordsAffected;
                    case DBType.Oracle:
                        return dbhorcl.RecordsAffected;
                    case DBType.MSSql:
                        return dbhmssql.RecordsAffected;
                    case DBType.PgSql:
                        return dbhpgsql.RecordsAffected;
                    case DBType.Odbc:
                        return dbhodbc.RecordsAffected;
                }
            }
        }
        /// <summary> datareader depth </summary>
        public int Depth
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql:
                        return dbhmysql.Depth;
                    case DBType.Oracle:
                        return dbhorcl.Depth;
                    case DBType.MSSql:
                        return dbhmssql.Depth;
                    case DBType.PgSql:
                        return dbhpgsql.Depth;
                    case DBType.Odbc:
                        return dbhodbc.Depth;
                }
            }
        }
        /// <summary> datareader hasrows </summary>
        public bool HasRows
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql:
                        return dbhmysql.HasRows;
                    case DBType.Oracle:
                        return dbhorcl.HasRows;
                    case DBType.MSSql:
                        return dbhmssql.HasRows;
                    case DBType.PgSql:
                        return dbhpgsql.HasRows;
                    case DBType.Odbc:
                        return dbhodbc.HasRows;
                }
            }
        }
        /// <summary> isclosed </summary>
        public bool IsClosed
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql:
                        return dbhmysql.IsClosed;
                    case DBType.Oracle:
                        return dbhorcl.IsClosed;
                    case DBType.MSSql:
                        return dbhmssql.IsClosed;
                    case DBType.PgSql:
                        return dbhpgsql.IsClosed;
                    case DBType.Odbc:
                        return dbhodbc.IsClosed;
                }
            }
        }
        /// <summary> database type </summary>
        private DBType dbType;
        /// <summary> server version </summary>
        public string ServerVersion
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql:
                        return dbhmysql.ServerVersion;
                    case DBType.Oracle:
                        return dbhorcl.ServerVersion;
                    case DBType.MSSql:
                        return dbhmssql.ServerVersion;
                    case DBType.PgSql:
                        return dbhpgsql.ServerVersion;
                    case DBType.Odbc:
                        return dbhodbc.ServerVersion;
                }
            }
        }
        /// <summary> OracleInitLongFetchSize, can be overwritten if needed </summary>
        public static int OrclInitLongFetchSize
        {
            get { return orclInitLongFetchSize; }
            set { orclInitLongFetchSize = value; }
        }
        private static int orclInitLongFetchSize = 200;
        /// <summary> last sent query </summary>
        public string LastQuery { get { return lastQuery; } }
        string lastQuery = String.Empty;

        /// <summary> exceptions </summary>
        public DBException ErrExc
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql:
                        return dbhmysql.ErrExc;
                    case DBType.Oracle:
                        return dbhorcl.ErrExc;
                    case DBType.MSSql:
                        return dbhmssql.ErrExc;
                    case DBType.PgSql:
                        return dbhpgsql.ErrExc;
                    case DBType.Odbc:
                        return dbhodbc.ErrExc;
                }
            }
        }
        private DBHelperMySql dbhmysql;
        private DBHelperOrcl dbhorcl;
        private DBHelperMSSql dbhmssql;
        private DBHelperPgSql dbhpgsql;
        private DBHelperOdbc dbhodbc;

        /// <summary>
        /// DbHelper init function
        /// </summary>
        /// <param name="databaseType">eDbType</param>
        public DBHelper(DBType databaseType)
        {
            dbType = databaseType;
            switch (databaseType)
            {
                case DBType.MySql:
                    dbhmysql = new DBHelperMySql();
                    break;
                case DBType.Oracle:
                    // get initlongfetch
                    if (NyxMain.ConProfile.InitLongFetchSize > -1)
                        orclInitLongFetchSize = NyxMain.ConProfile.InitLongFetchSize;
                    else
                        orclInitLongFetchSize = NyxMain.UASett.InitLongFetchSize;
                    // init dbhelper
                    dbhorcl = new DBHelperOrcl();
                    break;
                case DBType.MSSql:
                    dbhmssql = new DBHelperMSSql();
                    break;
                case DBType.PgSql:
                    dbhpgsql = new DBHelperPgSql();
                    break;
                case DBType.Odbc:
                    dbhodbc = new DBHelperOdbc();
                    break;
            }
        }
        /// <summary> unset internal variables </summary>
        ~DBHelper()
        {   // close datareader, to get sure we closed it
            Close();
            // disconnect database
            DisconnectDB();
            // unset class vars
            dbhmssql = null;
            dbhmysql = null;
            dbhpgsql = null;
            dbhorcl = null;
            dbhodbc = null;
        }

        /// <summary> connect </summary>
        public bool ConnectDB(ProfileSettings prf, bool eventStateChanged)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.ConnectDB(prf, eventStateChanged);
                case DBType.Oracle:
                    return dbhorcl.ConnectDB(prf, eventStateChanged);
                case DBType.MSSql:
                    return dbhmssql.ConnectDB(prf, eventStateChanged);
                case DBType.PgSql:
                    return dbhpgsql.ConnectDB(prf);
                case DBType.Odbc:
                    return dbhodbc.ConnectDB(prf, eventStateChanged);
            }
        }
        /// <summary> disconnect </summary>
        public bool DisconnectDB()
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    if (dbhmysql != null)
                        return dbhmysql.DisconnectDB();
                    return true;
                case DBType.Oracle:
                    if (dbhorcl != null)
                        return dbhorcl.DisconnectDB();
                    return true;
                case DBType.MSSql:
                    if (dbhmssql != null)
                        return dbhmssql.DisconnectDB();
                    return true;
                case DBType.PgSql:
                    if (dbhpgsql != null)
                        return dbhpgsql.DisconnectDB();
                    return true;
                case DBType.Odbc:
                    if (dbhodbc != null)
                        return dbhodbc.DisconnectDB();
                    return true;
            }
        }
        /// <summary> isconnected </summary>
        public bool IsConnected()
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.IsConnected();
                case DBType.Oracle:
                    return dbhorcl.IsConnected();
                case DBType.MSSql:
                    return dbhmssql.IsConnected();
                case DBType.PgSql:
                    return dbhpgsql.IsConnected();
                case DBType.Odbc:
                    return dbhodbc.IsConnected();
            }
        }

        /// <summary> query functions </summary>
        public string ParseSqlQuery(string parmSqlQry, int limit, bool removeCtrl)
        {
            string finqry = String.Empty, sqlqry = String.Empty;
            if (parmSqlQry != null && parmSqlQry.Length > 0)
                sqlqry = parmSqlQry;
            else
                return null;
            // triming 
            sqlqry = sqlqry.Trim();
            // remove control chars?
            if(removeCtrl)
                sqlqry = sqlqry.Replace("\n", " ").Replace("\r", " ").Replace("\t", " ").Replace("\0", " ");
            // removing final ';' (would cause error with OleDB)
            if (sqlqry.Substring(sqlqry.Length - 1, 1) == ";")
                finqry = sqlqry.Substring(0, sqlqry.Length - 1);
            else
                finqry = sqlqry;
            // check if we shall limit
            if (limit > 0)
            {
                string finqryUp = finqry.ToUpper(NyxMain.NyxCI);
                // allready a limit clause inside the query?
                switch (this.dbType)
                {
                    case DBType.MySql:
                    case DBType.PgSql:
                    default:
                        if (finqryUp.IndexOf(" LIMIT ") < 0)
                            finqry = String.Format(NyxMain.NyxCI, "{0} LIMIT {1}", finqry, limit);
                        break;

                    case DBType.Oracle:
                        if (finqryUp.IndexOf(" ROWNUM ") < 0)
                        {   // checking if a 'WHERE' is allready inside
                            if (finqryUp.IndexOf(" WHERE ") < 0)
                                finqry = String.Format(NyxMain.NyxCI, "{0} WHERE ROWNUM < {1}", finqry, limit);
                            else
                            {   // non 'WHERE' found, so inserting rownum after the last where
                                int strpos = finqryUp.LastIndexOf(" WHERE ");
                                finqry = String.Format(NyxMain.NyxCI, "{0} WHERE ROWNUM < {1} AND {2}",
                                    finqry.Substring(0, strpos), limit, finqry.Substring(strpos + 7));
                            }
                        }
                        break;

                    case DBType.MSSql:
                        if (finqryUp.IndexOf(" TOP ") < 0)
                        {
                            string[] tmpsplit = finqry.Split(new Char[] { ' ' }, 2);
                            finqry = String.Format(NyxMain.NyxCI, "{0} TOP {1} {2}", tmpsplit[0], limit, tmpsplit[1]);
                        }
                        break;
                }
            }
            //returning
            return finqry;
        }
        /// <summary> execute non select query </summary>
        public bool ExecuteNonQuery(string qry)
        {   // set last query
            lastQuery = qry;
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.ExecuteNonQuery(qry);
                case DBType.Oracle:
                    return dbhorcl.ExecuteNonQuery(qry);
                case DBType.MSSql:
                    return dbhmssql.ExecuteNonQuery(qry);
                case DBType.PgSql:
                    return dbhpgsql.ExecuteNonQuery(qry);
                case DBType.Odbc:
                    return dbhodbc.ExecuteNonQuery(qry);
            }
        }
        /// <summary> update dataset </summary>
        public bool Update(string qry, System.Data.DataSet ds)
        {   // set last query
            lastQuery = qry;
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.Update(qry, ds);
                case DBType.Oracle:
                    return dbhorcl.Update(qry, ds);
                case DBType.MSSql:
                    return dbhmssql.Update(qry, ds);
                case DBType.PgSql:
                    return dbhpgsql.Update(qry, ds);
                case DBType.Odbc:
                    return dbhodbc.Update(qry, ds);
            }
        }
        /// <summary> fill datatable </summary>
        public bool FillDT(string qry, ref System.Data.DataTable dt)
        {   // set last query
            lastQuery = qry;
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.FillDT(qry, ref dt);
                case DBType.Oracle:
                    return dbhorcl.FillDT(qry, ref dt);
                case DBType.MSSql:
                    return dbhmssql.FillDT(qry, ref dt);
                case DBType.PgSql:
                    return dbhpgsql.FillDT(qry, ref dt);
                case DBType.Odbc:
                    return dbhodbc.FillDT(qry, ref dt);
            }
        }
        /// <summary> fill dataset </summary>
        public bool FillDS(string qry, ref System.Data.DataSet ds)
        {   // set last query
            this.lastQuery = qry;
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.FillDS(qry, ref ds);
                case DBType.Oracle:
                    return dbhorcl.FillDS(qry, ref ds);
                case DBType.MSSql:
                    return dbhmssql.FillDS(qry, ref ds);
                case DBType.PgSql:
                    return dbhpgsql.FillDS(qry, ref ds);
                case DBType.Odbc:
                    return dbhodbc.FillDS(qry, ref ds);
            }
        }
        /// <summary> alternate dataset/datagridview update </summary>
        public bool AlternateDGridUpdate(string table, System.Data.DataSet ds)
        {   
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.AlternateDGridUpdate(table, ds);
                case DBType.Oracle:
                    return dbhorcl.AlternateDGridUpdate(table, ds);
                case DBType.MSSql:
                    return dbhmssql.AlternateDGridUpdate(table, ds);
                case DBType.PgSql:
                    return dbhpgsql.AlternateDGridUpdate(table, ds);
            } 
        }

        /// <summary> execute reader </summary>
        public bool ExecuteReader(string qry)
        {   // set last query
            lastQuery = qry;
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.ExecuteReader(qry);
                case DBType.Oracle:
                    return dbhorcl.ExecuteReader(qry);
                case DBType.MSSql:
                    return dbhmssql.ExecuteReader(qry);
                case DBType.PgSql:
                    return dbhpgsql.ExecuteReader(qry);
                case DBType.Odbc:
                    return dbhodbc.ExecuteReader(qry);
            }
        }
        /// <summary> read reader </summary>
        public bool Read()
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.DtaRdrRead();
                case DBType.Oracle:
                    return dbhorcl.DtaRdrRead();
                case DBType.MSSql:
                    return dbhmssql.DtaRdrRead();
                case DBType.PgSql:
                    return dbhpgsql.DtaRdrRead();
                case DBType.Odbc:
                    return dbhodbc.DtaRdrRead();
            }
        }
        /// <summary> get next result from reader </summary>
        public bool NextResult()
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.DtaRdrNextResult();
                case DBType.Oracle:
                    return dbhorcl.DtaRdrNextResult();
                case DBType.MSSql:
                    return dbhmssql.DtaRdrNextResult();
                case DBType.PgSql:
                    return dbhpgsql.DtaRdrNextResult();
                case DBType.Odbc:
                    return dbhodbc.DtaRdrNextResult();
            }
        }
        /// <summary> close reader </summary>
        public void Close()
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    if (dbhmysql != null)
                        dbhmysql.DtaRdrClose();
                    break;
                case DBType.Oracle:
                    if (dbhorcl != null)
                        dbhorcl.DtaRdrClose();
                    break;
                case DBType.MSSql:
                    if (dbhmssql != null)
                        dbhmssql.DtaRdrClose();
                    break;
                case DBType.PgSql:
                    if (dbhpgsql != null)
                        dbhpgsql.DtaRdrClose();
                    break;
                case DBType.Odbc:
                    if (dbhodbc != null)
                        dbhodbc.DtaRdrClose();
                    break;
            }
        }

        /// <summary> datareader 'type' functions </summary>
        /// <summary> getname </summary>
        public string GetName(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetName(index);
                case DBType.Oracle:
                    return dbhorcl.GetName(index);
                case DBType.MSSql:
                    return dbhmssql.GetName(index);
                case DBType.PgSql:
                    return dbhpgsql.GetName(index);
            }
        }
        /// <summary> isdbnull </summary>
        public bool IsDBNull(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.IsDBNull(index);
                case DBType.Oracle:
                    return dbhorcl.IsDBNull(index);
                case DBType.MSSql:
                    return dbhmssql.IsDBNull(index);
                case DBType.PgSql:
                    return dbhpgsql.IsDBNull(index);
            }
        }
        /// <summary> getfieldtype </summary>
        public Type GetFieldType(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetFieldType(index);
                case DBType.Oracle:
                    return dbhorcl.GetFieldType(index);
                case DBType.MSSql:
                    return dbhmssql.GetFieldType(index);
                case DBType.PgSql:
                    return dbhpgsql.GetFieldType(index);
            }
        }
        /// <summary> getdatatypename </summary>
        public string GetDataTypeName(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetDataTypeName(index);
                case DBType.Oracle:
                    return dbhorcl.GetDataTypeName(index);
                case DBType.MSSql:
                    return dbhmssql.GetDataTypeName(index);
                case DBType.PgSql:
                    return dbhpgsql.GetDataTypeName(index);
            }
        }


        /// <summary> datareader retrieve functions </summary>
        /// <summary> getvalue </summary>
        public object GetValue(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetValue(index);
                case DBType.Oracle:
                    return dbhorcl.GetValue(index);
                case DBType.MSSql:
                    return dbhmssql.GetValue(index);
                case DBType.PgSql:
                    return dbhpgsql.GetValue(index);
            }
        }
        /// <summary> getint32 </summary>
        public int GetInt32(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetInt32(index);
                case DBType.Oracle:
                    return dbhorcl.GetInt32(index);
                case DBType.MSSql:
                    return dbhmssql.GetInt32(index);
                case DBType.PgSql:
                    return dbhpgsql.GetInt32(index);
            }
        }
        /// <summary> getstring </summary>
        public string GetString(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetName(index);
                case DBType.Oracle:
                    return dbhorcl.GetName(index);
                case DBType.MSSql:
                    return dbhmssql.GetName(index);
                case DBType.PgSql:
                    return dbhpgsql.GetName(index);
            }
        }
        /// <summary> getboolean </summary>
        public bool GetBoolean(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetBoolean(index);
                case DBType.Oracle:
                    return dbhorcl.GetBoolean(index);
                case DBType.MSSql:
                    return dbhmssql.GetBoolean(index);
                case DBType.PgSql:
                    return dbhpgsql.GetBoolean(index);
            }
        }
        /// <summary> getbyte </summary>
        public byte GetByte(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetByte(index);
                case DBType.Oracle:
                    return dbhorcl.GetByte(index);
                case DBType.MSSql:
                    return dbhmssql.GetByte(index);
                case DBType.PgSql:
                    return dbhpgsql.GetByte(index);
            }
        }
        /// <summary> getbytes </summary>
        long GetBytes(int index, long dataIndex, byte[] buffer, int bufferIndex, int length)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetBytes(index, dataIndex, buffer, bufferIndex, length);
                case DBType.Oracle:
                    return dbhorcl.GetBytes(index, dataIndex, buffer, bufferIndex, length);
                case DBType.MSSql:
                    return dbhmssql.GetBytes(index, dataIndex, buffer, bufferIndex, length);
                case DBType.PgSql:
                    return dbhpgsql.GetBytes(index, dataIndex, buffer, bufferIndex, length);
            }
        }
        /// <summary> getchar </summary>
        public char GetChar(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetChar(index);
                case DBType.Oracle:
                    return dbhorcl.GetChar(index);
                case DBType.MSSql:
                    return dbhmssql.GetChar(index);
                case DBType.PgSql:
                    return dbhpgsql.GetChar(index);
            }
        }
        /// <summary> getchars </summary>
        public long GetChars(int index, long fieldOffset, char[] buffer, int bufferoffset, int length)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetChars(index, fieldOffset, buffer, bufferoffset, length);
                case DBType.Oracle:
                    return dbhorcl.GetChars(index, fieldOffset, buffer, bufferoffset, length);
                case DBType.MSSql:
                    return dbhmssql.GetChars(index, fieldOffset, buffer, bufferoffset, length);
                case DBType.PgSql:
                    return dbhpgsql.GetChars(index, fieldOffset, buffer, bufferoffset, length);
            }
        }
        /// <summary> getdatetime </summary>
        public DateTime GetDateTime(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetDateTime(index);
                case DBType.Oracle:
                    return dbhorcl.GetDateTime(index);
                case DBType.MSSql:
                    return dbhmssql.GetDateTime(index);
                case DBType.PgSql:
                    return dbhpgsql.GetDateTime(index);
            }
        }
        /// <summary> getdouble </summary>
        public double GetDouble(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetDouble(index);
                case DBType.Oracle:
                    return dbhorcl.GetDouble(index);
                case DBType.MSSql:
                    return dbhmssql.GetDouble(index);
                case DBType.PgSql:
                    return dbhpgsql.GetDouble(index);
            }
        }
        /// <summary> getfloat </summary>
        public float GetFloat(int index)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetFloat(index);
                case DBType.Oracle:
                    return dbhorcl.GetFloat(index);
                case DBType.MSSql:
                    return dbhmssql.GetFloat(index);
                case DBType.PgSql:
                    return dbhpgsql.GetFloat(index);
            }
        }
        /// <summary> gettimespan </summary>
        public TimeSpan GetTimeSpan(int index)
        {
            switch (this.dbType)
            {
                case DBType.MySql:
                    return dbhmysql.GetTimeSpan(index);
                case DBType.Oracle:
                    return dbhorcl.GetTimeSpan(index);
                default:
                    return new TimeSpan(0, 0, 0);
            }
        }
        /// <summary> getvalues </summary>
        public int GetValues(object[] values)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return dbhmysql.GetValues(values);
                case DBType.Oracle:
                    return dbhorcl.GetValues(values);
                case DBType.MSSql:
                    return dbhmssql.GetValues(values);
                case DBType.PgSql:
                    return dbhpgsql.GetValues(values);
            }
        }


        /// <summary> datareader get oracle decimal </summary>
        public Oracle.DataAccess.Types.OracleDecimal GetOracleDecimal(int index)
        {
            return dbhorcl.GetOracleDecimal(index);
        }
        /// <summary> datareader get oracle string </summary>
        public Oracle.DataAccess.Types.OracleString GetOracleString(int index)
        {
            return dbhorcl.GetOracleString(index);
        }

        /// <summary> querys </summary>
        public string GetQuery(DBHelper.EnumGetQueryType queryType)
        {
            switch (this.dbType)
            {
                default:
                case DBType.MySql:
                    return DBHelperMySql.GetQuery(queryType);
                case DBType.Oracle:
                    return DBHelperOrcl.GetQuery(queryType);
                case DBType.MSSql:
                    return DBHelperMSSql.GetQuery(queryType);
                case DBType.PgSql:
                    return DBHelperPgSql.GetQuery(queryType);
            }
        }
        /// <summary> reserved words </summary>
        public System.Collections.ObjectModel.ReadOnlyCollection<string> ReservedWords
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql: return DBHelperMySql.ReservedWords;
                    case DBType.Oracle: return DBHelperOrcl.ReservedWords;
                    case DBType.MSSql: return DBHelperMSSql.ReservedWords;
                    case DBType.PgSql: return DBHelperPgSql.ReservedWords;
                }
            }
        }
        /// <summary> encodings </summary>
        public System.Collections.ObjectModel.ReadOnlyCollection<string> Encodings
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql: return DBHelperMySql.Charsets;
                }
            }
        }
        /// <summary> collations </summary>
        public System.Collections.ObjectModel.ReadOnlyCollection<string> Collations
        {
            get
            {
                switch (this.dbType)
                {
                    default:
                    case DBType.MySql: return DBHelperMySql.Collations;
                }
            }
        }
    }
}
