/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using NyxSettings;
using MySql.Data.MySqlClient;
using System.Data.Common;
using System.Collections.ObjectModel;

namespace Nyx.Classes
{
    class DBHelperMySql
    {
        private string serverVersion = String.Empty;
        /// <summary> connection properties </summary>
        public string ServerVersion { get { return serverVersion; } }
        /// <summary> datareader properties </summary>
        private int depth;
        public int Depth { get { return depth; } }
        private int fieldCount;
        public int FieldCount { get { return fieldCount; } }
        private bool hasRows;
        public bool HasRows { get { return hasRows; } }
        private bool isClosed = true;
        public bool IsClosed { get { return isClosed; } }
        private int recordsAffected;
        public int RecordsAffected { get { return recordsAffected;} }

        private DBException errExc;
        /// <summary> exceptions </summary>
        public DBException ErrExc { get { DBException tmpExc = errExc; errExc = null; return tmpExc; } }
        /// <summary> db connection </summary>
        private MySqlConnection dbCon;
        /// <summary> datareader </summary>
        private MySqlDataReader dtaRdr;
        
        private static string GetConStr(ProfileSettings prf)
        {   // init
            string encoding = String.Empty;
            string[] splitServerPort = null, splitEncoding = null;
            string pasw = NyxCrypt.DecryptPasw(prf.ProfilePasw);
            // check encoding
            splitEncoding = prf.Encoding.Split(new Char[] { ',' });
            if (splitEncoding.Length == 2)
                encoding = splitEncoding[0];
            // check port
            splitServerPort = prf.ProfileTarget.Split(new Char[] { ':' });
            if (splitServerPort.Length == 2)
                return
                    String.Format(NyxMain.NyxCI, "Server={0};Port={1};Database={2};Uid={3};Pwd={4};Connection Timeout={5};Charset={6};Allow Zero Datetime=True;{7}",
                                   splitServerPort[0], splitServerPort[1], prf.ProfileDB, prf.ProfileUser, pasw, 
                                   NyxMain.UASett.ConnectionTimeout, encoding, prf.ProfileAddString);
            else
                return
                    String.Format(NyxMain.NyxCI, "Server={0};Port=3306;Database={1};Uid={2};Pwd={3};Connection Timeout={4};Charset={5};Allow Zero Datetime=True;{6}",
                                   prf.ProfileTarget, prf.ProfileDB, prf.ProfileUser, pasw, 
                                   NyxMain.UASett.ConnectionTimeout, encoding, prf.ProfileAddString);
        }

        /// <summary>
        /// Connect to database
        /// </summary>
        /// <param name="prf"></param>
        /// <param name="eventStateChanged"></param>
        /// <returns></returns>
        public bool ConnectDB(ProfileSettings prf, bool eventStateChanged)
        {   // init connection
            string conStr = GetConStr(prf);
            dbCon = new MySqlConnection(conStr);
            // open connection
            try 
            {   // open connection and subsribe event handler
                dbCon.Open();
                serverVersion = dbCon.ServerVersion;
                if (eventStateChanged)
                    dbCon.StateChange += new System.Data.StateChangeEventHandler(dbCon_StateChange);
            }
            catch (MySqlException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Disconnect from database
        /// </summary>
        /// <returns></returns>
        public bool DisconnectDB()
        {
            try
            {
                if (dbCon != null)
                {   // unregister eventhandler
                    dbCon.StateChange -= dbCon_StateChange;
                    // close connection
                    if (dbCon.State == System.Data.ConnectionState.Open)
                        dbCon.Close();
                }
            }
            catch (MySqlException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc); 
                return false;
            }
            finally
            {   // release resources
                if(dbCon != null)
                    dbCon.Dispose();
                dbCon = null;
            }
            return true;
        }
        /// <summary>
        /// Is connection enabled?
        /// </summary>
        /// <returns>true=connected/false=not connected</returns>
        public bool IsConnected()
        {
            if (dbCon != null && dbCon.State != ConnectionState.Closed)
                return true;
            else
                return false;
        }

        private void dbCon_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            MySqlConnection sdbCon = (MySqlConnection)sender;
            if (sdbCon.State == System.Data.ConnectionState.Closed)
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("ErrConnectionClosed"));
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrConnectionClosed"),
                    AppDialogs.MessageHandler.EnumMsgType.Warning);

                System.Threading.Thread.CurrentThread.Abort();
            }
        }

        /// <summary>
        /// Execute non query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteNonQuery(string qry)
        {   // init
            MySqlCommand mysqlCmd = new MySqlCommand(qry, dbCon);
            // execute 
            try { mysqlCmd.ExecuteNonQuery(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // return 
                return false;
            }
            return true;
        }

        public bool Update(string qry, System.Data.DataSet ds)
        {
            MySqlDataAdapter DtaAdpt = new MySqlDataAdapter();
            DtaAdpt.SelectCommand = new MySqlCommand(qry, dbCon);
            MySqlCommandBuilder cmdBld = new MySqlCommandBuilder(DtaAdpt);
            // try to update
            try { DtaAdpt.Update(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill datatable
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool FillDT(string qry, ref System.Data.DataTable dt)
        {   // init
            MySqlDataAdapter DtaAdpt = new MySqlDataAdapter(qry, dbCon);
            try { DtaAdpt.Fill(dt); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc); 
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill dataset
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool FillDS(string qry, ref System.Data.DataSet ds)
        {   // init
            MySqlDataAdapter DtaAdpt = new MySqlDataAdapter(qry, dbCon);
            try { DtaAdpt.Fill(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc); 
                return false;
            }
            return true;
        }
        /// <summary> alternate dataset/datagridview update </summary> 
        /// <param name="table"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool AlternateDGridUpdate(string table, System.Data.DataSet ds)
        {   // validate
            if (table == null)
                throw new ArgumentNullException("table", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (ds == null)
                throw new ArgumentNullException("ds", GuiHelper.GetResourceMsg("ArgumentNull"));
            // check after changed rows and correct numbers of tables
            if (ds != null && ds.Tables.Count == 1 && ds.HasChanges())
            {   // get changed rows
                DataSet dsChanged = ds.GetChanges();
                // init stringbuilder
                System.Text.StringBuilder sbAddStm = new System.Text.StringBuilder();
                System.Text.StringBuilder sbAddStmColumns = new System.Text.StringBuilder();

                System.Text.StringBuilder sbDelStm = new System.Text.StringBuilder();
                System.Text.StringBuilder sbDelStmColumns = new System.Text.StringBuilder();

                System.Text.StringBuilder sbChgStm = new System.Text.StringBuilder();

                // parse changed rows
                foreach (DataRow dr in dsChanged.Tables[0].Rows)
                {
                    switch (dr.RowState)
                    {
                        case DataRowState.Added:
                            // allready build values?
                            if (sbAddStmColumns.Length == 0)
                            {
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    if (sbAddStmColumns.Length == 0)
                                        sbAddStmColumns.Append(String.Format(NyxMain.NyxCI, "'{0}'", dr[dc.ColumnName].ToString()));
                                    else
                                        sbAddStmColumns.Append(String.Format(NyxMain.NyxCI, ",'{0}'", dr[dc.ColumnName].ToString()));
                                }
                            }
                            if (sbAddStm.Length == 0)
                                sbAddStm.Append(String.Format(NyxMain.NyxCI,
                                    "INSERT INTO {0} ({1}) VALUES ({2})", table, sbDelStmColumns.ToString(), sbAddStmColumns.ToString()));
                            else
                                sbAddStm.Append(String.Format(NyxMain.NyxCI,
                                    ";INSERT INTO {0} ({1}) VALUES ({2})", table, sbDelStmColumns.ToString(), sbAddStmColumns.ToString()));
                            // clear columns stringbuilder
                            sbAddStmColumns = new System.Text.StringBuilder();
                            break;
                        case DataRowState.Deleted:
                            // allready build columns?
                            if (sbDelStmColumns.Length == 0)
                            {
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    if (sbDelStmColumns.Length == 0)
                                        sbDelStmColumns.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                            dc.ColumnName.ToString(), dr[dc.ColumnName].ToString()));
                                    else
                                        sbDelStmColumns.Append(String.Format(NyxMain.NyxCI, " AND {0} = '{1}'",
                                            dc.ColumnName.ToString(), dr[dc.ColumnName].ToString()));
                                }
                            }
                            // finally build query statement
                            if (sbDelStm.Length == 0)
                                sbDelStm.Append(String.Format(NyxMain.NyxCI, "DELETE FROM {0} WHERE {1} LIMIT 1", table, sbDelStmColumns.ToString()));
                            else
                                sbDelStm.Append(String.Format(NyxMain.NyxCI, ";DELETE FROM {0} WHERE {1} LIMIT 1", table, sbDelStmColumns.ToString()));
                            // clear columns stringbuilder
                            sbDelStmColumns = new System.Text.StringBuilder();
                            break;
                        case DataRowState.Modified:
                            System.Text.StringBuilder sbChgUpdStm = new System.Text.StringBuilder(), sbChgWhereStm = new System.Text.StringBuilder();
                            foreach (DataColumn dc in ds.Tables[0].Columns)
                            {   
                                // always adding the where state
                                if (sbChgWhereStm.Length == 0)
                                    sbChgWhereStm.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                                        dc.ColumnName.ToString(), dr[dc.ColumnName, DataRowVersion.Original].ToString()));
                                else
                                    sbChgWhereStm.Append(String.Format(NyxMain.NyxCI, " AND {0} = '{1}'",
                                                        dc.ColumnName.ToString(), dr[dc.ColumnName, DataRowVersion.Original].ToString()));
                                // update column statement
                                if (dr[dc.ColumnName, DataRowVersion.Original].ToString() != dr[dc.ColumnName, DataRowVersion.Current].ToString())
                                {
                                    if (sbChgUpdStm.Length == 0)
                                        sbChgUpdStm.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                                            dc.ColumnName, dr[dc.ColumnName].ToString()));
                                    else
                                        sbChgUpdStm.Append(String.Format(NyxMain.NyxCI, ", {0} = '{1}'",
                                                            dc.ColumnName, dr[dc.ColumnName].ToString()));
                                }
                            }
                            // finally build query statement
                            if (sbChgStm.Length == 0)
                                sbChgStm.Append(String.Format(NyxMain.NyxCI, "UPDATE {0} SET {1} WHERE {2} LIMIT 1",
                                                table, sbChgUpdStm.ToString(), sbChgWhereStm.ToString()));
                            else
                                sbChgStm.Append(String.Format(NyxMain.NyxCI, ";UPDATE {0} SET {1} WHERE {2} LIMIT 1",
                                                table, sbChgUpdStm.ToString(), sbChgWhereStm.ToString()));
                            break;
                    }
                }
                // modified rows
                if (sbChgStm.Length > 0)
                {   // finaly build and execute query
                    if (!ExecuteNonQuery(sbChgStm.ToString()))
                        return false;
                }
                // deleted rows
                if (sbDelStm.Length > 0)
                {
                    if (!ExecuteNonQuery(sbDelStm.ToString()))
                        return false;
                }
                // added rows
                if (sbAddStm.Length > 0)
                {   
                    if (!ExecuteNonQuery(sbAddStm.ToString()))
                        return false;
                }
            }
            // return
            return true;
        }
        
        /// <summary>
        /// Execute query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteReader(string qry)
        {   // init
            MySqlCommand mysqlCmd = new MySqlCommand(qry, dbCon);
            // read
            try { dtaRdr = mysqlCmd.ExecuteReader(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc); 
                // check datareader
                if (dtaRdr != null)
                {   // reset datareader
                    DtaRdrClose();
                }
                // return 
                return false;
            }
            finally
            {   // set default properties
                if (dtaRdr != null)
                {
                    this.depth = dtaRdr.Depth;
                    this.fieldCount = dtaRdr.FieldCount;
                    this.hasRows = dtaRdr.HasRows;
                    this.recordsAffected = dtaRdr.RecordsAffected;
                    this.isClosed = dtaRdr.IsClosed;
                }
            }
            return true;
        }
        /// <summary>
        /// Datareader read
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrRead()
        {   // read
            try { return dtaRdr.Read(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc); 
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Datareader nextresult
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrNextResult()
        {   // read
            try { return dtaRdr.NextResult(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc); 
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Close current datareader
        /// </summary>
        public void DtaRdrClose()
        {   // check if allready null?
            if(dtaRdr != null)
            {   // check if close
                if (!dtaRdr.IsClosed)
                    dtaRdr.Close();
                dtaRdr = null;
            }
        }

        /// <summary> datareader 'type' functions </summary>
        public bool IsDBNull(int index)
        {   // read
            try { return dtaRdr.IsDBNull(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetDataTypeName(int index)
        {   // read
            try { return dtaRdr.GetDataTypeName(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public Type GetFieldType(int index)
        {   // read
            try { return dtaRdr.GetFieldType(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetName(int index)
        {   // read
            try { return dtaRdr.GetName(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        
        /// <summary> datareader retrieve functions </summary>
        public object GetValue(int index)
        {   // read
            try { return dtaRdr.GetValue(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public int GetInt32(int index)
        {   // read
            try { return dtaRdr.GetInt32(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public TimeSpan GetTimeSpan(int index)
        {   // read
            try { return dtaRdr.GetTimeSpan(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public DateTime GetDateTime(int index)
        {   // read
            try { return dtaRdr.GetDateTime(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetString(int index)
        {   // read
            try { return dtaRdr.GetString(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public int GetValues(object[] values)
        {   // read
            try { return dtaRdr.GetValues(values); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public float GetFloat(int index)
        {   // read
            try { return dtaRdr.GetFloat(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public double GetDouble(int index)
        {   // read
            try { return dtaRdr.GetDouble(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public bool GetBoolean(int index)
        {   // read
            try { return dtaRdr.GetBoolean(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (MySqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public byte GetByte(int index)
        {   // read
            try { return dtaRdr.GetByte(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public long GetBytes(int index, long dataIndex, byte[] buffer, int bufferIndex, int length)
        {   // read
            try { return dtaRdr.GetBytes(index, dataIndex, buffer, bufferIndex, length); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public char GetChar(int index)
        {   // read
            try { return dtaRdr.GetChar(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public long GetChars(int index, long fieldOffset, char[] buffer, int bufferoffset, int length)
        {   // read
            try { return dtaRdr.GetChars(index, fieldOffset, buffer, bufferoffset, length); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (MySqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }

        /// <summary> querys </summary>
        public static string GetQuery(DBHelper.EnumGetQueryType queryType)
        {
            switch (queryType)
            {
                case DBHelper.EnumGetQueryType.Databases: return "SHOW DATABASES";
                case DBHelper.EnumGetQueryType.Indexes: return "SHOW INDEX FROM {0}";
                case DBHelper.EnumGetQueryType.IndexDetails: return "SHOW INDEXES FROM {0}";
                case DBHelper.EnumGetQueryType.IndexedColumns: return "SHOW INDEX FROM {0}";
                case DBHelper.EnumGetQueryType.UserProcedures: return "SELECT specific_name FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_SCHEMA = '{0}' AND ROUTINE_TYPE = 'PROCEDURE' ORDER BY specific_name ASC";
                case DBHelper.EnumGetQueryType.ProcedureDetails: return "SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE SPECIFIC_NAME = '{0}'";
                case DBHelper.EnumGetQueryType.ProcedureDetailsStm: return "SHOW CREATE PROCEDURE {0}";
                case DBHelper.EnumGetQueryType.UserTables:
                    int iVnr;
                    if (Int32.TryParse(NyxMain.DBVersion.Substring(7, 1), out iVnr) && iVnr < 5) return "SHOW TABLES";
                    else return "SHOW FULL TABLES";
                case DBHelper.EnumGetQueryType.TableColumns: return "SHOW FULL COLUMNS FROM {0}";
                case DBHelper.EnumGetQueryType.UserTriggers: return "SHOW TRIGGERS";
                case DBHelper.EnumGetQueryType.TriggerDetails: return "SELECT * FROM INFORMATION_SCHEMA.TRIGGERS WHERE TRIGGER_NAME = '{0}'";
                case DBHelper.EnumGetQueryType.AllTriggers: return "SELECT trigger_name FROM information_schema.triggers ORDER BY trigger_name ASC";
                case DBHelper.EnumGetQueryType.UserViews: return "SELECT table_name FROM INFORMATION_SCHEMA.VIEWS ORDER BY table_name ASC";
                case DBHelper.EnumGetQueryType.ViewDetails: return "SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = '{0}'";
                case DBHelper.EnumGetQueryType.ViewDetailsStm: return "SHOW CREATE VIEW {0}";
                default:
                    return String.Empty;
            }
        }
        /// <summary> reserved words </summary>
        public static ReadOnlyCollection<string> ReservedWords
        {
            get
            {
                return new ReadOnlyCollection<string>(new string[] { "ACTION", "ADD", "AFTER", "AGAINST", "AGGREGATE", "ALGORITHM", "ALL", "ALTER",
                            "ANALYSE", "ANALYZE", "AND", "AS", "ASC", "AUTOCOMMIT", "AUTO_INCREMENT", "AVG_ROW_LENGTH", "BACKUP", "BEGIN", "BETWEEN", "BINLOG", "BOTH", "BY", "CASCADE", "CASE", "CHANGE", "CHANGED",
                            "CHARSET", "CHECK", "CHECKSUM", "CLIENT", "COLLATE", "COLLATION", "COLUMN", "COLUMNS", "COMMENT", "COMMIT", "COMMITTED", "COMPRESSED", "CONCURRENT", "CONSTRAINT", "CREATE", "CROSS", "CURRENT_TIMESTAMP",
                            "DATA", "DATABASE", "DATABASES", "DAY", "DAY_HOUR", "DAY_MINUTE", "DAY_SECOND", "DELAYED", "DELAY_KEY_WRITE", "DELETE", "DESC", "DESCRIBE", "DISTINCT", "DISTINCTROW", "DIV", "DO", "DROP", "DUMPFILE", "DYNAMIC",
                            "ELSE", "ENCLOSED", "END", "ENGINE", "ENGINES", "ESCAPE", "ESCAPED", "EVENTS", "EXECUTE", "EXISTS", "EXPLAIN", "EXTENDED", "FAST", "FIELDS", "FILE", "FIRST", "FIXED", "FLUSH", "FOR", "FORCE", "FOREIGN",
                            "FROM", "FULL", "FULLTEXT", "FUNCTION", "GEMINI", "GEMINI_SPIN_RETRIES", "GLOBAL", "GRANT", "GRANTS", "GROUP", "HAVING", "HEAP", "HIGH_PRIORITY", "HOSTS", "HOUR", "HOUR_MINUTE", "HOUR_SECOND", "IDENTIFIED",
                            "IF", "IGNORE", "IN", "INDEX", "INDEXES", "INFILE", "INNER", "INSERT", "INSERT_ID", "INSERT_METHOD", "INTERVAL", "INTO", "IS", "ISOLATION", "JOIN", "KEY", "KEYS", "KILL", "LAST_INSERT_ID", "LEADING",
                            "LEFT", "LEVEL", "LIKE", "LIMIT", "LINES", "LOAD", "LOCAL", "LOCK", "LOCKS", "LOGS", "LOW_PRIORITY", "MASTER", "MASTER_CONNECT_RETRY", "MASTER_HOST", "MASTER_LOG_FILE", "MASTER_LOG_POS", "MASTER_PASSWORD",
                            "MASTER_PORT", "MASTER_USER", "MATCH", "MAX_CONNECTIONS_PER_HOUR", "MAX_QUERIES_PER_HOUR", "MAX_ROWS", "MAX_UPDATES_PER_HOUR", "MAX_USER_CONNECTIONS", "MEDIUM", "MERGE", "MIN_ROWS", "MINUTE", "MINUTE_SECOND",
                            "MODE", "MODIFY", "MONTH", "MRG_MYISAM", "MYISAM", "NAMES", "NATURAL", "NOT", "NULL", "OFFSET", "ON", "OPEN", "OPTIMIZE", "OPTION", "OPTIONALLY", "OR", "ORDER", "OUTER", "OUTFILE", "PACK_KEYS", "PARTIAL",
                            "PASSWORD", "PRIMARY", "PRIVILEGES", "PROCEDURE", "PROCESS", "PROCESSLIST", "PURGE", "QUICK", "RAID0", "RAID_CHUNKS", "RAID_CHUNKSIZE", "RAID_TYPE", "READ", "REFERENCES", "REGEXP", "RELOAD", "RENAME",
                            "REPAIR", "REPEATABLE", "REPLACE", "REPLICATION", "RESET", "RESTORE", "RESTRICT", "RETURN", "RETURNS", "REVOKE", "RIGHT", "RLIKE", "ROLLBACK", "ROW", "ROW_FORMAT", "ROWS", "SECOND", "SELECT", "SEPARATOR",
                            "SERIALIZABLE", "SESSION", "SHARE", "SHOW", "SHUTDOWN", "SLAVE", "SONAME", "SOUNDS", "SQL_AUTO_IS_NULL", "SQL_BIG_RESULT", "SQL_BIG_SELECTS", "SQL_BIG_TABLES", "SQL_BUFFER_RESULT", "SQL_CACHE", "SQL_CALC_FOUND_ROWS",
                            "SQL_LOG_BIN", "SQL_LOG_OFF", "SQL_LOG_UPDATE", "SQL_LOW_PRIORITY_UPDATES", "SQL_MAX_JOIN_SIZE", "SQL_NO_CACHE", "SQL_QUOTE_SHOW_CREATE", "SQL_SAFE_UPDATES", "SQL_SELECT_LIMIT", "SQL_SLAVE_SKIP_COUNTER", "SQL_SMALL_RESULT",
                            "SQL_WARNINGS", "START", "STARTING", "STATUS", "STOP", "STORAGE", "STRAIGHT_JOIN", "STRING", "STRIPED", "SUPER", "TABLE", "TABLES", "TEMPORARY", "TERMINATED", "THEN", "TO", "TRAILING", "TRUNCATE", "TYPE",
                            "TYPES", "UNCOMMITTED", "UNION", "UNIQUE", "UNLOCK", "UPDATE", "USAGE", "USE", "USING", "VALUES", "VARIABLES", "VIEW", "WHEN", "WHERE", "WITH", "WORK", "WRITE", "XOR", "YEAR_MONTH"
                } );
            }
        }
        /// <summary> charsets </summary>
        public static ReadOnlyCollection<string> Charsets
        {
            get
            {
                return new ReadOnlyCollection<string>(new string[] { "big5", "dec8", "cp850", "hp8", "koi8r", "latin1", "latin2", "swe7", "ascii","ujis", "sjis", "hebrew", "tis620", "euckr", "koi8u", "gb2312", "greek", "cp1250", "gbk",
                            "latin5", "armscii8", "utf8", "ucs2", "cp866", "keybcs2", "macce", "macroman", "cp852","latin7", "cp1251", "cp1256", "cp1257", "binary", "geostd8" 
                } );
            }
        }
        /// <summary> collations </summary>
        public static ReadOnlyCollection<string> Collations
        {
            get
            {
                return new ReadOnlyCollection<string>(new string[] { "big5_chinese_ci", "big5_bin", "dec8_swedish_ci", "dec8_bin", "cp850_general_ci","cp850_bin", "hp8_english_ci", "hp8_bin", "koi8r_general_ci", "koi8r_bin", "latin1_german1_ci",
                            "latin1_swedish_ci", "latin1_danish_ci", "latin1_german2_ci", "latin1_bin", "latin1_general_ci","latin1_general_cs", "latin1_spanish_ci", "latin2_czech_cs", "latin2_general_ci", "latin2_hungarian_ci",
                            "latin2_croatian_ci", "latin2_bin", "swe7_swedish_ci", "swe7_bin", "ascii_general_ci", "ascii_bin","ujis_japanese_ci", "ujis_bin", "sjis_japanese_ci", "sjis_bin", "hebrew_general_ci", "hebrew_bin",
                            "tis620_thai_ci", "tis620_bin", "euckr_korean_ci", "euckr_bin", "koi8u_general_ci", "koi8u_bin","gb2312_chinese_ci", "gb2312_bin", "greek_general_ci", "greek_bin", "cp1250_general_ci", "cp1250_czech_cs",
                            "cp1250_bin", "gbk_chinese_ci", "gbk_bin", "latin5_turkish_ci", "latin5_bin", "armscii8_general_ci","armscii8_bin", "utf8_general_ci", "utf8_bin", "utf8_unicode_ci", "utf8_icelandic_ci", "utf8_latvian_ci",
                            "utf8_romanian_ci", "utf8_slovenian_ci", "utf8_polish_ci", "utf8_estonian_ci", "utf8_spanish_ci","utf8_swedish_ci", "utf8_turkish_ci", "utf8_czech_ci", "utf8_danish_ci", "utf8_lithuanian_ci",
                            "utf8_slovak_ci", "utf8_spanish2_ci", "utf8_roman_ci", "utf8_persian_ci", "ucs2_general_ci","ucs2_bin", "ucs2_unicode_ci", "ucs2_icelandic_ci", "ucs2_latvian_ci", "ucs2_romanian_ci",
                            "ucs2_slovenian_ci", "ucs2_polish_ci", "ucs2_estonian_ci", "ucs2_spanish_ci", "ucs2_swedish_ci","ucs2_turkish_ci", "ucs2_czech_ci", "ucs2_danish_ci", "ucs2_lithuanian_ci", "ucs2_slovak_ci",
                            "ucs2_spanish2_ci", "ucs2_roman_ci", "ucs2_persian_ci", "cp866_general_ci", "cp866_bin","keybcs2_general_ci", "keybcs2_bin", "macce_general_ci", "macce_bin", "macroman_general_ci",
                            "macroman_bin", "cp852_general_ci", "cp852_bin", "latin7_estonian_cs", "latin7_general_ci","latin7_general_cs", "latin7_bin", "cp1251_bulgarian_ci", "cp1251_ukrainian_ci", "cp1251_bin",
                            "cp1251_general_ci", "cp1251_general_cs", "cp1256_general_ci", "cp1256_bin", "cp1257_lithuanian_ci","cp1257_bin", "cp1257_general_ci", "binary", "geostd8_general_ci", "geostd8_bi" 
                } );
            }
        }
    }
}
