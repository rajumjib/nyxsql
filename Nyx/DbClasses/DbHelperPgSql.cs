/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using NyxSettings;
using Npgsql;
using System.Data.Common;
using System.Collections.ObjectModel;

namespace Nyx.Classes
{
    class DBHelperPgSql
    {
        private string serverVersion = String.Empty;
        /// <summary> connection properties </summary>
        public string ServerVersion { get { return serverVersion; } }
        /// <summary> datareader properties </summary>
        private int depth;
        public int Depth { get { return depth; } }
        private int fieldCount;
        public int FieldCount { get { return fieldCount; } }
        private bool hasRows;
        public bool HasRows { get { return hasRows; } }
        private bool isClosed = true;
        public bool IsClosed { get { return isClosed; } }
        private int recordsAffected;
        public int RecordsAffected { get { return recordsAffected; } }

        private DBException errExc;
        /// <summary> exceptions </summary>
        public DBException ErrExc { get { DBException tmpExc = errExc; errExc = null; return tmpExc; } }
        /// <summary> db connection </summary>
        private NpgsqlConnection dbCon;
        /// <summary> datareader </summary>
        private NpgsqlDataReader dtaRdr;
                
        private static string GetConStr(ProfileSettings prf)
        {   // decrypt password
            string pasw = NyxCrypt.DecryptPasw(prf.ProfilePasw);
            // got a port?
            string[] split = prf.ProfileTarget.Split(new Char[] { ':' });
            if (split.Length == 2)
                return
                    String.Format(NyxMain.NyxCI, "Server={0};Port={1};Database={2};Uid={3};Pwd={4};Connection Timeout={5};Allow Zero Datetime=True;{6}",
                                    split[0], split[1], prf.ProfileDB, prf.ProfileUser, pasw, 
                                    NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
            else
                return
                    String.Format(NyxMain.NyxCI, "Server={0};Database={1};Uid={2};Pwd={3};Connection Timeout={4};Allow Zero Datetime=True;{5}",
                                    prf.ProfileTarget, prf.ProfileDB, prf.ProfileUser, pasw, 
                                    NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
        }

        /// <summary>
        /// Connect to database
        /// </summary>
        /// <param name="prf"></param>
        /// <returns></returns>
        public bool ConnectDB(ProfileSettings prf)
        {   // init connection
            string conStr = GetConStr(prf);
            dbCon = new NpgsqlConnection(conStr);
            // open connection
            try
            {   // open connection and subsribe event handler
                dbCon.Open();
                serverVersion = dbCon.ServerVersion.ToString();
            }
            catch (NpgsqlException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Disconnect from database
        /// </summary>
        /// <returns></returns>
        public bool DisconnectDB()
        {
            try
            {   // close connection
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();
            }
            catch (NpgsqlException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {   // release resources
                if (dbCon != null)
                    dbCon.Dispose();
                dbCon = null;
            }
            return true;
        }
        /// <summary>
        /// Is connection enabled?
        /// </summary>
        /// <returns>true=connected/false=not connected</returns>
        public bool IsConnected()
        {
            if (dbCon != null && dbCon.State != ConnectionState.Closed)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Execute non query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteNonQuery(string qry)
        {   // init
            NpgsqlCommand mysqlCmd = new NpgsqlCommand(qry, dbCon);
            // execute 
            try { mysqlCmd.ExecuteNonQuery(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // return 
                return false;
            }
            return true;
        }

        public bool Update(string qry, System.Data.DataSet ds)
        {
            NpgsqlDataAdapter DtaAdpt = new NpgsqlDataAdapter();
            DtaAdpt.SelectCommand = new NpgsqlCommand(qry, dbCon);
            NpgsqlCommandBuilder cmdBld = new NpgsqlCommandBuilder(DtaAdpt);
            // try to update
            try { DtaAdpt.Update(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill datatable
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool FillDT(string qry, ref System.Data.DataTable dt)
        {   // init
            NpgsqlDataAdapter DtaAdpt = new NpgsqlDataAdapter(qry, dbCon);
            try { DtaAdpt.Fill(dt); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill dataset
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool FillDS(string qry, ref System.Data.DataSet ds)
        {   // init
            NpgsqlDataAdapter DtaAdpt = new NpgsqlDataAdapter(qry, dbCon);
            try { DtaAdpt.Fill(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary> alternate dataset/datagridview update </summary> 
        /// <param name="table"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool AlternateDGridUpdate(string table, System.Data.DataSet ds)
        {   // validate
            if (table == null)
                throw new ArgumentNullException("table", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (ds == null)
                throw new ArgumentNullException("ds", GuiHelper.GetResourceMsg("ArgumentNull"));
            // check after changed rows and correct numbers of tables
            if (ds != null && ds.Tables.Count == 1 && ds.HasChanges())
            {   // get changed rows
                DataSet dsChanged = ds.GetChanges();
                // init stringbuilder
                System.Text.StringBuilder sbAddStm = new System.Text.StringBuilder();
                System.Text.StringBuilder sbAddStmColumns = new System.Text.StringBuilder();

                System.Text.StringBuilder sbDelStm = new System.Text.StringBuilder();
                System.Text.StringBuilder sbDelStmColumns = new System.Text.StringBuilder();

                System.Text.StringBuilder sbChgStm = new System.Text.StringBuilder();

                // parse changed rows
                foreach (DataRow dr in dsChanged.Tables[0].Rows)
                {
                    switch (dr.RowState)
                    {
                        case DataRowState.Added:
                            // allready build values?
                            if (sbAddStmColumns.Length == 0)
                            {
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    if (sbAddStmColumns.Length == 0)
                                        sbAddStmColumns.Append(String.Format(NyxMain.NyxCI, "'{0}'", dr[dc.ColumnName].ToString()));
                                    else
                                        sbAddStmColumns.Append(String.Format(NyxMain.NyxCI, ",'{0}'", dr[dc.ColumnName].ToString()));
                                }
                            }
                            if (sbAddStm.Length == 0)
                                sbAddStm.Append(String.Format(NyxMain.NyxCI,
                                    "INSERT INTO {0} ({1}) VALUES ({2})", table, sbDelStmColumns.ToString(), sbAddStmColumns.ToString()));
                            else
                                sbAddStm.Append(String.Format(NyxMain.NyxCI,
                                    ";INSERT INTO {0} ({1}) VALUES ({2})", table, sbDelStmColumns.ToString(), sbAddStmColumns.ToString()));
                            // clear columns stringbuilder
                            sbAddStmColumns = new System.Text.StringBuilder();
                            break;
                        case DataRowState.Deleted:
                            // allready build columns?
                            if (sbDelStmColumns.Length == 0)
                            {
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    if (sbDelStmColumns.Length == 0)
                                        sbDelStmColumns.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                            dc.ColumnName.ToString(), dr[dc.ColumnName].ToString()));
                                    else
                                        sbDelStmColumns.Append(String.Format(NyxMain.NyxCI, " AND {0} = '{1}'",
                                            dc.ColumnName.ToString(), dr[dc.ColumnName].ToString()));
                                }
                            }
                            // finally build query statement
                            if (sbDelStm.Length == 0)
                                sbDelStm.Append(String.Format(NyxMain.NyxCI, "DELETE FROM {0} WHERE {1} LIMIT 1", table, sbDelStmColumns.ToString()));
                            else
                                sbDelStm.Append(String.Format(NyxMain.NyxCI, ";DELETE FROM {0} WHERE {1} LIMIT 1", table, sbDelStmColumns.ToString()));
                            // clear columns stringbuilder
                            sbDelStmColumns = new System.Text.StringBuilder();
                            break;
                        case DataRowState.Modified:
                            System.Text.StringBuilder sbChgUpdStm = new System.Text.StringBuilder(), sbChgWhereStm = new System.Text.StringBuilder();
                            foreach (DataColumn dc in ds.Tables[0].Columns)
                            {
                                // always adding the where state
                                if (sbChgWhereStm.Length == 0)
                                    sbChgWhereStm.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                                        dc.ColumnName.ToString(), dr[dc.ColumnName, DataRowVersion.Original].ToString()));
                                else
                                    sbChgWhereStm.Append(String.Format(NyxMain.NyxCI, " AND {0} = '{1}'",
                                                        dc.ColumnName.ToString(), dr[dc.ColumnName, DataRowVersion.Original].ToString()));
                                // update column statement
                                if (dr[dc.ColumnName, DataRowVersion.Original].ToString() != dr[dc.ColumnName, DataRowVersion.Current].ToString())
                                {
                                    if (sbChgUpdStm.Length == 0)
                                        sbChgUpdStm.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                                            dc.ColumnName, dr[dc.ColumnName].ToString()));
                                    else
                                        sbChgUpdStm.Append(String.Format(NyxMain.NyxCI, ", {0} = '{1}'",
                                                            dc.ColumnName, dr[dc.ColumnName].ToString()));
                                }
                            }
                            // finally build query statement
                            if (sbChgStm.Length == 0)
                                sbChgStm.Append(String.Format(NyxMain.NyxCI, "UPDATE {0} SET {1} WHERE {2} LIMIT 1",
                                                table, sbChgUpdStm.ToString(), sbChgWhereStm.ToString()));
                            else
                                sbChgStm.Append(String.Format(NyxMain.NyxCI, ";UPDATE {0} SET {1} WHERE {2} LIMIT 1",
                                                table, sbChgUpdStm.ToString(), sbChgWhereStm.ToString()));
                            break;
                    }
                }
                // modified rows
                if (sbChgStm.Length > 0)
                {   // finaly build and execute query
                    if (!ExecuteNonQuery(sbChgStm.ToString()))
                        return false;
                }
                // deleted rows
                if (sbDelStm.Length > 0)
                {
                    if (!ExecuteNonQuery(sbDelStm.ToString()))
                        return false;
                }
                // added rows
                if (sbAddStm.Length > 0)
                {
                    if (!ExecuteNonQuery(sbAddStm.ToString()))
                        return false;
                }
            }
            // return
            return true;
        }

        /// <summary>
        /// Execute query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteReader(string qry)
        {   // init
            NpgsqlCommand mysqlCmd = new NpgsqlCommand(qry, dbCon);
            // read
            try { dtaRdr = mysqlCmd.ExecuteReader(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // check datareader
                if (dtaRdr != null)
                {   // reset datareader
                    DtaRdrClose();
                }
                // return 
                return false;
            }
            finally
            {   // set default properties
                if (dtaRdr != null)
                {
                    this.depth = dtaRdr.Depth;
                    this.fieldCount = dtaRdr.FieldCount;
                    this.hasRows = dtaRdr.HasRows;
                    this.recordsAffected = dtaRdr.RecordsAffected;
                    this.isClosed = dtaRdr.IsClosed;
                }
            }
            return true;
        }
        /// <summary>
        /// Datareader read
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrRead()
        {   // read
            try { return dtaRdr.Read(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Datareader nextresult
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrNextResult()
        {   // read
            try { return dtaRdr.NextResult(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Close current datareader
        /// </summary>
        public void DtaRdrClose()
        {   // check if allready null?
            if (dtaRdr != null)
            {   // check if close
                if (!dtaRdr.IsClosed)
                    dtaRdr.Close();
                dtaRdr = null;
            }
        }

        /// <summary> datareader 'type' functions </summary>
        public bool IsDBNull(int index)
        {   // read
            try { return dtaRdr.IsDBNull(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetDataTypeName(int index)
        {   // read
            try { return dtaRdr.GetDataTypeName(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public Type GetFieldType(int index)
        {   // read
            try { return dtaRdr.GetFieldType(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetName(int index)
        {   // read
            try { return dtaRdr.GetName(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }

        /// <summary> datareader retrieve functions </summary>
        public object GetValue(int index)
        {   // read
            try { return dtaRdr.GetValue(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public int GetInt32(int index)
        {   // read
            try { return dtaRdr.GetInt32(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public DateTime GetDateTime(int index)
        {   // read
            try { return dtaRdr.GetDateTime(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetString(int index)
        {   // read
            try { return dtaRdr.GetString(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public int GetValues(object[] values)
        {   // read
            try { return dtaRdr.GetValues(values); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public float GetFloat(int index)
        {   // read
            try { return dtaRdr.GetFloat(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public double GetDouble(int index)
        {   // read
            try { return dtaRdr.GetDouble(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public bool GetBoolean(int index)
        {   // read
            try { return dtaRdr.GetBoolean(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (NpgsqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public byte GetByte(int index)
        {   // read
            try { return dtaRdr.GetByte(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public long GetBytes(int index, long dataIndex, byte[] buffer, int bufferIndex, int length)
        {   // read
            try { return dtaRdr.GetBytes(index, dataIndex, buffer, bufferIndex, length); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public char GetChar(int index)
        {   // read
            try { return dtaRdr.GetChar(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public long GetChars(int index, long fieldOffset, char[] buffer, int bufferoffset, int length)
        {   // read
            try { return dtaRdr.GetChars(index, fieldOffset, buffer, bufferoffset, length); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (NpgsqlException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }

        /// <summary> querys </summary>
        public static string GetQuery(DBHelper.EnumGetQueryType queryType)
        {
            switch (queryType)
            {
                case DBHelper.EnumGetQueryType.Indexes: return "SELECT i.indisunique, i.indisprimary, i.indisclustered, c.relname " +
                                "FROM pg_catalog.pg_index i, pg_catalog.pg_class c, pg_catalog.pg_class c2, pg_catalog.pg_am a " +
                                "WHERE i.indexrelid = c.oid AND c.relam = a.oid AND i.indrelid = c2.oid AND c2.relname = '{0}'";
                case DBHelper.EnumGetQueryType.UserProcedures: return "SELECT proname FROM pg_proc ORDER BY proname ASC";
                case DBHelper.EnumGetQueryType.ProcedureDetails: return "SELECT n.nspname as \"Schema\",  " +
                        "   p.proname as \"Name\"," +
                        "   CASE WHEN p.proretset THEN 'setof ' ELSE '' END || pg_catalog.format_type(p.prorettype, NULL) as \"Result data type\"," +
                        "   pg_catalog.oidvectortypes(p.proargtypes) as \"Argument data types\", r.rolname as \"Owner\", l.lanname as \"Language\", p.prosrc as \"Source\", " +
                        "   pg_catalog.obj_description(p.oid, 'pg_proc') as \"Description\" " +
                        "FROM pg_catalog.pg_proc p LEFT JOIN pg_catalog.pg_namespace n ON n.oid = p.pronamespace LEFT JOIN pg_catalog.pg_language l ON l.oid = p.prolang " +
                        "   JOIN pg_catalog.pg_roles r ON r.oid = p.proowner " +
                        "WHERE p.prorettype <> 'pg_catalog.cstring'::pg_catalog.regtype AND (p.proargtypes[0] IS NULL OR p.proargtypes[0] <> 'pg_catalog.cstring'::pg_catalog.regtype) " +
                        "   AND NOT p.proisagg AND p.proname = '{0}' ORDER BY 1,2,3,4";
                case DBHelper.EnumGetQueryType.UserTables: return "SELECT tablename FROM pg_tables WHERE schemaname = 'public' ORDER BY tablename ASC";
                case DBHelper.EnumGetQueryType.AllTables: return "SELECT tablename,schemaname FROM pg_tables ORDER BY tablename ASC";
                case DBHelper.EnumGetQueryType.TableColumns: return "SELECT a.attnum, a.attname AS field, t.typname AS type, a.attlen AS length, " +
                                        "a.atttypmod AS lengthvar, a.attnotnull AS notnull, a.atthasdef AS hasdefault FROM pg_class c,pg_attribute a,pg_type t " +
                                        "WHERE c.relname = '{0}' AND a.attnum > 0 AND a.attrelid = c.oid AND a.atttypid = t.oid ORDER BY a.attnum";
                case DBHelper.EnumGetQueryType.UserTriggers: return "SELECT tgname FROM pg_trigger ORDER BY tgname ASC";
                case DBHelper.EnumGetQueryType.TriggerDetails: return "SELECT * FROM INFORMATION_SCHEMA.TRIGGERS WHERE TRIGGER_NAME = '{0}'";
                case DBHelper.EnumGetQueryType.UserViews: return "SELECT viewname FROM pg_views WHERE schemaname = 'public' ORDER BY viewname ASC"; 
                case DBHelper.EnumGetQueryType.AllViews: return "SELECT viewname,schemaname FROM pg_views ORDER BY viewname ASC";
                case DBHelper.EnumGetQueryType.ViewDetails: return "SELECT * FROM pg_views WHERE viewname = '{0}'";
                default: return String.Empty;
            }
        }
        /// <summary> reserved words </summary>
        public static ReadOnlyCollection<string> ReservedWords
        {
            get
            {
                return new ReadOnlyCollection<string>(new string[] { "ABORT","ABS","ABSOLUTE","ACCESS","ACTION","ADA","ADD","ADMIN","AFTER","AGGREGATE","ALIAS","ALL","ALLOCATE","ALSO","ALTER","ALWAYS","ANALYSE","ANALYZE","AND","ANY","ARE","ARRAY","AS","ASC","ASENSITIVE","ASSERTION","ASSIGNMENT","ASYMMETRIC","AT"
                            ,"ATOMIC","ATTRIBUTE","ATTRIBUTES","AUTHORIZATION","AVG","BACKWARD","BEFORE","BEGIN","BERNOULLI","BETWEEN","BIGINT","BINARY","BIT","BITVAR","BIT_LENGTH","BLOB","BOOLEAN","BOTH","BREADTH","BY","CACHE","CALL","CALLED","CARDINALITY"
                            ,"CASCADE","CASCADED","CASE","CAST","CATALOG","CATALOG_NAME","CEIL","CEILING","CHAIN","CHAR","CHARACTER","CHARACTERISTICS","CHARACTERS","CHARACTER_LENGTH","CHARACTER_SET_CATALOG","CHARACTER_SET_NAME","CHARACTER_SET_SCHEMA","CHAR_LENGTH","CHECK"
                            ,"CHECKED","CHECKPOINT","CLASS","CLASS_ORIGIN","CLOB","CLOSE","CLUSTER","COALESCE","COBOL","COLLATE","COLLATION","COLLATION_CATALOG","COLLATION_NAME","COLLATION_SCHEMA","COLLECT","COLUMN","COLUMN_NAME","COMMAND_FUNCTION","COMMAND_FUNCTION_CODE","COMMENT"
                            ,"COMMIT","COMMITTED","COMPLETION","CONDITION","CONNECT","CONNECTION","CONNECTION_NAME","CONSTRAINT","CONSTRAINTS","CONSTRAINT_CATALOG","CONSTRAINT_NAME","CONSTRAINT_SCHEMA","CONSTRUCTOR","CONTAINS","CONTINUE","CONVERSION","CONVERT","COPY"
                            ,"CORR","CORRESPONDING","COUNT","COVAR_POP","COVAR_SAMP","CREATE","CREATEDB","CREATEROLE","CREATEUSER","CROSS","CSV","CUBE","CUME_DIST","CURRENT","CURRENT_DATE","CURRENT_DEFAULT_TRANSFORM_GROUP","CURRENT_PATH","CURRENT_ROLE","CURRENT_TIME"
                            ,"CURRENT_TIMESTAMP","CURRENT_TRANSFORM_GROUP_FOR_TYPE","CURRENT_USER","CURSOR","CURSOR_NAME","CYCLE","DATA","DATABASE","DATE","DATETIME_INTERVAL_CODE","DATETIME_INTERVAL_PRECISION","DAY","DEALLOCATE","DEC","DECIMAL","DECLARE","DEFAULT","DEFAULTS"
                            ,"DEFERRABLE","DEFERRED","DEFINED","DEFINER","DEGREE","DELETE","DELIMITER","DELIMITERS","DENSE_RANK","DEPTH","DEREF","DERIVED","DESC","DESCRIBE","DESCRIPTOR","DESTROY","DESTRUCTOR","DETERMINISTIC","DIAGNOSTICS","DICTIONARY","DISABLE","DISCONNECT","DISPATCH"
                            ,"DISTINCT","DO","DOMAIN","DOUBLE","DROP","DYNAMIC","DYNAMIC_FUNCTION","DYNAMIC_FUNCTION_CODE","EACH","ELEMENT","ELSE","ENABLE","ENCODING","ENCRYPTED","END","END-EXEC","EQUALS","ESCAPE","EVERY","EXCEPT","EXCEPTION","EXCLUDE","EXCLUDING","EXCLUSIVE","EXEC"
                            ,"EXECUTE","EXISTING","EXISTS","EXP","EXPLAIN","EXTERNAL","EXTRACT","FALSE","FETCH","FILTER","FINAL","FIRST","FLOAT","FLOOR","FOLLOWING","FOR","FORCE","FOREIGN","FORTRAN","FORWARD","FOUND","FREE","FREEZE","FROM","FULL","FUNCTION","FUSION","GENERAL"
                            ,"GENERATED","GET","GLOBAL","GO","GOTO","GRANT","GRANTED","GREATEST","GROUP","GROUPING","HANDLER","HAVING","HEADER","HIERARCHY","HOLD","HOST","HOUR","IDENTITY","IGNORE","ILIKE","IMMEDIATE","IMMUTABLE","IMPLEMENTATION","IMPLICIT","IN","INCLUDING"
                            ,"INCREMENT","INDEX","INDICATOR","INFIX","INHERIT","INHERITS","INITIALIZE","INITIALLY","INNER","INOUT","INPUT","INSENSITIVE","INSERT","INSTANCE","INSTANTIABLE","INSTEAD","INT","INTEGER","INTERSECT","INTERSECTION","INTERVAL","INTO","INVOKER","IS"
                            ,"ISNULL","ISOLATION","ITERATE","JOIN","KEY","KEY_MEMBER","KEY_TYPE","LANCOMPILER","LANGUAGE","LARGE","LAST","LATERAL","LEADING","LEAST","LEFT","LENGTH","LESS","LEVEL","LIKE","LIMIT","LISTEN","LN","LOAD","LOCAL","LOCALTIME","LOCALTIMESTAMP"
                            ,"LOCATION","LOCATOR","LOCK","LOGIN","LOWER","MAP","MATCH","MATCHED","MAX","MAXVALUE","MEMBER","MERGE","MESSAGE_LENGTH","MESSAGE_OCTET_LENGTH","MESSAGE_TEXT","METHOD","MIN","MINUTE","MINVALUE","MOD","MODE","MODIFIES","MODIFY","MODULE","MONTH"
                            ,"MORE","MOVE","MULTISET","MUMPS","NAME","NAMES","NATIONAL","NATURAL","NCHAR","NCLOB","NESTING","NEW","NEXT","NO","NOCREATEDB","NOCREATEROLE","NOCREATEUSER","NOINHERIT","NOLOGIN","NONE","NORMALIZE","NORMALIZED","NOSUPERUSER","NOT","NOTHING","NOTIFY"
                            ,"NOTNULL","NOWAIT","NULL","NULLABLE","NULLIF","NULLS","NUMBER","NUMERIC","OBJECT","OCTETS","OCTET_LENGTH","OFF","OFFSET","OIDS","OLD","ON","ONLY","OPEN","OPERATION","OPERATOR","OPTION","OPTIONS","OR","ORDER","ORDERING","ORDINALITY","OTHERS"
                            ,"OUT","OUTER","OUTPUT","OVER","OVERLAPS","OVERLAY","OVERRIDING","OWNER","PAD","PARAMETER","PARAMETERS","PARAMETER_MODE","PARAMETER_NAME","PARAMETER_ORDINAL_POSITION","PARAMETER_SPECIFIC_CATALOG","PARAMETER_SPECIFIC_NAME","PARAMETER_SPECIFIC_SCHEMA"
                            ,"PARTIAL","PARTITION","PASCAL","PASSWORD","PATH","PERCENTILE_CONT","PERCENTILE_DISC","PERCENT_RANK","PLACING","PLI","POSITION","POSTFIX","POWER","PRECEDING","PRECISION","PREFIX","PREORDER","PREPARE","PREPARED","PRESERVE","PRIMARY","PRIOR","PRIVILEGES"
                            ,"PROCEDURAL","PROCEDURE","PUBLIC","QUOTE","RANGE","RANK","READ","READS","REAL","RECHECK","RECURSIVE","REF","REFERENCES","REFERENCING","REGR_AVGX","REGR_AVGY","REGR_COUNT","REGR_INTERCEPT","REGR_R2","REGR_SLOPE","REGR_SXX","REGR_SXY","REGR_SYY"
                            ,"REINDEX","RELATIVE","RELEASE","RENAME","REPEATABLE","REPLACE","RESET","RESTART","RESTRICT","RESULT","RETURN","RETURNED_CARDINALITY","RETURNED_LENGTH","RETURNED_OCTET_LENGTH","RETURNED_SQLSTATE","RETURNS","REVOKE","RIGHT","ROLE","ROLLBACK","ROLLUP","ROUTINE"
                            ,"ROUTINE_CATALOG","ROUTINE_NAME","ROUTINE_SCHEMA","ROW","ROWS","ROW_COUNT","ROW_NUMBER","RULE","SAVEPOINT","SCALE","SCHEMA","SCHEMA_NAME","SCOPE","SCOPE_CATALOG","SCOPE_NAME","SCOPE_SCHEMA","SCROLL","SEARCH","SECOND","SECTION","SECURITY","SELECT"
                            ,"SELF","SENSITIVE","SEQUENCE","SERIALIZABLE","SERVER_NAME","SESSION","SESSION_USER","SET","SETOF","SETS","SHARE","SHOW","SIMILAR","SIMPLE","SIZE","SMALLINT","SOME","SOURCE","SPACE","SPECIFIC","SPECIFICTYPE","SPECIFIC_NAME","SQL","SQLCODE","SQLERROR"
                            ,"SQLEXCEPTION","SQLSTATE","SQLWARNING","SQRT","STABLE","START","STATE","STATEMENT","STATIC","STATISTICS","STDDEV_POP","STDDEV_SAMP","STDIN","STDOUT","STORAGE","STRICT","STRUCTURE","STYLE","SUBCLASS_ORIGIN","SUBLIST","SUBMULTISET","SUBSTRING"
                            ,"SUM","SUPERUSER","SYMMETRIC","SYSID","SYSTEM","SYSTEM_USER","TABLE","TABLESAMPLE","TABLESPACE","TABLE_NAME","TEMP","TEMPLATE","TEMPORARY","TERMINATE","THAN","THEN","TIES","TIME","TIMESTAMP","TIMEZONE_HOUR","TIMEZONE_MINUTE","TO","TOAST","TOP_LEVEL_COUNT"
                            ,"TRAILING","TRANSACTION","TRANSACTIONS_COMMITTED","TRANSACTIONS_ROLLED_BACK","TRANSACTION_ACTIVE","TRANSFORM","TRANSFORMS","TRANSLATE","TRANSLATION","TREAT","TRIGGER","TRIGGER_CATALOG","TRIGGER_NAME","TRIGGER_SCHEMA","TRIM","TRUE","TRUNCATE"
                            ,"TRUSTED","TYPE","UESCAPE","UNBOUNDED","UNCOMMITTED","UNDER","UNENCRYPTED","UNION","UNIQUE","UNKNOWN","UNLISTEN","UNNAMED","UNNEST","UNTIL","UPDATE","UPPER","USAGE","USER","USER_DEFINED_TYPE_CATALOG","USER_DEFINED_TYPE_CODE","USER_DEFINED_TYPE_NAME"
                            ,"USER_DEFINED_TYPE_SCHEMA","USING","VACUUM","VALID","VALIDATOR","VALUE","VALUES","VARCHAR","VARIABLE","VARYING","VAR_POP","VAR_SAMP","VERBOSE","VIEW","VOLATILE","WHEN","WHENEVER","WHERE","WIDTH_BUCKET","WINDOW","WITH","WITHIN","WITHOUT"
                            ,"WORK","WRITE","YEAR","ZONE" } );
            }
        }
    }
}        