/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using NyxSettings;
using System.Collections.Generic;
using System.Text;

namespace Nyx.Classes
{
    
    /// <summary>
    /// Tables Management Class
    ///     Class for typical table, index actions like drop,alter, repair and so on
    ///     Created to seperate from DbManagement
    /// </summary>
    class TableManagement
    {
        private DBHelper dbh;
        private NyxSettings.DBType dbType;
        private Exception errExc;
        public Exception ErrExc { get { return errExc; } }

        private struct StructQuery
        {
            public string prefix;
            public string suffix;
        }
        public struct StructTableOptions
        {
            public TableAction tblAction;
            // controls 'purge' for drop and 'compute statistics' for analyze
            public string[] objectList;
            public string table;
            public bool addOpt;
        }

        public delegate void TblThrdPDelegate(TblThreadReturnVars param);
        public struct TblThreadReturnVars
        {
            public TableAction tblAction;
            public Exception errExc;
        }

        public TableManagement(DBHelper pDbh)
        {   // initalize ourself
            this.dbType = NyxMain.ConProfile.ProfileDBType;
            dbh = pDbh;
        }
     
        private bool GetQuery(StructTableOptions tblOpt, out StructQuery strQry)
        {
            strQry = new StructQuery();
            strQry.suffix = String.Empty;

            switch (tblOpt.tblAction)
            {
                case TableAction.TableAnalyze:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "ANALYZE TABLE `";
                            strQry.suffix = "`";
                            return true;
                        case DBType.Oracle:
                            strQry.prefix = "ANALYZE TABLE ";
                            if (tblOpt.addOpt)
                                strQry.suffix = " COMPUTE STATISTICS";
                            return true;
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "ANALYZE TABLE ";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.TableCheck:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "CHECK TABLE `";
                            strQry.suffix = "`";
                            return true; 
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "CHECK TABLE ";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.TableDrop:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "DROP TABLE `";
                            strQry.suffix = "`";
                            return true;
                        case DBType.Oracle:
                            strQry.prefix = "DROP TABLE ";
                            if (tblOpt.addOpt)
                                strQry.suffix = " PURGE";
                            return true;
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "DROP TABLE ";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.TableOptimize:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "OPTIMIZE TABLE `";
                            strQry.suffix = "`";
                            return true;
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "OPTIMIZE TABLE ";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.TableRepair:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "REPAIR TABLE `";
                            strQry.suffix = "`";
                            return true;
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "REPAIR TABLE ";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.TableTruncate:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "TRUNCATE TABLE `";
                            strQry.suffix = "`";
                            return true;
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "TRUNCATE TABLE ";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.FieldDrop:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = String.Format(NyxMain.NyxCI, "ALTER TABLE `{0}` DROP COLUMN `", tblOpt.table);
                            strQry.suffix = "`";
                            return true;
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = String.Format(NyxMain.NyxCI, "ALTER TABLE {0} DROP COLUMN ", tblOpt.table);
                            //strQry.suffix = "'";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.IndexDrop:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "DROP INDEX `";
                            strQry.suffix = String.Format(NyxMain.NyxCI, "` ON `{0}`", tblOpt.table);
                            return true;
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "DROP INDEX \"";
                            strQry.suffix = "\"";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.IndexRebuild:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "ALTER INDEX `";
                            strQry.suffix = "` REBUILD";
                            return true;
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "ALTER INDEX ";
                            strQry.suffix = " REBUILD";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.IndexAnalyze:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "ANALYZE INDEX `";
                            strQry.suffix = "` VALIDATE STRUCTURE";
                            return true;
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "ANALYZE INDEX ";
                            strQry.suffix = " VALIDATE STRUCTURE";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
                case TableAction.ViewDrop:
                    switch (this.dbType)
                    {
                        case DBType.MySql:
                            strQry.prefix = "DROP VIEW `";
                            return true;
                        case DBType.Oracle:
                        case DBType.MSSql:
                        case DBType.PgSql:
                            strQry.prefix = "DROP VIEW ";
                            return true;
                        default:
                            this.errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                            return false;
                    }
            }
            this.errExc = new DBException(GuiHelper.GetResourceMsg("TableActionIdentErr"));
            return false;
        }
        public bool ExecTableQuery(StructTableOptions tblOpt)
        {
            StructQuery strQry;
            if (GetQuery(tblOpt, out strQry))
            {
                string qry;
                foreach (string table in tblOpt.objectList)
                {
                    qry = String.Format(NyxMain.NyxCI, "{0}{1}{2}", strQry.prefix, table, strQry.suffix);
                    if (!dbh.ExecuteNonQuery(qry))
                    {
                        this.errExc = dbh.ErrExc;
                        return false;
                    }
                }
                return true;
            }
            else
                return false;
        }
    }
}
