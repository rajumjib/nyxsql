/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using NyxSettings;

using Oracle.DataAccess.Client;
using MySql.Data.MySqlClient;
using System.Data.SqlClient;
using Npgsql;
using System.Data.Odbc;

namespace Nyx.Classes
{
    /// <summary> Thread start parameters </summary>
    public class DBThreadStartParam
    {   // private members
        private object obj;
        private string param1;
        private string param2;
        private DatabaseAction thrdAction;
        private DBHelper dbh;

        /// <remarks> object </remarks>
        public object Obj { get { return obj; } set { obj = value; } }
        /// <remarks> connectstring at connectdb / query at query actions and so on </remarks>
        public string Param1 { get { return param1; } set { param1 = value; } }
        /// <remarks> 2cond parameter </remarks>
        public string Param2 { get { return param2; } set { param2 = value; } }
        /// <remarks> thread action </remarks>
        public DatabaseAction ThrAction { get { return thrdAction; } set { thrdAction = value; } }
        /// <remarks> databasehelper to use for opeartions </remarks>
        public DBHelper Dbh { get { return dbh; } set { dbh = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(obj.GetHashCode() + param1.GetHashCode() + param2.GetHashCode() 
                + thrdAction.GetHashCode() + dbh.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(DBThreadStartParam struct1, DBThreadStartParam struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // ==
            if (struct1.obj == struct2.obj
                && struct1.param1 == struct2.param1
                && struct1.param2 == struct2.param2
                && struct1.thrdAction == struct2.thrdAction
                && struct1.dbh == struct2.dbh)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(DBThreadStartParam struct1, DBThreadStartParam struct2) 
        {
            // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // !=
            return !(struct1 == struct2); }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            DBThreadStartParam dbtrp = obj as DBThreadStartParam;
            if (obj != null)
                return this == dbtrp;
            else
                return false;

        }
    }
    /// <summary> threadreturn struct </summary>
    public class DBThreadReturnParam
    {   // private members
        private double affRows;
        private object data1;
        private object data2;
        private Queue<string> gqString1 = new Queue<string>();
        private Queue<string> gqString2 = new Queue<string>();
        private Queue<string> gqString3 = new Queue<string>();
        private DBException dbException;
        private DatabaseAction thrAction;
        private System.Data.DataSet ds;
        private string query = String.Empty;

        /// <remarks> set generic queue </remarks>
        public void SetGQString1(Queue<string> gq) { gqString1 = gq; }
        /// <remarks> set generic queue </remarks>
        public void SetGQString2(Queue<string> gq) { gqString2 = gq; }
        /// <remarks> set generic queue </remarks>
        public void SetGQString3(Queue<string> gq) { gqString3 = gq; }

        /// <remarks> affected rows </remarks>
        public double AffRows { get { return affRows; } set { affRows = value; } }
        /// <remarks> object1 </remarks>
        public object Data1 { get { return data1; } set { data1 = value; } }
        /// <remarks> object2 </remarks>
        public object Data2 { get { return data2; } set { data2 = value; } }
        /// <remarks> generic string queue </remarks>
        public Queue<string> GQString1 { get { return gqString1; } }
        /// <remarks> generic string queue </remarks>
        public Queue<string> GQString2 { get { return gqString2; } }
        /// <remarks> generic string queue </remarks>
        public Queue<string> GQString3 { get { return gqString3; } }
        /// <remarks> database exception, if occured </remarks>
        public DBException DBException { get { return dbException; } set { dbException = value; } }
        /// <remarks> thread action to execute</remarks>
        public DatabaseAction ThrAction { get { return thrAction; } set { thrAction = value; } }
        /// <remarks> dataset </remarks>
        public System.Data.DataSet DS { get { return ds; } set { ds = value; } }
        /// <remarks> query to use </remarks>
        public string Query { get { return query; } set { query = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(affRows.GetHashCode() + data1.GetHashCode() + data2.GetHashCode() 
                + gqString1.GetHashCode() + gqString2.GetHashCode() + gqString3.GetHashCode() + dbException.GetHashCode()
                + thrAction.GetHashCode() + ds.GetHashCode() + query.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(DBThreadReturnParam struct1, DBThreadReturnParam struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // ==
            if (struct1.affRows == struct2.affRows && struct1.data1 == struct2.data1
                && struct1.data2 == struct2.data2 && struct1.gqString1== struct2.gqString1
                && struct1.gqString2 == struct2.gqString2 && struct1.gqString3 ==struct2.gqString3
                && struct1.dbException == struct2.dbException && struct1.thrAction == struct2.thrAction
                && struct1.ds == struct2.ds && struct1.query == struct2.query)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(DBThreadReturnParam struct1, DBThreadReturnParam struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull")); 
            // !=
            return !(struct1 == struct2);
        }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            DBThreadReturnParam dbtrp = obj as DBThreadReturnParam;
            if (dbtrp != null)
                return this == dbtrp;
            else
                return false;
        }
    }
    #region structs for retrieving data
    /// <summary> tabledetails struct </summary>
    public class TableDetails
    {   // private members
        private string name;
        private string hasIndex;
        private string type;
        private string length;
        private string precision;
        private string extra;
        private string fldNull;
        private string defaultValue;
        private string fldAttribute;
        private string collation;
        private string comment;

        /// <remarks> fieldname </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> index on field? </remarks>
        public string HasIndex { get { return hasIndex; } set { hasIndex = value; } }
        /// <remarks> type </remarks>
        public string Type { get { return type; } set { type = value; } }
        /// <remarks> oracle, mssql, pgsql </remarks>
        public string Lenght { get { return length; } set { length = value; } }
        /// <remarks> oracle, mssql </remarks>
        public string Precision { get { return precision; } set { precision = value; } }
        /// <remarks> mysql </remarks>
        public string Extra { get { return extra; } set { extra = value; } }
        /// <remarks> mysql, mssql, pgsql </remarks>
        public string Null { get { return fldNull; } set { fldNull = value; } }
        /// <remarks> postgre, mysql </remarks>
        public string DefaultValue { get { return defaultValue; } set { defaultValue = value; } }
        /// <remarks> mysql </remarks>
        public string Attribute { get { return fldAttribute; } set { fldAttribute = value; } }
        /// <remarks> </remarks>
        public string Collation { get { return collation; } set { collation = value; } }
        /// <remarks> </remarks>
        public string Comment { get { return comment; } set { comment = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(name.GetHashCode() + hasIndex.GetHashCode() + type.GetHashCode() 
                + length.GetHashCode() + precision.GetHashCode() + extra.GetHashCode() + fldNull.GetHashCode()
                + defaultValue.GetHashCode() + fldAttribute.GetHashCode() + collation.GetHashCode() + comment.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(TableDetails struct1, TableDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // ==
            if (struct1.name == struct2.name && struct1.hasIndex == struct2.hasIndex
                && struct1.type == struct2.type && struct1.length == struct2.length
                && struct1.precision == struct2.precision && struct1.extra == struct2.extra
                && struct1.fldNull == struct2.fldNull && struct1.defaultValue == struct2.defaultValue
                && struct1.fldAttribute == struct2.fldAttribute && struct1.collation == struct2.collation
                && struct1.comment == struct2.comment)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(TableDetails struct1, TableDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // !=
            return !(struct1 == struct2);
        }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            TableDetails td = obj as TableDetails;
            if (td != null)
                return this == td;
            else
                return false;
        }
    }
    /// <summary> indexes </summary>
    public class IndexDetails
    {   // private
        private string name;
        private string tablename;
        private string status;
        private string analyzed;
        private bool descending;
        private bool isPrimary;
        private bool isUnique;
        private bool isClustered;
        private bool isFulltext;

        /// <remarks> indexname </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> tablename </remarks>
        public string Tablename { get { return tablename; } set { tablename = value; } }
        /// <remarks> status </remarks>
        public string Status { get { return status; } set { status = value; } }
        /// <remarks> analyzed </remarks>
        public string Analyzed { get { return analyzed; } set { analyzed = value; } }
        /// <remarks> sort order descending? ascending default </remarks>
        public bool Descending { get { return descending; } set { descending = value; } }
        /// <remarks> is primary </remarks>
        public bool IsPrimary { get { return isPrimary; } set { isPrimary = value; } }
        /// <remarks> is unique </remarks>
        public bool IsUnique { get { return isUnique; } set { isUnique = value; } }
        /// <remarks> is clustered </remarks>
        public bool IsClustered { get { return isClustered; } set { isClustered = value; } }
        /// <remarks> is fulltext </remarks>
        public bool IsFulltext { get { return isFulltext; } set { isFulltext = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(name.GetHashCode() + tablename.GetHashCode() + status.GetHashCode() 
                + analyzed.GetHashCode() + descending.GetHashCode() + isPrimary.GetHashCode() + isUnique.GetHashCode()
                + isClustered.GetHashCode() + isFulltext.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(IndexDetails struct1, IndexDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // ==
            if (struct1.name == struct2.name && struct1.tablename == struct2.tablename
                && struct1.status == struct2.status && struct1.analyzed == struct2.analyzed
                && struct1.descending == struct2.descending && struct1.isPrimary == struct2.isPrimary
                && struct1.isUnique == struct2.isUnique && struct1.isClustered == struct2.isClustered
                && struct1.isFulltext == struct2.isFulltext)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(IndexDetails struct1, IndexDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull")); 
            // !=
            return !(struct1 == struct2);
        }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            IndexDetails idx = obj as IndexDetails;
            if (idx != null)
                return this == idx;
            else
                return false;
        }
    }
    /// <summary> IndexDescribe </summary>
    public class IndexDescribe
    {   // private
        private string name;
        private string type;
        private string position;
        private string subpart;
        private string descend;

        /// <remarks> fieldname </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> fieldtype </remarks>
        public string Type { get { return type; } set { type = value; } }
        /// <remarks> position in index </remarks>
        public string Position { get { return position; } set { position = value; } }
        /// <remarks> subpart to use </remarks>
        public string Subpart { get { return subpart; } set { subpart = value; } }
        /// <remarks> sort order descending </remarks>
        public string Descend { get { return descend; } set { descend = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(name.GetHashCode() + type.GetHashCode() + position.GetHashCode() 
                + subpart.GetHashCode() + descend.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(IndexDescribe struct1, IndexDescribe struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // ==
            if (struct1.name == struct2.name && struct1.type == struct2.type
                && struct1.position == struct2.position && struct1.subpart == struct2.subpart
                && struct1.descend == struct2.descend)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(IndexDescribe struct1, IndexDescribe struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // !=
            return !(struct1 == struct2);
        }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            IndexDescribe idxDesc = obj as IndexDescribe;
            if (obj != null)
                return this == idxDesc;
            else
                return false;
        }
    }
    /// <summary> viewdetails </summary>
    public class ViewDetails
    {   // private
        private string name = String.Empty;
        private Queue<string[]> info = new Queue<string[]>();
        private string createStm = String.Empty;

        /// <remarks> viewname </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> view informations (key/value pair) </remarks>
        public Queue<string[]> Info { get { return info; } }
        /// <remarks> </remarks>
        public string CreateStm { get { return createStm; } set { createStm = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(name.GetHashCode() + info.GetHashCode() + createStm.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(ViewDetails struct1, ViewDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // ==
            if (struct1.name == struct2.name
                && struct1.info == struct2.info
                && struct1.createStm == struct2.createStm)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(ViewDetails struct1, ViewDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // !=
            return !(struct1 == struct2);
        }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            ViewDetails vd = obj as ViewDetails;
            if (vd != null)
                return this == vd;
            else
                return false;
        }
    }
    /// <summary> proceduredetails </summary>
    public class ProcedureDetails
    {   // private
        private string name = String.Empty;
        private Queue<string[]> info = new Queue<string[]>();
        private string createStm = String.Empty;

        /// <remarks> procedurename </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> procedure informations (key/value pair) </remarks>
        public Queue<string[]> Info { get { return info; } }
        /// <remarks> create statement </remarks>
        public string CreateStm { get { return createStm; } set { createStm = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(name.GetHashCode() + info.GetHashCode() + createStm.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(ProcedureDetails struct1, ProcedureDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // ==
            if (struct1.name == struct2.name
                && struct1.info == struct2.info
                && struct1.createStm == struct2.createStm)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(ProcedureDetails struct1, ProcedureDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // !=
            return !(struct1 == struct2);
        }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            ProcedureDetails pd = obj as ProcedureDetails;
            if (pd != null)
                return this == pd;
            else
                return false;
        }
    }
    /// <summary> triggerdetails </summary>
    public class TriggerDetails
    {   // private
        private string name = String.Empty;
        private Queue<string[]> info = new Queue<string[]>();
        private string createStm = String.Empty;

        /// <remarks> triggername </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> trigger informations (key/value pair) </remarks>
        public Queue<string[]> Info { get { return info; } }
        /// <remarks> create statement </remarks>
        public string CreateStm { get { return createStm; } set { createStm = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(name.GetHashCode() + info.GetHashCode() + createStm.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(TriggerDetails struct1, TriggerDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // == 
            if (struct1.name == struct2.name
                && struct1.info == struct2.info
                && struct1.createStm == struct2.createStm)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(TriggerDetails struct1, TriggerDetails struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // !=
            return !(struct1 == struct2);
        }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            TriggerDetails td = obj as TriggerDetails;
            if (td != null)
                return this == td;
            else
                return false;
        }
    }
    /// <summary> tablestatistics </summary>
    public class TableStatistic
    {   // private
        private string name = String.Empty;
        private int rows;
        private double size;
        private string msSize = String.Empty;
        private string addInfo = String.Empty;
        private int fieldCount;

        /// <remarks> tablename </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> rowcount </remarks>
        public int Rows { get { return rows; } set { rows = value; } }
        /// <remarks> size used </remarks>
        public double Size { get { return size; } set { size = value; } }
        /// <remarks> mssql size used </remarks>
        public string MSSize { get { return msSize; } set { msSize = value; } }
        /// <remarks> additional infor </remarks>
        public string AddInfo { get { return addInfo; } set { addInfo = value; } }
        /// <remarks> fieldcount </remarks>
        public int FieldCount { get { return fieldCount; } set { fieldCount = value; } }

        /// <summary> override default GetHashCode </summary>
        public override int GetHashCode()
        {
            return (int)System.Math.Sqrt(name.GetHashCode() + rows.GetHashCode() + size.GetHashCode() 
                + msSize.GetHashCode() + addInfo.GetHashCode() + fieldCount.GetHashCode());
        }
        /// <summary> override default == </summary>
        public static bool operator ==(TableStatistic struct1, TableStatistic struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct2", GuiHelper.GetResourceMsg("ArgumentNull"));
            // ==
            if (struct1.name == struct2.name
                && struct1.rows == struct2.rows
                && struct1.size == struct2.size
                && struct1.msSize == struct2.msSize
                && struct1.addInfo == struct2.addInfo
                && struct1.fieldCount == struct2.fieldCount)
                return true;
            else
                return false;
        }
        /// <summary> override default != </summary>
        public static bool operator !=(TableStatistic struct1, TableStatistic struct2)
        {   // validate
            if (struct1 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (struct2 == null)
                throw new ArgumentNullException("struct1", GuiHelper.GetResourceMsg("ArgumentNull"));
            // !=
            return !(struct1 == struct2); 
        }
        /// <summary> override default equals </summary>
        public override bool Equals(object obj)
        {
            TableStatistic ts = obj as TableStatistic;
            if (ts != null)
                return this == ts;
            else
                return false;
        }
    }
    #endregion
    /// <summary>
    /// Database Management Class
    ///     Executes nearly every Get-Operation (for Tables,Views,Preview and so on...)
    ///     Normally every function out of here will run inside a thread.
    /// </summary>
    class DBManagement
    {
        // init vars
        private DBType dbType;

        // error var
        private DBException errExc;
        public DBException ErrExc { get { return errExc; } }

        // last query
        private string lastQuery = String.Empty;
        public string LastQuery { get { return lastQuery; } }

        /// <summary>
        /// ParameterizedThreadDelegate, gets invoked after a thread action finished 
        /// </summary>
        /// <param name="param">struct StructThrdRetVal</param>
        public delegate void DbmThrdEndPDelegate(DBThreadReturnParam param);
        
        // constructor
        public DBManagement(DBType dbType)
        {   // init database helper
            this.dbType = dbType;
        }

        public bool GetDatabases(out Queue<string> qDbs, DBHelper dbh)
        {
            qDbs = new Queue<string>();
            // checkup
            if (this.dbType != DBType.MySql && this.dbType != DBType.MSSql)
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                return false;
            }
            // retrieve dbs
            if (dbh.ExecuteReader(dbh.GetQuery(DBHelper.EnumGetQueryType.Databases)))
            {   // filling combobox
                try
                {
                    while (dbh.Read())
                        qDbs.Enqueue(dbh.GetValue(0).ToString());
                }
                catch (IndexOutOfRangeException exc)
                {
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabasesRetrieveErr", new string[] { exc.Message.ToString() }), exc);
                    return false;
                }
                finally { dbh.Close(); }
                return true;
            }
            else
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("DatabasesRetrieveErr", new string[] { dbh.ErrExc.Message.ToString() }), dbh.ErrExc);
                return false;
            }
        }

        public bool GetTriggers(out Queue<string> qTriggers, bool userscope, DBHelper dbh)
        {
            string qry = String.Empty;
            qTriggers = new Queue<string>();
            // get query
            switch (this.dbType)
            {
                case DBType.MySql:
                case DBType.Oracle:
                    if (userscope)
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserTriggers);
                    else
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.AllTriggers);
                    break;
                case DBType.PgSql:
                case DBType.MSSql:
                    qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserTriggers);
                    break;
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                    return false;
            }
            if (dbh.ExecuteReader(qry))
            {
                try
                {
                    while (dbh.Read())
                        qTriggers.Enqueue(dbh.GetValue(0).ToString());
                }
                catch (IndexOutOfRangeException exc)
                {
                    errExc = new DBException(GuiHelper.GetResourceMsg("TriggerGetErr"), exc);
                    return false;
                }
                finally { dbh.Close(); }
                return true;
            }
            else
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("TriggerGetErr"), dbh.ErrExc);
                return false;
            }
        }
        public bool GetTriggerDetails(string trigger, out TriggerDetails strTriggerDet, DBHelper dbh)
        {   // init
            strTriggerDet = new TriggerDetails();
            strTriggerDet.Name = trigger;
            string qry = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.TriggerDetails), trigger);
            // set column name where to get the statement from
            string colname = String.Empty;
            switch (this.dbType)
            {
                case DBType.PgSql:
                case DBType.MySql:
                    colname = "ACTION_STATEMENT";
                    break;
                case DBType.Oracle:
                    colname = "TRIGGER_BODY";
                    break;
            }
            // exec
            if (dbh.ExecuteReader(qry))
            {   // read
                try
                {
                    if (dbh.Read())
                    {
                        for (int i = 0; i < dbh.FieldCount; i++)
                        {
                            string[] procDet = new string[2];
                            procDet[0] = dbh.GetName(i).ToString();
                            procDet[1] = dbh.GetValue(i).ToString();
                            if (colname.Length > 0 && procDet[0] == colname)
                                strTriggerDet.CreateStm = procDet[1];
                            // queue
                            strTriggerDet.Info.Enqueue(procDet);
                        }
                    }
                    return true;
                }
                catch (IndexOutOfRangeException exc)
                {
                    errExc = new DBException(GuiHelper.GetResourceMsg("TriggerGetDetailsErr"), exc);
                    return false;
                }
                finally { dbh.Close(); }
            }
            else
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("TriggerGetDetailsErr"), dbh.ErrExc);
                return false;
            }
        }

        public bool GetViews(out Queue<string> qViews, bool userscope, DBHelper dbh)
        {
            string qry = String.Empty;
            qViews = new Queue<string>();
            // check dbtype
            switch (this.dbType)
            {
                case DBType.MSSql:
                case DBType.MySql:
                    qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserViews);
                    break;
                case DBType.Oracle:
                case DBType.PgSql:
                    if (userscope)
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserViews);
                    else
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.AllViews);
                    break;
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                    return false;
            }
            // execute
            if (dbh.ExecuteReader(qry))
            {
                try
                {   // check if postgresql and if we shall retrieve all views, if so we have to check the schemaname (bool faster in while...)
                    bool pgSpecialCase = false;
                    if (!userscope && this.dbType == DBType.PgSql)
                        pgSpecialCase = true;
                    // retrieve views
                    while (dbh.Read())
                    {   // special case for postgre
                        if (pgSpecialCase)
                        {
                            string schema = dbh.GetValue(1).ToString();
                            if (schema != NyxMain.DBName)
                                qViews.Enqueue(String.Format(NyxMain.NyxCI, "{0}.{1}", schema, dbh.GetValue(0).ToString()));
                            else
                                qViews.Enqueue(dbh.GetValue(0).ToString());
                        }
                        else
                            qViews.Enqueue(dbh.GetValue(0).ToString());
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetErr"), exc);
                    return false;
                }
                finally { dbh.Close(); }
                return true;
            }
            else
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetErr"), dbh.ErrExc);
                return false;
            }
        }
        public bool GetViewDetails(string view, out ViewDetails strViewDet, DBHelper dbh)
        {   // init
            strViewDet = new ViewDetails();
            strViewDet.Name = view;
            //strViewDet.Info = new Queue<string[]>();
            string qryDet = dbh.GetQuery(DBHelper.EnumGetQueryType.ViewDetails);
            string qryStm = dbh.GetQuery(DBHelper.EnumGetQueryType.ViewDetailsStm);
            // get details
            switch (this.dbType)
            {
                case DBType.MySql:
                    // getting create procedure
                    if (dbh.ExecuteReader(String.Format(NyxMain.NyxCI, qryStm, view)))
                    {
                        try
                        {
                            if(dbh.Read())
                                strViewDet.CreateStm = dbh.GetValue(1).ToString();
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), exc); 
                            return false;
                        }
                        finally { dbh.Close(); }
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                    // getting the values out of rountime_schema 
                    if (dbh.ExecuteReader(String.Format(NyxMain.NyxCI, qryDet, view)))
                    {
                        try
                        {
                            if (dbh.Read())
                            {
                                for (int i = 0; i < dbh.FieldCount; i++)
                                {
                                    string[] procDet = new string[2];
                                    procDet[0] = dbh.GetName(i).ToString();
                                    procDet[1] = dbh.GetValue(i).ToString();
                                    strViewDet.Info.Enqueue(procDet);
                                }
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.Oracle:
                    // get length
                    int defLongFetchSize = DBHelper.OrclInitLongFetchSize;
                    int tmpLongFetchSize = defLongFetchSize;
                    // exec
                    if (dbh.ExecuteReader("SELECT text_length FROM all_views WHERE view_name = '" + view + "'"))
                    {   // get initlongfetchsize
                        try
                        {   
                            if(dbh.Read())
                                tmpLongFetchSize = (int)dbh.GetOracleDecimal(0);
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        // set initlongfetchsize
                        DBHelper.OrclInitLongFetchSize = tmpLongFetchSize;
                        // get details
                        if (dbh.ExecuteReader(String.Format(NyxMain.NyxCI, qryDet, view)))
                        {   // get the describe
                            try
                            {
                                if (dbh.Read())
                                {
                                    for (int i = 0; i < dbh.FieldCount; i++)
                                    {
                                        string[] procDet = new string[2];
                                        procDet[0] = dbh.GetName(i).ToString();
                                        procDet[1] = dbh.GetValue(i).ToString();
                                        if (procDet[0] == "TEXT")
                                            strViewDet.CreateStm = procDet[1];
                                        strViewDet.Info.Enqueue(procDet);
                                    }
                                }
                            }
                            catch (IndexOutOfRangeException exc)
                            {
                                errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), exc);
                                return false;
                            }
                            finally
                            {
                                DBHelper.OrclInitLongFetchSize = defLongFetchSize;
                                dbh.Close();
                            }
                            return true;
                        }
                        else
                        {
                            errExc = new DBException("Error retrieving View-Details.\n" + dbh.ErrExc.Message.ToString());
                            return false;
                        }
                    }
                    else
                    {
                        errExc = new DBException("Error retrieving View-Details.\n" + dbh.ErrExc.Message.ToString());
                        return false;
                    }
                case DBType.MSSql:
                    if (dbh.ExecuteReader(String.Format(NyxMain.NyxCI, qryDet, view)))
                    {
                        try
                        {
                            if (dbh.Read())
                            {
                                for (int i = 0; i < dbh.FieldCount; i++)
                                {
                                    string[] procDet = new string[2];
                                    procDet[0] = dbh.GetName(i).ToString();
                                    procDet[1] = dbh.GetValue(i).ToString();
                                    if (procDet[0] == "VIEW_DEFINITION")
                                        strViewDet.CreateStm = procDet[1];
                                    strViewDet.Info.Enqueue(procDet);
                                }
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.PgSql:
                    // view not inside our database ?
                    int idx = view.IndexOf('.');
                    if (idx > 0)
                        view = view.Substring(idx + 1);

                    if (dbh.ExecuteReader(String.Format(NyxMain.NyxCI, qryDet, view)))
                    {
                        try
                        {
                            if (dbh.Read())
                            {
                                for (int i = 0; i < dbh.FieldCount; i++)
                                {
                                    string[] procDet = new string[2];
                                    procDet[0] = dbh.GetName(i).ToString();
                                    procDet[1] = dbh.GetValue(i).ToString();
                                    if (procDet[0] == "definition")
                                        strViewDet.CreateStm = procDet[1];
                                    strViewDet.Info.Enqueue(procDet);
                                }
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("ViewGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr")); ;
                    return false;
            }
        }

        public bool GetProcedures(out Queue<string> qProcedures, bool userscope, DBHelper dbh)
        {
            string qry = String.Empty;
            qProcedures = new Queue<string>();
            // get query
            switch (this.dbType)
            {
                case DBType.Oracle:
                    if (userscope)
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserProcedures);
                    else
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.AllProcedures);
                    break;
                case DBType.MySql:
                    qry = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.UserProcedures), NyxMain.DBName);
                    break;
                case DBType.MSSql:
                case DBType.PgSql:
                    qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserProcedures);
                    break;
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                    return false;
            }
            // exec
            if (dbh.ExecuteReader(qry))
            {
                try
                {
                    while (dbh.Read())
                        qProcedures.Enqueue(dbh.GetValue(0).ToString());
                }
                catch (IndexOutOfRangeException exc)
                {
                    errExc = new DBException(GuiHelper.GetResourceMsg("ProceduresGetErr"), exc);
                    return false;
                }
                finally { dbh.Close(); }
                return true;
            }
            else
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("ProceduresGetErr"), dbh.ErrExc);
                return false;
            }
        }
        public bool GetProcedureDetails(string procedure, out ProcedureDetails strProcedureDet, DBHelper dbh)
        {   // init
            strProcedureDet = new ProcedureDetails();
            strProcedureDet.Name = procedure;
            //strProcedureDet.Info = new Queue<string[]>();
            string qryDet = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.ProcedureDetails), procedure);
            string qryStm = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.ProcedureDetailsStm), procedure);
            string colCreateStm = String.Empty;

            // check dbtype
            switch (this.dbType)
            {
                case DBType.MySql:
                    // getting create procedure
                    if (dbh.ExecuteReader(qryStm))
                    {
                        try
                        {
                            if(dbh.Read())
                                strProcedureDet.CreateStm = dbh.GetValue(2).ToString();
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("ProceduresGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("ProceduresGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                    break;
                case DBType.MSSql:
                    colCreateStm = "ROUTINE_DEFINITION";
                    break;
                case DBType.PgSql:
                    colCreateStm = "Source";
                    break;
            }
            // get details
            if (dbh.ExecuteReader(qryDet))
            {
                try
                {
                    if (dbh.Read())
                    {
                        for (int i = 0; i < dbh.FieldCount; i++)
                        {   // details
                            string[] procDet = new string[2];
                            procDet[0] = dbh.GetName(i).ToString();
                            procDet[1] = dbh.GetValue(i).ToString();
                            strProcedureDet.Info.Enqueue(procDet);
                            // create statement
                            if (colCreateStm.Length > 0 && procDet[0] == colCreateStm)
                                strProcedureDet.CreateStm = procDet[1];
                        }
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    errExc = new DBException(GuiHelper.GetResourceMsg("ProceduresGetDetailsErr"), exc);
                    return false;
                }
                finally { dbh.Close(); }
                return true;
            }
            else
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("ProceduresGetDetailsErr"), dbh.ErrExc);
                return false;
            }
        }

        public bool GetTables(out Queue<string> qTables, bool userscope, DBHelper dbh)
        {
            string qry = String.Empty;
            qTables = new Queue<string>();
            // special cases
            bool pgSpecialCase = false, mySqlSpecialCase = false;
            // get query
            switch (this.dbType)
            {
                case DBType.Oracle:
                    if (userscope)
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserTables);
                    else
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.AllTables);
                    break;
                case DBType.PgSql:
                    if (userscope)
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserTables);
                    else
                    {   // set pg special case
                        pgSpecialCase = true;
                        qry = dbh.GetQuery(DBHelper.EnumGetQueryType.AllTables);
                    }
                    break;
                case DBType.MySql:
                    int iVnr;
                    if (Int32.TryParse(NyxMain.DBVersion.Substring(7, 1), out iVnr) && iVnr >= 5)
                        mySqlSpecialCase = true;
                    qry = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.UserTables), NyxMain.DBName);
                    break;
                case DBType.MSSql:
                    qry = dbh.GetQuery(DBHelper.EnumGetQueryType.UserTables);
                    break;
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                    return false;
            }
            // exec
            if (dbh.ExecuteReader(qry))
            {
                try
                {
                    while (dbh.Read())
                    {
                        if (mySqlSpecialCase)
                        {
                            if (dbh.GetValue(1).ToString().ToUpper(NyxMain.NyxCI) == "BASE TABLE")
                                qTables.Enqueue(dbh.GetValue(0).ToString());
                        }
                        else if (pgSpecialCase)
                        {
                            string schema = dbh.GetValue(1).ToString();
                            if (schema != NyxMain.DBName)
                                qTables.Enqueue(schema + "." + dbh.GetValue(0).ToString());
                            else
                                qTables.Enqueue(dbh.GetValue(0).ToString());
                        }
                        else
                            qTables.Enqueue(dbh.GetValue(0).ToString());
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    errExc = new DBException(GuiHelper.GetResourceMsg("CI_TablesRetrieveErr"), exc);
                    return false;
                }
                finally { dbh.Close(); }
                return true;
            }
            else
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("CI_TablesRetrieveErr"), dbh.ErrExc);
                return false;
            }
        }
        public bool GetTableDetails(string table, out Queue<TableDetails> qTableDetails, DBHelper dbh)
        {   // init
            string qryIndCols = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.IndexedColumns), table);
            string qryTblCols = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.TableColumns), table);
            List<string> primary = new List<string>(), unique = new List<string>(), index = new List<string>(), fulltext = new List<string>();
            qTableDetails = new Queue<TableDetails>();
            // exec
            switch (this.dbType)
            {
                case DBType.Oracle:
                    // getting indexed columns
                    if (dbh.ExecuteReader(qryIndCols))
                    {   // read
                        try
                        {
                            string readField = String.Empty;
                            while (dbh.Read())
                            {
                                readField = dbh.GetValue(0).ToString();
                                // primary index?
                                if (dbh.GetValue(1).ToString() == "P")
                                {
                                    if (!primary.Contains(readField))
                                        primary.Add(readField);
                                }
                                // normal index
                                else
                                {
                                    if (!index.Contains(readField))
                                        index.Add(readField);
                                }
                                if (!unique.Contains(readField) && dbh.GetValue(2).ToString() == "UNIQUE")
                                    unique.Add(readField);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                    // getting columns
                    if (dbh.ExecuteReader(qryTblCols))
                    {   // read
                        try
                        {
                            while (dbh.Read())
                            {   // fillup vars
                                string idx = String.Empty;
                                string readFld = dbh.GetValue(0).ToString();
                                // check indexes
                                if (primary.Contains(readFld))
                                    idx = "P ";
                                if (index.Contains(readFld))
                                    idx = String.Format(NyxMain.NyxCI, "{0}I ", idx);
                                if (unique.Contains(readFld))
                                    idx = String.Format(NyxMain.NyxCI, "{0}U", idx);
                                // fill struct
                                TableDetails tblDesc = new TableDetails();
                                tblDesc.Name = readFld;
                                tblDesc.HasIndex = idx;
                                tblDesc.Type = dbh.GetValue(1).ToString();
                                tblDesc.Lenght = dbh.GetValue(2).ToString();
                                tblDesc.Precision = dbh.GetValue(3).ToString();
                                // add to hashtable :)
                                qTableDetails.Enqueue(tblDesc);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.MySql:
                    // first getting the indexed columns
                    if (dbh.ExecuteReader(qryIndCols))
                    {   
                        try
                        { // read
                            while (dbh.Read())
                            {   // temp var
                                string readFld = dbh.GetValue(4).ToString();
                                if (dbh.GetValue(2).ToString() == "PRIMARY")
                                {   // primary index
                                    if (!primary.Contains(readFld))
                                        primary.Add(readFld);
                                }
                                else
                                {   // normal index
                                    if (!index.Contains(readFld))
                                        index.Add(readFld);
                                }
                                // unique ?
                                if (!unique.Contains(readFld) && dbh.GetValue(1).ToString() == "0")
                                    unique.Add(readFld);
                                // check fulltext and be aware of mysql 3(fieldcount)
                                if (dbh.FieldCount >= 10 && dbh.GetValue(10).ToString().ToUpper(NyxMain.NyxCI) == "FULLTEXT")
                                    fulltext.Add(readFld);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                    // getting columns
                    if (dbh.ExecuteReader(qryTblCols))
                    {
                        try
                        {   // read
                            while (dbh.Read())
                            {   // fillup vars
                                string idx = String.Empty;
                                string readFld = dbh.GetValue(0).ToString();
                                // check the indexes
                                if (primary.Contains(readFld))
                                    idx = "P ";
                                if (index.Contains(readFld))
                                    idx = String.Format(NyxMain.NyxCI, "{0}I ", idx);
                                if (unique.Contains(readFld))
                                    idx = String.Format(NyxMain.NyxCI, "{0}U ", idx);
                                if (fulltext.Contains(readFld))
                                    idx = String.Format(NyxMain.NyxCI, "{0}F", idx);
                                // fill struct
                                TableDetails tblDesc = new TableDetails();
                                tblDesc.Name = readFld;
                                tblDesc.HasIndex = idx;
                                tblDesc.Type = dbh.GetValue(1).ToString();
                                tblDesc.Collation = dbh.GetValue(2).ToString();
                                tblDesc.Null = dbh.GetValue(3).ToString();
                                tblDesc.DefaultValue = dbh.GetValue(5).ToString();
                                tblDesc.Extra = dbh.GetValue(6).ToString();
                                // fix for mysql 4.0 (comment didn't exist this long time ago...)
                                if (dbh.FieldCount == 8)
                                    tblDesc.Comment = dbh.GetValue(8).ToString();
                                // add to hashtable
                                qTableDetails.Enqueue(tblDesc);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.MSSql:
                    // first getting the indexes, to display them inside the listview
                    if (dbh.ExecuteReader(qryIndCols))
                    {
                        try
                        {   // read
                            while (dbh.Read())
                            {   // temp var
                                if (dbh.GetValue(20).ToString() == "PRIMARY")
                                {
                                    for (int i = 4; i < 20; i++)
                                    {
                                        if (dbh.GetValue(i).ToString().Length > 0)
                                            primary.Add(dbh.GetValue(i).ToString());
                                    }
                                }
                                else
                                {
                                    for (int i = 4; i < 20; i++)
                                    {
                                        if (dbh.GetValue(i).ToString().Length > 0)
                                            index.Add(dbh.GetValue(i).ToString());
                                    }
                                }
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                    // now getting the describe and fillingl istview
                    if (dbh.ExecuteReader(qryTblCols))
                    {
                        try
                        {   // read
                            while (dbh.Read())
                            {   // fillup vars
                                string idx = String.Empty;
                                string readFld = dbh.GetValue(3).ToString();
                                // check the indexes
                                if (primary.Contains(readFld))
                                    idx = "P ";
                                if (index.Contains(readFld))
                                    idx = String.Format(NyxMain.NyxCI, "{0}I", idx);
                                // fill struct
                                TableDetails tblDesc = new TableDetails();
                                tblDesc.Name = readFld;
                                tblDesc.HasIndex = idx;
                                tblDesc.Type = dbh.GetValue(5).ToString();
                                tblDesc.Lenght = dbh.GetValue(7).ToString();
                                tblDesc.Precision = dbh.GetValue(6).ToString();
                                tblDesc.Null = dbh.GetValue(17).ToString();
                                // add to hashtable
                                qTableDetails.Enqueue(tblDesc);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.PgSql:
                    // now getting the describe and fillingl istview
                    if (dbh.ExecuteReader(qryTblCols))
                    {
                        try
                        {   // read
                            while (dbh.Read())
                            {   // fillup vars
                                string idx = String.Empty;
                                string readFld = dbh.GetValue(1).ToString();
                                // check the indexes
                                if (primary.Contains(readFld))
                                    idx = "P ";
                                if (index.Contains(readFld))
                                    idx = String.Format(NyxMain.NyxCI, "{0}I", idx);
                                // fill struct
                                TableDetails tblDesc = new TableDetails();
                                tblDesc.Name = readFld;
                                tblDesc.HasIndex = idx;
                                tblDesc.Type = dbh.GetValue(2).ToString();
                                tblDesc.Lenght = dbh.GetValue(3).ToString();
                                tblDesc.Precision = dbh.GetValue(4).ToString();
                                tblDesc.Null = dbh.GetValue(5).ToString();
                                tblDesc.DefaultValue = dbh.GetValue(6).ToString();
                                // add to hashtable
                                qTableDetails.Enqueue(tblDesc);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TablesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr")); ;
                    return false;
            }
        }

        public bool GetIndexes(string table, out Queue<IndexDetails> qIndexes, DBHelper dbh)
        {   // init
            string qry = String.Empty;
            IndexDetails strIdxDet;
            qIndexes = new Queue<IndexDetails>();
            // exec
            switch (this.dbType)
            {
                case DBType.MySql:
                    qry = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.Indexes), table);
                    if (dbh.ExecuteReader(qry))
                    {   // init vars
                        string lastidx = String.Empty;
                        // read
                        try
                        {
                            while (dbh.Read())
                            {
                                string idxName = dbh.GetValue(2).ToString();
                                if (lastidx.Length == 0 || lastidx != idxName)
                                {
                                    // setting temp-lastindex
                                    lastidx = idxName;
                                    // setting index details
                                    strIdxDet = new IndexDetails();
                                    strIdxDet.Name = idxName;
                                    strIdxDet.Tablename = dbh.GetValue(0).ToString();
                                    if (dbh.GetValue(1).ToString() == "1")
                                        strIdxDet.IsUnique = false;
                                     else 
                                        strIdxDet.IsUnique = true;
                                    if (String.Compare(dbh.GetValue(10).ToString(), "FULLTEXT", true, NyxMain.NyxCI) == 0)
                                        strIdxDet.IsFulltext = true;
                                    else
                                        strIdxDet.IsFulltext = false;
                                    // add to queue
                                    qIndexes.Enqueue(strIdxDet);
                                }
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.Oracle:
                    qry = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.Indexes), table);
                    if (dbh.ExecuteReader(qry))
                    {   // read
                        try
                        {
                            while (dbh.Read())
                            {
                                strIdxDet = new IndexDetails();
                                strIdxDet.Name = dbh.GetValue(0).ToString();
                                if(dbh.GetValue(4).ToString() == "P")
                                    strIdxDet.IsPrimary = true;
                                else
                                    strIdxDet.IsPrimary = false;
                                if (dbh.GetValue(1).ToString() == "UNIQUE")
                                    strIdxDet.IsUnique = true;
                                else
                                    strIdxDet.IsUnique = false;
                                strIdxDet.Status = dbh.GetValue(2).ToString();
                                strIdxDet.Analyzed = dbh.GetValue(3).ToString();
                                // add to queue
                                qIndexes.Enqueue(strIdxDet);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.MSSql:
                    qry = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.Indexes), table);
                    if (dbh.ExecuteReader(qry))
                    {   // read
                        try
                        {
                            while (dbh.Read())
                            {
                                strIdxDet = new IndexDetails();
                                strIdxDet.Name = dbh.GetValue(0).ToString();
                                strIdxDet.Status = dbh.GetValue(1).ToString();
                                if (dbh.GetValue(21).ToString() == "1") 
                                    strIdxDet.IsFulltext = true;
                                else 
                                    strIdxDet.IsFulltext = false;
                                if (dbh.GetValue(22).ToString() == "1") 
                                    strIdxDet.Descending = true;
                                else 
                                    strIdxDet.Descending = false;
                                // add to queue
                                qIndexes.Enqueue(strIdxDet);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.PgSql:
                    qry = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.Indexes), table);
                    if (dbh.ExecuteReader(qry))
                    {   // read
                        try
                        {
                            while (dbh.Read())
                            {
                                strIdxDet = new IndexDetails();
                                strIdxDet.IsUnique = dbh.GetBoolean(0);
                                strIdxDet.IsPrimary = dbh.GetBoolean(1);
                                strIdxDet.IsClustered = dbh.GetBoolean(2);
                                strIdxDet.Name = dbh.GetValue(3).ToString();
                                // add to queue
                                qIndexes.Enqueue(strIdxDet);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetErr"), dbh.ErrExc);
                        return false;
                    }
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr")); ;
                    return false;
            }
        }
        public bool GetIndexDetails(string table, string index, out Queue<IndexDescribe> qIndexDesc, DBHelper dbh)
        {
            string qryIdxDet = String.Empty;
            IndexDescribe strIdxDesc;
            qIndexDesc = new Queue<IndexDescribe>();
            switch (this.dbType)
            {
                case DBType.MySql:
                    qryIdxDet = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.IndexDetails), table);
                    if (dbh.ExecuteReader(qryIdxDet))
                    {
                        try
                        {   // read 
                            while (dbh.Read())
                            {
                                if (dbh.GetValue(2).ToString() == index)
                                {
                                    strIdxDesc = new IndexDescribe();
                                    strIdxDesc.Name = dbh.GetValue(4).ToString();
                                    strIdxDesc.Position = dbh.GetValue(3).ToString();
                                    strIdxDesc.Subpart = dbh.GetValue(7).ToString();
                                    strIdxDesc.Type = dbh.GetValue(10).ToString();
                                    // enqueue
                                    qIndexDesc.Enqueue(strIdxDesc);
                                }
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.Oracle:
                    qryIdxDet = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.IndexDetails), table, index);
                    if (dbh.ExecuteReader(qryIdxDet))
                    {
                        try
                        {   // read
                            while (dbh.Read())
                            {
                                strIdxDesc = new IndexDescribe();
                                strIdxDesc.Name = dbh.GetValue(0).ToString();
                                strIdxDesc.Type = dbh.GetValue(1).ToString();
                                strIdxDesc.Position = dbh.GetValue(2).ToString();
                                strIdxDesc.Descend = dbh.GetValue(3).ToString();
                                // add to queue
                                qIndexDesc.Enqueue(strIdxDesc);
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.MSSql:
                    qryIdxDet = String.Format(NyxMain.NyxCI, dbh.GetQuery(DBHelper.EnumGetQueryType.IndexDetails), table);
                    if (dbh.ExecuteReader(qryIdxDet))
                    {
                        try
                        {   // read
                            while (dbh.Read())
                            {
                                if (dbh.GetValue(0).ToString() == index)
                                {
                                    strIdxDesc = new IndexDescribe();
                                    for (int i = 4; i < 20; i++)
                                    {
                                        if (dbh.GetValue(i).ToString().Length > 0)
                                            strIdxDesc.Name = dbh.GetValue(i).ToString();
                                    }
                                    qIndexDesc.Enqueue(strIdxDesc);
                                }
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetDetailsErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("IndexesGetDetailsErr"), dbh.ErrExc);
                        return false;
                    }
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr")); ;
                    return false;
            }
        }

        public bool PreviewGetObjects(bool userscope, DBHelper dbh, out List<PreviewObject> lpo)
        {
            lpo = new List<PreviewObject>();
            Queue<string> qTables, qViews;

            switch (this.dbType)
            {
                case DBType.MySql:
                    // getting tables
                    if (!this.GetTables(out qTables, userscope, dbh))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewTablesRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    // check after correct mysql-version
                    int iVnr;
                    if (Int32.TryParse(NyxMain.DBVersion.Substring(7, 1), out iVnr) && iVnr < 5)
                        qViews = new Queue<string>();
                    else
                    {   // getting views
                        if (!this.GetViews(out qViews, userscope, dbh))
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("PreviewViewsRetrieveErr"), dbh.ErrExc);
                            return false;
                        }
                    }
                    break;
                case DBType.MSSql:
                case DBType.PgSql:
                case DBType.Oracle:
                    // getting tables
                    if (!this.GetTables(out qTables, userscope, dbh))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewTablesRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    // getting views
                    if (!this.GetViews(out qViews, userscope, dbh))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewViewsRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    break;
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr")); ;
                    return false;
            }
            // finally adding both to an array and sort, afterwards ad to qPreviewObjects for returning
            string[] tmpSort = new string[qTables.Count + qViews.Count];
            int i = 0;
            foreach (string table in qTables)
            {
                tmpSort[i] = table;
                lpo.Add(new PreviewObject(table, "Table"));
                i++;
            }
            foreach (string view in qViews)
            {
                tmpSort[i] = view;
                lpo.Add(new PreviewObject(view, "View"));
                i++;
            }
            lpo.Sort(delegate(PreviewObject po1, PreviewObject po2) { return po1.Name.CompareTo(po2.Name); });
            // return :)
            return true;
        }
        public bool PreviewInit(PreviewQuery prvQry, out int rowCnt, out System.Data.DataSet dsData, out Queue<String> qTblFields, DBHelper dbh, DatabaseAction dbAction)
        {   // init
            dsData = new System.Data.DataSet("Preview");
            dsData.Locale = NyxMain.NyxCI;
            // init vars
            rowCnt = 0;
            Queue<TableDetails> qTblDetails;
            qTblFields = new Queue<String>();
            // get tablecount
            lastQuery = String.Format(NyxMain.NyxCI, "SELECT COUNT(*) FROM {0}", prvQry.Table);
            if (dbh.ExecuteReader(lastQuery))
            {
                try
                {   // getting count
                    if (dbh.Read())
                    {
                        string res = dbh.GetValue(0).ToString();
                        // checking count
                        if (!Int32.TryParse(res, out rowCnt))
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                            return false;
                        }
                    }
                }
                catch (IndexOutOfRangeException exc)
                {
                    errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), exc);
                    return false;
                }
                finally { dbh.Close(); }
            }
            else
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                return false;
            }
            // get tabledetails only if init, not filterinit
            if (dbAction == DatabaseAction.PreviewInit)
            {
                if (this.GetTableDetails(prvQry.Table, out qTblDetails, dbh))
                {
                    foreach (TableDetails tblDet in qTblDetails)
                        qTblFields.Enqueue(tblDet.Name);
                }
            }
            /* 
             * execute first query (get count of the tables)
            */
            switch (this.dbType)
            {
                case DBType.Oracle:
                    // building query
                    if (prvQry.RowsPerPage != -1)
                    {
                        if (prvQry.WhereStm.Length > 0)
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM (SELECT {0} FROM {1} WHERE {2}) WHERE ROWNUM < {3}",
                                prvQry.Columns, prvQry.Table, prvQry.WhereStm, prvQry.RowsPerPage);
                        else
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM (SELECT {0} FROM {1}) WHERE ROWNUM < {2}", prvQry.Columns, prvQry.Table, prvQry.RowsPerPage);
                    }
                    else
                    {
                        if (prvQry.WhereStm.Length > 0)
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM {1} WHERE {2}", prvQry.Columns, prvQry.Table, prvQry.WhereStm);
                        else
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM {1}", prvQry.Columns, prvQry.Table);
                    }
                    // exec query
                    if (!dbh.FillDS(lastQuery, ref dsData))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    break;
                case DBType.MySql:
                    // building query
                    if (prvQry.RowsPerPage != -1)
                    {
                        if (prvQry.WhereStm.Length > 0)
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM {1} WHERE {2} LIMIT {3}", 
                                                        prvQry.Columns, prvQry.Table, prvQry.WhereStm, prvQry.RowsPerPage);
                        else
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM {1} LIMIT {2}", prvQry.Columns, prvQry.Table, prvQry.RowsPerPage);
                    }
                    else
                    {
                        if (prvQry.WhereStm.Length > 0)
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM {1} WHERE {2}", prvQry.Columns, prvQry.Table, prvQry.WhereStm);
                        else
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM {1}", prvQry.Columns, prvQry.Table);
                    }
                    // exec query
                    if (!dbh.FillDS(lastQuery, ref dsData))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    break;
                case DBType.MSSql:
                    // building query
                    if (prvQry.RowsPerPage != -1)
                    {
                        if (prvQry.WhereStm.Length > 0)
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT TOP {0} {1} FROM {2} WHERE {3}", 
                                                        prvQry.RowsPerPage, prvQry.Columns, prvQry.Table, prvQry.WhereStm);
                        else
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT TOP {0} {1} FROM {2}", prvQry.RowsPerPage, prvQry.Columns, prvQry.Table);
                    }
                    else
                    {
                        if (prvQry.WhereStm.Length > 0)
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM {1} WHERE {2}", prvQry.Columns, prvQry.Table, prvQry.WhereStm);
                        else
                            lastQuery = String.Format(NyxMain.NyxCI, "SELECT {0} FROM {1}", prvQry.Columns, prvQry.Table);
                    }
                    // exec query
                    if (!dbh.FillDS(lastQuery, ref dsData))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    break;
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr")); ;
                    break;
            }
            return true;
        }
        public bool PreviewGet(PreviewQuery prvQry, out System.Data.DataSet dsData, DBHelper dbh)
        {   // init vars
            dsData = new System.Data.DataSet("Preview");
            dsData.Locale = NyxMain.NyxCI;
            int rowEnd;
            string col1 = String.Empty, cols = String.Empty;
            //
            switch (this.dbType)
            {
                case DBType.Oracle:
                    rowEnd = prvQry.RowStart + prvQry.RowsPerPage;
                    // therefor oracle sucks and got no limit, we have to get first the columns of the table
                    lastQuery = String.Format(NyxMain.NyxCI, "SELECT column_name FROM user_tab_columns WHERE table_name = '{0}'", prvQry.Table);
                    if (dbh.ExecuteReader(lastQuery))
                    {
                        try
                        {   // read
                            while (dbh.Read())
                            {
                                if (cols.Length == 0)
                                {
                                    cols = dbh.GetValue(0).ToString();
                                    col1 = cols;
                                }
                                else
                                    cols = String.Format(NyxMain.NyxCI, "{0},{1}", cols, dbh.GetValue(0).ToString());
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    // now getting the next results
                    lastQuery = String.Format(NyxMain.NyxCI, "SELECT * FROM (SELECT ROW_NUMBER() OVER(ORDER BY {0}) rownum1,{1} FROM {2}) WHERE rownum1 BETWEEN {3} AND {4}",
                                        col1, cols, prvQry.Table, prvQry.RowStart, rowEnd);
                    // exec query
                    if (dbh.FillDS(lastQuery, ref dsData))
                    {   // remove rownum colun
                        if(dsData.Tables[0].Columns["ROWNUM1"] != null)
                            dsData.Tables[0].Columns.Remove("ROWNUM1");
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    return true;
                case DBType.MySql:
                    lastQuery = String.Format(NyxMain.NyxCI, "SELECT * FROM {0} LIMIT {1},{2}", prvQry.Table, prvQry.RowStart, prvQry.RowsPerPage);
                    if (!dbh.FillDS(lastQuery, ref dsData))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    return true;
                case DBType.MSSql:
                    rowEnd = prvQry.RowStart + prvQry.RowsPerPage;
                    // therefor oracle sucks and got no limit, we have to get first the columns of the table
                    lastQuery = String.Format(NyxMain.NyxCI, "exec sp_columns {0}", prvQry.Table);
                    if (dbh.ExecuteReader(lastQuery))
                    {
                        try
                        {   // read
                            while (dbh.Read())
                            {
                                if (cols.Length == 0)
                                {
                                    cols = dbh.GetValue(3).ToString();
                                    col1 = cols;
                                }
                                else
                                    cols = String.Format(NyxMain.NyxCI, "{0},{1}", cols, dbh.GetValue(3).ToString());
                            }
                        }
                        catch (IndexOutOfRangeException exc)
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), exc);
                            return false;
                        }
                        finally { dbh.Close(); }
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    // now getting the next results
                    lastQuery = String.Format(NyxMain.NyxCI, "SELECT TOP {0} * FROM {1} WHERE ({2} NOT IN (SELECT TOP {3} {2} FROM {1}))",
                                            prvQry.RowsPerPage, prvQry.Table, col1, prvQry.RowStart);
                    if (!dbh.FillDS(lastQuery, ref dsData))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("PreviewDataRetrieveErr"), dbh.ErrExc);
                        return false;
                    }
                    return true;
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr")); ;
                    return false;
            }
        }

        public bool GetTableStat(out Queue<TableStatistic> qTableStat, DBHelper dbh)
        {   // init
            TableStatistic strTblStat;
            Queue<string> qTables;
            qTableStat = new Queue<TableStatistic>();
            switch (this.dbType)
            {
                case DBType.MySql:
                    if (dbh.ExecuteReader("show table status"))
                    {   // init vars
                        int iVal = -1;

                        // read
                        while (dbh.Read())
                        {
                            strTblStat = new TableStatistic();
                            strTblStat.Name = dbh.GetValue(0).ToString();

                            if(Int32.TryParse(dbh.GetValue(4).ToString(), out iVal))
                                strTblStat.Rows = iVal;
                            else
                                strTblStat.Rows = -1;
                            // rows
                            iVal = 0;
                            // table size
                            if (Int32.TryParse(dbh.GetValue(6).ToString(), out iVal))
                            {
                                int tmpi = 0;
                                if (Int32.TryParse(dbh.GetValue(8).ToString(), out tmpi))
                                    iVal += tmpi;
                                iVal = iVal / 1024;
                            }
                            strTblStat.Size = iVal;
                            qTableStat.Enqueue(strTblStat);
                        }
                        dbh.Close();
                        return true;
                    }
                    else
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TableStatisticsGetErr"), dbh.ErrExc);
                        return false;
                    }
                case DBType.Oracle:
                    // get tables
                    if (!this.GetTables(out qTables, true, dbh))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TableStatisticsGetErr"), dbh.ErrExc);
                        return false;
                    }
                    // exec query
                    foreach (string table in qTables)
                    {
                        // starting getting the rows, size and fields
                        strTblStat = new TableStatistic();
                        strTblStat.Name = table;
                        string qry =
                             String.Format(NyxMain.NyxCI, "select count(*) as cnt from {0} " +
                                            "UNION ALL select bytes/1024 from user_segments where segment_name = '{0}' " +
                                            "UNION ALL select count(column_name) from all_tab_columns where table_name = '{0}'", table);

                        if (dbh.ExecuteReader(qry))
                        {   // read rows
                            try
                            {
                                if (dbh.Read())
                                {
                                    int iVal = 0;
                                    string sVal = dbh.GetValue(0).ToString();
                                    Int32.TryParse(sVal, out iVal);
                                    strTblStat.Rows = iVal;
                                    // read size
                                    if (dbh.Read())
                                    {
                                        sVal = dbh.GetValue(0).ToString();
                                        Int32.TryParse(sVal, out iVal);
                                        strTblStat.Size = iVal;
                                        if (dbh.GetValue(0) is double)
                                            strTblStat.Size = (double)dbh.GetValue(0);
                                        // read fields
                                        if (dbh.Read())
                                        {
                                            sVal = dbh.GetValue(0).ToString();
                                            Int32.TryParse(sVal, out iVal);
                                            strTblStat.FieldCount = iVal;
                                            qTableStat.Enqueue(strTblStat);
                                        }
                                    }
                                }
                            }
                            catch (IndexOutOfRangeException ioexc)
                            {
                                errExc = new DBException(GuiHelper.GetResourceMsg("TableStatisticsGetErr"), ioexc);
                                return false;
                            }
                            finally { dbh.Close(); }
                        }
                        else
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TableStatisticsGetErr"), dbh.ErrExc);
                            return false;
                        }

                    }
                    return true;
                case DBType.MSSql:
                    if (!this.GetTables(out qTables, true, dbh))
                    {
                        errExc = new DBException(GuiHelper.GetResourceMsg("TableStatisticsGetErr"), dbh.ErrExc);
                        return false;
                    }
                    // exec query
                    foreach (string table in qTables)
                    {
                        // starting listview and query
                        strTblStat = new TableStatistic();
                        strTblStat.Name = table;
                        if (dbh.ExecuteReader("EXEC sp_spaceused @objname = '" + table + "'"))
                        {   // read rows
                            int iTmp;
                            if (dbh.Read())
                            {
                                if (Int32.TryParse(dbh.GetValue(4).ToString(), out iTmp))
                                    strTblStat.Rows = iTmp;
                                else
                                    strTblStat.Rows = -1;
                                // read datasize
                                strTblStat.AddInfo = dbh.GetValue(5).ToString() + "/" + dbh.GetValue(2).ToString();
                                strTblStat.MSSize = dbh.GetValue(3).ToString() + "/" + dbh.GetValue(4).ToString();
                                // close
                                dbh.Close();
                                qTableStat.Enqueue(strTblStat);
                            }
                        }
                        else
                        {
                            errExc = new DBException(GuiHelper.GetResourceMsg("TableStatisticsGetErr"), dbh.ErrExc);
                            return false;
                        }
                    }
                    return true;
                default:
                    errExc = new DBException(GuiHelper.GetResourceMsg("DatabaseNotSupportedErr"));
                    return false;
            }
        }
    }

    /// <summary> PreviewObjectClass </summary>
    public class PreviewObject
    {
        private string name;
        private string type;
        /// <summary> name of the object </summary>
        public string Name { get { return name; } set { name = value; } }
        /// <summary> type of the object </summary>
        public string Type { get { return type; } set { type = value; } }
        /// <summary> init previewobject </summary>
        public PreviewObject(string objectName, string objectType)
        {
            name = objectName;
            type = objectType;
        }
    }
    /// <summary> PreviewQuery </summary>
    public class PreviewQuery
    {
        private string table = String.Empty;
        private string columns = "*";
        private string whereStm = String.Empty;
        private int rowStart = -1;
        private int rowsPerPage = -1;

        /// <summary> name of the object </summary>
        public string Table { get { return table; } set { table = value; } }
        /// <summary> columns to get </summary>
        public string Columns { get { return columns; } set { columns = value; } }
        /// <summary> where statement </summary>
        public string WhereStm { get { return whereStm; } set { whereStm = value; } }
        /// <summary> row to start from </summary>
        public int RowStart { get { return rowStart; } set { rowStart = value; } }
        /// <summary> rows to get (display / page) </summary>
        public int RowsPerPage { get { return rowsPerPage; } set { rowsPerPage = value; } }

        /// <summary> init previewobject with select * </summary>
        public PreviewQuery(string selectTable) { table = selectTable; }
        /// <summary> init previewobject with select * and limit </summary>
        public PreviewQuery(string selectTable, int selectLimit)
        {
            table = selectTable;
            rowsPerPage = selectLimit;
        }
        /// <summary> init previewobject with select * and limit from,to </summary>
        public PreviewQuery(string selectTable, int selectStart, int selectRows)
        {
            table = selectTable;
            rowStart = selectStart;
            rowsPerPage = selectRows;
        }
        /// <summary> init previewobject </summary>
        public PreviewQuery(string selectTable, string selectCols, string selectWhereStm, int selectLimit)
        {
            table = selectTable;
            columns = selectCols;
            whereStm = selectWhereStm;
            rowsPerPage = selectLimit;
        }
    }
}
