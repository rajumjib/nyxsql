/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using NyxSettings;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;
using System.Data.Common;
using System.Collections.ObjectModel;

namespace Nyx.Classes
{
    class DBHelperOrcl 
    {   /// <summary> connection properties </summary>
        public string ServerVersion { get { return serverVersion; } }
        private string serverVersion = String.Empty;
        /// <summary> datareader properties </summary>
        private int depth;
        public int Depth { get { return depth; } }
        private int fieldCount;
        public int FieldCount { get { return fieldCount; } }
        private bool hasRows;
        public bool HasRows { get { return hasRows; } }
        private bool isClosed = true;
        public bool IsClosed { get { return isClosed; } }
        private int recordsAffected;
        public int RecordsAffected { get { return recordsAffected; } }

        private DBException errExc;
        /// <summary> exceptions </summary>
        public DBException ErrExc { get { DBException tmpExc = errExc; errExc = null; return tmpExc; } }
        /// <summary> db connection </summary>
        private OracleConnection dbCon;
        /// <summary> datareader </summary>
        private OracleDataReader dtaRdr;
                
        private static string GetConStr(ProfileSettings prf)
        {   // note: encoding is set after connect time for oracle
            // decrypt password
            string pasw = NyxCrypt.DecryptPasw(prf.ProfilePasw);
            // return string
            if (prf.ProfileParam1 == "integrated security")
                return
                    String.Format(NyxMain.NyxCI, "Data Source={0};User Id={1};Password={2};Connection Timeout={3};Integrated Security=yes;{4}",
                                    prf.ProfileTarget, prf.ProfileUser, pasw, 
                                    NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
            else
                return
                    String.Format(NyxMain.NyxCI, "Data Source={0};User Id={1};Password={2};Connection Timeout={3};{4}",
                                    prf.ProfileTarget, prf.ProfileUser, pasw,
                                    NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
        }

        /// <summary>
        /// Connect to database
        /// </summary>
        /// <param name="prf"></param>
        /// <param name="eventStateChanged"></param>
        /// <returns></returns>
        public bool ConnectDB(ProfileSettings prf, bool eventStateChanged)
        {   // init connection
            string conStr = GetConStr(prf);
            dbCon = new OracleConnection(conStr);
            // open connection
            try 
            {   // open connection and subsribe event handler
                dbCon.Open();
                serverVersion = dbCon.ServerVersion;
                if (eventStateChanged)
                    dbCon.StateChange += new System.Data.StateChangeEventHandler(dbCon_StateChange);
            }
            catch (OracleException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Disconnect from database
        /// </summary>
        /// <returns></returns>
        public bool DisconnectDB()
        {
            try
            {
                if (dbCon != null)
                {   // unregister eventhandler
                    dbCon.StateChange -= dbCon_StateChange;
                    // close connection
                    if (dbCon.State == System.Data.ConnectionState.Open)
                        dbCon.Close();
                }
            }
            catch (OracleException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {   // release resources
                if (dbCon != null)
                    dbCon.Dispose();
                dbCon = null;
            }
            return true;
        }
        /// <summary>
        /// Is connection enabled?
        /// </summary>
        /// <returns>true=connected/false=not connected</returns>
        public bool IsConnected()
        {
            if (dbCon != null && dbCon.State != ConnectionState.Closed)
                return true;
            else
                return false;
        }

        private void dbCon_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            OracleConnection sdbCon = (OracleConnection)sender;
            if (sdbCon.State == System.Data.ConnectionState.Closed)
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("ErrConnectionClosed"));
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrConnectionClosed"),
                    AppDialogs.MessageHandler.EnumMsgType.Warning);

                System.Threading.Thread.CurrentThread.Abort();
            }
        }

        /// <summary>
        /// Execute non query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteNonQuery(string qry)
        {   // init
            OracleCommand cmd = new OracleCommand(qry, dbCon);
            // execute 
            try { cmd.ExecuteNonQuery(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // return 
                return false;
            }
            return true;
        }

        public bool Update(string qry, System.Data.DataSet ds)
        {
            OracleDataAdapter dtaAdpt = new OracleDataAdapter();
            OracleCommand cmd = new OracleCommand(qry, dbCon);
            cmd.InitialLONGFetchSize = DBHelper.OrclInitLongFetchSize;
            dtaAdpt.SelectCommand = cmd;
            OracleCommandBuilder cmdBld = new OracleCommandBuilder(dtaAdpt);

            try { dtaAdpt.Update(ds); }
            catch (OracleException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            
            return true;
        }
        /// <summary>
        /// Fill datatable
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool FillDT(string qry, ref System.Data.DataTable dt)
        {   // init
            OracleCommand cmd = new OracleCommand(qry, dbCon);
            cmd.InitialLONGFetchSize = DBHelper.OrclInitLongFetchSize;
            OracleDataAdapter orclDtaAdpt = new OracleDataAdapter(cmd);
            try { orclDtaAdpt.Fill(dt); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill dataset
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool FillDS(string qry, ref System.Data.DataSet ds)
        {   // init
            OracleCommand cmd = new OracleCommand(qry, dbCon);
            cmd.InitialLONGFetchSize = DBHelper.OrclInitLongFetchSize;
            OracleDataAdapter orclDtaAdpt = new OracleDataAdapter(cmd);
            try { orclDtaAdpt.Fill(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary> alternate dataset/datagridview update </summary> 
        /// <param name="table"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool AlternateDGridUpdate(string table, System.Data.DataSet ds)
        {   // validate
            if (table == null)
                throw new ArgumentNullException("table", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (ds == null)
                throw new ArgumentNullException("ds", GuiHelper.GetResourceMsg("ArgumentNull"));
            // check after changed rows and correct numbers of tables
            if (ds != null && ds.Tables.Count == 1 && ds.HasChanges())
            {   // get changed rows
                DataSet dsChanged = ds.GetChanges();
                // init stringbuilder
                System.Text.StringBuilder sbAddStm = new System.Text.StringBuilder();
                System.Text.StringBuilder sbAddStmColumns = new System.Text.StringBuilder();

                System.Text.StringBuilder sbDelStm = new System.Text.StringBuilder();
                System.Text.StringBuilder sbDelStmColumns = new System.Text.StringBuilder();

                System.Text.StringBuilder sbChgStm = new System.Text.StringBuilder();

                // parse changed rows
                foreach (DataRow dr in dsChanged.Tables[0].Rows)
                {
                    switch (dr.RowState)
                    {
                        case DataRowState.Added:
                            // allready build values?
                            if (sbAddStmColumns.Length == 0)
                            {
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    if (sbAddStmColumns.Length == 0)
                                        sbAddStmColumns.Append(String.Format(NyxMain.NyxCI, "'{0}'", dr[dc.ColumnName].ToString()));
                                    else
                                        sbAddStmColumns.Append(String.Format(NyxMain.NyxCI, ",'{0}'", dr[dc.ColumnName].ToString()));
                                }
                            }
                            if (sbAddStm.Length == 0)
                                sbAddStm.Append(String.Format(NyxMain.NyxCI,
                                    "INSERT INTO {0} ({1}) VALUES ({2})", table, sbDelStmColumns.ToString(), sbAddStmColumns.ToString()));
                            else
                                sbAddStm.Append(String.Format(NyxMain.NyxCI,
                                    ";INSERT INTO {0} ({1}) VALUES ({2})", table, sbDelStmColumns.ToString(), sbAddStmColumns.ToString()));
                            // clear columns stringbuilder
                            sbAddStmColumns = new System.Text.StringBuilder();
                            break;
                        case DataRowState.Deleted:
                            // allready build columns?
                            if (sbDelStmColumns.Length == 0)
                            {
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    if (sbDelStmColumns.Length == 0)
                                        sbDelStmColumns.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                            dc.ColumnName.ToString(), dr[dc.ColumnName].ToString()));
                                    else
                                        sbDelStmColumns.Append(String.Format(NyxMain.NyxCI, " AND {0} = '{1}'",
                                            dc.ColumnName.ToString(), dr[dc.ColumnName].ToString()));
                                }
                            }
                            // finally build query statement
                            if (sbDelStm.Length == 0)
                                sbDelStm.Append(String.Format(NyxMain.NyxCI, "DELETE FROM {0} WHERE {1} AND ROWNUM = 1", table, sbDelStmColumns.ToString()));
                            else
                                sbDelStm.Append(String.Format(NyxMain.NyxCI, ";DELETE FROM {0} WHERE {1} AND ROWNUM = 1", table, sbDelStmColumns.ToString()));
                            // clear columns stringbuilder
                            sbDelStmColumns = new System.Text.StringBuilder();
                            break;
                        case DataRowState.Modified:
                            System.Text.StringBuilder sbChgUpdStm = new System.Text.StringBuilder(), sbChgWhereStm = new System.Text.StringBuilder();
                            foreach (DataColumn dc in ds.Tables[0].Columns)
                            {
                                // always adding the where state
                                if (sbChgWhereStm.Length == 0)
                                    sbChgWhereStm.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                                        dc.ColumnName.ToString(), dr[dc.ColumnName, DataRowVersion.Original].ToString()));
                                else
                                    sbChgWhereStm.Append(String.Format(NyxMain.NyxCI, " AND {0} = '{1}'",
                                                        dc.ColumnName.ToString(), dr[dc.ColumnName, DataRowVersion.Original].ToString()));
                                // update column statement
                                if (dr[dc.ColumnName, DataRowVersion.Original].ToString() != dr[dc.ColumnName, DataRowVersion.Current].ToString())
                                {
                                    if (sbChgUpdStm.Length == 0)
                                        sbChgUpdStm.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                                            dc.ColumnName, dr[dc.ColumnName].ToString()));
                                    else
                                        sbChgUpdStm.Append(String.Format(NyxMain.NyxCI, ", {0} = '{1}'",
                                                            dc.ColumnName, dr[dc.ColumnName].ToString()));
                                }
                            }
                            // finally build query statement
                            if (sbChgStm.Length == 0)
                                sbChgStm.Append(String.Format(NyxMain.NyxCI, "UPDATE {0} SET {1} WHERE {2} AND ROWNUM = 1",
                                                table, sbChgUpdStm.ToString(), sbChgWhereStm.ToString()));
                            else
                                sbChgStm.Append(String.Format(NyxMain.NyxCI, ";UPDATE {0} SET {1} WHERE {2} AND ROWNUM = 1",
                                                table, sbChgUpdStm.ToString(), sbChgWhereStm.ToString()));
                            break;
                    }
                }
                // modified rows
                if (sbChgStm.Length > 0)
                {   // finaly build and execute query
                    if (!ExecuteNonQuery(sbChgStm.ToString()))
                        return false;
                }
                // deleted rows
                if (sbDelStm.Length > 0)
                {
                    if (!ExecuteNonQuery(sbDelStm.ToString()))
                        return false;
                }
                // added rows
                if (sbAddStm.Length > 0)
                {
                    if (!ExecuteNonQuery(sbAddStm.ToString()))
                        return false;
                }
            }
            // return
            return true;
        }
        
        /// <summary>
        /// Execute query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteReader(string qry)
        {   // init
            OracleCommand cmd = new OracleCommand(qry, dbCon);
            cmd.InitialLONGFetchSize = DBHelper.OrclInitLongFetchSize;
            // read
            try { dtaRdr = cmd.ExecuteReader(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // check datareader
                if(dtaRdr != null)
                {   // reset datareader
                    DtaRdrClose();
                }
                // return 
                return false;
            }
            finally
            {   // set default properties
                if (dtaRdr != null)
                {
                    this.depth = dtaRdr.Depth;
                    this.fieldCount = dtaRdr.FieldCount;
                    this.hasRows = dtaRdr.HasRows;
                    this.recordsAffected = dtaRdr.RecordsAffected;
                    this.isClosed = dtaRdr.IsClosed;
                }
            }
            return true;
        }
        /// <summary>
        /// Datareader read
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrRead()
        {   // read
            try { return dtaRdr.Read(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Datareader nextresult
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrNextResult()
        {   // read
            try { return dtaRdr.NextResult(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Close current datareader
        /// </summary>
        public void DtaRdrClose()
        {   // check if allready null?
            if(dtaRdr != null)
            {   // check if close
                if (!dtaRdr.IsClosed)
                    dtaRdr.Close();
                dtaRdr = null;
            }
        }

        /// <summary> Database dependant functions </summary>
        public OracleDecimal GetOracleDecimal(int index)
        {   // read
            try { return dtaRdr.GetOracleDecimal(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public OracleString GetOracleString(int index)
        {   // read
            try { return dtaRdr.GetOracleString(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        
        /// <summary> datareader 'type' functions </summary>
        public bool IsDBNull(int index)
        {   // read
            try { return dtaRdr.IsDBNull(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetDataTypeName(int index)
        {   // read
            try { return dtaRdr.GetDataTypeName(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public Type GetFieldType(int index)
        {   // read
            try { return dtaRdr.GetFieldType(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetName(int index)
        {   // read
            try { return dtaRdr.GetName(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        
        /// <summary> datareader retrieve functions </summary>
        public object GetValue(int index)
        {   // read
            try { return dtaRdr.GetValue(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public int GetInt32(int index)
        {   // read
            try { return dtaRdr.GetInt32(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public TimeSpan GetTimeSpan(int index)
        {   // read
            try { return dtaRdr.GetTimeSpan(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public DateTime GetDateTime(int index)
        {   // read
            try { return dtaRdr.GetDateTime(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetString(int index)
        {   // read
            try { return dtaRdr.GetString(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public int GetValues(object[] values)
        {   // read
            try { return dtaRdr.GetValues(values); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public float GetFloat(int index)
        {   // read
            try { return dtaRdr.GetFloat(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public double GetDouble(int index)
        {   // read
            try { return dtaRdr.GetDouble(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public bool GetBoolean(int index)
        {   // read
            try { return dtaRdr.GetBoolean(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OracleException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public byte GetByte(int index)
        {   // read
            try { return dtaRdr.GetByte(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public long GetBytes(int index, long dataIndex, byte[] buffer, int bufferIndex, int length)
        {   // read
            try { return dtaRdr.GetBytes(index, dataIndex, buffer, bufferIndex, length); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public char GetChar(int index)
        {   // read
            try { return dtaRdr.GetChar(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public long GetChars(int index, long fieldOffset, char[] buffer, int bufferoffset, int length)
        {   // read
            try { return dtaRdr.GetChars(index, fieldOffset, buffer, bufferoffset, length); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (OracleException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }

        /// <summary> querys </summary>
        public static string GetQuery(DBHelper.EnumGetQueryType queryType)
        {
            switch (queryType)
            {
                case DBHelper.EnumGetQueryType.Indexes:
                    return "SELECT a.index_name, a.uniqueness, a.status, a.last_analyzed, b.constraint_type " +
                            "FROM all_indexes a, all_constraints b " +
                            "WHERE a.table_name = b.table_name AND a.index_name = b.index_name AND a.table_name = '{0}' " +
                            "UNION SELECT index_name, uniqueness, status, last_analyzed, '' FROM ALL_INDEXES WHERE table_name = '{0}'";
                case DBHelper.EnumGetQueryType.IndexDetails:
                    return "SELECT cols.column_name, tcols.data_type, cols.column_position, cols.descend " +
                            "FROM all_ind_columns cols, all_tab_columns tcols " +
                            "WHERE cols.index_name = '{1}' AND tcols.table_name = '{0}' AND cols.column_name = tcols.column_name " +
                            "ORDER BY cols.column_position ASC";
                case DBHelper.EnumGetQueryType.IndexedColumns:
                    return "SELECT b.column_name, a.constraint_type, c.uniqueness FROM all_constraints a, all_cons_columns b, all_indexes c " +
                            "WHERE a.table_name = b.table_name AND a.table_name = c.table_name " +
                            "AND a.constraint_name = b.constraint_name AND a.constraint_name = c.index_name " +
                            "AND a.table_name = '{0}' AND a.constraint_type = 'P'  " +
                            "UNION SELECT DISTINCT a.column_name, '', c.uniqueness FROM all_ind_columns a, all_tab_columns b, all_indexes c " +
                            "WHERE a.table_name = b.table_name AND a.table_name = c.table_name AND a.column_name = b.column_name " +
                            "AND a.index_name = c.index_name AND b.table_name = '{0}'";
                case DBHelper.EnumGetQueryType.UserProcedures: return "SELECT procedure_name FROM user_procedures ORDER BY procedure_name ASC";
                case DBHelper.EnumGetQueryType.AllProcedures: return "SELECT procedure_name FROM all_procedures ORDER BY procedure_name ASC";
                case DBHelper.EnumGetQueryType.ProcedureDetails: return "SELECT * FROM ALL_PROCEDURES WHERE procedure_name = '{0}'";
                case DBHelper.EnumGetQueryType.UserTables: return "SELECT table_name FROM user_tables WHERE table_name NOT LIKE 'BIN$%' ORDER BY table_name ASC"; 
                case DBHelper.EnumGetQueryType.AllTables: return "SELECT table_name FROM all_tables WHERE table_name NOT LIKE 'BIN$%' ORDER BY table_name ASC";
                case DBHelper.EnumGetQueryType.TableColumns: return "SELECT column_name,data_type,data_length,data_precision FROM all_tab_columns WHERE table_name = '{0}'";
                case DBHelper.EnumGetQueryType.TableCreateStm: return "SELECT dbms_metadata.get_ddl('TABLE','{0}') from dual";
                case DBHelper.EnumGetQueryType.UserTriggers: return "SELECT trigger_name FROM user_triggers ORDER BY trigger_name ASC";
                case DBHelper.EnumGetQueryType.AllTriggers: return "SELECT trigger_name FROM all_triggers ORDER BY trigger_name ASC";
                case DBHelper.EnumGetQueryType.TriggerDetails: return "SELECT * FROM ALL_TRIGGERS WHERE TRIGGER_NAME = '{0}'";
                case DBHelper.EnumGetQueryType.UserViews: return "SELECT view_name FROM user_views ORDER BY view_name ASC";
                case DBHelper.EnumGetQueryType.AllViews: return "SELECT view_name FROM all_views ORDER BY view_name ASC";
                case DBHelper.EnumGetQueryType.ViewDetails: return "SELECT * FROM ALL_VIEWS WHERE VIEW_NAME = '{0}'";
                case DBHelper.EnumGetQueryType.ViewDetailsStm: return "SELECT text FROM all_views WHERE view_name = '{0}'";
                default: return String.Empty;
            }
        }
        /// <summary> reserved words </summary>
        public static ReadOnlyCollection<string> ReservedWords
        {
            get
            {
                return new ReadOnlyCollection<string>(new string[] { "ACCESS","ADD","ALL","ALTER","AND","ANY","AS","ASC","AUDIT","BETWEEN","BY","CHAR","CHECK","CLUSTER","COLUMN","COMMENT","COMPRESS"
                            ,"CONNECT","CREATE","CURRENT","DATE","DECIMAL","DEFAULT","DELETE","DESC","DISTINCT","DROP","ELSE","EXCLUSIVE","EXISTS","FILE","FLOAT"
                            ,"FOR","FROM","GRANT","GROUP","HAVING","IDENTIFIED","IMMEDIATE","IN","INCREMENT","INDEX","INITIAL","INSERT","INTEGER","INTERSECT","INTO"
                            ,"IS","LEVEL","LIKE","LOCK","LONG","MAXEXTENTS","MINUS","MLSLABEL","MODE","MODIFY","NOAUDIT","NOCOMPRESS","NOT","NOWAIT","NULL","NUMBER"
                            ,"OF","OFFLINE","ON","ONLINE","OPTION","OR","ORDER","PCTFREE","PRIOR","PRIVILEGES","PUBLIC","RAW","RENAME","RESOURCE","REVOKE","ROW"
                            ,"ROWID","ROWNUM","ROWS","SELECT","SESSION","SET","SHARE","SIZE","SMALLINT","START","SUCCESSFUL","SYNONYM","SYSDATE","TABLE","THEN"
                            ,"TO","TRIGGER","UID","UNION","UNIQUE","UPDATE","USER","VALIDATE","VALUES","VARCHAR","VARCHAR2","VIEW","WHENEVER","WHERE","WITH" 
                } );
            } 
        }
    }
}
