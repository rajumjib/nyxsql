/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using NyxSettings;
using System.Data.Odbc;
using System.Data.Common;
using System.Collections.Generic;

namespace Nyx.Classes
{
    class DBHelperOdbc
    {
        private string serverVersion = String.Empty;
        /// <summary> connection properties </summary>
        public string ServerVersion { get { return serverVersion; } }
        /// <summary> datareader properties </summary>
        private int depth;
        public int Depth { get { return depth; } }
        private int fieldCount;
        public int FieldCount { get { return fieldCount; } }
        private bool hasRows;
        public bool HasRows { get { return hasRows; } }
        private bool isClosed = true;
        public bool IsClosed { get { return isClosed; } }
        private int recordsAffected;
        public int RecordsAffected { get { return recordsAffected; } }
        private DBException errExc;
        /// <summary> exceptions </summary>
        public DBException ErrExc { get { DBException tmpExc = errExc; errExc = null; return tmpExc; } }
        /// <summary> db connection </summary>
        private OdbcConnection dbCon;
        /// <summary> datareader </summary>
        private OdbcDataReader dtaRdr;
        
        private static string GetConStr(ProfileSettings prf)
        {   // decrypt password
            string pasw = NyxCrypt.DecryptPasw(prf.ProfilePasw);
            // check if we got a port and encoding
            switch (prf.ProfileParam1)
            {
                case "Microsoft Access Driver (*.mdb)":
                    return
                        String.Format(NyxMain.NyxCI, "Driver={Microsoft Access Driver (*.mdb)};Dbq={0};Uid={1};Pwd={2};Connection Timeout={3};{4}",
                                      prf.ProfileTarget, prf.ProfileUser, pasw,
                                      NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
                default:
                case "Client Access ODBC Driver (32-bit)":
                    return
                        String.Format(NyxMain.NyxCI, "Driver={Client Access ODBC Driver (32-bit)};System={0};Uid={1};Pwd={2};Connection Timeout={3};{4}",
                                      prf.ProfileTarget, prf.ProfileUser, pasw,
                                      NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
                case "IBM DB2 ODBC DRIVER":
                    return
                        String.Format(NyxMain.NyxCI, "Driver={IBM DB2 ODBC DRIVER};Database={0};hostname={1};port=1527;protocol=TCPIP;uid={2};pwd={3};Connection Timeout={4};{5}",
                                      prf.ProfileDB, prf.ProfileTarget, prf.ProfileUser, pasw,
                                      NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
            }
        }

        /// <summary>
        /// Connect to database
        /// </summary>
        /// <param name="prf"></param>
        /// <param name="eventStateChanged"></param>
        /// <returns></returns>
        public bool ConnectDB(ProfileSettings prf, bool eventStateChanged)
        {   // init connection
            string conStr = GetConStr(prf);
            dbCon = new OdbcConnection(conStr);
            // open connection
            try
            {   // open connection and subsribe event handler
                dbCon.Open();
                serverVersion = dbCon.ServerVersion;
                if (eventStateChanged)
                    dbCon.StateChange += new System.Data.StateChangeEventHandler(dbCon_StateChange);
            }
            catch (OdbcException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Disconnect from database
        /// </summary>
        /// <returns></returns>
        public bool DisconnectDB()
        {
            try
            {
                if (dbCon != null)
                {   // unregister eventhandler
                    dbCon.StateChange -= dbCon_StateChange;
                    // close connection
                    if (dbCon.State == System.Data.ConnectionState.Open)
                        dbCon.Close();
                }
            }
            catch (OdbcException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {   // release resources
                if (dbCon != null)
                    dbCon.Dispose();
                dbCon = null;
            }
            return true;
        }
        /// <summary>
        /// Is connection enabled?
        /// </summary>
        /// <returns>true=connected/false=not connected</returns>
        public bool IsConnected()
        {
            if (dbCon != null && dbCon.State != ConnectionState.Closed)
                return true;
            else
                return false;
        }

        private void dbCon_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            OdbcConnection sdbCon = (OdbcConnection)sender;
            if (sdbCon.State == System.Data.ConnectionState.Closed)
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("ErrConnectionClosed"));
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrConnectionClosed"),
                    AppDialogs.MessageHandler.EnumMsgType.Warning);

                System.Threading.Thread.CurrentThread.Abort();
            }
        }

        /// <summary>
        /// Execute non query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteNonQuery(string qry)
        {   // init
            OdbcCommand mysqlCmd = new OdbcCommand(qry, dbCon);
            // execute 
            try { mysqlCmd.ExecuteNonQuery(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OdbcException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // return 
                return false;
            }
            return true;
        }

        public bool Update(string qry, System.Data.DataSet ds)
        {
            OdbcDataAdapter DtaAdpt = new OdbcDataAdapter();
            DtaAdpt.SelectCommand = new OdbcCommand(qry, dbCon);
            OdbcCommandBuilder cmdBld = new OdbcCommandBuilder(DtaAdpt);
            // try to update
            try { DtaAdpt.Update(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OdbcException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill datatable
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool FillDT(string qry, ref System.Data.DataTable dt)
        {   // init
            OdbcDataAdapter DtaAdpt = new OdbcDataAdapter(qry, dbCon);
            try { DtaAdpt.Fill(dt); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OdbcException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill dataset
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool FillDS(string qry, ref System.Data.DataSet ds)
        {   // init
            OdbcDataAdapter DtaAdpt = new OdbcDataAdapter(qry, dbCon);
            try { DtaAdpt.Fill(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OdbcException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Execute query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteReader(string qry)
        {   // init
            OdbcCommand mysqlCmd = new OdbcCommand(qry, dbCon);
            // read
            try { dtaRdr = mysqlCmd.ExecuteReader(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OdbcException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // check datareader
                if (dtaRdr != null)
                {   // reset datareader
                    DtaRdrClose();
                }
                // return 
                return false;
            }
            finally
            {   // set default properties
                if (dtaRdr != null)
                {
                    this.depth = dtaRdr.Depth;
                    this.fieldCount = dtaRdr.FieldCount;
                    this.hasRows = dtaRdr.HasRows;
                    this.recordsAffected = dtaRdr.RecordsAffected;
                    this.isClosed = dtaRdr.IsClosed;
                }
            }
            return true;
        }
        /// <summary>
        /// Datareader read
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrRead()
        {   // read
            try { return dtaRdr.Read(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OdbcException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Datareader nextresult
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrNextResult()
        {   // read
            try { return dtaRdr.NextResult(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (OdbcException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Close current datareader
        /// </summary>
        public void DtaRdrClose()
        {   // check if allready null?
            if (dtaRdr != null)
            {   // check if close
                if (!dtaRdr.IsClosed)
                    dtaRdr.Close();
                dtaRdr = null;
            }
        }
    }
}