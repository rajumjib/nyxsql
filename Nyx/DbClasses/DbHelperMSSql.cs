/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Data;
using NyxSettings;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections.ObjectModel;

namespace Nyx.Classes
{
    class DBHelperMSSql
    {
        private string serverVersion = String.Empty;
        /// <summary> connection properties </summary>
        public string ServerVersion { get { return serverVersion; } }
        /// <summary> datareader properties </summary>
        private int depth;
        public int Depth { get { return depth; } }
        private int fieldCount;
        public int FieldCount { get { return fieldCount; } }
        private bool hasRows;
        public bool HasRows { get { return hasRows; } }
        private bool isClosed = true;
        public bool IsClosed { get { return isClosed; } }
        private int recordsAffected;
        public int RecordsAffected { get { return recordsAffected; } }
        
        private DBException errExc;
        /// <summary> exceptions </summary>
        public DBException ErrExc { get { DBException tmpExc = errExc; errExc = null; return tmpExc; } }
        /// <summary> db connection </summary>
        private SqlConnection dbCon;
        /// <summary> datareader </summary>
        private SqlDataReader dtaRdr;
                
        private static string GetConStr(ProfileSettings prf)
        {   // check if we got a port and encoding
            if (prf.ProfileParam1 == "Windows Auth (NTLM)")
                return
                    String.Format(NyxMain.NyxCI, "Data Source={0};Initial Catalog={1};Integrated Security=SSPI;Connection Timeout={2};{3}",
                                    prf.ProfileTarget, prf.ProfileDB,
                                    NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
            else
            {   // decrypt password
                string pasw = NyxCrypt.DecryptPasw(prf.ProfilePasw);
                return String.Format(NyxMain.NyxCI, "Data Source={0};Initial Catalog={1};User Id={2};Password={3};Connection Timeout={4};{5}",
                                    prf.ProfileTarget, prf.ProfileDB, prf.ProfileUser, pasw,
                                    NyxMain.UASett.ConnectionTimeout, prf.ProfileAddString);
            }
        }

        /// <summary>
        /// Connect to database
        /// </summary>
        /// <param name="prf"></param>
        /// <param name="eventStateChanged"></param>
        /// <returns></returns>
        public bool ConnectDB(ProfileSettings prf, bool eventStateChanged)
        {   // init connection
            string conStr = GetConStr(prf);
            dbCon = new SqlConnection(conStr);
            // open connection
            try
            {   // open connection and subsribe event handler
                dbCon.Open();
                serverVersion = dbCon.ServerVersion;
                if (eventStateChanged)
                    dbCon.StateChange += new System.Data.StateChangeEventHandler(dbCon_StateChange);
            }
            catch (SqlException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Disconnect from database
        /// </summary>
        /// <returns></returns>
        public bool DisconnectDB()
        {
            try
            {
                if (dbCon != null)
                {   // unregister eventhandler
                    dbCon.StateChange -= dbCon_StateChange;
                    // close connection
                    if (dbCon.State == System.Data.ConnectionState.Open)
                        dbCon.Close();
                }
            }
            catch (SqlException exc)
            {   // connection open? if yes, close
                if (dbCon != null && dbCon.State == System.Data.ConnectionState.Open)
                    dbCon.Close();

                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {   // release resources
                if (dbCon != null)
                    dbCon.Dispose();
                dbCon = null;
            }
            return true;
        }
        /// <summary>
        /// Is connection enabled?
        /// </summary>
        /// <returns>true=connected/false=not connected</returns>
        public bool IsConnected()
        {
            if (dbCon != null && dbCon.State != ConnectionState.Closed)
                return true;
            else
                return false;
        }

        private void dbCon_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {
            SqlConnection sdbCon = (SqlConnection)sender;
            if (sdbCon.State == System.Data.ConnectionState.Closed)
            {
                errExc = new DBException(GuiHelper.GetResourceMsg("ErrConnectionClosed"));
                AppDialogs.MessageHandler.ShowDialog(GuiHelper.GetResourceMsg("ErrConnectionClosed"),
                    AppDialogs.MessageHandler.EnumMsgType.Warning);

                System.Threading.Thread.CurrentThread.Abort();
            }
        }

        /// <summary>
        /// Execute non query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteNonQuery(string qry)
        {   // init
            SqlCommand mysqlCmd = new SqlCommand(qry, dbCon);
            // execute 
            try { mysqlCmd.ExecuteNonQuery(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // return 
                return false;
            }
            return true;
        }

        public bool Update(string qry, System.Data.DataSet ds)
        {
            SqlDataAdapter DtaAdpt = new SqlDataAdapter();
            DtaAdpt.SelectCommand = new SqlCommand(qry, dbCon);
            SqlCommandBuilder cmdBld = new SqlCommandBuilder(DtaAdpt);
            // try to update
            try { DtaAdpt.Update(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill datatable
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public bool FillDT(string qry, ref System.Data.DataTable dt)
        {   // init
            SqlDataAdapter DtaAdpt = new SqlDataAdapter(qry, dbCon);
            try { DtaAdpt.Fill(dt); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary>
        /// Fill dataset
        /// </summary>
        /// <param name="qry"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool FillDS(string qry, ref System.Data.DataSet ds)
        {   // init
            SqlDataAdapter DtaAdpt = new SqlDataAdapter(qry, dbCon);
            try { DtaAdpt.Fill(ds); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            return true;
        }
        /// <summary> alternate dataset/datagridview update </summary> 
        /// <param name="table"></param>
        /// <param name="ds"></param>
        /// <returns></returns>
        public bool AlternateDGridUpdate(string table, System.Data.DataSet ds)
        {   // validate
            if (table == null)
                throw new ArgumentNullException("table", GuiHelper.GetResourceMsg("ArgumentNull"));
            if (ds == null)
                throw new ArgumentNullException("ds", GuiHelper.GetResourceMsg("ArgumentNull"));
            // check after changed rows and correct numbers of tables
            if (ds != null && ds.Tables.Count == 1 && ds.HasChanges())
            {   // get changed rows
                DataSet dsChanged = ds.GetChanges();
                // init stringbuilder
                System.Text.StringBuilder sbAddStm = new System.Text.StringBuilder();
                System.Text.StringBuilder sbAddStmColumns = new System.Text.StringBuilder();

                System.Text.StringBuilder sbDelStm = new System.Text.StringBuilder();
                System.Text.StringBuilder sbDelStmColumns = new System.Text.StringBuilder();

                System.Text.StringBuilder sbChgStm = new System.Text.StringBuilder();

                // parse changed rows
                foreach (DataRow dr in dsChanged.Tables[0].Rows)
                {
                    switch (dr.RowState)
                    {
                        case DataRowState.Added:
                            // allready build values?
                            if (sbAddStmColumns.Length == 0)
                            {
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    if (sbAddStmColumns.Length == 0)
                                        sbAddStmColumns.Append(String.Format(NyxMain.NyxCI, "'{0}'", dr[dc.ColumnName].ToString()));
                                    else
                                        sbAddStmColumns.Append(String.Format(NyxMain.NyxCI, ",'{0}'", dr[dc.ColumnName].ToString()));
                                }
                            }
                            if (sbAddStm.Length == 0)
                                sbAddStm.Append(String.Format(NyxMain.NyxCI,
                                    "INSERT INTO {0} ({1}) VALUES ({2})", table, sbDelStmColumns.ToString(), sbAddStmColumns.ToString()));
                            else
                                sbAddStm.Append(String.Format(NyxMain.NyxCI,
                                    ";INSERT INTO {0} ({1}) VALUES ({2})", table, sbDelStmColumns.ToString(), sbAddStmColumns.ToString()));
                            // clear columns stringbuilder
                            sbAddStmColumns = new System.Text.StringBuilder();
                            break;
                        case DataRowState.Deleted:
                            // allready build columns?
                            if (sbDelStmColumns.Length == 0)
                            {
                                foreach (DataColumn dc in ds.Tables[0].Columns)
                                {
                                    if (sbDelStmColumns.Length == 0)
                                        sbDelStmColumns.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                            dc.ColumnName.ToString(), dr[dc.ColumnName].ToString()));
                                    else
                                        sbDelStmColumns.Append(String.Format(NyxMain.NyxCI, " AND {0} = '{1}'",
                                            dc.ColumnName.ToString(), dr[dc.ColumnName].ToString()));
                                }
                            }
                            // finally build query statement
                            if (sbDelStm.Length == 0)
                                sbDelStm.Append(String.Format(NyxMain.NyxCI, "DELETE FROM {0} WHERE {1} LIMIT 1", table, sbDelStmColumns.ToString()));
                            else
                                sbDelStm.Append(String.Format(NyxMain.NyxCI, ";DELETE FROM {0} WHERE {1} LIMIT 1", table, sbDelStmColumns.ToString()));
                            // clear columns stringbuilder
                            sbDelStmColumns = new System.Text.StringBuilder();
                            break;
                        case DataRowState.Modified:
                            System.Text.StringBuilder sbChgUpdStm = new System.Text.StringBuilder(), sbChgWhereStm = new System.Text.StringBuilder();
                            foreach (DataColumn dc in ds.Tables[0].Columns)
                            {
                                // always adding the where state
                                if (sbChgWhereStm.Length == 0)
                                    sbChgWhereStm.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                                        dc.ColumnName.ToString(), dr[dc.ColumnName, DataRowVersion.Original].ToString()));
                                else
                                    sbChgWhereStm.Append(String.Format(NyxMain.NyxCI, " AND {0} = '{1}'",
                                                        dc.ColumnName.ToString(), dr[dc.ColumnName, DataRowVersion.Original].ToString()));
                                // update column statement
                                if (dr[dc.ColumnName, DataRowVersion.Original].ToString() != dr[dc.ColumnName, DataRowVersion.Current].ToString())
                                {
                                    if (sbChgUpdStm.Length == 0)
                                        sbChgUpdStm.Append(String.Format(NyxMain.NyxCI, "{0} = '{1}'",
                                                            dc.ColumnName, dr[dc.ColumnName].ToString()));
                                    else
                                        sbChgUpdStm.Append(String.Format(NyxMain.NyxCI, ", {0} = '{1}'",
                                                            dc.ColumnName, dr[dc.ColumnName].ToString()));
                                }
                            }
                            // finally build query statement
                            if (sbChgStm.Length == 0)
                                sbChgStm.Append(String.Format(NyxMain.NyxCI, "UPDATE {0} SET {1} WHERE {2} LIMIT 1",
                                                table, sbChgUpdStm.ToString(), sbChgWhereStm.ToString()));
                            else
                                sbChgStm.Append(String.Format(NyxMain.NyxCI, ";UPDATE {0} SET {1} WHERE {2} LIMIT 1",
                                                table, sbChgUpdStm.ToString(), sbChgWhereStm.ToString()));
                            break;
                    }
                }
                // modified rows
                if (sbChgStm.Length > 0)
                {   // finaly build and execute query
                    if (!ExecuteNonQuery(sbChgStm.ToString()))
                        return false;
                }
                // deleted rows
                if (sbDelStm.Length > 0)
                {
                    if (!ExecuteNonQuery(sbDelStm.ToString()))
                        return false;
                }
                // added rows
                if (sbAddStm.Length > 0)
                {
                    if (!ExecuteNonQuery(sbAddStm.ToString()))
                        return false;
                }
            }
            // return
            return true;
        }

        /// <summary>
        /// Execute query datareader
        /// </summary>
        /// <param name="qry"></param>
        /// <returns></returns>
        public bool ExecuteReader(string qry)
        {   // init
            SqlCommand mysqlCmd = new SqlCommand(qry, dbCon);
            // read
            try { dtaRdr = mysqlCmd.ExecuteReader(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // check datareader
                if (dtaRdr != null)
                {   // reset datareader
                    DtaRdrClose();
                }
                // return 
                return false;
            }
            finally
            {   // set default properties
                if (dtaRdr != null)
                {
                    this.depth = dtaRdr.Depth;
                    this.fieldCount = dtaRdr.FieldCount;
                    this.hasRows = dtaRdr.HasRows;
                    this.recordsAffected = dtaRdr.RecordsAffected;
                    this.isClosed = dtaRdr.IsClosed;
                }
            }
            return true;
        }
        /// <summary>
        /// Datareader read
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrRead()
        {   // read
            try { return dtaRdr.Read(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Datareader nextresult
        /// </summary>
        /// <returns></returns>
        public bool DtaRdrNextResult()
        {   // read
            try { return dtaRdr.NextResult(); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // set exception
                errExc = new DBException(exc.Message.ToString(), exc);
                // close datareader
                DtaRdrClose();
                // return 
                return false;
            }
        }
        /// <summary>
        /// Close current datareader
        /// </summary>
        public void DtaRdrClose()
        {   // check if allready null?
            if (dtaRdr != null)
            {   // check if close
                if (!dtaRdr.IsClosed)
                    dtaRdr.Close();
                dtaRdr = null;
            }
        }

        /// <summary> datareader 'type' functions </summary>
        public bool IsDBNull(int index)
        {   // read
            try { return dtaRdr.IsDBNull(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetDataTypeName(int index)
        {   // read
            try { return dtaRdr.GetDataTypeName(index); }
            catch (InvalidOperationException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public Type GetFieldType(int index)
        {   // read
            try { return dtaRdr.GetFieldType(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetName(int index)
        {   // read
            try { return dtaRdr.GetName(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }

        /// <summary> datareader retrieve functions </summary>
        public object GetValue(int index)
        {   // read
            try { return dtaRdr.GetValue(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public int GetInt32(int index)
        {   // read
            try { return dtaRdr.GetInt32(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public DateTime GetDateTime(int index)
        {   // read
            try { return dtaRdr.GetDateTime(index); }
            catch (InvalidOperationException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public string GetString(int index)
        {   // read
            try { return dtaRdr.GetString(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public int GetValues(object[] values)
        {   // read
            try { return dtaRdr.GetValues(values); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public float GetFloat(int index)
        {   // read
            try { return dtaRdr.GetFloat(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public double GetDouble(int index)
        {   // read
            try { return dtaRdr.GetDouble(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public bool GetBoolean(int index)
        {   // read
            try { return dtaRdr.GetBoolean(index); }
            catch (InvalidOperationException exc)
            {   // set exception && return
                errExc = new DBException(exc.Message.ToString(), exc);
                return false;
            }
            catch (SqlException exc)
            {   // close datareader
                DtaRdrClose();
                // return 
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public byte GetByte(int index)
        {   // read
            try { return dtaRdr.GetByte(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public long GetBytes(int index, long dataIndex, byte[] buffer, int bufferIndex, int length)
        {   // read
            try { return dtaRdr.GetBytes(index, dataIndex, buffer, bufferIndex, length); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public char GetChar(int index)
        {   // read
            try { return dtaRdr.GetChar(index); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }
        public long GetChars(int index, long fieldOffset, char[] buffer, int bufferoffset, int length)
        {   // read
            try { return dtaRdr.GetChars(index, fieldOffset, buffer, bufferoffset, length); }
            catch (InvalidOperationException exc)
            {
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
            catch (SqlException exc)
            {   
                DtaRdrClose();
                throw new DBException(exc.Message.ToString(), exc);
            }
        }

        /// <summary> querys </summary>
        public static string GetQuery(DBHelper.EnumGetQueryType queryType)
        {
            switch (queryType)
            {
                case DBHelper.EnumGetQueryType.Databases: return "EXEC sp_databases";
                case DBHelper.EnumGetQueryType.Indexes: return "EXEC sp_MShelpindex '{0}'";
                case DBHelper.EnumGetQueryType.IndexDetails: return "EXEC sp_MShelpindex '{0}'";
                case DBHelper.EnumGetQueryType.IndexedColumns: return "EXEC sp_MShelpindex '{0}'";
                case DBHelper.EnumGetQueryType.UserProcedures: return "SELECT specific_name FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_TYPE = 'PROCEDURE' ORDER BY specific_name ASC";
                case DBHelper.EnumGetQueryType.ProcedureDetails: return "SELECT * FROM INFORMATION_SCHEMA.ROUTINES WHERE SPECIFIC_NAME = '{0}'";
                case DBHelper.EnumGetQueryType.UserTables: return "SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' ORDER BY table_name ASC";
                case DBHelper.EnumGetQueryType.TableColumns: return "EXEC sp_columns {0}";
                case DBHelper.EnumGetQueryType.UserTriggers: return "SELECT name FROM sysobjects WHERE xtype = 'TR' ORDER BY name ASC";
                case DBHelper.EnumGetQueryType.TriggerDetails: return "SELECT * FROM sysobjects WHERE name = '{0}'";
                case DBHelper.EnumGetQueryType.UserViews: return "SELECT table_name FROM INFORMATION_SCHEMA.VIEWS ORDER BY table_name ASC";
                case DBHelper.EnumGetQueryType.ViewDetails: return "SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = '{0}'";
                default: return String.Empty;
            }
        }
        /// <summary> reserved words </summary>
        public static ReadOnlyCollection<string> ReservedWords
        {
            get
            {
                return new ReadOnlyCollection<string>(new string[] { "ADD","EXECUTE","PRECISION","ALL","EXISTS","PRIMARY","ALTER","EXIT","PRINT","AND","EXTERNAL","PROC","ANY","FETCH","PROCEDURE","AS","FILE","PUBLIC","ASC","FILLFACTOR","RAISERROR","AUTHORIZATION","FOR","READ","BACKUP","FOREIGN"
                                    ,"READTEXT","BEGIN","FREETEXT","RECONFIGURE","BETWEEN","FREETEXTTABLE","REFERENCES","BREAK","FROM","REPLICATION","BROWSE","FULL","RESTORE","BULK","FUNCTION","RESTRICT","BY","GOTO","RETURN","CASCADE","GRANT","REVERT","CASE"
                                    ,"GROUP","REVOKE","CHECK","HAVING","RIGHT","CHECKPOINT","HOLDLOCK","ROLLBACK","CLOSE","IDENTITY","ROWCOUNT","CLUSTERED","IDENTITY_INSERT","ROWGUIDCOL","COALESCE","IDENTITYCOL","RULE","COLLATE","IF","SAVE","COLUMN","IN","SCHEMA","COMMIT"
                                    ,"INDEX","SELECT","COMPUTE","INNER","SESSION_USER","CONSTRAINT","INSERT","SET","CONTAINS","INTERSECT","SETUSER","CONTAINSTABLE","INTO","SHUTDOWN","CONTINUE","IS","SOME","CONVERT","JOIN","STATISTICS","CREATE","KEY","SYSTEM_USER"
                                    ,"CROSS","KILL","TABLE","CURRENT","LEFT","TABLESAMPLE","CURRENT_DATE","LIKE","TEXTSIZE","CURRENT_TIME","LINENO","THEN","CURRENT_TIMESTAMP","LOAD","TO","CURRENT_USER","NATIONAL ","TOP","CURSOR","NOCHECK","TRAN","DATABASE","NONCLUSTERED"
                                    ,"TRANSACTION","DBCC","NOT","TRIGGER","DEALLOCATE","NULL","TRUNCATE","DECLARE","NULLIF","TSEQUAL","DEFAULT","OF","UNION","DELETE","OFF","UNIQUE","DENY","OFFSETS","UNPIVOT","DESC","ON","UPDATE","DISK","OPEN","UPDATETEXT","DISTINCT"
                                    ,"OPENDATASOURCE","USE","DISTRIBUTED","OPENQUERY","USER","DOUBLE","OPENROWSET","VALUES","DROP","OPENXML","VARYING","DUMMY","OPTION","VIEW","DUMP","OR","WAITFOR","ELSE","ORDER","WHEN","END","OUTER","WHERE","ERRLVL","OVER","WHILE"
                                    ,"ESCAPE","PERCENT","WITH","EXCEPT","PIVOT","WRITETEXT","EXEC","PLAN" } );
            }
        }
    }
}