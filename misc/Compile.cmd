@echo off
rem # check microsoft.net
if not exist %windir%\Microsoft.NET\Framework\v2.0.50727 goto ERR-netfrmwrk
if exist %windir%\Microsoft.NET\Framework\v2.0.50727 set netfrmwrk=%windir%\Microsoft.NET\Framework\v2.0.50727

rem # check nyx
if not exist ..\nyxsql.exe goto ERR-nyxsql

rem # start ngen
echo ######################################################
echo # Starting ngen for precompiling nyx. Please wait... #
echo ######################################################
%netfrmwrk%\ngen.exe install ..\NyxSql.exe /ExeConfig:..\NyxSql.exe
%netfrmwrk%\ngen.exe install ..\NyxUpdate.exe /ExeConfig:..\NyxUpdate.exe
%netfrmwrk%\ngen.exe install ifr.exe /ExeConfig:ifr.exe

echo ######################################################
echo # ngen finished.                                     #
echo ######################################################
%netfrmwrk%\ngen.exe display ..\NyxSql.exe

goto EOF
:ERR-netfrmwrk
echo ##################################################
echo # Error, Microsoft .Net Framework 2 not found!   #
echo ##################################################
echo # .Net Framework is required for running NyxSql! #
echo #            Please check/get it from            #
echo #           www.microsoft.com/downloads          #
echo ##################################################
goto EOF

:ERR-nyxsql
echo ###########################################################
echo # Error, can't find NyxSql.exe! Not starting ngen.cmd...  #
echo ###########################################################
goto EOF

:EOF
pause