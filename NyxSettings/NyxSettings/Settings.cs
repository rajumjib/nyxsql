/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.IO;
using System.Windows.Forms;
using System.Configuration;
using System.Collections;
using System.Xml.Serialization;

[assembly: CLSCompliant(true)]
namespace NyxSettings
{
    /// <summary>
    /// Save (Serialize) or Load (DeSerialize) any xml file which is used from within Nyx
    /// </summary>
    public sealed class Settings
    {   // private 
        private static System.Globalization.CultureInfo nyxCI = System.Globalization.CultureInfo.InvariantCulture;
        private static SettingsException errExc;
        private static SettingsLocation userSettingsLoc = SettingsLocation.UsersApplicationFolder;
        private static SettingsLocation profileLoc = SettingsLocation.ApplicationFolder;
        private static SettingsLocation historyLoc = SettingsLocation.UsersApplicationFolder;
        private static SettingsLocation bookmarkLoc = SettingsLocation.UsersApplicationFolder;
        private static SettingsLocation hotkeyLoc = SettingsLocation.ApplicationFolder;
        private static SettingsLocation runtimeLoc = SettingsLocation.UsersApplicationFolder;

        /// <remarks> error message for any error which occurs </remarks>
        public static SettingsException ErrExc { get { return errExc; } }
        /// <remarks> cultureinfo </remarks>
        public static System.Globalization.CultureInfo NyxCI { get { return nyxCI; } }
        /// <remarks> history filename </remarks>
        public const string FileNameHistory = "nyx-history.xml";
        /// <remarks> profiles filename </remarks>
        public const string FileNameProfile = "nyx-profiles.xml";
        /// <remarks> user application settings filename </remarks>
        public const string FileNameUserSettings = "nyx-usersettings.xml";
        /// <remarks> runtime settings filename </remarks>
        public const string FileNameRuntime = "nyx-userruntime.xml";
        /// <remarks> bookmarks filename </remarks>
        public const string FileNameBookmark = "nyx-bookmarks.xml";
        /// <remarks> hotkeys filename </remarks>
        public const string FileNameHotkeys = "nyx-hotkeys.xml";
        /// <remarks> main settings filename </remarks>
        public const string FileNameMain = "nyx-main.xml";

        /// <remarks> prevent compiler from generating default constructor </remarks>
        private Settings() { }
        
        private static bool CheckFolder(string folder)
        {   // validate
            if (folder == null)
                throw new ArgumentNullException("folder", GetResourceMsg("ArgumentNull"));
            if (folder.Length > 0)
            {
                // check if we got the folder
                if (!Directory.Exists(folder))
                {
                    try { Directory.CreateDirectory(folder); }
                    catch (IOException exc)
                    {
                        errExc = new SettingsException(exc.Message.ToString(), exc);
                        return false;
                    }
                    catch (UnauthorizedAccessException exc)
                    {
                        errExc = new SettingsException(exc.Message.ToString(), exc); 
                        return false;
                    }
                }
            }
            else
            {
                errExc = new SettingsException(GetResourceMsg("PathInvalid"));
                return false;
            }
            return true;
        }
        /// <summary>
        /// Returns the folderpath for the selected setting. <paramref name="sType"/> is
        /// used to identify the type of settings like History, Profile, AppSettings etc.
        /// </summary>
        /// <param name="saveType">setting</param>
        /// <returns>folderpath</returns>
        public static string GetFolder(SettingsType saveType)
        {   
            switch (saveType)
            {
                case SettingsType.History:
                    if (historyLoc == SettingsLocation.UsersApplicationFolder)
                        return System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                        + "\\Nyx\\";
                    break;
                case SettingsType.Profile:
                    if (profileLoc == SettingsLocation.UsersApplicationFolder)
                        return System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                        + "\\Nyx\\";
                    break;
                case SettingsType.AppSettings:
                    if (userSettingsLoc == SettingsLocation.UsersApplicationFolder)
                        return System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                        + "\\Nyx\\";
                    break;
                case SettingsType.Runtime:
                    if (runtimeLoc == SettingsLocation.UsersApplicationFolder)
                        return System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                        + "\\Nyx\\";
                    break;
                case SettingsType.Bookmarks:
                    if (bookmarkLoc == SettingsLocation.UsersApplicationFolder)
                        return System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                        + "\\Nyx\\";
                    break;
                case SettingsType.Hotkeys:
                    if (hotkeyLoc == SettingsLocation.UsersApplicationFolder)
                        return System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData)
                        + "\\Nyx\\";
                    break;
            }
            // when we reach here it's:
            return Application.StartupPath + "\\";
        }

        /// <summary> DeSerialize main settings </summary>
        /// <param name="mainSett">main settings</param>
        /// <param name="folder">folder to load from</param>
        /// <returns></returns>
        public static bool DeserializeMain(ref MainSettings mainSett, string folder)
        {   // validate
            if (mainSett == null)
                mainSett = new MainSettings();
            // check folder
            if (!Settings.CheckFolder(folder))
                return false;
            // check file
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameMain);
            if (!File.Exists(fullpath))
            {
                SerializeMain(mainSett, Settings.GetFolder(SettingsType.Main));
                return true;
            }
            // init serializer, streamreader
            StreamReader sr = null;
            XmlSerializer xs = new XmlSerializer(typeof(MainSettings));
            try
            {
                sr = File.OpenText(fullpath);
                // readin and close reader
                mainSett = (MainSettings)xs.Deserialize(sr);
                // set intenal usersettingslocation
                Settings.userSettingsLoc = mainSett.UserSettingsLoc;
                // return
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        /// <summary> DeSerialize usersettings </summary>
        /// <param name="usrSett">usersettings class</param>
        /// <param name="folder">folder to load from</param>
        /// <returns></returns>
        public static bool DeserializeUserSettings(ref UserAppSettings usrSett, string folder)
        {   // validate
            if (usrSett == null)
                usrSett = new UserAppSettings();
            // check folder
            if (!Settings.CheckFolder(folder))
                return false;
            // check file
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameUserSettings);
            if (!File.Exists(fullpath))
            {
                SerializeUserSettings(usrSett, Settings.GetFolder(SettingsType.AppSettings));
                return true;
            }
            // init serializer, streamreader
            StreamReader sr = null;
            XmlSerializer xs = new XmlSerializer(typeof(UserAppSettings));
            try
            {
                sr = File.OpenText(fullpath);
                // readin and close reader
                usrSett = (UserAppSettings)xs.Deserialize(sr);
                // setting internal locations
                Settings.profileLoc = usrSett.SaveScopeProfiles;
                Settings.historyLoc = usrSett.SaveScopeHistory;
                Settings.bookmarkLoc = usrSett.SaveScopeBookmarks;
                Settings.hotkeyLoc = usrSett.SaveScopeHotKeys;
                Settings.runtimeLoc = usrSett.SaveScopeRuntime;
                // return
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        /// <summary> DeSerialize runtime settings </summary>
        /// <param name="runtSett">class runtimesettings</param>
        /// <param name="folder">folder to load from</param>
        /// <returns></returns>
        public static bool DeserializeRuntime(ref RuntimeSettings runtSett, string folder)
        {   // validate
            if (runtSett == null)
                runtSett = new RuntimeSettings();
            // check folder
            if (!Settings.CheckFolder(folder))
                return false;
            // check file
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameRuntime);
            if (!File.Exists(fullpath))
            {   // file doesn't exist, we write the default configuration and return
                SerializeRuntime(runtSett, Settings.GetFolder(SettingsType.Runtime));
                return true;
            }
            // init serializer, streamreader
            StreamReader sr = null;
            XmlSerializer xs = new XmlSerializer(typeof(RuntimeSettings));
            try
            {
                sr = File.OpenText(fullpath);
                // readin and close reader
                runtSett = (RuntimeSettings)xs.Deserialize(sr);
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc); 
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        /// <summary> DeSerialize bookmarks datatable </summary>
        /// <param name="dt">datatable containing bookmarks</param>
        /// <param name="folder">folder to load from</param>
        /// <returns></returns>
        public static bool DeserializeBookmarks(ref System.Data.DataTable dt, string folder)
        {   // validate
            if (dt == null)
            {
                dt = new System.Data.DataTable("Bookmarks");
                dt.Locale = nyxCI;
            }
            // preset datatable
            dt.Columns.Add("name");
            dt.Columns.Add("iscat");
            dt.Columns.Add("cat");
            dt.Columns.Add("desc");
            dt.Columns.Add("query");
            // check directory
            if (!Settings.CheckFolder(folder))
                return false;
            // checkfile, if it doesn't exist return
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameBookmark);
            if (!File.Exists(fullpath))
                return true;
            // init stream
            Stream str = null;
            // open && deserialize
            try
            {
                str = File.Open(fullpath, FileMode.Open);
                // Daten aus der Datei deserialisieren
                dt.ReadXml(str);
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {   // close file
                if (str != null)
                    str.Close();
            }
        }
        /// <summary> DeSerialize hotkeyhashtable </summary>
        /// <param name="hotkeyDef">hashtable containing hotkeys</param>
        /// <param name="folder">folder to load from</param>
        /// <returns></returns>
        public static bool DeserializeHotkeys(ref HotKeys hotkeyDef, string folder)
        {   // validate
            if (hotkeyDef == null)
                hotkeyDef = new HotKeys();
            // check if we got the folder
            if (!Settings.CheckFolder(folder))
                return false;
            // further init
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameHotkeys);
            // checkfile, if it doesn't exist return
            if (!File.Exists(fullpath))
                return true;
            // init serializer, streamreader
            StreamReader sr = null;
            XmlSerializer xs = new XmlSerializer(typeof(HotKeys));
            try
            {
                sr = File.OpenText(fullpath);
                // readin and close reader
                hotkeyDef = (HotKeys)xs.Deserialize(sr);
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
        /// <summary> DeSerialize profilesettings </summary>
        /// <param name="htProfile">Hashtable containing profiles</param>
        /// <param name="folder">folder to load from</param>
        /// <returns></returns>
        public static bool DeserializeProfiles(ref Hashtable htProfile, string folder)
        {   // validate
            if (htProfile == null)
                htProfile = new Hashtable();
            // check if we got the folder
            if (!Settings.CheckFolder(folder))
                return false;
            // init
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameProfile);
            // checkfile, if it doesn't exist return
            if (!File.Exists(fullpath))
                return true;
            // further init
            StreamReader sr = null;
            XmlSerializer xs = new XmlSerializer(typeof(ProfileSettings[]));

            try
            {   // streamreader
                sr = File.OpenText(fullpath);
                // readin and close reader
                ProfileSettings[] prfSett = (ProfileSettings[])xs.Deserialize(sr);
                // now add the profiles to the hashtable
                foreach (ProfileSettings profile in prfSett)
                    htProfile.Add(profile.ProfileName, profile);
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }

        /// <summary> Serialize mainsettings </summary>
        /// <param name="mainSett">mainsettings to serialize</param>
        /// <param name="folder">folder to save to</param>
        /// <returns></returns>
        public static bool SerializeMain(MainSettings mainSett, string folder)
        {   // validate
            if (mainSett == null)
                throw new ArgumentNullException("mainSett", GetResourceMsg("ArgumentNull"));
            if(folder == null)
                throw new ArgumentNullException("folder", GetResourceMsg("ArgumentNull"));
            // init vars
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameMain);
            // set intenal usersettingslocation
            Settings.userSettingsLoc = mainSett.UserSettingsLoc;
            // init serializer, streamwriter
            StreamWriter sw = null;
            XmlSerializer xs = new XmlSerializer(mainSett.GetType());
            // do...
            try
            {
                sw = File.CreateText(fullpath);
                // now we serialize
                xs.Serialize(sw, mainSett);
                // flush
                sw.Flush();
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }
        /// <summary> Serialize usersettings </summary>
        /// <param name="usrSett">usersettings to serialize</param>
        /// <param name="folder">folder to save to</param>
        /// <returns></returns>
        public static bool SerializeUserSettings(UserAppSettings usrSett, string folder)
        {   // validate
            if (usrSett == null)
                throw new ArgumentNullException("usrSett", GetResourceMsg("ArgumentNull"));
            if(folder == null)
                throw new ArgumentNullException("folder", GetResourceMsg("ArgumentNull"));
            // init vars
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameUserSettings);
            // setting internal locations
            Settings.profileLoc = usrSett.SaveScopeProfiles;
            Settings.historyLoc = usrSett.SaveScopeHistory;
            Settings.bookmarkLoc = usrSett.SaveScopeBookmarks;
            Settings.hotkeyLoc = usrSett.SaveScopeHotKeys;
            Settings.runtimeLoc = usrSett.SaveScopeRuntime;
            // init serializer, streamwriter
            StreamWriter sw = null;
            XmlSerializer xs = new XmlSerializer(usrSett.GetType());
            // do...
            try
            {
                sw = File.CreateText(fullpath);
                // now we serialize
                xs.Serialize(sw, usrSett);
                // flush
                sw.Flush();
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }
        /// <summary> Serialize runtimesettings </summary>
        /// <param name="runtSett">class runtimesettings</param>
        /// <param name="folder">folder to save to</param>
        /// <returns></returns>
        public static bool SerializeRuntime(RuntimeSettings runtSett, string folder)
        {   // validate
            if (runtSett == null)
                throw new ArgumentNullException("runtSett", GetResourceMsg("ArgumentNull"));
            if (folder == null)
                throw new ArgumentNullException("folder", GetResourceMsg("ArgumentNull"));
            // init vars
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameRuntime);
            // init serializer, streamwriter
            StreamWriter sw = null;
            XmlSerializer xs = new XmlSerializer(runtSett.GetType());
            try
            {
                sw = File.CreateText(fullpath);
                // now we serialize
                xs.Serialize(sw, runtSett);
                // flush
                sw.Flush();
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }
        /// <summary> Serialize bookmarks through datatable </summary>
        /// <param name="dt">bookmark datatable</param>
        /// <param name="folder">folder to save to</param>
        /// <returns></returns>
        public static bool SerializeBookmarks(System.Data.DataTable dt, string folder)
        {   // validate
            if (dt == null)
                throw new ArgumentNullException("dt", GetResourceMsg("ArgumentNull"));
            if (folder == null)
                throw new ArgumentNullException("folder", GetResourceMsg("ArgumentNull"));
            // init file, arraylist
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameBookmark);
            // init stream
            Stream str = null;
            // write
            try
            {
                str = File.Open(fullpath, FileMode.Create);
                // Serialisieren des Arrays
                dt.WriteXml(str);
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {   // close file
                if (str != null)
                    str.Close();
               
            }
        }
        /// <summary> Serialize hotkeys </summary>
        /// <param name="hkDef">hashtable containing hotkeydefinition</param>
        /// <param name="folder">file to save to</param>
        /// <returns></returns>
        public static bool SerializeHotkeys(HotKeys hkDef, string folder)
        {   // validate
            if (hkDef == null)
                throw new ArgumentNullException("hkDef", GetResourceMsg("ArgumentNull"));
            if (folder == null)
                throw new ArgumentNullException("folder", GetResourceMsg("ArgumentNull"));
            // init vars
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameHotkeys);
            // init serializer, streamwriter
            StreamWriter sw = null;
            XmlSerializer xs = new XmlSerializer(hkDef.GetType());
            try
            {
                sw = File.CreateText(fullpath);
                // now we serialize
                xs.Serialize(sw, hkDef);
                // flush
                sw.Flush();
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }
        /// <summary> Serialize profiles </summary>
        /// <param name="htProfiles">hashtable containing profiles</param>
        /// <param name="folder">folder to save to</param>
        /// <returns></returns>
        public static bool SerializeProfiles(Hashtable htProfiles, string folder)
        {   // validate
            if (htProfiles == null)
                throw new ArgumentNullException("htProfiles", GetResourceMsg("ArgumentNull"));
            if(folder == null)
                throw new ArgumentNullException("folder", GetResourceMsg("ArgumentNull"));
            // init vars
            string fullpath = String.Format(nyxCI, "{0}{1}", folder, FileNameProfile);
            // convert hashtable to array
            ProfileSettings[] prfS = new ProfileSettings[htProfiles.Count];
            htProfiles.Values.CopyTo(prfS, 0);
            // init serializer, streamwriter
            StreamWriter sw = null;
            XmlSerializer xs = new XmlSerializer(prfS.GetType());
            // do...
            try
            {   // streamwriter
                sw = File.CreateText(fullpath);
                // now we serialize
                xs.Serialize(sw, prfS);
                // flush
                sw.Flush();
                return true;
            }
            catch (UnauthorizedAccessException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (IOException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            catch (InvalidOperationException exc)
            {
                errExc = new SettingsException(exc.Message.ToString(), exc);
                return false;
            }
            finally
            {
                if (sw != null)
                    sw.Close();
            }
        }


        /// <summary> get resource message </summary>
        /// <param name="variable"></param>
        /// <returns></returns>
        public static string GetResourceMsg(string variable)
        {   // validate
            if (variable == null)
                throw new ArgumentNullException("variable", GetResourceMsg("ArgumentNull"));

            System.Resources.ResourceManager resmng =
                new System.Resources.ResourceManager("NyxSettings.NyxSettings", System.Reflection.Assembly.GetExecutingAssembly());
            if (resmng != null)
                return resmng.GetString(variable, nyxCI).Replace("\\lf", "\n");
            else
                return String.Empty;
        }
    }
    /// <summary> custom exception for database errors </summary>
    [Serializable]
    public class SettingsException : Exception
    {
        /// <summary> default constructor </summary>
        public SettingsException() { }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected SettingsException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context)
            : base(info, context)
        {
            // not implemented
        }
        /// <summary>
        /// create exception with message
        /// </summary>
        /// <param name="message"></param>
        public SettingsException(string message) : base (message)
        {   // helplink
            base.HelpLink = "https://nyx.sigterm.eu/cgi-bin/trac.cgi/newticket";
            // stacktrace
            System.Diagnostics.StackTrace str = new System.Diagnostics.StackTrace(true);
            base.Source = str.ToString();
        }
        /// <summary>
        /// create exception with message and innerexception
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public SettingsException(string message, Exception innerException)
            : base(message, innerException)
        {   // helplink
            base.HelpLink = "https://nyx.sigterm.eu/cgi-bin/trac.cgi/newticket";
            // stacktrace
            System.Diagnostics.StackTrace str = new System.Diagnostics.StackTrace(true);
            base.Source = str.ToString();
        }
    }
}
