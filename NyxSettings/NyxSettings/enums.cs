/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections.Generic;
using System.Text;

namespace NyxSettings
{
    /// <summary> type of settings </summary>
    public enum SettingsType
    {
        /// <remarks> main settings </remarks> 
        Main,
        /// <remarks> query history </remarks>
        History,
        /// <remarks> database connect profiles </remarks>
        Profile,
        /// <remarks> application settings </remarks>
        AppSettings,
        /// <remarks> runtime settings (f.x. splitter distance) </remarks>
        Runtime,
        /// <remarks> bookmarks </remarks>
        Bookmarks,
        /// <remarks> hotkeys </remarks>
        Hotkeys
    }
    /// <summary> location of settings, either </summary>
    public enum SettingsLocation
    {
        /// <remarks> user application data folder </remarks>
        UsersApplicationFolder,
        /// <remarks> nyx application folder </remarks>
        ApplicationFolder
    }
    /// <summary> hotkey actions which are availabled </summary>
    public enum HotKeyAction
    {
        /// <remarks> connect database </remarks>
        Connect,
        /// <remarks> disconnect database </remarks>
        Disconnect,
        /// <remarks> execute sql </remarks>
        ExecuteSql,
        /// <remarks> execute only selected sql </remarks>
        ExecuteSelectedSql,
        /// <remarks> open sql file and load into the query editor box </remarks>
        OpenSql,
        /// <remarks> save sql from the query editor to file </remarks>
        SaveSql,
        /// <remarks> clear query editor text </remarks>
        ClearSql,
        /// <remarks> edit datagrid </remarks>
        EditSql,
        /// <remarks> update datagrid data into database </remarks>
        UpdateSql,
        /// <remarks> switch for showing/hiding the sqlbuilder </remarks>
        ShowSqlBuilder,
        /// <remarks> show autocompletition listbox </remarks>
        ShowAutoCompletition,
        /// <remarks> new sql-query tab window </remarks>
        NewSqlTab,
        /// <remarks> close current sql-query tab window </remarks>
        CloseSqlTab,
        /// <remarks> switch to sql-query tabcontrol </remarks>
        ViewSql,
        /// <remarks> switch to preview </remarks>
        ViewPreview,
        /// <remarks> switch to tablemanager </remarks>
        ViewTables,
        /// <remarks> switch to tables </remarks>
        ViewViews,
        /// <remarks> switch to procedures </remarks>
        ViewPrcdrs,
        /// <remarks> switch to triggers </remarks>
        ViewTriggers,
        /// <remarks> load profilemanager </remarks>
        ProfileDialog,
        /// <remarks> load settingsmanager </remarks>
        SettingsDialog,
        /// <remarks> load bookmarkmanager </remarks>
        BookmarkDialog,
        /// <remarks> check for update </remarks>
        CheckForUpdate,
        /// <remarks> quick connect database </remarks>
        QuickConnect,
        /// <remarks> send nyxsql to systray </remarks>
        MinimizeToSystray,
        /// <remarks> exit nyxsql </remarks>
        Exit
    }
    /// <summary> languages </summary>
    public enum Language {
        /// <remarks> try to use the language of the operating system </remarks>
        Auto,
        /// <remarks> english/us </remarks>
        enUS,
        /// <remarks> german/german </remarks>
        deDE }
    /// <summary> database types </summary>
    public enum DBType {
        /// <remarks> oracle database </remarks>
        Oracle,
        /// <remarks> mysql database </remarks>
        MySql,
        /// <remarks> odbc database </remarks>
        Odbc,
        /// <remarks> microsoft sql server database </remarks>
        MSSql,
        /// <remarks> postgresql database </remarks>
        PgSql,
        /// <remarks> no database set </remarks>
        NotSet 
    }
    /// <summary> default timespan enum </summary>
    public enum DefaultTimeSpan {
        /// <remarks> represents every day </remarks>
        EveryDay,
        /// <remarks> represents every week </remarks>
        EveryWeek,
        /// <remarks> represents every 2 weeks </remarks>
        Every2Weeks,
        /// <remarks> represents every month </remarks>
        EveryMonth
    }
}
