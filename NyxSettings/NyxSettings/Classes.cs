/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using System.Collections.Generic;

namespace NyxSettings
{
    /// <summary>
    /// Holds the location of the UserSettings, which further holds the location of each other config file
    /// </summary>
    public class MainSettings
    {
        private SettingsLocation userSettingsLoc = SettingsLocation.UsersApplicationFolder;
        /// <remarks> user application settings location </remarks>
        public SettingsLocation UserSettingsLoc { get { return userSettingsLoc; } set { userSettingsLoc = value; } }
    }
    /// <summary>
    /// The Bookmarkentry, either category or entry
    /// </summary>
    public class BookmarkEntry
    {   // private
        private string name = String.Empty;
        private string qry = String.Empty;
        private string category = String.Empty;
        private string description = String.Empty;
        private bool isCategory;

        /// <remarks> bookmark name </remarks>
        public string Name { get { return name; } set { name = value; } }
        /// <remarks> bookmarked query </remarks>
        public string Qry { get { return qry; } set { qry = value; } }
        /// <remarks> bookmark category </remarks>
        public string Category { get { return category; } set { category = value; } }
        /// <remarks> either the description, if bookmark, or the parrent node, if category </remarks>
        public string Description { get { return description; } set { description = value; } }
        /// <remarks> is category (true) or normal entry (false) </remarks>
        public bool IsCategory { get { return isCategory; } set { isCategory = value; } }
    }
    /// <summary>
    /// RuntimeSettings
    /// </summary>
    public class RuntimeSettings
    {   // private
        private Rectangle windowPosition = new Rectangle(10, 10, 1024, 768);
        private FormWindowState windowState = FormWindowState.Normal;
        private bool tbVisibilityMain = true;
        private Point tbLocationMain = new Point(3, 0);
        private int distanceSqlSplitter = 150;
        private int distanceSqlBldrSplitter = 500;
        private int distanceTablesSplitter = 190;
        private int distancePreviewSplitter = 190;
        private int distancePreviewDataSplitter = 360;
        private int distanceIndexesSplitter = 450;
        private int distanceIndexDetailsSplitter = 150;
        private int distanceViewsSplitter = 500;
        private int distancePrcdrsSplitter = 500;
        private int distanceTriggersSplitter = 500;
        private int distanceViews2Splitter = 150;
        private int distanceTriggers2Splitter = 150;
        private int distancePrcdrs2Splitter = 150;
        private string lastSelectedPath = String.Empty;
        private DateTime lastUpdatecheck;
        // public 
        /// <remarks> WindowPosition of Nyx </remarks>
        public Rectangle WindowPosition { get { return windowPosition; } set { windowPosition = value; } }
        /// <remarks> WindowState of Nyx </remarks>
        public FormWindowState WindowState { get { return windowState; } set { windowState = value; } }
        /// <remarks> Visibility of the main Toolbar </remarks>
        public bool TBVisibilityMain { get { return tbVisibilityMain; } set { tbVisibilityMain = value; } }
        /// <remarks> Location of the main Toolbar </remarks>
        public Point TBLocationMain { get { return tbLocationMain; } set { tbLocationMain = value; } }
        /// <remarks> Distance of the Sql-Editor/Result-Splitter </remarks>
        public int DistanceSqlSplitter { get { return distanceSqlSplitter; } set { distanceSqlSplitter = value; } }
        /// <remarks> Distance of the Sql-Editor/Builder-Splitter </remarks>
        public int DistanceSqlBldrSplitter { get { return distanceSqlBldrSplitter; } set { distanceSqlBldrSplitter = value; } }
        /// <remarks> Distance of the Tables-Splitter </remarks>
        public int DistanceTablesSplitter { get { return distanceTablesSplitter; } set { distanceTablesSplitter = value; } }
        /// <remarks> Distance of the Preview-Splitter </remarks>
        public int DistancePreviewSplitter { get { return distancePreviewSplitter; } set { distancePreviewSplitter = value; } }
        /// <remarks> Distance of the Preview-Data-Splitter (Dataarea/filter) </remarks>
        public int DistancePreviewDataSplitter { get { return distancePreviewDataSplitter; } set { distancePreviewDataSplitter = value; } }
        /// <remarks> Distance of the Tables/Indexes-Splitter </remarks>
        public int DistanceIndexesSplitter { get { return distanceIndexesSplitter; } set { distanceIndexesSplitter = value; } }
        /// <remarks> Distance of the Indexes/Indexdetails-Splitter </remarks>
        public int DistanceIndexDetailsSplitter { get { return distanceIndexDetailsSplitter; } set { distanceIndexDetailsSplitter = value; } }
        /// <remarks> Distance of the Triggers-Splitter </remarks>
        public int DistanceTriggersSplitter { get { return distanceTriggersSplitter; } set { distanceTriggersSplitter = value; } }
        /// <remarks> Distance of the Views-Splitter </remarks>
        public int DistanceViewsSplitter { get { return distanceViewsSplitter; } set { distanceViewsSplitter = value; } }
        /// <remarks> Distance of the Procedures-Splitter </remarks>
        public int DistancePrcdrsSplitter { get { return distancePrcdrsSplitter; } set { distancePrcdrsSplitter = value; } }
        /// <remarks> Distance of the second Views-Splitter </remarks>
        public int DistanceViews2Splitter { get { return distanceViews2Splitter; } set { distanceViews2Splitter = value; } }
        /// <remarks> Distance of the second Procedures-Splitter </remarks>
        public int DistancePrcdrs2Splitter { get { return distancePrcdrs2Splitter; } set { distancePrcdrs2Splitter = value; } }
        /// <remarks> Distance of the second Triggers-Splitter </remarks>
        public int DistanceTriggers2Splitter { get { return distanceTriggers2Splitter; } set { distanceTriggers2Splitter = value; } }
        /// <remarks> Last selected load/save path </remarks>
        public string LastSelectedPath { get { return lastSelectedPath; } set { lastSelectedPath = value; } }
        /// <remarks> Last successfull update check </remarks>
        public DateTime LastUpdatecheck { get { return lastUpdatecheck; } set { lastUpdatecheck = value; } }
    }
    /// <summary>
    /// UserSettings, which are set through UserSettingsManager 
    /// </summary>
    public class UserAppSettings
    {   // private
        // misc stuff
        private Language lang = Language.enUS;
        private string defaultProfile = String.Empty;
        private int historyDepth = 50;
        private bool historySaveLimit;
        private int sqlQryLimit = 100;
        private bool systrayIcon;
        private bool fadeWindows;
        private int fadeDelay = 75;

        private bool autoRetrieveDbs = true;
        private bool autoCompletition = true;
        private bool autoCompletUseWords = true;
        private bool autoCompletUseTables = true;
        private bool autoCompletUseViews = true;
        private bool autoCompletUseProcedures = true;
        private bool sqlBldrRetTables = true;
        private bool sqlBldrRetViews;
        private bool sqlBldrRetProcedures;
        // fonts
        private string sqlEditorFont = "Verdana;8;Regular";
        private string historyFont = "Verdana;8;Regular";
        private string logFont = "Verdana;8;Regular";
        // datagrid
        private string dataGridFont = "Verdana;8;Regular";
        private int dataGridForeColor = Color.Black.ToArgb();
        private int dataGridBackColor = Color.White.ToArgb();
        private int dataGridAltBackColor = Color.GhostWhite.ToArgb();
        private int dataGridAltForeColor = Color.Black.ToArgb();
        private int dataGridSelForeColor = Color.AliceBlue.ToArgb();
        private int dataGridSelBackColor = Color.Gray.ToArgb();
        // auto-update
        private bool autoCheckUpdate = true;
        private DefaultTimeSpan autoCheckDelay = DefaultTimeSpan.EveryWeek;
        // save scope, use user history?
        private SettingsLocation sSUserHistory = SettingsLocation.UsersApplicationFolder;
        private SettingsLocation sSProfiles = SettingsLocation.UsersApplicationFolder;
        private SettingsLocation sSRuntime = SettingsLocation.UsersApplicationFolder;
        private SettingsLocation sSBookmarks = SettingsLocation.UsersApplicationFolder;
        private SettingsLocation sSHotKeys = SettingsLocation.ApplicationFolder;
        // oracle, advanced
        private int initLongFetchSize = 200;
        // database general, advanced
        private int connectionTimeout = 15;
        // show windows again?
        private bool showWdExit = true;
        private bool showWdExecQry = true;
        private bool showWdDBContentChanged = true;
        private bool showWdSettingsRestart = true;
        private bool showWdSettingsSave = true;
        private bool showWdAltDGridUpdate = true;

        /// <remarks> selected language </remarks>
        public Language Language { get { return lang; } set { lang = value; } }
        /// <remarks> default profile on load </remarks>
        public string DefaultProfile { get { return defaultProfile; } set { defaultProfile = value; } }
        /// <remarks> how much history entrys to save </remarks>
        public int HistorySaveQueries { get { return historyDepth; } set { historyDepth = value; } }
        /// <remarks> save the limit of select querys inside history </remarks>
        public bool HistorySaveQueryLimit { get { return historySaveLimit; } set { historySaveLimit = value; } }
        /// <remarks> default query limit </remarks>
        public int SqlDefaultQryLimit { get { return sqlQryLimit; } set { sqlQryLimit = value; } }
        /// <remarks> display/use system tray icone </remarks>
        public bool ShowSystrayIcon { get { return systrayIcon; } set { systrayIcon = value; } }
        /// <remarks> fade windows </remarks>
        public bool FadeWindows { get { return fadeWindows; } set { fadeWindows = value; } }
        /// <remarks> delay for fading windows </remarks>
        public int FadeWindowDelay { get { return fadeDelay; } set { fadeDelay = value; } }
        /// <remarks> automatically retrieve databases at connect, if dbms supported </remarks>
        public bool AutoPrefetchDbs { get { return autoRetrieveDbs; } set { autoRetrieveDbs = value; } }
        /// <remarks> use autocompletition </remarks>
        public bool AutoCompletition { get { return autoCompletition; } set { autoCompletition = value; } }
        /// <remarks> use reserved words for autocompletition </remarks>
        public bool AutoCompletitionUseWords { get { return autoCompletUseWords; } set { autoCompletUseWords = value; } }
        /// <remarks> use tables for autocompletition </remarks>
        public bool AutoCompletitionUseTables { get { return autoCompletUseTables; } set { autoCompletUseTables = value; } }
        /// <remarks> use views for autocompletition </remarks>
        public bool AutoCompletitionUseViews { get { return autoCompletUseViews; } set { autoCompletUseViews = value; } }
        /// <remarks> use procedures for autocompletition </remarks>
        public bool AutoCompletitionUseProcedures { get { return autoCompletUseProcedures; } set { autoCompletUseProcedures = value; } }
        /// <remarks> retrieve tables at connect time </remarks>
        public bool SqlBuilderPrefetchTables { get { return sqlBldrRetTables; } set { sqlBldrRetTables = value; } }
        /// <remarks> retrieve views at connect time </remarks>
        public bool SqlBuilderPrefetchViews { get { return sqlBldrRetViews; } set { sqlBldrRetViews = value; } }
        /// <remarks> retrieve procedures at connect time </remarks>
        public bool SqlBuilderPrefetchProcedures { get { return sqlBldrRetProcedures; } set { sqlBldrRetProcedures = value; } }
        /// <remarks> sql query editor font </remarks>
        public string SqlEditorFont { get { return sqlEditorFont; } set { sqlEditorFont = value; } }
        /// <remarks> history font </remarks>
        public string HistoryFont { get { return historyFont; } set { historyFont = value; } }
        /// <remarks> log window font </remarks>
        public string LogFont { get { return logFont; } set { logFont = value; } }
        /// <remarks> datagrid font </remarks>
        public string DataGridFont { get { return dataGridFont; } set { dataGridFont = value; } }
        /// <remarks> datagrid fore color </remarks>
        public int DataGridForeColor { get { return dataGridForeColor; } set { dataGridForeColor = value; } }
        /// <remarks> datagrid back color </remarks>
        public int DataGridBackColor { get { return dataGridBackColor; } set { dataGridBackColor = value; } }
        /// <remarks> datagrid alternating fore color </remarks>
        public int DataGridAltForeColor { get { return dataGridAltForeColor; } set { dataGridAltForeColor = value; } }
        /// <remarks> datagrid alternating back color </remarks>
        public int DataGridAltBackColor { get { return dataGridAltBackColor; } set { dataGridAltBackColor = value; } }
        /// <remarks> datagrid selected fore color </remarks>
        public int DataGridSelForeColor { get { return dataGridSelForeColor; } set { dataGridSelForeColor = value; } }
        /// <remarks> datagrid selected back color </remarks>
        public int DataGridSelBackColor { get { return dataGridSelBackColor; } set { dataGridSelBackColor = value; } }
        /// <remarks> automatically check for update </remarks>
        public bool AutoCheckUpdate { get { return autoCheckUpdate; } set { autoCheckUpdate = value; } }
        /// <remarks> automatic check delay </remarks>
        public DefaultTimeSpan AutoCheckDelay { get { return autoCheckDelay; } set { autoCheckDelay = value; } }
        /// <remarks> SaveScope history </remarks>
        public SettingsLocation SaveScopeHistory { get { return sSUserHistory; } set { sSUserHistory = value; } }
        /// <remarks> SaveScope profiles </remarks>
        public SettingsLocation SaveScopeProfiles { get { return sSProfiles; } set { sSProfiles = value; } }
        /// <remarks> SaveScope runtime </remarks>
        public SettingsLocation SaveScopeRuntime { get { return sSRuntime; } set { sSRuntime = value; } }
        /// <remarks> SaveScope bookmarks </remarks>
        public SettingsLocation SaveScopeBookmarks { get { return sSBookmarks; } set { sSBookmarks = value; } }
        /// <remarks> SaveScope hotkeys </remarks>
        public SettingsLocation SaveScopeHotKeys { get { return sSHotKeys; } set { sSHotKeys = value; } }
        /// <remarks> oracle only setting for the initlongfetchsize (retrieving data from long fields) </remarks>
        public int InitLongFetchSize { get { return initLongFetchSize; } set { initLongFetchSize = value; } }
        /// <remarks> connection timeout for database connections </remarks>
        public int ConnectionTimeout { get { return connectionTimeout; } set { connectionTimeout = value; } }
        /// <remarks> show exit window </remarks>
        public bool WindowExit { get { return showWdExit; } set { showWdExit = value; } }
        /// <remarks> show 'execute non select query' window </remarks>
        public bool WindowExecuteQuery { get { return showWdExecQry; } set { showWdExecQry = value; } }
        /// <remarks> show dbcontent change, refresh - question dialog? </remarks>
        public bool WindowDBContentChanged { get { return showWdDBContentChanged; } set { showWdDBContentChanged = value; } }
        /// <remarks> show 'use alternate datagrid update' - question dialog? </remarks>
        public bool WindowAlternateDataUpdate { get { return showWdAltDGridUpdate; } set { showWdAltDGridUpdate = value; } }
        /// <remarks> show restart window </remarks>
        public bool WindowRestartNyx { get { return showWdSettingsRestart; } set { showWdSettingsRestart = value; } }
        /// <remarks> show settings save window </remarks>
        public bool WindowSaveSettings { get { return showWdSettingsSave; } set { showWdSettingsSave = value; } }
    }
    /// <summary>
    /// profile settings class
    /// </summary>
    public class ProfileSettings
    {   // private 
        private string name = String.Empty;
        private string description = String.Empty;
        private string user = String.Empty;
        private string pasw = String.Empty;
        private DBType dbType = DBType.NotSet;
        private string target = String.Empty;
        private string db = String.Empty;
        private string param1 = String.Empty;           // auth mssql
        private string encoding = String.Empty;         // encoding/charset for connecting
        private string addString = String.Empty;
        private int initLongFetchSize = -1;    // initlongfetchsize for oracle
        // public
        /// <remarks> name </remarks>
        public string ProfileName { get { return name; } set { name = value; } }
        /// <remarks> description </remarks>
        public string ProfileDescription { get { return description; } set { description = value; } }
        /// <remarks> user </remarks>
        public string ProfileUser { get { return user; } set { user = value; } }
        /// <remarks> password </remarks>
        public string ProfilePasw { get { return pasw; } set { pasw = value; } }
        /// <remarks> database type </remarks>
        public DBType ProfileDBType { get { return dbType; } set { dbType = value; } }
        /// <remarks> host/sid to connect to </remarks>
        public string ProfileTarget { get { return target; } set { target = value; } }
        /// <remarks> database name </remarks>
        public string ProfileDB { get { return db; } set { db = value; } }
        /// <remarks> additional parameter 1 </remarks>
        public string ProfileParam1 { get { return param1; } set { param1 = value; } }
        /// <remarks> encoding </remarks>
        public string Encoding { get { return encoding; } set { encoding = value; } }
        /// <remarks> additional string, user defined </remarks>
        public string ProfileAddString { get { return addString; } set { addString = value; } }
        /// <remarks> initLongFetchSize, Profilesetting overriding global setting if > 0 </remarks>
        public int InitLongFetchSize { get { return initLongFetchSize; } set { initLongFetchSize = value; } }
    }
    /// <summary>
    /// Hotkey definition class
    /// </summary>
    public class HotKeys
    {
        private Shortcut connect = Shortcut.F2;
        private Shortcut disconnect = Shortcut.F3;
        private Shortcut executeSql = Shortcut.CtrlE;
        private Shortcut executeSelectedSql = Shortcut.CtrlR;
        private Shortcut openSql = Shortcut.CtrlO;
        private Shortcut saveSql = Shortcut.CtrlS;
        private Shortcut clearSql = Shortcut.CtrlD;
        private Shortcut showSqlBuilder = Shortcut.CtrlP;
        private Shortcut showAutoCompletition = Shortcut.CtrlJ;
        private Shortcut newSqlTab = Shortcut.CtrlT;
        private Shortcut closeSqlTab = Shortcut.CtrlW;
        private Shortcut viewSql = Shortcut.ShiftF1;
        private Shortcut viewPreview = Shortcut.ShiftF2;
        private Shortcut viewTables = Shortcut.ShiftF3;
        private Shortcut viewViews = Shortcut.ShiftF4;
        private Shortcut viewPrcdrs = Shortcut.ShiftF5;
        private Shortcut viewTriggers = Shortcut.ShiftF6;
        private Shortcut profileDialog = Shortcut.F10;
        private Shortcut settingsDialog = Shortcut.F11;
        private Shortcut bookmarkDialog = Shortcut.F9;
        private Shortcut checkForUpdate = Shortcut.F12;
        private Shortcut quickConnect = Shortcut.ShiftF8;
        private Shortcut minimizeToSystray = Shortcut.ShiftF7;
        private Shortcut exit = Shortcut.CtrlQ;

        /// <summary> connect shortcut </summary>
        [CategoryAttribute("Database connection"), DefaultValueAttribute(typeof(Shortcut), "F2"),
        Description("Connect to selected profile."), DisplayName("Connect database")]
        public Shortcut Connect { get { return connect; } set { if(Checkup(value)) connect = value; } }

        /// <summary> disconnect shortcut </summary>
        [CategoryAttribute("Database connection"), DefaultValueAttribute(typeof(Shortcut), "F3"),
        Description("Disconnect current connection."), DisplayName("Disconnect database")]
        public Shortcut Disconnect { get { return disconnect; } set { if (Checkup(value)) disconnect = value; } }

        /// <summary> execute sql shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlE"),
        Description("Execute current sql."), DisplayName("Execute sql")]
        public Shortcut ExecuteSql { get { return executeSql; } set { if (Checkup(value)) executeSql = value; } }

        /// <summary> execute selected sql shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlR"),
        Description("Execute selected sql."), DisplayName("Execute selected sql")]
        public Shortcut ExecuteSelectedSql { get { return executeSelectedSql; } set { if (Checkup(value)) executeSelectedSql = value; } }

        /// <summary> open sql file shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlO"),
        Description("Open sql file and load into sql editor."), DisplayName("Open sql file")]
        public Shortcut OpenSql { get { return openSql; } set { if (Checkup(value)) openSql = value; } }

        /// <summary> save sql to file shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlS"),
        Description("Save sql from sql editor to file."), DisplayName("Save sql to file")]
        public Shortcut SaveSql { get { return saveSql; } set { if (Checkup(value)) saveSql = value; } }

        /// <summary> clear sql query tab shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlD"),
        Description("Clear sql editor tab."), DisplayName("Clear sql tab")]
        public Shortcut ClearSql { get { return clearSql; } set { if (Checkup(value)) clearSql = value; } }

        /// <summary> show sqlbuilder shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlP"),
        Description("Show sql builder."), DisplayName("Show sql builder")]
        public Shortcut ShowSqlBuilder { get { return showSqlBuilder; } set { if (Checkup(value)) showSqlBuilder = value; } }

        /// <summary> show autocompletition listbox shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlJ"),
        Description("Show auto-completition listbox."), DisplayName("Show auto-completition")]
        public Shortcut ShowAutoCompletition { get { return showAutoCompletition; } set { if (Checkup(value)) showAutoCompletition = value; } }

        /// <summary> new sqltab shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlT"),
        Description("Create new sql tab."), DisplayName("Create new tab")]
        public Shortcut NewSqlTab { get { return newSqlTab; } set { if (Checkup(value)) newSqlTab = value; } }

        /// <summary> close sqltab shortcut </summary>
        [CategoryAttribute("Sql editor"), DefaultValueAttribute(typeof(Shortcut), "CtrlW"),
        Description("Close current sql tab."), DisplayName("Close current tab")]
        public Shortcut CloseSqlTab { get { return closeSqlTab; } set { if (Checkup(value)) closeSqlTab = value; } }

        /// <summary> sql query tab shortcut </summary>
        [CategoryAttribute("View"), DefaultValueAttribute(typeof(Shortcut), "ShiftF1"),
        Description("Show sql tab."), DisplayName("Show sql tab")]
        public Shortcut ViewSql { get { return viewSql; } set { if (Checkup(value)) viewSql = value; } }

        /// <summary> preview tab shortcut </summary>
        [CategoryAttribute("View"), DefaultValueAttribute(typeof(Shortcut), "ShiftF2"),
        Description("Show preview tab."), DisplayName("Show preview tab")]
        public Shortcut ViewPreview { get { return viewPreview; } set { if (Checkup(value)) viewPreview = value; } }

        /// <summary> tables tab shortcut </summary>
        [CategoryAttribute("View"), DefaultValueAttribute(typeof(Shortcut), "ShiftF3"),
        Description("Show tables tab."), DisplayName("Show tables tab")]
        public Shortcut ViewTables { get { return viewTables; } set { if (Checkup(value)) viewTables = value; } }

        /// <summary> views tab shortcut </summary>
        [CategoryAttribute("View"), DefaultValueAttribute(typeof(Shortcut), "ShiftF4"),
        Description("Show views tab."), DisplayName("Show views tab")]
        public Shortcut ViewViews { get { return viewViews; } set { if (Checkup(value)) viewViews = value; } }

        /// <summary> procedures tab shortcut </summary>
        [CategoryAttribute("View"), DefaultValueAttribute(typeof(Shortcut), "ShiftF5"),
        Description("Show procedures tab."), DisplayName("Show procedures tab")]
        public Shortcut ViewPrcdrs { get { return viewPrcdrs; } set { if (Checkup(value)) viewPrcdrs = value; } }

        /// <summary> triggers tab shortcut </summary>
        [CategoryAttribute("View"), DefaultValueAttribute(typeof(Shortcut), "ShiftF6"),
        Description("Show triggers tab."), DisplayName("Show triggers tab")]
        public Shortcut ViewTriggers { get { return viewTriggers; } set { if (Checkup(value)) viewTriggers = value; } }

        /// <summary> profiles shortcut </summary>
        [CategoryAttribute("Dialogs"), DefaultValueAttribute(typeof(Shortcut), "F10"),
        Description("Show profiles dialog."), DisplayName("Profiles")]
        public Shortcut ProfileDialog { get { return profileDialog; } set { if (Checkup(value)) profileDialog = value; } }

        /// <summary> settings shortcut </summary>
        [CategoryAttribute("Dialogs"), DefaultValueAttribute(typeof(Shortcut), "F11"),
        Description("Show settings dialog."), DisplayName("Settings")]
        public Shortcut SettingsDialog { get { return settingsDialog; } set { if (Checkup(value)) settingsDialog = value; } }

        /// <summary> bookmarks shortcut </summary>
        [CategoryAttribute("Dialogs"), DefaultValueAttribute(typeof(Shortcut), "F9"),
        Description("Show bookmarks dialog."), DisplayName("Bookmarks")]
        public Shortcut BookmarkDialog { get { return bookmarkDialog; } set { if (Checkup(value)) bookmarkDialog = value; } }

        /// <summary> check for update shortcut </summary>
        [CategoryAttribute("Dialogs"), DefaultValueAttribute(typeof(Shortcut), "F12"),
        Description("Check for update."), DisplayName("Update")]
        public Shortcut CheckForUpdate { get { return checkForUpdate; } set { if (Checkup(value)) checkForUpdate = value; } }

        /// <summary> quickconnect shortcut </summary>
        [CategoryAttribute("Database connection"), DefaultValueAttribute(typeof(Shortcut), "ShiftF8"),
        Description("Show quickconnect."), DisplayName("Quickconnect")]
        public Shortcut QuickConnect { get { return quickConnect; } set { if (Checkup(value)) quickConnect = value; } }

        /// <summary> minimize to systray shortcut </summary>
        [CategoryAttribute("App"), DefaultValueAttribute(typeof(Shortcut), "ShiftF7"),
        Description("Minimize to system tray."), DisplayName("Minimize to systray")]
        public Shortcut MinimizeToSystray { get { return minimizeToSystray; } set { if (Checkup(value)) minimizeToSystray = value; } }

        /// <summary> exit nyx shortcut </summary>
        [CategoryAttribute("App"), DefaultValueAttribute(typeof(Shortcut), "CtrlQ"),
        Description("Exit Nyx."), DisplayName("Exit Nyx")]
        public Shortcut Exit { get { return exit; } set { if (Checkup(value)) exit = value; } }

        private bool Checkup(Shortcut sc)
        {
            if (this.bookmarkDialog != sc && this.checkForUpdate != sc && this.clearSql != sc && this.closeSqlTab != sc &&
                this.connect != sc && this.disconnect != sc && this.executeSelectedSql != sc &&
                this.executeSql != sc && this.exit != sc && this.minimizeToSystray != sc && this.newSqlTab != sc &&
                this.openSql != sc && this.profileDialog != sc && this.quickConnect != sc && this.saveSql != sc &&
                this.settingsDialog != sc && this.showAutoCompletition != sc && this.showSqlBuilder != sc &&
                this.viewPrcdrs != sc && this.viewPreview != sc && this.viewSql != sc && this.viewTables != sc &&
                this.viewTriggers != sc && this.viewViews != sc)
            {
                return true;
            }
            else
            {
                MessageBox.Show(Settings.GetResourceMsg("HotkeyAssigned"), 
                    Settings.GetResourceMsg("DefaultTitle"), MessageBoxButtons.OK, MessageBoxIcon.Warning, 
                    MessageBoxDefaultButton.Button1, MessageBoxOptions.RightAlign);
                return false;
            }
        }
    }
}
