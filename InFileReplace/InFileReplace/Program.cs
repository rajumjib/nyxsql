/****************************************************************
 * NyxSql                                                       *
 * license: gpl (see doc\License-GPL.txt)                       *
 * copyright: Dominik Schischma                                 *
 ***************************************************************/
using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Xml.Serialization;
using NyxSettings;

namespace InFileReplace
{
    class Program
    {
        private static ArrayList alBookmark = new ArrayList();
        private static ArrayList alHistory = new ArrayList();
        private static ArrayList alProfile = new ArrayList();
        private static ArrayList alRuntime = new ArrayList();
        private static ArrayList alAppsettings = new ArrayList();
        private static ArrayList alHotkey = new ArrayList();

        private static Configuration[] config;

        static void Main(string[] args)
        {
            switch (args.Length)
            {
                default:
                    Console.WriteLine("InFileReplace " + Assembly.GetExecutingAssembly().GetName().Version);
                    Console.WriteLine("License  : GPL");
                    Console.WriteLine("Copyright: 2006 Dominik Schischma");
                    Console.WriteLine("---------------------------------");
                    Console.WriteLine("Usage: ");
                    Console.WriteLine(" ifr.exe <cfgfile> ");
                    ExitWithKeypress(2);
                    break;
                case 1:
                    // check if file exists
                    if (!File.Exists(args[0].ToString()))
                        ErrorHandler("Configuration file not found...", true);
                    break;
            }
            string cfgFile = args[0].ToString();
            // load configuration
            DeSerialize(cfgFile);
            if (config.Length == 0)
                ErrorHandler("No configuration found inside " + cfgFile, true);

            // get the save path
            UserAppSettings uSett = new UserAppSettings();
            Settings.DeserializeUserSettings(ref uSett, Settings.GetFolder(SettingsType.AppSettings));

            // sort the actions to edit
            foreach (Configuration cfg in config)
            {
                switch (cfg.CfgFile)
                {
                    case SettingsType.AppSettings: 
                        alAppsettings.Add(cfg); 
                        break;
                    case SettingsType.Bookmarks: 
                        alBookmark.Add(cfg); 
                        break;
                    case SettingsType.History: 
                        alHistory.Add(cfg); 
                        break;
                    case SettingsType.Hotkeys: 
                        alHotkey.Add(cfg); 
                        break;
                    case SettingsType.Profile: 
                        alProfile.Add(cfg); 
                        break;
                    case SettingsType.Runtime: 
                        alRuntime.Add(cfg); 
                        break;
                }
            }

            updateSettings(SettingsType.AppSettings);
            updateSettings(SettingsType.Bookmarks);
            updateSettings(SettingsType.History);
            updateSettings(SettingsType.Hotkeys);
            updateSettings(SettingsType.Profile);
            updateSettings(SettingsType.Runtime);
            ExitWithKeypress(0);
        }

        private static bool CreateBackup(string file)
        {   // create backup file
            try
            {
                File.Copy(file, file + ".bak", true);
                return true;
            }
            catch (Exception exc)
            {
                ErrorHandler("Error creating backupfile: \n" + exc.Message.ToString(), false);
                return false;
            }
        }
        private static void EditFile(string file, string search, string replace)
        {
            // start edit line by line
            TextReader tr = null;
            TextWriter tw = null;
            try
            {
                int cnt = 0;
                string line;
                tr = new StreamReader(file + ".bak");
                tw = new StreamWriter(file, false);
                while ((line = tr.ReadLine()) != null)
                {
                    // to edit?
                    if (line.Contains(search))
                    {
                        line = line.Replace(search, replace);
                        Console.Write(".");
                        cnt++;
                    }
                    tw.WriteLine(line);
                }
                Console.WriteLine("Sucessfully replaced: {0} lines edited.", cnt);
            }
            catch (Exception exc)
            {
                ErrorHandler("Error parsing/editing file: \n" + exc.Message.ToString(), true);
            }
            finally
            {
                if (tr != null)
                    tr.Close();
                if (tw != null)
                {
                    tw.Flush();
                    tw.Close();
                }
            }
        }

        private static void ErrorHandler(string msg, bool fatal)
        {
            Console.WriteLine(msg);
            if (fatal)  // exit?
                ExitWithKeypress(3);
        }
        private static void ExitWithKeypress(int returncode)
        {
            Console.Write("\nPress any key to exit.");
            Console.Read();
            Environment.Exit(returncode);
        }

        private static void updateSettings(SettingsType settingType)
        {
            ArrayList modifiedSettings = new ArrayList();
            string fileloc = "";
            switch (settingType)
            {
                case SettingsType.AppSettings:
                    modifiedSettings = alAppsettings;
                    fileloc = Settings.GetFolder(SettingsType.AppSettings) + Settings.FileNameUserSettings;
                    break;
                case SettingsType.Bookmarks:
                    modifiedSettings = alBookmark;
                    fileloc = Settings.GetFolder(SettingsType.Bookmarks) + Settings.FileNameBookmark;
                    break;
                case SettingsType.History:
                    modifiedSettings = alHistory;
                    fileloc = Settings.GetFolder(SettingsType.History) + Settings.FileNameHistory;
                    break;
                case SettingsType.Hotkeys:
                    modifiedSettings = alHotkey;
                    fileloc = Settings.GetFolder(SettingsType.Hotkeys) + Settings.FileNameHotkeys;
                    break;
                case SettingsType.Profile:
                    modifiedSettings = alProfile;
                    fileloc = Settings.GetFolder(SettingsType.Profile) + Settings.FileNameProfile;
                    break;
                case SettingsType.Runtime:
                    modifiedSettings = alRuntime;
                    fileloc = Settings.GetFolder(SettingsType.Runtime) + Settings.FileNameRuntime;
                    break;
                default:
                    Console.WriteLine("Could not identify the type of setting.");
                    break;
            }

            if (modifiedSettings.Count > 0)
            {
                // get file location and exec backup
                Console.WriteLine("UserSettings: Replacing in " + fileloc);
                if (CreateBackup(fileloc))
                    foreach (Configuration cfg in alAppsettings)
                        EditFile(fileloc, cfg.Search, cfg.Replace);
                else
                    Console.WriteLine("Skipping file...");
            }
        }

        public static void DeSerialize(string file)
        {   // init vars
            StreamReader sr = null;
            XmlSerializer xs = new XmlSerializer(typeof(Configuration[]));
            // deserialize
            try
            {   // streamreader
                sr = File.OpenText(file);
                // readin and close reader
                config = (Configuration[])xs.Deserialize(sr);
            }
            catch (Exception exc)
            {
                ErrorHandler("Error loading Configuration:\n" + exc.Message.ToString(), true);
            }
            finally
            {
                if (sr != null)
                    sr.Close();
            }
        }
    }

    /// <summary>  
    /// Single search and replace class  
    /// </summary>  
    public class Configuration
    {
        private SettingsType cfgFile;
        private string search;
        private string replace;

        /// <remarks> configuration file to edit </remarks> 
        public SettingsType CfgFile { get { return cfgFile; } set { cfgFile = value; } }
        /// <remarks> string to search </remarks>
        public string Search { get { return search; } set { search = value; } }
        /// <remarks> string to replace </remarks> 
        public string Replace { get { return replace; } set { replace = value; } }
    }
}
